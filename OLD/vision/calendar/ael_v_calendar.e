note
	description: "A portable calendar widget";
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
To use this widget, create it using either 'make' or 'make_for_gmt'.
Then add to your container and disable expansion of this widget.
Because the parent class is EV_FRAME, you have the option to define
a frame label string, via 'set_text'.
If you wish to receive a notification on each change to this widget,
you can register your agent using 'set_change_notify_procedure'.
To change the apparent date programmatically, create a new AEL_DATE_TIME
object and provide it to the widget using 'set_date'.
To reset the widget to the current date, call 'set_date_to_present'.
To create a calendar in different languages, provide an alternate
implementation of AEL_DATE_TIME_NAMES using the Eiffel cluster
mechanism.
}"

class AEL_V_CALENDAR

inherit
	EV_FRAME
	AEL_V_WIDGET
		undefine
			is_equal
		redefine
			make, make_components, set_initial_values, set_actions
		end
	AEL_PRINTF
		undefine
			out, default_create, is_equal, copy
		end

create
	make, make_for_gmt

feature {NONE}

	make is
		do
			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			Precursor
			set_date_to_present
		end


	make_for_gmt is
		-- Create the calendar as usual, but base the time on GMT (UTC)
		do
			use_gmt := True
			make
		end

feature {NONE}

	make_components is
		-- Add the components that make up the calendar widget
		do
			Precursor

			create date_box
			extend (date_box)

			-------------------------

			create cal_ctl_frame
			date_box.extend (cal_ctl_frame)
			create cal_ctl
			cal_ctl_frame.extend (cal_ctl)

			cal_ctl.set_minimum_height (22)
			date_box.disable_item_expand (cal_ctl_frame)

			---------------

			create cal_month_ctl
			cal_ctl.extend (cal_month_ctl)

			create cal_month_ctl_dn_btn
			cal_month_ctl_dn_btn.set_pixmap (create {AEL_V_SMALL_LEFT_ARROW_PIXMAP}.make)
			cal_month_ctl.extend (cal_month_ctl_dn_btn)
			cal_month_ctl_dn_btn.set_tooltip ("Go to previous month")
			cal_month_ctl_dn_btn.set_minimum_width (12)
			cal_month_ctl.disable_item_expand (cal_month_ctl_dn_btn)

			create cal_month_ctl_label.make_with_text (current_month_string)
			cal_month_ctl.extend (cal_month_ctl_label)
			cal_month_ctl_label.set_minimum_width (90)
			cal_month_ctl.disable_item_expand (cal_month_ctl_label)

			create cal_month_ctl_up_btn
			cal_month_ctl_up_btn.set_pixmap (create {AEL_V_SMALL_RIGHT_ARROW_PIXMAP}.make)
			cal_month_ctl.extend (cal_month_ctl_up_btn)
			cal_month_ctl_up_btn.set_tooltip ("Go to next month")
			cal_month_ctl_up_btn.set_minimum_width (12)
			cal_month_ctl.disable_item_expand (cal_month_ctl_up_btn)

			cal_ctl.disable_item_expand (cal_month_ctl)

			-------------------------

			cal_ctl.extend (create {EV_CELL })

			-------------------------

			create cal_year_ctl
			cal_ctl.extend (cal_year_ctl)

			create cal_year_ctl_dn_btn
			cal_year_ctl_dn_btn.set_pixmap (create {AEL_V_SMALL_LEFT_ARROW_PIXMAP}.make)
			cal_year_ctl.extend (cal_year_ctl_dn_btn)
			cal_month_ctl_dn_btn.set_tooltip ("Go to previous year")
			cal_year_ctl_dn_btn.set_minimum_width (12)
			cal_year_ctl.disable_item_expand (cal_year_ctl_dn_btn)

			create cal_year_ctl_label.make_with_text (current_year_string)
			cal_year_ctl.extend (cal_year_ctl_label)
			cal_year_ctl_label.set_minimum_width (60)
			cal_year_ctl.disable_item_expand (cal_year_ctl_label)

			create cal_year_ctl_up_btn
			cal_year_ctl_up_btn.set_pixmap (create {AEL_V_SMALL_RIGHT_ARROW_PIXMAP}.make)
			cal_year_ctl.extend (cal_year_ctl_up_btn)
			cal_year_ctl_up_btn.set_tooltip ("Go to next year")
			cal_year_ctl_up_btn.set_minimum_width (12)
			cal_year_ctl.disable_item_expand (cal_year_ctl_up_btn)

			cal_ctl.disable_item_expand (cal_year_ctl)

			-------------------------

			create cal_tbl
			cal_tbl.resize (7, 7)
			date_box.extend (cal_tbl)
			cal_tbl.set_minimum_width (251)
			date_box.disable_item_expand (cal_tbl)

			set_minimum_width (255)
		end


	make_column_titles is
		-- Create and add the day-of-week column headings
		local
			lb: like label_at_position
			la: like hdr_labels
			i: INTEGER
		do
			la := hdr_labels
			from i := 1
			until i > 7
			loop
				create lb.make_for_header (la.item(i), i = 1)
				lb.set_date (date)
				lb.set_minimum_width (35)
				cal_tbl.put_at_position (lb, i, 1, 1, 1)
				i := i + 1
			end
		end


	set_initial_values is
		do
			Precursor
			cal_tbl.set_row_spacing (1)
			cal_tbl.set_column_spacing (1)
			make_column_titles
			fill_blank_weeks
			update_calendar_for_date
		end


	set_actions is
		do
			Precursor

			cal_month_ctl_dn_btn.select_actions.extend(
																agent on_ctl_select(cal_month_ctl_dn_btn))
			cal_month_ctl_up_btn.select_actions.extend(
																agent on_ctl_select(cal_month_ctl_up_btn))
			cal_year_ctl_dn_btn.select_actions.extend(
																agent on_ctl_select(cal_year_ctl_dn_btn))
			cal_year_ctl_up_btn.select_actions.extend(
																agent on_ctl_select(cal_year_ctl_up_btn))
		end

feature {NONE} -- Components

	cal_box: EV_HORIZONTAL_BOX

	date_box: EV_VERTICAL_BOX

	cal_ctl_frame: EV_FRAME
	cal_ctl: EV_HORIZONTAL_BOX

	cal_month_ctl: EV_HORIZONTAL_BOX
	cal_month_ctl_dn_btn: EV_BUTTON
	cal_month_ctl_label: EV_LABEL
	cal_month_ctl_up_btn: EV_BUTTON

	cal_year_ctl: EV_HORIZONTAL_BOX
	cal_year_ctl_dn_btn: EV_BUTTON
	cal_year_ctl_label: EV_LABEL
	cal_year_ctl_up_btn: EV_BUTTON

	cal_tbl: EV_TABLE


	label_at_position (wd, wno: INTEGER): AEL_V_CALENDAR_DAY_LABEL is
		do
			Result ?= cal_tbl.item_at_position (wd, wno)
		end

feature -- Status

	date: AEL_DATE_TIME
	reference_date: AEL_DATE_TIME
		-- Date at time of creation, or resetting to present


	current_month_string: STRING is
		do
			Result := cal_const.month_long_names.item (date.month)
		end


	current_year_string: STRING is
		local
			ti: INTEGER
		do
			ti := date.year
			Result := ti.out
		end

feature -- Update notification

	set_change_notify_procedure (v: like change_notify_proc) is
		-- Define the agent to call when there is a change to the calendar.
		-- For example, when the year or month is changed by the user, or
		-- the user selects a day cell.
		do
			change_notify_proc := v
		end

feature -- Value setting

	set_use_gmt (tf: BOOLEAN) is
		do
			use_gmt := tf
		end


	set_date_to_present is
		do
			if use_gmt then
				create reference_date.make_now_utc
				create date.make_now_utc
			else
				create reference_date.make_now
				create date.make_now
			end
			update_calendar_for_date
		end


	set_date (v: like date) is
		require
			date_exists: v /= Void
		do
			date := v
			update_calendar_for_date
		end

feature {NONE} -- Basic operation

	update_calendar_for_date is
		local
			nw: INTEGER
			idx: INTEGER
		do
			--| The calendar matrix is 7 days wide (columns)
			--| by 6 weeks long (rows), not including the header row
			--| This accomodates all possible week starts and month lengths.
			--| The "largest" month would be one with 31 days starting on a
			--| Saturday
			--| The "smallest" month would be February, starting on a Sunday

			create year.make (date.year, False)
			create month.make (date.month, year)
			nw := month.number_of_weeks
			number_of_weeks := nw

			-- Weeks 2 through 4 will have all days updated, so
			-- there is no need to clear them first

			clear_week (1)
			clear_week (5)
			clear_week (6)

			from idx := 1
			until idx > nw
			loop
				fill_week (month, idx)
				idx := idx + 1
			end

			cal_month_ctl_label.set_text (current_month_string)
			cal_year_ctl_label.set_text (current_year_string)
		end


	fill_week (mo: AEL_DT_CALENDAR_MONTH; wno: INTEGER) is
		require
			month_exists: mo /= Void
		local
			days: ARRAY [ STRING ]
			d, idx, nw: INTEGER
			lb: like label_at_position
			wk: AEL_DT_CALENDAR_WEEK
		do
			nw := mo.number_of_weeks
			idx := wno + 1

			create wk.make (wno, month)
			days := wk.days
			from d := 1
			until d > 7
			loop
				lb := label_at_position (d, idx)
				-- If is today, then set reference date
				lb.set_date (date)
				lb.set_text (days.item(d))
				lb.disable_selected
				d := d + 1
			end
		end


	clear_week (wno: INTEGER) is
		local
			d, w: INTEGER
			lb: like label_at_position
		do
			w := wno + 1
			from d := 1
			until d > 7
			loop
				lb := label_at_position (d, w)
				lb.remove_text
				d := d + 1
			end
		end


	clear_day_selections is
		local
			lb: like label_at_position
		do
			from cal_tbl.start
			until cal_tbl.exhausted
			loop
				lb ?= cal_tbl.item
				lb.disable_selected
				cal_tbl.forth
			end
			cal_tbl.start
		end


	fill_blank_weeks is
		local
			d, w: INTEGER
			lb: like label_at_position
		do
			from w := 2
			until w > 7
			loop
				from d := 1
				until d > 7
				loop
					create lb.make_with_day (d)
					lb.set_date (date)
					lb.set_selection_proc (agent on_day_select)
					lb.set_minimum_width (35)
					cal_tbl.put_at_position (lb, d, w, 1, 1)
					d := d + 1
				end
				w := w + 1
			end
		end

feature {NONE} -- Private attributes

	number_of_weeks: INTEGER
	year: AEL_DT_CALENDAR_YEAR
	month: AEL_DT_CALENDAR_MONTH

	use_gmt: BOOLEAN

feature {NONE} -- Actions and agents

	on_day_select (lb: like label_at_position) is
		do
			clear_day_selections
			lb.enable_selected
			--| update current date to new day
			date.set_day (lb.day_of_month)
			notify_clients_of_change
		end


	on_ctl_select (b: like cal_month_ctl_dn_btn) is
		do
			if b = cal_month_ctl_dn_btn then
				date.date.month_back
			elseif b = cal_month_ctl_up_btn then
				date.date.month_forth
			elseif b = cal_year_ctl_dn_btn then
				date.date.year_back
			elseif b = cal_year_ctl_up_btn then
				date.date.year_forth
			else
			end
			clear_day_selections
			update_calendar_for_date
			notify_clients_of_change
		end


	notify_clients_of_change is
		do
--			if attached change_notify_proc as p then
--				p.call ([ Current ])
--			end
		end

	change_notify_proc: detachable PROCEDURE [ ANY, TUPLE [ like Current ] ]

feature {NONE} -- Constants

	hdr_labels: ARRAY [ STRING ] is
		once
			Result := cal_const.day_short_names
		end


	cal_const: AEL_DATE_TIME_CONSTANTS is
		once
			create Result
		end

end -- class AEL_V_CALENDAR
