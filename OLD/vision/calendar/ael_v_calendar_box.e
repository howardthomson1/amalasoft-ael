note
	description: "A container hold a calendar widget and other controls"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
To use this widget, create it using either 'make' or 'make_for_gmt'.
Then add to your container.
This widget is not intended to be a finished product, but rather a
working example of how to use the AEL_V_CALENDAR widget.
}"

class AEL_V_CALENDAR_BOX

inherit
	EV_VERTICAL_BOX
		redefine
			destroy
		end
	AEL_V_WIDGET
		undefine
			is_equal
		redefine
			make, make_components, set_actions,
			update_widget_rendition
		end
	AEL_PRINTF
		undefine
			out, default_create, is_equal, copy
		end

create
	make, make_for_gmt

feature {NONE}

	make is
		do
			if use_gmt then
				create original_date.make_now_utc
				create date.make_now_utc
			else
				create original_date.make_now
				create date.make_now
			end
			Precursor
			update_widget_rendition
		end

	make_for_gmt is
		do
			use_gmt := True
			make
		end

feature {NONE}

	make_components is
		do
			Precursor

			create upper_box
			extend (upper_box)

			if use_gmt then
				create calendar.make_for_gmt
			else
				create calendar.make
			end
			upper_box.extend (calendar)
			calendar.set_text ("Date")
			upper_box.disable_item_expand (calendar)

			--------------------------

			build_time_box
			upper_box.extend (time_box_frame)

			--------------------------

			create gmt_box
			extend (gmt_box)
			gmt_box.extend (create {EV_CELL})

			create gmt_btn.make_with_text ("Show time as GMT")
			gmt_box.extend (gmt_btn)
			gmt_btn.set_minimum_height (22)
			gmt_box.disable_item_expand (gmt_btn)
		end

	build_time_box is
		local
			tc: EV_CELL
		do
			create time_box_frame.make_with_text ("Time")
			create time_box
			time_box_frame.extend (time_box)

			-------------------------

			create current_time_box
			time_box.extend (current_time_box)
			current_time_box.set_minimum_height (25)
			time_box.disable_item_expand (current_time_box)

			create tc
			current_time_box.extend (tc)
			tc.set_minimum_width (10)
			current_time_box.disable_item_expand (tc)

			create time_label.make_with_text ("Current Time:")
			time_label.set_tooltip ("Press to reset displayed time values to current time")
			current_time_box.extend (time_label)
			time_label.align_text_right

			create tc
			current_time_box.extend (tc)
			tc.set_minimum_width (5)
			current_time_box.disable_item_expand (tc)

			create time_textw.make_with_text (current_time_string)
			current_time_box.extend (time_textw)
			time_textw.set_minimum_width (70)
			current_time_box.disable_item_expand (time_textw)
			time_textw.align_text_left

			current_time_box.extend (create {EV_CELL})

			-------------------------

			time_box.extend (create {EV_CELL})

			create hour_box
			time_box.extend (hour_box)
			hour_box.set_minimum_height (25)
			time_box.disable_item_expand (hour_box)
			create hour_label.make_with_text ("Hour:")
			hour_box.extend (hour_label)
			hour_label.align_text_right
			create tc
			hour_box.extend (tc)
			tc.set_minimum_width (10)
			hour_box.disable_item_expand (tc)

			create hour_ctl.make_with_value_range (hours_range)
			hour_box.extend (hour_ctl)
			hour_ctl.set_minimum_width (70)
			hour_box.disable_item_expand (hour_ctl)

			-------------------------

			time_box.extend (create {EV_CELL})

			create minute_box
			time_box.extend (minute_box)
			minute_box.set_minimum_height (25)
			time_box.disable_item_expand (minute_box)
			create minute_label.make_with_text ("Minute:")
			minute_box.extend (minute_label)
			minute_label.align_text_right
			create tc
			minute_box.extend (tc)
			tc.set_minimum_width (10)
			minute_box.disable_item_expand (tc)
			create minute_ctl.make_with_value_range (minutes_range)
			minute_box.extend (minute_ctl)
			minute_ctl.set_minimum_width (70)
			minute_box.disable_item_expand (minute_ctl)

			-------------------------

			time_box.extend (create {EV_CELL})

			create second_box
			time_box.extend (second_box)
			second_box.set_minimum_height (25)
			time_box.disable_item_expand (second_box)
			create second_label.make_with_text ("Second:")
			second_box.extend (second_label)
			second_label.align_text_right
			create tc
			second_box.extend (tc)
			tc.set_minimum_width (10)
			second_box.disable_item_expand (tc)
			create second_ctl.make_with_value_range (seconds_range)
			second_box.extend (second_ctl)
			second_ctl.set_minimum_width (70)
			second_box.disable_item_expand (second_ctl)

			-------------------------

			time_box.extend (create {EV_CELL})
		end

	set_actions is
		do
			Precursor

			calendar.set_change_notify_procedure (agent on_calendar_change)

			time_label.select_actions.extend (agent on_current_time_select)

			gmt_btn.select_actions.extend (agent on_gmt_select(gmt_btn))

			hour_ctl.change_actions.extend (agent on_hour_change)
			minute_ctl.change_actions.extend (agent on_minute_change)
			second_ctl.change_actions.extend (agent on_second_change)

			hour_ctl.return_actions.extend (agent
			                                on_time_unit_text_change(hour_ctl))
			minute_ctl.return_actions.extend (agent
			                                  on_time_unit_text_change(minute_ctl))
			second_ctl.return_actions.extend (agent
			                                  on_time_unit_text_change(second_ctl))

			create update_timer.make_with_interval (900)
			update_timer.actions.extend (agent on_tick)
		end

feature {NONE} -- Components

	upper_box: EV_HORIZONTAL_BOX
	calendar: AEL_V_CALENDAR

	gmt_box: EV_HORIZONTAL_BOX
	gmt_btn: EV_CHECK_BUTTON

	---------------

	time_box_frame: EV_FRAME
	time_box: EV_VERTICAL_BOX

	current_time_box: EV_HORIZONTAL_BOX
	time_label: EV_BUTTON
	time_textw: EV_LABEL

	hour_box: EV_HORIZONTAL_BOX
	hour_label: EV_LABEL
	hour_ctl: EV_SPIN_BUTTON

	minute_box: EV_HORIZONTAL_BOX
	minute_label: EV_LABEL
	minute_ctl: EV_SPIN_BUTTON

	second_box: EV_HORIZONTAL_BOX
	second_label: EV_LABEL
	second_ctl: EV_SPIN_BUTTON

feature -- Queries

	date: AEL_DATE_TIME

	current_time_string: STRING is
		do
			if use_gmt then
				Result := current_gmt_time_string
			else
				Result := current_local_time_string
			end
		end

	current_local_time_string: STRING is
		local
			tm: like date
		do
			create tm.make_now
			Result := aprintf (Ks_time_only_fmt,
			                   << tm.hour, tm.minute, tm.second >>)
		end

	current_gmt_time_string: STRING is
		local
			tm: like date
		do
			create tm.make_now_utc
			Result := aprintf (Ks_time_only_fmt,
			                   << tm.hour, tm.minute, tm.second >>)
		end

	use_gmt: BOOLEAN

feature -- Update agents

	set_change_notify_procedure (v: like change_notify_proc) is
		do
			change_notify_proc := v
		end

	change_notify_proc: PROCEDURE [ANY, TUPLE [like Current]]

feature -- Value setting

	set_use_gmt (tf: BOOLEAN) is
		do
			if tf then
				gmt_btn.enable_select
			else
				gmt_btn.disable_select
			end
			update_widget_rendition
		end

	set_date_to_present is
		do
			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			update_widget_rendition
		end

	set_date (v: like date) is
		require
			date_exists: v /= Void
		do
			date := v
			update_widget_rendition
		end

feature {NONE} -- Basic operation

	update_widget_rendition is
		do
			Precursor
			hour_ctl.set_text (date.hour.out)
			minute_ctl.set_text (date.minute.out)
			second_ctl.set_text (date.second.out)
			calendar.set_date (date)
		end

feature {NONE} -- Private attributes

	reference_date: like date
	original_date: like date

feature {NONE} -- Callbacks and agents

	on_calendar_change (c: like calendar) is
		do
		end

	on_current_time_select is
		do
			set_date_to_present
			notify_clients_of_change
		end

	on_time_unit_text_change (w: like hour_ctl) is
		local
			has_error: BOOLEAN
			ts: STRING
		do
			if w = hour_ctl then
				ts := w.text
				if cal_routines.is_valid_hour_value (ts) then
					date.set_hour (ts.to_integer)
				else
					has_error := True
					report_error_msg ("Value for hours must be between 0 and 23.")
				end
			elseif w = minute_ctl then
				ts := w.text
				if cal_routines.is_valid_minute_value (ts) then
					date.set_minute (ts.to_integer)
				else
					has_error := True
					report_error_msg ("Value for minutes must be between 0 and 59.")
				end
			elseif w = second_ctl then
				ts := w.text
				if cal_routines.is_valid_second_value (ts) then
					date.set_second (ts.to_integer)
				else
					has_error := True
					report_error_msg ("Value for seconds must be between 0 and 59.")
				end
			end
			notify_clients_of_change
		end

	on_hour_change (v: INTEGER) is
		do
			date.set_hour (v)
			notify_clients_of_change
		end

	on_minute_change (v: INTEGER) is
		do
			date.set_minute (v)
			notify_clients_of_change
		end

	on_second_change (v: INTEGER) is
		do
			--| do nothing, it's lost in the noise (for now)
		end

	on_gmt_select (tb: like gmt_btn) is
		do
			if gmt_btn.is_selected then
				use_gmt := True
			else
				use_gmt := False
			end
			-- Update

			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			update_widget_rendition
			notify_clients_of_change
		end

	notify_clients_of_change is
		do
			if change_notify_proc /= Void then
				change_notify_proc.call ([ Current ])
			end
		end

	update_timer: EV_TIMEOUT

	on_tick is
			-- Keep the current time display up to date
		do
			if is_displayed and not application.is_destroyed then
				time_textw.set_text (current_time_string)
			end
		end

	kill_update_timer is
		do
			if update_timer /= Void then
				update_timer.actions.wipe_out
				update_timer := Void
			end
		end

	destroy is
		do
			if update_timer /= Void then
				kill_update_timer
			end
			Precursor
		end

feature -- Error reporting

	report_error_msg (msg: STRING) is
		do
			--| Redefine this to tie into your error reporting/recording system
		end

feature {NONE} -- Constants

	Ks_time_only_fmt: STRING is "%%#02d:%%#02d:%%#02d"

	hours_range: INTEGER_INTERVAL is
		once
			create Result.make (0, 23)
		end

	minutes_range: INTEGER_INTERVAL is
		once
			create Result.make (0, 59)
		end

	seconds_range: INTEGER_INTERVAL is
		once
			create Result.make (0, 59)
		end

	cal_routines: AEL_DATE_TIME_ROUTINES is
		once
			create Result
		end

end -- class AEL_V_CALENDAR_BOX
