note
	description: "An individual day cell for a the AEL_V_CALENDAR widget"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
This widget is a component of AEL_V_CALENDAR and is not to be created
and used by itself.
}"

class AEL_V_CALENDAR_DAY_LABEL

inherit
	EV_LABEL
		redefine
			set_text
		end
	AEL_V_WIDGET
		redefine
			make, set_actions, update_widget_rendition
		end

create
	make_with_day, make_with_label, make_for_sunday, make_for_header

feature {NONE} -- Creation

	make is
		do
			Precursor
			update_widget_rendition
		end

	make_with_day (v: INTEGER) is
		do
			if v = 1 then
				make_for_sunday
			else
				make
			end
		end

	make_for_header (v: STRING; sf: BOOLEAN) is
		require
			text_exists: v /= Void
		do
			is_header := True
			is_sunday := sf
			make
			set_text (v)
		end

	make_with_label (v: STRING; sf: BOOLEAN) is
		require
			text_exists: v /= Void
		do
			is_sunday := sf
			make
			set_text (v)
		end

	make_for_sunday is
		do
			is_sunday := True
			make
		end

feature {NONE} -- Agents and actions

	set_actions is
		do
			Precursor
			pointer_button_press_actions.extend (agent on_btn_press)
		end

	on_btn_press (x, y, button: INTEGER; xt, yt, pressure: DOUBLE; sx, sy: INTEGER) is
		do
			if selection_proc /= Void and has_text then
				selection_proc.call ([Current])
			end
		end

	selection_proc: PROCEDURE [ ANY, TUPLE [ like Current ] ]

feature {AEL_V_CALENDAR} -- Status setting

	set_text (v: STRING) is
		do
			Precursor (v)
			update_widget_rendition
		end

	enable_selected is
		do
			is_selected := True
			update_widget_rendition
		end

	disable_selected is
		do
			is_selected := False
			update_widget_rendition
		end

feature {AEL_V_CALENDAR} -- Client-settable actions

	set_selection_proc (v: like selection_proc) is
			-- Set the agent to call when this label is selected
		do
			selection_proc := v
		end

feature {AEL_V_CALENDAR} -- Status

	is_sunday: BOOLEAN
	is_header: BOOLEAN
	is_selected: BOOLEAN

	has_text: BOOLEAN is
		do
			Result := text /= Void and then not text.is_empty
		end

	is_today: BOOLEAN is
			-- Is this day actually today?
		local
			tm: like date
		do
			if date /= Void then
				create tm.make_now
				if tm.month = date.month then
					Result := tm.day = day_of_month
				end
			end
		end

	is_reference_date: BOOLEAN is
			-- Is the date associated with this label the same as the date
			-- defined to be the reference date?
		do
			if reference_date /= Void then
				if reference_date.month = date.month then
					Result := reference_date.day = day_of_month
				end
			end
		end

	set_date (v: like date) is
			-- Set the date associated with this day label
		do
			date := v
		end

	set_reference_date (v: like date) is
			-- Set the given date to be a reference date.	This could be a
			-- a start date, or end date, or whatever is deemed important.
			-- This is not required.
		do
			reference_date := v
		end

	day_of_month: INTEGER is
		do
			if has_text and not is_header then
				Result := text.to_integer
			end
		end

	date: AEL_DATE_TIME
	reference_date: AEL_DATE_TIME

feature {NONE} -- Implementation

	update_widget_rendition is
		do
			Precursor
			set_background_color (bg_color)
			set_foreground_color (fg_color)
		end

	bg_color: EV_COLOR is
		do
			if is_header then
				Result := colors.light_gray
			else
				if is_selected then
					Result := colors.cyan
				else
					if is_today then
						Result := colors.light_gray
					elseif is_reference_date then
						Result := colors.pale_green
					else
						Result := colors.white
					end
				end
			end
		end

	fg_color: EV_COLOR is
		do
			if is_header then
				Result := colors.black
			elseif is_today then
				if is_sunday then
					Result := colors.dark_red
				else
					Result := colors.blue
				end
			elseif is_reference_date then
				if is_sunday then
					Result := colors.dark_red
				else
					Result := colors.blue
				end
			else
				if is_sunday then
					Result := colors.red
				else
					Result := colors.black
				end
			end
		end

end -- class AEL_V_CALENDAR_DAY_LABEL
