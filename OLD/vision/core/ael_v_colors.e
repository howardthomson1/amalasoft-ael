note
	description: "Common color constants and support routines"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 002$"
	howto: "{
    This class is included in the AEL_V_CONSTANTS class as a supplier.
    It is therefore available to classes that inherit AEL_V_CONTANTS,
    included AEL_V_WIDGET.
    This class can be instantiated by itself if desired.
}"

class AEL_V_COLORS

inherit
	EV_STOCK_COLORS

create
	default_create

--|========================================================================
feature -- Colors not defined already in EV_STOCK_COLORS
--|========================================================================

	brown: EV_COLOR is
		once
			create Result.make_with_rgb (0.584, 0.506, 0.404)
		end

	dark_brown: EV_COLOR is
		once
			create Result.make_with_rgb (0.443, 0.388, 0.365)
		end

	dark_navy: EV_COLOR is
		once
			create Result.make_with_rgb (0.306, 0.286, 0.365)
		end

	extra_pale_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.9, 1, 1)
		end

	extra_pale_green: EV_COLOR is
		once
			create Result.make_with_rgb (0.8, 1, 0.8)
		end

	forest_green: EV_COLOR is
		once
			create Result.make_with_rgb (0.1333, 0.545, 0.1333)
		end

	gold: EV_COLOR is
		once
			create Result.make_with_rgb (1, 0.843, 0)
		end

	honeydew: EV_COLOR is
		once
			create Result.make_with_rgb (0.94118, 1, 0.94118)
		end

	light_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.6784, .847, .9019)
		end

	light_brown: EV_COLOR is
		once
			create Result.make_with_rgb (0.757, 0.639, 0.486)
		end

	light_cyan: EV_COLOR is
		once
			create Result.make_with_rgb (0.87843, 1, 1)
		end

	light_goldenrod: EV_COLOR is
		once
			create Result.make_with_rgb (0.9333, 0.8666, 0.5098)
		end

	light_gray: EV_COLOR is
		once
			create Result.make_with_rgb (0.9, 0.9, 0.9)
		end

	light_green: EV_COLOR is
		once
			create Result.make_with_rgb (0.863, 1, 0.863)
		end

	light_pink: EV_COLOR is
		once
			create Result.make_with_rgb (1, 0.7137, 0.7569)
		end

	light_sky_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.5294, .8078, .9804)
		end

	light_yellow: EV_COLOR is
		once
			create Result.make_with_rgb (1, 1, 0.4)
		end

	linen: EV_COLOR is
		once
			create Result.make_with_rgb (0.9804, 0.9412, 0.902)
		end

	mint_cream: EV_COLOR is
		once
			create Result.make_with_rgb (0.9608, 1, 0.9804)
		end

	navy: EV_COLOR is
		once
			create Result.make_with_rgb (0, 0, 0.5)
		end

	olive_drab: EV_COLOR is
		once
			create Result.make_with_rgb (0.45, 0.44, 0.39)
		end

	orange: EV_COLOR is
		once
			create Result.make_with_rgb (1, 0.2706, 0)
		end

	orange_red: EV_COLOR is
		once
			create Result.make_with_rgb (1, 0.5, 0)
		end

	pale_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.7098, 1, 1)
		end

	pale_green: EV_COLOR is
		once
			create Result.make_with_rgb (0.7, 1, 0.7)
		end

	pale_yellow: EV_COLOR is
		once
			create Result.make_with_rgb (1, 1, 0.7)
		end

	peach_puff: EV_COLOR is
		once
			create Result.make_with_rgb (1, 0.8549, 0.7255)
		end

	powder_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.6902, .87843, .9019)
		end

	purple: EV_COLOR is
		once
			create Result.make_with_rgb (0.5, 0, .05)
		end

	red_brown: EV_COLOR is
		once
			create Result.make_with_rgb (0.6, 0.408, 0.29)
		end

	royal_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.255, .41176, .8824)
		end

	sky_blue: EV_COLOR is
		once
			create Result.make_with_rgb (.5294, .8078, .9216)
		end

	teal: EV_COLOR is
		once
			create Result.make_with_rgb (0, 0.5, 0.5)
		end

	violet: EV_COLOR is
		once
			create Result.make_with_rgb (.9333, .5098, .9333)
		end

	wheat: EV_COLOR is
		once
			create Result.make_with_rgb (.9608, .8706, .702)
		end

	windows_gray: EV_COLOR is
		do
			Result := default_background_color
		end

	--|--------------------------------------------------------------

	gray_25: EV_COLOR is
		do
			Result := gray_by_percent_white (25)
		end

	gray_40: EV_COLOR is
		do
			Result := gray_by_percent_white (40)
		end

	gray_50: EV_COLOR is
		do
			Result := gray_by_percent_white (50)
		end

	gray_60: EV_COLOR is
		do
			Result := gray_by_percent_white (60)
		end

	gray_75: EV_COLOR is
		do
			Result := gray_by_percent_white (75)
		end

	gray_80: EV_COLOR is
		do
			Result := gray_by_percent_white (80)
		end

	--|--------------------------------------------------------------

	gray_by_percent_black (v: INTEGER): EV_COLOR is
			-- A gray that is from 0 to 100 percent black
		require
			value_in_range: v >= 0 and v <= 100
		local
			b: REAL
		do
			b := 1 - (v.to_real / 100.0)
			create Result.make_with_rgb (b, b, b)
		end

	--|--------------------------------------------------------------

	gray_by_percent_white (v: INTEGER): EV_COLOR is
			-- A gray that is from 0 to 100 percent white
		require
			value_in_range: v >= 0 and v <= 100
		local
			b: REAL
		do
			b := (v.to_real / 100.0)
			create Result.make_with_rgb (b, b, b)
		end

	--|--------------------------------------------------------------

	colors_by_name: HASH_TABLE [EV_COLOR, STRING] is
			-- Collection of defined color constants, indexed by their common
			-- names
		once
			create Result.make (47)
			Result.extend (black, "black")
			Result.extend (blue, "blue")
			Result.extend (brown, "brown")
			Result.extend (cyan, "cyan")
			Result.extend (dark_blue, "dark_blue")
			Result.extend (dark_brown, "dark_brown")
			Result.extend (dark_cyan, "dark_cyan")
			Result.extend (dark_gray, "dark_gray")
			Result.extend (dark_gray, "dark_grey")
			Result.extend (dark_green, "dark_green")
			Result.extend (dark_magenta, "dark_magenta")
			Result.extend (dark_navy, "dark_navy")
			Result.extend (dark_red, "dark_red")
			Result.extend (dark_yellow, "dark_yellow")
			Result.extend (extra_pale_blue, "extra_pale_blue")
			Result.extend (extra_pale_green, "extra_pale_green")
			Result.extend (forest_green, "forest_green")
			Result.extend (gold, "gold")
			Result.extend (gray, "gray")
			Result.extend (gray, "grey")
			Result.extend (gray_25, "gray_25")
			Result.extend (gray_25, "grey_25")
			Result.extend (gray_40, "gray_40")
			Result.extend (gray_40, "grey_40")
			Result.extend (gray_50, "gray_50")
			Result.extend (gray_50, "grey_50")
			Result.extend (gray_60, "gray_60")
			Result.extend (gray_60, "grey_60")
			Result.extend (gray_75, "gray_75")
			Result.extend (gray_75, "grey_75")
			Result.extend (gray_80, "gray_80")
			Result.extend (gray_80, "grey_80")
			Result.extend (green, "green")
			Result.extend (honeydew, "honeydew")
			Result.extend (light_blue, "light_blue")
			Result.extend (light_brown, "light_brown")
			Result.extend (light_cyan, "light_cyan")
			Result.extend (light_goldenrod, "light_goldenrod")
			Result.extend (light_gray, "light_gray")
			Result.extend (light_green, "light_green")
			Result.extend (light_pink, "light_link")
			Result.extend (light_sky_blue, "light_sky_blue")
			Result.extend (light_yellow, "light_yellow")
			Result.extend (linen, "linen")
			Result.extend (magenta, "magenta")
			Result.extend (mint_cream, "mint_cream")
			Result.extend (navy, "navy")
			Result.extend (olive_drab, "olive_drab")
			Result.extend (orange, "orange")
			Result.extend (orange_red, "orange_red")
			Result.extend (pale_blue, "pale_blue")
			Result.extend (pale_green, "pale_green")
			Result.extend (pale_yellow, "pale_yellow")
			Result.extend (peach_puff, "peach_puff")
			Result.extend (powder_blue, "powder_blue")
			Result.extend (purple, "purple")
			Result.extend (red, "red")
			Result.extend (red_brown, "red_brown")
			Result.extend (royal_blue, "royal_blue")
			Result.extend (sky_blue, "sky_blue")
			Result.extend (teal, "teal")
			Result.extend (violet, "violet")
			Result.extend (wheat, "wheat")
			Result.extend (white, "white")
			Result.extend (windows_gray, "windows_gray")
			Result.extend (yellow, "yellow")
		end

end -- class AEL_V_COLORS
