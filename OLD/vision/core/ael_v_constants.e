note
	description: "Constants common to AEL_V classes"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
This class is inherited by AEL_V_WIDGET and is therefore available to
all descendents of that class.
This class can also be instantiated by itself if desired.
}"

class AEL_V_CONSTANTS

--|========================================================================
feature {NONE} -- Colors
--|========================================================================

	colors: AEL_V_COLORS is
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Pixmaps
--|========================================================================

	pixmaps: AEL_V_PIXMAPS is
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Fonts
--|========================================================================

	fonts: AEL_V_FONTS is
		once
			create Result
		end

	font_const: EV_FONT_CONSTANTS is
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	key_const: EV_KEY_CONSTANTS is
		once
			create Result
		end

end -- class AEL_V_CONSTANTS
