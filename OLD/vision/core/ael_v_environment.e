note
	description: "Encapsulation of the application execution environment."
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class should be instantiated (as a once) in the APPLICATION class
    prior to creation of any classes requiring its values.
    This class can be instantiated by itself if desired.
}"

class AEL_V_ENVIRONMENT

create
	default_create

--|========================================================================
feature -- Default pixmap directory path
--|========================================================================

	Ks_dflt_pixmap_dirname : DIRECTORY_NAME is
			-- Shared name of directory from which to read pixmap files
		once
			if private_pixmaps_dirname = Void then
				create Result.make_from_string (".")
			else
				create Result.make_from_string (private_pixmaps_dirname)
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {EV_APPLICATION,APPLICATION} -- Shared Initialization
--|========================================================================

	set_default_pixmap_path (v : STRING) is
			-- Set the default directory path in which to find pixmap files
			-- This must be called only from APPLICATION and only Once
			--
			-- It must be called before any windowing components are created!
		do
			private_pixmaps_dirname := v
			if Ks_dflt_pixmap_dirname /= Void then end
		end

--|========================================================================
feature {NONE} -- Private attributes
--|========================================================================

	private_pixmaps_dirname : detachable STRING

end -- class AEL_V_ENVIRONMENT
