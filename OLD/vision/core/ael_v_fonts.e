note
	description: "Common font constants and support routines"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is included in the AEL_V_CONSTANTS class as a supplier.
    It is therefore available to classes that inherit AEL_V_CONTANTS,
    included AEL_V_WIDGET.
    This class can be instantiated by itself if desired.
}"

class AEL_V_FONTS

--|========================================================================
feature -- Custom fonts
--|========================================================================

	arial_font (ps: INTEGER; bfl, ifl: BOOLEAN): EV_FONT is
		local
			wt: INTEGER
			it: INTEGER
		do
			if bfl then
				wt := Kfw_bold
			else
				wt := Kfw_regular
			end
			if ifl then
				it := Kfs_italic
			else
				it := Kfs_regular
			end
			create Result.make_with_values (Kff_sans, wt, it, ps)
		end

	--|--------------------------------------------------------------

	times_new_roman_font (ps: INTEGER; bfl, ifl: BOOLEAN): EV_FONT is
		local
			wt: INTEGER
			it: INTEGER
		do
			if bfl then
				wt := Kfw_bold
			else
				wt := Kfw_regular
			end
			if ifl then
				it := Kfs_italic
			else
				it := Kfs_regular
			end
			create Result.make_with_values (Kff_roman, wt, it, ps)
		end

	--|--------------------------------------------------------------

	courier_font (ps: INTEGER; bfl, ifl: BOOLEAN): EV_FONT is
		local
			wt: INTEGER
			it: INTEGER
		do
			if bfl then
				wt := Kfw_bold
			else
				wt := Kfw_regular
			end
			if ifl then
				it := Kfs_italic
			else
				it := Kfs_regular
			end
			create Result.make_with_values (Kff_fixed, wt, it, ps)
		end

--|========================================================================
feature {NONE} -- Font constants
--|========================================================================

	font_constants: EV_FONT_CONSTANTS is
			-- Family_screen, Family_roman, Family_sans,
			-- Family_typewriter, Family_modern
			-- Weight_thin, Weight_regular, Weight_bold, Weight_black
			-- Shape_regular, Shape_italic
		once
			create Result
		end

	Kff_screen: INTEGER is once Result := font_constants.Family_screen end
	Kff_roman: INTEGER is once Result := font_constants.Family_roman end
	Kff_sans: INTEGER is once Result := font_constants.Family_sans end
	Kff_fixed: INTEGER is once Result := font_constants.Family_typewriter end
	Kff_modern: INTEGER is once Result := font_constants.Family_sans end

	Kfw_thin: INTEGER is once Result := font_constants.Weight_thin end
	Kfw_regular: INTEGER is once Result := font_constants.Weight_regular end
	Kfw_bold: INTEGER is once Result := font_constants.Weight_bold end
	Kfw_black: INTEGER is once Result := font_constants.Weight_black end

	Kfs_regular: INTEGER is once Result := font_constants.Shape_regular end
	Kfs_italic: INTEGER is once Result := font_constants.Shape_italic end

	Ks_ff_screen: STRING is "Screen"
	Ks_ff_roman: STRING is "Roman"
	Ks_ff_sans: STRING is "Sans Serif"
	Ks_ff_fixed: STRING is "Fixed"
	Ks_ff_modern: STRING is "Modern"

	Ks_fw_thin: STRING is "Narrow"
	Ks_fw_regular: STRING is "Normal"
	Ks_fw_bold: STRING is "Bold"
	Ks_fw_black: STRING is "Black"

	Ks_fs_regular: STRING is "Regular"
	Ks_fs_italic: STRING is "Italic"

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_family (v: INTEGER): BOOLEAN is
			-- v >= Kff_screen and v <= Kff_modern
		do
			Result := font_constants.valid_family (v)
		end

	is_valid_weight (v: INTEGER): BOOLEAN is
			-- v >= Kfw_thin and v <= Kfw_black
		do
			Result := font_constants.valid_weight (v)
		end

	is_valid_shape (v: INTEGER): BOOLEAN is
			-- v >= Kfs_regular and v <= Kfs_italic
		do
			Result := font_constants.valid_shape (v)
		end

--|========================================================================
feature -- External representation
--|========================================================================

	family_names: ARRAY [STRING] is
		do
			Result := << Ks_ff_screen, Ks_ff_roman, Ks_ff_sans,
		   Ks_ff_fixed, Ks_ff_modern >>
		end

	family_name (v: INTEGER): STRING is
		require
			valid_index: is_valid_family(v)
		do
			Result := family_names.item (v)
		end

	--|--------------------------------------------------------------

	weights: ARRAY [STRING] is
		do
			Result := << Ks_fw_thin, Ks_fw_regular, Ks_fw_bold, Ks_fw_black >>
		end

	weight_out (v: INTEGER): STRING is
		require
			valid_index: is_valid_weight(v)
		do
			Result := weights.item ((v - Kfw_thin) + 1)
		end

	--|--------------------------------------------------------------

	shapes: ARRAY [STRING] is
		do
			Result := << Ks_fs_regular, Ks_fs_italic >>
		end

	shape_out (v: INTEGER): STRING is
		require
			valid_index: is_valid_shape(v)
		do
			Result := shapes.item ((v - Kfs_regular) + 1)
		end

	--|--------------------------------------------------------------

	font_out (f: EV_FONT): STRING is
		local
			ns: STRING
			pf: AEL_PRINTF
		do
			if f.name.is_empty then
				ns := "(Unnamed)"
			else
				ns := f.name
			end
			create pf
			Result := pf.aprintf ("%%s %%s %%s %%d pts",
			<< ns,
			family_name(f.family),
			weight_out(f.weight),
			shape_out(f.shape),
			f.height_in_points
			>>)
		end

end -- class AEL_V_FONTS
