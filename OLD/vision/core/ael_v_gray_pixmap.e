note
	description: "Pixmaps generated programmatically, without the use of files"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    To use this class, create a child class.  Implement the pm_width
    and pm_height features as constants representing the widht and
    height of the pixmap.
    Implement the draw routine by calling the various drawing procedures
    in EV_DRAWABLE (e.g. draw_segment) to create the image you want.
    Be sure to call the make routine when creating the child.
    Gray pixmaps are used for mouse-over rendition changes.
    Unfortunately the vision2 library code is a bit fussy about pixmaps
    and won't support subclasses of EV_PIXMAP.  So, the pixmap has to be
    an attribute here.
}"

deferred class AEL_V_GRAY_PIXMAP

inherit
	AEL_V_CONSTANTS

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make is
			-- Create and initialized this class and its associated pixmap
		do
			create pixmap.make_with_size (pm_width, pm_height)
			draw
		end

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
			-- Draw the actual image using the drawing routines from
			-- EV_DRAWABLE on the pixmap attribute.
		deferred
		end

--|========================================================================
feature -- Attributes
--|========================================================================

	pixmap : EV_PIXMAP
			-- The actual pixmap being drawn

	pm_width : INTEGER is
			-- The defined width of the pixmap
		deferred
		end

	pm_height : INTEGER is
			-- The defined height of the pixmap
		deferred
		end

--|--------------------------------------------------------------
invariant
	pixmap_exists: pixmap /= Void
	pixmap_sized: pixmap.height = pm_height and pixmap.width = pm_width

end -- class AEL_V_GRAY_PIXMAP
