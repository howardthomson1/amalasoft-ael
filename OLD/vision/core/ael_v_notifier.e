note
	description: "A class representing a general purpose notifier"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated from within an instance of AEL_V_NOTIFY_CLIENT
    as a once.  There is no need to create another instance of this class.
}"

class AEL_V_NOTIFIER

inherit
	EXCEPTIONS

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make is
		do
			create clients.make
			create event_buffer.make
		end

--|========================================================================
feature -- Registration
--|========================================================================

	register (v: AEL_V_NOTIFY_CLIENT) is
			-- Remember the given client for subsequent notification
		require
			exists: v /= Void
			is_new: not clients.has (v)
		local
			oc: CURSOR
		do
			if updating then
				if register_attempts > K_max_retries then
					-- Raise an exception
					r_timeout.set_interval (0)
					raise ("Failed attempts to register for notification")
				else
					-- Try again real soon
					create r_timeout.make_with_interval (50)
					r_timeout.actions.extend (agent register(v))
					register_attempts := register_attempts + 1
				end
			else
				updating := True
				if r_timeout /= Void then
					r_timeout.set_interval (0)
				end
				register_attempts := 0
				oc := clients.cursor
				clients.extend (v)
				clients.go_to (oc)
				updating := False
			end
		ensure
			registered: clients.has (v)
		end

	--|--------------------------------------------------------------

	unregister (v: AEL_V_NOTIFY_CLIENT) is
			-- Forget the given client
		require
			exists: v /= Void
			is_registered: clients.has (v)
		local
			oc: CURSOR
		do
			if updating then
				if unregister_attempts > K_max_retries then
					-- Raise an exception
					r_timeout.set_interval (0)
					raise ("Failed attempts to Unregister for notification")
				else
					-- Try again real soon
					create ur_timeout.make_with_interval (50)
					ur_timeout.actions.extend (agent unregister(v))
					unregister_attempts := unregister_attempts + 1
				end
			else
				updating := True
				if r_timeout /= Void then
					r_timeout.set_interval (0)
				end
				unregister_attempts := 0
				oc := clients.cursor
				clients.start
				clients.search (v)
				clients.remove
				if clients.valid_cursor (oc) then
					clients.go_to (oc)
				else
					clients.start
				end
				updating := False
			end
		ensure
			unregistered: not clients.has (v)
		end

--|========================================================================
feature -- Notification
--|========================================================================

	post_events_for_notification (ev_ids: ARRAY [INTEGER]) is
			-- Notify all clients that the given events have occurred
		require
			exists: ev_ids /= Void
		do
			if not updating then
				if buffering_enabled then
					add_events_to_buffer (ev_ids)
				else
					send_events_to_clients (ev_ids)
				end
			end
    end

	 --|--------------------------------------------------------------

	 enable_buffering is
		do
			buffering_enabled := True
		end

	disable_buffering is
		do
			buffering_enabled := False
		end

	buffering_enabled: BOOLEAN

	--|--------------------------------------------------------------

	flush_buffered_events is
		local
			buf: like buffered_events
		do
			buf := buffered_events
			if not buf.is_empty then
				send_events_to_clients (buf)
			end
			event_buffer.wipe_out
		end

	--|--------------------------------------------------------------

	send_events_to_clients (ev_ids: like buffered_events) is
		require
			events_exist: ev_ids /= Void
		local
			cc: CURSOR
		do
			if not updating then
				updating := True
				if not ev_ids.is_empty then
					cc := clients.cursor
					from clients.start
					until clients.exhausted
					loop
						clients.item.catch_notification (ev_ids)
						clients.forth
					end
					clients.go_to (cc)
				end
				updating := False
			end
		end

--|========================================================================
feature -- Clients
--|========================================================================

	clients: LINKED_LIST [AEL_V_NOTIFY_CLIENT]

--|========================================================================
feature {NONE} -- Private state
--|========================================================================

	event_buffer: LINKED_LIST [INTEGER]

	buffered_events: ARRAY [INTEGER] is
		local
			i, lim: INTEGER
			oc: CURSOR
		do
			lim := event_buffer.count
			if lim = 0 then
				Result := << >>
			else
				create Result.make (1, lim)
				oc := event_buffer.cursor
				from
					i := 1
					event_buffer.start
				until i > lim
				loop
					Result.put (event_buffer.item, i)
					i := i + 1
					event_buffer.forth
				end
				event_buffer.go_to (oc)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	add_events_to_buffer (ev_ids: ARRAY [INTEGER]) is
		require
			exists: ev_ids /= Void
		local
			i, lim: INTEGER
		do
			if not ev_ids.is_empty then
				if event_buffer.is_empty then
					event_buffer.fill (ev_ids)
				else
					lim := ev_ids.count
					from i := 1
					until i > lim
					loop
						if not event_buffer.has (ev_ids.item (i)) then
							event_buffer.extend (ev_ids.item (i))
						end
						i := i + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	r_timeout: EV_TIMEOUT
	ur_timeout: EV_TIMEOUT

	updating: BOOLEAN

	register_attempts: INTEGER
	unregister_attempts: INTEGER

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_max_retries: INTEGER is 3

invariant
	clients_exist: clients /= Void

end -- class AEL_V_NOTIFIER
