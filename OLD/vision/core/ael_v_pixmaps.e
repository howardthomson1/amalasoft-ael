note
	description: "Common pixmaps and facilities"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is a built-in supplier to AEL_V_WIDGET and so is available
    to all descendents of that class.  This class can also be instantiated
    directly or inherited as desired.
    For the new_pixmap function to be of any value, an AEL_V_ENVIRONMENT
    or descendent must be instantiated prior to any calls to new_pixmap.
    The default pixmap path in AEL_V_ENVIRONMENT must be set first (it is
    a once), else the pixmap path will be hard-wired to the current directory
    unless an absolute path is provided.
}"

class AEL_V_PIXMAPS

inherit
	AEL_V_ENVIRONMENT
	AEL_V_CONSTANTS
	MATH_CONST

create
	default_create

--|========================================================================
feature -- Common pixmaps (generated programmatically)
--|========================================================================

	K_pm_application_icon: like new_pixmap is
		do
			Result := create {AEL_V_APPLICATION_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_save: like new_pixmap is
		do
			Result := create {AEL_V_SAVE_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_open: like new_pixmap is
		do
			Result := create {AEL_V_OPEN_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_new: like new_pixmap is
		do
			Result := create {AEL_V_NEW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_print: like new_pixmap is
		do
			Result := create {AEL_V_PRINT_PIXMAP}.make
		end

	--|---------------------------------------------------------------

	K_pm_search: like new_pixmap is
		do
			Result := create {AEL_V_SEARCH_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_help: like new_pixmap is
		do
			Result := create {AEL_V_HELP_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_small_arrow_left: like new_pixmap is
		do
			Result := create {AEL_V_SMALL_LEFT_ARROW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_small_arrow_right: like new_pixmap is
		do
			Result := create {AEL_V_SMALL_RIGHT_ARROW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_thumb_15x8: like new_pixmap is
		do
			Result := create {AEL_V_THUMB_15X8_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_locked_18x18: like new_pixmap is
		do
			Result := create {AEL_V_LOCKED_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_unlocked_18x18: like new_pixmap is
		do
			Result := create {AEL_V_UNLOCKED_PIXMAP}.make
		end

--|========================================================================
feature -- Pixmap path construction (for file-based pixmaps)
--|========================================================================

	pixmap_path (pmn: STRING): FILE_NAME is
		require
			name_exists: pmn /= Void
		do
			create Result.make_from_string (Ks_dflt_pixmap_dirname)
			Result.extend (pmn)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_pixmap (fn: STRING): EV_PIXMAP is
			-- Create a new pixmap from the given pathname
		require
			filename_exists: fn /= Void and then not fn.is_empty
		local
			f: RAW_FILE
			path: like pixmap_path
			ex: EXECUTION_ENVIRONMENT
		do
			create ex
			last_pixmap_create_successful := False
			if fn.substring_index(ex.root_directory_name, 1) /= 0 then
				-- Is an absolute path already
				create path.make_from_string (fn)
			else
				-- Is a file name only
				path := pixmap_path (fn)
			end
			create f.make (path)
			if f.exists then
				create Result
				Result.set_with_named_file (path)
				last_pixmap_create_successful := True
			else
				-- File not found, create a default pixmap
				create Result.make_with_size (16, 16)
				report_pixmap_file_error ("Pixmap file not found", << Result >>)
			end
		ensure
			result_exists: Result /= Void
		end

--|========================================================================
feature -- Pixmap error reporting
--|========================================================================

	report_pixmap_file_error (msg: STRING; args: ARRAY [ANY]) is
		require
			message_exists: msg /= Void
			args_exist: args /= Void
		do
			if error_report_proc /= Void then
				error_report_proc.call ([msg, args])
			end
		end

	--|--------------------------------------------------------------

	last_pixmap_create_successful: BOOLEAN

--|========================================================================
feature -- Error reporting callbacks
--|========================================================================

	set_error_report_procedure (v: like error_report_proc) is
		do
			error_report_proc := v
		end

	error_report_proc: PROCEDURE [ANY, TUPLE [STRING, ARRAY [ANY]]]

end -- class AEL_V_PIXMAPS
