note
	description: "Constants used to describe relative position"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
This class is typically inherited by other classes needing access to
the features of this class, but this class can be instantiated itself
if desired.
This class contains only constants and queries.
}"

class AEL_V_POSITION_CONSTANTS

create
	default_create

--|========================================================================
feature -- Constants
--|========================================================================

	Kc_pos_left: CHARACTER is 'l'
	Kc_pos_center: CHARACTER is 'c'
	Kc_pos_right: CHARACTER is 'r'

	Kc_pos_top: CHARACTER is 't'
	Kc_pos_middle: CHARACTER is 'm'
	Kc_pos_bottom: CHARACTER is 'b'

	--|--------------------------------------------------------------

	Ks_pos_top_left: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_top)
			Result.extend (Kc_pos_left)
		end

	Ks_pos_top_center: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_top)
			Result.extend (Kc_pos_center)
		end

	Ks_pos_top_right: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_top)
			Result.extend (Kc_pos_right)
		end

	--|--------------------------------------------------------------

	Ks_pos_middle_left: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_middle)
			Result.extend (Kc_pos_left)
		end

	Ks_pos_middle_center: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_middle)
			Result.extend (Kc_pos_center)
		end

	Ks_pos_middle_right: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_middle)
			Result.extend (Kc_pos_right)
		end

	--|--------------------------------------------------------------

	Ks_pos_bottom_left: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_bottom)
			Result.extend (Kc_pos_left)
		end

	Ks_pos_bottom_center: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_bottom)
			Result.extend (Kc_pos_center)
		end

	Ks_pos_bottom_right: STRING is
		once
			create Result.make (2)
			Result.extend (Kc_pos_bottom)
			Result.extend (Kc_pos_right)
		end

--|========================================================================
feature -- Queries
--|========================================================================

	is_valid_position_tag (v: STRING): BOOLEAN is
			-- Is the given string a valid position tag?
		do
			if v /= Void and then v.count = 2 then
				inspect v.item (1)
				when Kc_pos_top, Kc_pos_middle, Kc_pos_bottom then
					inspect v.item (2)
					when Kc_pos_left, Kc_pos_center, Kc_pos_right then
						Result := True
					else
					end
				else
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_position_tag_index (v: INTEGER): BOOLEAN is
			-- Is the given value in the range of valid indice for position tags?
		do
			Result := v > 0 and v <= 9
		end

	--|--------------------------------------------------------------

	position_vertical_component (pos: STRING): CHARACTER
			-- The vertical position component of the given position tag
		require
			valid_position: is_valid_position_tag (pos)
		do
			Result := pos.item (1)
		end

	position_horizontal_component (pos: STRING): CHARACTER
			-- The horizontal position component of the given position tag
		require
			valid_position: is_valid_position_tag (pos)
		do
			Result := pos.item (1)
		end

	--|--------------------------------------------------------------

	indexed_position_tags: ARRAY [ STRING ] is
			-- Postition tag constants arranged in sequence as if traversing
			-- the position matrix left to right, then top top to bottom
		once
			Result := << Ks_pos_top_left,
		   Ks_pos_top_center,
		   Ks_pos_top_right,
		   Ks_pos_middle_left,
		   Ks_pos_middle_center,
		   Ks_pos_middle_right,
		   Ks_pos_bottom_left,
		   Ks_pos_bottom_center,
		   Ks_pos_bottom_right
		   >>
		end

	--|--------------------------------------------------------------

	is_left_position (v: STRING): BOOLEAN is
			-- Does the given position string include a left placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_left)
		end

	is_center_position (v: STRING): BOOLEAN is
			-- Does the given position string include a center placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_center)
		end

	is_right_position (v: STRING): BOOLEAN is
			-- Does the given position string include a right placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_right)
		end

	--|--------------------------------------------------------------

	is_top_position (v: STRING): BOOLEAN is
			-- Does the given position string include a top placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_top)
		end

	is_middle_position (v: STRING): BOOLEAN is
			-- Does the given position string include a middle placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_middle)
		end

	is_bottom_position (v: STRING): BOOLEAN is
			-- Does the given position string include a bottom placement?
		require
			exists: v /= Void
		do
			Result := v.has (Kc_pos_bottom)
		end

end -- class AEL_V_POSITION_CONSTANTS
