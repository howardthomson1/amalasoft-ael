note
	description: "Common ancestor to all AEL_V custom widgets"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is the ancestor of custom widgets.  It provides a standard
    construction and intitialization sequence and makes available to its
    descendents the common classes for pixmaps, colors and fonts.
    Children of this class should be created using the already defined
    make procedure.  Complex widgets (with subcomponents) add their
    subordinate widgets in a redefined make_components routine.
    To enforce the standard construction sequence, there are pre and
    postconditions defined for the redefinable routines.  Be sure to 
    include Precursor calls in your redefined routines.
}"

deferred class AEL_V_WIDGET

inherit
	AEL_V_CONSTANTS
		undefine
			default_create, copy
		end

--|========================================================================
feature {NONE} -- Creation and Initialization
--|========================================================================

	make is
			-- Standard creation procedure.  Can be redefined in child but
			-- must call Precursor to ensure valid construction sequence.
		do
			default_create
			prepare
		end

	--|--------------------------------------------------------------

	prepare is
			-- Build and intialize all subordinate components.
		require
			not_prepared: not is_prepared
		do
			make_components
			set_initial_values
			set_actions
			is_prepared := True
			finish_preparation
		ensure
			prepared: is_prepared
		end

	--|--------------------------------------------------------------

	finish_preparation is
			-- To be called only after all other preparation steps are
			-- complete.
			-- Perform and preparation actions that can not occur before
			-- standard preparation is complete (e.g. calls to
			-- update_widget_rendition)
		require
			is_prepared: is_prepared
		do
		end

--|========================================================================
feature {NONE} -- Component construction
--|========================================================================

	make_components is
			-- Create the subordinate interface components and add them to this
			-- widget
			-- Redefine in child as needed, but be sure to include Precursor call.
		require
			no_components: not components_are_made
		do
			components_are_made := True
		ensure
			components_exist: components_are_made
		end

--|========================================================================
feature {NONE} -- Initial value setting
--|========================================================================

	set_initial_values is
			-- Set initial values for the components of this widget
			-- Redefine in child as needed, but be sure to include Precursor call.
		require
			components_exist: components_are_made
			not_initialized: not initial_values_are_set
		do
			initial_values_are_set := True
		ensure
			initialized: initial_values_are_set
		end

--|========================================================================
feature {NONE} -- Action setting
--|========================================================================

	set_actions is
			-- Define actions for widgets and events
			-- Redefine in child as needed, but be sure to include Precursor call.
		require
			components_exist: components_are_made
			initialized: initial_values_are_set
			no_actions: not actions_are_set
		do
			actions_are_set := True
		ensure
			actions_set: actions_are_set
		end

--|========================================================================
feature {NONE} -- Widget state setting
--|========================================================================

	update_widget_rendition is
			-- Update the states of widgets, like sensitivity, based on
			-- the current state of the application
			-- Redefine in child as needed, but be sure to include Precursor
			-- call.
			-- Note well that this cannot be called directly or indirectly
			-- from initialization routines other than finish_preparation
			-- due to the precondition.
		require
			prepared: is_prepared
		do
		end

--|========================================================================
feature -- Initialization Status
--|========================================================================

	components_are_made: BOOLEAN
			-- Have the interface components been create yet?

	is_prepared: BOOLEAN
			-- Has the widget been prepared yet?

	actions_are_set: BOOLEAN
			-- Have actions been defined?

	initial_values_are_set: BOOLEAN
			-- Have the initial values been defined?

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	enclosing_window: EV_WINDOW is
			-- Next Window widget in the widget hierarchy from Current
		local
			w: EV_WIDGET
		do
			w ?= Current
			from Result ?= w
			until Result /= Void
			loop
				w := w.parent
				Result ?= w
			end
		end

--|========================================================================
feature {NONE} -- Application root class
--|========================================================================

	application: detachable EV_APPLICATION
			-- The root class of this graphical application
			-- Often the APPLICATION class also holds, or has the path to the
			-- core date required for the application.
		once
			Result := (create {EV_ENVIRONMENT}).application
		end

end -- class AEL_V_WIDGET
