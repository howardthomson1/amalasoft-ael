note
	description: "EV_MODEL_TEXT with built-in size calculations"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated in the same way as EV_MODEL_TEXT and is
    plug-compatible with that class.
    The overall_* functions move the size calcuations and adjustments
    here instead of in the client.
}"

class AEL_V_MODEL_TEXT

inherit
	EV_MODEL_TEXT
		redefine
			assign_draw_id
		end

create
	default_create, make_with_text

--|========================================================================
feature -- Sizing
--|========================================================================

	overall_width: INTEGER is
			-- Current overall width of this figure; driven by font and text
			-- On some fonts, characters may extend outside of the bounds
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
		do
			if text /= Void and then not text.is_empty then
				ss := scaled_font.string_size (text)
				Result := ss.integer_item (1) - ss.integer_item (3) +
					ss.integer_item (4)
			end
		end

	--|------------------------------------------------------------------------

	overall_height: INTEGER is
			-- Current overall height of this figure driven by font and text
			-- On some fonts, characters may extend outside of the bounds
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
		do
			if text /= Void and then not text.is_empty then
				ss := scaled_font.string_size (text)
				Result := ss.integer_item (2)
			end
		end

	--|------------------------------------------------------------------------

	overall_size: ARRAY [INTEGER] is
			-- Current overall width and height of this figure.
			-- On some fonts, characters may extend outside of the bounds
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
			tw: INTEGER
		do
			if text /= Void and then not text.is_empty then
				ss := scaled_font.string_size (text)
				tw := ss.integer_item (1) - ss.integer_item (3) +
					ss.integer_item (4)
			end
			Result := << tw, ss.integer_item (2) >>
		ensure
			exists: Result /= Void
			has_two_items: Result.count = 2
		end

	--|--------------------------------------------------------------

	assign_draw_id is
			-- Set the drawing ID for this figure to be the same as for
			-- normal text figures
		do
			known_draw_ids.search ("EV_MODEL_TEXT")
			draw_id := known_draw_ids.found_item
			if draw_id = 0 then
				draw_id := draw_id_counter.item
				draw_id_counter.set_item (draw_id + 1)
				known_draw_ids.put (draw_id, "EV_MODEL_TEXT")
			end
		end

end -- class AEL_V_MODEL_TEXT
