class AEL_V_BAR_AND_LINE_GRAPH
-- A vertical bar graph with option overlayed lines

inherit
	AEL_V_VERTICAL_BAR_GRAPH
		undefine
			maximum_series_count, init_data_source, has_connectors
		redefine
			new_item_at_position, new_figure_at_position
		end
	AEL_V_LINE_GRAPH
		undefine
			on_button_3_ctl_press, update_geometry,
			top_margin, right_margin
		redefine
			new_item_at_position, new_figure_at_position
		end

create
	make

--|========================================================================
feature -- Rendering
--|========================================================================

	new_item_at_position (sl: REAL; si, i: INTEGER): AEL_V_GRAPH_ITEM is
		do
			if si = 1 then
				Result := Precursor {AEL_V_VERTICAL_BAR_GRAPH} (sl, si, i)
			else
				Result := Precursor {AEL_V_LINE_GRAPH} (sl, si, i)
			end
		end

	new_figure_at_position (si, i: INTEGER; pt: EV_COORDINATE): EV_MODEL is
		do
			if si = 1 then
				Result := Precursor {AEL_V_VERTICAL_BAR_GRAPH} (si, i, pt)
			else
				Result := Precursor {AEL_V_LINE_GRAPH} (si, i, pt)
			end
		end

end -- class AEL_V_BAR_AND_LINEGRAPH
