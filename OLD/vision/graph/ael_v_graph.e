deferred class AEL_V_GRAPH

inherit
	EV_VERTICAL_BOX

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make is
		do
			default_create
			core_make
		end

	--|--------------------------------------------------------------

	core_make is
		do
			init_data_source
			create chart
			extend (chart)
			create figure_world
			create projector.make_with_buffer (figure_world, chart)
			build_item_lists

			resize_actions.extend (agent on_resize)
			figure_world.pointer_button_release_actions.extend (
				agent on_mouse_btn_release)

			set_minimum_size (K_min_width, K_min_height)
		end

	--|--------------------------------------------------------------

	init_data_source is
		deferred
		ensure
			has_data: data_source /= Void
		end

	--|--------------------------------------------------------------

	build_item_lists is
		local
			i, lim: INTEGER
		do
			lim := data_source.count
			create items.make (1, lim)
			from i := 1
			until i > lim
			loop
				items.put (create {LINKED_LIST [AEL_V_GRAPH_ITEM]}.make, i)
				i := i + 1
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	chart: EV_DRAWING_AREA

--	projector:  EV_MODEL_DRAWING_AREA_PROJECTOR
-- Eliminates flicker
	projector:  EV_MODEL_BUFFER_PROJECTOR
	figure_world: EV_MODEL_WORLD

	items: ARRAY [LINKED_LIST [AEL_V_GRAPH_ITEM]]

--|========================================================================
feature -- Data
--|========================================================================

	series (v: INTEGER): ARRAY [REAL] is
		require
			valid_series: v > 0 and v <= data_source.count
		do
			Result := data_source.i_th (v)
		end

	data_source: LINKED_LIST [like series]

	--|--------------------------------------------------------------

	series_labels (v: INTEGER): ARRAY [STRING] is
		require
			has_labels: data_labels /= Void
			valid_series: v >= data_labels.lower and v >= data_labels.upper
		do
			Result := data_labels.item (v)
		end

	data_labels: ARRAY [like series_labels]

	--|--------------------------------------------------------------

	data_item_count: INTEGER

	smallest_value: REAL
	largest_value: REAL

	maximum_series_count: INTEGER is
		do
			-- Number of colors in palette
			Result := item_colors.count
		end

--|========================================================================
feature -- Status
--|========================================================================

	left_margin: INTEGER is
		do
			Result := private_left_margin
			if Result = 0 then
				Result := K_dflt_left_margin
			end
		end

	right_margin: INTEGER is
		do
			Result := private_right_margin
			if Result = 0 then
				Result := K_dflt_right_margin
			end
		end

	top_margin: INTEGER is
		do
			Result := private_top_margin
			if Result = 0 then
				Result := K_dflt_top_margin
			end
		end

	bottom_margin: INTEGER is
		do
			Result := private_bottom_margin
			if Result = 0 then
				Result := K_dflt_bottom_margin
			end
		end

	--|--------------------------------------------------------------

	item_colors: like default_colors is
		do
			Result := private_item_colors
			if Result = Void then
				Result := default_colors
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_data_source (v: like data_source) is
		require
			exists: v /= Void
		do
			data_source.wipe_out
			data_source.fill (v)
		end

	--|--------------------------------------------------------------

	set_series_data (v: like series; si: INTEGER) is
			-- For the given data series 'si', set the data to 'v'
		require
			exists: v /= Void
			valid_series: si > 0 and si <= data_source.count
		do
			data_source.go_i_th (si)
			data_source.replace (v)
		ensure
			is_set: data_source.i_th (si) = v
		end

	--|--------------------------------------------------------------

	add_series (v: like series) is
			-- Add the given data series to data source
		require
			exists: v /= Void
		do
			if data_source = Void then
				create data_source.make
			end
			data_source.extend (v)
		ensure
			added: data_source.last = v
		end

	--|--------------------------------------------------------------

	set_item_color_db (v: like default_colors) is
		do
			private_item_colors := v
		end

	set_item_colors (si: INTEGER; ca: ARRAY [EV_COLOR]) is
			-- Set colors for the item at positions 'si'
		require
			colors_exist: ca /= Void
			valid_count: ca.count = 3
		do
			if private_item_colors = Void then
				private_item_colors := default_colors.deep_twin
			end
			private_item_colors.put (ca, si)
		end

	--|--------------------------------------------------------------

	set_left_margin (v: INTEGER) is
		do
			private_left_margin := v
		end

	set_right_margin (v: INTEGER) is
		do
			private_right_margin := v
		end

	set_top_margin (v: INTEGER) is
		do
			private_top_margin := v
		end

	set_bottom_margin (v: INTEGER) is
		do
			private_bottom_margin := v
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	update_graph is
		deferred
		end

--|========================================================================
feature {NONE} -- Rendering support
--|========================================================================

	purge is
		do
			figure_world.wipe_out
			build_item_lists

			smallest_value := {INTEGER_64}.max_value.to_real
			largest_value := 0.0
		end

	--|--------------------------------------------------------------

	factor_series_for_scale (v: like series) is
		local
			tc: INTEGER
		do
			tc := data_item_count
			data_item_count := 0
			v.do_all (agent factor_for_scale)
			data_item_count := tc.max (data_item_count)
		end

	--|--------------------------------------------------------------

	factor_for_scale (v: REAL) is
		do
			data_item_count := data_item_count + 1
			smallest_value := smallest_value.min (v)
			largest_value := largest_value.max (v)
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER) is
		do
			update_graph
		end

	--|--------------------------------------------------------------

	on_item_mouse_over (ti: AEL_V_GRAPH_ITEM) is
		do
		end

	--|--------------------------------------------------------------

	on_item_mouse_leave (ti: AEL_V_GRAPH_ITEM) is
		do
		end

	--|--------------------------------------------------------------

	on_item_mouse_btn_press (ti: AEL_V_GRAPH_ITEM; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
		end

	--|--------------------------------------------------------------

	on_mouse_btn_press (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
		end

	--|--------------------------------------------------------------

	on_mouse_btn_release (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
		end

	--|--------------------------------------------------------------

	on_2click (f: EV_MODEL; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
		end

--|========================================================================
feature {NONE} -- Geometry support
--|========================================================================

	update_geometry is
			-- Recalculate sizes, offsets and positions for current size
		deferred
		end

	--|--------------------------------------------------------------

	plot_area_height: INTEGER is
			-- Height of actual plot area (area less margins)
		do
			Result := (height - top_margin) - bottom_margin
		end

	plot_area_width: INTEGER is
			-- Width of actual plot area (area less margins)
		do
			Result := (width - left_margin) - right_margin
		end

	private_left_margin: INTEGER
	private_right_margin: INTEGER
	private_top_margin: INTEGER
	private_bottom_margin: INTEGER

	private_item_colors: like default_colors

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_min_height: INTEGER is 60
	K_min_width: INTEGER is 60

	--|--------------------------------------------------------------

	K_dflt_left_margin: INTEGER is 15

	K_dflt_right_margin: INTEGER is 10

	K_dflt_top_margin: INTEGER is 10

	K_dflt_bottom_margin: INTEGER is 15

	--|--------------------------------------------------------------

	colors: AEL_V_COLORS is
		once
			create Result
		end

	default_colors: ARRAY [ARRAY [EV_COLOR]] is
			-- Default color sets (for items, shadows, etc)
		once
			Result := <<
			<< colors.dark_red, colors.light_brown, colors.red_brown >>,
			<< colors.navy, colors.royal_blue, colors.dark_navy >>,
			<< colors.orange, colors.gold, colors.red_brown >>,
			<< colors.green, colors.light_green, colors.dark_green >>,
			<< colors.blue, colors.royal_blue, colors.navy >>,
			<< colors.cyan, colors.light_cyan, colors.dark_gray >>,
			<< colors.magenta, colors.violet, colors.dark_magenta >>,
			<< colors.light_green, colors.pale_green, colors.dark_green >>,
			<< colors.yellow, colors.pale_yellow, colors.light_goldenrod >>,
			<< colors.dark_green, colors.green, colors.dark_gray >>,
			<< colors.light_blue, colors.pale_blue, colors.navy >>,
			<< colors.gold, colors.dark_yellow, colors.brown >>,
			<< colors.dark_gray, colors.gray, colors.black >>,
			<< colors.light_gray, colors.light_gray, colors.gray >>
			>>
		end

	application: EV_APPLICATION is
		local
			ev: EV_ENVIRONMENT
		once
			create ev
			Result := ev.application
		end

	--|--------------------------------------------------------------

invariant
	data_source_exists: data_source /= Void
	valid_data_count: data_source.count <= maximum_series_count

end -- class AEL_V_GRAPH
