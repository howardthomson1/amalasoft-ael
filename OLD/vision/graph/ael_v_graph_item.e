class AEL_V_GRAPH_ITEM

create
	make

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make (f: like figure; v: like data; lb: STRING) is
		require
			figure_exists: f /= Void
		do
			figure := f
			data := v
			label := lb
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	figure: EV_MODEL
	data: REAL
	label: STRING

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_data (v: like data) is
		do
			data := v
		end

	set_label (v: STRING) is
		do
			label := v
		end

	--|--------------------------------------------------------------

invariant
	has_figure: figure /= Void

end -- class AEL_V_GRAPH_ITEM
