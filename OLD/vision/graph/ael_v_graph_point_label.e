class AEL_V_GRAPH_POINT_LABEL
-- Label at a point in a graph field

inherit
	EV_MODEL_GROUP
		redefine
			default_create, set_point_position
		end

create
	default_create, make_with_text

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create is
		do
			Precursor
			make_components
		end

	--|--------------------------------------------------------------

	make_with_text (v: STRING) is
		require
			exists: v /= Void
		do
			original_text := v
			default_create
		end

	--|--------------------------------------------------------------

	make_components is
		do
			create bg_fig
			extend (bg_fig)
			bg_fig.set_background_color (colors.pale_yellow)
			if original_text /= Void then
				create text_fig.make_with_text (original_text)
			else
				create text_fig
			end
			extend (text_fig)
			update_geometry
		end

--|========================================================================
feature -- Status
--|========================================================================

	original_text: STRING

	bg_fig: EV_MODEL_ROUNDED_RECTANGLE
	text_fig: AEL_V_MODEL_TEXT

	overall_width: INTEGER is
		do
			Result := bg_fig.width
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_point_position (ax, ay: INTEGER) is
		do
			Precursor (ax, ay)
			update_geometry
		end

	set_foreground_color (v: EV_COLOR) is
		require
			exists: v /= Void
		do
			text_fig.set_foreground_color (v)
		end

	set_background_color (v: EV_COLOR) is
		require
			exists: v /= Void
		do
			bg_fig.set_background_color (v)
		end

	set_text (v: STRING) is
		do
			text_fig.set_text (v)
			update_geometry
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	update_geometry is
		do
			bg_fig.set_radius (2)
			bg_fig.set_width (text_fig.overall_width + K_margins_width)
			bg_fig.set_height (text_fig.overall_height + K_margins_height)
			text_fig.set_point_position (
				point_x + K_left_margin, point_y + K_top_margin)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_top_margin: INTEGER is 2
	K_left_margin: INTEGER is 3

	K_margins_width: INTEGER is
			-- Width of left and right margins, combined
		once
			Result := K_left_margin * 2
		end

	K_margins_height: INTEGER is
			-- Height of top and bottom margins, combined
		once
			Result := K_top_margin * 2
		end

	colors: AEL_V_COLORS is
		once
			create Result
		end

end -- class AEL_V_GRAPH_POINT_LABEL
