class AEL_V_LINE_GRAPH

inherit
	AEL_V_XY_GRAPH
		redefine
			has_connectors, new_item_at_position
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	init_data_source is
		do
			add_series (<< 2.5, 1.0, 0.5, 1.2, 1.8, 2.1, 2.6, 3.8 >>)
			add_series (<< 2, 1.7, 4.2, 2, 2.7 >>)
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	new_figure_at_position (si, i: INTEGER; pt: EV_COORDINATE): EV_MODEL is
		local
			tfig: EV_MODEL_RECTANGLE
		do
			create tfig.make_rectangle (
				pt.x - half_dot, pt.y - half_dot, dot_width, dot_width)
			tfig.set_foreground_color (default_colors.item (si + 1).item (1))
			tfig.set_background_color (colors.white)
			Result := tfig
		end

	--|--------------------------------------------------------------

	new_item_at_position (sl: REAL; si, i: INTEGER): AEL_V_GRAPH_ITEM is
		local
			pt, ppt: EV_COORDINATE
		do
			Result := Precursor (sl, si, i)

			if not items.item (si).is_empty then
				create ppt.make (items.item (si).last.figure.x, items.item (si).last.figure.y)
				create pt.make (Result.figure.x, Result.figure.y)
				add_connector (si, ppt, pt)
			end
		end

	--|--------------------------------------------------------------

	has_connectors: BOOLEAN is
		do
			Result := True
		end

end -- class AEL_V_LINE_GRAPH
