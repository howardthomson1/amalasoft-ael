class AEL_V_MODEL_BAR
-- A bar graph bar figure

inherit
	EV_MODEL_GROUP
		redefine
			default_create
		end

create
	default_create

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	default_create is
		do
			Precursor {EV_MODEL_GROUP}

			bar_thickness := 10

			create bar
			extend (bar)
		end

--|========================================================================
feature -- Components
--|========================================================================

	bar: EV_MODEL_RECTANGLE
	shadow: EV_MODEL_POLYGON
	end_shadow: like shadow
	edge_shadow: like shadow

	setting_bar_points: BOOLEAN

	bar_color: EV_COLOR is
		do
			Result := private_bar_color
			if Result = Void then
				Result := dflt_bar_color
			end
		end

	end_shadow_color: EV_COLOR is
		do
			Result := private_end_shadow_color
			if Result = Void then
				Result := dflt_shadow_color
			end
		end

	edge_shadow_color: EV_COLOR is
		do
			Result := private_edge_shadow_color
			if Result = Void then
				Result := dflt_shadow_color
			end
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_graph_orientation (v: CHARACTER) is
			-- Single character, 'b', 'l', 't', 'r'
			-- Denoting: bottom, left, top and right, respectively
		do
			private_graph_orientation := v
		end

	graph_orientation: CHARACTER is
		do
			Result := private_graph_orientation
			if (Result = '%U') then
				Result := Kc_dflt_graph_orientation
			end
		end

	--|--------------------------------------------------------------

	set_bar_thickness (v: INTEGER) is
		do
			bar_thickness := v
		end

	set_line_width (v: INTEGER) is
		do
			bar.set_line_width (v)
		end

	set_bar_color (v: EV_COLOR) is
		do
			private_bar_color := v
			bar.set_background_color (v)
		end

	set_end_shadow_color (v: EV_COLOR) is
		do
			private_end_shadow_color := v
			if end_shadow /= Void then
				end_shadow.set_background_color (v)
			end
		end

	set_edge_shadow_color (v: EV_COLOR) is
		do
			private_edge_shadow_color := v
			if edge_shadow /= Void then
				edge_shadow.set_background_color (v)
			end
		end

	set_position (x1, y1, ay: INTEGER) is
		do
			bar.set_point_a_position (x1, y1)
			bar.set_point_b_position (x1 + bar_thickness, ay)
			draw_lines
		end

	set_point_a_position (xp, yp: INTEGER) is
		do
			bar.set_point_a_position (xp, yp)
--RFO			if (bar.point_b /= Void) then
				draw_lines
--RFO			end
		end

	set_point_b_position (xp, yp: INTEGER) is
		do
			bar.set_point_b_position (xp, yp)
--RFO			if (bar.point_a /= Void) then
				draw_lines
--RFO			end
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	draw_lines is
		local
--RFO			pt1, pt2: EV_RELATIVE_POINT
		do
			draw_shadows
--RFO			inspect graph_orientation
--RFO			when 'l' then
--RFO				-- End shadow is right Edge shadow is below
--RFO				create pt1.make_with_position (bar_right_x + 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_left_x + 1, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 1, bar_bottom_y + 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 't' then
--RFO				-- End shadow is below Edge shadow is right
--RFO				create pt1.make_with_position (bar_left_x + 1, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_right_x + 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y - 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 'r' then
--RFO				-- End shadow is left Edge shadow is below
--RFO				create pt1.make_with_position (bar_right_x - 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_left_x - 0, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 0, bar_bottom_y + 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 'b' then
--RFO				-- End shadow is top Edge shadow is right
--RFO				create pt1.make_with_position (bar_left_x + 2, bar_top_y - 2)
--RFO				create pt2.make_with_position (bar_right_x + 0, bar_top_y - 2)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_right_x + 0, bar_bottom_y - 4)
--RFO				edge_shadow.set_point_a_position (pt2.x, pt2.y)
--RFO				edge_shadow.set_point_b_position (pt1.x, pt1.y)
--RFO			end
		end

	draw_shadows is
		local
			bx1, bx2, by1, by2, st: INTEGER
--RFO			pt1, pt2: EV_RELATIVE_POINT
		do
			bx1 := bar_left_x
			bx2 := bar_right_x
			by1 := bar_top_y
			by2 := bar_bottom_y
			st := shadow_thickness

			create end_shadow.make_with_coordinates (
				<<
				create {EV_COORDINATE}.make (bx1, by1),
				create {EV_COORDINATE}.make (bx2, by1),
				create {EV_COORDINATE}.make (bx2 + st, by1 - st),
				create {EV_COORDINATE}.make (bx1 + st, by1 - st)
				>>)
			end_shadow.set_background_color (end_shadow_color)
			extend (end_shadow)
			send_to_back (end_shadow)

			create edge_shadow.make_with_coordinates (
				<<
				create {EV_COORDINATE}.make (bx2, by1),
				create {EV_COORDINATE}.make (bx2, by2),
				create {EV_COORDINATE}.make (bx2 + st, by2 - st),
				create {EV_COORDINATE}.make (bx2 + st, by1 - st)
				>>)
			edge_shadow.set_background_color (edge_shadow_color)
			extend (edge_shadow)
			send_to_back (edge_shadow)
		end

--|========================================================================
feature -- Geometry support
--|========================================================================

	bar_thickness: INTEGER

	shadow_thickness: INTEGER is
		do
			Result := (bar_thickness / 4).rounded.max (3)
		end

	bar_left_x: INTEGER is
		do
			Result := bar.point_a_x
		end

	bar_top_y: INTEGER is
		do
			inspect graph_orientation
			when 'l', 't', 'r' then
				Result := bar.point_a_y
			else
				Result := bar.point_a_y
			end
		end

	bar_right_x: INTEGER is
		do
			Result := bar.point_b_x
		end

	bar_bottom_y: INTEGER is
		do
			inspect graph_orientation
			when 'l', 'r', 't' then
				Result := bar.point_b_y
			else
				Result := bar.point_b_y
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	private_graph_orientation: CHARACTER
	Kc_dflt_graph_orientation: CHARACTER is 'b'

	private_bar_color: EV_COLOR
	private_end_shadow_color: EV_COLOR
	private_edge_shadow_color: EV_COLOR

	dflt_bar_color: EV_COLOR is
		once
			Result := colors.blue
		end

	dflt_shadow_color: EV_COLOR is
		once
			Result := colors.dark_gray
		end

	colors: AEL_V_COLORS is
		once
			create Result
		end

end -- class AEL_V_MODEL_BAR
