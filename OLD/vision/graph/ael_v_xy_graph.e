deferred class AEL_V_XY_GRAPH
-- Graph/plot widget using an XY (coordinate system)

inherit
	AEL_V_GRAPH
		redefine
			core_make, purge,
			on_item_mouse_over, on_item_mouse_leave, on_item_mouse_btn_press,
			on_mouse_btn_press, on_mouse_btn_release
		end

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	core_make is
		do
			if has_connectors then
				create connectors.make
			end
			Precursor
			projector.world.pointer_button_press_actions.extend (agent on_item_mouse_btn_press (Void,?,?,?,?,?,?,?,?))
		end

--|========================================================================
feature -- Components
--|========================================================================

	connectors: TWO_WAY_LIST [EV_MODEL_LINE]

	x_axis: EV_MODEL_LINE
	y_axis: EV_MODEL_LINE

--	point_label: AEL_V_MODEL_TEXT
	point_label: AEL_V_GRAPH_POINT_LABEL
	h_hair_line_line: EV_MODEL_LINE
	h_hair_line_text: EV_MODEL_TEXT
	h_hair_line: EV_MODEL_GROUP
	v_hair_line: EV_MODEL_LINE

--|========================================================================
feature -- Status
--|========================================================================

	has_connectors: BOOLEAN is
		do
		end

	axes_are_hidden: BOOLEAN
			-- Should x and y axes be hidden?

	x_axis_color: EV_COLOR is
		do
			Result := private_x_axis_color
			if Result = Void then
				Result := colors.black
			end
		end

	y_axis_color: EV_COLOR is
		do
			Result := private_y_axis_color
			if Result = Void then
				Result := colors.black
			end
		end

	axis_line_width: INTEGER is
		do
			Result := private_axis_line_width
			if Result = 0 then
				Result := K_dflt_axis_line_width
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	hide_axes is
		do
			axes_are_hidden := True
			if x_axis /= Void then
				x_axis. hide
			end
			if y_axis /= Void then
				y_axis. hide
			end
		end

	--|--------------------------------------------------------------

	set_x_axis_color (v: EV_COLOR) is
		do
			private_x_axis_color := v
		end

	set_y_axis_color (v: EV_COLOR) is
		do
			private_y_axis_color := v
		end

	set_axis_line_width (v: INTEGER) is
		require
			not_negative: v >= 0
		do
			private_axis_line_width := v
		end

--|========================================================================
feature {NONE} -- Hidden status
--|========================================================================

	private_x_axis_color: EV_COLOR
	private_y_axis_color: EV_COLOR
	private_axis_line_width: INTEGER

--|========================================================================
feature -- Rendering
--|========================================================================

	update_graph is
			-- Regenerate graph with updated geometry
		local
			si, i: INTEGER
			ti: AEL_V_GRAPH_ITEM
			dl: like series
		do
			purge
			data_source.do_all (agent factor_series_for_scale)
			update_geometry

			-- Axes should be layered below all other elements
			if not axes_are_hidden then
				draw_axes
			end

			from
				si := 1
				data_source.start
			until data_source.exhausted
			loop
				dl := data_source.item
				from i := dl.lower
				until i > dl.upper
				loop
					ti := new_item_at_position (dl.item (i), si, i)
					items.item (si).extend (ti)
					i := i + 1
				end
				-- For each series, add items last to force z-order
				from items.item (si).start
				until items.item (si).exhausted
				loop
					projector.world.extend (items.item (si).item.figure)
					items.item (si).forth
				end
				si := si + 1
				data_source.forth
			end
--RFO			draw_labels
			projector.full_project
		end

	--|--------------------------------------------------------------

	new_figure_at_position (si, i: INTEGER; pt: EV_COORDINATE): EV_MODEL is
		deferred
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_item_at_position (sl: REAL; si, i: INTEGER): AEL_V_GRAPH_ITEM is
		local
			tfig: like new_figure_at_position
			pt: EV_COORDINATE
		do
			create pt.make (
				y_axis_horizontal_margin + (chart_x_interval * (i - 1)),
				y_from_value (sl))

			tfig := new_figure_at_position (si, i, pt)
			create Result.make (tfig, sl, "Item " + si.out + "." + i.out)

			tfig.pointer_enter_actions.extend (agent on_item_mouse_over (Result))
			tfig.pointer_leave_actions.extend (agent on_item_mouse_leave (Result))
			tfig.pointer_button_press_actions.extend (agent on_item_mouse_btn_press (Result,?,?,?,?,?,?,?,?))
			tfig.pointer_button_release_actions.extend (agent on_mouse_btn_release)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	add_connector (si: INTEGER; ppt, pt: EV_COORDINATE) is
		require
			has_connectors: has_connectors
		local
			tl: EV_MODEL_LINE
		do
			create tl.make_with_points (ppt, pt)
			tl.set_foreground_color (item_colors.item (si).item (1))
			tl.pointer_button_release_actions.extend (agent on_mouse_btn_release)
			connectors.extend (tl)
			projector.world.extend (tl)
		end

--|========================================================================
feature {NONE} -- Rendering support
--|========================================================================

	draw_axes is
		require
			not_hidden: not axes_are_hidden
		local
			i, lim, x_spos, x_epos, y_spos, y_epos, h_xp, h_yp, h1, h2: INTEGER
			th: EV_MODEL_LINE
			lb: AEL_V_MODEL_TEXT
		do
			x_spos := y_axis_horizontal_margin
			x_epos := width - y_axis_horizontal_margin
			y_spos := x_axis_vertical_margin
			y_epos := x_axis_vertical_offset

			create x_axis.make_with_positions (x_spos, y_epos, x_epos, y_epos)
			x_axis.set_line_width (axis_line_width)
			x_axis.set_foreground_color (x_axis_color)
			projector.world.extend (x_axis)
			x_axis.pointer_double_press_actions.extend (
				agent on_2click (x_axis,?,?,?,?,?,?,?,?))
			create y_axis.make_with_positions (x_spos, y_spos, x_spos, y_epos)
			y_axis.set_line_width (axis_line_width)
			y_axis.set_foreground_color (y_axis_color)
			projector.world.extend (y_axis)
			y_axis.pointer_double_press_actions.extend (
				agent on_2click (y_axis,?,?,?,?,?,?,?,?))

			-- Draw X axis hashes
			lim := data_item_count.max (4)
			h1 := y_epos
			h2 := h1 + K_hash_mark_length
			from i := 1
			until i > lim
			loop
				h_xp := x_spos + (chart_x_interval * i)
				if h_xp < x_epos then
					create th.make_with_positions (h_xp, h1, h_xp, h2)
					th.set_foreground_color (y_axis_color)
					projector.world.extend (th)
				end
				i := i + 1
			end
			-- Draw Y axis hashes
			h1 := x_spos - K_hash_mark_length
			h2 := x_spos
			from i := 1
			until i > number_of_y_intervals
			loop
				h_yp := y_from_value (yhash_values.i_th (i))
				create th.make_with_positions (h1, h_yp, h2, h_yp)
				th.set_foreground_color (y_axis_color)
				projector.world.extend (th)
				lb := yhash_labels.i_th (i)
				lb.set_point_position (K_y_axis_label_margin, h_yp)
				projector.world.extend (lb)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	purge is
		do
			Precursor

			point_label := Void
			h_hair_line := Void
			v_hair_line := Void
			x_axis := Void
			y_axis := Void
			if has_connectors then
				connectors.wipe_out
			end
		end

	--|--------------------------------------------------------------

	hide_hair_lines is
		do
			if h_hair_line /= Void then
				h_hair_line.hide
				h_hair_line.set_point_position (0, 0)
			end
			if v_hair_line /= Void then
				v_hair_line.hide
				v_hair_line.set_point_a_position (0, 0)
				v_hair_line.set_point_b_position (0, 0)
			end
		end

	hide_point_label is
		do
			if point_label /= Void then
				point_label.hide
				point_label.set_point_position (width, height)
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_item_mouse_over (ti: AEL_V_GRAPH_ITEM) is
		local
			px, py: INTEGER
			f: EV_MODEL
			is_new: BOOLEAN
		do
			if point_label = Void then
				create point_label
				point_label.set_foreground_color (colors.blue)
				projector.world.extend (point_label)
				is_new := True
			end
			f ?= ti.figure
			px := f.x
			-- Place text enough above bar to clear
			py := (y_from_value (ti.data) - K_mouse_over_text_v_offset).max (0)
			if ti.label /= Void then
				point_label.set_text (ti.label)
			else
				point_label.set_text (ti.data.out)
			end
			px := px.min (width - point_label.overall_width)
			point_label.set_point_position (px, py)
			point_label.show
			if is_new then
				projector.project
			end
		end

	--|--------------------------------------------------------------

	on_item_mouse_leave (ti: AEL_V_GRAPH_ITEM) is
		do
			if point_label /= Void then
				point_label.set_point_position (width, height)
				point_label.hide
			end
		end

	--|--------------------------------------------------------------

	on_item_mouse_btn_press (ti: AEL_V_GRAPH_ITEM; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		local
			is_new: BOOLEAN
		do
			hide_point_label
			hide_hair_lines
			if b = 3 then
				if application.ctrl_pressed then
					is_new := h_hair_line = Void
					on_button_3_ctl_press (ti, xp, yp)
				else
					is_new := v_hair_line = Void
					on_button_3_noctl_press (ti, xp, yp)
				end
				if is_new then
					projector.project
				end
			else
			end
		end

	--|--------------------------------------------------------------

	on_button_3_noctl_press (ti: AEL_V_GRAPH_ITEM; xp, yp: INTEGER) is
		do
			if h_hair_line = Void then
				create h_hair_line
				create h_hair_line_line
				h_hair_line.extend (h_hair_line_line)
				h_hair_line_line.set_point_a_position (0, 0)
				h_hair_line_line.set_point_b_position (width - right_margin - 15, 0)
				create h_hair_line_text
				h_hair_line.extend (h_hair_line_text)
				h_hair_line_text.set_point_position (width - right_margin - 12, -10)
				projector.world.extend (h_hair_line)
				projector.world.bring_to_front (h_hair_line)
			end
			h_hair_line.set_point_position (0, yp)
			h_hair_line_text.set_text (value_from_y (yp).out)
			h_hair_line.show
		end

	--|--------------------------------------------------------------

	on_button_3_ctl_press (ti: AEL_V_GRAPH_ITEM; xp, yp: INTEGER) is
		do
			if v_hair_line = Void then
				create v_hair_line
				projector.world.extend (v_hair_line)
				projector.world.send_to_back (v_hair_line)
			end
			v_hair_line.set_point_a_position (xp, 0)
			v_hair_line.set_point_b_position (xp, height)
			v_hair_line.show
		end

	--|--------------------------------------------------------------

	on_mouse_btn_press (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
			hide_point_label
			hide_hair_lines
		end

	--|--------------------------------------------------------------

	on_mouse_btn_release (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER) is
		do
			hide_point_label
			hide_hair_lines
			if b = 3 then
			else
			end
--RFO			projector.project
		end

--|========================================================================
feature {NONE} -- Geometry support
--|========================================================================

	update_geometry is
		local
			i, n, m, td, nd, dv, iv: INTEGER
			v: REAL
			isneg: BOOLEAN
			ts: STRING
			lb: AEL_V_MODEL_TEXT
		do
			-- Set chart y interval as the distance, in pixels,
			-- between points on the Y axis
			v := largest_value
			if v < 0 then
				isneg := True
				v := -v
			end
			ts := v.truncated_to_integer.out
			nd := ts.count
			if nd = 1 then
				m := v.ceiling
				n := m - 1
				iv := 1
			else
				dv := (10^(nd - 1)).rounded
				n := (v / dv).ceiling
				m := n * dv
				n := n - 1
				iv := dv
			end
			if isneg then
				m := -m
			end
			number_of_y_intervals := n
			chart_y_interval := (plot_area_height / n).rounded.max (1)

			create yhash_labels.make (n)
			create yhash_values.make (n)
			max_y_label_width := 0
			from i := 1
			until i > n
			loop
				create lb.make_with_text (i.out)
				max_y_label_width := max_y_label_width.max (lb.overall_width)
				yhash_labels.extend (lb)
				yhash_values.extend (i)
				i := i + iv
			end

			-- Set chart x interval as the distance, in pixels,
			-- between points on the X axis
			-- Interval = Bar + (shadow=bar*.25) + (space=bar*.75) == Bar*2
			if data_item_count = 0 then
				-- 10 pixel separate is reasonable when no data is 
				-- present, just to show something
			else
				-- Items (bar, shadow and space) must fit within 
				-- plot_area_width
				td := data_item_count
				if td = 0 then
					td := 1
				end
				chart_x_interval := (plot_area_width / td).rounded
			end

			value_scale_factor := plot_area_height / largest_value
		end

	--|--------------------------------------------------------------

	chart_x_interval: INTEGER
			-- Distance, in pixels, between points on the X axis

	chart_y_interval: INTEGER
			-- Distance, in pixels, between points on the Y axis

	x_axis_vertical_offset: INTEGER is
			-- Offset from top to x axis
		do
			Result := height - x_axis_vertical_margin
		end

	x_axis_vertical_margin: INTEGER is
		do
			Result := bottom_margin
		end

	y_axis_horizontal_margin: INTEGER is
			-- Distance, in pixels, between edge of area and y axis
		do
			-- Determine x axis margins
			Result := (width // 8).min (20).max (max_y_label_width + K_y_axis_label_margin)
		end

	value_scale_factor: REAL
			-- The factor by which the series values should be scaled

	y_from_value (v: REAL): INTEGER is
			-- Y position for given value
		do
			Result := x_axis_vertical_offset - (v * value_scale_factor).rounded
		end

	value_from_y (v: INTEGER): REAL is
			-- Value at given Y position
		do
			Result := (x_axis_vertical_offset - v) / value_scale_factor
		end

	yhash_labels: ARRAYED_LIST [AEL_V_MODEL_TEXT]
	yhash_values: ARRAYED_LIST [INTEGER]

	number_of_y_intervals: INTEGER

	max_y_label_width: INTEGER

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	dot_width: INTEGER is 5

	half_dot: INTEGER is
		do
			Result := dot_width // 2
		end

	--|--------------------------------------------------------------

	K_hash_mark_length: INTEGER is 5

	K_dflt_axis_line_width: INTEGER is 2

	K_y_axis_label_margin: INTEGER is 3

	K_mouse_over_text_v_offset: INTEGER is 25

	K_color_x_axis: EV_COLOR is
		do
			Result := colors.dark_green
		end

	K_color_y_axis: EV_COLOR is
		do
			Result := colors.dark_green
		end

end -- class AEL_V_XY_GRAPH
