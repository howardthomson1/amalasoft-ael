class AV_BAR_GRAPH_ITEM

inherit
  AV_GRAPH_ITEM
   redefine
     cached_data, figure
   end;

creation
 make

 --|========================================================================
feature
 --|========================================================================

 cached_data : AV_BAR_GRAPH_ITEM_DATA;
-- figure : EV_FIGURE_RECTANGLE;
 figure : AV_FIGURE_BAR;

 --|------------------------------------------------------------------------
invariant

end -- class AV_BAR_GRAPH_ITEM
