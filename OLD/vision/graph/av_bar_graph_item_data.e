class AV_BAR_GRAPH_ITEM_DATA

inherit
  AV_GRAPH_ITEM_DATA

creation
 make

 --|========================================================================
feature {NONE}
 --|========================================================================

 make( os : DOUBLE; oct : INTEGER ) is
  do
    set_offset( os );
    set_octant( oct );
  end;

 --|========================================================================
feature
 --|========================================================================

 offset : DOUBLE;

 set_offset( v : DOUBLE ) is
  do
    offset := v;
  end;

 --|------------------------------------------------------------------------

 octant : INTEGER;

 set_octant( v : INTEGER ) is
  do
    octant := v;
  end;

 --|------------------------------------------------------------------------
invariant

end -- class AV_BAR_GRAPH_ITEM_DATA
