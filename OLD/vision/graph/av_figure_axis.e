class AV_FIGURE_AXIS

inherit
	EV_MODEL_GROUP

create
	make_horizontal, make_vertical, make_with_graph_orientation

 --|========================================================================
feature {NONE}
 --|========================================================================

	make_horizontal is
		do
			is_horizontal := True
			graph_orientation := 'l'
			core_make
		end

	make_vertical is
		do
			is_horizontal := False
			graph_orientation := 'l'
			core_make
		end

	--|--------------------------------------------------------------

	make_with_graph_orientation (o: CHARACTER; hf: BOOLEAN) is
		do
			graph_orientation := o
			is_horizontal := hf
			core_make
		end

	--|--------------------------------------------------------------

	core_make is
		do
			create hash_marks.make
			default_create

			create axis
			extend (axis)
		end

 --|========================================================================
feature
 --|========================================================================

	axis: EV_MODEL_LINE
	hash_marks: LINKED_LIST [EV_MODEL_LINE]

	--|--------------------------------------------------------------

	set_line_width (v: INTEGER) is
		do
			private_line_width := v
			axis.set_line_width (v)
			from hash_marks.start
			until hash_marks.exhausted
			loop
				hash_marks.item.set_line_width (v)
				hash_marks.forth
			end
		end

	--|--------------------------------------------------------------

	line_width: INTEGER is
		do
			Result := private_line_width
			if Result = 0 then
				Result := K_dflt_line_width
			end
		end

	--|--------------------------------------------------------------

	set_foreground_color (v: EV_COLOR) is
		do
			private_foreground_color := v
			axis.set_foreground_color (v)
			from hash_marks.start
			until hash_marks.exhausted
			loop
				hash_marks.item.set_foreground_color (v)
				hash_marks.forth
			end
		end

	foreground_color: EV_COLOR is
		do
			Result := private_foreground_color
			if (Result = Void) then
				Result := K_dflt_foreground_color
			end
		end

	--|--------------------------------------------------------------

	set_point_a (v: EV_RELATIVE_POINT) is
		do
--			axis.set_point_a (v)
			axis.set_point_a (create {EV_COORDINATE}.make (v.x, v.y))
			if axis.point_b /= Void then
				draw_hash_marks
			end
		end

	set_point_b (v: EV_RELATIVE_POINT) is
		do
--			axis.set_point_b (v)
			axis.set_point_b (create {EV_COORDINATE}.make (v.x, v.y))
			if axis.point_a /= Void then
				draw_hash_marks
			end
		end

	--|--------------------------------------------------------------

	draw_hash_marks is
		do
			remove_hash_marks
			if not no_hash_marks then
				if is_horizontal then
					draw_hash_marks_for_horizontal_axis
				else
					draw_hash_marks_for_vertical_axis
				end
			end
		end

	--|--------------------------------------------------------------

	draw_hash_marks_for_horizontal_axis is
		local
			pt: EV_RELATIVE_POINT
			i, lim, alen, hlen, spc, xpos, ypos1, ypos2: INTEGER
			hm: EV_MODEL_LINE
		do
			alen := axis_length
			lim := hash_mark_count
			hlen := hash_mark_length
			spc := alen // lim
			xpos := 0

			if hash_is_spanning then
				ypos1 := -(hlen // 2)
				ypos2 := hlen // 2
			elseif hash_is_inside then
				ypos1 := -hlen
				ypos2 := 0
			else
				-- Default is hash_is_outside
				ypos1 := 0
				ypos2 := hlen
			end

			from i := 0
			until i > lim
			loop
				create pt.make_with_origin_and_position (axis_origin, xpos, ypos1)
				create hm
				hm.set_line_width (line_width)
				hm.set_foreground_color (foreground_color)
				hm.set_point_a (create {EV_COORDINATE}.make (pt.x, pt.y))
				create pt.make_with_origin_and_position (axis_origin, xpos, ypos2)
				hm.set_point_b (create {EV_COORDINATE}.make (pt.x, pt.y))
				hash_marks.extend (hm)
				extend (hm)
				xpos := xpos + spc
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	draw_hash_marks_for_vertical_axis is
		local
			pt: EV_RELATIVE_POINT
			i, lim, alen, hlen, spc, xpos1, xpos2, ypos: INTEGER
			hm: EV_MODEL_LINE
		do
			alen := axis_length
			lim := hash_mark_count
			hlen := hash_mark_length
			spc := alen // lim
			ypos := 0

			if hash_is_spanning then
				xpos1 := -(hlen // 2)
				xpos2 := hlen // 2
			elseif hash_is_inside then
				xpos1 := 0
				xpos2 := hlen
			else
				-- Default is hash_is_outside
				xpos1 := -hlen
				xpos2 := 0
			end

			if is_right then
				from i := 0
				until i > lim
				loop
					create pt.make_with_origin_and_position (axis_origin, xpos1, ypos)
					create hm
					hm.set_line_width (line_width)
					hm.set_foreground_color (foreground_color)
					hm.set_point_a (create {EV_COORDINATE}.make (pt.x, pt.y))
					create pt.make_with_origin_and_position (axis_origin, xpos2, ypos)
					hm.set_point_b (create {EV_COORDINATE}.make (pt.x, pt.y))
					hash_marks.extend (hm)
					extend (hm)
					ypos := ypos - spc
					i := i + 1
				end
			else
				from i := 0
				until i > lim
				loop
					create pt.make_with_origin_and_position (axis_origin, xpos1, ypos)
					create hm
					hm.set_line_width (line_width)
					hm.set_foreground_color (foreground_color)
					hm.set_point_a (create {EV_COORDINATE}.make (pt.x, pt.y))
					create pt.make_with_origin_and_position (axis_origin, xpos2, ypos)
					hm.set_point_b (create {EV_COORDINATE}.make (pt.x, pt.y))
					hash_marks.extend (hm)
					extend (hm)
					ypos := ypos - spc
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	remove_hash_marks is
		do
			from hash_marks.start
			until hash_marks.exhausted or hash_marks.exhausted
			loop
				prune_all (hash_marks.item)
				hash_marks.remove
			end
		end

	--|--------------------------------------------------------------

	axis_origin: EV_RELATIVE_POINT is
		do
			if is_horizontal then
--				Result := axis.point_a
				create Result.make_with_position (axis.point_a.x, axis.point_a.y)
			else
--				Result := axis.point_b
				create Result.make_with_position (axis.point_b.x, axis.point_b.y)
			end
		end

	axis_terminus: EV_RELATIVE_POINT is
		do
			if is_horizontal then
--				Result := axis.point_b
				create Result.make_with_origin_and_position (axis_origin, axis.point_b.x, axis.point_b.y)
			else
--				Result := axis.point_a
				create Result.make_with_origin_and_position (axis_origin, axis.point_a.x, axis.point_a.y)
			end
		end

	--|--------------------------------------------------------------

	set_hash_mark_length (v: INTEGER) is
		do
			private_hash_mark_length := v
		end

	hash_mark_length: INTEGER is
		do
			Result := private_hash_mark_length
			if Result = 0 then
				Result := K_dflt_hash_mark_length
			end
		ensure
			positive: Result > 0
		end

	--|--------------------------------------------------------------

	hash_mark_count: INTEGER is
			-- Number of hash marks, exclusive of mark at origin
		local
			len, spc: INTEGER
		do
			spc := hash_mark_spacing
			len := axis_length
			Result := len // spc
		ensure
			positive: Result > 0
		end

	--|--------------------------------------------------------------

	hash_mark_spacing: INTEGER is
		do
			Result := private_hash_mark_spacing
			if Result = 0 then
				Result := K_dflt_hash_mark_spacing
			end
		ensure
			positive: Result > 0
		end

	--|--------------------------------------------------------------

	axis_length: INTEGER is
		do
			if is_horizontal then
				Result := axis_terminus.x - axis_origin.x
			else
				Result := axis_origin.y - axis_terminus.y
			end
			Result := Result.abs
		ensure
			positive: Result > 0
		end

	--|--------------------------------------------------------------

	is_horizontal: BOOLEAN
	graph_orientation: CHARACTER

	is_bottom: BOOLEAN is
		do
			Result := is_horizontal and graph_orientation = 'b'
		end

	is_top: BOOLEAN is
		do
			Result := is_horizontal and graph_orientation = 't'
		end

	is_left: BOOLEAN is
		do
			Result := (not is_horizontal) and graph_orientation = 'l'
		end

	is_right: BOOLEAN is
		do
			Result := (not is_horizontal) and graph_orientation = 'r'
		end

	--|--------------------------------------------------------------

	no_hash_marks: BOOLEAN is
		do
			Result := hash_alignment = K_hash_align_none
		end

	hash_is_spanning: BOOLEAN is
		do
			Result := hash_alignment = K_hash_align_spanning
		end

	hash_is_inside: BOOLEAN is
		do
			Result := hash_alignment = K_hash_align_inside
		end

	hash_is_outside: BOOLEAN is
		do
			Result := hash_alignment = K_hash_align_outside
		end

	--|--------------------------------------------------------------

	hash_alignment: INTEGER

	--|--------------------------------------------------------------

	set_hash_alignment (v: INTEGER) is
		do
			hash_alignment := v
		end

	--|--------------------------------------------------------------

	hide_hash_marks is
		do
			hash_alignment := K_hash_align_none
		end

	align_hash_inside is
		do
			hash_alignment := K_hash_align_inside
		end

	align_hash_outside is
		do
			hash_alignment := K_hash_align_outside
		end

	align_hash_spanning is
		do
			hash_alignment := K_hash_align_spanning
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	private_line_width: INTEGER
	private_hash_mark_length: INTEGER
	private_hash_mark_spacing: INTEGER

	private_foreground_color: EV_COLOR

	--|--------------------------------------------------------------

	K_dflt_line_width: INTEGER is 1
	K_dflt_hash_mark_length: INTEGER is 7
	K_dflt_hash_mark_spacing: INTEGER is 10

	K_dflt_foreground_color: EV_COLOR is
		do
			Result := colors.black
		end

	--|--------------------------------------------------------------

	K_hash_align_none: INTEGER is -1
	K_hash_align_outside: INTEGER is 0
	K_hash_align_inside: INTEGER is 1
	K_hash_align_spanning: INTEGER is 2

	colors: AEL_V_COLORS is
		once
			create Result
		end

end -- class AV_FIGURE_AXIS
