class AV_FIGURE_BAR

inherit
  EV_FIGURE_GROUP
   redefine
     default_create
   end;
  AV_COLORS
   undefine
     default_create
   end;

creation
 default_create
--, make_with_point

 --|========================================================================
feature {NONE}
 --|========================================================================

 default_create is
  do
    Precursor {EV_FIGURE_GROUP};
    !! end_shadow;
    end_shadow.set_line_width( 2 );
    end_shadow.set_foreground_color( K_color_dark_gray );
    extend( end_shadow );
    !! edge_shadow;
    edge_shadow.set_line_width( 3 );
    edge_shadow.set_foreground_color( K_color_dark_gray );
    extend( edge_shadow );

    !! bar;
    extend( bar );
  end;

 --|========================================================================
feature
 --|========================================================================

 set_graph_orientation( v : CHARACTER ) is
  -- Single character, 'b', 'l', 't', 'r'
  -- Denoting: bottom, left, top and right, respectively
  do
    private_graph_orientation := v;
  end;

 graph_orientation : CHARACTER is
  do
    Result := private_graph_orientation;
    if( Result = '%U' ) then
      Result := Kc_dflt_graph_orientation;
    end;
  end;

 --|------------------------------------------------------------------------

 bar : EV_FIGURE_RECTANGLE;
 shadow : EV_FIGURE_POLYGON;
 end_shadow : EV_FIGURE_LINE;
 edge_shadow : EV_FIGURE_LINE;

 set_line_width( v : INTEGER ) is
  do
    bar.set_line_width( v );
  end;

 set_background_color( v : EV_COLOR ) is
  do
    bar.set_background_color( v );
  end;

 set_point_a( v : EV_RELATIVE_POINT ) is
  do
    bar.set_point_a( v );
    if( bar.point_b /= Void ) then
      draw_lines;
    end;
  end;

 set_point_b( v : EV_RELATIVE_POINT ) is
  do
    bar.set_point_b( v );
    if( bar.point_a /= Void ) then
      draw_lines;
    end;
  end;

 --|------------------------------------------------------------------------

 draw_lines is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
  do
    inspect graph_orientation
    when 'l' then
      -- End shadow is right; Edge shadow is below
      !! pt1.make_with_position( bar_right_x + 1, bar_top_y + 1 );
      !! pt2.make_with_position( bar_right_x + 1, bar_bottom_y + 1 );
      end_shadow.set_point_a( pt1 );
      end_shadow.set_point_b( pt2 );

      !! pt1.make_with_position( bar_left_x + 1, bar_bottom_y + 1 );
      !! pt2.make_with_position( bar_right_x - 1, bar_bottom_y + 1 );
      edge_shadow.set_point_a( pt1 );
      edge_shadow.set_point_b( pt2 );
    when 't' then
      -- End shadow is below; Edge shadow is right
      !! pt1.make_with_position( bar_left_x + 1, bar_bottom_y + 1 );
      !! pt2.make_with_position( bar_right_x + 1, bar_bottom_y + 1 );
      end_shadow.set_point_a( pt1 );
      end_shadow.set_point_b( pt2 );

      !! pt1.make_with_position( bar_right_x + 1, bar_top_y + 1 );
      !! pt2.make_with_position( bar_right_x + 1, bar_bottom_y - 1 );
      edge_shadow.set_point_a( pt1 );
      edge_shadow.set_point_b( pt2 );
    when 'r' then
      -- End shadow is left; Edge shadow is below
      !! pt1.make_with_position( bar_right_x - 1, bar_top_y + 1 );
      !! pt2.make_with_position( bar_right_x - 1, bar_bottom_y + 1 );
      end_shadow.set_point_a( pt1 );
      end_shadow.set_point_b( pt2 );

      !! pt1.make_with_position( bar_left_x - 0, bar_bottom_y + 1 );
      !! pt2.make_with_position( bar_right_x - 0, bar_bottom_y + 1 );
      edge_shadow.set_point_a( pt1 );
      edge_shadow.set_point_b( pt2 );
    when 'b' then
      -- End shadow is top; Edge shadow is right
      !! pt1.make_with_position( bar_left_x + 1, bar_top_y - 1 );
      !! pt2.make_with_position( bar_right_x + 0, bar_top_y - 1 );
      end_shadow.set_point_a( pt1 );
      end_shadow.set_point_b( pt2 );

      !! pt1.make_with_position( bar_right_x + 0, bar_bottom_y - 2 );
      edge_shadow.set_point_a( pt2 );
      edge_shadow.set_point_b( pt1 );
    end;
  end;

 --|------------------------------------------------------------------------

 bar_left_x : INTEGER is
  do
    Result := bar.point_a.x;
  end;

 bar_top_y : INTEGER is
  do
    inspect graph_orientation
    when 'l', 't', 'r' then
      Result := bar.point_a.y;
    else
      Result := bar.point_a.y;
    end;
  end;

 bar_right_x : INTEGER is
  do
    Result := bar.point_b.x;
  end;

 bar_bottom_y : INTEGER is
  do
    inspect graph_orientation
    when 'l', 'r', 't' then
      Result := bar.point_b.y;
    else
      Result := bar.point_b.y;
    end;
  end;

 --|------------------------------------------------------------------------

 private_graph_orientation : CHARACTER;
 Kc_dflt_graph_orientation : CHARACTER is 'b';

 --|------------------------------------------------------------------------
invariant

end -- class AV_FIGURE_BAR
