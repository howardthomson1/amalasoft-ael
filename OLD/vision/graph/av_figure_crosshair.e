class AV_FIGURE_CROSSHAIR

inherit
  EV_FIGURE_GROUP
   redefine
     default_create
   end;
  AV_COLORS
   undefine
     default_create
   end;

creation
 make_with_ranges

 --|========================================================================
feature {NONE}
 --|========================================================================

 make is
  do
    default_create;
  end;

 make_with_ranges( xr, yr : INTEGER ) is
  Require
    valid_ranges: xr >= 0 and yr >= 0;
  do
    private_x_range := xr;
    private_y_range := yr;
    make;
  end;

 --|------------------------------------------------------------------------

 default_create is
  do
    Precursor {EV_FIGURE_GROUP};

    !! xline;
    xline.set_foreground_color( foreground_color );
    extend( xline );

    !! yline;
    yline.set_foreground_color( foreground_color );
    extend( yline );
  end;

 --|========================================================================
feature
 --|========================================================================

 xline : EV_FIGURE_LINE;
 yline : EV_FIGURE_LINE;

 set_position( ax, ay : INTEGER ) is
  local
    pt : EV_RELATIVE_POINT;
  do
--    !! pt.make_with_position( ax, ay );
    !! pt.make_with_position( 0, ay );
    set_x_point_a( pt );
    !! pt.make_with_position( private_x_range, ay );
    set_x_point_b( pt );

    !! pt.make_with_position( ax, 0 );
    set_y_point_a( pt );
    !! pt.make_with_position( ax, private_y_range );
    set_y_point_b( pt );
  end;

 --|------------------------------------------------------------------------

 set_line_width( v : INTEGER ) is
  do
    private_line_width := v;
    xline.set_line_width( v );
    yline.set_line_width( v );
  end;

 --|------------------------------------------------------------------------

 line_width : INTEGER is
  do
    Result := private_line_width;
    if( Result = 0 ) then
      Result := K_dflt_line_width;
    end;
  end;

 --|------------------------------------------------------------------------

 set_foreground_color( v : EV_COLOR ) is
  do
    private_foreground_color := v;
    xline.set_foreground_color( v );
    yline.set_foreground_color( v );
  end;

 foreground_color : EV_COLOR is
  do
    Result := private_foreground_color;
    if( Result = Void ) then
      Result := K_dflt_foreground_color;
    end;
  end;

 --|------------------------------------------------------------------------

 set_x_point_a( v : EV_RELATIVE_POINT ) is
  do
    xline.set_point_a( v );
    if( xline.point_b /= Void ) then
    end;
  end;

 set_x_point_b( v : EV_RELATIVE_POINT ) is
  do
    xline.set_point_b( v );
    if( xline.point_a /= Void ) then
    end;
  end;

 --|------------------------------------------------------------------------

 set_y_point_a( v : EV_RELATIVE_POINT ) is
  do
    yline.set_point_a( v );
    if( yline.point_b /= Void ) then
    end;
  end;

 set_y_point_b( v : EV_RELATIVE_POINT ) is
  do
    yline.set_point_b( v );
    if( yline.point_a /= Void ) then
    end;
  end;

 --|------------------------------------------------------------------------

 xline_origin : EV_RELATIVE_POINT is
  do
    Result := xline.point_a;
  end;

 xline_terminus : EV_RELATIVE_POINT is
  do
    Result := xline.point_b;
  end;

 --|------------------------------------------------------------------------

 yline_origin : EV_RELATIVE_POINT is
  do
    Result := yline.point_a;
  end;

 yline_terminus : EV_RELATIVE_POINT is
  do
    Result := yline.point_b;
  end;

 --|------------------------------------------------------------------------

 xline_length : INTEGER is
  do
    Result := xline_terminus.x - xline_origin.x;
  Ensure
    positive: Result > 0
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 private_line_width : INTEGER;

 private_foreground_color : EV_COLOR;

 private_x_range : INTEGER;
 private_y_range : INTEGER;

 --|------------------------------------------------------------------------

 K_dflt_line_width : INTEGER is 1;

 K_dflt_foreground_color : EV_COLOR is
  do
    Result := K_color_red;
  end;

 --|------------------------------------------------------------------------
invariant

end -- class AV_FIGURE_CROSSHAIR
