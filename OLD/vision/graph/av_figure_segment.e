class AV_FIGURE_SEGMENT
  -- Individual segment (decorated rectangle) of a segmented bar

inherit
  EV_FIGURE_GROUP
   redefine
     default_create
   end;
  AV_COLORS
   undefine
     default_create
   end;

creation
 default_create

 --|========================================================================
feature {NONE}
 --|========================================================================

 default_create is
  do
    Precursor {EV_FIGURE_GROUP};

    !! bar;
    extend( bar );
    set_line_width( 1 );
  end;

 --|========================================================================
feature
 --|========================================================================

 bar : EV_FIGURE_RECTANGLE;

 --|------------------------------------------------------------------------

 minimum_start_position : INTEGER;
 original_start_position : INTEGER;
 original_size : INTEGER;

 --|------------------------------------------------------------------------

 width : INTEGER is
  do
    Result := bar.width;
  end;

 size : INTEGER is
  -- The size of the associated object -- NOT the width of the figure!!!
  do
    Result := private_size;
  end;

 start_position : INTEGER is
  do
    Result := private_start_position;
  end;

 end_position : INTEGER is
  do
    Result := point_b.x;
  end;

 --|------------------------------------------------------------------------

 set_line_width( v : INTEGER ) is
  do
    bar.set_line_width( v );
  end;

 --|------------------------------------------------------------------------

 set_background_color( v : EV_COLOR ) is
  do
    bar.set_background_color( v );
  end;

 --|------------------------------------------------------------------------

 enable_select is
  do
    is_selected := True;
    --bar.set_line_width( 1 );
    bar.set_foreground_color( K_color_blue );
  end;

 disable_select is
  do
    is_selected := False;
    --bar.set_line_width( 1 );
    bar.set_foreground_color( K_color_black );
  end;

 --|------------------------------------------------------------------------

 point_a : EV_RELATIVE_POINT is
  do
    Result := bar.point_a;
  end;

 point_b : EV_RELATIVE_POINT is
  do
    Result := bar.point_b;
  end;

 --|------------------------------------------------------------------------

 set_point_a( v : EV_RELATIVE_POINT ) is
  do
    bar.set_point_a( v );
  end;

 --|------------------------------------------------------------------------

 set_point_b( v : EV_RELATIVE_POINT ) is
  do
    bar.set_point_b( v );
  end;

 --|------------------------------------------------------------------------

 x_position : INTEGER is
  do
    Result := point_a.x;
  end;

 --|------------------------------------------------------------------------

 set_x_position( v : INTEGER ) is
  require
    valid_position: v >= minimum_start_position;
  local
    offset : INTEGER;
  do
--    hide;
    offset := v - point_a.x;
    point_a.set_x( v );
    point_b.set_x( point_b.x + offset );
    show;
  end;

 --|------------------------------------------------------------------------

 bar_left_x : INTEGER is
  do
    Result := bar.point_a.x;
  end;

 bar_top_y : INTEGER is
  do
    Result := bar.point_a.y;
  end;

 bar_right_x : INTEGER is
  do
    Result := bar.point_b.x;
  end;

 bar_bottom_y : INTEGER is
  do
    Result := bar.point_b.y;
  end;

 --|------------------------------------------------------------------------

 is_selected : BOOLEAN;

 --|========================================================================
feature
 --|========================================================================

 set_minimum_start_position( v : INTEGER ) is
  do
    minimum_start_position := v;
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 private_size : INTEGER;
 private_start_position : INTEGER;

 --|------------------------------------------------------------------------
invariant
  bar_exists: bar /= Void;

end -- class AV_FIGURE_SEGMENT
