class AV_FIGURE_SIZER

inherit
  EV_FIGURE_PICTURE

creation
 default_create,
 make_with_pixmap

 --|========================================================================
feature -- Status report
 --|========================================================================

 segment : AV_FIGURE_SEGMENT;

 --|========================================================================
feature -- Status setting
 --|========================================================================

 set_segment( v : like segment ) is
  do
    segment := v;
  end;

 --|------------------------------------------------------------------------

 set_x_position( v : INTEGER ) is
  do
--    hide;
    point.set_x( v );
--    show;
  end;

 --|------------------------------------------------------------------------
invariant

end -- class AV_FIGURE_SIZER
