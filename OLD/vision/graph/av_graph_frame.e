deferred class AV_GRAPH_FRAME

inherit
	EV_FRAME
		rename
			extend as af_extend
		redefine
			set_background_color
		end
	BASIC_ROUTINES
		undefine
			default_create, is_equal, copy
		end

 --|========================================================================
feature {NONE} -- Creation and initializaation
 --|========================================================================

	make is
		do
			default_create
			create items.make
			make_components

			figure_world.pointer_button_release_actions.extend (
				agent select_item_by_xy)
			resize_actions.extend (agent on_resize)

			set_geometry
		end

  --|--------------------------------------------------------------

	make_components is
		do
			create chart
			af_extend (chart)

			create figure_world
			create projector.make (figure_world, chart)
		end

	--|--------------------------------------------------------------

	set_geometry is
		do
			if private_left_margin = 0 then
				private_left_margin := 20
			end
			if private_right_margin = 0 then
				private_right_margin := 20
			end
			if private_top_margin = 0 then
				private_top_margin := 20
			end
			if private_bottom_margin = 0 then
				private_bottom_margin := 20
			end
		end

 --|========================================================================
feature
 --|========================================================================

	data_source: CONTAINER [ANY] is
		do
		end

	reference_object: ANY is
		do
		end

	reference_item: AV_GRAPH_ITEM
	reference_figure: EV_MODEL

	items: LINKED_LIST [like reference_item]

	--|--------------------------------------------------------------

	chart: EV_DRAWING_AREA
	projector:  EV_MODEL_DRAWING_AREA_PROJECTOR
	figure_world: EV_MODEL_WORLD

	shadow_thickness: INTEGER is
		do
			Result := 2
		end

	--|--------------------------------------------------------------

	margin_height: INTEGER is
		do
			Result := private_top_margin
		end

	margin_width: INTEGER is
		do
			Result := private_left_margin
		end

	--|--------------------------------------------------------------

	top_margin: INTEGER is
		do
			Result := private_top_margin
			if Result = 0 then
				Result := dflt_margin_height
			end
		end

	left_margin: INTEGER is
		do
			Result := private_left_margin
			if Result = 0 then
				Result := dflt_margin_width
			end
		end

	right_margin: INTEGER is
		do
			Result := private_right_margin
			if Result = 0 then
				Result := dflt_margin_width
			end
		end

	bottom_margin: INTEGER is
		do
			Result := private_bottom_margin
			if Result = 0 then
				Result := dflt_margin_height
			end
		end

	--|--------------------------------------------------------------

	x_minimum: INTEGER is
		do
			Result := left_margin
		end

	x_maximum: INTEGER is
		do
			Result := minimum_width.max(width) - right_margin
		end

	--|--------------------------------------------------------------

	y_minimum: INTEGER is
		do
			Result := top_margin
		end
	
	y_maximum: INTEGER is
		do
			Result := minimum_height.max(height) - bottom_margin
		end

 --|========================================================================
feature
 --|========================================================================

	extend (v: like reference_item) is
		do
		end

	--|--------------------------------------------------------------

	clear is
		do
			chart.clear
		end

	--|--------------------------------------------------------------

	update_graph_with_data (v: like data_source) is
		do
		end

	update_graph is
		deferred
		end

	--|--------------------------------------------------------------

	new_item (v: like reference_object): like reference_item is
		require
			obj_exists: v /= Void
		do
		ensure
			result_exists: Result /= Void
		end

 --|========================================================================
feature -- Color
 --|========================================================================

	set_background_color (v: EV_COLOR) is
		do
			Precursor (v)
			figure_world.set_background_color (v)
		end

	num_colors: INTEGER is
		do
			Result := item_palette.count
		end

	item_color (v: INTEGER): EV_COLOR is
		local
			ti: INTEGER
		do
			if v <= num_colors then
				Result := item_palette.item (v)
			else
				ti := v \\ num_colors
				if ti = 0 then
					ti := num_colors
				end
				Result := item_palette.item (ti)
			end
		end

	item_palette: ARRAY [EV_COLOR] is
		once
--    Result :=
--	<<
--	  colors.blue,
--	  colors.dark_blue,
--	  colors.cyan,
--	  colors.dark_cyan,
--	  colors.dark_green,
--	  colors.yellow,
--	  colors.dark_yellow,
--	  colors.red,
--	  colors.dark_red,
--	  colors.magenta,
--	  colors.dark_magenta
--	  >>
--      
			Result := pastel_colors
		end

 --|------------------------------------------------------------------------

	spectrum_colors: ARRAY [EV_COLOR] is
			-- nearly progressive colors for chart
		once
			Result :=
				<<
				colors.black,
			colors.red,
			colors.orange,
			colors.gold,
			colors.yellow,
			colors.light_green,
			colors.green,
			colors.dark_green,
			colors.cyan,
			colors.light_sky_blue,
			colors.blue,
			colors.navy,
			colors.magenta,
			colors.white,
			colors.dark_gray,
			colors.light_gray
			--	  colors.dark_gray,
			--	  colors.lightgray
			>>
		end

	--|--------------------------------------------------------------

	default_colors: ARRAY [EV_COLOR] is
			-- Default colors for chart
		once
			Result :=
				<<
				colors.red,
			colors.navy,
			colors.orange,
			colors.light_green,
			colors.blue,
			colors.yellow,
			colors.magenta,
			colors.green,
			colors.cyan,
			colors.dark_green,
			colors.gold,
			colors.light_sky_blue,
			--	  colors.dark_gray,
			colors.dark_gray,
			--	  colors.light_gray
			colors.light_gray
			>>
		end

	--|--------------------------------------------------------------

	pastel_colors: ARRAY [EV_COLOR] is
		once
			Result :=
				<<
				colors.light_pink,
			colors.light_blue,
			colors.light_goldenrod,
			colors.light_green,
			colors.powder_blue,
			colors.light_yellow,
			colors.peach_puff,
			colors.mint_cream,
			colors.light_cyan,
			colors.dark_green,
			colors.gold,
			colors.light_sky_blue,
			--	  colors.dark_gray,
			colors.dark_gray,
			colors.black,
			colors.white,
			--	  colors.light_gray
			colors.light_gray
			>>
		end

	--|--------------------------------------------------------------

	current_item_color: EV_COLOR is
		do
			Result := private_item_color
			if Result = Void then
				Result := colors.blue
			end
		end

	--|--------------------------------------------------------------

	set_current_item_color (v: EV_COLOR) is
		do
			private_item_color := v
		end

	--|--------------------------------------------------------------

	private_item_color: EV_COLOR

	--|--------------------------------------------------------------

	shadow_color: EV_COLOR is
		do
			Result := private_shadow_color
			if Result = Void then
				Result := colors.dark_gray
			end
		end

	--|--------------------------------------------------------------

	set_shadow_color (v: EV_COLOR) is
		do
			private_shadow_color := v
		end

 --|========================================================================
feature -- Labeling
 --|========================================================================

	draw_labels is
		local
			tt: EV_MODEL_TEXT
			ti: like reference_item
			w: EV_MODEL_WORLD
		do
			w := projector.world
			from items.start
			until items.exhausted
			loop
				ti := items.item
				if ti.drawn then
					tt := item_label (ti)
					ti.set_label (tt)
					if tt /= Void then
						w.extend (tt)
						position_item_label (ti)
					end
				end
				items.forth
			end
		end

	--|--------------------------------------------------------------

	item_label (v: like reference_item): EV_MODEL_TEXT is
		require
			item_exists: v /= Void
		local
			color: EV_COLOR
		do
			if v.label_text /= Void then
				color := v.label_color
				if color = Void then
					color := v.color
				end
				if color = Void then
					color := colors.black
				end

				create Result.make_with_text (v.label_text)
				Result.set_foreground_color (color)
			end
		ensure
			label_for_text: v.label_text /= Void implies Result /= Void
		end

	--|--------------------------------------------------------------

	position_item_label (v: like reference_item) is
		require
			item_exists: v /= Void
		do
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	resizing: BOOLEAN

	on_resize (newx, newy, neww, newh: INTEGER) is
		do
			if not resizing then
				resizing := True
				update_graph
				resizing := False
			end
		end

 --|========================================================================
feature
 --|========================================================================

	selection_proc: PROCEDURE [ANY, TUPLE [AV_GRAPH_ITEM]]
			-- Agent to call when user presses mouse button #1 or
			-- otherwise selects or deselects one or more list items

	set_selection_proc (v: like selection_proc) is
		do
			selection_proc := v
		end

	--|--------------------------------------------------------------

	select_item_by_xy (a_x, a_y, btn: INTEGER; axt, ayt, pr: DOUBLE; sx, sy: INTEGER) is
		local
			ti: like reference_item
		do
			if btn = 1 then
				ti := item_at_position (a_x, a_y)
				if ti /= Void then
					last_selected_item := ti
				end
				if selection_proc /= Void then
					selection_proc.call ([ti])
				end
			end
		end

	--|--------------------------------------------------------------

	item_at_position (a_x, a_y: INTEGER): like reference_item is
		local
			i: INTEGER
			pf: like reference_figure
		do
			from figure_world.start
			until figure_world.exhausted or Result /= Void
			loop
				pf ?= figure_world.item
				if pf /= Void then
					i := i + 1
					if pf.position_on_figure (a_x, a_y) then
						--	  Result := figure_world.item
						Result := items.i_th (i)
					end
				end
				if Result = Void then
					figure_world.forth
				end
			end
		end

	last_selected_item: like reference_item

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	K_I: INTEGER is 1
	K_II: INTEGER is 2
	K_III: INTEGER is 3
	K_IV: INTEGER is 4

	--|--------------------------------------------------------------

	private_shadow_color: EV_COLOR

	private_top_margin: INTEGER
	private_bottom_margin: INTEGER
	private_left_margin: INTEGER
	private_right_margin: INTEGER

	dflt_margin_width: INTEGER is
		do
			Result := 10
		end

	dflt_margin_height: INTEGER is
		do
			Result := 10
		end

	colors: AEL_V_COLORS is
		once
			create Result
		end

end -- class AV_GRAPH_FRAME
