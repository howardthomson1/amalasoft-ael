class AV_GRAPH_ITEM

create
	make

 --|========================================================================
feature {NONE}
 --|========================================================================

	make (v: like obj) is
		do
			obj := v
		end

 --|========================================================================
feature
 --|========================================================================

	obj: ANY

	cached_data: AV_GRAPH_ITEM_DATA

	color: EV_COLOR

	label_color: EV_COLOR

	label_text: STRING

	drawn: BOOLEAN is True

-- figure: EV_CLOSED_FIGURE
	figure: EV_MODEL

	line_width: INTEGER

	label: EV_MODEL_TEXT

 --|========================================================================
feature
 --|========================================================================

	size: DOUBLE is
		do
			Result := private_size
		end

	set_size (v: DOUBLE) is
		do
			private_size := v
		end

	--|--------------------------------------------------------------

	set_cached_data (v: AV_GRAPH_ITEM_DATA) is
		do
			cached_data := v
		end

	--|--------------------------------------------------------------

	set_color (v: EV_COLOR) is
		local
			cf: EV_CLOSED_FIGURE
		do
			color := v
			cf ?= figure
			if cf /= Void and then v /= Void then
				cf.set_foreground_color (v)
				cf.set_background_color (v)
			end
		end

	--|--------------------------------------------------------------

	set_label_color (v: EV_COLOR) is
		do
			label_color := v
		end

	--|--------------------------------------------------------------

	set_label_text (v: STRING) is
		do
			label_text := v
			if label /= Void then
				label.set_text (v)
			end
		end

	--|--------------------------------------------------------------

	set_figure (v: like figure) is
		do
			figure := v
		end

	--|--------------------------------------------------------------

	set_line_width (v: INTEGER) is
		do
			line_width := v
		end

	--|--------------------------------------------------------------

	set_label (v: EV_MODEL_TEXT) is
		do
			label := v
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	private_size: DOUBLE

end -- class AV_GRAPH_ITEM
