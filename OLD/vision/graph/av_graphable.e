class AV_GRAPHABLE

create
	make_with_range

 --|========================================================================
feature {NONE}
 --|========================================================================

	make_with_range (bv, v : REAL) is
		do
			base_value := bv
			value := v
		end

 --|========================================================================
feature
 --|========================================================================

	value : REAL

	integer_value : INTEGER is
		do
			Result := value.rounded
		end

	base_value : REAL
			-- Default is zero

end -- class AV_GRAPHABLE
