class AV_HORIZONTAL_SEGMENT_GRAPH
 -- Horizontal bar, divible into segments
 -- Conceptually a pie-graph unrolled

inherit
  AV_GRAPH_FRAME
   redefine
     extend, update_graph, position_item_label,
     reference_item, reference_figure ,draw_labels,
     dflt_margin_width, dflt_margin_height
   end;

creation
 make, make_with_orientation,
 make_horizontal_left, make_horizontal_right,
 make_vertical_bottom, make_vertical_top,
 default_create

 --|========================================================================
feature {NONE}
 --|========================================================================

 make_with_orientation( v : CHARACTER ) is
  do
    set_orientation( v.lower );
    make;
  end;

 --|------------------------------------------------------------------------

 make_horizontal_left is do make_with_orientation( 'l' ); end;
 make_horizontal_right is do make_with_orientation( 'r' ); end;
 make_vertical_bottom is do make_with_orientation( 'b' ); end;
 make_vertical_top is do make_with_orientation( 't' ); end;

 --|========================================================================
feature
 --|========================================================================

 is_horizontal : BOOLEAN is
  do
    Result := orientation = 'l' or orientation = 'r';
  end;

 --|------------------------------------------------------------------------

 rotate_clockwise is
  -- Re-orient the graph by 90 degrees clockwise
  do
    inspect( orientation )
    when 'b' then
      set_orientation( 'l' );
    when 'l' then
      set_orientation( 't' );
    when 't' then
      set_orientation( 'r' );
    when 'r' then
      set_orientation( 'b' );
    end;
--!    private_left_margin := dflt_margin_width;
--!    private_right_margin := dflt_margin_width;
--!    private_top_margin := dflt_margin_height;
--!    private_bottom_margin := dflt_margin_height;
    remove_base_lines;
    update_graph;
  end;

 --|------------------------------------------------------------------------

 rotate_counterclockwise is
  -- Re-orient the graph by 90 degrees counterclockwise
  do
    inspect( orientation )
    when 'b' then
      set_orientation( 'r' );
    when 'l' then
      set_orientation( 'b' );
    when 't' then
      set_orientation( 'l' );
    when 'r' then
      set_orientation( 't' );
    end;
--!    private_left_margin := dflt_margin_width;
--!    private_right_margin := dflt_margin_width;
--!    private_top_margin := dflt_margin_height;
--!    private_bottom_margin := dflt_margin_height;
    remove_base_lines;
    update_graph;
  end;

 --|------------------------------------------------------------------------

 reference_item : AV_BAR_GRAPH_ITEM;
-- reference_figure : EV_FIGURE_RECTANGLE;
 reference_figure : AV_FIGURE_BAR;

 shadow : EV_FIGURE_RECTANGLE;

 base_line : EV_FIGURE_LINE;
 base_line_shadow1 : EV_FIGURE_LINE;
 base_line_shadow2 : EV_FIGURE_LINE;

 --|========================================================================
feature
 --|========================================================================

 extend( v : like reference_item ) is
  local
    ps : like reference_figure;
  do
    ps := new_figure( v );
    v.set_figure( ps );

    projector.world.extend( ps );
    items.extend( v );
  end;

 --|------------------------------------------------------------------------

 new_figure_points( v : like reference_item ) : ARRAY [ EV_RELATIVE_POINT ] is
  Require
    item_exists: v /= Void;
  local
    pt1, pt2 : EV_RELATIVE_POINT;
    xpos, ypos, bar_length : INTEGER;
  do
    inspect( orientation )
    when 'b' then
      xpos := xorigin + (items.count * (bar_thickness + bar_spacing)) + 10;
      ypos := yorigin - v.size.rounded;
      !! pt1.make_with_position( xpos, ypos );
      !! pt2.make_with_position( xpos + bar_thickness, yorigin );
    when 'l' then
      xpos := xorigin;
--      ypos := margin_height +
      ypos := yterminus + (items.count * (bar_thickness + bar_spacing)) + 5;
      bar_length := v.size.rounded;
      !! pt1.make_with_position( xpos, ypos );
      !! pt2.make_with_position( xpos + bar_length, ypos + bar_thickness );
    when 't' then
      xpos := xorigin + (items.count * (bar_thickness + bar_spacing)) + 10;
      ypos := yorigin;
      !! pt1.make_with_position( xpos, ypos );
      !! pt2.make_with_position( xpos + bar_thickness, ypos + v.size.rounded );
    when 'r' then
      xpos := xorigin;
--      ypos := margin_height +
      ypos := yterminus +
	  (items.count * (bar_thickness + bar_spacing)) + 5;
      bar_length := v.size.rounded;
      !! pt1.make_with_position( xpos, ypos );
      !! pt2.make_with_position( xpos - bar_length, ypos + bar_thickness );
    end;
    Result := << pt1, pt2 >>;
  end;

 --|------------------------------------------------------------------------

 new_figure( v : like reference_item ) : like reference_figure is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
    pts : ARRAY [ EV_RELATIVE_POINT ];
  do
    !! Result;
    --Result.set_is_horizontal( is_horizontal );
    Result.set_graph_orientation( orientation );
    Result.set_line_width( 1 );
    Result.set_background_color( v.color );

    pts := new_figure_points( v );
    pt1 := pts.item( 1 );
    pt2 := pts.item( 2 );

    Result.set_point_a( pt1 );
    Result.set_point_b( pt2 );
  end;

 --|------------------------------------------------------------------------

 update_graph is
  local
    i, lim : INTEGER;
    sl : GRAPHABLE_STATISTIC;
    ti : AV_BAR_GRAPH_ITEM;
    dl : SEQUENCE [ like reference_object ];
    ds : like data_source;
  do
    purge_figures;
    items.wipe_out;
    make_axes;
    set_background_color( chart_background_color );

    ds := data_source;
    if( ds /= Void ) then
      dl ?= ds.linear_representation;
      check
	is_sequence: dl /= Void;
      end;
      if( is_horizontal ) then
	dl := inverted_data_source( dl );
      end;
      lim := dl.count + 1;
      from dl.start;
      until dl.exhausted
      loop
	sl ?= dl.item;
	i := i + 1;
	ti := new_item( sl );
	if( is_horizontal ) then
	  ti.set_color( item_color( lim  - i ) );
	else
	  ti.set_color( item_color( i ) );
	end;
	on_extend( sl, ti, i );
	extend( ti );
	dl.forth;
      end;
    end;
    draw_labels;
    projector.project;
  end;

 --|------------------------------------------------------------------------

 inverted_data_source( ds : SEQUENCE [ like reference_object ] )
				: TWO_WAY_LIST [ like reference_object ] is
  Require
    src_exists: ds /= Void;
  local
    ti : like reference_object;
  do
    !! Result.make;
    from ds.start;
    until ds.exhausted
    loop
      ti := ds.item;
      if( ti /= Void ) then
	Result.put_front( ti );
      end;
      ds.forth;
    end;
  end;

 --|------------------------------------------------------------------------

 on_extend( obj : like reference_object;
	    ti : like reference_item; idx : INTEGER ) is
  Require
    obj_exists: obj /= Void;
    item_exists: ti /= Void;
    valid_index: idx > 0;
  do
    if( on_extend_proc /= Void ) then
      on_extend_proc.call( [ obj, ti, idx ] );
    end;
  end;

 --|------------------------------------------------------------------------

 on_extend_proc : PROCEDURE [ ANY, TUPLE [ like reference_object,
					   like reference_item, INTEGER ] ];

 set_on_extend_proc( v : like on_extend_proc ) is
  do
    on_extend_proc := v;
  end;

 --|------------------------------------------------------------------------

 draw_labels is
  do
  end;

 --|------------------------------------------------------------------------

 x_line : AV_FIGURE_AXIS;
 y_line : AV_FIGURE_AXIS;

 --|------------------------------------------------------------------------

 purge_figures is
  do
    from figure_world.start;
    until figure_world.exhausted
    loop
      if( figure_world.item /= x_line and figure_world.item /= y_line ) then
	figure_world.remove;
      else
	figure_world.forth;
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 make_axes is
  do
    if( has_y_axis and (y_line = Void) ) then
      make_y_axis;
    end;

    if( has_x_axis and (x_line = Void) ) then
      make_x_axis;
    end;
  end;

 --|------------------------------------------------------------------------

 make_x_axis is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
    y0, y1 : INTEGER;
  do
    y0 := yorigin;
    y1 := yterminus;

    !! x_line.make_horizontal;
    x_line.set_line_width( 1 );
    x_line.set_foreground_color( axis_color );

    inspect orientation
    when 'b' then
      x_line.set_hash_alignment( x_hash_alignment );
      !! pt1.make_with_position( left_margin, y0 );
      !! pt2.make_with_position( width - right_margin, y0 );
    when 'l' then
      x_line.set_hash_alignment( x_hash_alignment );
      !! pt1.make_with_position( left_margin, y0 );
      !! pt2.make_with_position( width - right_margin, y0 );
    when 't' then
      x_line.set_hash_alignment( inverted_hash_alignment( True ) );
      !! pt1.make_with_position( left_margin, y0 );
      !! pt2.make_with_position( width - right_margin, y0 );
    when 'r' then
      x_line.set_hash_alignment( x_hash_alignment );
      !! pt1.make_with_position( left_margin, y0 );
      !! pt2.make_with_position( width - right_margin, y0 );
    end;

    x_line.set_point_a( pt1 );
    x_line.set_point_b( pt2 );
    figure_world.extend( x_line );
  end;

 --|------------------------------------------------------------------------

 make_y_axis is
  local
    bl : like y_line;
    pt1, pt2 : EV_RELATIVE_POINT;
    xpos : INTEGER;
  do
    !! y_line.make_vertical;
    y_line.set_hash_alignment( y_hash_alignment );
    bl := y_line;
    bl.set_line_width( 1 );
    bl.set_foreground_color( axis_color );

    inspect( orientation )
    when 'b', 'l' then
      xpos := xorigin;
      y_line.set_hash_alignment( y_hash_alignment );
      !! pt1.make_with_position( xpos, yterminus );
      !! pt2.make_with_position( xpos, yorigin );
    when 't' then
      xpos := xorigin;
      y_line.set_hash_alignment( y_hash_alignment );
      !! pt1.make_with_position( xpos, yorigin );
      !! pt2.make_with_position( xpos, yterminus );
    when 'r' then
      xpos := xorigin;
      y_line.set_hash_alignment( inverted_hash_alignment( False ) );
      !! pt1.make_with_position( xpos, yterminus );
      !! pt2.make_with_position( xpos, yorigin );
    end;
    bl.set_point_a( pt1 );
    bl.set_point_b( pt2 );
    figure_world.extend( bl );
  end;

 --|------------------------------------------------------------------------

 redraw_base_lines is
  do
    remove_base_lines;
    make_axes;
    projector.project;
  end;

 remove_base_lines is
  do
    figure_world.prune_all( x_line );
    x_line := Void;
    figure_world.prune_all( y_line );
    y_line := Void;
  end;

 --|------------------------------------------------------------------------

 make_base_line is
  local
    w : EV_FIGURE_WORLD;
    bl : EV_FIGURE_LINE;
    pt1, pt2 : EV_RELATIVE_POINT;
    xpos, ypos, y0 : INTEGER;
  do
    w := projector.world;
    y0 := yorigin;

    inspect orientation
    when 'l', 'r' then
      xpos := left_margin;
      ypos := top_margin;
      !! pt1.make_with_position( left_margin, ypos );
      !! pt2.make_with_position( left_margin, y0 - 2);
    when 'b', 't' then
      xpos := left_margin;
      ypos := minimum_height - bottom_margin;
      !! pt1.make_with_position( left_margin, ypos - 2 );
      !! pt2.make_with_position( (width - right_margin) + 2, ypos  - 2);
    end;

    !! base_line_shadow2;
    bl := base_line_shadow2;
    bl.set_line_width( 1 );
    bl.set_foreground_color( colors.K_color_dark_gray );
    bl.set_point_a( pt1 );
    bl.set_point_b( pt2 );
    figure_world.extend( bl );

    if( is_horizontal ) then
      !! pt1.make_with_position( left_margin + 1, ypos + 1 );
      !! pt2.make_with_position( left_margin + 1, y0 - 1);
    else
      !! pt1.make_with_position( left_margin - 1, ypos - 1 );
      !! pt2.make_with_position( (width - right_margin) + 1, ypos  - 1);
    end;
    !! base_line_shadow1;
    bl := base_line_shadow1;
    bl.set_line_width( 1 );
    bl.set_foreground_color( colors.K_color_dark_gray );
    bl.set_point_a( pt1 );
    bl.set_point_b( pt2 );
    figure_world.extend( bl );

    if( is_horizontal ) then
      !! pt1.make_with_position( left_margin + 2, ypos + 2 );
      !! pt2.make_with_position( left_margin + 2, y0 );
    else
      !! pt1.make_with_position( left_margin - 2, ypos );
      !! pt2.make_with_position( width - right_margin, ypos );
    end;
    !! base_line;
    bl := base_line;
    bl.set_line_width( 1 );
    bl.set_foreground_color( colors.K_color_navy );
    bl.set_point_a( pt1 );
    bl.set_point_b( pt2 );
    figure_world.extend( bl );
  end;

 --|========================================================================
feature -- Standard Reference Points (May be cacheable)
 --|========================================================================

 yorigin : INTEGER is
  do
    inspect orientation
    when 't' then
      Result := top_margin;
    else
      Result := minimum_height - bottom_margin;
    end;
  end;

 yterminus : INTEGER is
  do
    inspect orientation
    when 't' then
      Result := minimum_height - bottom_margin;
    else
      Result := top_margin;
    end;
  end;

 --|------------------------------------------------------------------------

 xorigin : INTEGER is
  do
    inspect orientation
    when 'r' then
      Result := minimum_width - right_margin;
    else
      Result := left_margin;
    end;
  end;

 xterminus : INTEGER is
  do
    inspect orientation
    when 'r' then
      Result := left_margin;
    else
      Result := minimum_width - right_margin;
    end;
  end;

 --|------------------------------------------------------------------------

--! base_point : EV_RELATIVE_POINT is
--!  -- The absolute anchor, relative to which other points are reckoned.
--!  do
--!    !! Result.make_with_position( margin_width, margin_height );
--!  end;

 --|------------------------------------------------------------------------

--! shadow_point_a : EV_RELATIVE_POINT is
--!  do
--!    !! Result.make_with_origin_and_position( base_point, 1, 1 );
--!  end;

 --|------------------------------------------------------------------------

--! shadow_point_b : EV_RELATIVE_POINT is
--!  local
--!    tx, ty : INTEGER;
--!  do
--!    tx := width - (2 * margin_width);
--!    ty := height - (2 * margin_height);
--!    !! Result.make_with_origin_and_position( base_point, tx, ty );
--!  end;

 --|------------------------------------------------------------------------

--! bg_point_a : EV_RELATIVE_POINT is
--!  do
--!    !! Result.make_with_origin_and_position( base_point, 0, 0 );
--!  end;

 --|------------------------------------------------------------------------

--! bg_point_b : EV_RELATIVE_POINT is
--!  local
--!    tx, ty : INTEGER;
--!  do
--!    tx := width - ((2 * margin_width) + shadow_thickness - 1);
--!    ty := height - ((2 * margin_height) + shadow_thickness + 1);
--!    !! Result.make_with_origin_and_position( base_point, tx, ty );
--!  end;

 --|========================================================================
feature -- Labeling
 --|========================================================================

 using_approximate_label_position : BOOLEAN;

 --|------------------------------------------------------------------------

 set_using_approximate_label_position( tf : BOOLEAN ) is
  do
    using_approximate_label_position := tf;
  end;

 --|------------------------------------------------------------------------

 position_item_label( v : like reference_item ) is
  do
    if( v.label /= Void ) then
      position_label( v.label, v.size, v.cached_data.offset, 
		      v.cached_data.octant );
    end;
  end;

 --|------------------------------------------------------------------------

 position_label( tf : EV_FIGURE_TEXT; sz, start_angle : DOUBLE; oct : INTEGER )
  is
  local
--!    bisector : DOUBLE;
--!    tx, ty : INTEGER;
--!    x_perim, y_perim : INTEGER;
--!    x_radius, y_radius : INTEGER;
--!    rect_origin, text_origin : EV_RELATIVE_POINT;
--!    bcos, bsin : DOUBLE;
--!    inv_rad, radius : DOUBLE;
  do
--!    x_radius := (width - ((2 * margin_width) + shadow_thickness - 1)) //  2;
--!    y_radius := (height - ((2 * margin_height) + shadow_thickness + 1)) // 2;
--!
--!    bisector := start_angle + ( sz / 2.0 );
--!    bsin := sine( bisector );
--!    bcos := cosine( bisector );
--!
--!    if( using_approximate_label_position ) then
--!      -- This approximation converges to the exact answer as the 
--!      -- eccentricity converges to zero.  The points it produces are 
--!      -- guaranteed to be on the ellipse, but will be shifted in the 
--!      -- direction of the longer axis.
--!      -- Should be used only when the graph is guaranteed to be       
--!      -- nearly circular.
--!      -- Should definitely NOT be used if the graph can be resized 
--!      -- arbitrarily.
--!
--!      x_perim := abs( (bcos * x_radius).truncated_to_integer );
--!      y_perim := abs( (bsin * y_radius).truncated_to_integer );
--!    else
--!      inv_rad := ( (bcos * bcos) / (x_radius * x_radius) ) + 
--!	  ( (bsin * bsin) / (y_radius * y_radius) );
--!      radius := sqrt( 1 / inv_rad );
--!
--!      x_perim := abs( (radius * bcos).truncated_to_integer );
--!      y_perim := abs( (radius * bsin).truncated_to_integer );
--!    end;
--!
--!    inspect( oct )
--!    when K_I, K_II, K_III, K_IV then
--!      -- Above X axis, shift up by text height
--!      ty := y_radius - (y_perim + tf.height);
--!    else
--!      ty := y_radius + y_perim;
--!    end;
--!
--!    inspect( oct )
--!    when K_III, K_IV, K_V, K_VI then
--!      -- To left of Y axis, shift left by text width
--!      tx := x_radius - (x_perim + tf.width);
--!    else
--!      tx := x_radius + x_perim;
--!    end;
--!
--!    !! rect_origin.make_with_position( margin_width, margin_height );
--!    !! text_origin.make_with_origin_and_position( rect_origin, tx, ty );
--!    tf.set_point( text_origin );
  end;

 max_item_length : INTEGER is
  -- Largest item length, in Pixels
  do
--    Result := 200;
    if( is_horizontal ) then
      Result := minimum_width - (left_margin + right_margin);
    else
      Result := minimum_height - (top_margin + bottom_margin);
    end;
  end;

 --|------------------------------------------------------------------------

 private_bar_thickness : INTEGER;
 private_bar_spacing : INTEGER;
 private_orientation : CHARACTER;
 private_axis_config : STRING;

 --|------------------------------------------------------------------------

 set_bar_spacing( v : INTEGER ) is
  do
    private_bar_spacing := v;
  end;

 bar_spacing : INTEGER is
  do
    Result := private_bar_spacing;
    if( Result = 0 ) then
      Result := K_dflt_bar_spacing;
    end;
  end;

 --|------------------------------------------------------------------------

 set_orientation( v : CHARACTER ) is
  -- Single character, 'b', 'l', 't', 'r'
  -- Denoting: bottom, left, top and right, respectively
  do
    private_orientation := v;
  end;

 orientation : CHARACTER is
  do
    Result := private_orientation;
    if( Result = '%U' ) then
      Result := Kc_dflt_orientation;
    end;
  end;

 --|------------------------------------------------------------------------

 set_bar_thickness( v : INTEGER ) is
  do
    private_bar_thickness := v;
  end;

 bar_thickness : INTEGER is
  do
    Result := private_bar_thickness;
    if( Result = 0 ) then
      Result := K_dflt_bar_thickness;
    end;
  end;

 --|------------------------------------------------------------------------

 set_axis_configuration( xf, yf : BOOLEAN ) is
  local
    cfg : STRING;
  do
    !! cfg.make( 2 );
    if( xf ) then
      cfg.extend( 'x' );
    end;
    if( yf ) then
      cfg.extend( 'y' );
    end;
    set_axis_configuration_str( cfg );
  end;

 set_axis_configuration_str( v : STRING ) is
  do
    private_axis_config := clone( v );
  end;

 axis_config : STRING is
  do
    Result := private_axis_config;
    if( Result = Void ) then
      Result := Ks_dflt_axis_config;
    end;
  end;

 --|------------------------------------------------------------------------

 toggle_x_axis is
  do
    set_axis_configuration( not has_x_axis, has_y_axis );
    redraw_base_lines;
  end;

 toggle_y_axis is
  do
    set_axis_configuration( has_x_axis, not has_y_axis );
    redraw_base_lines;
  end;

 --|------------------------------------------------------------------------

 has_x_axis : BOOLEAN is
  do
    Result := axis_config.has( 'x' );
  end;

 has_y_axis : BOOLEAN is
  do
    Result := axis_config.has( 'y' );
  end;

 --|------------------------------------------------------------------------

 align_hash_marks_inside is
  do
    x_hash_alignment := K_hash_align_inside;
    y_hash_alignment := K_hash_align_inside;
    redraw_base_lines;
  end;

 align_hash_marks_outside is
  do
    x_hash_alignment := K_hash_align_outside;
    y_hash_alignment := K_hash_align_outside;
    redraw_base_lines;
  end;

 align_hash_marks_spanning is
  do
    x_hash_alignment := K_hash_align_spanning;
    y_hash_alignment := K_hash_align_spanning;
    redraw_base_lines;
  end;

 hide_hash_marks is
  do
    x_hash_alignment := K_hash_align_none;
    y_hash_alignment := K_hash_align_none;
    redraw_base_lines;
  end;

 --|------------------------------------------------------------------------

 align_x_hash_marks_inside is
  do
    x_hash_alignment := K_hash_align_inside;
    redraw_base_lines;
  end;

 align_x_hash_marks_outside is
  do
    x_hash_alignment := K_hash_align_outside;
    redraw_base_lines;
  end;

 align_x_hash_marks_spanning is
  do
    x_hash_alignment := K_hash_align_spanning;
    redraw_base_lines;
  end;

 hide_x_hash_marks is
  do
    x_hash_alignment := K_hash_align_none;
    redraw_base_lines;
  end;

 x_hash_alignment : INTEGER;

 --|------------------------------------------------------------------------

 align_y_hash_marks_inside is
  do
    y_hash_alignment := K_hash_align_inside;
    redraw_base_lines;
  end;

 align_y_hash_marks_outside is
  do
    y_hash_alignment := K_hash_align_outside;
    redraw_base_lines;
  end;

 align_y_hash_marks_spanning is
  do
    y_hash_alignment := K_hash_align_spanning;
    redraw_base_lines;
  end;

 hide_y_hash_marks is
  do
    y_hash_alignment := K_hash_align_none;
    redraw_base_lines;
  end;

 y_hash_alignment : INTEGER;

 --|------------------------------------------------------------------------

 inverted_hash_alignment( is_x : BOOLEAN ) : INTEGER is
  local
    ta : INTEGER;
  do
    if( is_x ) then
      ta := x_hash_alignment;
    else
      ta := y_hash_alignment;
    end;

    inspect ta
    when K_hash_align_outside then
      Result := K_hash_align_inside;
    when K_hash_align_inside then
      Result := K_hash_align_outside;
    else
      Result := ta;
    end;
  end;

 --|------------------------------------------------------------------------

 private_axis_color : EV_COLOR;

 axis_color : EV_COLOR is
  do
    Result := private_axis_color;
    if( Result = Void ) then
      Result := colors.K_color_black;
    end;
  end;

 set_axis_color( v : EV_COLOR ) is
  do
    private_axis_color := v;
    if( x_line /= Void ) then
      x_line.set_foreground_color( axis_color );
    end;
    if( y_line /= Void ) then
      y_line.set_foreground_color( axis_color );
    end;
    if( x_line /= Void and y_line /= Void ) then
      redraw_base_lines;
    end;
  end;

 --|------------------------------------------------------------------------

 private_chart_background_color : EV_COLOR;

 chart_background_color : EV_COLOR is
  do
    Result := private_chart_background_color;
    if( Result = Void ) then
      Result := colors.K_color_light_gray;
    end;
  end;

 set_chart_background_color( v : EV_COLOR ) is
  do
    private_chart_background_color := v;
    set_background_color( chart_background_color );
  end;

 --|------------------------------------------------------------------------

 Ks_dflt_axis_config : STRING is "xy";
-- Kc_dflt_orientation : CHARACTER is 'b';
 Kc_dflt_orientation : CHARACTER is
  do
    Result := 'b';
  end;

 K_hash_align_none : INTEGER is -1;
 K_hash_align_outside : INTEGER is 0;
 K_hash_align_inside : INTEGER is 1;
 K_hash_align_spanning : INTEGER is 2;

 K_dflt_bar_thickness : INTEGER is 15;
 K_dflt_bar_spacing : INTEGER is
  do
    if( is_horizontal ) then
      Result := 15;
    else
      Result := 20;
    end;
  end;

 dflt_margin_width : INTEGER is
  do
    inspect( orientation )
    when 'b', 'l', 't', 'r' then
      Result := 20;
    end;
  end;

 dflt_margin_height : INTEGER is
  do
    inspect( orientation )
    when 'b', 'r' then
      Result := 20;
    when 'l', 't' then
      Result := 10;
    end;
  end;

------------------------------------------------------------------------
invariant

end -- class AV_HORIZONTAL_SEGMENT_GRAPH
