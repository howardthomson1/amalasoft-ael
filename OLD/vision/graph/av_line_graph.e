class AV_LINE_GRAPH

inherit
	AV_GRAPH_FRAME
		redefine
			reference_object, data_source,
			extend, update_graph, position_item_label,
			new_item, reference_item, reference_figure, create_implementation,
			update_graph_with_data
		end

create
	make, default_create

 --|========================================================================
feature
 --|========================================================================

	data_source: ARRAY [CONTAINER [like reference_object]] is
		do
			Result := private_data_source
		end

	reference_object: AV_GRAPHABLE
	reference_item: AV_LINE_GRAPH_ITEM
	reference_figure: EV_MODEL_DOT

	bg_figure: EV_MODEL_RECTANGLE
	shadow: EV_MODEL_RECTANGLE

	x_line: AV_FIGURE_AXIS
	y_line: AV_FIGURE_AXIS

	connectors: TWO_WAY_LIST [EV_MODEL_LINE]

	min_value: INTEGER is
		do
			Result := 0
		end

	max_value: INTEGER is
		do
			Result := 100
		end

	max_item_length: INTEGER is
			-- Largest item length, in Pixels
		do
			Result := minimum_height - (top_margin + bottom_margin)
		end

	max_connector_length: INTEGER is
			-- Largest item length, in Pixels
		do
			Result := minimum_width - (left_margin + right_margin)
		end

 --|========================================================================
feature
 --|========================================================================

	extend (v: like reference_item) is
		local
			ps: like reference_figure
			prev_item: like reference_item
			--pt: EV_RELATIVE_POINT
		do
			if not items.is_empty then
				prev_item := items.last
			end

			ps := new_figure (v)
			v.set_figure (ps)

			projector.world.extend (ps)
			items.extend (v)
		end

	--|--------------------------------------------------------------

	add_connectors is
		local
			ps: like reference_figure
			ti, prev_item: like reference_item
			tl: EV_MODEL_LINE
			last_x: INTEGER
		do
			data_source_index := 1
			if (items.count > 1) then
				from items.start
				until items.exhausted
				loop
					ti := items.item
					if ok_to_show_lines and (prev_item /= Void) then
						ps := ti.figure
						if ps.point.x > last_x then
							create tl
							tl.set_line_width (line_thickness)
							tl.set_foreground_color (line_color)
							tl.set_point_a_position (prev_item.figure.point_x, prev_item.figure.point_y)
							tl.set_point_b_position (ps.point_x, ps.point_y)
							figure_world.extend (tl)
							connectors.extend (tl)
						else
							data_source_index := data_source_index + 1
						end
					end
					prev_item := ti
					last_x := ti.figure.point.x
					items.forth
				end
			end
		end

	--|--------------------------------------------------------------

	new_figure (v: like reference_item): like reference_figure is
		local
			pt: EV_RELATIVE_POINT
			xpos, ypos, yorigin: INTEGER
		do
			xpos := left_margin + (data_item_count * dot_spacing) + 10
			yorigin := minimum_height - bottom_margin
			ypos := yorigin - v.size.rounded

			create pt.make_with_position (xpos, ypos)

			create Result.make_with_point (create {EV_COORDINATE}.make (pt.x, pt.y))
			Result.set_line_width (dot_width)
			Result.set_foreground_color (dot_color)
			data_item_count := data_item_count + 1
		end

	--|--------------------------------------------------------------

	update_graph is
		local
			ci: INTEGER
			i, lim: INTEGER
			sl: like reference_object
			ti: AV_LINE_GRAPH_ITEM
			dl: LINEAR [ANY]
			ds: CONTAINER [like reference_object]
		do
			purge_figures
			items.wipe_out
			connectors.wipe_out

			make_base_lines

			lim := data_source.upper
			from i := data_source.lower
			until i > lim
			loop
				data_item_count := 0
				ds := data_source.item (i)
				if ds /= Void then
					dl := ds.linear_representation
					from dl.start
					ci := 1
					until dl.exhausted
					loop
						sl ?= dl.item
						ti := new_item (sl)
						
						ci := ci + 1
						ti.set_color (item_color (ci))
						extend (ti)
						dl.forth
					end
				end
				add_connectors
				draw_labels
				i := i + 1
			end
			projector.project
		end

	--|--------------------------------------------------------------

	new_item (v: like reference_object): like reference_item is
		local
			dd: AV_LINE_GRAPH_ITEM_DATA
			sz_d, offset_d: DOUBLE
		do
			create Result.make (v, Current)

			sz_d :=  (v.value / max_value) *  (1.0 * max_item_length)

			create dd.make (offset_d, 0)
			Result.set_size (sz_d)
			Result.set_cached_data (dd)
		end

	--|--------------------------------------------------------------

	purge_figures is
		do
			from figure_world.start
			until figure_world.exhausted
			loop
				if figure_world.item /= x_line and figure_world.item /= y_line then
					figure_world.remove
				else
					figure_world.forth
				end
			end
		end

	--|--------------------------------------------------------------

	grid_visible: BOOLEAN

	toggle_grid is
		do
			if grid_visible then
				figure_world.hide_grid
			else
				figure_world.enable_grid
				figure_world.show_grid
			end
			projector.full_project
			grid_visible := not grid_visible
		end

	--|--------------------------------------------------------------

	toggle_connectors is
		do
			if ok_to_show_lines then
				show_connectors
			else
				hide_connectors
			end
		end

	hide_connectors is
		do
			purge_connectors
			ok_to_show_lines := False
			projector.project
		end

	show_connectors is
		do
			ok_to_show_lines := True
			update_graph
		end

	--|--------------------------------------------------------------

	purge_connectors is
		do
			if not connectors.is_empty then
				from connectors.start
				until connectors.exhausted or connectors.is_empty
				loop
					figure_world.prune_all (connectors.item)
					connectors.remove
				end
			end
		end

	--|--------------------------------------------------------------

	make_base_lines is
		local
			w: EV_MODEL_WORLD
			bl: like x_line
			pt: EV_RELATIVE_POINT
			xpos, ypos: INTEGER
		do
			if y_line = Void then
				w := projector.world

				xpos := left_margin
				ypos := minimum_height - bottom_margin

				create pt.make_with_position (left_margin, top_margin)
				create y_line.make_vertical
				y_line.set_hash_alignment (hash_alignment)
				bl := y_line
				bl.set_line_width (1)
				--      bl.set_foreground_color (colors.navy)
				bl.set_foreground_color (axis_color)
				bl.set_point_a (pt)
				create pt.make_with_position (left_margin, ypos)
				bl.set_point_b (pt)
				figure_world.extend (bl)

				create pt.make_with_position (left_margin, ypos)
				create x_line.make_horizontal
				x_line.set_hash_alignment (hash_alignment)
				bl := x_line
				bl.set_line_width (1)
				--      bl.set_foreground_color (colors.navy)
				bl.set_foreground_color (axis_color)
				bl.set_point_a (pt)
				create pt.make_with_position (width - right_margin, ypos)
				bl.set_point_b (pt)
				figure_world.extend (bl)
			end
		end

	--|--------------------------------------------------------------

	redraw_base_lines is
		do
			figure_world.prune_all (x_line)
			x_line := Void
			figure_world.prune_all (y_line)
			y_line := Void
			make_base_lines
			projector.project
		end

 --|========================================================================
feature -- Labeling
 --|========================================================================

	using_approximate_label_position: BOOLEAN

	--|--------------------------------------------------------------

	set_using_approximate_label_position (tf: BOOLEAN) is
		do
			using_approximate_label_position := tf
		end

	--|--------------------------------------------------------------

	position_item_label (v: like reference_item) is
		do
			if v.label /= Void then
				position_label (v.label, v.size, v.cached_data.offset, 
		      v.cached_data.octant)
			end
		end

	--|--------------------------------------------------------------

	position_label (tf: EV_MODEL_TEXT; sz, start_angle: DOUBLE; oct: INTEGER) is
		do
		end

	--|--------------------------------------------------------------

	ok_to_show_lines: BOOLEAN

	private_dot_width: INTEGER
	private_dot_spacing: INTEGER
	private_dot_color: EV_COLOR

	private_data_source: like data_source

	--|--------------------------------------------------------------

	set_dot_spacing (v: INTEGER) is
		do
			private_dot_spacing := v
		end

	dot_spacing: INTEGER is
		do
			Result := private_dot_spacing
			if Result = 0 then
				Result := K_dflt_dot_spacing
			end
		end

	--|--------------------------------------------------------------

	set_dot_color (v: EV_COLOR) is
		do
			private_dot_color := v
		end

	dot_color: EV_COLOR is
		do
			Result := private_dot_color
			if Result = Void then
				Result := colors.black
			end
		end

	--|--------------------------------------------------------------

	set_dot_width (v: INTEGER) is
		do
			private_dot_width := v
		end

	dot_width: INTEGER is
		do
			Result := private_dot_width
			if Result = 0 then
				Result := K_dflt_dot_width
			end
		end

	K_dflt_dot_width: INTEGER is 5
	K_dflt_dot_spacing: INTEGER is 30

	--|--------------------------------------------------------------

	align_hash_marks_inside is
		do
			hash_alignment := K_hash_align_inside
			redraw_base_lines
		end

	align_hash_marks_outside is
		do
			hash_alignment := K_hash_align_outside
			redraw_base_lines
		end

	align_hash_marks_spanning is
		do
			hash_alignment := K_hash_align_spanning
			redraw_base_lines
		end

	hide_hash_marks is
		do
			hash_alignment := K_hash_align_none
			redraw_base_lines
		end

	hash_alignment: INTEGER

	--|--------------------------------------------------------------

	private_line_thickness: INTEGER

	line_thickness: INTEGER is
		do
			Result := private_line_thickness
			if Result = 0 then
				Result := 1
			end
		end

	set_line_thickness (v: INTEGER) is
		do
			private_line_thickness := v
		end

	--|--------------------------------------------------------------

	private_line_color: EV_COLOR

	line_color: EV_COLOR is
		do
			Result := private_line_color
			if Result = Void then
				Result := colors.red
			end
		end

	set_line_color (v: EV_COLOR) is
		do
			private_line_color := v
		end

	--|--------------------------------------------------------------

	private_axis_color: EV_COLOR

	axis_color: EV_COLOR is
		do
			Result := private_axis_color
			if Result = Void then
				Result := colors.black
			end
		end

	set_axis_color (v: EV_COLOR) is
		do
			private_axis_color := v
			if x_line /= Void then
				x_line.set_foreground_color (axis_color)
			end
			if y_line /= Void then
				y_line.set_foreground_color (axis_color)
			end
			if x_line /= Void and y_line /= Void then
				redraw_base_lines
			end
		end

	--|--------------------------------------------------------------

	base_figure_color: EV_COLOR is
		do
			Result := private_base_figure_color
			if Result = Void then
				Result := colors.white
			end
		end

	--|--------------------------------------------------------------

	set_base_figure_color (v: EV_COLOR) is
		do
			private_base_figure_color := v
		end

	--|--------------------------------------------------------------

	shadow_figure_color: EV_COLOR is
		do
			Result := private_shadow_figure_color
			if Result = Void then
				Result := colors.white
			end
		end

	--|--------------------------------------------------------------

	set_shadow_figure_color (v: EV_COLOR) is
		do
			private_shadow_figure_color := v
		end

	update_graph_with_data (v: like data_source) is
		do
			private_data_source := v
			update_graph
		end

	set_data_series (sa: ARRAY [ARRAY [REAL]]) is
		local
			ts: like data_source
			ti: like reference_object
			i, idx, j: INTEGER
			ra: ARRAY [REAL]
			ta: LINKED_LIST [like reference_object]
		do
			create ts.make (1, sa.count)
			from
				i := sa.lower
				idx := 1
			until i > sa.upper
			loop
				ra := sa.item (i)
				create ta.make
				ts.put (ta, idx)
				from j := ra.lower
				until j > ra.upper
				loop
					create ti.make_with_range (0.0, ra.item (j))
					ta.extend (ti)
					j := j + 1
				end
				i := i + 1
				idx := idx + 1
			end
			update_graph_with_data (ts)
		end

	--|--------------------------------------------------------------

	data_item_count: INTEGER
	data_source_index: INTEGER

	private_base_figure_color: EV_COLOR
	private_shadow_figure_color: EV_COLOR

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	create_implementation is
		do
			create connectors.make
			Precursor
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_hash_align_none: INTEGER is -1
	K_hash_align_outside: INTEGER is 0
	K_hash_align_inside: INTEGER is 1
	K_hash_align_spanning: INTEGER is 2

end -- class AV_LINE_GRAPH
