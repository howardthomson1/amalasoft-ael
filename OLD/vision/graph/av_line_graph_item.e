class AV_LINE_GRAPH_ITEM

inherit
	AV_GRAPH_ITEM
		rename
			make as apg_make
		redefine
			obj, cached_data, figure
		end

create
	make

 --|========================================================================
feature {NONE}
 --|========================================================================

 make (v : like obj; gr : like graph) is
  do
    apg_make (v)
    graph := gr
  end

 graph : AV_LINE_GRAPH
 obj : AV_GRAPHABLE

 --|========================================================================
feature
 --|========================================================================

 cached_data : AV_LINE_GRAPH_ITEM_DATA
 figure : EV_MODEL_DOT

end -- class AV_LINE_GRAPH_ITEM
