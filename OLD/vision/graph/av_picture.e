class AV_PICTURE

inherit
  AV_BULLETIN
   rename
     set_background_pixmap as bb_set_bg_pixmap,
     background_pixmap as pixmap
   redefine
     enable_sensitive, disable_sensitive, set_callbacks,
     on_key_press
   end;

creation
 default_create, make_with_pixmap, make_with_pixmap_list

 --|========================================================================
feature {NONE}
 --|========================================================================

 make_with_pixmap( v : like sensitive_pixmap ) is
  do
    default_create;
    set_pixmap( v );
    prepare;
  end;

 --|------------------------------------------------------------------------

 make_with_pixmap_list( v : CONTAINER [ like sensitive_pixmap ] ) is
  Require
    arg_exists: v /= Void;
  local
    ll : LINKED_LIST [ like sensitive_pixmap ];
  do
    default_create;
    if( not v.is_empty ) then
      !! ll.make;
      ll.fill( v );
      set_pixmap( ll.first );
      if( ll.count > 1 ) then
	set_insensitive_pixmap( ll.i_th( 2 ) );
      end;
    end;
    prepare;
  end;

 --|------------------------------------------------------------------------

 set_callbacks is
  do
    Precursor;

    pointer_button_press_actions.extend( agent on_button_down );
    pointer_button_release_actions.extend( agent on_button_up );
    pointer_enter_actions.extend( agent on_mouse_enter );
    pointer_leave_actions.extend( agent on_mouse_leave );
    pointer_motion_actions.extend( agent on_mouse_move );
--!    key_press_actions.extend( agent on_key_press );
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 button1_is_down : BOOLEAN;

 --|------------------------------------------------------------------------

 on_button_down( ax, ay, btn : INTEGER;
		 xt, yt, p : DOUBLE; sx, sy : INTEGER ) is
  do
    if( btn = 1 ) then
      button1_is_down := True;
    else
      button1_is_down := False;
    end;

    if( selection_proc /= Void and btn = 1 ) then
      -- ignore
    elseif( button_down_proc /= Void ) then
      button_down_proc.call( [ Current, btn, ax, ay ] );
    end;
  end;

 --|------------------------------------------------------------------------

 on_button_up( ax, ay, btn : INTEGER;
		 xt, yt, p : DOUBLE; sx, sy : INTEGER ) is
  local
    did_select : BOOLEAN;
  do
    if( btn = 1 ) then
      if( button1_is_down ) then
	if( selection_proc /= Void ) then
	  selection_proc.call( [ Current ] );
	  did_select := True;
	end;
      end;
    elseif( btn = 3 ) then
      popup_rtb_menu;
    end;
    button1_is_down := False;

    if( (not did_select) and (button_up_proc /= Void) ) then
      button_up_proc.call( [ Current, btn, ax, ay ] );
    end;
  end;

 --|------------------------------------------------------------------------

 popup_rtb_menu is
  do
  end;

 --|------------------------------------------------------------------------
 --|------------------------------------------------------------------------

 on_mouse_enter is
  do
    --button1_is_down := False;
    if( mouse_enter_proc /= Void ) then
      mouse_enter_proc.call(  [ Current ] );
    end;
  end;

 --|------------------------------------------------------------------------

 on_mouse_leave is
  do
    --button1_is_down := False;
    if( mouse_leave_proc /= Void ) then
      mouse_leave_proc.call( [ Current ] );
    end;
  end;

 --|------------------------------------------------------------------------

 on_mouse_move( ax, ay : INTEGER;
		dx, dy, dp : DOUBLE; sx, sy : INTEGER ) is
  do
    if( mouse_motion_proc /= Void ) then
      mouse_motion_proc.call(  [ Current, ax, ay ] );
    end;
  end;

 --|------------------------------------------------------------------------
 --|------------------------------------------------------------------------

 on_key_press( key : EV_KEY ) is
  do
    button1_is_down := False;
    if( key_press_proc /= Void ) then
      key_press_proc.call(  [ Current, key ] );
    end;
  end;

 --|========================================================================
feature
 --|========================================================================

 selection_proc : PROCEDURE [ ANY, TUPLE [ like Current ] ];

 set_selection_proc( v : like selection_proc ) is
  do
    selection_proc := v;
  end;

 --|------------------------------------------------------------------------

 button_down_proc : PROCEDURE [ ANY, TUPLE
				[ like Current, INTEGER, INTEGER, INTEGER ] ];

 set_button_down_proc( v : like button_down_proc ) is
  do
    button_down_proc := v;
  end;

 --|------------------------------------------------------------------------

 button_up_proc : PROCEDURE [ ANY, TUPLE
			      [ like Current, INTEGER, INTEGER, INTEGER ] ];

 set_button_up_proc( v : like button_up_proc ) is
  do
    button_up_proc := v;
  end;

 --|------------------------------------------------------------------------

 mouse_enter_proc : PROCEDURE [ ANY, TUPLE [ like Current ] ];

 set_mouse_enter_proc( v : like mouse_enter_proc ) is
  do
    mouse_enter_proc := v;
  end;

 --|------------------------------------------------------------------------

 mouse_leave_proc : PROCEDURE [ ANY, TUPLE [ like Current ] ];

 set_mouse_leave_proc( v : like mouse_leave_proc ) is
  do
    mouse_leave_proc := v;
  end;

 --|------------------------------------------------------------------------

 mouse_motion_proc : PROCEDURE [ ANY,
				 TUPLE [ like Current, INTEGER, INTEGER ] ];

 set_mouse_motion_proc( v : like mouse_motion_proc ) is
  do
    mouse_motion_proc := v;
  end;

 --|------------------------------------------------------------------------

 key_press_proc : PROCEDURE [ ANY,
			      TUPLE [ like Current, EV_KEY ] ];

 set_key_press_proc( v : like key_press_proc ) is
  do
    key_press_proc := v;
  end;

 --|========================================================================
feature
 --|========================================================================

 disable_pointer_button_press_actions is
  -- Disable all pointer_button_press actions for this picture
  do
    pointer_button_press_actions.wipe_out;
  end;

 disable_pointer_button_release_actions is
  -- Disable all pointer_button_release actions for this picture
  do
    pointer_button_release_actions.wipe_out;
  end;

 disable_pointer_enter_actions is
  -- Disable all pointer_enter actions for this picture
  do
    pointer_enter_actions.wipe_out;
  end;

 disable_pointer_leave_actions is
  -- Disable all pointer_leave actions for this picture
  do
    pointer_leave_actions.wipe_out;
  end;

 disable_pointer_motion_actions is
  -- Disable all pointer_motion actions for this picture
  do
    pointer_motion_actions.wipe_out;
  end;

 --|========================================================================
feature
 --|========================================================================

 set_pixmap( v : like sensitive_pixmap ) is
  do
    private_sensitive_pixmap := v;
    bb_set_bg_pixmap( v );
    if( v /= Void ) then
      set_minimum_size( v.width, v.height );
    end;
  end;

 set_insensitive_pixmap( v : like sensitive_pixmap ) is
  do
    private_insensitive_pixmap := v;
  end;

 --|------------------------------------------------------------------------

 insensitive_pixmap : like sensitive_pixmap is
  do
    Result := private_insensitive_pixmap;
    if( Result = Void ) then
      Result := sensitive_pixmap;
    end;
  end;

 sensitive_pixmap : AV_PIXMAP is
  do
    Result := private_sensitive_pixmap;
  end;

 --|========================================================================
feature -- Sensitivity and rendition
 --|========================================================================

 enable_sensitive is
  do
    Precursor;
    bb_set_bg_pixmap( sensitive_pixmap );
  end;

 disable_sensitive is
  do
    Precursor;
    bb_set_bg_pixmap( insensitive_pixmap );
  end;

 --|------------------------------------------------------------------------

 is_pressed : BOOLEAN;

 set_pressed( tf : BOOLEAN ) is
  do
    is_pressed := tf;
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 private_insensitive_pixmap : like sensitive_pixmap;
 private_sensitive_pixmap : like sensitive_pixmap;

 --|------------------------------------------------------------------------
invariant

end -- class AV_PICTURE
