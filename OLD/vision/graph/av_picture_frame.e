class AV_PICTURE_FRAME

inherit
  AV_FRAME
   rename
     extend as af_extend,
     set_background_color as af_set_bg_color,
     set_background_pixmap as af_set_bg_pixmap,
     background_pixmap as af_bg_pixmap
   redefine
     make, make_components, set_values, set_geometry, set_callbacks
   select
     default_create
   end;
  BASIC_ROUTINES
   rename
     default_create as dm_default_create
   undefine
     is_equal, copy
   end;

creation
 make, make_with_pixmap

 --|========================================================================
feature {NONE}
 --|========================================================================

 make is
  do
    !! items.make;
    Precursor;
  end;

 --|------------------------------------------------------------------------

 make_with_pixmap( v : AV_PIXMAP ) is
  Require
    pixmap_exists: v /= Void;
  do
    make;
    set_minimum_size( v.width, v.height );
    set_background_pixmap( v );
  end;

 --|------------------------------------------------------------------------

 make_components is
  do
    Precursor;

    !! picture;
    af_extend( picture );

    !! figure_world;
    !! projector.make( figure_world, picture );
  end;

 --|------------------------------------------------------------------------

 set_geometry is
  do
    Precursor;
--    margin_width := 20;
--    margin_height := 20;
  end;

 --|------------------------------------------------------------------------

 set_callbacks is
  do
    Precursor;
    figure_world.pointer_button_release_actions.extend( agent 
							select_item_by_xy );
    resize_actions.extend( agent on_resize );
  end;

 --|------------------------------------------------------------------------

 set_values is
  do
    Precursor;
    set_border_lowered;
  end;

 --|========================================================================
feature
 --|========================================================================

 reference_object : ANY is
  do
  end;

 reference_item : AV_PICTURE_ITEM;
 reference_figure : EV_FIGURE_RECTANGLE;

 items : LINKED_LIST [ like reference_item ];

 picture : EV_DRAWING_AREA;
 projector :  EV_DRAWING_AREA_PROJECTOR;
 figure_world : EV_FIGURE_WORLD;

 background_pixmap : AV_PIXMAP;
 background_picture : EV_FIGURE_PICTURE;

 margin_height : INTEGER;
 margin_width : INTEGER;

 --|========================================================================
feature
 --|========================================================================

 extend( v : like reference_item ) is
  local
    ps : like reference_figure;
  do
    ps := new_figure( v );
    v.set_figure( ps );

--RFO
-- Just trying stuff here, not permanent!!!!

    figure_world.extend( ps );
    items.extend( v );
    projector.project;
  end;

 --|------------------------------------------------------------------------

 new_figure( v : like reference_item ) : like reference_figure is
  local
    pt : EV_RELATIVE_POINT;
    xpos, ypos, yorigin : INTEGER;
  do
--!    xpos := margin_width + (items.count * (bar_width + bar_spacing)) + 10;
--!    yorigin := minimum_height - margin_height;
--!    ypos := yorigin - v.size.rounded;
--!
--!    !! pt.make_with_position( xpos, ypos );
--!
--!    !! Result;
--!    Result.set_line_width( 1 );
--!    Result.set_background_color( v.color );
--!
--!    Result.set_point_a( pt );
--!    !! pt.make_with_position( xpos + bar_width, yorigin );
--!    Result.set_point_b( pt );
  end;

 --|------------------------------------------------------------------------

 clear is
  do
    picture.clear;
  end;

 --|------------------------------------------------------------------------

 new_item( v : like reference_object ) : like reference_item is
  Require
    obj_exists: v /= Void;
  do
  Ensure
    result_exists: Result /= Void;
  end;

 --|========================================================================
feature -- Color
 --|========================================================================

 set_background_color( v : EV_COLOR ) is
  Require
    exists: v /= Void;
  do
    figure_world.set_background_color( v );
  end;

 set_background_pixmap( v : AV_PIXMAP ) is
  Require
    exists: v /= Void;
  do
    background_pixmap := clone( v );
    !! background_picture.make_with_pixmap( background_pixmap );
    figure_world.extend( background_picture );
    --items.extend( background_picture );
    projector.project;
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 resizing : BOOLEAN;

 on_resize( newx, newy, neww, newh : INTEGER ) is
  do
    if( not resizing ) then
      resizing := True;
      --update;
      resizing := False;
    end;
  end;

 --|========================================================================
feature
 --|========================================================================

 selection_proc : PROCEDURE [ ANY, TUPLE [ like reference_item ] ];
  -- Agent to call when user presses mouse button #1 or
  -- otherwise selects or deselects one or more list items

 set_selection_proc( v : like selection_proc ) is
  do
    selection_proc := v;
  end;

 --|------------------------------------------------------------------------

 select_item_by_xy( a_x, a_y, btn : INTEGER;
		    axt, ayt, pr : DOUBLE; sx, sy : INTEGER ) is
  local
    ti : like reference_item;
  do
    if( btn = 1 ) then
      ti := item_at_position( a_x, a_y );
      if( ti /= Void ) then
	last_selected_item := ti;
      end;
      if( selection_proc /= Void ) then
	selection_proc.call( [ ti ] );
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 item_at_position( a_x, a_y : INTEGER ) : like reference_item is
  local
    i : INTEGER;
    pf : like reference_figure;
  do
    from figure_world.start;
    until figure_world.exhausted or Result /= Void
    loop
      pf ?= figure_world.item;
      if( pf /= Void ) then
	i := i + 1;
	if( pf.position_on_figure( a_x, a_y ) ) then
	  Result := items.i_th( i );
	end;
      end;
      if( Result = Void ) then
	figure_world.forth;
      end;
    end;
  end;

 last_selected_item : like reference_item;

 --|------------------------------------------------------------------------
invariant

end -- class AV_PICTURE_FRAME
