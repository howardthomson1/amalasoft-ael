class AV_PIE_GRAPH

inherit
  AV_GRAPH
   rename
     base_figure as pie,
     item_color as slice_color,
     current_item_color as current_slice_color,
     set_current_item_color as set_current_slice_color
   redefine
     pie, shadow, extend, update_graph, position_item_label,
     reference_item, reference_figure
   end;

creation
 make, default_create

 --|========================================================================
feature
 --|========================================================================

 reference_item : AV_PIE_GRAPH_ITEM;
 reference_figure : EV_FIGURE_PIE_SLICE;

 pie : EV_FIGURE_ELLIPSE;
 shadow : EV_FIGURE_ELLIPSE;

 --|========================================================================
feature
 --|========================================================================

 extend( v : like reference_item ) is
  local
    w : EV_FIGURE_WORLD;
    ps : like reference_figure;
    pt : EV_RELATIVE_POINT;
    os, sz : DOUBLE;
  do
    w := projector.world;
    !! pt.make_with_position( margin_width, margin_height );

    !! ps;
    v.set_figure( ps );
    ps.set_line_width( v.line_width );
    ps.set_background_color( v.color );

    ps.set_point_a( pie_point_a );
    ps.set_point_b( pie_point_b );

    sz := v.size;
    os := v.cached_data.offset;

    ps.set_start_angle( os );
    ps.set_aperture( sz );

    w.extend( ps );
    items.extend( v );
  end;

 --|------------------------------------------------------------------------

 update_graph is
  local
    i : INTEGER;
    sl : TUPLE [ INTEGER, INTEGER ];
    ti : AV_PIE_GRAPH_ITEM;
    dl : LINEAR [ ANY ];
  do
    figure_world.wipe_out;
    items.wipe_out;

    make_shadow;
    make_pie;

    dl := data_source.linear_representation;
    from dl.start;
    until dl.exhausted
    loop
      sl ?= dl.item;
      i := i + 1;
      ti := new_item( sl );
      ti.set_color( slice_color( i ) );
      extend( ti );
      dl.forth;
    end;
    draw_labels;
    projector.project;
  end;

 --|------------------------------------------------------------------------

 make_shadow is
  do
    !! shadow;
    shadow.set_line_width( 0 );
    shadow.set_background_color( shadow_figure_color );
    shadow.set_point_a( shadow_point_a );
    shadow.set_point_b( shadow_point_b );
    figure_world.extend( shadow );
  end;

 --|------------------------------------------------------------------------

 make_pie is
  do
    !! pie;
    pie.set_line_width( 0 );
    pie.set_background_color( base_figure_color );
    pie.set_point_a( pie_point_a );
    pie.set_point_b( pie_point_b );
    figure_world.extend( pie );
  end;

 --|========================================================================
feature -- Standard Reference Points (May be cacheable)
 --|========================================================================

 base_point : EV_RELATIVE_POINT is
  -- The absolute anchor, relative to which other points are reckoned.
  do
    !! Result.make_with_position( margin_width, margin_height );
  end;

 --|------------------------------------------------------------------------

 shadow_point_a : EV_RELATIVE_POINT is
  do
    !! Result.make_with_origin_and_position( base_point, 1, 1 );
  end;

 --|------------------------------------------------------------------------

 shadow_point_b : EV_RELATIVE_POINT is
  local
    tx, ty : INTEGER;
  do
    tx := width - (2 * margin_width);
    ty := height - (2 * margin_height);
    !! Result.make_with_origin_and_position( base_point, tx, ty );
  end;

 --|------------------------------------------------------------------------

 pie_point_a : EV_RELATIVE_POINT is
  do
    !! Result.make_with_origin_and_position( base_point, 0, 0 );
  end;

 --|------------------------------------------------------------------------

 pie_point_b : EV_RELATIVE_POINT is
  local
    tx, ty : INTEGER;
  do
    tx := width - ((2 * margin_width) + shadow_thickness - 1);
    ty := height - ((2 * margin_height) + shadow_thickness + 1);
    !! Result.make_with_origin_and_position( base_point, tx, ty );
  end;

 --|========================================================================
feature -- Labeling
 --|========================================================================

 using_approximate_label_position : BOOLEAN;

 --|------------------------------------------------------------------------

 set_using_approximate_label_position( tf : BOOLEAN ) is
  do
    using_approximate_label_position := tf;
  end;

 --|------------------------------------------------------------------------

 position_item_label( v : like reference_item ) is
  do
    if( v.label /= Void ) then
      position_label( v.label, v.size, v.cached_data.offset, 
		      v.cached_data.octant );
    end;
  end;

 --|------------------------------------------------------------------------

 position_label( tf : EV_FIGURE_TEXT; sz, start_angle : DOUBLE; oct : INTEGER )
  is
  local
    bisector : DOUBLE;
    tx, ty : INTEGER;
    x_perim, y_perim : INTEGER;
    x_radius, y_radius : INTEGER;
    rect_origin, text_origin : EV_RELATIVE_POINT;
    bcos, bsin : DOUBLE;
    inv_rad, radius : DOUBLE;
  do
    x_radius := (width - ((2 * margin_width) + shadow_thickness - 1)) //  2;
    y_radius := (height - ((2 * margin_height) + shadow_thickness + 1)) // 2;

    bisector := start_angle + ( sz / 2.0 );
    bsin := sine( bisector );
    bcos := cosine( bisector );

    if( using_approximate_label_position ) then
      -- This approximation converges to the exact answer as the 
      -- eccentricity converges to zero.  The points it produces are 
      -- guaranteed to be on the ellipse, but will be shifted in the 
      -- direction of the longer axis.
      -- Should be used only when the graph is guaranteed to be       
      -- nearly circular.
      -- Should definitely NOT be used if the graph can be resized 
      -- arbitrarily.

      x_perim := abs( (bcos * x_radius).truncated_to_integer );
      y_perim := abs( (bsin * y_radius).truncated_to_integer );
    else
      inv_rad := ( (bcos * bcos) / (x_radius * x_radius) ) + 
	  ( (bsin * bsin) / (y_radius * y_radius) );
      radius := sqrt( 1 / inv_rad );

      x_perim := abs( (radius * bcos).truncated_to_integer );
      y_perim := abs( (radius * bsin).truncated_to_integer );
    end;

    inspect( oct )
    when K_I, K_II, K_III, K_IV then
      -- Above X axis, shift up by text height
      ty := y_radius - (y_perim + tf.height);
    else
      ty := y_radius + y_perim;
    end;

    inspect( oct )
    when K_III, K_IV, K_V, K_VI then
      -- To left of Y axis, shift left by text width
      tx := x_radius - (x_perim + tf.width);
    else
      tx := x_radius + x_perim;
    end;

    !! rect_origin.make_with_position( margin_width, margin_height );
    !! text_origin.make_with_origin_and_position( rect_origin, tx, ty );
    tf.set_point( text_origin );
  end;

 --|========================================================================
feature -- Octant calculations
 --|========================================================================

 octant( ad : DOUBLE ) : INTEGER is
  -- Octant of the angle 'ad' given in radians
  --
  -- 2Pi (~6.28) radians per full circle,
  -- 1/8 or 2Pi is 1/4Pi ~= 0.39 radians per octant
  -- Octant is (by our definition) one-based, beginning with
  -- the positions between 0 and 45 degrees (1/4Pi ~= 0.79r)
  -- and continuing anti-clockwise.
  --
  --   ------------------------------------------------------
  --  |  Octant     Radians      |  Octant       Radians     |
  --  |--------------------------|---------------------------|
  --  |    I      0.00 -> 0.79   |    V       3.15 -> 3.93   |
  --  |    II     0.80 -> 1.57   |    VI      3.94 -> 4.71   |
  --  |    III    1.58 -> 2.36   |    VII     4.72 -> 5.50   |
  --  |    IV     2.37 -> 3.14   |    VIII    5.51 -> 6.28   |
  --   ------------------------------------------------------
  do
--    Result := (ad * 180 / Pi).truncated_to_integer // 45;

    -- We get the octant with at most 3 comparisons
    if( ad > Pi ) then
      if( ad < 4.72 ) then
	if( ad < 3.94 ) then Result := K_V;
	else Result := K_VI;
	end;
      else
	if( ad < 5.51 ) then Result := K_VII;
	else Result := K_VIII;
	end;
      end;
    else
      if( ad < 1.58 ) then
	if( ad < 0.8 ) then Result := K_I;
	else Result := K_II;
	end;
      else
	if( ad < 2.37 ) then Result := K_III;
	else Result := K_IV;
	end;
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 K_V : INTEGER is 5;
 K_VI : INTEGER is 6; 
 K_VII : INTEGER is 7;
 K_VIII : INTEGER is 8;

 K_2_pi : DOUBLE is
  once
    Result := Pi * 2.0;
  end;

 --|------------------------------------------------------------------------
invariant

end -- class AV_PIE_GRAPH
