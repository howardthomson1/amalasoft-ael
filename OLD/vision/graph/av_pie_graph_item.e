class AV_PIE_GRAPH_ITEM

inherit
  AV_GRAPH_ITEM
   redefine
     cached_data, figure
   end;

creation
 make

 --|========================================================================
feature
 --|========================================================================

 cached_data : AV_PIE_GRAPH_ITEM_DATA;
 figure : EV_FIGURE_PIE_SLICE;

 --|------------------------------------------------------------------------
invariant

end -- class AV_PIE_GRAPH_ITEM
