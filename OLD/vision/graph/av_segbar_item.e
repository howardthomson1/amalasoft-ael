class AV_SEGMENTED_BAR_ITEM

inherit
  AV_GRAPH_ITEM
   rename
     make as gi_make
   redefine
     figure
   end;

creation
 make

 --|========================================================================
feature {NONE}
 --|========================================================================

 make( v : like obj; gr : like graph ) is
  do
    gi_make( v );
    graph := gr;
  end;

 --|========================================================================
feature
 --|========================================================================

 graph : AV_SEGMENTED_BAR;

 figure : AV_FIGURE_SEGMENT;

 start_position : INTEGER;

 w_size : INTEGER is
  do
    Result := size.truncated_to_integer;
  end;

 is_free_space : BOOLEAN;

 --|========================================================================
feature
 --|========================================================================

 set_start_position( v : INTEGER ) is
  do
    start_position := v;
  end;

--O set_size( v : INTEGER ) is
--O  do
--O    size := v;
--O  end;

 set_is_free_space( tf : BOOLEAN ) is
  do
    is_free_space := tf;
  end;

 --|========================================================================
feature
 --|========================================================================

 is_selected : BOOLEAN is
  do
--    Result := graph.is_selected( obj );
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 create_implementation is
  do
  end;

 --|------------------------------------------------------------------------
invariant

end -- class AV_SEGMENTED_BAR_ITEM
