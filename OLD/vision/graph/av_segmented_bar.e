class AV_SEGMENTED_BAR

inherit
  AV_GRAPH_FRAME
   rename
     extend as graph_extend
   redefine
     reference_item, reference_figure, set_callbacks, set_values,
     data_source
   end;

creation
 make

 --|========================================================================
feature {NONE}
 --|========================================================================

 set_values is
  do
    Precursor;
  end;

 --|------------------------------------------------------------------------

 set_callbacks is
  do
    Precursor;
  end;

 --|========================================================================
feature
 --|========================================================================

 reference_figure : AV_FIGURE_SEGMENT;
 reference_item : AV_SEGMENTED_BAR_ITEM;

 data_source : LINKED_LIST [ like reference_item ] is
  local
    ti : like reference_item;
  do
    !! Result.make;

    !! ti.make( Void, Current );
    ti.set_start_position( 0 );
    ti.set_size( 150 );
    Result.extend( ti );

    !! ti.make( Void, Current );
    ti.set_start_position( 150 );
    ti.set_size( 50 );
    ti.set_is_free_space( True );
    Result.extend( ti );

    !! ti.make( Void, Current );
    ti.set_start_position( 200 );
    ti.set_size( 5 );
    Result.extend( ti );

    !! ti.make( Void, Current );
    ti.set_start_position( 220 );
    ti.set_size( 75 );
    Result.extend( ti );
  end;

 --|------------------------------------------------------------------------

 selected_figure : AV_FIGURE_SEGMENT;

 next_figure( f : like selected_figure ) : like selected_figure is
  require
    figure_exists: f /= Void;
  local
    idx : INTEGER;
  do
    if( f /= figure_world.last ) then
      if( f = figure_world.first ) then
	Result ?= figure_world.i_th( 2 );
      else
	idx := index_in_world( f );
	Result ?= figure_world.i_th( idx + 1 );
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 previous_figure( f : like selected_figure ) : like selected_figure is
  require
    figure_exists: f /= Void;
  local
    idx : INTEGER;
  do
    if( f /= figure_world.first ) then
      if( f = figure_world.last ) then
	Result ?= figure_world.i_th( figure_world.count - 1 );
      else
	idx := index_in_world( f );
	Result ?= figure_world.i_th( idx - 1 );
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 update_graph is
  -- Create individual segments to represent the data source
  -- Calculate the whole value range, then normalize the range
  -- and the separate sizes to the current overall width.
  local
    dl : like data_source;
    ti : like reference_item;
    oc : CURSOR;
  do
    figure_world.wipe_out;
    if( figure_world.capture_figure /= Void ) then
      figure_world.remove_capture_figure;
    end;
    items.wipe_out;
    set_background_color( colors.K_color_dark_gray );
    --set_background_color( colors.K_color_white );

    draw_sizer_track;
    left_sizer := Void;
    right_sizer := Void;

    dl := data_source;
    if( dl /= Void and then not dl.is_empty ) then
      oc := dl.cursor;
      from dl.start;
      until dl.exhausted
      loop
	ti := dl.item;
	extend( ti.start_position, ti.w_size, ti.is_free_space );
	dl.forth;
      end;
      dl.go_to( oc );
    end;
    draw_labels;
    projector.project;
  end;

 --|------------------------------------------------------------------------

 set_right_btn_down_procedure( v : like right_btn_down_proc ) is
  do
    right_btn_down_proc := v
  end;

 set_selection_change_procedure( v : like selection_change_proc ) is
  do
    selection_change_proc := v;
  end;

 set_geometry_change_procedure( v : like geometry_change_proc ) is
  do
    geometry_change_proc := v;
  end;

 --|------------------------------------------------------------------------

 select_figure( f : like reference_figure ) is
  require
    figure_exists: f /= Void
  do
    f.enable_select;
    if( selection_change_proc /= Void ) then
      selection_change_proc.call( [ f ] );
    end;
    selected_figure := f;
    draw_sizers;
  end;

 --|------------------------------------------------------------------------

 remove_figure( f : like reference_figure ) is
  require
    figure_exists: f /= Void
  do
    figure_world.start;
    figure_world.search( f );
    if( not figure_world.exhausted ) then
      figure_world.remove;
    end;
    projector.project;
    clear_selections;
  end;

 --|========================================================================
feature
 --|========================================================================

 kill_capture is
  local
    tf : ev_figure;
  do
    tf := figure_world.capture_figure;
    if( tf /= Void ) then
      tf.disable_capture;
    end;
  end;

 --|------------------------------------------------------------------------

 extend( pos, sz : INTEGER; is_free : BOOLEAN ) is
  local
    ps, tf : like reference_figure;
    ti : like reference_item;
    c : EV_COLOR;
  do
    if( is_free ) then
      c := colors.K_color_gray;
    else
      c := colors.K_color_honeydew;
    end;
    !! ti.make( Void, Current );
    ps := new_figure( ti, pos, sz );
    ti.set_figure( ps );
    if( c /= Void ) then
      ps.set_background_color( c );
    end;
    if( not figure_world.is_empty ) then
      tf ?= figure_world.last;
    end;
    figure_world.extend( ps );
    items.extend( ti );
  end;

 --|------------------------------------------------------------------------

 draw_sizer_track is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
  do
    if( sizer_track = Void ) then
      !! sizer_track;
      sizer_track.set_background_color( colors.K_color_light_gray );
    end;
    !! pt1.make_with_position( 0, 0 );
    !! pt2.make_with_position( width - 2, 10 );
    sizer_track.set_point_a( pt1 );
    sizer_track.set_point_b( pt2 );
    figure_world.extend( sizer_track );
  end;

 --|------------------------------------------------------------------------

 new_figure( v : like reference_item;
	     pos, sz : INTEGER ) : like reference_figure is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
    pts : ARRAY [ EV_RELATIVE_POINT ];
  do
    !! Result;
    Result.set_line_width( 1 );

    pts := new_figure_points( pos, sz );
    pt1 := pts.item( 1 );
    pt2 := pts.item( 2 );

    Result.set_point_a( pt1 );
    Result.set_point_b( pt2 );

    Result.pointer_button_press_actions.extend( agent
						on_figure_btn_dn(?,?,?,?,
								 ?,?,?,?,
								 Result) );
    Result.pointer_button_release_actions.extend( agent
						  on_figure_btn_up(?,?,?,?,
								   ?,?,?,?,
								   Result) );
    Result.pointer_motion_actions.extend( agent
					  on_figure_move(?,?,?,?,?,?,?,
							 Result) );
    Result.pointer_enter_actions.extend( agent on_figure_enter(Result) );
    Result.pointer_leave_actions.extend( agent on_figure_leave(Result) );
  end;

 --|------------------------------------------------------------------------

 new_figure_points( pos, w : INTEGER ) : ARRAY [ EV_RELATIVE_POINT ] is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
  do
    !! pt1.make_with_position( pos, K_figure_y_origin );
    !! pt2.make_with_position( pos + w, K_figure_y_origin + K_figure_height );
    Result := << pt1, pt2 >>;
  end;

 --|------------------------------------------------------------------------

 sizer_track : EV_FIGURE_RECTANGLE;
-- left_sizer : AV_FIGURE_SIZER;
-- right_sizer : AV_FIGURE_SIZER;
 left_sizer : EV_FIGURE_PICTURE;
 right_sizer : EV_FIGURE_PICTURE

 --|------------------------------------------------------------------------

 draw_sizers is
  local
    pt1, pt2 : EV_RELATIVE_POINT;
    f : like selected_figure;
    pm : AV_PIXMAP;
  do
    if( left_sizer = Void ) then
      left_sizer := new_sizer;
      right_sizer := new_sizer;
    end;
    f := selected_figure;
    if( f = Void ) then
      hide_sizers;
    else
      !! pt1.make_with_position( f.point_a.x - 7, 0 );
      left_sizer.set_point( pt1 );
      left_sizer.show;

      !! pt1.make_with_position( f.point_b.x - 7, 0 );
      right_sizer.set_point( pt1 );
      right_sizer.show;
    end;
  end;

 --|------------------------------------------------------------------------

 new_sizer : like left_sizer is
  do
    !! Result.make_with_pixmap( pixmaps.new_pixmap("thumb_15x8.png") );
    figure_world.extend( Result );
    Result.pointer_button_press_actions.extend( agent
					on_sizer_btn_dn(?,?,?,?,?,?,?,?,
							Result) );
    Result.pointer_button_release_actions.extend( agent
					on_sizer_btn_up(?,?,?,?,?,?,?,?,
							Result) );
    Result.pointer_motion_actions.extend( agent
					  on_sizer_move(?,?,?,?,?,?,?,
							Result) );
  end;

 --|------------------------------------------------------------------------

 hide_sizers is
  do
    left_sizer.hide;
    right_sizer.hide;
  end;

 --|========================================================================
feature {NONE} -- Agents and callbacks
 --|========================================================================

 right_btn_down_proc : PROCEDURE [ ANY, TUPLE [ like reference_figure ] ];
 selection_change_proc : PROCEDURE [ ANY, TUPLE [ like reference_figure ] ];
 geometry_change_proc : PROCEDURE [ ANY, TUPLE [ like reference_figure ] ];

 --|------------------------------------------------------------------------

 button_1_down_pos : INTEGER;
 button_1_down_original_ax : INTEGER;
 button_1_down_original_bx : INTEGER;

 sizer_btn_down_pos : INTEGER;
 sizer_btn_down_original_ax : INTEGER;

 --|------------------------------------------------------------------------

 on_figure_btn_dn( x, y, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER;
		   f : like reference_figure ) is
  local
    tf : ev_figure;
    ts : like reference_figure;
  do
    clear_selections;
    if( f /= Void and then b = 1 ) then
      select_figure( f );
      f.enable_capture;
      button_1_down_pos := x;
      button_1_down_original_ax := f.point_a.x;
      button_1_down_original_bx := f.point_b.x;
    else
      kill_capture;
      button_1_down_pos := 0;
      button_1_down_original_ax := 0;
      button_1_down_original_bx := 0;
      if( b = 3 ) then
	if( right_btn_down_proc /= Void ) then
	  right_btn_down_proc.call( [ f ] );
	end;
      end;
    end;
  end;

 --|------------------------------------------------------------------------

 on_figure_btn_up( x, y, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER;
		   f : like reference_figure ) is
  local
    tf : ev_figure;
  do
    kill_capture;
    button_1_down_pos := 0;
    button_1_down_original_ax := 0;
    button_1_down_original_bx := 0;
  end;

 --|------------------------------------------------------------------------

 on_figure_enter( f : like reference_figure ) is
  local
    tf : ev_figure;
  do
    kill_capture;
    if( f /= Void ) then
      f.set_pointer_style( stock_pixmaps.Sizeall_cursor );
    end;
  end;

 --|------------------------------------------------------------------------

 on_figure_leave( f : like reference_figure ) is
  local
    tf : ev_figure;
  do
    kill_capture;
  end;

 --|------------------------------------------------------------------------

 on_figure_move( x, y: INTEGER; xt, yt, pressure: DOUBLE; sx, sy: INTEGER;
		 f : like reference_figure ) is
  -- Once a figure has capture, it allegedly gets all mouse move events,
  -- though it seems it gets them only when the mouse is over it?????
  local
    ax, new_ax, displacement, minx, maxx : INTEGER;
    tf : ev_figure;
    fudge : INTEGER;
  do
    if( f.has_capture ) then
      hide_sizers;
      ax := button_1_down_original_ax;
      displacement := x - button_1_down_pos;
      new_ax := ax + displacement;
      minx := minimum_xpos( f );
      maxx := maximum_xpos( f );
      fudge := 50;
      if( (new_ax >= (minx - fudge)) and (new_ax <= (maxx + fudge)) and
	  (sy >= (mouse_min_y - fudge)) and (sy <= (mouse_max_y + fudge)) )
      then
	new_ax := minx.max(new_ax);
	new_ax := maxx.min(new_ax);
	f.set_x_position( new_ax );
	if( geometry_change_proc /= Void ) then
	  geometry_change_proc.call( [ f ] );
	end;
      else
	f.disable_capture;
      end;
      draw_sizers;
    end;
  end;

 --|------------------------------------------------------------------------
 --|------------------------------------------------------------------------

 on_sizer_btn_dn( x, y, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER;
		  f : like left_sizer ) is
  do
    if( f /= Void and then b = 1 ) then
      f.enable_capture;
      sizer_btn_down_pos := x;
      sizer_btn_down_original_ax := f.point.x;
      button_1_down_original_ax := selected_figure.point_a.x;
      button_1_down_original_bx := selected_figure.point_b.x;
    else
      kill_capture;
      sizer_btn_down_pos := 0;
      sizer_btn_down_original_ax := 0;
      button_1_down_original_ax := 0;
      button_1_down_original_bx := 0;
    end;
  end;

 --|------------------------------------------------------------------------

 on_sizer_btn_up( x, y, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER;
		  f : like left_sizer ) is
  do
    kill_capture;
    sizer_btn_down_pos := 0;
    sizer_btn_down_original_ax := 0;
    button_1_down_original_ax := 0;
    button_1_down_original_bx := 0;
  end;

 --|------------------------------------------------------------------------

 on_sizer_move( x, y: INTEGER; xt, yt, pressure: DOUBLE; sx, sy: INTEGER;
		f : like left_sizer ) is
  -- Sizer movement denotes figure resize
  local
    ax, newx, displacement, minx, maxx : INTEGER;
    new_fx, min_fx, max_fx : INTEGER;
    fudge : INTEGER;
    tf : like reference_figure;
    other_sizer : like left_sizer;
    fpoint : EV_RELATIVE_POINT;
  do
    if( f.has_capture ) then
      ax := sizer_btn_down_original_ax;
      displacement := x - sizer_btn_down_pos;
      newx := ax + displacement;

      tf := selected_figure;
      if( f = left_sizer ) then
	max_fx := button_1_down_original_bx;
	min_fx := minimum_xpos( tf );
	ax := button_1_down_original_ax;
	fpoint := tf.point_a;
	other_sizer := right_sizer;
	minx := min_fx - 7;
	maxx := button_1_down_original_bx - 7;
      else
	max_fx := maximum_bxpos( tf );
	min_fx := button_1_down_original_ax;
	ax := button_1_down_original_bx;
	fpoint := tf.point_b;
	other_sizer := left_sizer;
	minx := button_1_down_original_ax - 7;
	maxx := max_fx - 8;
      end;
      new_fx := ax + displacement;

      fudge := 50;

      if( (newx >= (minx - fudge)) and (newx <= (maxx + fudge)) and
	  (sy >= (mouse_min_y - fudge)) and (sy <= (mouse_max_y + fudge)) )
      then
	newx := maxx.min(minx.max(newx));
	new_fx := max_fx.min(min_fx.max(new_fx));
	fpoint.set_x( new_fx );
	f.point.set_x( newx );
	f.show;
	other_sizer.show;
	tf.show
	if( geometry_change_proc /= Void ) then
	  geometry_change_proc.call( [ tf ] );
	end;
      else
	f.disable_capture;
      end;
    end;
  end;

 --|------------------------------------------------------------------------
 --|------------------------------------------------------------------------

 minimum_xpos( f : like reference_figure ) : INTEGER is
  -- The lower legal value for the left-most edge of the given
  -- figure (based on other figures in world)
  require
    figure_exists: f /= Void;
  local
    idx : INTEGER;
    tf : like reference_figure;
  do
    idx := index_in_world( f );
    if( idx > 1 ) then
      tf ?= figure_world.i_th( idx - 1 );
      if( tf /= Void ) then
	Result := tf.end_position;
      end;
    end;
  ensure
    not_off_left: Result >= 0;
  end;

 --|------------------------------------------------------------------------

 maximum_xpos( f : like reference_figure ) : INTEGER is
  -- The highest legal value for the left-most edge of the given
  -- figure (based on other figures in world)
  require
    figure_exists: f /= Void;
  local
    idx : INTEGER;
    tf : like reference_figure;
  do
    idx := index_in_world( f );
    if( idx < world_count ) then
      tf ?= figure_world.i_th( idx + 1 );
      if( tf /= Void ) then
	Result := tf.x_position - f.width;
      else
	Result := (width - 2) - f.width;
      end;
    else
      Result := (width - 2) - f.width;
    end;
  ensure
    not_off_right: Result <= width;
  end;

 --|------------------------------------------------------------------------

 maximum_bxpos( f : like reference_figure ) : INTEGER is
  -- The highest legal value for the right-most edge of the given
  -- figure (based on other figures in world)
  require
    figure_exists: f /= Void;
  local
    idx : INTEGER;
    tf : like reference_figure;
  do
    idx := index_in_world( f );
    if( idx < world_count ) then
      tf ?= figure_world.i_th( idx + 1 );
      if( tf /= Void ) then
	Result := tf.x_position;
      else
	Result := width - 1;
      end;
    else
      Result := width - 1;
    end;
  ensure
    not_off_right: Result <= width;
  end;

 --|------------------------------------------------------------------------

 mouse_min_x : INTEGER is
  do
    Result := screen_x;
  end;

 mouse_min_y : INTEGER is
  do
    Result := screen_y;
  end;

 --|------------------------------------------------------------------------

 mouse_max_x : INTEGER is
  do
    Result := screen_x + width;
  end;

 mouse_max_y : INTEGER is
  do
    Result := screen_y + height;
  end;

 --|------------------------------------------------------------------------

 index_in_world( f : like reference_figure ) : INTEGER is
  require
    figure_exists: f /= Void;
    in_world: figure_world.has( f );
  local
    idx, lim : INTEGER;
  do
    lim := figure_world.count;
    from idx := 1;
    until idx > lim or Result /= 0
    loop
      if( figure_world.i_th(idx) = f ) then
	Result := idx;
      else
	idx := idx + 1;
      end;
    end;
  ensure
    valid_index: (not world_is_empty) implies
		 Result > 0 and Result <= world_count;
  end;

 --|------------------------------------------------------------------------

 world_is_empty : BOOLEAN is
  do
    Result := figure_world.is_empty;
  end;

 --|------------------------------------------------------------------------

 world_count : INTEGER is
  do
    Result := figure_world.count;
  end;

 --|------------------------------------------------------------------------

 clear_selections is
  local
    tf : like reference_figure;
  do
    from figure_world.start;
    until figure_world.exhausted
    loop
      tf ?= figure_world.item;
      if( tf /= Void ) then
	tf.disable_select;
      end;
      figure_world.forth;
    end;
    selected_figure := Void;
    if( selection_change_proc /= Void ) then
      selection_change_proc.call( [ Void ] );
    end;
    if( left_sizer /= Void ) then
      hide_sizers;
    end;
  end;

 --|========================================================================
feature {NONE}
 --|========================================================================

 stock_pixmaps : EV_STOCK_PIXMAPS is
  once
    !! Result;
  end;

 --|------------------------------------------------------------------------

 K_figure_height : INTEGER is 40;
 K_figure_y_origin : INTEGER is 10;

 --|------------------------------------------------------------------------
invariant

end -- class AV_SEGMENTED_BAR
