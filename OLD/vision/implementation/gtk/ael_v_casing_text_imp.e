indexing
	description: "GTK Impementation of the AEL_V_CASING_TEXT widget"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
This is an implemenation class is not be accessed directlry by clients
}"
	
class AEL_V_CASING_TEXT_IMP

inherit
	EV_PASSWORD_FIELD_IMP
		redefine
			interface, initialize
		end

create
	make

 --|========================================================================
feature -- Interface
 --|========================================================================

	interface: AEL_V_CASING_TEXT

	is_upper: BOOLEAN is
		do
			Result := interface.is_upper
		end

	--|--------------------------------------------------------------

	initialize is
		do
			Precursor {EV_PASSWORD_FIELD_IMP}
			{EV_GTK_EXTERNALS}.gtk_entry_set_visibility (entry_widget, True)
		end

end -- class AEL_V_CASING_TEXT_IMP
