note
	description: "A programmatically created pixmap to associate with a 'new' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_NEW_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			set_line_width (1)

			-- Draw file
			set_foreground_color (colors.black)
			draw_rectangle (2, 1, 11, 13)
			set_foreground_color (colors.white)
			fill_rectangle (3, 2, 9, 11)
			set_foreground_color (colors.black)
			draw_segment (9, 1, 12, 4)
			draw_segment (9, 2, 9, 4)
			draw_segment (10, 4, 11, 4)

			-- Clean up corner
			set_foreground_color (colors.default_background_color)
			draw_segment (10, 1, 12, 3)
			draw_segment (11, 1, 12, 2)
			draw_point (12, 1)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 16
	pm_height: INTEGER is 15

end -- class AEL_V_NEW_PIXMAP
