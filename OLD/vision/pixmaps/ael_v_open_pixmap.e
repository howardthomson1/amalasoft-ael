note
	description: "A programmatically created pixmap to associate with an 'open' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_OPEN_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			set_line_width (1)

			-- Draw arrow
			set_foreground_color (colors.black)
			draw_point (8, 2)
			draw_segment (9, 1, 11, 1)
			draw_point (12, 2)
			draw_point (13, 3)
			draw_segment (14, 2, 14, 4)
			draw_segment (12, 4, 13, 4)

			-- Draw folder outline

			set_foreground_color (colors.black)
			draw_rectangle (0, 5, 11, 9)
			draw_segment (1, 4, 3, 4)

			-- Draw folder outside

			set_foreground_color (colors.black)
			draw_rectangle (1, 8, 14, 6)
			set_foreground_color (colors.dark_yellow)
			fill_rectangle (2, 9, 12, 4)
			set_foreground_color (colors.black)
			draw_segment (1, 12, 5, 8)
			draw_segment (10, 13, 14, 9)

			-- Draw folder inside
			-- Pale yellow
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 1, 0.7))
			draw_segment (1, 5, 3, 5)
			fill_rectangle (1, 6, 9, 2)
			draw_segment (1, 8, 4, 8)
			draw_segment (1, 9, 3, 9)
			draw_segment (1, 10, 2, 10)
			draw_point (1, 11)

			-- Clean up corners
			set_foreground_color (colors.default_background_color)
			draw_point (14, 10)
			fill_rectangle (13, 11, 2, 3)
			draw_point (12, 12)
			draw_point (11, 13)
			draw_point (12, 13)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 16
	pm_height: INTEGER is 15

end -- class AEL_V_OPEN_PIXMAP
