note
	description: "A programmatically created pixmap to associate with a 'search' or 'find' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_SEARCH_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw is
		do
			set_line_width (1)

			-- Draw file box
			set_foreground_color (colors.black)
			draw_rectangle (3, 0, 11, 14)
			set_foreground_color (colors.white)
			fill_rectangle (4, 1, 9, 12)

			-- Draw magnifiy glass lens
			-- Pale blue
			set_foreground_color (create {EV_COLOR}.make_with_rgb (.7098, 1, 1))
			fill_rectangle (3, 3, 6, 7)
			set_foreground_color (colors.white)
			draw_point (5, 4)
			draw_point (4, 5)
			draw_point (6, 5)
			draw_point (5, 6)
			draw_point (7, 6)
			draw_point (4, 7)
			-- 60% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.6, 0.6, 0.6))
			draw_segment (3, 2, 7, 2)
			draw_segment (3, 10, 7, 10)
			draw_segment (1, 4, 1, 8)
			draw_segment (9, 4, 9, 8)
			draw_point (2, 3)
			draw_point (8, 3)
			draw_point (2, 9)
			draw_point (8, 9)
			draw_point (8, 9)
			-- 40% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.4, 0.4, 0.4))
			draw_segment (2, 4, 2, 8)

			-- Draw handle
			set_foreground_color (colors.default_background_color)
			draw_segment (10, 9, 12, 11)
			-- 40% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.4, 0.4, 0.4))
			draw_segment (9, 9, 13, 13)
			-- 60% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.6, 0.6, 0.6))
			draw_segment (8, 9, 13, 14)
			-- 75% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (.75, .75, .75))
			draw_segment (8, 10, 12, 14)
			draw_point (11, 14)
			draw_point (13, 14)
			draw_point (13, 13)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 16
	pm_height: INTEGER is 15

end -- class AEL_V_SEARCH_PIXMAP
