note
	description: "A programmatically created small left arrow pixmap"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_SMALL_LEFT_ARROW_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			-- Draw arrow
			set_background_color (colors.default_background_color)
			set_foreground_color (colors.default_background_color)
			fill_rectangle (0, 0, 9, 10)
			set_foreground_color (colors.black)
			fill_polygon (<< create {EV_COORDINATE}.make (2, 5),
			                 create {EV_COORDINATE}.make (6, 1),
								  create {EV_COORDINATE}.make (6, 9)
								  >>)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 10
	pm_height: INTEGER is 11


end -- class AEL_V_SMALL_LEFT_ARROW_PIXMAP
