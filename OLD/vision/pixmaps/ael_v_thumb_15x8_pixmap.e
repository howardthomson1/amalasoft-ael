note
	description: "A programmatically created pixmap to be used as a 'thumb' in slider-like widgets"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_THUMB_15X8_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			-- 30% black
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.7, 0.7, 0.7))
			clear

			set_line_width (1)

			-- 10% black
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.9, 0.9, 0.9))
			draw_rectangle (5, 0, 15, 3)

			draw_point (14, 0)
			draw_point (14, 5)
			draw_segment (8, 2, 8, 5)

			set_foreground_color (colors.dark_gray)
			draw_segment (6, 1, 6, 5)

			set_foreground_color (colors.black)
			draw_segment (1, 5, 5, 5)
			draw_segment (9, 5, 13, 5)
			draw_segment (7, 1, 7, 7)
			draw_segment (14, 1, 14, 4)
			draw_segment (6, 6, 8, 6)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 15
	pm_height: INTEGER is 8

end -- class AEL_V_THUMB_15X8_PIXMAP
