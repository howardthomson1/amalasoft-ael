indexing
	description: "A programmatically created pixmap to associate with a 'undo' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_UNDO_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			-- Draw arrowhead
			set_foreground_color (colors.black)
			fill_rectangle (1, 6, 5, 5)
			set_foreground_color (colors.default_background_color)
			draw_segment (2, 6, 5, 9)
			draw_segment (3, 6, 5, 8)
			draw_segment (4, 6, 5, 7)

			-- Draw arrow arc
			set_foreground_color (colors.black)
			draw_point (4, 7)
			draw_segment (5, 6, 6, 6)
			draw_segment (7, 5, 10, 5)
			draw_point (11, 6)
			draw_segment (12, 7, 12, 9)
			draw_segment (11, 10, 11, 11)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 16
	pm_height: INTEGER is 15

end -- class AEL_V_UNDO_PIXMAP
