note
	description: "A programmatically created pixmap showing 'unlocked'"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V_UNLOCKED_PIXMAP

inherit
	AEL_V_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw is
		do
			set_background_color (colors.default_background_color)
			clear

			-- Draw lock body

			set_foreground_color (colors.dark_gray)
			fill_rectangle (3, 8, 13, 9)
			draw_segment (2, 9, 2, 15)
			draw_segment (16, 9, 16, 16)

			-- Gold
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 0.843, 0))
			fill_rectangle (3, 9, 12, 7)

			-- Draw keyhole

			set_foreground_color (colors.black)
			fill_rectangle (7, 10, 2, 5)

			-- 80% black
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.2, 0.2, 0.2))
			fill_rectangle (9, 10, 2, 7)

			set_foreground_color (colors.black)
			draw_point (9, 10)

			-- Gold
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 0.843, 0))
			draw_segment (7, 12, 7, 13)
			draw_segment (10, 12, 10, 13)

			-- Shadow on body

			set_foreground_color (colors.black)
			draw_segment (17, 10, 17, 16)
			draw_segment (3, 17, 16, 17)

			-- Draw bale
			-- Pale blue

			set_foreground_color (create {EV_COLOR}.make_with_rgb (.7098, 1, 1))
			draw_segment (10, 0, 15, 0)
			draw_segment (9, 1, 9, 3)
			draw_point (17, 1)
			draw_segment (12, 6, 12, 7)
			draw_segment (13, 5, 13, 6)
			draw_segment (14, 4, 14, 5)
			draw_segment (15, 3, 15, 4)
			draw_point (16, 3)

			set_foreground_color (colors.black)
			draw_point (8, 2)
			draw_point (10, 2)
			draw_segment (11, 1, 12, 1)
			draw_segment (13, 0, 14, 0)
			draw_segment (14, 1, 15, 1)
			draw_segment (15, 2, 17, 2)
			draw_point (17, 3)
			draw_point (16, 4)
			draw_segment (15, 5, 16, 5)
			draw_segment (14, 6, 15, 6)
			draw_segment (13, 5, 14, 4)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER is 18
	pm_height: INTEGER is 18

end -- class AEL_V_UNLOCKED_PIXMAP
