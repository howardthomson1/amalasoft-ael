note
	description: "A text widget that forces case.  WINDOWS ONLY at this time"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
To use this widget, create it using either make_for_lower or make_for_upper.
After creation, manipulate it as you would a EV_TEXT widget.
}"

class AEL_V_CASING_TEXT

inherit
	EV_PASSWORD_FIELD
		redefine
			check_text_modification, implementation, create_implementation,
			is_in_default_state
		end

create
	make_for_lower, make_for_upper

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	make_for_lower is
		do
			is_lower := True
			default_create
		end

	make_for_upper is
		do
			default_create
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	implementation: AEL_V_CASING_TEXT_IMP

	create_implementation is
		do
			create implementation.make (Current)
		end

	is_in_default_state: BOOLEAN is
		do
			Result := True
		end

 --|========================================================================
feature
 --|========================================================================

	is_lower : BOOLEAN
			-- Are all chars forced to lower case?

	is_upper : BOOLEAN is
			-- Are all chars forced to upper case?
		do
			Result := not is_lower
		end

 --|------------------------------------------------------------------------

	check_text_modification (old_text, added_text: STRING_32): BOOLEAN is
			-- Ensure that `text' is equal to `old_text' + `added_text' with
			-- all %R removed.
		local
			s0, s1, s2: STRING
		do
			Result := Precursor (old_text, added_text)
			if not Result then
				s0 := text.twin
				s1 := old_text.twin
				s2 := added_text.twin
				s0.to_upper
				s1.to_upper
				s2.to_upper
				Result := s0.is_equal (s1 + s2)
			end
		end

end -- class AEL_V_CASING_TEXT
