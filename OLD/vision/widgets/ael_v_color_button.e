note
	description: "Compound widget with a button and label, for selecting a color"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    The button has an inset showing a settable color.
    Default action on the button invokes the color dialog, the selection
    from which then becomes the inset's background color.
    To use this widget, create it using 'make_with_attributes',
    giving it a label string, the current color, and a position flag.
    Then add to your container and disable expansion of this widget if needed.
    If you wish to receive a notification on each change to this widget,
    you can register your agent using 'set_notification_procedure'.
    Query the 'button_color' feature to get the color currently held in
    the inset.
}"

class AEL_V_COLOR_BUTTON

inherit
	AEL_V_LABELED_BUTTON
		redefine
			build_button, on_button_press
		end

create
	make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (lb: STRING; c: EV_COLOR; pf: BOOLEAN) is
			-- Create a new widget with the label string 'lb', intial color 'c'
			-- and with text aligned either to right of button (default) or
			-- to left of button (if 'pf' is True)
		require
			label_exists: lb /= Void
		do
			private_button_color := c
			make_with_label (lb, pf)
		end

	--|--------------------------------------------------------------

	build_button is
		do
			Precursor
			draw_button
		end

	--|--------------------------------------------------------------

	draw_button is
		local
			pm: EV_PIXMAP
		do
			create pm.make_with_size (pixmap_width, pixmap_height)
			pm.set_background_color (button_color)
			pm.clear
			button.set_pixmap (pm)
		end

--|========================================================================
feature -- Status
--|========================================================================

	button_color: EV_COLOR is
		do
			Result := private_button_color
			if Result = Void then
				Result := K_color_button
			end
		end

	--|--------------------------------------------------------------

	pixmap_width: INTEGER is
		do
			Result := private_pixmap_width
			if Result = 0 then
				Result := K_dflt_pixmap_width
			end
		end

	pixmap_height: INTEGER is
		do
			Result := private_pixmap_height
			if Result = 0 then
				Result := K_dflt_pixmap_height
			end
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press is
		local
			tc: EV_COLOR
			cd: EV_COLOR_DIALOG
		do
			create cd.make_with_title (label_text)
			cd.set_color (button.pixmap.background_color)
			cd.show_modal_to_window (enclosing_window)
			tc := cd.color
			set_button_color (tc)
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_button_color (c: EV_COLOR) is
		require
			exists: c /= Void
		do
			private_button_color := c
			draw_button
		end

	--|--------------------------------------------------------------

	set_pixmap_size (w, h: INTEGER) is
		do
			private_pixmap_width := w
			private_pixmap_height := h
			draw_button
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_pixmap_width: INTEGER
	private_pixmap_height: INTEGER
	private_button_color: EV_COLOR

	K_dflt_pixmap_width: INTEGER is 12
	K_dflt_pixmap_height: INTEGER is 12

	K_color_button: EV_COLOR is
		once
			Result := colors.white
		end

end -- class AEL_V_COLOR_BUTTON
