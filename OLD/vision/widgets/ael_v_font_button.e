note
	description: "Compound widget with a button and label, for selecting a font"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    The button has an inset showing a settable font.
    Default action on the button invokes the font dialog, the selection
    from which then becomes the inset's font.  The inset shows a capital
    'A' in the selected font, but with a height proportional to the
    height of the button.
    To use this widget, create it using 'make_with_attributes',
    giving it a label string, the current font, and a position flag.
    Then add to your container and disable expansion of this widget if needed.
    If you wish to receive a notification on each change to this widget,
    you can register your agent using 'set_notification_procedure'.
}"

class AEL_V_FONT_BUTTON

inherit
	AEL_V_LABELED_BUTTON
		redefine
			build_button, on_button_press
		end

create
	make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (lb: STRING; f: EV_FONT; pf: BOOLEAN) is
			-- Create a new widget with the label string 'lb', intial font 'f'
			-- and with text aligned either to right of button (default) or
			-- to left of button (if 'pf' is True)
		require
			label_exists: lb /= Void
		do
			private_button_font := f
			make_with_label (lb, pf)
		end

	--|--------------------------------------------------------------

	build_button is
		do
			Precursor
			draw_button
		end

	--|--------------------------------------------------------------

	draw_button is
		local
			pm: EV_PIXMAP
			tf: EV_FONT
			sw: INTEGER
		do
			create pm.make_with_size (pixmap_width, pixmap_height)
			pm.set_background_color (colors.white)
			pm.set_foreground_color (colors.black)
			pm.clear
			tf := button_font.twin
			tf.set_height (pixmap_height)
			sw := tf.string_width ("A")
			pm.set_font (tf)
			pm.draw_text_top_left (0, 0, "A")
			button.set_pixmap (pm)
		end

--|========================================================================
feature -- Status
--|========================================================================

	button_font: EV_FONT is
		do
			Result := private_button_font
			if Result = Void then
				Result := K_font_button
			end
		end

	--|--------------------------------------------------------------

	pixmap_width: INTEGER is
		do
			Result := private_pixmap_width
			if Result = 0 then
				Result := K_dflt_pixmap_width
			end
		end

	pixmap_height: INTEGER is
		do
			Result := private_pixmap_height
			if Result = 0 then
				Result := K_dflt_pixmap_height
			end
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press is
		local
			tc: EV_FONT
			cd: EV_FONT_DIALOG
		do
			create cd.make_with_title (label_text)
			cd.set_font (button_font)
			cd.show_modal_to_window (enclosing_window)
			tc := cd.font
			set_button_font (tc)
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_button_font (f: EV_FONT) is
		require
			exists: f /= Void
		do
			private_button_font := f
			draw_button
		end

	--|--------------------------------------------------------------

	set_pixmap_size (w, h: INTEGER) is
		do
			private_pixmap_width := w
			private_pixmap_height := h
			draw_button
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_pixmap_width: INTEGER
	private_pixmap_height: INTEGER
	private_button_font: EV_FONT

	K_dflt_pixmap_width: INTEGER is 12
	K_dflt_pixmap_height: INTEGER is 12

	K_font_button: EV_FONT is
		once
			Result := fonts.times_new_roman_font (12, False, False)
		end

end -- class AEL_V_FONT_BUTTON
