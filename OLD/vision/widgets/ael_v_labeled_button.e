note
  description: "Compound widget with a button and a label"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    To use this widget, create it using 'make_with_label', giving
    it a string to use as the widget label.
    Then add to your container and disable expansion of this widget if needed.
    If you wish to receive a notification on each change to this widget,
    you can register your agent using 'set_notification_procedure'.
    This widget by itself is not very interesting or useful, but its
    descendents are.  Descendents include AEL_V_COLOR_BUTTON and
    AEL_V_FONT_BUTTON.
}"

class AEL_V_LABELED_BUTTON

inherit
	AEL_V_LABELED_WIDGET
		rename
			widget as button, build_widget as build_button
		redefine
			build_button, set_actions
		end

create
	make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	set_actions is
		do
			button.select_actions.extend (agent on_button_press)
			Precursor
		end

	--|--------------------------------------------------------------

	build_button is
		do
			create button
		end

--|========================================================================
feature -- Components
--|========================================================================

	button: EV_BUTTON

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

end -- class AEL_V_LABELED_BUTTON
