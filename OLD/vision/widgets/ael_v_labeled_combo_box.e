note
  description: "Compound widget with a combo box and a label"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
		To use this widget, create it using 'make_with_label', or using
		'make_with_label_and_strings', giving it a string to use as the
		widget label and optionally as item labels.
    Then add to your container and disable expansion of this widget if needed.
    If you wish to receive a notification on each change to this widget,
    you can register your agent using 'set_notification_procedure'.
}"

class AEL_V_LABELED_COMBO_BOX

inherit
	AEL_V_LABELED_WIDGET
		rename
			widget as combo_box, build_widget as build_combo_box
		redefine
			build_combo_box, set_actions
		end

create
	make_with_label, make_with_label_and_strings

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_label_and_strings (lb: STRING; sl: like box_strings; pf: BOOLEAN) is
		require
			label_exists: lb /= Void
			strings_exist: sl /= Void
		do
			box_strings := sl
			make_with_label (lb, pf)
		end

	--|--------------------------------------------------------------

	set_actions is
		do
			combo_box.select_actions.extend (agent on_select)
			Precursor
		end

	--|--------------------------------------------------------------

	build_combo_box is
		do
			if box_strings /= Void then
				create combo_box.make_with_strings (box_strings)
			else
				create combo_box
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	combo_box: EV_COMBO_BOX

	box_strings: ARRAY [STRING]

	text: STRING is
		do
			Result := combo_box.text
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_select is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	disable_edit is
		do
			combo_box.disable_edit
		end

	enable_edit is
		do
			combo_box.enable_edit
		end

end -- class AEL_V_LABELED_COMBO_BOX
