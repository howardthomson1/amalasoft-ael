note
  description: "Compound widget with a list widget and a label"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 001$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
To use this widget, create it using 'make_with_label',
giving it a string to use for the label component.
You can optionally set the width of the list widget.
Add the widget to your container and disable expansion of this widget
if desired.
If you wish to receive a notification on selection change (select actions)
of the list widget, you can register your agent using the
'set_notification_procedure' routine.  In the agent, simply query
the 'selected_item' routine for the committed value.
}"

class AEL_V_LABELED_LIST

inherit
	AEL_V_LABELED_WIDGET
		rename
			widget as listw,
			build_widget as build_listw,
			extend as box_extend
		redefine
			build_listw, set_actions
		end

create
  make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	set_actions is
		do
			Precursor
			listw.select_actions.extend (agent on_selection_change)
			listw.deselect_actions.extend (agent on_selection_change)
		end

	--|--------------------------------------------------------------

	build_listw is
		do
			create listw
		end

--|========================================================================
feature {EV_ANY} -- Components
--|========================================================================

	listw: EV_LIST

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: STRING) is
		require
			exists: v /= Void
		do
			listw.extend (create {EV_LIST_ITEM}.make_with_text (v))
		ensure
			was_added: listw.count = old listw.count + 1
			at_end: listw.last.text.is_equal (v)
		end

	--|--------------------------------------------------------------

	selected_items: LINKED_LIST [STRING] is
		local
			tl: DYNAMIC_LIST [EV_LIST_ITEM]
		do
			create Result.make
			tl := listw.selected_items
			from tl.start
			until tl.exhausted
			loop
				Result.extend (tl.item.text)
				tl.forth
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	all_items: LINKED_LIST [STRING] is
			-- All items in list, in sequence
		do
			create Result.make
			from listw.start
			until listw.exhausted
			loop
				Result.extend (listw.item.text)
				listw.forth
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	on_selection_change is
		do
			notify
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_multiple_selection is
		do
			listw.enable_multiple_selection
		end

end
