note
  description: "Compound widget with a multiline text field and a label"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 001$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
To use this widget, create it using 'make_with_label',
giving it a string to use for the label component.
You can optionally set the width of the text field, and the initial
text value.
Add the widget to your container and disable expansion of this widget
if desired.
If you wish to receive a notification on commit (enter actions) of the
string in the text widget, you can register your agent using the
'set_notification_procedure' routine.  In the agent, simply query
the 'text' routine for the committed value.
}"

class AEL_V_LABELED_MULTILINE_TEXT

inherit
	AEL_V_LABELED_WIDGET
		rename
			widget as textw,
			build_widget as build_textw
		redefine
			build_textw, set_actions
		end

create
  make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	set_actions is
		do
			resize_actions.extend (agent on_resize)
			textw.change_actions.extend (agent on_text_change)
			Precursor
		end

	--|--------------------------------------------------------------

	build_textw is
		do
			create {EV_TEXT}textw
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	textw: EV_TEXT_COMPONENT

--|========================================================================
feature -- Values
--|========================================================================

	text: STRING is
		do
			Result := textw.text
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_text (v: STRING) is
		do
			textw.set_text (v)
		end

	remove_text is
			-- 	Remove text from the text widget
		do
			textw.remove_text
		end

   set_read_only is
		do
			textw.disable_edit
		end

	--|--------------------------------------------------------------

	set_text_width (v: INTEGER) is
		require
			positive: v > 0
		do
			enable_item_expand (textw)
			textw.set_minimum_width (v)
--			disable_item_expand (textw)
		end

	--|--------------------------------------------------------------

	set_change_notification_procedure (v: like change_notification_procedure) is
		do
			change_notification_procedure := v
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER) is
		local
			ts: STRING
		do
			-- RFO
			-- There is an issue, at last on Windows, where the label
			-- and text are not being redrawn on resize, unless there's an
			-- intervening expose event.
			-- The set_text calls force lower-level invalidation
			ts := label.text.twin
			label.remove_text
			label.set_text (ts)

			ts := textw.text.twin
			textw.remove_text
			textw.set_text (ts)
		end

	--|--------------------------------------------------------------

	on_text_change is
		do
			if change_notification_procedure /= Void then
				change_notification_procedure.call ([Current])
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	default_text_width: INTEGER is
		do
			Result := 80
		end

	--|--------------------------------------------------------------

	change_notification_procedure: PROCEDURE [ANY, TUPLE [like Current]]

end -- class AEL_V_LABELED_MULTILINE_TEXT
