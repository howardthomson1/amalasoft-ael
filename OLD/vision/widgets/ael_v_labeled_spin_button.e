note
	description: "Compound widget with a spin button and a label"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    To use this widget, create it using 'make_with_attributes', giving
    it a minimum and maximum value, a label string and a position flag.
    Then add to your container and disable expansion of this widget if needed.
    If you wish to receive a notification on each change to this widget,
    you can register your agent using 'set_notification_procedure'.
    To access the current value, query the 'value' feature.
}"

class AEL_V_LABELED_SPIN_BUTTON

inherit
	AEL_V_LABELED_WIDGET
		rename
			widget as button, build_widget as build_button
		redefine
			build_button, set_actions, update_widget_rendition
		end

create
	make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (r1, r2: INTEGER; t: STRING; pf: BOOLEAN) is
			-- Create a new widget with a value range of r1 to r2 and a label
			-- of 't'.
			-- If position flag 'pf' is True, place the label to the left of
			-- the spin button, else place the label to the right.
		require
			range_valid:  r2 > r1
			label_exists: t /= Void and then not t.is_empty
		do
			minimum_value := r1
			maximum_value := r2
			make_with_label (t, pf)
		end

	--|--------------------------------------------------------------

	build_button is
		do
			create button
			if has_valid_range then
				update_button_values
			end

			button.set_minimum_width (default_spinner_width)
		end

	--|--------------------------------------------------------------

	set_actions is
		do
			button.change_actions.extend (agent on_value_change)
			Precursor
		end

--|========================================================================
feature {NONE} -- Widget rendition updates
--|========================================================================

	update_button_values is
		require
			button_exists: button /= Void
		do
			button.value_range.resize_exactly (minimum_value, maximum_value)
			button.set_value (initial_value)
		end

	--|--------------------------------------------------------------

	update_widget_rendition is
		do
			update_label_text
			update_button_values
			Precursor
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_value_change (v: INTEGER) is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	button: EV_SPIN_BUTTON

--|========================================================================
feature -- Values
--|========================================================================

	value: INTEGER is
		do
			Result := button.value
		end

	--|--------------------------------------------------------------

	minimum_value: INTEGER
	maximum_value: INTEGER

	initial_value: INTEGER is
		do
			Result := private_initial_value.max (minimum_value)
		end

	--|--------------------------------------------------------------

	has_valid_range: BOOLEAN is
		do
			Result := minimum_value < maximum_value
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_value (v: INTEGER) is
		require
			in_range: v >= minimum_value and v <= maximum_value
		do
			button.set_value (v)
		end

	--|--------------------------------------------------------------

	set_initial_value (v: INTEGER) is
		require
			in_range: v >= minimum_value and v <= maximum_value
		do
			private_initial_value := v
			update_button_values
		end

	--|--------------------------------------------------------------

	set_value_range (minv, maxv: INTEGER) is
		do
			minimum_value := minv
			maximum_value := maxv
			update_button_values
		end

	--|--------------------------------------------------------------

	set_spinner_width (v: INTEGER) is
		require
			positive: v > 0
		do
			enable_item_expand (button)
			button.set_minimum_width (v)
			disable_item_expand (button)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_initial_value: INTEGER

	default_spinner_width: INTEGER is
		do
			Result := 80
		end

end -- class AEL_V_LABELED_SPIN_BUTTON
