note
  description: "Compound widget with a single-line text field and a label"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 002$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
To use this widget, create it using 'make_with_label',
giving it a string to use for the label component.
You can optionally set the width of the text field, and the initial
text value.
Add the widget to your container and disable expansion of this widget
if desired.
If you wish to receive a notification on commit (enter actions) of the
string in the text widget, you can register your agent using the
'set_notification_procedure' routine.  In the agent, simply query
the 'text' routine for the committed value.
}"

class AEL_V_LABELED_TEXT

inherit
	AEL_V_LABELED_MULTILINE_TEXT
		redefine
			build_textw, textw, set_actions
		end

create
  make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	build_textw is
		do
			create textw
		end

	--|--------------------------------------------------------------

	set_actions is
		do
			textw.return_actions.extend (agent on_text_enter)
			Precursor
		end

--|========================================================================
feature -- Components
--|========================================================================

	textw: EV_TEXT_FIELD

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_text_enter is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

end -- class AEL_V_LABELED_TEXT
