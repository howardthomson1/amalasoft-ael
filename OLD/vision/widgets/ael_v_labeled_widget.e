note
  description: "Compound widget with a primitive widget and a label"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 002$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
This class is deferred and so cannot be instantiated as-is. Instead,
use one of its descendents, like AEL_V_LABELED_SPIN_BUTTON,
AEL_V_LABELED_BUTTON, AEL_V_LABELED_POSITIONER or AEL_V_LABELED_TEXT.
}"

deferred class AEL_V_LABELED_WIDGET

inherit
	EV_HORIZONTAL_BOX
	AEL_V_WIDGET
		undefine
			is_equal
		redefine
			make_components
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_label (v: STRING; pf: BOOLEAN) is
			-- Create a new widget with the label string 'v', and with label
			-- aligned either to right of widget (default) or to left of the
			-- widget (if 'pf' is True)
		require
			exists: v /= Void
		do
			label_on_left := pf
			label_text := v
			make
			set_border_width (1)
		end

--|--------------------------------------------------------------

	 make_components is
		local
			tc: EV_CELL
		do
			if label_on_left then
				create label.make_with_text (label_text)
				extend (label)
				label.align_text_right

				create tc
				extend (tc)
				tc.set_minimum_width (gutter_width)
				disable_item_expand (tc)
			end

			build_widget
			extend (widget)
			--disable_item_expand (widget)

			if not label_on_left then
				create tc
				extend (tc)
				tc.set_minimum_width (gutter_width)
				disable_item_expand (tc)

				create label.make_with_text (label_text)
				extend (label)
				label.align_text_left
			end

			Precursor
		end

	--|--------------------------------------------------------------

	build_widget is
		do
		ensure
			widget_exists: widget /= Void
		end

	--|--------------------------------------------------------------

	update_label_text is
		do
			if label_text = Void or else label_text.is_empty then
				label.remove_text
			else
				label.set_text (label_text)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	notification_proc: PROCEDURE [ANY, TUPLE[like Current]]

	notify is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	widget: EV_WIDGET is
		deferred
		end

	label: EV_LABEL

	label_text: STRING
	label_on_left: BOOLEAN

--|========================================================================
feature -- Status
--|========================================================================

	label_font: EV_FONT is
		do
			Result := label.font
		end

	--|--------------------------------------------------------------

	gutter_width: INTEGER is
		do
			Result := 5
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_label_font (v: EV_FONT) is
		do
			label.set_font (v)
		end

	--|--------------------------------------------------------------

	set_label_text (v: STRING) is
		do
			label_text := v
			update_label_text
		end

	set_label_width (v: INTEGER) is
		do
			label.set_minimum_width (v)
			disable_item_expand (label)
		end

	set_widget_width (v: INTEGER) is
		do
			widget.set_minimum_width (v)
			disable_item_expand (widget)
		end

	set_event_focus is
		do
			widget.set_focus
		end

	--|--------------------------------------------------------------

	set_notification_procedure (v: like notification_proc) is
		do
			notification_proc := v
		end

end -- class AEL_V_LABELED_WIDGET
