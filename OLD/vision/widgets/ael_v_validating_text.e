note
  description: "Compound widget with a text filed and a label"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 002$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
To use this widget, create it using 'make_with_label',
giving it a string to use for the label component.
You can optionally set the width of the text field, and the initial
text value.
Add the widget to your container and disable expansion of this widget
if desired.
If you wish to receive a notification on commit (enter actions) of the
string in the text widget, you can register your agent using the
'set_notification_procedure' routine.  In the agent, simply query
the 'text' routine for the committed value.
}"

class AEL_V_VALIDATING_TEXT

inherit
	AEL_V_LABELED_TEXT
		redefine
			set_actions, set_text, build_textw, on_text_change
		end

create
  make_with_label, make_for_upper, make_for_lower

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_for_upper (lb: STRING; tf: BOOLEAN) is
		do
			is_upper := True
			make_with_label (lb, tf)
		end

	make_for_lower (lb: STRING; tf: BOOLEAN) is
		do
			is_lower := True
			make_with_label (lb, tf)
		end

	build_textw is
		do
			if is_upper then
				create {AEL_V_CASING_TEXT}textw.make_for_upper
			elseif is_lower then
				create {AEL_V_CASING_TEXT}textw.make_for_lower
			else
				Precursor
			end
		end

	set_actions is
		do
--RFO			textw.change_actions.extend (agent on_text_change)
			textw.key_press_actions.extend (agent on_text_keypress)

			Precursor
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_text_keypress (key: EV_KEY) is
		require
			key_exists: key /= Void
		local
			kc: INTEGER
			ec: EV_KEY_CONSTANTS
		do
			create ec
			kc := key.code
			if kc = ec.Key_escape then
				if text_has_changed then
					execute_cancel_action
				end
			elseif kc = ec.Key_enter or kc = ec.Key_tab then
				if text_has_changed then
					execute_ok_action
				else
					tab_out (False)
				end
			else
			end
		end

	--|--------------------------------------------------------------

	on_text_change is
		local
			ts: STRING
			cf: BOOLEAN
		do
			if not clearing then
				ts := textw.text
				if ts = Void then
					if original_text = Void or else original_text.is_empty then
						cf := False
					else
						cf := True
					end
				else
					if original_text = Void or else not ts.is_equal(original_text) then
						cf := True
					end
				end
				set_changed (cf)
			end
		end

	--|--------------------------------------------------------------

	tab_out (go_back: BOOLEAN) is
		do
--RFO			if tab_out_proc /= Void then
--RFO				tab_out_proc.call ([Current, go_back])
--RFO			else
--RFO				--fake_tab_out
--RFO			end
		end

	--|--------------------------------------------------------------

	validation_function: FUNCTION [ANY, TUPLE [STRING], BOOLEAN]
			-- validation function should have the form:
			-- func( v : STRING ) : BOOLEAN
			-- Where v is the string to be validated

	update_proc: PROCEDURE [ANY, TUPLE [STRING]]
			-- update function should have the form:
			-- func( v : STRING )

 --|========================================================================
feature {NONE} -- Basic actions
 --|========================================================================

	execute_cancel_action is
			-- Revert the text to the original value
		do
			if original_original_text /= Void then
				set_text (original_original_text)
			else
				set_text (original_text)
			end
		end

	--|--------------------------------------------------------------

	execute_ok_action is
			-- Update values from the text widget
			-- set buttons insensitive
		local
			text_changed: BOOLEAN
		do
			if not executing_ok_action then
				executing_ok_action := True
				if original_text = Void then
					text_changed := not textw.text.is_empty
				else
					text_changed := not text.is_equal (original_text)
				end
				if text_changed then
					if text_is_valid then
						update_text
						if not action_is_from_focus_out then
							tab_out (False )
							--fake_tab_out
						end
					else
						-- sound off at the user
						set_invalid_text_color
					end
				elseif  textw.background_color = in_flux_color   then
					-- Force to look changed, even though value matches original
					if original_text /= Void and then text.is_equal (original_text) then
						if text_is_valid then
							update_text
							if not action_is_from_focus_out then
								tab_out (False)
								--fake_tab_out
							end
						else
							-- sound off at the user
							set_invalid_text_color
						end
					end
				else
					if not action_is_from_focus_out then
						-- simply tab out
						tab_out (False)
						--fake_tab_out
					end
				end
				executing_ok_action := False
			end
		end

	notify_observer (is_changed: BOOLEAN) is
		do
			if notification_proc /= Void then
				notification_proc.call ([Current])
			end
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	ever_had_text: BOOLEAN

	set_text (v: STRING) is
		do
			if not ever_had_text then
				original_text := v.twin
			end
			Precursor (v)
		end

 --|========================================================================
 feature -- Validation
 --|========================================================================

	 text_is_valid: BOOLEAN is
		do
			if validation_function /= Void then
				validation_function.call ([text])
				Result := validation_function.last_result
			else
				Result := True
			end
		end

	--|--------------------------------------------------------------

	set_validation_function (v: like validation_function) is
		do
			validation_function := v
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_changed (tf: BOOLEAN) is
		do
			if is_read_only then
				textw.set_background_color (K_color_read_only)
			else
				if tf then
					textw.set_background_color (in_flux_color)
				else
					textw.set_background_color (K_color_white_text_bg)
				end
				notify_observer (tf)
			end
		end

	--|--------------------------------------------------------------

	set_change_committed is
		do
			textw.set_background_color (K_color_committed_change)
			notify_observer (False)
		end

	disable_flux_color is
		do
			flux_color_disabled := True
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_upper: BOOLEAN
	is_lower: BOOLEAN

	flux_color_disabled: BOOLEAN

	text_has_changed : BOOLEAN is
		local
			ts: STRING
		do
			ts := textw.text
			if ts = Void then
				Result := original_text /= Void and then not original_text.is_empty
			else
				if (not ts.is_equal(original_text)) or
					(textw.background_color = in_flux_color) then
						Result := True
				end
			end
		end

	--|-------------------------------------------------------------

	text_is_original: BOOLEAN is
		local
			ts, os : STRING
		do
			ts := textw.text
			if  ts = Void   then
				ts := ""
			end
			os := original_original_text
			if  os = Void   then
				os := ""
			end
			Result := not os.is_equal (ts)
		end

  --|--------------------------------------------------------------

	is_changing: BOOLEAN is
		do
			Result := (textw.background_color = in_flux_color) and not updating
		end

  --|--------------------------------------------------------------

	clearing: BOOLEAN
	original_text: STRING
	original_original_text: STRING
	updating: BOOLEAN
	action_is_from_focus_out: BOOLEAN
	executing_ok_action: BOOLEAN
	is_read_only: BOOLEAN

 --|========================================================================
feature {NONE} -- Text change
 --|========================================================================

	update_text is
		local
			ts: STRING
		do
			ts := textw.text
			check
				valid_text: text_is_valid
			end
			if update_proc /= Void then
				updating := True
				update_proc.call ([text])
				updating := False
			end
				original_text := ts
			if original_original_text /= Void and then
				not original_original_text.is_equal(ts) then
					set_change_committed
			else
				set_changed (False)
			end
		end

	--|--------------------------------------------------------------

	set_invalid_text_color is
		do
			textw.set_background_color (K_color_error)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	K_color_white_text_bg: EV_COLOR is
		local
			ac: AEL_V_COLORS
		once
			create ac
			Result := ac.white
		end

	K_color_read_only: EV_COLOR is
		local
			ac: AEL_V_COLORS
		once
			create ac
			Result := ac.gray
		end

	K_color_error: EV_COLOR is
		local
			ac: AEL_V_COLORS
		once
			create ac
			Result := ac.orange
		end

	in_flux_color: EV_COLOR is
		do
			if flux_color_disabled then
				Result := K_color_in_flux2
			else
				Result := K_color_in_flux
			end
		end

	K_color_in_flux: EV_COLOR is
		local
			sc : EV_STOCK_COLORS;
		once
			create sc;
			Result := sc.cyan
		end

	K_color_in_flux2: EV_COLOR is
		local
			ac : AEL_V_COLORS;
		once
			create ac;
			Result := ac.extra_pale_blue
		end

	K_color_committed_change : EV_COLOR is
		local
			ac: AEL_V_COLORS
		once
			create ac
			Result := ac.light_goldenrod
		end

invariant

end -- class AEL_V_VALIDATING_TEXT
