note
	description: "A vertically oriented toolbar"
  legal: "Eiffel Forum License, v2; See license.tst."
  status: "Copyright 2007-2008, Amalasoft"
  date: "$Date: 2008/04/26 $"
  revision: "$Revision: 001$"
  source: "{
	  Amalasoft Corporation
	  273 Harwood Avenue
	  Littleton, MA 01460 USA
	  }"
  howto: "{
    This class is instantiated like any other AEL_V_WIDGET, by calling the
    'make' creation routine.
    To add buttons or other toolbar items, simply call extend as you would
    with the classic horizontal toolbar.
}"

class AEL_V_VERTICAL_TOOLBAR

inherit
	EV_VERTICAL_BOX
		rename
			extend as box_extend
		end
	AEL_V_WIDGET
		undefine
			is_equal
		end

create
	make

--|========================================================================
feature -- Components
--|========================================================================

	toolbars: LINKED_LIST [EV_TOOL_BAR] is
			-- List of subordinate toolbar widgets
		local
			tb: EV_TOOL_BAR
		do
			create Result.make
			from start
			until exhausted
			loop
				tb ?= item
				if (tb /= Void) then
					Result.extend (tb)
				end
				forth
			end
		end

--|========================================================================
feature -- Component setting
--|========================================================================

	extend (v: EV_TOOL_BAR_ITEM) is
			-- Add a new item
		require
			exists: v /= Void
		local
			tb: EV_TOOL_BAR
		do
			create tb
			box_extend (tb)
			disable_item_expand (tb)
			tb.extend (v)
		end

end -- class AEL_V_VERTICAL_TOOLBAR
