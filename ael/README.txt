README.txt - Introdution to the Amalasoft Eiffel Library

The Amalasoft Eiffel Library (AEL) is a collection of classes, arranged by
clusters, that provide what are hoped to be useful capabilities and 
abstractions.
In some cases, classes extend the capabilities of standard classes and
in other cases, they simply address a perceived need.
The clusters depend on the Eiffel base and other libraries and clusters
that come standard with Eiffel Studio from Eiffel Software.

This version of AEL is built and tested with Eiffel Studio 6.5, which
is the latest general relase version.
The AEL version compatible with the 6.2 release of Eiffel Studio is
located adjacent to this one, and is called 'ael_62'.

To use the clusters in this package, add them to your configuration file
as needed.  Typically, you will need to define exclude rules for each
cluster including the typical ones, and one for test.  Here is an example.

	<file_rule>
		<exclude>/EIFGENs$</exclude>
		<exclude>/.svn$</exclude>
		<exclude>/CVS$</exclude>
		<exclude>/test$</exclude>
	</file_rule>

If you use the AEL vision cluster, you will also have to exclude the
toolkit not used, as in:

		<exclude>/gtk$</exclude>
