Amalasoft Eiffel Library Application Cluster

To support void safety and provide a more flexible hierarchy, with easier
customization that doesn't compromise Void safety, the initialization
sequence within these classes builds on the default initialization squence
from the standard Eiffel librarys, as follows:

AEL_APPLICATION.make do
  default_create do -- AEL_ANY, redef from AEL_PRINTF
    default_create_stable_attributes do -- renamed from AEL_ANY.ceate...
      initialize_constants do end
      create_stable_attributes do end
    end
    if requires_implementation then
      create_implementation do end
      bind_implementation do
        implementation.assign_interface (Current)
      end
    end
    default_initialize do -- frozen, AEL_ANY
      initialization_state := 1
      ael_initialize do -- rename AEL_ANY.initialize, redefined, frozen
        {AEL_ANY}Precursor do
          initialization_state := 2
        end
        attach_application_root do
          if attached application_root then end
        end
        initialize_application do end
      end
    end
    set_default_create_executed do
      initialization_state := 3
    end
  end
  complete_initialization do -- redefined from AEL_ANY
    is_preparing_application := True
    if retrying then
      reset_context_on_retry do end
    else
      complete_initialization_pre_arg_parsing do end
      execute_argument_parsing do
        if argument_count = 0 then
          if no_args_denotes_help do
              Result := not no_args_is_not_a_help_request
              end then
            help_requested := True
          end
        else
          analyze_command_line do
            -- LOTS of code
          end
        end
      end
      complete_app_initialization do end
    end
  end
  if help_requested or has_argument_errors then
    if has_argument_errors then
      merge_arg_errors
    end
  else
    if attached one_and_done_procedure as lp then
      lp.call ([])
    else
      prepare_application
      if not has_error then
        execute_application
      end
    end
  end
  if help_requested then
    print_help
    clear_errors
  elseif has_error then
    report_errors
  end
  logit (0, application_name + " exiting")
end
