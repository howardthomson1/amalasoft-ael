note
	description: "Base class for AEL-based applications and components"
	system: "Amalasoft Eiffel LIbrary"
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/09/16 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class
	AEL_ANY

inherit
	AEL_PRINTF
		redefine
			default_create
		end

 --|========================================================================
feature {NONE} -- Creation and initialization
 --|========================================================================

	default_create
			-- Standard creation procedure.
			--
			-- Must be called exactly once during creation.
			--
			-- Creation sequence for all AEL-based objects is modeled
			-- closely after the Vision2 library:
			--
			-- default_create is defined once in AEL_ANY.
			-- create_ael_implementation is defined in descendants,
			-- but only if an implementation is required
			--   If required, the implementation is then bound to Current
			-- default_initialize is called and it in turns calls the
			-- descendent-redefineable routine 'initialize'
			--
			-- The normal pattern is that default_create will produce
			-- a properly initialized default object and any special
			-- creation routines (e.g. 'make') will call default_create
			-- and then do their extra work, including but not limited 
			-- to 'complete_initialization' (armed with state contracts).
		do
			check
				not_twice: not default_create_executed
			end
			create_stable_attributes
			if requires_implementation then
				create_implementation
				bind_implementation
			end
			default_initialize
			set_default_create_executed
		ensure then
			implemented: requires_implementation implies implementation_exists
			is_initialized: is_initialized
			default_created: default_create_executed
			is_in_default_state: is_in_default_state
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	requires_implementation: BOOLEAN
			-- Do instances of Current require an attached 
			-- implementation?
			-- Not generally
		do
		end

 --|========================================================================
feature {NONE} -- Optional implementation partner support
 --|========================================================================

	implementation: like implementation_type
		do
			check attached private_implementation as lpi then
				Result := lpi
			end
		end

	implementation_exists: BOOLEAN
			-- Is implementation attached?
		do
			Result := private_implementation /= Void
		ensure
			bound: Result implies implementation /= Void
		end

	create_implementation
			-- Create `implementation'.
			-- Must be redefined in each descendant that requires an 
			-- attached implementation
		require
			needed: requires_implementation
		do
		ensure
			implementation_created: requires_implementation implies
				attached implementation
		end

	--|--------------------------------------------------------------

	bind_implementation
			-- Bind 'implementation' to Current
		require
			exists: requires_implementation and then implementation /= Void
		do
			implementation.assign_interface (Current)
		ensure
			is_bound: implementation.interface = Current
		end

	private_implementation: detachable like implementation_type
		note
			option: stable
			attribute
		end

	implementation_type: AEL_ANY_I
		require False
		do
			check False then end
		ensure False
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_stable_attributes
			-- Create any attached or stabile attributes.
			-- Feature name is deliberately mispelled for conformance
		deferred
		end

	--|--------------------------------------------------------------

	frozen default_initialize
			-- Mark `Current' as initialized.
			-- This must be called during the creation procedure
			-- to satisfy the `is_initialized' invariant.
			-- Descendants may redefine 'initialize' to perform
			-- additional setup tasks.
		require
			not_already_initialized: is_uninitialized
		do
			initialization_state := 1
			initialize
		ensure
			is_initialized: is_initialized
		end

	--|--------------------------------------------------------------

	initialize
			-- Complete initializations required DURING the default 
			-- create sequence
		require
			default_initialized: is_default_initialized
		do
			initialization_state := 2
		ensure
			is_initialized: is_initialized
		end

	--|--------------------------------------------------------------

	complete_initialization
			-- Complete the initialization process
			-- To be called AFTER the default create sequence,
			-- typically from a 'make' routine
		require
			default_created: default_create_executed
			is_in_default_state: is_in_default_state
			initialized: is_initialized
		do
		end

 --|========================================================================
feature {NONE} -- Contract support
 --|========================================================================

	is_in_default_state: BOOLEAN
			-- Is `Current' in its default state?
			--| Checked by the postcondition of default create.
			--| Should be redefined and precursed in decendants to check new
			--| objects for proper default state initialization.
			--| Redefinitions must be pure queries with no side effects.
			--| eg: do Result := Precursor and ( check_local_state ) end
		do
			check
				should_only_be_called_from_default_create_postcondition: False
					-- (Check is ignored when it is called from postcondition.)
			end
			Result := True
		end

	set_default_create_executed
		do
			initialization_state := 3
		end

--|========================================================================
feature -- Contract support
--|========================================================================

	initialization_state: INTEGER
			-- State of initilization sequence
			-- uninitialized = 0
			-- default_initialized = 1
			-- initialized = 2
			-- default_create executed (completed) = 3
			-- initialization error < 0

	frozen is_default_initialized: BOOLEAN
			-- Has Current been initialized properly to default state
			-- and _only_ to teh default state?
		do
			Result := initialization_state = 1
		end

	frozen default_create_executed: BOOLEAN
			-- Has default_create for Current been executed already?
		do
			Result := initialization_state >= 3
		end

	is_initialized: BOOLEAN
			-- Has `Current' been initialized properly?
		do
			Result := initialization_state >= 2
		end

	is_uninitialized: BOOLEAN
			-- Has `Current' NOT been initialized, even partially?
		do
			Result := initialization_state = 0
		end

	has_initialization_error: BOOLEAN
			-- Has `Current' encountered an initialization error?
		do
			Result := initialization_state < 0
		end

	--|--------------------------------------------------------------
invariant
	is_initialized: is_initialized
	default_created: default_create_executed
	implemented: default_create_executed and then requires_implementation
		implies attached implementation as ai and then ai.interface = Current

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Sep-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by any classes for which the behavior
--| and structure of Current is desired.  Most useful for classes 
--| that have non-trivial initialization sequences or dependencies.
--|----------------------------------------------------------------------

end -- class AEL_ANY
