class AEL_ANY_I
-- Abstract implementation

--|========================================================================
feature {AEL_ANY} -- initialization
--|========================================================================

	assign_interface (v: like interface)
		require
			exists: v /= Void
		do
			check attached v then
				interface := v
			end
		ensure
			assigned: interface = v
		end

	interface: detachable AEL_ANY
		note
			option: stable
			attribute
		end

end -- class AEL_ANY_I
