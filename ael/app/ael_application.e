note
	description: "{
AEL Root class for a non-GUI application
Ensures valid creation sequence and share-ability, and includes common
features like logging, exception handling, common arguments, etc.

For GUI applications, use the av2 library classes
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_APPLICATION

inherit
	AEL_ANY
		rename
			create_stable_attributes as default_create_stable_attributes,
			initialize as ael_initialize
		redefine
			ael_initialize, complete_initialization
		end
	AEL_SPRT_MULTI_FALLIBLE
		undefine
			default_create, copy
		end
	APPLICATION_ENV
		undefine
			default_create, copy,
			errors, last_error_message, last_error_code, last_error, has_error,
			set_error_message, set_error, clear_errors, copy_errors, error_report,
			record_error, set_error_message_formatted
		end
	EXCEPTIONS
		export {NONE} all
		undefine
			default_create
		end
	UNIX_SIGNALS
		rename
			meaning as signal_meaning,
			catch as catch_signal,
			ignore as ignore_signal,
			is_defined as is_signal_defined
		undefine
			default_create
		end
	AEL_SPRT_ARGUMENTS
		rename
			optional_arguments as combined_optional_arguments
		undefine
			apf, default_create, copy_errors
		redefine
			combined_optional_arguments
		end

create
	make

create {AEL_APPLICATION_ENV}
	make_invalid

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	make
		do
			default_create
			complete_initialization

			if help_requested or has_argument_errors then
				if has_argument_errors then
					merge_arg_errors
				end
			else
				if attached one_and_done_procedure as lp then
					-- execute procedure and skip remainder of processing
					lp.call ([])
				else
					prepare_application
					if not has_error then
						execute_application
					end
				end
			end

			if help_requested then
				print_help
				clear_errors
			elseif has_error then
				report_errors
			end
			log_exit
		rescue
			terminate_abnormally
		end

	log_exit
			-- Write exiting msg to log
			-- Redefine in child to suppress msg
		do
--			logit (0, application_name + " exiting")
		end

	--|--------------------------------------------------------------

	make_invalid
			-- Create Current in invalid state to force exception
			-- (likely due to violated initialization sequence)
		local
			ex: EXCEPTIONS
		do
			create ex
			ex.raise ("In AEL_APPLICATION.make_invalid.")
			default_create
			complete_initialization
		end

	--|--------------------------------------------------------------

	reset_context_on_retry
			-- Reset running context in case of
			-- an exception-triggered retry
		do
		end

	--|--------------------------------------------------------------

	terminate_abnormally
			-- Set up for and exit from abnormal termination
			-- From make's rescue clause
		do
			log_and_print (exception_trace_text, Void)
			close_log
			die (1)
		end

	retriable_signals: HASH_TABLE [BOOLEAN, INTEGER]
			-- Table/map of signal numbers to retriability for Current
			-- Toe determin if a signal is retriable, execute
			--  if retriable_signals.item (signum) then ...
			-- No signals are retryable by default, but signals can be
			--
			-- added easily by retriable_signals.force (True,signum)
		once
			create Result.make (37)
		end

	--|--------------------------------------------------------------

	log_error (lvl: INTEGER; msg: STRING_8)
			-- Write the given message to the log file if the level is
			-- equal or less than current logging level
			-- N.B. This routine must be redefined to use an actual
			-- logging mechanism, if available.  For example, it could
			-- call logit from AEL_SPRT_LOGGING
		do
			logit (lvl, msg)
		end

	close_log
			-- Close an open logs files or pipe prior to termination
			-- N.B. This routine must be redefined to use an actual
			-- logging mechanism, if available.  For example, it could
			-- call logfile.close from AEL_SPRT_LOGGING
		do
			logfile.close
		end

--|========================================================================
feature -- Crash dump support
--|========================================================================

	dump_stack_trace (fn: STRING)
			-- Dump the latest stack trace to file named 'fn'
		require
			valid_name: fn /= Void and then not fn.is_empty
		local
			tf: PLAIN_TEXT_FILE
		do
			create tf.make_open_append (fn)
			tf.put_string (exception_trace_text)
			tf.close
		end

	exception_trace_text: STRING
			-- Text to put into stack trace
		do
			if attached exception_trace as tr then
				Result := tr
			else
				Result := "NO TRACE AVAILABLE"
			end
		ensure
			exists: Result /= Void
		end

 --|========================================================================
feature {NONE} -- User Initialization
 --|========================================================================

	create_stable_attributes
			-- Create any attached or stabile attributes.
			-- Feature name is deliberately mispelled for conformance
		do
		end

	--|--------------------------------------------------------------

	initialize_application
			-- Complete default_create initialization sequence
			-- Called DURING default_create sequence
		do
		end

	--|--------------------------------------------------------------

	frozen complete_initialization
			-- Complete the initialization process
			-- To be called AFTER the default create sequence,
			-- typically from a 'make' routine
		local
			retrying: BOOLEAN
		do
			is_preparing_application := True
			if retrying then
				reset_context_on_retry
			else
				complete_initialization_pre_arg_parsing
				execute_argument_parsing
				complete_app_initialization
			end
		rescue
			if is_developer_exception_of_name ("User database is corrupted.") then
				if has_error then
					log_and_print (
						"FATAL ERROR: %%s", error_report (<<False, false, True>>))
				end
				terminate_abnormally
			elseif is_signal and attached exception_manager.last_exception as lx
			 then
				 no_message_on_failure
				if attached lx.description as lxd then
					log_and_print (
						"Received signal %%d. %%s", <<signal, lxd.to_string_8>>)
				else
					log_and_print ("Received signal %%d.", <<signal>>)
				end
			elseif retriable_signals.item (signal) then
				retry
			else
				terminate_abnormally
			end
		end

	--|--------------------------------------------------------------

	complete_initialization_pre_arg_parsing
			-- Complete the initialization process, before arg parsing
			-- is begun
			-- Called AFTER the default create sequence,
		do
		end

	--|--------------------------------------------------------------

	execute_argument_parsing
			-- If application supports cmdline arguments, process them now
		do
			if argument_count = 0 then
				if no_args_denotes_help then
					help_requested := True
				end
			else
				analyze_command_line
			end
		end

	--|--------------------------------------------------------------

	complete_app_initialization
			-- Complete the initialization process, AFTER arg parsing
			-- is completed
		do
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	no_args_is_not_a_help_request: BOOLEAN
			-- Does the absense of cmdline arguments not denote
			-- a request for help/usage?
			-- Nota Bene - Called ONLY from no_args_denotes_help, used
			-- to provide a default false flag

	frozen ael_initialize
			-- <Precursor>
			-- Do not alter routine.
		do
			Precursor
			-- ensure that application root is initialized to current
			attach_application_root
			initialize_application
		end

	attach_application_root
			-- Force attachement at earliest opportunity (from initialize)
		do
			if attached application_root then end
		end

	--|--------------------------------------------------------------

--	frozen default_create_stable_attributes
	default_create_stable_attributes
			-- <Precursor>
			-- Do not alter routine.
		do
			initialize_constants
			create_stable_attributes
		end

	--|--------------------------------------------------------------

	initialize_constants
			-- As the first step in the default_create sequence,
			-- initialize any constant values upon with other components
			-- depend
		do
		end

	--|--------------------------------------------------------------

	prepare_application
			-- Complete application initialization prior to commencing
			-- execution proper
		do
			is_preparing_application := False
			application_is_prepared := True
		ensure
			initialized: application_is_prepared and
				not is_preparing_application
		end

	--|--------------------------------------------------------------

	execute_application
			-- Begin execution of application logic
			-- At this point, application is initialized and cmdline
			-- arguments are processed
		require
			no_errors: not has_error
			no_help: not help_requested
			initialized: application_is_prepared
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	help_requested: BOOLEAN
			-- Was help requested from the cmd line?

	--|--------------------------------------------------------------

	no_args_denotes_help: BOOLEAN
			-- Does the absense of cmdline arguments denote a request
			-- for help/usage?
		do
			Result := not no_args_is_not_a_help_request
		end

	--|--------------------------------------------------------------

	one_and_done_procedure: detachable PROCEDURE
			-- Procedure to invoke as one_and_done, if any
			-- Used to bypass normal request processing

	application_is_prepared: BOOLEAN
			-- Has Current been prepared for execution?

	is_preparing_application: BOOLEAN
			-- Is Current presently preparing for execution?

--|========================================================================
feature -- Support
--|========================================================================

	print_usage
			-- Echo usage text to stdout
		do
			if has_error then
				print_errors (number_of_errors > 1, False, False)
			end
			print (usage_text)
		end

	print_help
			-- Echo help text to stdout
		do
			print (help_msg_text)
		end

	usage_text: STRING
		do
			Result := aprintf (
				"Usage: %%s [-h] | {[-f <infile>]}%N", << application_name >>)
		end

	help_msg_text: STRING
		do
			Result := usage_text
		end

	input_filename: detachable STRING
			-- Optional input filename
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	report_errors
			-- Report (print, log, etc) accumulated errors
		require
			has_error: has_error
		do
			print_errors (False, False, False)
			if has_argument_errors then
				print (usage_text)
			end
		end

--|========================================================================
feature {NONE} -- Command-line argument support
--|========================================================================

	set_debug_from_arg (a: AEL_SPRT_CL_ARGUMENT)
		do
			enable_debug
			enable_dbprint
		end

	set_help_from_arg (a: AEL_SPRT_CL_ARGUMENT)
		do
			help_requested := True
		end

	set_input_file_from_arg (a: AEL_SPRT_CL_ARGUMENT)
		do
			input_filename := a.string_value
		end

	--|--------------------------------------------------------------

	optional_arguments: detachable like combined_optional_arguments
			-- Optional arguments added to inherited ones by Current
			-- Redefine in child as needed
		do
		end

	common_optional_args: ARRAY [AEL_SPRT_CL_ARGUMENT]
		once
			Result :=
				<<
			create {AEL_SPRT_CL_ARGUMENT}.make (
				K_argtype_char,'-',"d", agent set_debug_from_arg),
			create {AEL_SPRT_CL_ARGUMENT}.make (
				K_argtype_char,'-',"h", agent set_help_from_arg),
			create {AEL_SPRT_CL_ARGUMENT}.make (
				K_argtype_word,'-',"help", agent set_help_from_arg),
			create {AEL_SPRT_CL_ARGUMENT}.make (
				K_argtype_word_appended,'-',"-help", agent set_help_from_arg),
			create {AEL_SPRT_CL_ARGUMENT}.make (
				K_argtype_word_after,'-',"f", agent set_input_file_from_arg)
			>>
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	combined_optional_arguments: ARRAY [AEL_SPRT_CL_ARGUMENT]
			-- Command-line arguments that are not required,
			-- but are permitted
		local
			dflt_args: like combined_optional_arguments
			new_args: detachable like combined_optional_arguments
			i, j, lim, dflt_count, new_count: INTEGER
			default_arg: like new_default_argument
		once
			dflt_args := common_optional_args
			dflt_count := dflt_args.count
			new_args := optional_arguments
			if new_args /= Void then
				new_count := new_args.count
			end
			-- Create a new arg array large enough to hold common
			-- optional args and local args
			-- New array will have default_arg placeholders
			default_arg := new_default_argument
			create Result.make_filled (default_arg, 1, dflt_count + new_count)
			lim := dflt_args.upper
			-- Fill lower range of new args with dflt args,
			-- replacing the default arg
			from
				i := dflt_args.lower
			until i > lim
			loop
				Result.put (dflt_args.item (i), i)
				i := i + 1
			end
			-- If there are any local (aka new) args, add them
			-- after the dflt args, replacing the default arg
			if new_args /= Void then
				lim := Result.upper
				from j := 1
				until i > lim
				loop
					Result.put (new_args.item (j), i)
					i := i + 1
					j := j + 1
				end
			end
		end

invariant
	initialized: (not is_preparing_application) implies application_is_prepared

--|----------------------------------------------------------------------
--| History
--|
--| 004 6-Nov-2013
--|     Changed initialization logic to support merging with
--|     AEL_V2_APPLICATION to converge GUI and non GUI tracks.
--|     Renamed initialize to ael_initialize to avoid conflict with EV*
--| 003 5-Oct-2013
--|     Added considerable logic formerly in fe_connector_client to
--|     add more common features (e.g. logging)
--| 002 19-Aug-2013
--|     Inherited exceptions and arguments
--| 001 11-Sep-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create your APPLICATION_ROOT class and have it inherit this class.
--| There are several opportunities for specialization, most notably:
--|  reset_context_on_retry
--|      See feature comments
--|  retriable_signals
--|      See feature comments
--|  create_stable_attributes
--|      Add any attributes that must be created during default_create
--|  initialize_application
--|      For customizing DURING default create; see feature comments
--|  complete_initialization_pre_arg_parsing
--|      For specialization after default create and before arg
--|      parsing
--|  complete_app_initialization
--|      Performs arg parsing, so placement of Precursor in redefined
--|      implementation can have effect desired
--|  no_args_denotes_help
--|      If application execution with no args is acceptable, and
--|      does NOT denote a request for help, then redefine to be False
--|  prepare_application
--|      Called just prior to execute_application.  Redefine as needed
--|      but include call to Precursor to set assertions correctly.
--|  execute_application
--|      Called after initialization and arg parsing; the core exec
--|  usage_text
--|      Ripe for redefinition
--|  help_text
--|      Also ripe for redefinition
--|  required_arguments
--|      From AEL_SPRT_ARGUMENTS; look there for details
--|  optional_arguments
--|      Result can be redefined to include not-so-common optional
--|      arguments, to be added to combined_optional_arguments
--|----------------------------------------------------------------------

end -- end class AEL_APPLICATION
