note
	description: "{
Application environment common to elements in a system
Inherits project-supplied APPLICATION_ENV
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_APPLICATION_ENV

inherit
	AEL_SPRT_LOGGING
	AEL_SPRT_DEBUG_ENV

--|========================================================================
feature {NONE} -- Shared application root
--|========================================================================

	application_root_cell: CELL [detachable AEL_APPLICATION]
		once
			create Result.put (Void)
		end

--|========================================================================
feature -- Status Setting
--|========================================================================

	set_application_id (v: INTEGER)
			-- Set ID that identifies the current application amongst
			-- its peers on a given system
		do
			application_id_cell.put (v)
		end

--|========================================================================
feature {AEL_APPLICATION_ENV} -- Common core
--|========================================================================

	application_root_type: AEL_APPLICATION
		require False
		do
			check False then end
		ensure
			False
		end

	application_root: like application_root_type
			-- Root of application
			-- N.B. MUST be called first from AEL_APPLICATION
		do
			if attached {like application_root_type}application_root_cell.item as ar then
				Result := ar
			elseif attached {like application_root_type} Current  as ca then
				Result := ca
				application_root_cell.put (ca)
			else
				create Result.make_invalid
				check
					init_sequence_invalid: False
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	application_name: STRING
			-- Name of application
		do
			Result := env_implementation.application_name
		end

	application_id: INTEGER
			-- ID that identifies the current application amongst its 
			-- peers on a given system
		do
			Result := application_id_cell.item
		end

	Ks_log_dir_name: STRING
		once
			Result := env_implementation.Ks_log_path
		end

	Ks_log_parent_dir_name: DIRECTORY_NAME
			-- Path of log directory for this application
		once
			create Result.make_from_string (env_implementation.Ks_log_path_parent)
		end

	has_webkit_by_default: BOOLEAN
			-- Does platform have webkit installed by default?
		do
			Result := env_implementation.has_webkit_by_default
		end

	platform_type: STRING
			-- Gross-level platform type ("windows", "linux", etc.)
		do
			Result := env_implementation.platform_type
		ensure
			valid: is_valid_ael_platform_type (Result)
		end

--|========================================================================
feature -- Error recording
--|========================================================================

	errors: TWO_WAY_LIST [AEL_SPRT_ERROR_INSTANCE]
			-- List of recorded errors
		do
			Result := application_root.errors
		end

	--|--------------------------------------------------------------

	last_error_message: STRING
			-- Most recent error message
		do
			Result := application_root.last_error_message
		end

	last_error_code: INTEGER
			-- Most recent error code
		do
			Result := application_root.last_error_code
		end

	last_error: detachable AEL_SPRT_ERROR_INSTANCE
			-- Most recent error, if any
		do
			Result := application_root.last_error
		end

	has_error: BOOLEAN
			-- Has there been an error recorded?
		do
			Result := application_root.has_error
		end

	--|--------------------------------------------------------------

	set_error_message (msg: STRING)
			-- Set the error state with the given error message
			-- Set the error code to '1'
		do
			application_root.set_error_message (msg)
		end

	set_error (ec: INTEGER; msg: STRING)
			-- Set the error state with the given error code 'c'
			-- and message 'msg'
		do
			application_root.set_error (ec, msg)
		end

	set_error_message_formatted, record_error (fmt: STRING; args: detachable ANY)
			-- Set the error state with the given error message format 
			-- values (using printf conventions)
			-- Set the error code to '1'
		require
			msg_exists: fmt /= Void and then not fmt.is_empty
		do
			application_root.record_error (fmt, args)
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	clear_errors
			-- Clear error codes and messages, if any
		do
			application_root.clear_errors
		end

	copy_errors (other: AEL_SPRT_MULTI_FALLIBLE)
			-- copy error codes and messages from other
		do
			application_root.copy_errors (other)
		end

	--|--------------------------------------------------------------

	error_report (oa: ARRAY [BOOLEAN]): STRING
			-- Formatted error report, per options provided
			-- If 'oa'[1], then show sequence numbers
			-- If 'oa'[2] then show error codes
			-- If 'oa'[3], then clear errors when finished reporting
		do
			Result := application_root.error_report (oa)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	env_implementation: AEL_APPLICATION_ENV_IMP
			-- Implementation-specific env support
		deferred
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	application_id_cell: CELL [INTEGER]
			-- ID that identifies the current application amongst its 
			-- peers on a given system
		once
			create Result.put (0)
		end

--|========================================================================
feature -- Constants
--|========================================================================

	Ks_ael_platform_type_win: STRING = "windows"
	Ks_ael_platform_type_linux: STRING = "linux"
	Ks_ael_platform_type_mac: STRING = "mac"

	is_valid_ael_platform_type (v: STRING): BOOLEAN
			-- Is 'v' a valid (defined) platform type tag?
		do
			if attached v as tv and then not tv.is_empty then
				if tv ~ Ks_ael_platform_type_win then
					Result := True
				elseif tv ~ Ks_ael_platform_type_linux then
					Result := True
				elseif tv ~ Ks_ael_platform_type_mac then
					Result := True
				else
				end
			end
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by AEL_V2_WIDGET and therefor by its 
--| descendents. Each system must supply its own APPLICATION_ENV class
--| to hold anything that is environment-specific
--|----------------------------------------------------------------------

end -- class AEL_APPLICATION_ENV
