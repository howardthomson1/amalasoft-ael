note
	description: "{
Implementation-specific environment values
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/06/25 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_APPLICATION_ENV_IMP

inherit
	APPLICATION_NAME -- to be supplied by application itself
	AEL_ANY_I

--|========================================================================
feature -- Paths
--|========================================================================

	Ks_log_path: DIRECTORY_NAME
			-- Path of the root directory for this application
		once
			create Result.make_from_string (Ks_log_path_parent)
			Result.extend (application_name)
		ensure
			exists: Result /= Void
		end

	Ks_log_path_parent: STRING
			-- Path of the parent of the application log directory
		deferred
		end

	has_webkit_by_default: BOOLEAN
			-- Does platform have webkit installed by default?
		do
			-- Not likely; needs to be installed, with Chrome, Safari, etc.
		end

	platform_type: STRING = "linux"
			-- Gross-level platform type ("windows", "linux", etc.)

--|----------------------------------------------------------------------
--| History
--|
--| 001 19-Aug-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiated in a conce within AEL_APPLICATION_ENV, redefined in
--| descendent of AEL_APPLICATION_ENV as needed.
--|----------------------------------------------------------------------

end -- class AEL_APPLICATION_ENV_IMP
