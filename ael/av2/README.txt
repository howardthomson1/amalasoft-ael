README.txt - Introdution to the Amalasoft EiffelVision (av2) cluster

The Amalasoft Eiffel vision cluster (av2)is a collection of classes that extend
the vision2 library from Eiffel Software.  It depends on the Eiffel base and
vision2 libraries (available in open source or commercial form from
Eiffel Software www.eiffel.com) and is, like other Eiffel code, portable
across platforms.

This cluster also depends on the Amalasoft Printf cluster.

The calendar widget also depends on the Amalasoft Time cluster.

     ------------------------------------------------------------

The cluster includes the following subdirectories:

  calendar
    Calendar widget and supporting classes

  core
    Core vision classes, require by other higher-level classes

  figures
    Extensions to the EV_MODEL cluster

  graph
    Graphing and plotting classes

  implemenation
    Platform-specific classes (gtk, mswin)

  pixmaps
    Programmatically created pixmaps for things like save, open and such
    to obviate the annoying requirement for pixmap files

  widgets
    Custom widgets not warranting a separate subdirectory

  windows
    Custom window and dialog widgets
