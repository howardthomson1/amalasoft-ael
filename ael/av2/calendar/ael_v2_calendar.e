note
	description: "A portable calendar widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR

inherit
	AEL_V2_FRAME
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_actions,
			initialize_interface_values,
			post_initialize
		end

create
	make, make_for_gmt

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make_for_gmt
		-- Create the calendar as usual, but base the time on GMT (UTC)
		do
			use_gmt := True
			make
		end

 --|========================================================================
feature {NONE} -- Initialization (during default_create)
 --|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor
			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			create year.make (date.year, False)
			create month.make (date.month, year)

			create cal_box
			create date_box
			create cal_ctl_frame
			create cal_ctl
			create cal_month_ctl
			create cal_month_ctl_dn_btn
			create cal_month_ctl_label.make_with_text (current_month_string)
			create cal_month_ctl_up_btn
			create cal_year_ctl
			create cal_year_ctl_dn_btn
			create cal_year_ctl_label.make_with_text (current_year_string)
			create cal_year_ctl_up_btn
			create cal_tbl
		end

 --|========================================================================
feature {NONE} -- Initialization (after default_create)
 --|========================================================================

	build_interface_components
		-- Add the components that make up the calendar widget
		do
			Precursor

			extend (date_box)
			date_box.extend (cal_ctl_frame)
			cal_ctl_frame.extend (cal_ctl)

			cal_ctl.set_minimum_height (22)
			date_box.disable_item_expand (cal_ctl_frame)

			---------------

			cal_ctl.extend (cal_month_ctl)

			cal_month_ctl_dn_btn.set_pixmap (new_left_arrow_pixmap)
			cal_month_ctl.extend (cal_month_ctl_dn_btn)
			cal_month_ctl_dn_btn.set_minimum_width (12)
			cal_month_ctl.disable_item_expand (cal_month_ctl_dn_btn)

			cal_month_ctl.extend (cal_month_ctl_label)
			cal_month_ctl_label.set_minimum_width (90)
			cal_month_ctl.disable_item_expand (cal_month_ctl_label)

			cal_month_ctl_up_btn.set_pixmap (new_right_arrow_pixmap)
			cal_month_ctl.extend (cal_month_ctl_up_btn)
			cal_month_ctl_up_btn.set_minimum_width (12)
			cal_month_ctl.disable_item_expand (cal_month_ctl_up_btn)

			cal_ctl.disable_item_expand (cal_month_ctl)

			-------------------------

			cal_ctl.extend (create {EV_CELL })

			-------------------------

			cal_ctl.extend (cal_year_ctl)

			cal_year_ctl_dn_btn.set_pixmap (new_left_arrow_pixmap)
			cal_year_ctl.extend (cal_year_ctl_dn_btn)
			cal_year_ctl_dn_btn.set_minimum_width (12)
			cal_year_ctl.disable_item_expand (cal_year_ctl_dn_btn)

			cal_year_ctl.extend (cal_year_ctl_label)
			cal_year_ctl_label.set_minimum_width (60)
			cal_year_ctl.disable_item_expand (cal_year_ctl_label)

			cal_year_ctl_up_btn.set_pixmap (new_right_arrow_pixmap)
			cal_year_ctl.extend (cal_year_ctl_up_btn)
			cal_year_ctl_up_btn.set_minimum_width (12)
			cal_year_ctl.disable_item_expand (cal_year_ctl_up_btn)

			cal_ctl.disable_item_expand (cal_year_ctl)

			-------------------------

			date_box.extend (cal_tbl)
			cal_tbl.set_minimum_width (251)
			--date_box.disable_item_expand (cal_tbl)

			set_minimum_width (255)
		end

	--|--------------------------------------------------------------

	make_column_titles
		-- Create and add the day-of-week column headings
		local
			lb: like label_at_position
			la: like hdr_labels
			i: INTEGER
		do
			la := hdr_labels
			from i := 1
			until i > 7
			loop
				create lb.make_for_header (la.item(i), i = 1)
				lb.set_date (date)
				lb.set_minimum_width (35)
				cal_tbl.put_at_position (lb, i, 1, 1, 1)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
			cal_tbl.resize (7, 7)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			Precursor
			initialize_tooltips
			cal_tbl.set_row_spacing (1)
			cal_tbl.set_column_spacing (1)
			make_column_titles
			fill_blank_weeks
			set_date_to_present
			update_calendar_for_date
		end

	--|--------------------------------------------------------------

	initialize_tooltips
		do
			cal_month_ctl_dn_btn.set_tooltip (prev_month_tooltip_str)
			cal_month_ctl_up_btn.set_tooltip (next_month_tooltip_str)

			cal_year_ctl_dn_btn.set_tooltip (prev_year_tooltip_str)
			cal_year_ctl_up_btn.set_tooltip (next_year_tooltip_str)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor

			cal_month_ctl_dn_btn.select_actions.extend(
				agent on_ctl_select(cal_month_ctl_dn_btn))
			cal_month_ctl_up_btn.select_actions.extend(
				agent on_ctl_select(cal_month_ctl_up_btn))
			cal_year_ctl_dn_btn.select_actions.extend(
				agent on_ctl_select(cal_year_ctl_dn_btn))
			cal_year_ctl_up_btn.select_actions.extend(
				agent on_ctl_select(cal_year_ctl_up_btn))
		end

 --|========================================================================
feature {NONE} -- Components
 --|========================================================================

	cal_box: EV_HORIZONTAL_BOX

	date_box: EV_VERTICAL_BOX

	cal_ctl_frame: EV_FRAME
	cal_ctl: EV_HORIZONTAL_BOX

	cal_month_ctl: EV_HORIZONTAL_BOX
	cal_month_ctl_dn_btn: EV_BUTTON
	cal_month_ctl_label: EV_LABEL
	cal_month_ctl_up_btn: EV_BUTTON

	cal_year_ctl: EV_HORIZONTAL_BOX
	cal_year_ctl_dn_btn: EV_BUTTON
	cal_year_ctl_label: EV_LABEL
	cal_year_ctl_up_btn: EV_BUTTON

	cal_tbl: EV_TABLE

	--|--------------------------------------------------------------

	label_at_position (wd, wno: INTEGER): AEL_V2_CALENDAR_DAY_LABEL
		do
			create Result.make_for_sunday
			if attached {like label_at_position} cal_tbl.item_at_position (
				wd, wno) as lb
			 then
				 Result := lb
			end
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	date: DATE_TIME
	reference_date: detachable DATE_TIME
		-- Date at time of creation, or resetting to present

	--|--------------------------------------------------------------

	current_month_string: STRING
		do
			Result := cal_const.month_long_names.item (date.month)
		end

	current_year_string: STRING
		local
			ti: INTEGER
		do
			ti := date.year
			Result := ti.out
		end

 --|========================================================================
feature -- Update notification
 --|========================================================================

	set_change_notify_procedure (v: like change_notify_proc)
		-- Define the agent to call when there is a change to the calendar.
		-- For example, when the year or month is changed by the user, or
		-- the user selects a day cell.
		do
			change_notify_proc := v
		end

 --|========================================================================
feature -- Value setting
 --|========================================================================

	set_use_gmt (tf: BOOLEAN)
		do
			use_gmt := tf
		end

	--|--------------------------------------------------------------

	set_date_to_present
		do
			if use_gmt then
				create reference_date.make_now_utc
				create date.make_now_utc
			else
				create reference_date.make_now
				create date.make_now
			end
			update_calendar_for_date
		end

	--|--------------------------------------------------------------

	set_date (v: like date)
		require
			date_exists: v /= Void
		do
			if date /= v then
				date := v
				update_calendar_for_date
			end
		end

	--|--------------------------------------------------------------

	select_date (v: like date)
			-- Set date to 'v' and show as selected
		require
			date_exists: v /= Void
		local
			wno, wd: INTEGER
		do
			set_date (v)

			wd := v.date.day_of_the_week
			wno := dtr.week_in_month (v.date)
			clear_day_selections
			-- The week number of one less than the row number because 
			-- row 1 is the header row
			label_at_position (wd, wno + 1).enable_selected
		end

 --|========================================================================
feature {NONE} -- Basic operation
 --|========================================================================

	update_calendar_for_date
		local
			nw: INTEGER
			idx: INTEGER
		do
			--| The calendar matrix is 7 days wide (columns)
			--| by 6 weeks long (rows), not including the header row
			--| This accomodates all possible week starts and month lengths.
			--| The "largest" month would be one with 31 days starting on a 
			--| Saturday
			--| The "smallest" month would be February, starting on a Sunday

			create year.make (date.year, False)
			create month.make (date.month, year)
			nw := month.number_of_weeks
			number_of_weeks := nw

			-- Weeks 2 through 4 will have all days updated, so
			-- there is no need to clear them first

			clear_week (1)
			clear_week (5)
			clear_week (6)
			
			from idx := 1
			until idx > nw
			loop
				fill_week (month, idx)
				idx := idx + 1
			end

			cal_month_ctl_label.set_text (current_month_string)
			cal_year_ctl_label.set_text (current_year_string)
		end

	--|--------------------------------------------------------------

 	fill_week (mo: AEL_DT_CALENDAR_MONTH; wno: INTEGER)
		require
			month_exists: mo /= Void
		local
			days: ARRAY [ STRING ]
			d, idx, nw: INTEGER
			lb: like label_at_position
			wk: AEL_DT_CALENDAR_WEEK
		do
			nw := mo.number_of_weeks
			idx := wno + 1

			create wk.make (wno, month)
			days := wk.days
			from d := 1
			until d > 7
			loop
				lb := label_at_position (d, idx)
				-- If is today, then set reference date
				lb.set_date (date)
				lb.set_text (days.item(d))
				lb.disable_selected
				d := d + 1
			end
		end

	--|--------------------------------------------------------------

	clear_week (wno: INTEGER)
		local
			d, w: INTEGER
			lb: like label_at_position
		do
			w := wno + 1
			from d := 1
			until d > 7
			loop
				lb := label_at_position (d, w)
				lb.remove_text
				d := d + 1
			end
		end

	--|--------------------------------------------------------------

	clear_day_selections
		do
			from cal_tbl.start
			until cal_tbl.exhausted
			loop
				if attached {like label_at_position} cal_tbl.item as lb then
					lb.disable_selected
				end
				cal_tbl.forth
			end
			cal_tbl.start
		end

	--|--------------------------------------------------------------

	fill_blank_weeks
		local
			d, w: INTEGER
			lb: like label_at_position
		do
			from w := 2
			until w > 7
			loop
				from d := 1
				until d > 7
				loop
					create lb.make_with_day (d)
					lb.set_date (date)
					lb.set_selection_proc (agent on_day_select)
					lb.set_minimum_width (35)
					cal_tbl.put_at_position (lb, d, w, 1, 1)
					d := d + 1
				end
				w := w + 1
			end
		end

 --|========================================================================
feature {NONE} -- Private attributes
 --|========================================================================

	number_of_weeks: INTEGER
	year: AEL_DT_CALENDAR_YEAR
	month: AEL_DT_CALENDAR_MONTH

	use_gmt: BOOLEAN

 --|========================================================================
feature {NONE} -- Actions and agents
 --|========================================================================

	on_day_select (lb: like label_at_position)
		do
			clear_day_selections
			lb.enable_selected
			--| update current date to new day
			date.date.set_day (lb.day_of_month)
			notify_clients_of_change
		end

	--|--------------------------------------------------------------

	on_ctl_select (b: like cal_month_ctl_dn_btn)
		do
			if b = cal_month_ctl_dn_btn then
				date.date.month_back
			elseif b = cal_month_ctl_up_btn then
				date.date.month_forth
			elseif b = cal_year_ctl_dn_btn then
				date.date.year_back
			elseif b = cal_year_ctl_up_btn then
				date.date.year_forth
			else
			end
			clear_day_selections
			update_calendar_for_date
			notify_clients_of_change
		end

	--|--------------------------------------------------------------

	notify_clients_of_change
		do
			if attached change_notify_proc as p then
				p.call ([ Current ])
			end
		end

 	change_notify_proc: detachable PROCEDURE [TUPLE [ like Current ] ]

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	hdr_labels: ARRAY [ STRING ]
		once
			Result := cal_const.day_short_names
		end

 	cal_const: AEL_DATE_TIME_CONSTANTS
		once
			create Result
		end

	dtr: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

	--|--------------------------------------------------------------

	new_left_arrow_pixmap: AEL_V2_SMALL_LEFT_ARROW_PIXMAP
			-- Newly created (on each call) left arrow pixmap
		do
			create Result.make
		end

	new_right_arrow_pixmap: AEL_V2_SMALL_RIGHT_ARROW_PIXMAP
			-- Newly created (on each call) left arrow pixmap
		do
			create Result.make
		end

	--|--------------------------------------------------------------

	prev_month_tooltip_str: STRING
		do
			Result := "Go to previous month"
		end

	next_month_tooltip_str: STRING
		do
			Result := "Go to next month"
		end

	prev_year_tooltip_str: STRING
		do
			Result := "Go to previous year"
		end

	next_year_tooltip_str: STRING
		do
			Result := "Go to next year"
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using either 'make' or 'make_for_gmt'.
--| Then add to your container and disable expansion of this widget.
--| Because the parent class is EV_FRAME, you have the option to define
--| a frame label string, via 'set_text'.
--|
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_change_notify_procedure'.
--|
--| To change the apparent date programmatically, create a new DATE_TIME
--| object and provide it to the widget using 'set_date'.
--|
--| To reset the widget to the current date, call 
--| 'set_date_to_present'.
--|
--| To create a calendar in different languages, provide an alternate
--| implementation of AEL_DATE_TIME_NAMES using the Eiffel cluster
--| mechanism.
--|----------------------------------------------------------------------

end -- class AEL_V_CALENDAR
