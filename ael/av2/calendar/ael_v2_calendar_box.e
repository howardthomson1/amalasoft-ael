note
	description: "A container hold a calendar widget and other controls"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_BOX

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_values,
			initialize_interface_actions,
			post_initialize,
			update_widget_rendition
		end

create
	make, make_for_gmt

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make_for_gmt
			-- Create Current with current GMT time
		do
			use_gmt := True
			make
		end

 --|========================================================================
feature {NONE} -- Initialization (during default_create)
 --|========================================================================

	create_interface_objects
		do
			if use_gmt then
				create original_date.make_now_utc
				create date.make_now_utc
				create calendar.make_for_gmt
			else
				create original_date.make_now
				create date.make_now
				create calendar.make
			end
			create upper_box
		end

 --|========================================================================
feature {NONE} -- Initialization (after default_create)
 --|========================================================================

	build_interface_components
			-- Create subordinate components.
		do
			Precursor

			extend (upper_box)

			upper_box.extend (calendar)
			calendar.set_text ("Date")
			upper_box.disable_item_expand (calendar)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
			set_border_width (10)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Define the events and responses for the subordinate
			-- components
		do
			Precursor
			calendar.set_change_notify_procedure (agent on_calendar_change)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			Precursor
		end

 --|========================================================================
feature {NONE} -- Components
 --|========================================================================

	upper_box: EV_HORIZONTAL_BOX
	calendar: AEL_V2_CALENDAR

 --|========================================================================
feature -- Queries
 --|========================================================================

	date: DATE

	date_time: DATE_TIME
			-- equivalent to date, used for updating calendar widget
		do
			create Result.make_by_date (date)
		end

	use_gmt: BOOLEAN

 --|========================================================================
feature -- Update agents
 --|========================================================================

	set_change_notify_procedure (v: like change_notify_proc)
			-- Define the procedure to execute when a change has 
			-- occurred
		do
			change_notify_proc := v
		end

	change_notify_proc: detachable PROCEDURE [TUPLE [like Current]]
			-- The procedure to execute when a change has occurred

 --|========================================================================
feature -- Value setting
 --|========================================================================

	set_date_to_present
			-- Reset defined date to be the present date
		do
			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	set_date (v: like date)
			-- Set the defined date to 'v'
		require
			date_exists: v /= Void
		do
			date := v
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	set_date_from_calendar
			-- Set the defined date to the value in the calendar control
		do
			date_changing := True
			update_widget_rendition
			notify_clients_of_change
			date_changing := False
		end

 --|========================================================================
feature {NONE} -- Basic operation
 --|========================================================================

	updating: BOOLEAN
			-- Is Current updating rendition presently?

	date_changing: BOOLEAN
			-- Is date being changed presently?

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Update controls to reflect the current state
		do
			if not updating then
				updating := True
				Precursor
				if not date_changing then
					calendar.select_date (date_time)
				end
				updating := False
			end
		end

 --|========================================================================
feature {NONE} -- Private attributes
 --|========================================================================

	original_date: like date

 --|========================================================================
feature {NONE} -- Callbacks and agents
 --|========================================================================

	on_calendar_change (c: like calendar)
			-- Respond to calendar change events
		do
			--| post the event to the main event loop, not blockin
			application_root.do_once_on_idle (agent set_date_from_calendar)
		end

	--|--------------------------------------------------------------

	notify_clients_of_change
			-- Notify and clients that have registered
		do
			if attached change_notify_proc as p then
				p.call ([ Current ])
			end
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	dtr: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 003 31-Aug-2009
--|     Reorganized components to put current time on bottom and time
--|     setting at right in separate frames.  Made current time not
--|     resettable and not influenced by time setting controls.
--|     Added time setting time display with reset to current button
--|     Added gmt conversion so time setting controls are not 
--|     clobbered when switching between GMT and local times
--|     Added 'calendar.select_date' to set and select date
--|     Used do_once_on_idle to update calendar-initiated changes
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--| 001 21-Apr-2008
--|     Original published version
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using either 'make' or 'make_for_gmt'.
--| Then add to your container.
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_BOX
