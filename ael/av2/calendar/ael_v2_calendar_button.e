note
	description: "Compound widget with a button and label, for selecting a date and time"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_BUTTON

inherit
	AEL_V2_LABELED_TEXT
		redefine
			create_widget, build_interface_components,
			initialize_interface_actions,
			initialize_interface_images,
			on_text_enter
		end

create
	make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (lb: STRING; t: DATE_TIME; pf: BOOLEAN)
			-- Create a new widget with the label string 'lb', intial
			-- date/tim 't' and with text aligned either to right of
			-- button (default) or to left of button (if 'pf' is True)
		require
			label_exists: lb /= Void
		do
			date_time := t
			make_with_label (lb, pf)
		end

	--|--------------------------------------------------------------

	create_widget
		do
			Precursor
			create button
		end

--|========================================================================
feature {NONE} -- Initialization {after default_create)
--|========================================================================

	build_interface_components
		do
			Precursor
			textw.disable_edit
			--draw_button
			extend (button)
			disable_item_expand (button)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			button.select_actions.extend (agent on_button_press)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_images
		local
			pm: AEL_V2_CALENDAR_PIXMAP
		do
			create pm.make
			pm.set_minimum_size (pm.width, pm.height)
			button.set_pixmap (pm)
		end

--|========================================================================
feature -- Status
--|========================================================================

	button: EV_BUTTON
	date_time: DATE_TIME

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press
		local
--			cd: AEL_V2_CALENDAR_DIALOG
			cd: AEL_V2_CALENDAR_TIME_DIALOG
		do
			create cd.make_with_date_time ("Set Date/Time", date_time)
			cd.set_apply_action_proc (agent on_calendar_change)
			cd.show_modal_to_window (nearest_window)
		end

	--|--------------------------------------------------------------

	on_calendar_change (a: ANY)
		do
			if attached {DATE_TIME} a as d then
				date_time := d
				set_text (d.out)
				notify
			end
		end

	--|--------------------------------------------------------------

	on_text_enter
		do
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| The button has a label showing a date/time string.
--| Default action on the button invokes a calendar dialog, the selection
--| from which then becomes the label's new date/time.
--| To use this widget, create it using 'make_with_attributes',
--| giving it a label string and a position flag.
--| Then add to your container and disable expansion of this widget if 
--| needed.
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--| Query the 'date_time' feature to get the date/time currently shown in
--| the label.
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_BUTTON
