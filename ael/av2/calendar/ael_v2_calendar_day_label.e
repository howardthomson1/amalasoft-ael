note
	description: "An individual day cell for a the AEL_V2_CALENDAR widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_DAY_LABEL

inherit
	AEL_V2_LABEL
		redefine
			set_text,
			initialize_interface_actions,
			update_widget_rendition
		end

create
	make, make_with_day, make_with_label, make_for_sunday, make_for_header

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make_with_day (v: INTEGER)
			-- Create Current for Sunday
		do
			if v = 1 then
				make_for_sunday
			else
				make
			end
		end

	--|--------------------------------------------------------------

	make_for_header (v: STRING; sf: BOOLEAN)
			-- Create Current for use in calendar header
		require
			text_exists: v /= Void
		do
			is_header := True
			is_sunday := sf
			make_with_text (v)
		end

	--|--------------------------------------------------------------

	make_with_label (v: STRING; sf: BOOLEAN)
		require
			text_exists: v /= Void
		do
			is_sunday := sf
			make_with_text (v)
		end


	--|--------------------------------------------------------------

	make_for_sunday
		do
			is_sunday := True
			make
		end

 --|========================================================================
feature {NONE} -- Agents and actions
 --|========================================================================

	initialize_interface_actions
		do
			Precursor
			pointer_button_press_actions.extend (agent on_btn_press)
		end

	--|--------------------------------------------------------------

	on_btn_press (x, y, button: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
		do
			if has_text and attached selection_proc as p then
				p.call ([Current])
			end
		end

	selection_proc: detachable PROCEDURE [TUPLE [ like Current ] ]

 --|========================================================================
feature {AEL_V2_CALENDAR} -- Status setting
 --|========================================================================

	set_text (v: STRING_GENERAL)
		do
			Precursor (v)
			update_widget_rendition
		end

	enable_selected
		do
			is_selected := True
			update_widget_rendition
		end

	disable_selected
		do
			is_selected := False
			update_widget_rendition
		end

 --|========================================================================
feature {AEL_V2_CALENDAR} -- Client-settable actions
 --|========================================================================

	set_selection_proc (v: like selection_proc)
			-- Set the agent to call when this label is selected
		do
			selection_proc := v
		end

 --|========================================================================
feature {AEL_V2_CALENDAR} -- Status
 --|========================================================================

	is_sunday: BOOLEAN
	is_header: BOOLEAN
	is_selected: BOOLEAN

	--|--------------------------------------------------------------

	has_text: BOOLEAN
		do
			Result := attached text as t and then not t.is_empty
		end

	--|--------------------------------------------------------------

	is_today: BOOLEAN
			-- Is this day actually today?
		local
			tm: like date
		do
			if attached date as dt then
				create tm.make_now
				if tm.month = dt.month then
					Result := tm.day = day_of_month
				end
			end
		end

	--|--------------------------------------------------------------

	is_reference_date: BOOLEAN
			-- Is the date associated with this label the same as the date
			-- defined to be the reference date?
		do
			if attached reference_date as rd then
				if attached date as dt and then rd.month = dt.month then
					Result := rd.day = day_of_month
				end
			end
		end

	--|--------------------------------------------------------------

	set_date (v: like date)
			-- Set the date associated with this day label
		do
			date := v
		end

	--|--------------------------------------------------------------

	set_reference_date (v: like date)
			-- Set the given date to be a reference date.	This could be a
			-- a start date, or end date, or whatever is deemed important.
			-- This is not required.
		do
			reference_date := v
		end

	--|--------------------------------------------------------------

	day_of_month: INTEGER
		do
			if has_text and not is_header then
				Result := text.to_integer
			end
		end

	--|--------------------------------------------------------------

	date: detachable DATE_TIME
	reference_date: detachable DATE_TIME

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	update_widget_rendition
		do
			Precursor
			set_background_color (bg_color)
			set_foreground_color (fg_color)
		end

	--|--------------------------------------------------------------

	bg_color: EV_COLOR
		do
			Result := colors.light_gray
			if not is_header then
				if is_selected then
					Result := colors.cyan
				else
					if is_today then
						Result := colors.light_gray
					elseif is_reference_date then
						Result := colors.pale_green
					else
						Result := colors.white
					end
				end
			end
		end

	--|--------------------------------------------------------------

	fg_color: EV_COLOR
		do
			Result := colors.black
			if not is_header then
				if is_today then
					if is_sunday then
						Result := colors.dark_red
					else
						Result := colors.blue
					end
				elseif is_reference_date then
					if is_sunday then
						Result := colors.dark_red
					else
						Result := colors.blue
					end
				else
					if is_sunday then
						Result := colors.red
					else
						Result := colors.black
					end
				end
			end
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| This widget is a component of AEL_V2_CALENDAR and is not to be created
--| and used by itself.
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_DAY_LABEL
