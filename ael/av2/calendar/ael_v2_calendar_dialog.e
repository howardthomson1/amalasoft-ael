note
	description: "A dialog containing a calendar widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2015/06/05 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions,
			initialize_interface_values,
			window_initial_height, window_initial_width,
			update_committed_value,
			update_widget_rendition,
			committed_value, on_show
		end

create
	make_now, make_with_date

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_now (ts: STRING)
		do
			is_now := True
			create original_date.make_now
			make_with_title (ts)
		end

	--|--------------------------------------------------------------

	make_with_date (ts: STRING; d: DATE)
		do
			original_date := d
			make_with_title (ts)
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor
			if not is_now then
				date := original_date.twin
			end
			create calbox.make
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		local
			tc: EV_CELL
			hb: EV_HORIZONTAL_BOX
		do
			create hb
			working_area.extend (hb)

			create tc
			hb.extend (tc)
			tc.set_minimum_width (10)

			hb.extend (calbox)

			create tc
			hb.extend (tc)
			tc.set_minimum_width (10)

			-- Init before setting agents
			if attached date as d then
				calbox.set_date (d)
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			calbox.set_change_notify_procedure (agent on_calendar_change)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
			ok_button.disable_sensitive
		end

--|========================================================================
feature -- Status
--|========================================================================

	date: detachable DATE
	original_date: DATE

	calbox: AEL_V2_CALENDAR_BOX

	is_now: BOOLEAN
			-- Was Current created for 'now' (vs another date)

	committed_value: like date

	--|--------------------------------------------------------------

	has_changed: BOOLEAN
			-- Has the reservation changed during the life of Current?
		do
			Result := not calbox.date.is_equal (original_date)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update_committed_value
		do
			committed_value := date
			Precursor
		end

--|========================================================================
feature {NONE} -- Rendition support
--|========================================================================

	update_widget_rendition
		do
			Precursor
			if has_changed then
				ok_button.enable_sensitive
			else
				ok_button.disable_sensitive
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_calendar_change (w: like calbox)
			-- Respond to a change in 'w'
		do
			if has_changed then
				-- Create/update the date object
				date := calbox.date.twin
			end
			update_widget_rendition
		end

	on_show
			-- Repond to initial 'show' event
		do
			Precursor
			ok_button.disable_sensitive
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	window_initial_height: INTEGER = 225
	window_initial_width: INTEGER = 300

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 05-Jun-2015
--|     Adapted from what is now called AEL_V2_CALENDAR_TIME_DIALOG
--|     Removed time-related values and logic, to be date-only
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using either 'make' or 'make_with_title'.
--| Then open it (pref. modal)
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_DIALOG
