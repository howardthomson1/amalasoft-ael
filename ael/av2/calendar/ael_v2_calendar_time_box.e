note
	description: "A container hold a calendar widget and other controls"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_TIME_BOX

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			destroy,
			create_interface_objects,
			build_interface_components,
			initialize_interface_values,
			initialize_interface_actions,
			post_initialize,
			update_widget_rendition
		end

create
	make, make_for_gmt

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make_for_gmt
			-- Create Current with current GMT time
		do
			use_gmt := True
			make
		end

 --|========================================================================
feature {NONE} -- Initialization (during default_create)
 --|========================================================================

	create_interface_objects
		do
			if use_gmt then
				create original_date.make_now_utc
				create date.make_now_utc
				create calendar.make_for_gmt
			else
				create original_date.make_now
				create date.make_now
				create calendar.make
			end

			create upper_box
			create lower_box

			create time_setting_frame.make_with_text ("Time Setting")
			create time_setting_box
			create time_set_now_box
			create time_set_now_btn.make_with_text ("Set to Current")
			create time_setting_textw.make_with_text (current_time_string)
			create hour_box
			create hour_label.make_with_text ("Hour:")
			create hour_ctl.make_with_value_range (hours_range)
			create minute_box
			create minute_label.make_with_text ("Minute:")
			create minute_ctl.make_with_value_range (minutes_range)
			create second_box
			create second_label.make_with_text ("Second:")
			create second_ctl.make_with_value_range (seconds_range)

			create time_now_frame.make_with_text ("Time")
			create time_box
			create current_time_box
			create time_label.make_with_text ("Current Time:")
			create time_textw.make_with_text (current_time_string)
			create gmt_box
			create gmt_btn.make_with_text ("Show time as GMT")

			create update_timer
		end

 --|========================================================================
feature {NONE} -- Initialization (after default_create)
 --|========================================================================

	build_interface_components
			-- Create subordinate components.
			-- Upper box is a horiz box with a calendar in left
			-- and time-setting box on right
			-- Lower box contains a time-now box
		do
			Precursor

			extend (upper_box)

			upper_box.extend (calendar)
			calendar.set_text ("Date")
			upper_box.disable_item_expand (calendar)

			--------------------------

			build_time_setting_box
			upper_box.extend (time_setting_frame)

			--------------------------

			build_time_box
			extend (time_now_frame)
		end

	--|--------------------------------------------------------------

	build_time_setting_box
			-- Construct the frame set that houses the time setting 
			-- controls
		local
			tc: EV_CELL
		do
			time_setting_frame.extend (time_setting_box)

			time_setting_box.extend (time_set_now_box)
			time_set_now_box.set_minimum_height (25)
			time_setting_box.disable_item_expand (time_set_now_box)

			create tc
			time_set_now_box.extend (tc)
			tc.set_minimum_width (10)
			time_set_now_box.disable_item_expand (tc)

			time_set_now_btn.set_tooltip ("Press to reset time to current")
			time_set_now_box.extend (time_set_now_btn)
			time_set_now_btn.align_text_right

			create tc
			time_set_now_box.extend (tc)
			tc.set_minimum_width (5)
			time_set_now_box.disable_item_expand (tc)

			time_set_now_box.extend (time_setting_textw)
			time_setting_textw.set_minimum_width (70)
			time_set_now_box.disable_item_expand (time_setting_textw)
			time_setting_textw.align_text_left

			time_set_now_box.extend (create {EV_CELL})

			-------------------------

			time_setting_box.extend (create {EV_CELL})

			time_setting_box.extend (hour_box)
			hour_box.set_minimum_height (25)
			time_setting_box.disable_item_expand (hour_box)

			hour_box.extend (hour_label)
			hour_label.align_text_right

			create tc
			hour_box.extend (tc)
			tc.set_minimum_width (10)
			hour_box.disable_item_expand (tc)

			hour_box.extend (hour_ctl)
			hour_ctl.set_minimum_width (70)
			hour_box.disable_item_expand (hour_ctl)

			-------------------------

			time_setting_box.extend (create {EV_CELL})

			time_setting_box.extend (minute_box)
			minute_box.set_minimum_height (25)
			time_setting_box.disable_item_expand (minute_box)

			minute_box.extend (minute_label)
			minute_label.align_text_right

			create tc
			minute_box.extend (tc)
			tc.set_minimum_width (10)
			minute_box.disable_item_expand (tc)

			minute_box.extend (minute_ctl)
			minute_ctl.set_minimum_width (70)
			minute_box.disable_item_expand (minute_ctl)

			-------------------------

			time_setting_box.extend (create {EV_CELL})

			time_setting_box.extend (second_box)
			second_box.set_minimum_height (25)
			time_setting_box.disable_item_expand (second_box)

			second_box.extend (second_label)
			second_label.align_text_right

			create tc
			second_box.extend (tc)
			tc.set_minimum_width (10)
			second_box.disable_item_expand (tc)

			second_box.extend (second_ctl)
			second_ctl.set_minimum_width (70)
			second_box.disable_item_expand (second_ctl)

			-------------------------

			time_setting_box.extend (create {EV_CELL})
		end

	--|--------------------------------------------------------------

	build_time_box
			-- Construct the frame set that houses the current
			-- time controls
		local
			tc: EV_CELL
		do
			time_now_frame.extend (time_box)

			-------------------------

			time_box.extend (current_time_box)
			current_time_box.set_minimum_height (25)
			time_box.disable_item_expand (current_time_box)

			create tc
			current_time_box.extend (tc)
			tc.set_minimum_width (10)
			current_time_box.disable_item_expand (tc)

			current_time_box.extend (time_label)
			time_label.align_text_right
			current_time_box.disable_item_expand (time_label)

			create tc
			current_time_box.extend (tc)
			tc.set_minimum_width (5)
			current_time_box.disable_item_expand (tc)

			current_time_box.extend (time_textw)
			time_textw.set_minimum_width (70)
			current_time_box.disable_item_expand (time_textw)
			time_textw.align_text_left

			current_time_box.extend (create {EV_CELL})

			current_time_box.extend (gmt_box)
			gmt_box.extend (create {EV_CELL})

			gmt_box.extend (gmt_btn)
			gmt_btn.set_minimum_height (22)
			gmt_box.disable_item_expand (gmt_btn)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do

			set_border_width (10)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Define the events and responses for the subordinate
			-- components
		do
			Precursor

			calendar.set_change_notify_procedure (agent on_calendar_change)
			time_set_now_btn.select_actions.extend (agent on_current_time_select)
			gmt_btn.select_actions.extend (agent on_gmt_select(gmt_btn))

			hour_ctl.change_actions.extend (agent on_hour_change)
			minute_ctl.change_actions.extend (agent on_minute_change)
			second_ctl.change_actions.extend (agent on_second_change)

			hour_ctl.return_actions.extend (agent
			                                on_time_unit_text_change(hour_ctl))
			minute_ctl.return_actions.extend (agent
			                                  on_time_unit_text_change(minute_ctl))
			second_ctl.return_actions.extend (agent
			                                  on_time_unit_text_change(second_ctl))

			update_timer.actions.extend (agent on_tick)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			update_timer.set_interval (900)
		end

 --|========================================================================
feature {NONE} -- Components
 --|========================================================================

	upper_box: EV_HORIZONTAL_BOX
	calendar: AEL_V2_CALENDAR

	lower_box: EV_HORIZONTAL_BOX

	gmt_box: EV_HORIZONTAL_BOX
	gmt_btn: EV_CHECK_BUTTON

	---------------

	time_setting_frame: EV_FRAME
	time_setting_box: EV_VERTICAL_BOX
	time_set_now_box: EV_HORIZONTAL_BOX
	time_set_now_btn: EV_BUTTON
	time_setting_textw: EV_LABEL

	time_now_frame: EV_FRAME
	time_box: EV_VERTICAL_BOX
	current_time_box: EV_HORIZONTAL_BOX
	time_label: EV_LABEL
	time_textw: EV_LABEL

	hour_box: EV_HORIZONTAL_BOX
	hour_label: EV_LABEL
	hour_ctl: EV_SPIN_BUTTON

	minute_box: EV_HORIZONTAL_BOX
	minute_label: EV_LABEL
	minute_ctl: EV_SPIN_BUTTON

	second_box: EV_HORIZONTAL_BOX
	second_label: EV_LABEL
	second_ctl: EV_SPIN_BUTTON

 --|========================================================================
feature -- Queries
 --|========================================================================

	date: DATE_TIME
	use_gmt: BOOLEAN

	--|--------------------------------------------------------------

	current_time_string: STRING
			-- String representation of the current time
			-- (local or gmt depending on flag state)
		do
			if use_gmt then
				Result := current_gmt_time_string
			else
				Result := current_local_time_string
			end
		end

	--|--------------------------------------------------------------

	current_local_time_string: STRING
			-- String representation of the current local time
		local
			tm: like date
		do
			create tm.make_now
			Result := aprintf (Ks_time_only_fmt,
			                   << tm.hour, tm.minute, tm.second >>)
		end

	--|--------------------------------------------------------------

	current_gmt_time_string: STRING
			-- String representation of the current GMT time
		local
			tm: like date
		do
			create tm.make_now_utc
			Result := aprintf (Ks_time_only_fmt,
			                   << tm.hour, tm.minute, tm.second >>)
		end

	--|--------------------------------------------------------------

	date_time_string: STRING
			-- String representation of the defined time
			-- (local or gmt depending on flag state)
		do
			Result := aprintf (Ks_time_only_fmt,
			                   << date.hour, date.minute, date.second >>)
		end

 --|========================================================================
feature -- Update agents
 --|========================================================================

	set_change_notify_procedure (v: like change_notify_proc)
			-- Define the procedure to execute when a change has 
			-- occurred
		do
			change_notify_proc := v
		end

	change_notify_proc: detachable PROCEDURE [TUPLE [like Current]]
			-- The procedure to execute when a change has occurred

 --|========================================================================
feature -- Value setting
 --|========================================================================

	set_use_gmt (tf: BOOLEAN)
			-- Set the control state to use GMT instead of local time
		do
			if tf then
				gmt_btn.enable_select
			else
				gmt_btn.disable_select
			end
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	set_date_to_present
			-- Reset defined date to be the present date
		do
			if use_gmt then
				create date.make_now_utc
			else
				create date.make_now
			end
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	set_date (v: like date)
			-- Set the defined date to 'v'
		require
			date_exists: v /= Void
		do
			date := v
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	set_date_from_calendar
			-- Set the defined date to the value in the calendar control
		do
			date_changing := True
			update_widget_rendition
			notify_clients_of_change
			date_changing := False
		end

 --|========================================================================
feature {NONE} -- Basic operation
 --|========================================================================

	updating: BOOLEAN
			-- Is Current updating rendition presently?

	date_changing: BOOLEAN
			-- Is date being changed presently?

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Update controls to reflect the current state
		do
			if not updating then
				updating := True
				Precursor
				time_setting_textw.set_text (date_time_string)
				hour_ctl.set_text (date.hour.out)
				minute_ctl.set_text (date.minute.out)
				second_ctl.set_text (date.second.out)
				if not date_changing then
					calendar.select_date (date)
				end
				updating := False
			end
		end

 --|========================================================================
feature {NONE} -- Private attributes
 --|========================================================================

	original_date: like date

 --|========================================================================
feature {NONE} -- Callbacks and agents
 --|========================================================================

	on_calendar_change (c: like calendar)
			-- Respond to calendar change events
		do
			application_root.do_once_on_idle (agent set_date_from_calendar)
		end

	--|--------------------------------------------------------------

	on_current_time_select
			-- Respond to time reset events
		do
			set_date_to_present
			notify_clients_of_change
		end

	--|--------------------------------------------------------------

	on_time_unit_text_change (w: like hour_ctl)
			-- Respond to time unit changes occuring by means of text 
			-- entry in the time unit controls
		local
			ts: STRING
		do
			if w = hour_ctl then
				ts := w.text
				if dtr.is_valid_hour_value (ts) then
					on_hour_change (ts.to_integer)
				else
					set_error_message ("Value for hours must be between 0 and 23.")
				end
			elseif w = minute_ctl then
				ts := w.text
				if dtr.is_valid_minute_value (ts) then
					on_minute_change (ts.to_integer)
				else
					set_error_message ("Value for minutes must be between 0 and 59.")
				end
			elseif w = second_ctl then
				ts := w.text
				if dtr.is_valid_second_value (ts) then
					on_second_change (ts.to_integer)
				else
					set_error_message ("Value for seconds must be between 0 and 59.")
				end
			end
		end

	--|--------------------------------------------------------------

	on_hour_change (v: INTEGER)
			-- Respond to changes in the hour control
		do
			if not updating then
				date.time.set_hour (v)
				update_widget_rendition
				notify_clients_of_change
			end
		end

	--|--------------------------------------------------------------

	on_minute_change (v: INTEGER)
			-- Respond to changes in the minute control
		do
			if not updating then
				date.time.set_minute (v)
				update_widget_rendition
				notify_clients_of_change
			end
		end

	--|--------------------------------------------------------------

	on_second_change (v: INTEGER)
			-- Respond to changes in the second control
		do
			if not updating then
				date.time.set_second (v)
				update_widget_rendition
				notify_clients_of_change
			end
		end

	--|--------------------------------------------------------------

	on_gmt_select (tb: like gmt_btn)
			-- Respond to toggle event from "Show GMT" control
		local
			hd: INTEGER
		do
			if gmt_btn.is_selected then
				use_gmt := True
			else
				use_gmt := False
			end
			-- Update

			-- RFO, rather than create a new date/time,
			-- adjust the existing one
			hd := dtr.hours_behind_gmt

			if use_gmt then
				date.hour_add (hd)
			else
				date.hour_add (-hd)
			end
			update_widget_rendition
			notify_clients_of_change
		end

	--|--------------------------------------------------------------

	notify_clients_of_change
			-- Notify and clients that have registered
		do
			if attached change_notify_proc as p then
				p.call ([ Current ])
			end
		end

	--|--------------------------------------------------------------

	update_timer: EV_TIMEOUT
			-- Timer to driver time display updates

	on_tick
			-- Respond to timer events from update timere
			-- Keep the current time display up to date
		do
			if is_displayed and not is_destroyed then
				time_textw.set_text (current_time_string)
			end
		end

	--|--------------------------------------------------------------

	kill_update_timer
			-- Disable time updates
		do
			update_timer.actions.wipe_out
			update_timer.set_interval (0)
		end

	--|--------------------------------------------------------------

	destroy
			-- Destroy Current (window-wise)
			-- Timers must be disabled before destruction
		do
			kill_update_timer
			Precursor
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	Ks_time_only_fmt: STRING = "%%#02d:%%#02d:%%#02d"
			-- Output format to use when calling printf to show time 
			-- only

	hours_range: INTEGER_INTERVAL
			-- Valid range of hour values
		once
			create Result.make (0, 23)
		end

	minutes_range: INTEGER_INTERVAL
			-- Valid range of minute values
		once
			create Result.make (0, 59)
		end

	seconds_range: INTEGER_INTERVAL
			-- Valid range of second values
		once
			create Result.make (0, 59)
		end
 
	dtr: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 05-Jun-2015
--|     Renamed from AEL_V2_CALENDAR_BOX (misleading)
--|     Recompiled for Eiffel 15.1
--| 004 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 003 31-Aug-2009
--|     Reorganized components to put current time on bottom and time
--|     setting at right in separate frames.  Made current time not
--|     resettable and not influenced by time setting controls.
--|     Added time setting time display with reset to current button
--|     Added gmt conversion so time setting controls are not 
--|     clobbered when switching between GMT and local times
--|     Added 'calendar.select_date' to set and select date
--|     Used do_once_on_idle to update calendar-initiated changes
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--| 001 21-Apr-2008
--|     Original published version
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using either 'make' or 'make_for_gmt'.
--| Then add to your container.
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_TIME_BOX
