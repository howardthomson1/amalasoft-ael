note
	description: "A dialog containing calendar + time widgets"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2015 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2015/06/05 $"
	revision: "$Revision: 003$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CALENDAR_TIME_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions,
			initialize_interface_values,
			window_initial_height, window_initial_width,
			update_committed_value,
			update_widget_rendition,
			committed_value
		end

create
	make_now, make_with_date_time

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_now (ts: STRING)
		do
			is_now := True
			create original_date.make_now
			make_with_title (ts)
		end

	--|--------------------------------------------------------------

	make_with_date_time (ts: STRING; d: DATE_TIME)
		do
			original_date := d
			make_with_title (ts)
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor
			if not is_now then
				create date.make_by_date_time (
					original_date.date.twin, original_date.time.twin)
			end
			create calbox.make
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		do
			working_area.extend (calbox)
			-- Init before setting agents
			if attached date as d then
				calbox.set_date (d)
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			calbox.set_change_notify_procedure (agent on_calendar_change)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
			ok_button.disable_sensitive
		end

--|========================================================================
feature -- Status
--|========================================================================

	date: detachable DATE_TIME
	original_date: DATE_TIME

	calbox: AEL_V2_CALENDAR_TIME_BOX

	is_now: BOOLEAN
			-- Was Current created for 'now' (vs another date)

	committed_value: like date

	--|--------------------------------------------------------------

	has_changed: BOOLEAN
			-- Has the reservation changed during the life of Current?
		do
			Result := not calbox.date.is_equal (original_date)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update_committed_value
		do
			committed_value := date
			Precursor
		end

--|========================================================================
feature {NONE} -- Rendition support
--|========================================================================

	update_widget_rendition
		do
			Precursor
			if has_changed then
				ok_button.enable_sensitive
			else
				ok_button.disable_sensitive
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_calendar_change (w: like calbox)
			-- Respond to a change in 'w'
		do
			if has_changed then
				-- Create/update the date object
				create date.make_by_date_time (calbox.date.date, calbox.date.time)
			end
			update_widget_rendition
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	window_initial_height: INTEGER = 250
	window_initial_width: INTEGER = 450

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 06-Jun-2015
--|     Renamed class from AEL_V2_CALENDAR_DIALOG (misleading)
--|     Filled in ok action logic (create date) rebuilt for Eiffel 15.1
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using either 'make' or 'make_with_title'.
--| Then open it (pref. modal)
--|----------------------------------------------------------------------

end -- class AEL_V2_CALENDAR_TIME_DIALOG
