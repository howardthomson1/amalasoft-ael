Amalasoft Eiffel Library EiffelVision Application Cluster

To support void safety and provide a more flexible hierarchy, with easier
customization that doesn't compromise Void safety, the initialization
sequence within these classes builds on the default initialization squence
from the standard Eiffel librarys, as follows:

AEL_V2_APPLICATION.make_and_launch do
  launch_requested := True
  application_default_create do -- frozen
    is_preparing_application := True
    create_stable_attributes do end
    initialization_state := 1
    default_create do -- from EV_ANY via EV_APPLICATION, Frozen
    1 default_create_interface_objects do -- rename EV_APPLICATION, frozen
        create_main_window do
          create application_main_window
        end
        create_interface_objects do end
        {EV_APPLICATION} Precursor do end
      end
    2 create_implementation do
        create l_environment {EV_ENVIRONMENT}
        implementation := l_environment.implementation.application_i
      end
      implementation.set_state_flag ({EV_ANY_I}.interface_default_create_called_flag, True)
      implementation.assign_interface (Current)
      initialize do -- from EV_ANY
        implementation.set_state_flag ({EV_ANY_I}.interface_is_initialized_flag, True)
      end
    end
    set_default_create_executed do
      initialization_state := 3
    end
    if retrying then
      reset_context_on_retry do end
    else
      complete_initialization do -- from AEL_APPLICATION
        is_preparing_application := True
        if retrying then
          reset_context_on_retry do end
        else
          complete_initialization_pre_arg_parsing do end
          execute_argument_parsing do
            if argument_count = 0 then
              if no_args_denotes_help then
                help_requested := True
              end
            else
              analyze_command_line do
                -- LOTS of code
              end
            end
          end
          complete_app_initialization
        end
      end
      if not (help_requested or has_argument_errors) then
        if attached one_and_done_procedure as lp then
          lp.call ([])
        else
          prepare_application do
            is_preparing_application := False
            application_is_prepared := True
          end
          execute_application do -- frozen
            execute_app_pre_launch
            if launch_requested then
              launch
            end
          end
        end
      end
      if help_requested then
        print_help
        clear_errors
      elseif has_error then
        merge_arg_errors
        report_errors
      end
    end
    logit (0, application_name + " exiting")
  end -- application_default_create
end -- make_and_launch
