note
	description: "{
Generic AEL_V2 "ANY" class compatible with the Eiffel Vision2 library
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_ANY

inherit
	ANY
		undefine
			default_create, copy, is_equal
		end

--|========================================================================
feature -- Support
--|========================================================================

	color_at_xy (w: EV_WIDGET; xp, yp: INTEGER): EV_COLOR
			-- Color at pixel position, relative to widget 'w'
		local
			scrn: EV_SCREEN
		do
			create scrn
			Result := scrn.pixel_color_relative_to (w, xp, yp)
		end

	--|--------------------------------------------------------------

	color_of_pixel (pm: EV_PIXMAP; xp, yp: INTEGER): EV_COLOR
			-- Color of pixel, in pixmap 'pm' at position (xp,yp) 
			-- relative to top-left corner (0,0)
		require
			has_area: pm.height > 0 and pm.width > 0
			xy_in_area: xp >= 0 and xp < pm.width and yp >= 0 and yp < pm.height
		local
			pb: EV_PIXEL_BUFFER
			pi: like {EV_PIXEL_BUFFER}.pixel_iterator
			px: EV_PIXEL_BUFFER_PIXEL
			nx, ny: NATURAL_32
			r, g, b: INTEGER
		do
			create pb.make_with_pixmap (pm)
			pb.lock
			pi := pb.pixel_iterator
			nx := xp.as_natural_32 + 1
			ny := yp.as_natural_32 + 1
			pi.set_column (nx)
			pi.set_row (ny)
			px := pi.item
			r := px.red.to_integer_32
			g := px.green.to_integer_32
			b := px.blue.to_integer_32
			create Result.make_with_8_bit_rgb (r, g, b)
			pb.unlock
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-June-2019
--|     Created original module from existing AEL_V2_WIDGET
--|----------------------------------------------------------------------

end -- class AEL_V2_ANY
