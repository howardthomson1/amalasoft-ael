note
	description: "{
EiffelVision/AEL Custom Application
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_APPLICATION

inherit
	EV_APPLICATION
		rename
			create_interface_objects as default_create_interface_objects,
			is_initialized as ev_is_initialized
		redefine
			default_create_interface_objects,
			initialize
		end
	AEL_APPLICATION
		rename
			create_implementation as ael_create_implementation,
			implementation as ael_implementation,
			is_initialized as ael_is_initialized,
			make as ael_application_make
		undefine
			default_create, copy, is_in_default_state
		redefine
			main_window, execute_application, no_args_denotes_help,
			initialize_constants, complete_app_initialization
		end
	APPLICATION_ENV
		undefine
 			default_create, copy,
 			last_error_message, last_error_code, last_error, has_error, errors,
 			set_error_message, set_error, clear_errors, copy_errors, error_report,
			record_error, set_error_message_formatted, main_window
		end

create
--	make_no_launch,
	make_and_launch

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_and_launch
			-- Create, initialize and launch event loop.
		do
			launch_requested := True
			application_default_create
		end

	make_no_launch
			-- Create and initialize Current but do not launch event
			-- loop.
		do
			application_default_create
		end

	--|--------------------------------------------------------------

	frozen application_default_create
			-- Create application in its default state
		local
			retrying: BOOLEAN
		do
			is_preparing_application := True
			default_create_stable_attributes
			initialization_state := 1
			default_create
			set_default_create_executed
			--RFO, do args BEFORE showing window
--			show_main_window
			if retrying then
				--| Need to go through default_create sequence lest
				--| stable attrs are not created, causing void-safe
				--| compiler errors
				reset_context_on_retry
			else

				complete_initialization

				if not (help_requested or has_argument_errors) then
					if attached one_and_done_procedure as lp then
						-- execute procedure and skip remainder of processing
						lp.call ([])
					else
						prepare_application
						--RFO moved here from earlier
						show_main_window
						execute_application
					end
				end

				if help_requested then
					print_help
					clear_errors
				elseif has_error then
					merge_arg_errors
					report_errors
				elseif has_argument_errors then
					-- Report them
					-- They have not been merged with other errors
					merge_arg_errors
					report_errors
				end
			end
			log_exit
		rescue
			if is_developer_exception_of_name ("User database is corrupted.") then
				if has_error then
 					log_and_print (
 						"FATAL ERROR: %%s", error_report (<<False, false, True>>))
				end
				terminate_abnormally
			elseif is_signal and attached exception_manager.last_exception as lx
			 then
				 no_message_on_failure
				if attached lx.description as lxd then
					log_and_print (
						"Received signal %%d. %%s", <<signal, lxd.to_string_8>>)
				else
					log_and_print ("Received signal %%d.", <<signal>>)
				end
				if retriable_signals.item (signal) then
					retry
				else
					terminate_abnormally
				end
			else
				terminate_abnormally
			end
		end

--RFO 	log_exit
--RFO 			-- Log a message saying the app is exiting
--RFO 		do
--RFO 			logit (0, application_name + " exiting")
--RFO 		end

--|========================================================================
feature {NONE} -- User Initialization
--|========================================================================

	create_interface_objects
			-- User object creation.
			-- Add any additional top-level interface object creates
			-- here.
			-- This should NOT include any components of main_window.
			--
			-- The main_window create itself has been moved from here to
			-- its own create_main_window routine which in turn is called
			-- from create_interface_objects to allow more flexible
			-- redefinition.
		do
		end

	--|--------------------------------------------------------------

	initialize_constants
			-- As the first step in the default_create sequence,
			-- initialize any constant values upon with other components
			-- depend
			-- Example:  string_constants.set_application_name ("MyApp")
		do
			Precursor
		end

	--|--------------------------------------------------------------

	create_main_window
			-- Create the main_window object
		do
			create application_main_window
		end

	--|--------------------------------------------------------------

	user_initialize
			-- User object initialization.
		do
		end

	complete_main_window_initialization
		do
			application_main_window.complete_initialization
		end

	--|--------------------------------------------------------------

--RFO
	complete_app_initialization
			-- Complete the initialization process, AFTER arg parsing
			-- is completed
		do
--			complete_main_window_initialization
		end

	--|--------------------------------------------------------------

	show_main_window
			-- Expose/show the main window
			--
			-- Showing main_window is done from here so that it can, if
			-- necessary. be redefined
		do
			application_main_window.show
		end

--|========================================================================
feature -- Add Comment
--|========================================================================

	no_args_denotes_help: BOOLEAN
			-- Does the absense of cmdline arguments denote a request
			-- for help/usage? NO, this is a gui app
		do
		end

	launch_requested: BOOLEAN
			-- Was launch requested on creation?

--|========================================================================
feature -- Access
--|========================================================================

	application_main_window: MAIN_WINDOW
			-- Application Main Window.	

	main_window: like application_main_window
		do
			Result := application_main_window
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	frozen initialize
			-- <Precursor>
			-- Do not alter routine.
		do
			Precursor {EV_APPLICATION}
			ael_initialize
			user_initialize
--RFO 			complete_main_window_initialization
		end

	--|--------------------------------------------------------------

	frozen default_create_interface_objects
			-- <Precursor>
			-- Do not alter routine.
		do
			create_main_window
			create_interface_objects
			Precursor
		end

	frozen execute_application
			-- Begin execution of application logic
			-- At this point, application is initialized and cmdline
			-- arguments are processed
		do
			execute_app_pre_launch
			post_launch_actions.extend (agent on_launch)
			if launch_requested then
				launch
			end
		end

	execute_app_pre_launch
			-- Perform application execution operations prior to launch
			-- of main loop
		do
		end

	on_launch
			-- Action in response to launch being executed
			-- Triggered from post_launch_actions
		do
		end

--|----------------------------------------------------------------------
--| History
--|
--| 004 28-Feb-2018
--|     Shifted show_main_window to be after arg parsing in
--|     application_default_create
--|     Added merge+report erros on has_argument_errors to fixe
--|     missing errors on unexpected arguments
--| 003 11-Feb-2016
--|     Removed extra call to attach_application_root
--| 002 06-Nov-2013
--|     Added inherit of (not very compatible) AEL_APPLICATION to
--|     merge GUI and non GUI tracks, requiring quite a bit of
--|     adaptation.  EV_APPLICATION.default create is frozen and so
--|     was AEL_ANY.default create, making for an impossible situation.
--|     Un-froze default_create in AEL_ANY and hoisted some of its
--|     bits and pieces into application_default_create, along with
--|     some content from AEL_APPLICATION.make, including rescue and
--|     some more initialization sequence logic.  Rename
--|     AEL_APPLICATION.make to ael_application_make to prevent access.
--|     Removed default_create from creation options, added make_no_launch.
--|     Removed inherit of AEL_SPRT_MULTI_FALLIBLE (now from AEL_APPLICATION)
--|     Added launch_requested flag.
--|     Renamed create_implementation, implementation from
--|     AEL_APPLICATION to prevent execution.
--|     Renamed is_initialized from EV_ and AEL_APPLICATION to avoid
--|     premature evaluation in postconditions
--|     Undefined is_in_default_state from AEL_APPLICATION (defer to
--|     EV_ version)
--|     Redefined main_window to merge shared version and local one.
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- end class AEL_V2_APPLICATION
