note
	description: "{
CMYK Color values
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CMYK

inherit
	APPLICATION_ENV

create
	make_from_color, make_from_values

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_color (v: EV_COLOR)
			-- Create Current from color 'v'
		local
			r, g, b, cmax, cv, mv, yv, kv: REAL
			is_done: BOOLEAN
		do
			default_create
			-- Begin with rgb values as fractions
			r := v.red
			g := v.green
			b := v.blue
			cmax := r.max (g).max (b)
			if cmax = {REAL_32} 0.0 then
				-- Is black, all values are 0 except k, which 1
				kv := 1
				is_done := True
			elseif cmax = {REAL_32} 1.0 then
				if cmax = r and r = g and r = b then
					-- Is white, all values are 0
					is_done := True
				end
			end
			if not is_done then
				kv := 1 - cmax
				cv := (1 - r - kv) / (1 - kv)
				mv := (1 - g - kv) / (1 - kv)
				yv := (1 - b - kv) / (1 - kv)
			end
			set_values (cv, mv, yv, kv)
		end

	--|--------------------------------------------------------------

	make_from_values (cv, mv, yv, kv: REAL_32)
		do
			default_create
			set_values (cv, mv, yv, kv)
		end

--|========================================================================
feature -- Status
--|========================================================================

	c: REAL_32
	m: REAL_32
	y: REAL_32
	k: REAL_32

--|========================================================================
feature -- Status setting
--|========================================================================

	set_values (cv, mv, yv, kv: REAL_32)
		do
			c := cv
			m := mv
			y := yv
			k := kv
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

	to_color: EV_COLOR
			-- Color object with Current's values
		local
			r, g, b: REAL
		do
			r := 255 * (1 - c) * (1 - k)
			g := 255 * (1 - m) * (1 - k)
			b := 255 * (1 - y) * (1 - k)
			create Result.make_with_rgb (r, g, b)
		end

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 03-Feb-2019
--|     Created original module (Eiffel 18.07)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_CMYK
