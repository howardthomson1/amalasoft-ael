note
	description: "{
An enhanced EV_COLOR
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_COLOR

inherit
	EV_COLOR
		rename
			hue_as_degrees as hue_as_real_degrees
		end

create
	default_create,
	make_with_8_bit_rgb,
	make_with_rgb, make_with_rgb_hex,
	make_with_hsv, make_with_hsb, make_with_hsb_fraction,
	make_with_cmyk, make_with_grbw,
	make_with_hsl, make_with_hsl_wd, make_with_hsl_d, make_with_hsl_dp,
	make_with_color

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_color (v: EV_COLOR)
			-- Create Current from color 'v'
		do
			make_with_rgb (v.red, v.green, v.blue)
		end

	--|--------------------------------------------------------------

	make_with_rgb_hex (v: STRING)
			-- Create Current with hexadecimal representation 'v' of 
			-- 24-bit RGB value
		require
			valid: av2_colors.is_valid_rgb_hex (v)
		local
			r, g, b: INTEGER
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with ("0x") then
				low_v := low_v.substring (3, low_v.count)
			end
			r := av2_colors.hex_to_8_bit_red (low_v)
			g := av2_colors.hex_to_8_bit_green (low_v)
			b := av2_colors.hex_to_8_bit_blue (low_v)
			make_with_8_bit_rgb (r, g, b)
		end

	--|--------------------------------------------------------------

	make_with_cmyk (c, m, y, k: REAL)
			-- Create Current from CMYK values
			-- Where values are presented as fractions of 1 (0.0 .. 1.0)
			--
			-- The R,G,B values are given in the range of 0..255.
			-- Red is calculated from cyan and black:
			--    R = 255 � (1-C) � (1-K)
			-- Green is calculated from magenta and black:
			--    G = 255 � (1-M) � (1-K)
			-- Blue is calculated from yellow and black:
			--    B = 255 � (1-Y) � (1-K)
		require
			c_valid: c >= Kr_0 and c <= Kr_1
			m_valid: m >= Kr_0 and m <= Kr_1
			y_valid: y >= Kr_0 and y <= Kr_1
			k_valid: k >= Kr_0 and k <= Kr_1
		local
			r, g, b: INTEGER
		do
			default_create
 			r := (255 * (1 - c) * (1 - k)).truncated_to_integer
 			g := (255 * (1 - m) * (1 - k)).truncated_to_integer
 			b := (255 * (1 - y) * (1 - k)).truncated_to_integer
			set_red_with_8_bit (r)
			set_green_with_8_bit (g)
			set_blue_with_8_bit (b)
		end

	--|--------------------------------------------------------------

	make_with_hsv (h, s, v: REAL)
			-- Create Current from HSB/HSV values where:
			-- h is Hue, expressed as fraction of a circle (0.0 .. 1.0)
			-- s is Saturation expressed as fraction (0.0 .. 1.0)
			-- v is Brightness or Value, expressed as fraction (0.0 .. 1.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
			rgb_vals := hsv_to_rgb (h, s, v)
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_hsb (h, s, v: REAL)
			-- Create Current from HSB/HSV values where:
			-- h is Hue, expressed as Degrees (0.0 .. 360.0)
			-- s is Saturation expressed as PERCENTAGE (0.0 .. 100.0)
			-- v is Brightness, expressed as PERCENTAGE (0.0 .. 100.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
			rgb_vals := hsv_to_rgb (
				h / Kr_360, s / Kr_100, v / Kr_100)
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	make_with_hsb_fraction (h, s, v: REAL)
			-- Create Current from HSB/HSV values where:
			-- h is Hue, expressed as Degrees (0.0 .. 360.0)
			-- s is Saturation expressed as FRACTION (0.0 .. 1.0)
			-- v is Brightness, expressed as FRACTION (0.0 .. 1.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
			rgb_vals := hsv_to_rgb (h / Kr_360, s, v)
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_hsl (h, s, l: REAL)
			-- Create Current from HSL values where:
			-- h is Hue, expressed as fraction of a circle (0.0 .. 1.0)
			-- s is Saturation expressed as fraction (0.0 .. 1.0)
			-- l is Lightness or Value, expressed as fraction (0.0 .. 1.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
--			rgb_vals := hsl_to_rgb (h, s, l)
			rgb_vals := hsl_to_rgb (
				(h * Kr_360).rounded, s / Kr_100, l / Kr_100)
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_hsl_wd (h: INTEGER; s, l: REAL)
			-- Create Current from HSL values where:
			-- h is Hue, expressed as whole Degrees (0 .. 360)
			-- s is Saturation expressed as fraction of 1 (0.0 .. 1.0)
			-- l is Lightness, expressed as fraction of (0.0 .. 1.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
--			rgb_vals := hsl_to_rgb (h / Kr_360, s, l)
			rgb_vals := hsl_to_rgb (h, s.min (1.0), l.min (1.0))
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_hsl_d (h, s, l: REAL)
			-- Create Current from HSL values where:
			-- h is Hue, expressed as Degrees (0.0 .. 360.0)
			-- s is Saturation expressed as fraction of 1 (0.0 .. 1.0)
			-- l is Lightness, expressed as fraction of (0.0 .. 1.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
--			rgb_vals := hsl_to_rgb (h / Kr_360, s, l)
			rgb_vals := hsl_to_rgb (h.rounded, s.min (1.0), l.min (1.0))
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_hsl_dp (h, s, l: REAL)
			-- Create Current from HSL values where:
			-- h is Hue, expressed as Degrees (0.0 .. 360.0)
			-- s is Saturation expressed as Percentage (0.0 .. 100.0)
			-- l is Lightness, expressed as Percentage (0.0 .. 100.0)
		local
			rgb_vals: ARRAY [REAL]
		do
			default_create
--			rgb_vals := hsl_to_rgb (
--				h / Kr_360, s / Kr_100, l / Kr_100)
			rgb_vals := hsl_to_rgb (
				h.rounded, s / Kr_100, l / Kr_100)
			set_red (rgb_vals.item (1))
			set_green (rgb_vals.item (2))
			set_blue (rgb_vals.item (3))
		end

	--|--------------------------------------------------------------

	make_with_grbw (g, r, b, w: INTEGER)
			-- Create Current from GRBW values where:
			-- g is Green
			-- r is Red
			-- b is Blue
			-- w is White
		local
			rx, gx, bx: INTEGER
			wx, rf, gf, bf, wf, alpha, beta: REAL
		do
			if w = 0 then
				rx := r
				gx := g
				bx := b
			elseif (r + g + b) = 0 then
				-- Gray, using w only
				rx := w
				gx := w
				bx := w
			elseif r = g and r = b then
				-- Gray, lightened by w
				rf := r / Kr_255
				gf := g / Kr_255
				bf := b / Kr_255
				wf := (w / Kr_255) + 1
				rx := (rf * wf * Kr_255).rounded.min (255)
				gx := (bf * wf * Kr_255).rounded.min (255)
				bx := (gf * wf * Kr_255).rounded.min (255)
			else
				-- w desaturates the the max color(s)
				rf := r / Kr_255
				gf := g / Kr_255
				bf := b / Kr_255

				rx := (rf * wf * Kr_255).rounded.min (255)
				gx := (bf * wf * Kr_255).rounded.min (255)
				bx := (gf * wf * Kr_255).rounded.min (255)

				wx := Kr_1 - ((Kr_1 - wf) * (Kr_1 - wf))
				alpha := {REAL}0.6 * wx
				beta := Kr_1 - alpha
				rx := (((alpha * rf) + beta) * Kr_255).rounded.min (255)
				gx := (((alpha * gf) + beta) * Kr_255).rounded.min (255)
				bx := (((alpha * bf) + beta) * Kr_255).rounded.min (255)
			end
			make_with_8_bit_rgb (rx, gx, bx)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_gray: BOOLEAN
			-- Is Current a gray?
			-- Grays have the same red, green and blue values
		do
			if red_8_bit = green_8_bit then
				Result := red_8_bit = blue_8_bit
			end
		end

	is_whitish (rv: REAL): BOOLEAN
			-- Is Current white, or very close to it?
			-- If 'rv' > 0.0, use it as range value, else use default
		local
			lt, rng: REAL
		do
			lt := lightness
			if rv > 0.0 then
				rng := rv
			else
				rng := 0.92
			end
			Result := lt > rng
		end

	--|--------------------------------------------------------------

	is_blackish (rv: REAL): BOOLEAN
			-- Is Current black, or very close to it?
			-- If 'rv' > 0.0, use it as range value, else use default
		local
			lt, rng: REAL
		do
			lt := lightness
			if rv > 0.0 then
				rng := rv
			else
				rng := 0.12
			end
			Result := lt < rng
		end

--|========================================================================
feature -- Access
--|========================================================================

	hue_as_degrees: REAL
			-- Hue of Current as number from 0 to 360 (degrees)
			--
			-- Hues run from 0-360 (each being pure red)
			-- As degrees on the color wheel
			-- Yellow=60, Green=120, Cyan=180, Blue=240, Magenta=300
			--  0                                   360
			--  |     |     |     |     |     |     |
			-- Red  Yellow Green Cyan Blue Magenta Red
		do
			Result := hue_as_sectors * {REAL_32} 60.0
		end

	hue_as_sectors: REAL
			-- Hue of Current in 6ths of a circle (each a vertex
			-- of a hexagon, a common abstraction)
			-- Multiply by 60 to get degrees.
		local
			min_color, max_color, diff_color: REAL
		do
			-- RGB values represent the intensity of each component, as
			-- a fraction of full (full=1.0)
			-- The next step is to find the largest and smallest of the
			-- 3 values
			max_color := red.max(green).max(blue)
			min_color := red.min(green).min(blue)
			-- The value range is the difference between the largest and
			-- smallest values
			diff_color := max_color - min_color
			-- If the difference is 0, then the color is pure gray,
			-- though it could range from white to black, depending on
			-- lightness or brightness.  Pure grays have a hue of 0.
			if diff_color /= 0.0 then
				if max_color = red then
					Result := (green - blue) / diff_color
				elseif max_color = green then
					Result := {REAL_32} 2.0 + ((blue - red) / diff_color)
				elseif max_color = blue then
					Result := {REAL_32} 4.0 + ((red - green) / diff_color)
				else
					check
						must_have_max: False
					end
				end
				-- Value flips to negative at magenta (300 degrees),
				-- being the last of 5 vertices after red.  When this
				-- happens, we add a full complement of sectors to the
				-- result to avoid negative results (and get correct ones)
				if Result < 0 then
					Result := Result + {REAL_32} 6.0
				end
			end
		ensure
			in_range: Result >= 0 and Result <= 6
		end

	--|--------------------------------------------------------------

	hsl_saturation: REAL
			-- Saturation of Current in the HSL model
			-- Expressed as a fraction of 1.  To convert to percent,
			-- multiply by 100.0
		local
			diff_color: REAL
			sum_color: REAL
			max_color: REAL
			min_color: REAL
		do
			max_color := red.max(green).max(blue)
			min_color := red.min(green).min(blue)
			diff_color := max_color - min_color
			if diff_color /= 0.0 then
				sum_color := max_color + min_color
				Result := diff_color / (1 - (sum_color - 1))
			end
		end

	--|--------------------------------------------------------------

	hsb_saturation: REAL
			-- Saturation of Current in the HSB (HSV) model
			-- Expressed as a fraction of 1.  To convert to percent,
			-- multiply by 100.0
			-- N.B Not the same as saturation in the HSL model
		local
			diff_color: REAL
			max_color: REAL
			min_color: REAL
		do
			max_color := red.max(green).max(blue)
			min_color := red.min(green).min(blue)
			if max_color /= 0 then
				diff_color := max_color - min_color
				Result := diff_color / max_color
			end
		end

	--|--------------------------------------------------------------

	hsb_brightness: REAL
			-- Brightness of Current in the HSB (HSV) model
			-- Expressed as a fraction of 1.  To convert to percent,
			-- multiply by 100.0
			-- N.B. Not the same as 'lightness'
		do
			Result := red.max(green).max(blue)
		end

	--|--------------------------------------------------------------

	cyan_value: REAL
			-- Value of 'C' (cyan) component in CMYK equivalent of
			-- Current
			--
			-- N.B. This depends on a calculation that is performed for
			-- each call of the function.  A more efficient method, when
			-- suitable, would be to use the 'to_cmyk' function, then
			-- access its items, thus invoking the calculation only once
			-- per value set.
		do
			Result := to_cmyk.item (1)
		end

	yellow_value: REAL
			-- Value of 'Y' (yellow) component in CMYK equivalent of Current
			--
			-- N.B. This depends on a calculation that is performed for
			-- each call of the function.  A more efficient method, when
			-- suitable, would be to use the 'to_cmyk' function, then
			-- access its items, thus invoking the calculation only once
			-- per value set.
		do
			Result := to_cmyk.item (2)
		end

	magenta_value: REAL
			-- Value of 'M' (magenta) component in CMYK equivalent of Current
			--
			-- N.B. This depends on a calculation that is performed for
			-- each call of the function.  A more efficient method, when
			-- suitable, would be to use the 'to_cmyk' function, then
			-- access its items, thus invoking the calculation only once
			-- per value set.
		do
			Result := to_cmyk.item (3)
		end

	k_value: REAL
			-- Value of 'K' component in CMYK equivalent of Current
			--
			-- N.B. This depends on a calculation that is performed for
			-- each call of the function.  A more efficient method, when
			-- suitable, would be to use the 'to_cmyk' function, then
			-- access its items, thus invoking the calculation only once
			-- per value set.
		do
			Result := to_cmyk.item (4)
		end

--|========================================================================
feature -- Conversion, Current to other
--|========================================================================

	to_ev_color: EV_COLOR
			-- Current as instance of EV_COLOR
		do
			create Result.make_with_rgb (red, green, blue)
		end

	--|--------------------------------------------------------------

	to_cmyk: ARRAY [REAL]
			-- CMYK values equivalent to Current
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Cyan, Magenta, Yellow and 'K' (the
			-- extent of shading, or black)
		local
			cmax, cv, mv, yv, kv: REAL
			is_done: BOOLEAN
		do
			cmax := red.max (green).max (blue)
			if cmax = Kr_0 then
				-- Is black, all values are 0 except k, which 1
				kv := 1
				is_done := True
			elseif cmax = Kr_1 then
				if cmax = red and red = green and red = blue then
					-- Is white, all values are 0
					is_done := True
				end
			end
			if not is_done then
				kv := 1 - cmax
				cv := (1 - red - kv) / (1 - kv)
				mv := (1 - green - kv) / (1 - kv)
				yv := (1 - blue - kv) / (1 - kv)
			end
			Result := {ARRAY [REAL]} << cv, mv, yv, kv >>
		ensure
			valid_size: Result.count = 4
		end

	--|--------------------------------------------------------------

	to_hsl: ARRAY [REAL]
			-- HSL values equivalent to Current
			-- Result is array of values, with first item being the Hue,
			-- in degrees (0 .. 360), the second item being the saturation
			-- as fraction of 1 (0.0 .. 1.0), and the last item being the
			-- lightness as fraction of 1 (0.0 .. 1.0)
		do
			Result :=
				{ARRAY [REAL]} << hue_as_degrees, hsl_saturation, lightness >>
		ensure
			valid_size: Result.count = 3
		end

	--|--------------------------------------------------------------

	to_hsv: ARRAY [REAL]
			-- HSB/HSV values equivalent to Current
			-- Result is array of values, with first item being the Hue,
			-- in degrees (0 .. 360), the second item being the saturation
			-- as fraction of 1 (0.0 .. 1.0), and the last item being the
			-- briightness as fraction of 1 (0.0 .. 1.0)
		do
			Result :=
				{ARRAY [REAL]} << hue_as_degrees, hsb_saturation, hsb_brightness >>
		ensure
			valid_size: Result.count = 3
		end

	--|--------------------------------------------------------------

	to_rgbw: ARRAY [INTEGER]
			-- RGBW representation of Current
			-- Same values as RGBW, in different order
			-- Result is array of values, with first item being the
			-- Red value, the second being the Green value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
		do
			Result := rgb_8_bit_to_rgbw (red_8_bit, green_8_bit, blue_8_bit)
		ensure
			valid_size: Result.count = 4
		end

	to_grbw: ARRAY [INTEGER]
			-- GRBW representation of Current
			-- Same values as RGBW, in different order
			-- Result is array of values, with first item being the
			-- Green value, the second being the Red value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
		do
			Result := rgb_8_bit_to_grbw (red_8_bit, green_8_bit, blue_8_bit)
		ensure
			valid_size: Result.count = 4
		end

	to_rgb_hex (df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of RGB values of 
			-- Current
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		do
			Result := av2_colors.rgb_to_hex (
				red_8_bit, green_8_bit, blue_8_bit, df, ucf)
		end

	--|--------------------------------------------------------------

	to_rgbw_hex (df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of RGBW values of 
			-- Current
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			rgbw: ARRAY [INTEGER]
		do
			rgbw := rgb_8_bit_to_rgbw (red_8_bit, green_8_bit, blue_8_bit)
			Result := av2_colors.bytes_to_hex (rgbw, df, ucf)
		ensure
			valid_decorated_size: df implies Result.count = 10
			valid_plain_size: (not df) implies Result.count = 8
		end

	--|--------------------------------------------------------------

	to_grbw_hex (df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of GRBW values of 
			-- Current
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			rgbw: ARRAY [INTEGER]
			apf: AEL_PRINTF
			fmt: STRING
			r, g, b, w: INTEGER
		do
			rgbw := rgb_8_bit_to_rgbw (red_8_bit, green_8_bit, blue_8_bit)
			r := rgbw.item (1)
			g := rgbw.item (2)
			b := rgbw.item (3)
			w := rgbw.item (4)
			create apf
			fmt := av2_colors.hex_pf_format (4, df, ucf)
			Result := apf.aprintf (fmt, << g, r, b, w >>)
		ensure
			valid_decorated_size: df implies Result.count = 10
			valid_plain_size: (not df) implies Result.count = 8
		end

	--|--------------------------------------------------------------

	to_grbw_32: NATURAL_32
			-- GRBW representation of Current, as a 32bit unsigned value
			-- ggrrbbww
		local
			rgbw: ARRAY [INTEGER]
			ng, nr, nb, nw: NATURAL_32
		do
			rgbw := rgb_to_rgbw (red, green, blue)
			ng := rgbw.item (2).as_natural_32 |<< 24
			nr := rgbw.item (1).as_natural_32 |<< 16
			nb := rgbw.item (3).as_natural_32 |<< 8
			nw := rgbw.item (4).as_natural_32
			Result := ng + nr + nb + nw
		end

	--|--------------------------------------------------------------

	rgb_to_rgbw (rf, gf, bf: REAL): ARRAY [INTEGER]
			-- RGBW representation of RGB values rf, gf and bf
			-- Input values are fractions of 1.0
			-- Result is array of r values, with first item being the
			-- Red value, the second being the Green value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
			-- When r-g-b, the color is gray, expressed as a value 
			-- (intensity) of white, else values are the same as in RGB, 
			-- with white being 0
		local
			r, g, b: INTEGER
		do
			r := normalized_8_bit ((rf * Kr_255).rounded)
			g := normalized_8_bit ((gf * Kr_255).rounded)
			b := normalized_8_bit ((bf * Kr_255).rounded)
			Result := rgb_8_bit_to_rgbw (r, g, b)
		ensure
			valid_count: Result.count = 4
		end

	--|--------------------------------------------------------------

	rgb_8_bit_to_rgbw (ri, gi, bi: INTEGER): ARRAY [INTEGER]
			-- RGBW representation of RGB values rf, gf and bf
			-- Result is array of r values, with first item being the
			-- Red value, the second being the Green value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
			-- When r-g-b, the color is gray, expressed as a value 
			-- (intensity) of white, else values are the same as in RGB, 
			-- with white being 0
		local
			tc: AEL_V2_COLOR
			w: INTEGER
		do
			if ri = gi and ri = bi then
				-- Express as white
				create tc.make_with_8_bit_rgb (ri, gi, bi)
				w := normalized_8_bit (whiteness (tc).rounded)
				Result := {ARRAY [INTEGER]} << 0, 0, 0, w >>
			else
				Result := {ARRAY [INTEGER]} << ri, gi, bi, 0 >>
			end
		ensure
			valid_count: Result.count = 4
		end

	--|--------------------------------------------------------------

	rgb_8_bit_to_grbw (ri, gi, bi: INTEGER): ARRAY [INTEGER]
			-- GRBW representation of RGB values rf, gf and bf
			-- Result is array of r values, with first item being the
			-- Red value, the second being the Green value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
			-- When r-g-b, the color is gray, expressed as a value 
			-- (intensity) of white, else values are the same as in RGB, 
			-- with white being 0
		do
			Result := rgbw_to_grbw (rgb_8_bit_to_rgbw (ri, gi, bi))
		ensure
			valid_count: Result.count = 4
		end

	--|--------------------------------------------------------------

	rgbw_to_grbw (v: ARRAY [INTEGER]): ARRAY [INTEGER]
			-- RGBW values re-orderd to GRBW
		require
			valid_count: v.count = 4
		do
			Result := {ARRAY [INTEGER]}
				<< v.item (2), v.item (1), v.item (3), v.item (4) >>
		ensure
			valid_count: Result.count = 4
		end

	--|--------------------------------------------------------------

	color_to_rgbw (c: ev_color): ARRAY [INTEGER]
			-- RGBW representation of color 'c'
			-- Result is array of values, with first item being the
			-- Red value, the second being the Green value, the third
			-- being the Blue value, and the fourth being the white.
			-- Useful for programming LEDs
		local
			r, g, b, w: INTEGER
			wf: REAL
			rf, gf, bf: REAL
		do
			rf := c.red
			gf := c.green
			bf := c.blue
			wf := whiteness (c)
			w := (wf * Kr_255).rounded
			r := (rf * Kr_255).rounded
			g := (gf * Kr_255).rounded
			b := (bf * Kr_255).rounded
			-- normalize to between 0 and 255
			w := normalized_8_bit (w)
			r := normalized_8_bit (r)
			g := normalized_8_bit (g)
			b := normalized_8_bit (b)
			Result := {ARRAY [INTEGER]} << r, g, b, w >>
		ensure
			valid_count: Result.count = 4
		end

	--|--------------------------------------------------------------

	normalized_8_bit (v: INTEGER): INTEGER
			-- Value 'v' normalized to be between 0 and 255
		do
			if v < 0 then
				Result := 0
			elseif v > 255 then
				Result := 255
			else
				Result := v
			end
		end

	--|--------------------------------------------------------------

	rgb_saturation (c: EV_COLOR): INTEGER
			-- Saturation is the color intensity of color 'c' relative
			-- to its own brightness
			-- Expressed as whole percent
		local
			cmin, cmax: REAL
			r, g, b: REAL
		do
			r := c.red
			g := c.green
			b := c.blue
			cmin := r.min (g.min (b))
			cmax := r.max (g.max (b))
			-- The difference between the max and min,
			-- divided by the highest is the saturation
			Result := (100 * ((cmax - cmin) / cmax)).rounded
		end

	--|--------------------------------------------------------------

	whiteness (c: EV_COLOR): REAL
			-- Whiteness of color 'c'
		local
			avg, r, g, b: REAL
			sat: INTEGER
		do
			sat := rgb_saturation (c)
			r := c.red
			g := c.green
			b := c.blue
			avg := (r + g + b) / 3
			-- (255 - sat) / 255 * (c.red + c.green + c.blue) / 3
			-- TODO, investigate over-whiteness?
			Result := (Kr_255 - (sat / Kr_255)) * avg
		end

--|========================================================================
feature -- Conversion, between models
--|========================================================================

	rgb_to_cmyk (r, g, b: REAL): ARRAY [REAL]
			-- CMYK values equivalent if RGB values
			-- RGB values are fractions of 1 (0.0 .. 1.0)
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Cyan, Magenta, Yellow and 'K' (the
			-- extent of shading, or black)
		require
			red_in_range: r >= Kr_0 and r <= Kr_1
			green_in_range: g >= Kr_0 and g <= Kr_1
			blue_in_range: b >= Kr_0 and b <= Kr_1
		local
			cmax, cv, mv, yv, kv: REAL
			is_done: BOOLEAN
		do
			cmax := red.max (green).max (blue)
			if cmax = Kr_0 then
				-- Is black, all values are 0 except k, which 1
				kv := 1
				is_done := True
			elseif cmax = Kr_1 then
				if cmax = red and red = green and red = blue then
					-- Is white, all values are 0
					is_done := True
				end
			end
			if not is_done then
				kv := 1 - cmax
				cv := (1 - red - kv) / (1 - kv)
				mv := (1 - green - kv) / (1 - kv)
				yv := (1 - blue - kv) / (1 - kv)
			end
			Result := {ARRAY [REAL]} << cv, mv, yv, kv >>
		ensure
			valid_size: Result.count = 4
		end

	--|--------------------------------------------------------------

	rgb_8_bit_to_cmyk (r, g, b: INTEGER): ARRAY [REAL]
			-- CMYK values equivalent if RGB values
			-- RGB values are whole numbers between 0 and 255
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Cyan, Magenta, Yellow and 'K' (the
			-- extent of shading, or black)
		require
			red_in_range: r >= 0 and r <= 255
			green_in_range: g >= 0 and g <= 255
			blue_in_range: b >= 0 and b <= 255
		do
			Result := rgb_to_cmyk (r / Kr_255, g / Kr_255, b / Kr_255)
		ensure
			valid_size: Result.count = 4
		end

	--|--------------------------------------------------------------

	hsv_to_rgb (ha, sa, va: REAL): ARRAY [REAL]
			-- RGB equivalents for Hue, Saturation and Value (brightness)
			-- Where 'ha', 'sa', and 'va' are fractions of 1
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Red, Green, and Blue
		local
			r, g, b, f, p, q, t: REAL
			sec: INTEGER
		do
			-- When sa < 12, cannot change Hue
			-- When sa >= 12, hue change seems to happen in 2 degree step
			-- "*6" converts fraction of circle sector # (0 .. 5),
			-- each sector having 60 degrees (a hexagon)
			-- 0.0 -> 0 (reds)
			-- 0.167 = 60 deg; 0.167 * 6 = 1 -> 1
			-- 0.5 = 180 deg;  0.5 * 6 = 3 -> 3
			-- 0.333 = 120 deg; 0.333 * 6 = 2 -> 2
			-- 0.1 = 36 deg; 0.1 * 6 = 0.6 -> 0
			-- 0.833 = 300 deg; 0.833 * 6 = 5
			-- 0.9 = 324 deg; 0.9 * 6 = 5.4 -> 5
			sec := (ha * 6).truncated_to_integer
			f := (ha * 6) - sec
			p := va * (1 - sa)
			q := va * (1 - f * sa)
			t := va * (1 - (1 - f) * sa)
			-- translation depends on sector
			--RFO inspect sec \\ 6
			--RFO when 0 then
			--RFO 	r := va; g := t; b := p
			--RFO when 1 then
			--RFO 	r := q; g := va; b := p
			--RFO when 2 then
			--RFO 	r := p; g := va; b := t
			--RFO when 3 then
			--RFO 	r := p; g := q; b := va
			--RFO when 4 then
			--RFO 	r := t; g := p; b := va
			--RFO else
			--RFO 	r := va; g := p; b := q
			--RFO end
			inspect sec
			when 0 then
				r := va; g := t; b := p
			when 1 then
				r := q; g := va; b := p
			when 2 then
				r := p; g := va; b := t
			when 3 then
				r := p; g := q; b := va
			when 4 then
				r := t; g := p; b := va
			else
				r := va; g := p; b := q
			end
			Result := {ARRAY [REAL]}<< r, g, b >>
		end

	--|--------------------------------------------------------------

	hsb_to_rgb (h, s, v: REAL): ARRAY [REAL]
			-- RGB equivalents for Hue, Saturation and Value (brightness)
			-- Where 'h' is Hue in Degrees, 's' and 'v' are Saturation
			-- and Brightness (value, respectively, and expressed as
			-- fractions of 1
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Red, Green, and Blue
		do
			Result := hsv_to_rgb (h / 360, s, v)
		end

	--|--------------------------------------------------------------

	hsl_to_rgb_bad (ha, sa, la: REAL): ARRAY [REAL]
			-- RGB equivalents for Hue, Saturation and Lightness
			-- Where 'ha' is Hue, 'sa' is Saturation, and 'la' is Lightness,
			-- each expressed as a fraction of 1 (0.0 .. 1.0)
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Red, Green, and Blue
		local
			r, g, b, p, q, f13: REAL
		do
			f13 := Kr_1 / {REAL} 3.0
			if sa = 0 then
				-- fully destaturated; gray determined solely by
				-- lightness in this case
				-- BUT, there needs to be a way to work from here without
				-- forcing all values to 0
				-- TODO
				r := la
				g := la
				b := la
			else
				-- Values don't seem to be changeable under some
				-- conditions, like very high lightness, or very low
				-- saturation
--RFO         var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
				if la < 0.5 then
					q := la * (1 + sa)
				else
					q := la + sa - (la * sa)
				end
				p := (2 * la) - q
				r := special_hue_to_rgb (p, q, ha + f13)
				g := special_hue_to_rgb (p, q, ha)
				b := special_hue_to_rgb (p, q, ha - f13)
			end
			Result := {ARRAY [REAL]} << r, g, b >>
		end

	special_hue_to_rgb (pa, qa, ta: REAL): REAL
		local
			tv, f23, f16: REAL
		do
			f23 := {REAL} 2.0 / {REAL} 3.0
			f16 := Kr_1 / {REAL} 6.0
			tv := ta
			if ta < 0 then
				tv := ta + 1
			elseif ta > 1 then
				tv := ta - 1
			end
			if tv < f16 then
				Result := pa + ((qa - pa) * 6 * tv)
			elseif tv < 0.5 then
				Result := qa
			elseif tv < f23 then
				Result := pa + ((qa - pa) * (f23 - tv) * 6)
			else
				Result := pa
			end
		ensure
			in_range: Result >= Kr_0 and Result <= Kr_1
		end


	--|--------------------------------------------------------------

	hsl_to_rgb_bad2 (h: INTEGER; sf, lf: REAL): ARRAY [REAL]
			-- RGB equivalents for Hue, Saturation and Lightness
			-- Where 'h' is Hue (in 0..360),
			-- 'sf' is Saturation, and 'lf' is Lightness,
			-- each expressed as a fraction of 1 (0.0 .. 1.0)
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Red, Green, and Blue
		local
			r, g, b, c, x, m: REAL
			--p, q, hf: REAL
			hh: INTEGER
		do
			hh := h // 60
			c := (1 - ((2*lf)-1)).abs * sf
			x := c * (1 - ((hh \\ 2 ) - 1).abs)
			if hh >= 0 and hh < 1 then
				r := c
				g := x
			elseif hh >= 1 and hh < 2 then
				r := x
				g := c
			elseif hh >= 2 and hh < 3 then
				g := c
				b := x
			elseif hh >= 3 and hh < 4 then
				g := x
				b := c
			elseif hh >= 4 and hh < 5 then
				r := x
				b := c
			else
				r := c
				b := x
			end
			m := 1 - (c / 2)
			r := r + m
			g := g + m
			b := b + m
			Result := {ARRAY [REAL]} << r, g, b >>
		end

	--|--------------------------------------------------------------

	hsl_to_rgb (h: INTEGER; sf, lf: REAL): ARRAY [REAL]
			-- RGB equivalents for Hue, Saturation and Lightness
			-- Where 'h' is Hue (in 0..360),
			-- 'sf' is Saturation, and 'lf' is Lightness,
			-- each expressed as a fraction of 1 (0.0 .. 1.0)
			--
			-- Result is array of values, each expressed as a fraction
			-- of 1 (0.0 .. 1.0).
			-- Items are, in order, Red, Green, and Blue
		require
			hue_in_range: h >= 0 and h <= 360
			saturation_in_range: sf >= 0.0 and sf <= 1.0
			lightness_in_range: lf >= 0.0 and lf <= 1.0
		local
			r, g, b, c, x, m: REAL
			hh, one: INTEGER
			hfsegs, hfmod, tf1, tf2, tf3: REAL
		do
			-- C = (1-Math.abs(2*L-1))*s;
			-- hh = h/60;
			-- X = C*(1-Math.abs(hh%2-1));
			-- m = L-C/2;

			one := 1
			c := (Kr_1 - ((Kr_2*lf) - Kr_1).abs) * sf
			hh := h // 60
			-- x was too small in the original formula
			-- x := c * (one - ((hh \\ 2) - one).abs)
			-- e.g. 30,1.0,0.75 sb FFBF80 (255, 191, 128)
			-- but eventual values are FF8080 (255, 128, 128)
			-- x should be (0.75 + m) * 255 = 191
			-- Instead, is 0.5 --> 128
			-- The original formula is javascript, and uses floating 
			-- point values for the modulo operation (it really does)
			-- The formula for 'c' appears to be correct, so the error
			-- had to be in the modulo expression.  It was the pseudo
			-- mod functin that actually depended on it returning a
			-- non-whole value (despite the 'module' name) GRRRRRRRR
			-- The solution require a fake mod function to mimic the
			-- behavior of the javscript original
			hfsegs := h.to_real / {REAL}60.0
			hfmod := fp_mod (h.to_real / {REAL}60.0, Kr_2)
			tf1 := hfmod - 1
			tf2 := tf1.abs
			tf3 := 1 - tf2
			x := c * tf3
			m := lf - (c / 2)

			if h >= 0 and h < 60 then
				r := c
				g := x
				b := 0
			elseif h >= 60 and h < 120 then
				r := x
				g := c
				b := 0
			elseif h >= 120 and h < 180 then
				r := 0
				g := c
				b := x
			elseif h >= 180 and h < 240 then
				r := 0
				g := x
				b := c
			elseif h >= 240 and h < 300 then
				r := x
				g := 0
				b := c
			else
				r := c
				g := 0
				b := x
			end
			r := r + m
			g := g + m
			b := b + m
			Result := {ARRAY [REAL]} << r, g, b >>
		end

	fp_mod (v1, v2: REAL): REAL
			-- Floating point modulo of v1 \\ v2
			-- A more robust implementation can be found in
			-- AEL_NUMERIC_ROUTINEs, but is unnecessary for these values
		require
			values_small_enough: v1 <= 360 and v2 <= 360
		local
			tf1, tf2: REAL
			ti1, ti2: INTEGER
		do
			tf1 := v1 * Kr_100
			tf2 := v2 * Kr_100
			ti1 := tf1.rounded
			ti2 := tf2.rounded
			Result := (ti1 \\ ti2) / Kr_100
		end

	--|--------------------------------------------------------------

	bad_hsb_to_rgb (hd, sp, vp: REAL): ARRAY [REAL]
			-- hd is Hue in degrees
			-- sp is Saturation, as Percentage
			-- bp is Brightness (Value), as Percentage
		require
			hue_valid: hd >= 0 and hd <= 360
			saturation_valid: sp >= 0 and sp <= 100
			value_valid: vp >= 0 and vp <= 100
		local
			hh: INTEGER
			c, x, m, h, s, v, r, g, b, tf1, tf2, tf3, tf4: REAL
		do
			h := hd
			s := sp / 100
			v := vp / 100
			c := v * s
			-- convert to sectors, where each sector is 60 degrees
			-- 0-59, 61-119, 120-179, 180-239, 240-299, 330-359
			-- e.g. 119 // 60 = 1 (1 degree inside of first sector)

			hh := hd.rounded // 60
-- TODO x does seem to be the spot   where things go wrong
		--RFO 	X = C*(1-Math.abs(hh%2-1));
			tf1 := (hh \\ 2).to_real
			tf2 := tf1 - 1
			tf3 := tf2.abs
			tf4 := 1 - tf3
			x := c * (1 - ((hh \\ 2) - 1).abs)
			-- hh must be >= 0, by precondition
			if hh < 1 then
				r := c
				g := x
			elseif hh < 2 then
				r := x
				g := c
			elseif hh < 3 then
				g := c
				b := x
			elseif hh < 4 then
				g := x
				b := c
			elseif hh < 5 then
				r := x
				b := c
			else
				r := c
				b := x
			end
			m := v - c
			r := r + m
			g := g + m
			b := b + m
			Result := {ARRAY [REAL]} << r, g, b >>
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	contrast_ratio (c1, c2: EV_COLOR): REAL
			-- Contrast ratio between colors 'c1' and 'c2'
			-- NOTE WELL that expectation is 'c1' is lighter than 'c2',
			-- else the ratio becomes > 1.0 (you could take the inverse
			-- of that of you'd like)
		local
			tc1, tc2: AEL_V2_COLOR
			lum1, lum2: REAL
		do
			create tc1.make_with_color (c1)
			create tc2.make_with_color (c2)
			lum1 := tc1.lightness
			lum2 := tc2.lightness
			Result := (lum1 + 0.05) / (lum2 + 0.05)
		end

--|========================================================================
feature -- Constants
--|========================================================================

	Kr_0: REAL_32 = 0.0
	Kr_1: REAL_32 = 1.0
	Kr_2: REAL_32 = 2.0
	Kr_100: REAL_32 = 100.0
	Kr_127_5: REAL_32 = 127.5
	Kr_255: REAL_32 = 255.0
	Kr_360: REAL_32 = 360.0

	av2_colors: AEL_V2_COLORS
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Reference
--|========================================================================

--RFO function HSVtoRGB(h, s, v) {
--RFO     var r, g, b, i, f, p, q, t;
--RFO     i = Math.floor(h * 6);
--RFO     f = h * 6 - i;
--RFO     p = v * (1 - s);
--RFO     q = v * (1 - f * s);
--RFO     t = v * (1 - (1 - f) * s);
--RFO     switch (i % 6) {
--RFO         case 0: r = v, g = t, b = p; break;
--RFO         case 1: r = q, g = v, b = p; break;
--RFO         case 2: r = p, g = v, b = t; break;
--RFO         case 3: r = p, g = q, b = v; break;
--RFO         case 4: r = t, g = p, b = v; break;
--RFO         case 5: r = v, g = p, b = q; break;
--RFO     }
--RFO     return {
--RFO         r: Math.round(r * 255),
--RFO         g: Math.round(g * 255),
--RFO         b: Math.round(b * 255)
--RFO     };
--RFO }

-- HSV to RGB
		--RFO function calc()
		--RFO {
		--RFO 	h = document.calcform.h.value;
		--RFO 	s = document.calcform.s.value;
		--RFO 	v = document.calcform.v.value;
		--RFO 	if( h=="" ) h=0;
		--RFO 	if( s=="" ) s=0;
		--RFO 	if( v=="" ) v=0;
		--RFO 	h = parseFloat(h);
		--RFO 	s = parseFloat(s);
		--RFO 	v = parseFloat(v);
		--RFO 	if( h<0 ) h=0;
		--RFO 	if( s<0 ) s=0;
		--RFO 	if( v<0 ) v=0;
		--RFO 	if( h>=360 ) h=359;
		--RFO 	if( s>100 ) s=100;
		--RFO 	if( v>100 ) v=100;
		--RFO 	s/=100;
		--RFO 	v/=100;
		--RFO 	C = v*s;
		--RFO 	hh = h/60;
		--RFO 	X = C*(1-Math.abs(hh%2-1));
		--RFO 	r = g = b = 0;
		--RFO 	if( hh>=0 && hh<1 )
		--RFO 	{
		--RFO 		r = C;
		--RFO 		g = X;
		--RFO 	}
		--RFO 	else if( hh>=1 && hh<2 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		g = C;
		--RFO 	}
		--RFO 	else if( hh>=2 && hh<3 )
		--RFO 	{
		--RFO 		g = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	else if( hh>=3 && hh<4 )
		--RFO 	{
		--RFO 		g = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else if( hh>=4 && hh<5 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else
		--RFO 	{
		--RFO 		r = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	m = v-C;
		--RFO 	r += m;
		--RFO 	g += m;
		--RFO 	b += m;
		--RFO 	r *= 255.0;
		--RFO 	g *= 255.0;
		--RFO 	b *= 255.0;
		--RFO 	r = Math.round(r);
		--RFO 	g = Math.round(g);
		--RFO 	b = Math.round(b);
		--RFO 	hex = r*65536+g*256+b;
		--RFO 	hex = hex.toString(16,6);
		--RFO 	len = hex.length;
		--RFO 	if( len<6 )
		--RFO 		for(i=0; i<6-len; i++)
		--RFO 			hex = '0'+hex;
		--RFO  	document.calcform.hex.value = hex.toUpperCase();
		--RFO  	document.calcform.r.value = r;
		--RFO  	document.calcform.g.value = g;
		--RFO  	document.calcform.b.value = b;
		--RFO  	document.calcform.color.style.backgroundColor='#'+hex;
		--RFO }

-- HSL to RGB
		--RFO function calc()
		--RFO {
		--RFO 	h = document.calcform.h.value;
		--RFO 	s = document.calcform.s.value;
		--RFO 	v = document.calcform.v.value;
		--RFO 	if( h=="" ) h=0;
		--RFO 	if( s=="" ) s=0;
		--RFO 	if( v=="" ) v=0;
		--RFO 	h = parseFloat(h);
		--RFO 	s = parseFloat(s);
		--RFO 	v = parseFloat(v);
		--RFO 	if( h<0 ) h=0;
		--RFO 	if( s<0 ) s=0;
		--RFO 	if( v<0 ) v=0;
		--RFO 	if( h>=360 ) h=359;
		--RFO 	if( s>100 ) s=100;
		--RFO 	if( v>100 ) v=100;
		--RFO 	s/=100;
		--RFO 	v/=100;
		--RFO 	C = v*s;
		--RFO 	hh = h/60;
		--RFO 	X = C*(1-Math.abs(hh%2-1));
		--RFO 	r = g = b = 0;
		--RFO 	if( hh>=0 && hh<1 )
		--RFO 	{
		--RFO 		r = C;
		--RFO 		g = X;
		--RFO 	}
		--RFO 	else if( hh>=1 && hh<2 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		g = C;
		--RFO 	}
		--RFO 	else if( hh>=2 && hh<3 )
		--RFO 	{
		--RFO 		g = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	else if( hh>=3 && hh<4 )
		--RFO 	{
		--RFO 		g = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else if( hh>=4 && hh<5 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else
		--RFO 	{
		--RFO 		r = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	m = v-C;
		--RFO 	r += m;
		--RFO 	g += m;
		--RFO 	b += m;
		--RFO 	r *= 255.0;
		--RFO 	g *= 255.0;
		--RFO 	b *= 255.0;
		--RFO 	r = Math.round(r);
		--RFO 	g = Math.round(g);
		--RFO 	b = Math.round(b);
		--RFO 	hex = r*65536+g*256+b;
		--RFO 	hex = hex.toString(16,6);
		--RFO 	len = hex.length;
		--RFO 	if( len<6 )
		--RFO 		for(i=0; i<6-len; i++)
		--RFO 			hex = '0'+hex;
		--RFO  	document.calcform.hex.value = hex.toUpperCase();
		--RFO  	document.calcform.r.value = r;
		--RFO  	document.calcform.g.value = g;
		--RFO  	document.calcform.b.value = b;
		--RFO  	document.calcform.color.style.backgroundColor='#'+hex;
		--RFO }

-- HSL to RGB
		--RFO function calc()
		--RFO {
		--RFO 	h = document.calcform.h.value;
		--RFO 	s = document.calcform.s.value;
		--RFO 	l = document.calcform.l.value;
		--RFO 	if( h=="" ) h=0;
		--RFO 	if( s=="" ) s=0;
		--RFO 	if( l=="" ) l=0;
		--RFO 	h = parseFloat(h);
		--RFO 	s = parseFloat(s);
		--RFO 	l = parseFloat(l);
		--RFO 	if( h<0 ) h=0;
		--RFO 	if( s<0 ) s=0;
		--RFO 	if( l<0 ) l=0;
		--RFO 	if( h>=360 ) h=359;
		--RFO 	if( s>100 ) s=100;
		--RFO 	if( l>100 ) l=100;
		--RFO 	s/=100;
		--RFO 	l/=100;
		--RFO 	C = (1-Math.abs(2*l-1))*s;
		--RFO 	hh = h/60;
		--RFO 	X = C*(1-Math.abs(hh%2-1));
		--RFO 	r = g = b = 0;
		--RFO 	if( hh>=0 && hh<1 )
		--RFO 	{
		--RFO 		r = C;
		--RFO 		g = X;
		--RFO 	}
		--RFO 	else if( hh>=1 && hh<2 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		g = C;
		--RFO 	}
		--RFO 	else if( hh>=2 && hh<3 )
		--RFO 	{
		--RFO 		g = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	else if( hh>=3 && hh<4 )
		--RFO 	{
		--RFO 		g = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else if( hh>=4 && hh<5 )
		--RFO 	{
		--RFO 		r = X;
		--RFO 		b = C;
		--RFO 	}
		--RFO 	else
		--RFO 	{
		--RFO 		r = C;
		--RFO 		b = X;
		--RFO 	}
		--RFO 	m = l-C/2;
		--RFO 	r += m;
		--RFO 	g += m;
		--RFO 	b += m;
		--RFO 	r *= 255.0;
		--RFO 	g *= 255.0;
		--RFO 	b *= 255.0;
		--RFO 	r = Math.round(r);
		--RFO 	g = Math.round(g);
		--RFO 	b = Math.round(b);
		--RFO 	hex = r*65536+g*256+b;
		--RFO 	hex = hex.toString(16,6);
		--RFO 	len = hex.length;
		--RFO 	if( len<6 )
		--RFO 		for(i=0; i<6-len; i++)
		--RFO 			hex = '0'+hex;
		--RFO  	document.calcform.hex.value = hex.toUpperCase();
		--RFO  	document.calcform.r.value = r;
		--RFO  	document.calcform.g.value = g;
		--RFO  	document.calcform.b.value = b;
		--RFO  	document.calcform.color.style.backgroundColor='#'+hex;
		--RFO }

--RFO /**
--RFO  * Converts an HSL color value to RGB. Conversion formula
--RFO  * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
--RFO  * Assumes h, s, and l are contained in the set [0, 1] and
--RFO  * returns r, g, and b in the set [0, 255].
--RFO  *
--RFO  * @param   Number  h       The hue
--RFO  * @param   Number  s       The saturation
--RFO  * @param   Number  l       The lightness
--RFO  * @return  Array           The RGB representation
--RFO  */
--RFO function hslToRgb(h, s, l){
--RFO     var r, g, b;
--RFO     if(s == 0){
--RFO         r = g = b = l; // achromatic
--RFO     }else{
--RFO         function hue2rgb(p, q, t){
--RFO             if(t < 0) t += 1;
--RFO             if(t > 1) t -= 1;
--RFO             if(t < 1/6) return p + (q - p) * 6 * t;
--RFO             if(t < 1/2) return q;
--RFO             if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
--RFO             return p;
--RFO         }
--RFO         var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
--RFO         var p = 2 * l - q;
--RFO         r = hue2rgb(p, q, h + 1/3);
--RFO         g = hue2rgb(p, q, h);
--RFO         b = hue2rgb(p, q, h - 1/3);
--RFO     }
--RFO     return [r * 255, g * 255, b * 255];
--RFO }

	--|--------------------------------------------------------------
--RFO //Get the maximum between R, G, and B
--RFO float tM = Math.Max(Ri, Math.Max(Gi, Bi));

--RFO //If the maximum value is 0, immediately return pure black.
--RFO if(tM == 0)
--RFO    { return new rgbwcolor() { r = 0, g = 0, b = 0, w = 0 }; }

--RFO //This section serves to figure out what the color with 100% hue is
--RFO float multiplier = 255.0f / tM;
--RFO float hR = Ri * multiplier;
--RFO float hG = Gi * multiplier;
--RFO float hB = Bi * multiplier;  

--RFO //This calculates the Whiteness (not strictly speaking Luminance) of the color
--RFO float M = Math.Max(hR, Math.Max(hG, hB));
--RFO float m = Math.Min(hR, Math.Min(hG, hB));
--RFO float Luminance = ((M + m) / 2.0f - 127.5f) * (255.0f/127.5f) / multiplier;

--RFO //Calculate the output values
--RFO int Wo = Convert.ToInt32(Luminance);
--RFO int Bo = Convert.ToInt32(Bi - Luminance);
--RFO int Ro = Convert.ToInt32(Ri - Luminance);
--RFO int Go = Convert.ToInt32(Gi - Luminance);

--RFO //Trim them so that they are all between 0 and 255
--RFO if (Wo < 0) Wo = 0;
--RFO if (Bo < 0) Bo = 0;
--RFO if (Ro < 0) Ro = 0;
--RFO if (Go < 0) Go = 0;
--RFO if (Wo > 255) Wo = 255;
--RFO if (Bo > 255) Bo = 255;
--RFO if (Ro > 255) Ro = 255;
--RFO if (Go > 255) Go = 255;
--RFO return new rgbwcolor() { r = Ro, g = Go, b = Bo, w = Wo };


--RFO struct colorRgbw {
--RFO   unsigned int   red;
--RFO   unsigned int   green;
--RFO   unsigned int   blue;
--RFO   unsigned int   white;
--RFO };
 
	--|--------------------------------------------------------------
--RFO // The saturation is the colorfulness of a color relative to its own brightness.
--RFO unsigned int saturation(colorRgbw rgbw) {
--RFO     // Find the smallest of all three parameters.
--RFO     float low = min(rgbw.red, min(rgbw.green, rgbw.blue));
--RFO     // Find the highest of all three parameters.
--RFO     float high = max(rgbw.red, max(rgbw.green, rgbw.blue));
--RFO     // The difference between the last two variables
--RFO     // divided by the highest is the saturation.
--RFO     return round(100 * ((high - low) / high));
--RFO }
 
--RFO // Returns the value of White
--RFO unsigned int getWhite(colorRgbw rgbw) {
--RFO     return (255 - saturation(rgbw)) / 255 * (rgbw.red + rgbw.green + rgbw.blue) / 3;
--RFO }
 
--RFO // Use this function for too bright emitters. It corrects the highest possible value.
--RFO unsigned int getWhite(colorRgbw rgbw, int redMax, int greenMax, int blueMax) {
--RFO     // Set the maximum value for all colors.
--RFO     rgbw.red = (float)rgbw.red / 255.0 * (float)redMax;
--RFO     rgbw.green = (float)rgbw.green / 255.0 * (float)greenMax;
--RFO     rgbw.blue = (float)rgbw.blue / 255.0 * (float)blueMax;
--RFO     return (255 - saturation(rgbw)) / 255 * (rgbw.red + rgbw.green + rgbw.blue) / 3;
--RFO     return 0;
--RFO }
 
--RFO // Example function.
--RFO colorRgbw rgbToRgbw(unsigned int red, unsigned int green, unsigned int blue) {
--RFO     unsigned int white = 0;
--RFO     colorRgbw rgbw = {red, green, blue, white};
--RFO     rgbw.white = getWhite(rgbw);
--RFO     return rgbw;
--RFO }
 
--RFO // Example function with color correction.
--RFO colorRgbw rgbToRgbw(unsigned int red, unsigned int redMax,
--RFO                     unsigned int green, unsigned int greenMax,
--RFO                     unsigned int blue, unsigned int blueMax) {
--RFO     unsigned int white = 0;
--RFO     colorRgbw rgbw = {red, green, blue, white};
--RFO     rgbw.white = getWhite(rgbw, redMax, greenMax, blueMax);
--RFO     return rgbw;
--RFO }
	--|--------------------------------------------------------------


	--|--------------------------------------------------------------

	-- GRBW      RGB 8
	-- 00000000  000000  Black
	-- ffffff00  ffffff  White
	-- 00ff0000  ff0000  Red
	-- ff000000  00ff00  Green
	-- 0000ff00  0000ff  Blue
	-- ffff0000  ffff00  Yellow
	-- ff00ff00  00ffff  Cyan
	-- 0099cc00  9900cc  Purple
	-- bfffbf00  ffbfbf  Pink
	-- ffbfbf00  bfbfff  Pale Green
	-- bfbfff00  bfbfff  Light Blue
	-- ffffbf00  ffffbf  Pale Yellow
	-- ffbfff00  bfffff  Light Cyan
	-- bfffff00  bfffff  Light Magenta
	-- cccccc00  cccccc  20% black
	-- 80808000  808080  50% black

--|----------------------------------------------------------------------
--| History
--|
--| 001 02-Apr-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be used as a direct replacement for EV_COLOR
--| Its enhanced features are exported to all
--|----------------------------------------------------------------------

end -- class AEL_V2_COLOR
