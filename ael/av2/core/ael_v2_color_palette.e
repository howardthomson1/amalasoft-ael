note
	description: "{
A color palette, implemented as a paired list and hash table
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_COLOR_PALETTE

inherit
	AEL_SPRT_MULTI_FALLIBLE
		redefine
			default_create
		end
	AEL_PRINTF
		undefine
			default_create, copy, is_equal
		end

create
	make_from_file, make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_file (fn: STRING)
			-- Create Current from file with name 'fn'
		do
			make
			init_from_file (fn)
		end

	--|--------------------------------------------------------------

	make
		do
			default_create
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			output_format := "JSON"
			create color_list.make
			color_list.compare_objects
			create color_table.make (101)
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_file (fn: STRING)
		local
			tf: PLAIN_TEXT_FILE
			line: STRING
		do
			create tf.make_with_name (fn)
			if not tf.exists then
				-- ERROR
			else
				-- keep input file name?
				tf.open_read
				if not tf.is_open_read then
					-- ERROR
				else
					tf.read_line
					line := tf.last_string
					if first_line_is_json (line) then
						init_from_json_file (tf, line)
					elseif first_line_is_json (line) then
						init_from_cpf_file (tf, line)
					else
						-- ERROR
					end
					tf.close
				end
			end
		end

	--|--------------------------------------------------------------

	init_from_cpf_file (tf: PLAIN_TEXT_FILE; line1: STRING)
			-- Initialize Current from CSF file 'tf', whose first line has 
			-- been read into 'line1'
		require
			is_open: tf.is_open_read
			valid_line: first_line_is_cpf (line1)
		local
			sl: LIST [STRING]
			cl: LIST [EV_COLOR]
			ts: STRING
			nc: INTEGER
		do
			-- First line has number of colors, then color model
			sl := line1.split (',')
			if sl.count /= 2 then
				-- ERROR
			else
				ts := sl.first
				if not ts.is_integer then
					-- ERROR
				else
					nc := ts.to_integer
					-- Colors are stacked as a comma-separated list, on 
					-- the second line in the file
					tf.read_line
					sl := tf.last_string.split (',')
					tf.close
					if sl.count /= nc then
						-- ERROR
					else
						cl := ael_v2_colors.rgb_hex_strings_to_colors (sl)
						set_colors (cl)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	init_from_json_file (tf: PLAIN_TEXT_FILE; line1: STRING)
			-- Initialize Current from JSON file 'tf', whose first line has 
			-- been read into 'line1'
		require
			is_open: tf.is_open_read
			valid_line: first_line_is_json (line1)
		local
			jso: AEL_JSON_OBJECT
		do
			tf.start
			tf.read_stream (tf.count)
			create jso.make_from_stream ("colors", tf.last_string)
			if jso.has_error then
				copy_errors (jso)
			else
				init_from_json_object (jso)
			end
		end

	--|--------------------------------------------------------------

	init_from_json_object (jso: AEL_JSON_OBJECT)
			-- Initialize Current from JSON object 'jso'
			-- Format:
			--    "colors": {
			--        "count": 4,
			--        "model": "rgb",
			--        "items": [
			--             "ff8000",
			--             "ffcbbe",
			--             "ff0000",
			--             "ffbdc9"
			--        ]
			--    }
		local
			nc: INTEGER_64
			ms, ts: STRING
			sel: LINKED_LIST [AEL_JSON_OBJECT]
			co: AEL_JSON_OBJECT
			--tc: EV_COLOR
			--cht: HASH_TABLE [STRING, STRING]
			sl: LINKED_LIST [STRING]
			cl: LIST [EV_COLOR]
		do
			nc := jso.value_for_integer_attribute ("count")
			ms := jso.value_for_attribute ("model")
			sel := jso.serial_elements
			-- sel should have a single item: "items", a JSON array
			-- Each item in the array is a color
			if sel.count /= 1 then
				set_error_message (
					"ERROR initializing frame from JSON. Missing color map items.")
			elseif not attached {AEL_JSON_ARRAY}sel.first as jsa then
				set_error_message (
					"ERROR initializing frame from JSON. Unexpected element in color map.")
			else
				create sl.make
				sel := jsa.serial_elements
				from sel.start
				until sel.after
				loop
					co := sel.item
					-- co is a string object, its attribute values are a 
					-- hash table of 1 item, the color string, keyed by 
					-- the color string
					if attached co.attribute_values as cht
						and then cht.count = 1
					 then
						cht.start
						ts := cht.item_for_iteration
						sl.extend (ts)
					else
						-- ERROR
					end
					sel.forth
				end
				if sl.count /= nc then
					-- ERROR
				else
					cl := ael_v2_colors.rgb_hex_strings_to_colors (sl)
					set_colors (cl)
				end
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_stack_disabled: BOOLEAN
			-- Is stack behavior (LIFO) disabled?
			-- Default behavior is enabled

	output_format: STRING
			-- Tag identifying preferred output format

	output_format_is_json: BOOLEAN
			-- Is preferred output format JSON?
		do
			Result := output_format ~ "JSON"
		end

	--|--------------------------------------------------------------

	count: INTEGER
			-- Number of colors in Current
		do
			Result := color_list.count
		end

	is_empty: BOOLEAN
			-- Is Current devoid of items?
		do
			Result := color_list.count = 0
		end

	--|--------------------------------------------------------------

	color_list: TWO_WAY_LIST [EV_COLOR]
			-- List of colors in Current, with stack-ish behavior

	color_table: HASH_TABLE [EV_COLOR, INTEGER]
			-- Hash table with same contents as palette, but keyed 
			-- by RGB value, to enable more efficient lookup and
			-- avoid duplication

	has (v: EV_COLOR): BOOLEAN
			-- Does Current have 'v' (or equivalent)?
		do
			Result := color_table.has (v.rgb_24_bit)
		end

	--|--------------------------------------------------------------

	after: BOOLEAN
			-- Is cursor position in Current 'after' end
		do
			Result := color_list.after
		end

	before: BOOLEAN
			-- Is cursor position in Current 'before' start
		do
			Result := color_list.before
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	disable_stack_behavior
			-- Disable stack behavior (LIFO)
			-- New items insert at end, existing do not change position
		do
			is_stack_disabled := True
		end

	enable_stack_behavior
			-- Enable stack behavior (LIFO, default)
			-- New items insert at front, existing move on reset
		do
			is_stack_disabled := False
		end

	set_output_format (v: STRING)
		require
			valid: is_valid_output_format (v)
		do
			output_format := v
		end

	set_output_format_json
			-- Set, to JSON, the format for generated output
		do
			output_format := "JSON"
		end

	set_output_format_cpf
			-- Set, to CFP, the format for generated output
		do
			output_format := "CPF"
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: EV_COLOR)
			-- Add 'v' to Current as most recent
		require
			is_new: not has (v)
		do
			color_table.extend (v, v.rgb_24_bit)
			if is_stack_disabled then
				color_list.extend (v)
			else
				color_list.put_front (v)
			end
		end

	--|--------------------------------------------------------------

	move_to_front (v: EV_COLOR)
			-- Move color 'v' from its current position to front of list
		require
			has_it: has (v)
		local
			oc: CURSOR
		do
			oc := color_list.cursor
			color_list.start
			color_list.search (v)
			if color_list.exhausted then
				-- ERROR
			else
				color_list.remove
			end
			if color_list.valid_cursor (oc) then
				color_list.go_to (oc)
			end
			color_list.put_front (v)
		end

	--|--------------------------------------------------------------

	replace_i_th (v: EV_COLOR; i: INTEGER)
			-- Replace the color in position 'i' with 'v'
		require
			valid_index: i > 0 and i <= count
			not_there_already: not color_table.has (v.rgb_24_bit)
		local
			tc: EV_COLOR
		do
			tc := i_th (i)
			color_table.remove (tc.rgb_24_bit)
			color_list.go_i_th (i)
			color_list.replace (v)
			color_table.extend (v, v.rgb_24_bit)
		end

	--|--------------------------------------------------------------

	set_colors (cl: LIST [EV_COLOR])
			-- Replace contents of Current with that of 'cl'
			-- Retain item order (i.e. first in cl will be first in 
			-- Current)
		local
			tc: EV_COLOR
		do
			wipe_out
			from cl.start
			until cl.after
			loop
				tc := cl.item
				color_table.extend (tc, tc.rgb_24_bit)
				color_list.extend (tc)
				cl.forth
			end
		ensure
			same_count: count = cl.count
			same_first: count > 0 implies first = cl.first
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	wipe_out
			-- Remove all items from Current
		do
			color_list.wipe_out
			color_table.wipe_out
		ensure
			empty: is_empty
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: EV_COLOR
			-- Item a cursor, if any
		require
			not_off: not (before or after)
		do
			Result := color_list.item
		end

	first: detachable EV_COLOR
			-- First item in Current, if any
		do
			if not color_list.is_empty then
				Result := color_list.first
			end
		end

	i_th (i: INTEGER): EV_COLOR
			-- 'i'_th item in Current, if any
		require
			valid_index: i > 0 and i <= count
		do
			Result := color_list.i_th (i)
		end

--|========================================================================
feature -- Cursor movement
--|========================================================================

	start
			-- Position at front of list
		do
			color_list.start
		end

	forth
			-- Move forward 1 position
		do
			color_list.forth
		end

	back
			-- Move backward 1 position
		do
			color_list.back
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_output_format (v: STRING): BOOLEAN
			-- Is 'v' a valid output format
		do
			if not v.is_empty then
				Result :=  v ~ "JSON" or v ~ "CPF"
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

--|========================================================================
feature -- Serialization
--|========================================================================

	first_line_is_cpf (v: STRING): BOOLEAN
			-- Do the contents of line 'v' indicate that the format
			-- of the associated file is CPF (compact paletter format)?
		local
			c1: CHARACTER
		do
			if not v.is_empty then
				c1 := v.item (1)
				if c1.is_digit and then v.occurrences (',') = 5 then
					Result := True
				end
			end
		end

	--|--------------------------------------------------------------

	first_line_is_json (v: STRING): BOOLEAN
			-- Do the contents of line 'v' indicate that the format
			-- of the associated file is JSON?
		local
			c1: CHARACTER
		do
			if not v.is_empty then
				c1 := v.item (1)
				if c1 = '%"' then
					Result := True
				end
			end
		end

	--|--------------------------------------------------------------

	serial_out: STRING
			-- Contents of Current, in presently selected format
		do
			if output_format_is_json then
				Result := json_out ("")
			else
				Result := cpf_out
			end
		end

	cpf_out: STRING
			-- CPF-formatted contents of Current
			-- Format:
			--
			-- 8,rgb
			-- ff8000,ffcbbe,ff0000,ffbdc9,ffbdf8,d0beff,bbf3ff,bfffcb
			-- 
		local
			oc: CURSOR
		do
			create Result.make (1024)
			Result.append (count.out+",rgb%N")
			oc := color_list.cursor
			from color_list.start
			until color_list.after
			loop
				Result.append (
					ael_v2_colors.color_to_rgb_hex (color_list.item, False, False))
				color_list.forth
				if color_list.after then
					Result.append ("%N")
				else
					Result.append (",")
				end
			end
			if color_list.valid_cursor (oc) then
				color_list.go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	json_out (istr: STRING): STRING
			-- JSON-formatted contents of Current
			--
			-- Format:
			--    "colors": {
			--        "count": 4,
			--        "model": "rgb",
			--        "items": [
			--             "ff8000",
			--             "ffcbbe",
			--             "ff0000",
			--             "ffbdc9"
			--        ]
			--    }
		do
			create Result.make (1024)
			Result.append (aprintf ("{
%s"colors": {
%s    "count": %d,
%s    "model": "%s",
%s    "items": [
%s
%s    ]
%s}

}",  -- }"s get unbalanced
			<< istr, istr, count, istr, "rgb",
			istr, colors_json_out (istr + "        "), istr, istr >>))
		end

	--|--------------------------------------------------------------

	colors_json_out (istr: STRING): STRING
			-- JSON-formatted color list items
		local
			oc: CURSOR
		do
			create Result.make (1024)
			oc := color_list.cursor
			from color_list.start
			until color_list.after
			loop
				Result.append (aprintf ("{
%s"%s"
}",  -- }"s get unbalanced
			<< istr,
			ael_v2_colors.color_to_rgb_hex (color_list.item, False, False) >>))
				color_list.forth
				if color_list.after then
					-- no newline; provide in enclosing format
				else
					Result.append (",%N")
				end
			end
			if color_list.valid_cursor (oc) then
				color_list.go_to (oc)
			end
		end

--|========================================================================
feature -- Shared services
--|========================================================================

	ael_v2_colors: AEL_V2_COLORS
			-- Shared color constants
		once
			create Result
		end


--|----------------------------------------------------------------------
--| History
--|
--| 001 03-Apr-2019
--|     Adapted original module from AEL original
--|     Compiled with Eiffel 18.07, void-safe
--|----------------------------------------------------------------------

end -- class AEL_V2_COLOR_PALETTE
