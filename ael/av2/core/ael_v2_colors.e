note
	description: "Common color constants and support routines"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 002$"
	howto: "{
    This class is included in the AEL_V2_CONSTANTS class as a supplier.
    It is therefore available to classes that inherit AEL_V2_CONTANTS,
    included AEL_V2_WIDGET.
    This class can be instantiated by itself if desired.
}"

class AEL_V2_COLORS

inherit
	EV_STOCK_COLORS

create
	default_create

--|========================================================================
feature -- Colors not defined already in EV_STOCK_COLORS
--|========================================================================

	beige: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.94118, {REAL_32}0.8706, {REAL_32}0.7216)
		end

	brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.584, {REAL_32}0.506, {REAL_32}0.404)
		end

	burlywood: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.8706, {REAL_32}0.7216, {REAL_32}0.5294)
		end

	cadet_blue_4: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.3255, {REAL_32}0.5255, {REAL_32}0.5451)
		end

	chocolate: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.8235, {REAL_32}0.4118, {REAL_32}0.1176)
		end

	dark_brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.443, {REAL_32}0.388, {REAL_32}0.365)
		end

	dark_khaki: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.7412, {REAL_32}0.7176, {REAL_32}0.4196)
		end

	dark_navy: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.306, {REAL_32}0.286, {REAL_32}0.365)
		end

	dark_red_brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.4, {REAL_32}0.1, {REAL_32}0.1)
		end

	dark_teal: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.129, {REAL_32}0.345, {REAL_32}0.404)
		end

	deep_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.0, {REAL_32}0.25, {REAL_32}0.0)
		end

	ecru: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.8, {REAL_32}0.7, {REAL_32}0.5)
		end

	extra_dark_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.075, {REAL_32}0.075, {REAL_32}0.2)
		end

	extra_dark_gray: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.1, {REAL_32}0.1, {REAL_32}0.1)
		end

	extra_dark_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.05, {REAL_32}0.2, {REAL_32}0.05)
		end

	extra_dark_red: AEL_V2_COLOR
			-- 51, 0, 6
		once
			create Result.make_with_rgb (
				{REAL_32}0.2, {REAL_32}0.0, {REAL_32}0.0235)
		end

	extra_dark_teal: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.05, {REAL_32}0.175, {REAL_32}0.25)
		end

	extra_pale_yellow: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, 1, {REAL_32}0.9)
		end

	extra_pale_tan: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.98, {REAL_32}0.97, {REAL_32}0.95)
		end

	extra_pale_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (.9, 1, 1)
		end

	extra_pale_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.9, 1, {REAL_32}0.9)
		end

	firebrick: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.698, {REAL_32}0.133, {REAL_32}0.133)
		end

	forest_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.1333, {REAL_32}0.545, {REAL_32}0.1333)
		end

	gold: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.843, 0)
		end

	honeydew: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.94118, 1, {REAL_32}0.94118)
		end

	indian_red: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.8039, {REAL_32}0.3608, {REAL_32}0.3608)
		end

	khaki: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.9647, {REAL_32}0.5608)
		end

	lemon_chiffon: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.9804, {REAL_32}0.8039)
		end

	light_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.6784, {REAL_32}0.847, {REAL_32}0.9019)
		end

	light_brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.757, {REAL_32}0.639, {REAL_32}0.486)
		end

	light_cyan: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.87843, 1, 1)
		end

	light_goldenrod: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.9333, {REAL_32}0.8666, {REAL_32}0.5098)
		end

	light_gray: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.9, {REAL_32}0.9, {REAL_32}0.9)
		end

	light_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.863, 1, {REAL_32}0.863)
		end

	light_pink: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.7137, {REAL_32}0.7569)
		end

	light_sky_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.5294, {REAL_32}0.8078, {REAL_32}0.9804)
		end

	light_steel_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.792, {REAL_32}0.863, {REAL_32}0.949)
		end

	light_yellow: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, 1, {REAL_32}0.4)
		end

	linen: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.9804, {REAL_32}0.9412, {REAL_32}0.902)
		end

	maroon4: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.5451, {REAL_32}0.1098, {REAL_32}0.3843)
		end

	mint_cream: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.9608, 1, {REAL_32}0.9804)
		end

	navy: AEL_V2_COLOR
		once
			create Result.make_with_rgb (0, 0, {REAL_32}0.5)
		end

	olive_drab: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.45, {REAL_32}0.44, {REAL_32}0.39)
		end

	orange: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.5, 0)
		end

	light_orange: AEL_V2_COLOR
			-- FFA54B  255 165 75
		once
			create Result.make_with_rgb (1, {REAL_32}0.647, {REAL_32}0.294)
		end

	orange_red: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.25, 0)
		end

	pale_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (.7098, 1, 1)
		end

	pale_goldenrod: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.9333, {REAL_32}0.9098, {REAL_32}0.6667)
		end

	pale_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.7, 1, {REAL_32}0.7)
		end

	pale_yellow: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, 1, {REAL_32}0.7)
		end

	peach_puff: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.8549, {REAL_32}0.7255)
		end

	peru: AEL_V2_COLOR
		once
			create Result.make_with_rgb (.8039, {REAL_32}0.5216, {REAL_32}0.2471)
		end

	pink: AEL_V2_COLOR
		once
			create Result.make_with_rgb (1, {REAL_32}0.7529, {REAL_32}0.7961)
		end

	powder_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.6902, {REAL_32}0.87843, {REAL_32}0.9019)
		end

	purple: AEL_V2_COLOR
			-- 0x9900cc (153, 0, 204)
		once
			create Result.make_with_rgb ({REAL_32}0.6, 0, {REAL_32}0.8)
		end

	red_brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.6, {REAL_32}0.408, {REAL_32}0.29)
		end

	royal_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.255, {REAL_32}0.41176, {REAL_32}0.8824)
		end

	saddle_brown: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.5451, {REAL_32}0.2706, {REAL_32}0.0745)
		end

	sienna: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.6275, {REAL_32}0.3216, {REAL_32}0.1765)
		end

	sky_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.5294, {REAL_32}0.8078, {REAL_32}0.9216)
		end

	soft_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.75, 1, {REAL_32}0.75)
		end

	steel_blue: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.553, {REAL_32}0.706, {REAL_32}0.890)
		end

	super_dark_green: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.05, {REAL_32}0.15, {REAL_32}0.05)
		end

	tan: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.8235, {REAL_32}0.7059, {REAL_32}0.5490)
		end

	tan2: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.8588, {REAL_32}0.8078, {REAL_32}0.7019)
		end

	light_tan: AEL_V2_COLOR
		once
			-- 230 219 196
			create Result.make_with_rgb (
				{REAL_32}0.9019, {REAL_32}0.8588, {REAL_32}0.7686)
		end

	pale_tan: AEL_V2_COLOR
		once
			-- 242 234 217
			create Result.make_with_rgb (
				{REAL_32}0.9490, {REAL_32}0.9176, {REAL_32}0.8509)
		end

	tan3: AEL_V2_COLOR
		once
			create Result.make_with_rgb (.8039, {REAL_32}0.5216, {REAL_32}0.2471)
		end

	teal: AEL_V2_COLOR
		once
			create Result.make_with_rgb (0, {REAL_32}0.5, {REAL_32}0.5)
		end

	pale_violet: AEL_V2_COLOR
		once
			create Result.make_with_rgb ({REAL_32}0.863, {REAL_32}0.753, 1)
		end

	violet: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.9333, {REAL_32}0.5098, {REAL_32}0.9333)
		end

	wheat: AEL_V2_COLOR
		once
			create Result.make_with_rgb (
				{REAL_32}0.9608, {REAL_32}0.8706, {REAL_32}0.702)
		end

	windows_gray: EV_COLOR
		do
			Result := default_background_color
		end

	--|--------------------------------------------------------------

	gray_10: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (10)
		end

	gray_20: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (20)
		end

	gray_25: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (25)
		end

	gray_30: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (30)
		end

	gray_40: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (40)
		end

	gray_50: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (50)
		end

	gray_60: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (60)
		end

	gray_70: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (70)
		end

	gray_75: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (75)
		end

	gray_80: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (80)
		end

	gray_85: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (85)
		end

	gray_90: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (90)
		end

	gray_95: AEL_V2_COLOR
		do
			Result := gray_by_percent_white (95)
		end

	--|--------------------------------------------------------------

	gray_by_percent_black (v: INTEGER): AEL_V2_COLOR
			-- A gray that is from 0 to 100 percent black
		require
			value_in_range: v >= 0 and v <= 100
		local
			b: REAL_32
		do
			b := 1 - (v / {REAL_32}100.0)
			create Result.make_with_rgb (b, b, b)
		end

	--|--------------------------------------------------------------

	gray_by_percent_white (v: INTEGER): AEL_V2_COLOR
			-- A gray that is from 0 to 100 percent white
		require
			value_in_range: v >= 0 and v <= 100
		local
			b: REAL_32
		do
			b := v.to_real / {REAL_32}100.0
			create Result.make_with_rgb (b, b, b)
		end

	--|--------------------------------------------------------------

	colors_by_name: HASH_TABLE [EV_COLOR, STRING]
			-- Collection of defined color constants, indexed by their common 
			-- names
		once
			create Result.make (101)
			Result.extend (beige, "beige")
			Result.extend (black, "black")
			Result.extend (blue, "blue")
			Result.extend (brown, "brown")
			Result.extend (burlywood, "burlywood")
			Result.extend (cadet_blue_4, "cadet_blue_4")
			Result.extend (chocolate, "chocolate")
			Result.extend (cyan, "cyan")
			Result.extend (dark_blue, "dark_blue")
			Result.extend (dark_brown, "dark_brown")
			Result.extend (dark_cyan, "dark_cyan")
			Result.extend (dark_gray, "dark_gray")
			Result.extend (dark_gray, "dark_grey")
			Result.extend (dark_green, "dark_green")
			Result.extend (dark_khaki, "dark_khaki")
			Result.extend (dark_magenta, "dark_magenta")
			Result.extend (dark_navy, "dark_navy")
			Result.extend (dark_red, "dark_red")
			Result.extend (dark_yellow, "dark_yellow")
			Result.extend (ecru, "ecru")
			Result.extend (extra_pale_tan, "extra_pale_tan")
			Result.extend (extra_pale_yellow, "extra_pale_yellow")
			Result.extend (extra_pale_blue, "extra_pale_blue")
			Result.extend (extra_pale_green, "extra_pale_green")
			Result.extend (forest_green, "forest_green")
			Result.extend (firebrick, "firebrick")
			Result.extend (gold, "gold")
			Result.extend (gray, "gray")
			Result.extend (gray, "grey")
			Result.extend (gray_25, "gray_25")
			Result.extend (gray_25, "grey_25")
			Result.extend (gray_40, "gray_40")
			Result.extend (gray_40, "grey_40")
			Result.extend (gray_50, "gray_50")
			Result.extend (gray_50, "grey_50")
			Result.extend (gray_60, "gray_60")
			Result.extend (gray_60, "grey_60")
			Result.extend (gray_70, "gray_70")
			Result.extend (gray_70, "grey_70")
			Result.extend (gray_75, "gray_75")
			Result.extend (gray_75, "grey_75")
			Result.extend (gray_80, "gray_80")
			Result.extend (gray_80, "grey_80")
			Result.extend (gray_90, "gray_90")
			Result.extend (gray_90, "grey_90")
			Result.extend (green, "green")
			Result.extend (honeydew, "honeydew")
			Result.extend (indian_red, "indian_red")
			Result.extend (khaki, "khaki")
			Result.extend (honeydew, "lemon_chiffon")
			Result.extend (light_blue, "light_blue")
			Result.extend (light_brown, "light_brown")
			Result.extend (light_cyan, "light_cyan")
			Result.extend (light_goldenrod, "light_goldenrod")
			Result.extend (light_gray, "light_gray")
			Result.extend (light_green, "light_green")
			Result.extend (light_pink, "light_pink")
			Result.extend (light_sky_blue, "light_sky_blue")
			Result.extend (light_yellow, "light_yellow")
			Result.extend (linen, "linen")
			Result.extend (magenta, "magenta")
			Result.extend (maroon4, "maroon4")
			Result.extend (mint_cream, "mint_cream")
			Result.extend (navy, "navy")
			Result.extend (olive_drab, "olive_drab")
			Result.extend (orange, "orange")
			Result.extend (orange_red, "orange_red")
			Result.extend (pale_blue, "pale_blue")
			Result.extend (pale_green, "pale_green")
			Result.extend (pale_goldenrod, "pale_goldenrod")
			Result.extend (pale_yellow, "pale_yellow")
			Result.extend (peach_puff, "peach_puff")
			Result.extend (peru, "peru")
			Result.extend (pink, "pink")
			Result.extend (powder_blue, "powder_blue")
			Result.extend (purple, "purple")
			Result.extend (red, "red")
			Result.extend (red_brown, "red_brown")
			Result.extend (royal_blue, "royal_blue")
			Result.extend (saddle_brown, "saddle_brown")
			Result.extend (sienna, "sienna")
			Result.extend (sky_blue, "sky_blue")
			Result.extend (soft_green, "soft_green")
			Result.extend (tan, "tan")
			Result.extend (tan3, "tan3")
			Result.extend (teal, "teal")
			Result.extend (violet, "violet")
			Result.extend (wheat, "wheat")
			Result.extend (white, "white")
			Result.extend (windows_gray, "windows_gray")
			Result.extend (yellow, "yellow")
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_hex_integer (v: STRING): BOOLEAN
			-- Is 'v' a valid hexadecimal string?
		do
			Result := assr.string_is_hex_integer (v)
		end

	--|--------------------------------------------------------------

	is_valid_rgb_hex (v: detachable STRING): BOOLEAN
			-- Is 'v' a valid hexadecimal denoting a 24-bit RGB value?
		local
			low_v: STRING
			vc: INTEGER
		do
			if attached v as tv then
				low_v := tv.as_lower
				if low_v.starts_with ("0x") then
					vc := 8
				else
					vc := 6
				end
				if low_v.count = vc then
					Result := is_valid_hex_integer (low_v)
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_rgb_hex_list (xl: LIST [STRING]): BOOLEAN
			-- Are each of the items in 'xl' valid RGB hex values?
		require
			not_empty: not xl.is_empty
		do
			Result := True
			from xl.start
			until xl.after or not Result
			loop
				Result := is_valid_rgb_hex (xl.item)
				xl.forth
			end
		end

	--|--------------------------------------------------------------

	is_valid_grbw_hex (v: STRING): BOOLEAN
			-- Is 'v' a valid hex string representation of a GRBW color?
		local
			tc: CHARACTER
			i: INTEGER
		do
			if attached v as ts and then ts.count = 8 then
				Result := True
				from i := 1
				until i > 8 or not Result
				loop
					tc := ts.item (i)
					if not tc.is_hexa_digit then
						Result := False
					else
						i := i + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_grbw_hex_list (xl: LIST [STRING]): BOOLEAN
			-- Are each of the items in 'xl' valid GRBW hex values?
		require
			not_empty: not xl.is_empty
		do
			Result := True
			from xl.start
			until xl.after or not Result
			loop
				Result := is_valid_grbw_hex (xl.item)
				xl.forth
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	color_to_rgb_hex (c: EV_COLOR; df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of 8-bit RGB values
			-- for color 'c'
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			ca: ARRAY [INTEGER]
		do
			ca := {ARRAY[INTEGER]} <<c.red_8_bit, c.green_8_bit, c.blue_8_bit>>
			Result := bytes_to_hex (ca, df, ucf)
		ensure
			valid_decorated_size: df implies Result.count = 8
			valid_plain_size: (not df) implies Result.count = 6
		end

	--|--------------------------------------------------------------

	color_to_rgbw_hex (c: EV_COLOR; df, ucf: BOOLEAN): STRING
			-- RGBW hex representation of 'c'
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			ac: AEL_V2_COLOR
			rgbw: ARRAY [INTEGER]
			apf: AEL_PRINTF
			fmt: STRING
			r, g, b, w: INTEGER
		do
			if attached {AEL_V2_COLOR} c as tac then
				ac := tac
			else
				create ac.make_with_color (c)
			end
			rgbw := ac.rgb_8_bit_to_rgbw (c.red_8_bit, c.green_8_bit, c.blue_8_bit)
			r := rgbw.item (1)
			g := rgbw.item (2)
			b := rgbw.item (3)
			w := rgbw.item (4)
			create apf
			fmt := hex_pf_format (4, df, ucf)
			Result := apf.aprintf (fmt, << r, g, b, w >>)
		ensure
			valid_decorated_size: df implies Result.count = 10
			valid_plain_size: (not df) implies Result.count = 8
		end

	--|--------------------------------------------------------------

	color_to_grbw_hex (c: EV_COLOR; df, ucf: BOOLEAN): STRING
			-- GRBW hex representation of 'c'
			-- Same values as RGBW, in different order
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			ac: AEL_V2_COLOR
			rgbw: ARRAY [INTEGER]
			apf: AEL_PRINTF
			fmt: STRING
			r, g, b, w: INTEGER
		do
			if attached {AEL_V2_COLOR} c as tac then
				ac := tac
			else
				create ac.make_with_color (c)
			end
			rgbw := ac.rgb_8_bit_to_rgbw (c.red_8_bit, c.green_8_bit, c.blue_8_bit)
			r := rgbw.item (1)
			g := rgbw.item (2)
			b := rgbw.item (3)
			w := rgbw.item (4)
			create apf
			fmt := hex_pf_format (4, df, ucf)
			Result := apf.aprintf (fmt, << g, r, b, w >>)
		ensure
			valid_decorated_size: df implies Result.count = 10
			valid_plain_size: (not df) implies Result.count = 8
		end

	--|--------------------------------------------------------------

	rgb_to_hex (r, g, b: INTEGER; df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of 8-bit RGB values
			-- 'r', 'g', and 'b'
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			rgba: ARRAY [INTEGER]
		do
			rgba := {ARRAY[INTEGER]} << r, g, b >>
			Result := bytes_to_hex (rgba, df, ucf)
		ensure
			valid_decorated_size: df implies Result.count = 8
			valid_plain_size: (not df) implies Result.count = 6
		end

	hex_out_rgb (c: EV_COLOR; df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of color 'c'
			-- as an 8-bit RGB value
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			rgba: ARRAY [INTEGER]
			r, g, b: INTEGER
		do
			r := c.red_8_bit
			g := c.green_8_bit
			b := c.blue_8_bit
			rgba := {ARRAY[INTEGER]} << r, g, b >>
			Result := bytes_to_hex (rgba, df, ucf)
		ensure
			valid_decorated_size: df implies Result.count = 8
			valid_plain_size: (not df) implies Result.count = 6
		end

	--|--------------------------------------------------------------

	hex_to_rgb (v: STRING): INTEGER
			-- 24-bit integer value corresponding to RGB hex string 'v'
		require
			valid: is_valid_rgb_hex (v)
		do
			Result := assr.hex_substring_to_integer (v, 1, v.count)
		end

	hex_to_8_bit_red (v: STRING): INTEGER
			-- 8-bit integer value for Red in RGB hex string 'v'
		require
			valid: is_valid_rgb_hex (v)
		do
			Result := assr.hex_substring_to_integer (v, 1, 2)
		end

	hex_to_8_bit_green (v: STRING): INTEGER
			-- 8-bit integer value for Green in RGB hex string 'v'
		require
			valid: is_valid_rgb_hex (v)
		do
			Result := assr.hex_substring_to_integer (v, 3, 4)
		end

	hex_to_8_bit_blue (v: STRING): INTEGER
			-- 8-bit integer value for Blue in RGB hex string 'v'
		require
			valid: is_valid_rgb_hex (v)
		do
			Result := assr.hex_substring_to_integer (v, 5, 6)
		end

	--|--------------------------------------------------------------

	bytes_to_hex (ba: ARRAY [INTEGER]; df, ucf: BOOLEAN): STRING
			-- Hexadecimal string representation of 8-bit values 'ba'
			-- Accepts 1-4 values (for 1-4 bytes)
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		require
			valid_count: ba.count > 0 and ba.count <= 4
		local
			apf: AEL_PRINTF
			fmt: STRING
		do
			create apf
			fmt := hex_pf_format (ba.count, df, ucf)
			inspect ba.count
			when 1 then
				Result := apf.aprintf (fmt, << ba.item (1) >>)
			when 2 then
				Result := apf.aprintf (fmt, << ba.item (1), ba.item (2) >>)
			when 3 then
				Result := apf.aprintf (
					fmt, << ba.item (1), ba.item (2), ba.item (3) >>)
			when 4 then
				Result := apf.aprintf (
					fmt, << ba.item (1), ba.item (2), ba.item (3), ba.item (4) >>)
			end -- no else case
		ensure
			valid_decorated_size: df implies Result.count = 2 + (ba.count * 2)
			valid_plain_size: (not df) implies Result.count = (ba.count * 2)
		end

	rgb_hex_string_to_color (v: STRING): AEL_V2_COLOR
			-- Color described by 24-bit RGB hex number 'v'
		require
			valid: is_valid_rgb_hex (v)
		local
			rv, gv, bv: INTEGER
		do
			rv := assr.hex_substring_to_integer (v, 1,2)
			gv := assr.hex_substring_to_integer (v, 3,4)
			bv := assr.hex_substring_to_integer (v, 5,6)
			create Result.make_with_8_bit_rgb (rv, gv, bv)
		end

	--|--------------------------------------------------------------

	rgb_hex_strings_to_colors (cl: LIST [STRING]): ARRAYED_LIST [AEL_V2_COLOR]
			-- Colors represented by hexadecimal rgb values in 'cl'
		require
			valid: is_valid_rgb_hex_list (cl)
		local
			tc: AEL_V2_COLOR
			ts: STRING
		do
			create Result.make (cl.count)
			from cl.start
			until cl.after
			loop
				ts := cl.item
				if not is_valid_rgb_hex (ts) then
					-- Don't add, caught by precondition
				else
					tc := rgb_hex_string_to_color (ts)
					Result.extend (tc)
					cl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	grbw_hex_string_to_color (v: STRING): AEL_V2_COLOR
			-- Color described by 32-bit GRBW hex number 'v'
		require
			valid: is_valid_grbw_hex (v)
		local
			rv, gv, bv, wv: INTEGER
			ac: AEL_V2_COLOR
		do
			gv := assr.hex_substring_to_integer (v, 1,2)
			rv := assr.hex_substring_to_integer (v, 3,4)
			bv := assr.hex_substring_to_integer (v, 5,6)
			wv := assr.hex_substring_to_integer (v, 7,8)
			create ac.make_with_grbw (gv, rv, bv, wv)
--			Result := ac.to_ev_color
			Result := ac
		end

	--|--------------------------------------------------------------

	grbw_hex_strings_to_colors (cl: LIST [STRING]): ARRAYED_LIST [AEL_V2_COLOR]
			-- Colors represented by hexadecimal grbw values in 'cl'
		require
			valid: is_valid_grbw_hex_list (cl)
		local
			tc: AEL_V2_COLOR
			ts: STRING
		do
			create Result.make (cl.count)
			from cl.start
			until cl.after
			loop
				ts := cl.item
				if not is_valid_grbw_hex (ts) then
					-- Don't add, caught by precondition
				else
					tc := grbw_hex_string_to_color (ts)
					Result.extend (tc)
					cl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	hex_pf_format (nv: INTEGER; df, ucf: BOOLEAN): STRING
			-- Format string for aprintf to use representing 'nv' values 
			-- (each 0-255) in hexadecimal format
			-- If 'df' then include "0x" decoration, else don't
			-- If 'ucf' then show a-f as A-F
		local
			pds: STRING
			i: INTEGER
		do
			if ucf then
				pds := "%%02X"
			else
				pds := "%%02x"
			end
			if df then
				create Result.make (2 + (5 * nv))
				Result.append ("0x")
			else
				create Result.make (5 * nv)
			end
			from i := 1
			until i > nv
			loop
				Result.append (pds)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	color_of_pixel (pm: EV_PIXMAP; xp, yp: INTEGER): AEL_V2_COLOR
			-- Color of pixel, in pixmap 'pm' at position (xp,yp) 
			-- relative to top-left corner (0,0)
		require
			has_area: pm.height > 0 and pm.width > 0
			xy_in_area: xp >= 0 and xp < pm.width and yp >= 0 and yp < pm.height
		local
			pb: EV_PIXEL_BUFFER
			pi: like {EV_PIXEL_BUFFER}.pixel_iterator
			px: EV_PIXEL_BUFFER_PIXEL
			nx, ny: NATURAL_32
			r, g, b: INTEGER
		do
			create pb.make_with_pixmap (pm)
			pb.lock
			pi := pb.pixel_iterator
			nx := xp.as_natural_32 + 1
			ny := yp.as_natural_32 + 1
			pi.set_column (nx)
			pi.set_row (ny)
			px := pi.item
			r := px.red.to_integer_32
			g := px.green.to_integer_32
			b := px.blue.to_integer_32
			create Result.make_with_8_bit_rgb (r, g, b)
			pb.unlock
		end

	--|--------------------------------------------------------------

	colors_are_same (c1, c2: EV_COLOR): BOOLEAN
			-- Are 'c1' and 'c2' the same color?
		do
			Result := c1.rgb_24_bit = c2.rgb_24_bit
		end

--|========================================================================
feature {NONE} -- Services
--|========================================================================

	assr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_V2_COLORS
