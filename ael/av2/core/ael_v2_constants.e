note
	description: "Constants common to AEL_V2 classes"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
This class is inherited by AEL_V2_WIDGET and is therefore available to
all descendents of that class.
This class can also be instantiated by itself if desired.
}"

class AEL_V2_CONSTANTS

--|========================================================================
feature -- Colors
--|========================================================================

	colors: AEL_V2_COLORS
		once
			create Result
		end

--|========================================================================
feature -- Pixmaps
--|========================================================================

	pixmaps: AEL_V2_PIXMAPS
		once
			create Result
		end

--|========================================================================
feature -- Fonts
--|========================================================================

	fonts: AEL_V2_FONTS
		once
			create Result
		end

	font_const: EV_FONT_CONSTANTS
		once
			create Result
		end

--|========================================================================
feature -- Constants
--|========================================================================

	key_constants: EV_KEY_CONSTANTS
		once
			create Result
		end

	cursors: AEL_V2_CURSORS
		once
			create Result
		end

	string_constants: AEL_V2_INTERFACE_STRINGS
		do
			create Result
		end

	position_constants: AEL_V2_POSITION_CONSTANTS
		once
			create Result
		end

	--|--------------------------------------------------------------
	--| Convenient values (real constants default to 64

	K_real_32_0: REAL_32 = 0.0
	K_real_32_255: REAL_32 = 255.0
	K_real_32_256: REAL_32 = 256.0

end -- class AEL_V2_CONSTANTS
