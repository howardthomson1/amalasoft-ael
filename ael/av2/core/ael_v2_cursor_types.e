note
	description: "{
Cursor (pointer stype) types and related constants and support routines
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2014 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CURSOR_TYPES

create
	default_create

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_shared_fill_cursor_type (v: INTEGER)
			-- Set the shared type of fill cursor
		require
			valid: is_valid_fill_cursor_type (v)
		do
			shared_fill_cursor_type_cell.put (v)
		end

	--|--------------------------------------------------------------

	set_shared_fill_cursor_type_by_tag (v: STRING)
			-- Set the shared type of capture cursor, to that identified by 'v'
		local
			ts: STRING
		do
			ts := v.as_lower
			if ts ~ Ks_fill_cursor_type_rbucket then
				set_shared_fill_cursor_type (K_fill_cursor_type_rbucket)
			elseif ts ~ Ks_fill_cursor_type_lbucket then
				set_shared_fill_cursor_type (K_fill_cursor_type_lbucket)
			elseif ts ~ Ks_fill_cursor_type_circle then
				set_shared_fill_cursor_type (K_fill_cursor_type_circle)
			else
				set_shared_fill_cursor_type (K_fill_cursor_type_default)
			end
		end

	--|--------------------------------------------------------------

	set_shared_capture_cursor_type (v: INTEGER)
			-- Set the shared type of capture cursor
		require
			valid: is_valid_capture_cursor_type (v)
		do
			shared_capture_cursor_type_cell.put (v)
		end

	--|--------------------------------------------------------------

	set_shared_capture_cursor_type_by_tag (v: STRING)
			-- Set the shared type of capture cursor, to that identified by 'v'
		local
			ts: STRING
		do
			ts := v.as_lower
			if ts ~ Ks_capture_cursor_type_dropper then
				set_shared_capture_cursor_type (K_capture_cursor_type_dropper)
			elseif ts ~ Ks_capture_cursor_type_tweezer then
				set_shared_capture_cursor_type (K_capture_cursor_type_tweezer)
			elseif ts ~ Ks_capture_cursor_type_target then
				set_shared_capture_cursor_type (K_capture_cursor_type_target)
			elseif ts ~ Ks_capture_cursor_type_bnet then
				set_shared_capture_cursor_type (K_capture_cursor_type_bnet)
			else
				set_shared_capture_cursor_type (K_capture_cursor_type_default)
			end
		end

--|========================================================================
feature -- Cursor pixel buffers
--|========================================================================

	cursor_bfly_net_pbuf: CURSOR_BFLY_NET_PBUF
		once
			create Result.make
		end

	eyedropper_pbuf: EYEDROPPER_CURSOR_PBUF
		once
			create Result.make
		end

	tweezer_left_pbuf: TWEEZER_LEFT_PBUF
		once
			create Result.make
		end

	tweezer_right_pbuf: TWEEZER_RIGHT_PBUF
		once
			create Result.make
		end

	target_cursor_pbuf: TARGET_CURSOR_PBUF
		once
			create Result.make
		end

	--|--------------------------------------------------------------

	capture_btn_bn_pbuf: CAPTURE_BTN_BN_PBUF
			-- Butterfly net capture btn image
		once
			create Result.make
		end

	capture_btn_ed_pbuf: CAPTURE_BTN_ED_PBUF
			-- Eyedropper capture btn image
		once
			create Result.make
		end

	capture_btn_tw_left_pbuf: CAPTURE_BTN_TW_LEFT_PBUF
			-- Tweezers capture btn image
		once
			create Result.make
		end

	capture_btn_tw_right_pbuf: CAPTURE_BTN_TW_RIGHT_PBUF
			-- Tweezers capture btn image
		once
			create Result.make
		end

	capture_btn_target_pbuf: TARGET_BTN_PBUF
			-- Target capture btn image
		once
			create Result.make
		end

	--|--------------------------------------------------------------

	capture_btn_bn_ins_pbuf: CAPTURE_BTN_BN_INS_PBUF
			-- Butterfly net capture btn image
		once
			create Result.make
		end

	capture_btn_ed_ins_pbuf: CAPTURE_BTN_ED_INS_PBUF
			-- Eyedropper capture btn image
		once
			create Result.make
		end

	capture_btn_tw_left_ins_pbuf: CAPTURE_BTN_TW_LEFT_INS_PBUF
			-- Tweezers capture btn image
		once
			create Result.make
		end

	capture_btn_tw_right_ins_pbuf: CAPTURE_BTN_TW_RIGHT_INS_PBUF
			-- Tweezers capture btn image
		once
			create Result.make
		end

	capture_btn_target_ins_pbuf: TARGET_BTN_INS_PBUF
			-- Target capture btn image
		once
			create Result.make
		end

--|========================================================================
feature -- Cursor types
--|========================================================================

	Ks_fill_cursor_type_rbucket: STRING = "rbucket"
	Ks_fill_cursor_type_lbucket: STRING = "lbucket"
	Ks_fill_cursor_type_circle: STRING = "circle"

	K_fill_cursor_type_rbucket: INTEGER = 1
	K_fill_cursor_type_lbucket: INTEGER = 2
	K_fill_cursor_type_circle: INTEGER = 3

	K_fill_cursor_type_default: INTEGER
		once
			Result := K_fill_cursor_type_rbucket
		end

			-------------------------

	Ks_capture_cursor_type_bnet: STRING = "bnet"
	Ks_capture_cursor_type_dropper: STRING = "dropper"
	Ks_capture_cursor_type_tweezer: STRING = "tweezer"
	Ks_capture_cursor_type_target: STRING = "target"

	K_capture_cursor_type_bnet: INTEGER = 1
	K_capture_cursor_type_dropper: INTEGER = 2
	K_capture_cursor_type_tweezer: INTEGER = 3
	K_capture_cursor_type_target: INTEGER = 4

	K_capture_cursor_type_default: INTEGER
		once
			Result := K_capture_cursor_type_dropper
		end

--|========================================================================
feature -- Shared types (shared amongst descendents in same process)
--|========================================================================

	shared_fill_cursor_type: INTEGER
			-- Type of fill Cursor SHARED amongst all children
		do
			Result := shared_fill_cursor_type_cell.item
		end

	shared_fill_cursor_type_cell: CELL [INTEGER]
			-- Cell holding shared fill cursor type
		once
			create Result.put (K_fill_cursor_type_default)
		end

	shared_capture_cursor_type: INTEGER
			-- Type of capture Cursor SHARED amongst all children
		do
			Result := shared_capture_cursor_type_cell.item
		end

	shared_capture_cursor_type_cell: CELL [INTEGER]
			-- Cell holding shared capture cursor type
		once
			create Result.put (K_capture_cursor_type_default)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_fill_cursor_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid fill cursor type?
		do
			inspect v
			when
				K_fill_cursor_type_circle,
				K_fill_cursor_type_lbucket,
				K_fill_cursor_type_rbucket
			 then
				 Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	is_valid_capture_cursor_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid capture cursor type?
		do
			inspect v
			when K_capture_cursor_type_dropper,
			K_capture_cursor_type_bnet,
			K_capture_cursor_type_tweezer,
			K_capture_cursor_type_target
			 then
				Result := True
			else
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Sep-2014
--|     Created original module (Eiffel 14.05)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_CURSOR_TYPES
