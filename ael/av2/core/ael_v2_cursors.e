note
	description: "Common screen pointer (aka cursor) constants and support routines"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft"
	date: "$Date: 2010/07/05 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is included in the AEL_V2_CONSTANTS class as a supplier.
    It is therefore available to classes that inherit AEL_V2_CONTANTS,
    included AEL_V2_WIDGET.
    This class can be instantiated by itself if desired.
}"

class AEL_V2_CURSORS

inherit
	AEL_V2_CURSOR_TYPES

--|========================================================================
feature -- Predefined pointer styles
--|========================================================================

	busy_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.busy_cursor)
		end

	crosshair_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.crosshair_cursor)
		end

	help_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.help_cursor)
		end

	header_sizewe_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.header_sizewe_cursor)
		end

	hyperlink_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.hyperlink_cursor)
		end

	ibeam_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.ibeam_cursor)
		end

	no_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.no_cursor)
		end

	sizeall_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.sizeall_cursor)
		end

	sizenesw_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.sizenesw_cursor)
		end

	sizens_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.sizens_cursor)
		end

	sizenwse_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.sizenwse_cursor)
		end

	sizewe_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.sizewe_cursor)
		end

	standard_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.standard_cursor)
		end

	uparrow_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.uparrow_cursor)
		end

	wait_cursor: EV_POINTER_STYLE
		once
			create Result.make_predefined (pointer_constants.wait_cursor)
		end

--|========================================================================
feature -- Capture cursors
--|========================================================================

	new_typed_capture_cursor (ct: INTEGER): EV_POINTER_STYLE
			-- Newly generated capture cursor of type 'ct'
		do
			inspect ct
			when K_capture_cursor_type_bnet then
				Result := new_bnet_cursor
			when K_capture_cursor_type_tweezer then
				Result := new_tweezer_left_cursor
			when K_capture_cursor_type_dropper then
				Result := new_eyedropper_cursor
			when K_capture_cursor_type_target then
				Result := new_target_cursor
			else
				-- Assume eyedropper
				Result := new_eyedropper_cursor
			end
		end

	--|--------------------------------------------------------------

	new_capture_cursor: EV_POINTER_STYLE
			-- Newly generated capture cursor of present SHARED type
		do
			Result := new_typed_capture_cursor (shared_capture_cursor_type)
		end

	new_eyedropper_cursor: EV_POINTER_STYLE
			-- Newly generated eyedropper cursor
		do
			create Result.make_with_pixel_buffer (eyedropper_pbuf, 0, 25)
		end

	new_tweezer_right_cursor: EV_POINTER_STYLE
			-- Newly generated tweezer cursor
		do
			create Result.make_with_pixel_buffer (tweezer_right_pbuf, 1, 28)
		end

	new_tweezer_left_cursor: EV_POINTER_STYLE
			-- Newly generated tweezer cursor
		do
			create Result.make_with_pixel_buffer (tweezer_left_pbuf, 28, 28)
		end

	new_target_cursor: EV_POINTER_STYLE
			-- Newly generated target cursor
		do
			create Result.make_with_pixel_buffer (target_cursor_pbuf, 12, 12)
		end

	new_bnet_cursor: EV_POINTER_STYLE
			-- Newly generated butterfly net cursor
		do
			create Result.make_with_pixel_buffer (cursor_bfly_net_pbuf, 6, 8)
		end

--|========================================================================
feature -- Constants
--|========================================================================

	pointer_constants: EV_POINTER_STYLE_CONSTANTS
		once
			create Result
		end

end -- class AEL_V2_CURSORS
