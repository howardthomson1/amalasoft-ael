note
	description: "{
Application environment common to UI elements in a system
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_ENV

inherit
	AEL_APPLICATION_ENV

--|========================================================================
feature {NONE} -- Shared application root
--|========================================================================

--RFO 	application_root_cell: CELL [detachable APPLICATION_ROOT]
--RFO 		once
--RFO 			create Result.put (Void)
--RFO 		end

--|========================================================================
feature {AEL_V2_ENV} -- Common core
--|========================================================================

--RFO 	application_root_type: APPLICATION_ROOT
--RFO 		require False
--RFO 		do
--RFO 			check False then end
--RFO 		ensure
--RFO 			False
--RFO 		end

--RFO 	application_root: like application_root_type
--RFO 			-- Root of application
--RFO 			-- N.B. MUST be called first from AEL_V2_APPLICATION
--RFO 		do
--RFO 			if attached {like application_root_type}application_root_cell.item as ar then
--RFO 				Result := ar
--RFO 			elseif attached {like application_root_type} Current  as ca then
--RFO 				Result := ca
--RFO 				application_root_cell.put (ca)
--RFO 			else
--RFO 				create Result
--RFO 				check
--RFO 					init_sequence_invalid: False
--RFO 				end
--RFO 			end
--RFO 		ensure
--RFO 			exists: Result /= Void
--RFO 		end

	--|--------------------------------------------------------------

	main_window: MAIN_WINDOW
		once
			check attached application_root.main_window as mw then
				Result := mw
			end
		ensure
			right_one: Result = application_root.main_window
		end

	--|--------------------------------------------------------------

	alert (v: STRING)
			-- Open a modal information dialog with message string 'v'
			--
			-- N.B. This is a blocking call
		local
			td: EV_INFORMATION_DIALOG
		do
			create td.make_with_text (v)
			td.show_modal_to_window (main_window)
		end

--|========================================================================
feature -- Support
--|========================================================================

	nearest_window: EV_WINDOW
			-- Next Window widget in the widget hierarchy from Current
			-- If Current is a window, then Current
 		local
			tc: AEL_V2_IMP_IFC
		do
			Result := main_window
			if attached {EV_WINDOW} Current as cwin then
				Result := cwin
			elseif attached {EV_WIDGET} Current as cw then
				-- Current is a widget, but not a window
				create tc
				tc.set_uncle (cw.parent)
				if attached tc.top_level_window as w then
					Result := w
				end
			end
		end

--|========================================================================
feature -- Error recording
--|========================================================================

--RFO 	last_error_message: STRING
--RFO 			-- Most recent error message
--RFO 		do
--RFO 			Result := application_root.last_error_message
--RFO 		end
--RFO
--RFO 	last_error_code: INTEGER
--RFO 			-- Most recent error code
--RFO 		do
--RFO 			Result := application_root.last_error_code
--RFO 		end
--RFO
--RFO 	last_error: detachable AEL_SPRT_ERROR_INSTANCE
--RFO 			-- Most recent error, if any
--RFO 		do
--RFO 			Result := application_root.last_error
--RFO 		end
--RFO
--RFO 	has_error: BOOLEAN
--RFO 		do
--RFO 			Result := application_root.has_error
--RFO 		end
--RFO
--RFO 	--|--------------------------------------------------------------
--RFO
--RFO 	set_error_message (msg: STRING)
--RFO 		do
--RFO 			application_root.set_error_message (msg)
--RFO 		end
--RFO
--RFO 	set_error (ec: INTEGER; msg: STRING)
--RFO 		do
--RFO 			application_root.set_error (ec, msg)
--RFO 		end
--RFO
--RFO 	clear_errors
--RFO 			-- Clear error codes and messages, if any
--RFO 		do
--RFO 			application_root.clear_errors
--RFO 		end
--RFO
--RFO 	copy_errors (other: AEL_SPRT_MULTI_FALLIBLE)
--RFO 			-- copy error codes and messages from other
--RFO 		do
--RFO 			application_root.copy_errors (other)
--RFO 		end

--|========================================================================
feature -- Shared constants
--|========================================================================

	aelv2_constants: AEL_V2_CONSTANTS
		once
			create Result
		end

	colors: AEL_V2_COLORS
		obsolete
			"Use 'ael_colors' instead;  'colors' will be disappearing in 2019"
		do
			Result := ael_colors
		end

	ael_colors: AEL_V2_COLORS
		once
			Result := aelv2_constants.colors
		end

	pixmaps: AEL_V2_PIXMAPS
		obsolete
			"Use 'ael_pixmaps' instead;  'pixmaps' will be disappearing in 2019"
		do
			Result := ael_pixmaps
		end

	ael_pixmaps: AEL_V2_PIXMAPS
		once
			Result := aelv2_constants.pixmaps
		end

	fonts: AEL_V2_FONTS
		obsolete
			"Use 'ael_fonts' instead;  'fonts' will be disappearing in 2019"
		do
			Result := ael_fonts
		end

	ael_fonts: AEL_V2_FONTS
		once
			Result := aelv2_constants.fonts
		end

	cursors: AEL_V2_CURSORS
		obsolete
			"Use 'ael_cursors' instead;  'cursors' will be disappearing in 2019"
		do
			Result := ael_cursors
		end

	ael_cursors: AEL_V2_CURSORS
		once
			Result := aelv2_constants.cursors
		end

	position_constants: AEL_V2_POSITION_CONSTANTS
		once
			Result := aelv2_constants.position_constants
		end

	string_constants: AEL_V2_INTERFACE_STRINGS
		once
			Result := aelv2_constants.string_constants
		end

	--|--------------------------------------------------------------

	font_constants: EV_FONT_CONSTANTS
		once
			Result := aelv2_constants.font_const
		end

	key_constants: EV_KEY_CONSTANTS
		once
			Result := aelv2_constants.key_constants
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by AEL_V2_WIDGET and therefor by its
--| descendents. Each system must supply its own APPLICATION_ENV class
--| to hold anything that is environment-specific
--|----------------------------------------------------------------------

end -- class AEL_V2_ENV
