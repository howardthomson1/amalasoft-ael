note
	description: "{
Representation of a typeface.
Appearance is specified in terms of font family, height, shape and
weight. The local system font closest to the specification will be
displayed. Specific font names may optionally be specified
See `set_preferred_family'
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "$Date: 2019/06/24 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_FONT

inherit
	EV_FONT
		redefine
			make_with_values
		end

create
	make_with_values

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_values (a_family, a_weight, a_shape, a_height: INTEGER)
			-- Create with `a_family', `a_weight', `a_shape' and 
			-- `a_height'.
		do
			Precursor (a_family, a_weight, a_shape, a_height)
			if attached default_family_name (a_family) as fnm then
				set_preferred_family (fnm)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_preferred_family (v: STRING_32)
			-- Set, as preferred family name, 'v'
		require
			valid: attached v as tv and then not tv.is_empty
		local
			pfl: like preferred_families
			found: BOOLEAN
		do
			pfl := preferred_families
			if pfl.is_empty then
				pfl.extend (v)
			elseif pfl.first ~ v then
				-- Already most preferred
			else
				from pfl.start
				until pfl.after or found
				loop
					if pfl.item ~ v then
						pfl.remove
						found := True
					else
						pfl.forth
					end
				end
				pfl.put_front (v)
			end
		end

--|========================================================================
feature -- Font family names
--|========================================================================

	family_name_sans: STRING = "Arial"
	family_name_typewriter: STRING = "Courier New"
	family_name_roman: STRING = "Times New Roman"
	family_name_screen: STRING = "Lucida Sans"

	--|--------------------------------------------------------------

	default_family_name (ff: INTEGER): detachable STRING_32
			-- Default family name, if any, for font family 'ff'
		do
			inspect ff
			when family_screen then
				Result := family_name_screen
			when family_roman then
				Result := family_name_roman
			when family_sans then
				Result := family_name_sans
			when family_typewriter then
				Result := family_name_typewriter
			else
				-- family_modern or unknown
			end
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 24-June-2019
--|     Created original module (for Eiffel 18.07)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is a minor extension to EV_FONT, and should be used as
--| a drop-in replacement for same when EV_FONT does not seem to take
--| the family definition correctly.  The creation routine has been
--| redefined here to add a default preferred font family
--|----------------------------------------------------------------------

end -- class AEL_V2_FONT
