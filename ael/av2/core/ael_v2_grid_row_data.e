note
	description: "{
Structured data for use in data attribute of EV_GRID_ROWs
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_GRID_ROW_DATA

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (lvl: INTEGER; d: like value)
		do
			level := lvl
			value := d
		ensure
			level_set: level = lvl
			value_set: d ~ value
		end

--|========================================================================
feature -- Status
--|========================================================================

	level: INTEGER
			-- Nesting level of row

	value: detachable ANY
			-- Actual data associated with Current, if any

--|========================================================================
feature -- Status setting
--|========================================================================

	set_level (v: INTEGER)
		require
			valid: v > 0
		do
			level := v
		ensure
			assigned: level = v
		end

	set_value (v: like value)
		do
			value := v
		ensure
			assigned: value ~ v
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_level (v: INTEGER): BOOLEAN
			-- Is 'v' a valid level?
		do
			Result := v > 0
		end

	--|--------------------------------------------------------------
invariant
	valid_level: is_valid_level (level)

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with a valid level and optional data value
--| Assign to data feature of EV_GRID_ROWs
--|----------------------------------------------------------------------

end -- class AEL_V2_GRID_ROW_DATA
