class AEL_V2_IMP_IFC
-- Bridge class to help access implementation features

inherit
	EV_ANY
		redefine
			implementation
		end

create
	default_create

--|========================================================================
feature {EV_ANY} -- Implementation
--|========================================================================

	create_implementation
		do
			create implementation.make
		end

	create_interface_objects
		do
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	implementation: AEL_V2_IMP_IFC_IMP

	--|--------------------------------------------------------------

	set_uncle (v: like uncle)
		do
			uncle := v
		end

	uncle: detachable EV_CONTAINER
			-- Related to parent

	top_level_window: detachable EV_WINDOW
		do
			if attached uncle as tu then
				if attached {EV_CONTAINER_IMP} tu.implementation as x then
					Result := implementation.widget_top_level_window (x)
				end
			end
		end

end -- class AEL_V2_IMP_IFC
