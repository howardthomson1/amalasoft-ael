class AEL_V2_IMP_IFC_IMP
-- Bridge class to help access implementation features

inherit
	EV_ANY_I
		export
			{AEL_V2_IMP_IFC} all
			{ANY} deep_twin, is_deep_equal, standard_is_equal
		end

create
	make

--|========================================================================
feature -- Creation
--|========================================================================

	old_make (ifc: attached like interface)
		do
			interface := ifc
			make
		end

	make
		do
			set_is_initialized (True)
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	destroy
			-- Render `Current' unusable.
			-- No externals to deallocate, just set the flags.
		do
			set_is_destroyed (True)
		end

	widget_top_level_window (w: EV_WIDGET_IMP): detachable EV_WINDOW
		do
			Result := w.top_level_window
		end

end -- class AEL_V2_IMP_IFC_IMP
