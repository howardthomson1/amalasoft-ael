class AEL_V2_INTERFACE_STRINGS
-- Strings for use in widgets

 --|========================================================================
feature -- Widget label strings
 --|========================================================================

	Button_ok_item: STRING
			-- String for "OK" buttons.
		do
			Result := (create {EV_DIALOG_CONSTANTS}).ev_ok
		end

	Menu_file_item: STRING
			-- String for menu "File"
		do
			Result := "&File"
		end

	Menu_file_new_item: STRING
			-- String for menu "File/New"
		do
			Result := "&New%TCtrl+N"
		end

	Menu_file_open_item: STRING
			-- String for menu "File/Open"
		do
			Result := "&Open...%TCtrl+O"
		end

	Menu_file_save_item: STRING
			-- String for menu "File/Save"
		do
			Result := "&Save%TCtrl+S"
		end

	Menu_file_saveas_item: STRING
			-- String for menu "File/Save As"
		do
			Result := "Save &As..."
		end

	Menu_file_close_item: STRING
			-- String for menu "File/Close"
		do
			Result := "&Close"
		end

	Menu_file_exit_item: STRING
			-- String for menu "File/Exit"
		do
			Result := "E&xit"
		end

	Menu_print_item: STRING
			-- String for Print menus and buttons
		do
			Result := "&Print"
		end

	Menu_edit_item: STRING
			-- String for Edit menus and buttons
		do
			Result := "&Edit"
		end

	Menu_view_item: STRING
			-- String for View menus
		do
			Result := "&View"
		end

	Menu_options_item: STRING
			-- String for Options menus
		do
			Result := "&Options"
		end

	Menu_help_item: STRING
			-- String for menu "Help"
		do
			Result := "&Help"
		end

	Menu_help_contents_item: STRING
			-- String for menu "Help/Contents and Index"
		do
			Result := "&Contents and Index"
		end

	Menu_help_about_item: STRING
			-- String for menu "Help/About"
		do
			Result := "&About..."
		end

	Label_confirm_close_window: STRING
			-- String for the confirmation dialog box that appears
			-- when the user try to close the first window.
		do
			Result := "You are about to close this window.%NClick OK to proceed."
		end

	Icon_label_string: STRING
			-- String associated with the icon representing Current on a 
			-- desktop
		do
			Result := Application_name
		end

	Main_window_title: STRING
			-- String that appears in the main title bar
		do
			Result := Application_name
		end

	Help_about_title: STRING
		do
			Result := "About " + Application_name
		end

	Help_about_message: STRING
		do
			Result := Application_name + "%N" + Version_string +
				"%N%NCopyright (C) " + Copyright_date_string + " " + Vendor_string
		end

	--|--------------------------------------------------------------

	Application_name: STRING
			-- Name of application
			-- To reset, call set_application_name before any other 
			-- accesses
		once
			Result := "Application"
		end

	Version_string: STRING
			-- Application version string
			-- To reset, call set_version_string before any other 
			-- accesses
		once
			Result := "Version 1.0"
		end

	Vendor_string: STRING
			-- Application vendor string
			-- To reset, call set_vendor_string before any other 
			-- accesses
		once
			Result := "Amalasoft Corporation"
		end

	Copyright_date_string: STRING
			-- Copyright date (typically year) string
			-- To reset, call set_copyright_date_string before any other 
			-- accesses
		local
			now: DATE_VALUE
		once
			create now
			create Result.make_from_string (now.year.out)
		end

	--|--------------------------------------------------------------

	Tooltip_tb_save: STRING
			-- Tooltip text for a toolbar Save button
		do
			Result := "Save file to disk"
		end

	Tooltip_tb_new: STRING
			-- Tooltip text for a toolbar New button
		do
			Result := "Create a new file"
		end

	Tooltip_tb_open: STRING
			-- Tooltip text for a toolbar Open button
		do
			Result := "Open an existing file"
		end

	Tooltip_tb_delete: STRING
			-- Tooltip text for a toolbar Delete button
		do
			Result := "Delete the selected item or items"
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_application_name (v: STRING)
		do
			Application_name.wipe_out
			Application_name.append (v)
		end

	set_version_string (v: STRING)
		do
			Version_string.wipe_out
			Version_string.append (v)
		end

	set_vendor_string (v: STRING)
		do
			Vendor_string.wipe_out
			Vendor_string.append (v)
		end

	set_copyright_date_string (v: STRING)
			-- Set, for all classes that use the Copyright_date_string 
			-- feature, including the Help_about_message feature, the date 
			-- string to be 'v'
		do
			Copyright_date_string.wipe_out
			Copyright_date_string.append (v)
		end

end -- class AEL_V2_INTERFACE_STRINGS
