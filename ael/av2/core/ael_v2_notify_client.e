note
	description: "A class representing a notify-able entity"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class shou;d be inherited by the APPLICATION class (typically) and
    by any other classes that require anonymous notification of events in
    the application.
    Most often, such changes include data updates and non-widget events,
    and notification lets the clients respond to the changes as needed.
    In the APPLICATION class, call the make_as_notifier routine.
    In client classes, call the make_as_notifier_client routine.
    In client classes, redefine teh repond_to_notification routine.
    You will also need to create a class to hold the constants that
    denote the nature of change.  This can in turn be inherited or
    instantiated by APPLICATION and the client classes so that they can
    share the common definitions (INTEGER constants).
}"

class AEL_V2_NOTIFY_CLIENT

create
	make_as_notifier_client, make_as_notifier

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_as_notifier_client
		do
			event_notifier.register (Current)
		end

	--|--------------------------------------------------------------

	make_as_notifier
		do
			-- Force creation of notifier
			if attached event_notifier then end
		end

--|========================================================================
feature -- Notification
--|========================================================================

	catch_notification (ids: ARRAY [INTEGER])
			-- React as needed to the given event IDs
		require
			ids_exist: ids /= Void
		local
			i, lim: INTEGER
		do
			lim := ids.count
			from i := 1
			until i > lim
			loop
				respond_to_notification (ids.item (i))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	respond_to_notification (v: INTEGER)
			-- For the given ID, react as needed
			-- Redefine in descendent
		do
		end

	--|--------------------------------------------------------------

	unregister_for_notification
		do
			event_notifier.unregister (Current)
		end

--|========================================================================
feature {NONE}
--|========================================================================

	event_notifier: AEL_V2_NOTIFIER
		once ("PROCESS")
			create Result.make
		end

end -- class AEL_V2_NOTIFY_CLIENT
