note
	description: "Common pixmaps and facilities"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is a built-in supplier to AEL_V2_WIDGET and so is available
    to all descendents of that class.  This class can also be instantiated
    directly or inherited as desired.
    For the new_pixmap function to be of any value, an AEL_V2_ENVIRONMENT
    or descendent must be instantiated prior to any calls to new_pixmap.
    The default pixmap path in AEL_V2_ENVIRONMENT must be set first (it is
    a once), else the pixmap path will be hard-wired to the current directory
    unless an absolute path is provided.
}"

class AEL_V2_PIXMAPS

inherit
	AEL_V2_PATHS
	AEL_V2_CONSTANTS
	MATH_CONST

create
	default_create

--|========================================================================
feature -- Common pixmaps (generated programmatically)
--|========================================================================

	K_pm_application_icon: like new_pixmap
		do
			Result := create {AEL_V2_APPLICATION_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_save: like new_pixmap
		do
			Result := create {AEL_V2_SAVE_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_open: like new_pixmap
		do
			Result := create {AEL_V2_OPEN_PIXMAP}.make
		end

	--|--------------------------------------------------------------
  
	K_pm_file_new: like new_pixmap
		do
			Result := create {AEL_V2_NEW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_file_print: like new_pixmap
		do
			Result := create {AEL_V2_PRINT_PIXMAP}.make
		end

	--|---------------------------------------------------------------

	K_pm_search: like new_pixmap
		do
			Result := create {AEL_V2_SEARCH_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_help: like new_pixmap
		do
			Result := create {AEL_V2_TB_HELP_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_small_arrow_left: like new_pixmap
		do
			Result := create {AEL_V2_SMALL_LEFT_ARROW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_small_arrow_right: like new_pixmap
		do
			Result := create {AEL_V2_SMALL_RIGHT_ARROW_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_arrow_up (w, h: INTEGER): like new_pixmap
		do
			Result :=
				create {AEL_V2_ARROW_UP_PIXMAP}.make_with_scale (h)
		end

	--|--------------------------------------------------------------

	K_pm_arrow_down (w, h: INTEGER): like new_pixmap
		do
			Result :=
				create {AEL_V2_ARROW_DOWN_PIXMAP}.make_with_scale (h)
		end

	--|--------------------------------------------------------------

	K_pm_thumb_15x8: like new_pixmap
		do
			Result := create {AEL_V2_THUMB_15X8_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_locked_18x18: like new_pixmap
		do
			Result := create {AEL_V2_LOCKED_PIXMAP}.make
		end

	--|--------------------------------------------------------------

	K_pm_unlocked_18x18: like new_pixmap
		do
			Result := create {AEL_V2_UNLOCKED_PIXMAP}.make
		end

--|========================================================================
feature -- Pixmap path construction (for file-based pixmaps)
--|========================================================================

	--RFO obs_pixmap_path (pmn: STRING): FILE_NAME
	--RFO 	require
	--RFO 		name_exists: pmn /= Void
	--RFO 	do
	--RFO 		create Result.make_from_string (Ks_dflt_pixmap_dirname)
	--RFO 		Result.extend (pmn)
	--RFO 	ensure
	--RFO 		exists: Result /= Void
	--RFO 	end

	pixmap_path (pmn: STRING): PATH
		require
			name_exists: pmn /= Void
		do
			create Result.make_from_string (Ks_dflt_pixmap_dirname)
			Result := Result.extended (pmn)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_pixmap (fn: STRING): EV_PIXMAP
			-- Create a new pixmap from the given pathname
		require
			filename_exists: fn /= Void and then not fn.is_empty
		local
			f: RAW_FILE
			path: like pixmap_path
			ex: EXECUTION_ENVIRONMENT
		do
			create ex
			last_pixmap_create_successful := False
			if fn.substring_index(ex.root_directory_name, 1) /= 0 then
				-- Is an absolute path already
				create path.make_from_string (fn)
			else
				-- Is a file name only
				path := pixmap_path (fn)
			end
			--create f.make_with_name (path)
			create f.make_with_path (path)
			if f.exists then
				create Result
				Result.set_with_named_file (path.out)
				last_pixmap_create_successful := True
			else
				-- File not found, create a default pixmap
				create Result.make_with_size (16, 16)
				report_pixmap_file_error (
					"Pixmap file not found", {ARRAY [ANY]}<< Result >>)
			end
		ensure
			result_exists: Result /= Void
		end

--|========================================================================
feature -- Pixmap error reporting
--|========================================================================

	report_pixmap_file_error (msg: STRING; args: ARRAY [ANY])
		require
			message_exists: msg /= Void
			args_exist: args /= Void
		do
			if attached error_report_proc as erp then
				erp.call ([msg, args])
			end
		end

	--|--------------------------------------------------------------

	last_pixmap_create_successful: BOOLEAN

--|========================================================================
feature -- Error reporting callbacks
--|========================================================================

	set_error_report_procedure (v: like error_report_proc)
		do
			error_report_proc := v
		end

	error_report_proc: detachable PROCEDURE [STRING, ARRAY [ANY]]

end -- class AEL_V2_PIXMAPS
