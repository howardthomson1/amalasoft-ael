note
	description: "{
Generic widget compatible with the Eiffel Vision2 library
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_WIDGET

inherit
	AEL_V2_ANY
	EV_ANY
		rename
			create_interface_objects as default_create_interface_objects,
			implementation as implementation_attr
		undefine
			is_equal, is_in_default_state, initialize, destroy
		end
	APPLICATION_ENV
		undefine
			default_create, copy, is_equal
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
			-- Create current with default values
		do
			default_create
			complete_initialization
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence
			-- for Current
			-- Redefine in child but make certain to call Precursor
			-- within the redefined feature AFTER all other instructions
		do
			av2_create_interface_objects
		ensure then
			created: components_are_created
		end

	--|--------------------------------------------------------------

	frozen av2_create_interface_objects
			-- Establishes an immutable sequence of creation and
			-- provides descdendents with a better named routine
			-- as 'create_interface_objects'
		do
			create ael_voidsafe_dummy.put (True)
			create_interface_objects
		ensure then
			created: components_are_created
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create interface objects as part of the creation sequence
			-- for Current
		deferred
		end

--|========================================================================
feature {EV_ANY} -- Initialization (after default_create)
--|========================================================================

	frozen complete_initialization
			-- To be called by the client AFTER default_create because
			-- postcondition of default_create is 'is_in_default_state'
			--
			-- Complete initialization of components
			--
			-- Redefine in child but call Precursor as first
			-- instruction in redefined feature, BEFORE any other
			-- instructions
		require
			components_created: components_are_created
			not_already_called: not initialization_sequence_is_complete
		do
			initialize_interface_values
			build_interface_components
			initialize_interface_actions
			initialize_interface_strings
			initialize_interface_fonts
			initialize_interface_colors
			initialize_interface_images
			post_initialize
			initialization_sequence_is_complete := True
			update_widget_rendition
		end

--|========================================================================
feature {NONE} -- Interface initialization
--|========================================================================

	initialize_interface_values
			-- Define values needed after component creation and BEFORE
			-- component assembly or initialization
		do
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface
			-- components
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_fonts
			-- Initialize font types and sizes for interface components
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_strings
			-- Initialize strings for interface components
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_colors
			-- Initialize colors for interface components
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_images
			-- Initialize pixmaps and other images for interface components
		do
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed
			-- after the other operations defined from
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
		end

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	components_are_created: BOOLEAN
			-- Have components been created?
			-- Creation occurs in ev_create_interface_objects,
			-- itself called from default_create
		do
			Result := attached ael_voidsafe_dummy
		end

	initialization_sequence_is_complete: BOOLEAN
			-- Have all steps in the init sequence been completed?

--|========================================================================
feature -- Interface Components
--|========================================================================

	ael_voidsafe_dummy: detachable CELL [BOOLEAN]
			-- Dummy component used to verify that creation sequence has
			-- transpired
			-- A reference object is used rather than a BOOLEAN to work
			-- better with Void safety

	new_application_pixmap: EV_PIXMAP
			-- Pixmap for use in title bar and in minimal icon
		do
			create {AEL_V2_APPLICATION_PIXMAP}Result.make
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: EV_ANY_I
			-- Environment-specific implementation for Current
			-- Redefine in child to be of appropriate type and to refer
			-- to the actual implementation attribute acquired from the
			-- EV_* component
		do
			Result := implementation_attr
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is the ancestor of custom widgets.  It provides a standard
--| construction and intitialization sequence and makes available to its
--| descendents the common classes for pixmaps, colors and fonts.
--| Descendents of this class should be created using default_create or
--| a custom make routine defined within the child.
--| The creation/initialization sequence is as follows:
--|   default_create (frozen; from EV_ANY)
--|     create_interface_objects (redefine here and in decendent)
--|     create_implementation (from EV_ANY)
--|     ...
--|     initialize (from EV_ANY)
--|     ...
--|   complete_initialization (from descendent, after 'initialize')
--|     initialize_interface_values (redefinable, in this class)
--|     build_interface_components (redefinable, in this class)
--|     initialize_interface_actions (redefinable, in this class)
--|     initialize_interface_fonts (redefinable, in this class)
--|     initialize_interface_strings (redefinable, in this class)
--|     initialize_interface_colors (redefinable, in this class)
--|     initialize_interface_images (redefinable, in this class)
--|
--| Complex widgets (with subcomponents) add their
--| subordinate widgets in a redefined make_components routine.
--| To enforce the standard construction sequence, there are pre and
--| postconditions defined for the redefinable routines.  Be sure to
--| include Precursor calls in your redefined routines.
--|----------------------------------------------------------------------

end -- class AEL_V2_WIDGET
