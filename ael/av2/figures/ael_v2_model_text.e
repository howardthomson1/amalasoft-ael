note
	description: "EV_MODEL_TEXT with built-in size calculations"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_MODEL_TEXT

inherit
	EV_MODEL_TEXT
		redefine
			assign_draw_id
		end

create
	default_create, make_with_text

--|========================================================================
feature -- Sizing
--|========================================================================

	overall_width: INTEGER
			-- Current overall width of this figure; driven by font and text
			-- On some fonts, characters may extend outside of the bounds 
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
		do
			if attached text as tx and then not tx.is_empty then
				ss := scaled_font.string_size (tx)
				Result := ss.integer_item (1) - ss.integer_item (3) +
					ss.integer_item (4)
			end
		end

	--|------------------------------------------------------------------------

	overall_height: INTEGER
			-- Current overall height of this figure driven by font and text
			-- On some fonts, characters may extend outside of the bounds 
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
		do
			if attached text as tx and then not tx.is_empty then
				ss := scaled_font.string_size (tx)
				Result := ss.integer_item (2)
			end
		end

	--|------------------------------------------------------------------------

	overall_size: ARRAY [INTEGER]
			-- Current overall width and height of this figure.
			-- On some fonts, characters may extend outside of the bounds 
			-- given by `width' and `height', for example certain italic
			-- letters may overhang other letters.
		local
			ss: TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
			tw, sz: INTEGER
		do
			if attached text as tx and then not tx.is_empty then
				ss := scaled_font.string_size (tx)
				tw := ss.integer_item (1) - ss.integer_item (3) +
					ss.integer_item (4)
				sz := ss.integer_item (2)
			end
			Result := << tw, sz >>
		ensure
			exists: Result /= Void
			has_two_items: Result.count = 2
		end

	--|--------------------------------------------------------------

	assign_draw_id
			-- Set the drawing ID for this figure to be the same as for
			-- normal text figures
		do
			known_draw_ids.search ("EV_MODEL_TEXT")
			draw_id := known_draw_ids.found_item
			if draw_id = 0 then
				draw_id := draw_id_counter.item
				draw_id_counter.put (draw_id + 1)
				known_draw_ids.put (draw_id, "EV_MODEL_TEXT")
			end
		end
	 
--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated in the same way as EV_MODEL_TEXT and is
--| plug-compatible with that class.
--| The overall_* functions move the size calcuations and adjustments
--| here instead of in the client.
--|----------------------------------------------------------------------

end -- class AEL_V2_MODEL_TEXT
