deferred class AEL_V2_GRAPH

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			create_interface_objects, 	build_interface_components,
			initialize_interface_actions, post_initialize
		end

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	create_interface_objects
		do
			Precursor
			create chart
			create figure_world
			create projector.make_with_buffer (figure_world, chart)
			create data_source.make
		end

	build_interface_components
		do
			init_data_source
			extend (chart)
			build_item_lists
		end

	initialize_interface_actions
		do
			Precursor
			resize_actions.extend (agent on_resize)
			figure_world.pointer_button_release_actions.extend (
				agent on_mouse_btn_release)

		end

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			set_minimum_size (K_min_width, K_min_height)
		end

	--|--------------------------------------------------------------

	init_data_source
		deferred
		ensure
			has_data: data_source /= Void
		end

	--|--------------------------------------------------------------

	build_item_lists
		local
			i, lim: INTEGER
			tl: like items
		do
			lim := data_source.count
			create tl.make (lim)
			items := tl
			from i := 1
			until i > lim
			loop
				tl.extend (create {LINKED_LIST [AEL_V2_GRAPH_ITEM]}.make)
				i := i + 1
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	chart: EV_DRAWING_AREA

--	projector:  EV_MODEL_DRAWING_AREA_PROJECTOR
-- Eliminates flicker
	projector:  EV_MODEL_BUFFER_PROJECTOR
	figure_world: EV_MODEL_WORLD

	items: detachable ARRAYED_LIST [LINKED_LIST [AEL_V2_GRAPH_ITEM]]
		note
			option: stable
			attribute
		end

--|========================================================================
feature -- Data
--|========================================================================

	series (v: INTEGER): ARRAY [REAL]
		require
			valid_series: v > 0 and v <= data_source.count
		do
			Result := data_source.i_th (v)
		end

	data_source: LINKED_LIST [like series]

	--|--------------------------------------------------------------

--RFO 	series_labels (v: INTEGER): ARRAY [STRING]
--RFO 		require
--RFO 			has_labels: data_labels /= Void
--RFO 			valid_series: v >= data_labels.lower and v >= data_labels.upper
--RFO 		do
--RFO 			Result := data_labels.item (v)
--RFO 		end
--RFO 
--RFO 	data_labels: ARRAY [like series_labels]

	--|--------------------------------------------------------------

	data_item_count: INTEGER

	smallest_value: REAL
	largest_value: REAL

	maximum_series_count: INTEGER
		do
			-- Number of colors in palette
			Result := item_colors.count
		end

--|========================================================================
feature -- Status
--|========================================================================

	left_margin: INTEGER
		do
			Result := private_left_margin
			if Result = 0 then
				Result := K_dflt_left_margin
			end
		end

	right_margin: INTEGER
		do
			Result := private_right_margin
			if Result = 0 then
				Result := K_dflt_right_margin
			end
		end

	top_margin: INTEGER
		do
			Result := private_top_margin
			if Result = 0 then
				Result := K_dflt_top_margin
			end
		end

	bottom_margin: INTEGER
		do
			Result := private_bottom_margin
			if Result = 0 then
				Result := K_dflt_bottom_margin
			end
		end

	--|--------------------------------------------------------------

	item_colors: like default_colors
		do
			if attached private_item_colors as lpc then
				Result := lpc
			else
				Result := default_colors
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_data_source (v: like data_source)
		require
			exists: v /= Void
		do
			data_source.wipe_out
			data_source.fill (v)
		end

	--|--------------------------------------------------------------

	set_series_data (v: like series; si: INTEGER)
			-- For the given data series 'si', set the data to 'v'
		require
			exists: v /= Void
			valid_series: si > 0 and si <= data_source.count
		do
			data_source.go_i_th (si)
			data_source.replace (v)
		ensure
			is_set: data_source.i_th (si) = v
		end

	--|--------------------------------------------------------------

	add_series (v: like series)
			-- Add the given data series to data source
		require
			exists: v /= Void
		do
			data_source.extend (v)
		ensure
			added: data_source.last = v
		end

	--|--------------------------------------------------------------

	set_item_color_db (v: like default_colors)
		do
			private_item_colors := v
		end

	set_item_colors (si: INTEGER; ca: ARRAY [EV_COLOR])
			-- Set colors for the item at positions 'si'
		require
			colors_exist: ca /= Void
			valid_count: ca.count = 3
		do
			if not attached private_item_colors as lpc then
				private_item_colors := default_colors.deep_twin
			end
			if attached private_item_colors as lpc then
				lpc.put (ca, si)
			end
		end

	--|--------------------------------------------------------------

	set_left_margin (v: INTEGER)
		do
			private_left_margin := v
		end

	set_right_margin (v: INTEGER)
		do
			private_right_margin := v
		end

	set_top_margin (v: INTEGER)
		do
			private_top_margin := v
		end

	set_bottom_margin (v: INTEGER)
		do
			private_bottom_margin := v
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	update_graph
		deferred
		end

--|========================================================================
feature {NONE} -- Rendering support
--|========================================================================

	purge
		do
			figure_world.wipe_out
			build_item_lists

			smallest_value := {INTEGER_64}.max_value.to_real
			largest_value := 0.0
		end

	--|--------------------------------------------------------------

	factor_series_for_scale (v: like series)
		local
			tc: INTEGER
		do
			tc := data_item_count
			data_item_count := 0
			v.do_all (agent factor_for_scale)
			data_item_count := tc.max (data_item_count)
		end

	--|--------------------------------------------------------------

	factor_for_scale (v: REAL)
		do
			data_item_count := data_item_count + 1
			smallest_value := smallest_value.min (v)
			largest_value := largest_value.max (v)
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER)
		do
			update_graph
		end

	--|--------------------------------------------------------------

	on_item_mouse_over (ti: AEL_V2_GRAPH_ITEM)
		do
		end

	--|--------------------------------------------------------------

	on_item_mouse_leave (ti: AEL_V2_GRAPH_ITEM)
		do
		end

	--|--------------------------------------------------------------

	on_item_mouse_btn_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
		end

	--|--------------------------------------------------------------

	on_mouse_btn_press (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
		end

	--|--------------------------------------------------------------

	on_mouse_btn_release (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
		end

	--|--------------------------------------------------------------

	on_2click (f: EV_MODEL; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
		end

--|========================================================================
feature {NONE} -- Geometry support
--|========================================================================

	update_geometry
			-- Recalculate sizes, offsets and positions for current size
		deferred
		end

	--|--------------------------------------------------------------

	plot_area_height: INTEGER
			-- Height of actual plot area (area less margins)
		do
			Result := (height - top_margin) - bottom_margin
		end

	plot_area_width: INTEGER
			-- Width of actual plot area (area less margins)
		do
			Result := (width - left_margin) - right_margin
		end

	private_left_margin: INTEGER
	private_right_margin: INTEGER
	private_top_margin: INTEGER
	private_bottom_margin: INTEGER

	private_item_colors: detachable like default_colors

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_min_height: INTEGER = 60
	K_min_width: INTEGER = 60

	--|--------------------------------------------------------------

	K_dflt_left_margin: INTEGER = 15

	K_dflt_right_margin: INTEGER = 10

	K_dflt_top_margin: INTEGER = 10

	K_dflt_bottom_margin: INTEGER = 15

	--|--------------------------------------------------------------

--RFO 	colors: AEL_V2_COLORS
--RFO 		once
--RFO 			create Result
--RFO 		end

	default_colors: ARRAY [ARRAY [EV_COLOR]]
			-- Default color sets (for items, shadows, etc)
		once
			Result := <<
			<< colors.dark_red, colors.light_brown, colors.red_brown >>,
			<< colors.navy, colors.royal_blue, colors.dark_navy >>,
			<< colors.orange, colors.gold, colors.red_brown >>,
			<< colors.green, colors.light_green, colors.dark_green >>,
			<< colors.blue, colors.royal_blue, colors.navy >>,
			<< colors.cyan, colors.light_cyan, colors.dark_gray >>,
			<< colors.magenta, colors.violet, colors.dark_magenta >>,
			<< colors.light_green, colors.pale_green, colors.dark_green >>,
			<< colors.yellow, colors.pale_yellow, colors.light_goldenrod >>,
			<< colors.dark_green, colors.green, colors.dark_gray >>,
			<< colors.light_blue, colors.pale_blue, colors.navy >>,
			<< colors.gold, colors.dark_yellow, colors.brown >>,
			<< colors.dark_gray, colors.gray, colors.black >>,
			<< colors.light_gray, colors.light_gray, colors.gray >>
			>>
		end

	ev_application: EV_APPLICATION
		local
			ev: EV_ENVIRONMENT
		once
			create ev
			check attached ev.application as lea then
				Result := lea
			end
		end

	--|--------------------------------------------------------------

invariant
	data_source_exists: data_source /= Void
	valid_data_count: data_source.count <= maximum_series_count

end -- class AEL_V2_GRAPH
