class AEL_V2_GRAPH_POINT_LABEL
-- Label at a point in a graph field

inherit
	EV_MODEL_GROUP
		redefine
			default_create, set_point_position, create_interface_objects
		end

create
	default_create, make_with_text

create {EV_MODEL_GROUP}
	make_filled, list_make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create
		do
			Precursor
			build_interface_components
		end

	--|--------------------------------------------------------------

	make_with_text (v: STRING)
		require
			exists: v /= Void
		do
			default_create
			original_text.append (v)
			text_fig.set_text (v)
		end

	--|--------------------------------------------------------------

	create_interface_objects
		do
			Precursor
			create original_text.make (0)
			create bg_fig
			create text_fig
		end

	build_interface_components
		do
			extend (bg_fig)
			bg_fig.set_background_color (colors.pale_yellow)
			extend (text_fig)
			update_geometry
		end

--|========================================================================
feature -- Status
--|========================================================================

	original_text: STRING

	bg_fig: EV_MODEL_ROUNDED_RECTANGLE
	text_fig: AEL_V2_MODEL_TEXT

	overall_width: INTEGER
		do
			Result := bg_fig.width
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_point_position (ax, ay: INTEGER)
		do
			Precursor (ax, ay)
			update_geometry
		end

	set_foreground_color (v: EV_COLOR)
		require
			exists: v /= Void
		do
			text_fig.set_foreground_color (v)
		end

	set_background_color (v: EV_COLOR)
		require
			exists: v /= Void
		do
			bg_fig.set_background_color (v)
		end

	set_text (v: STRING)
		do
			text_fig.set_text (v)
			update_geometry
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	update_geometry
		do
			bg_fig.set_radius (2)
			bg_fig.set_width (text_fig.overall_width + K_margins_width)
			bg_fig.set_height (text_fig.overall_height + K_margins_height)
			text_fig.set_point_position (
				point_x + K_left_margin, point_y + K_top_margin)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_top_margin: INTEGER = 2
	K_left_margin: INTEGER = 3

	K_margins_width: INTEGER
			-- Width of left and right margins, combined
		once
			Result := K_left_margin * 2
		end

	K_margins_height: INTEGER
			-- Height of top and bottom margins, combined
		once
			Result := K_top_margin * 2
		end

	colors: AEL_V2_COLORS
		once
			create Result
		end

end -- class AEL_V2_GRAPH_POINT_LABEL
