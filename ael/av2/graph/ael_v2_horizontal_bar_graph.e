class AEL_V2_HORIZONTAL_BAR_GRAPH

inherit
	AEL_V2_XY_GRAPH
		redefine
			update_geometry, on_button_3_noctl_press,
			on_button_3_ctl_press, maximum_series_count,
			right_margin, top_margin, initialize_orientation,
			value_from_x
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	init_data_source
		do
			add_series (<< 2.5, 1.0, 0.5, 1.2, 1.8, 2.1, 2.6, 3.8 >>)
		end

	--|--------------------------------------------------------------

	initialize_orientation
		do
			orientation := 'e'
		end

--|========================================================================
feature -- Data
--|========================================================================

	maximum_series_count: INTEGER
		do
			Result := 1
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	new_figure_at_position (si, i: INTEGER; pt: EV_COORDINATE): EV_MODEL
		local
			tfig: AEL_V2_MODEL_BAR
		do
			create tfig
			tfig.	set_graph_orientation ('l')
			tfig.set_bar_thickness (bar_thickness)
			tfig.set_position (pt.x, pt.y, x_axis_vertical_offset)
			tfig.set_bar_color (item_colors.item (i).item (1))
			tfig.set_end_shadow_color (item_colors.item (i).item (2))
			tfig.set_edge_shadow_color (item_colors.item (i).item (3))
			Result := tfig
		end

--|========================================================================
feature {NONE} -- Geometry support
--|========================================================================

	update_geometry
		do
			Precursor
	 			-- Bar thickness is proportional to chart size
			bar_thickness := (chart_value_interval / 2).rounded
		end

	--|--------------------------------------------------------------

	bar_thickness: INTEGER

	--|--------------------------------------------------------------

	right_margin: INTEGER
		do
			Result := Precursor + 10
		end

	--|--------------------------------------------------------------

	top_margin: INTEGER
		do
			Result := Precursor + 10
		end

	value_from_x (v: INTEGER): REAL
			-- Value at given X position
		do
			Result := (v - y_axis_horizontal_margin) / value_scale_factor
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_button_3_ctl_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp: INTEGER)
		do
		end

	on_button_3_noctl_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp: INTEGER)
		do
			show_vertical_hairline (xp, yp, True)
		end

end -- class AEL_V2_HORIZONTAL_BAR_GRAPH
