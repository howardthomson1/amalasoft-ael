class AEL_V2_MODEL_BAR
-- A bar graph bar figure
--
-- Bar position, value, width and depth:
--
--  Bottom orientation
--                |      _______
--                |     /      /|
--                |    /      / |
--                |   /      /  |
--  top left ------->/______/   |
--                | |       |<->|-------- depth  ^
--                | |       |   |                |
--                | |<----->|------------ width  |
--                | |       |   |                | Value
--                | |       |  /                 |
--                | |       | /                  |
--                |_|_______|/__________         v
--
--  Left orientation
--                |   ___________________
--                |  /           ^      /|
--                | /            |------------- depth
--  top left ---->|/_____________v____/  |
--                |              ^    |  |
--                |              |------------- width
--                |______________v____|/
--                |
--                |<------------------>
--                |     Value

inherit
	EV_MODEL_GROUP
		redefine
			default_create, create_interface_objects
		end

create
	default_create

create {EV_MODEL_GROUP}
	make_filled, list_make

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	default_create
		do
			Precursor {EV_MODEL_GROUP}

			bar_thickness := 10
			extend (bar)
		end

	--|--------------------------------------------------------------

	create_interface_objects
		do
			Precursor
			create bar
		end

--|========================================================================
feature -- Components
--|========================================================================

	bar: EV_MODEL_RECTANGLE

	shadow: detachable EV_MODEL_POLYGON
	end_shadow: like shadow
	edge_shadow: like shadow

	setting_bar_points: BOOLEAN

	bar_color: EV_COLOR
		do
			if attached private_bar_color as lc then
				Result := lc
			else
				Result := dflt_bar_color
			end
		end

	end_shadow_color: EV_COLOR
		do
			if attached private_end_shadow_color as lc then
				Result := lc
			else
				Result := dflt_shadow_color
			end
		end

	edge_shadow_color: EV_COLOR
		do
			if attached private_edge_shadow_color as lc then
				Result := lc
			else
				Result := dflt_shadow_color
			end
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_graph_orientation (v: CHARACTER)
			-- Single character, 'b', 'l', 't', 'r'
			-- Denoting: bottom, left, top and right, respectively
			-- Bottom means growing up from bottom
		do
			private_graph_orientation := v
		end

	graph_orientation: CHARACTER
		do
			Result := private_graph_orientation
			if (Result = '%U') then
				Result := Kc_dflt_graph_orientation
			end
		end

	--|--------------------------------------------------------------

	set_bar_thickness (v: INTEGER)
		do
			bar_thickness := v
		end

	set_line_width (v: INTEGER)
		do
			bar.set_line_width (v)
		end

	set_bar_color (v: EV_COLOR)
		do
			private_bar_color := v
			bar.set_background_color (v)
		end

	set_end_shadow_color (v: EV_COLOR)
		do
			private_end_shadow_color := v
			if attached end_shadow as ls then
				ls.set_background_color (v)
			end
		end

	set_edge_shadow_color (v: EV_COLOR)
		do
			private_edge_shadow_color := v
			if attached edge_shadow as ls then
				ls.set_background_color (v)
			end
		end

	--|--------------------------------------------------------------

	set_position (x1, y1, ay: INTEGER)
			-- Set the starting position of Current on the graph
		do
			bar.set_point_a_position (x1, y1)
			inspect graph_orientation
			when 'l', 'r' then
				bar.set_point_b_position (ay, y1 +  + bar_thickness)
			else
				bar.set_point_b_position (x1 + bar_thickness, ay)
			end
			draw_lines
		end

	set_point_a_position (xp, yp: INTEGER)
		do
			bar.set_point_a_position (xp, yp)
--RFO			if (bar.point_b /= Void) then
				draw_lines
--RFO			end
		end

	set_point_b_position (xp, yp: INTEGER)
		do
			bar.set_point_b_position (xp, yp)
--RFO			if (bar.point_a /= Void) then
				draw_lines
--RFO			end
		end

--|========================================================================
feature -- Rendering
--|========================================================================

	draw_lines
		local
--RFO			pt1, pt2: EV_RELATIVE_POINT
		do
			draw_shadows
--RFO			inspect graph_orientation
--RFO			when 'l' then
--RFO				-- End shadow is right Edge shadow is below
--RFO				create pt1.make_with_position (bar_right_x + 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_left_x + 1, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 1, bar_bottom_y + 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 't' then
--RFO				-- End shadow is below Edge shadow is right
--RFO				create pt1.make_with_position (bar_left_x + 1, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_right_x + 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x + 1, bar_bottom_y - 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 'r' then
--RFO				-- End shadow is left Edge shadow is below
--RFO				create pt1.make_with_position (bar_right_x - 1, bar_top_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 1, bar_bottom_y + 1)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_left_x - 0, bar_bottom_y + 1)
--RFO				create pt2.make_with_position (bar_right_x - 0, bar_bottom_y + 1)
--RFO				edge_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				edge_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO			when 'b' then
--RFO				-- End shadow is top Edge shadow is right
--RFO				create pt1.make_with_position (bar_left_x + 2, bar_top_y - 2)
--RFO				create pt2.make_with_position (bar_right_x + 0, bar_top_y - 2)
--RFO				end_shadow.set_point_a_position (pt1.x, pt1.y)
--RFO				end_shadow.set_point_b_position (pt2.x, pt2.y)
--RFO
--RFO				create pt1.make_with_position (bar_right_x + 0, bar_bottom_y - 4)
--RFO				edge_shadow.set_point_a_position (pt2.x, pt2.y)
--RFO				edge_shadow.set_point_b_position (pt1.x, pt1.y)
--RFO			end
		end

	draw_shadows
		local
			bx1, bx2, by1, by2, st: INTEGER
--RFO			pt1, pt2: EV_RELATIVE_POINT
			tsh: like shadow
		do
			bx1 := bar_left_x
			bx2 := bar_right_x
			by1 := bar_top_y
			by2 := bar_bottom_y
			st := shadow_thickness

			create tsh.make_with_coordinates (
				<<
				create {EV_COORDINATE}.make (bx1, by1),
				create {EV_COORDINATE}.make (bx2, by1),
				create {EV_COORDINATE}.make (bx2 + st, by1 - st),
				create {EV_COORDINATE}.make (bx1 + st, by1 - st)
				>>)
			end_shadow := tsh
			tsh.set_background_color (end_shadow_color)
			extend (tsh)
			send_to_back (tsh)

			create tsh.make_with_coordinates (
				<<
				create {EV_COORDINATE}.make (bx2, by1),
				create {EV_COORDINATE}.make (bx2, by2),
				create {EV_COORDINATE}.make (bx2 + st, by2 - st),
				create {EV_COORDINATE}.make (bx2 + st, by1 - st)
				>>)
			edge_shadow := tsh
			tsh.set_background_color (edge_shadow_color)
			extend (tsh)
			send_to_back (tsh)
		end

--|========================================================================
feature -- Geometry support
--|========================================================================

	bar_thickness: INTEGER

	shadow_thickness: INTEGER
		do
			Result := (bar_thickness / 4).rounded.max (3)
		end

	bar_left_x: INTEGER
		do
			Result := bar.point_a_x
		end

	bar_top_y: INTEGER
		do
			inspect graph_orientation
			when 'l', 't', 'r' then
				Result := bar.point_a_y
			else
				Result := bar.point_a_y
			end
		end

	bar_right_x: INTEGER
		do
			Result := bar.point_b_x
		end

	bar_bottom_y: INTEGER
		do
			inspect graph_orientation
			when 'l', 'r', 't' then
				Result := bar.point_b_y
			else
				Result := bar.point_b_y
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	private_graph_orientation: CHARACTER
	Kc_dflt_graph_orientation: CHARACTER = 'b'

	private_bar_color: detachable EV_COLOR
	private_end_shadow_color: detachable EV_COLOR
	private_edge_shadow_color: detachable EV_COLOR

	dflt_bar_color: EV_COLOR
		once
			Result := colors.blue
		end

	dflt_shadow_color: EV_COLOR
		once
			Result := colors.dark_gray
		end

	colors: AEL_V2_COLORS
		once
			create Result
		end

end -- class AEL_V2_MODEL_BAR
