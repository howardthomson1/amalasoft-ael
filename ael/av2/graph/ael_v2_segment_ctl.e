class AEL_V2_SEGMENT_CTL
-- Complex widget with a SEGMENTED BAR graph

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			create_interface_objects, build_interface_components,
			initialize_interface_values, initialize_interface_actions
		end

create
	make

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	create_interface_objects
		do
			Precursor

			create top_cell
			create graph.make
			create bottom_cell
			create detail.make
			create space_before_box.make
			create space_before_label.make_with_text ("Free space before: ")
			create space_before_ctl
			create space_before_lock_btn.make
			create size_box.make
			create size_label.make_with_text ("Size: ")
			create size_ctl
			create size_lock_btn.make
			create space_after_box.make
			create space_after_label.make_with_text ("Free space after: ")
			create space_after_ctl
			create space_after_lock_btn.make
		end

	build_interface_components
		do
			Precursor

			extend (top_cell)
			top_cell.set_minimum_height (K_graph_margin_height)
			disable_item_expand (top_cell) 

			extend (graph)
			graph.set_minimum_height (K_bar_height + 2)
			disable_item_expand (graph) 

			extend (bottom_cell)
			bottom_cell.set_minimum_height (K_graph_margin_height)
			disable_item_expand (bottom_cell) 

			extend (detail) 

			detail.extend (space_before_box) 

			space_before_box.extend (space_before_label)
			space_before_label.align_text_right
			space_before_label.set_minimum_width (K_detail_label_width)
			space_before_box.disable_item_expand (space_before_label) 

			space_before_box.extend (space_before_ctl)
			space_before_ctl.set_minimum_width (K_detail_ctl_width)
			space_before_box.disable_item_expand (space_before_ctl) 

			space_before_box.extend (space_before_lock_btn)
			space_before_box.disable_item_expand (space_before_lock_btn)
			space_before_box.insert_free_space 

			detail.extend (size_box) 

			size_box.extend (size_label)
			size_label.align_text_right
			size_label.set_minimum_width (K_detail_label_width)
			size_box.disable_item_expand (size_label) 

			size_box.extend (size_ctl)
			size_ctl.set_minimum_width (K_detail_ctl_width)
			size_box.disable_item_expand (size_ctl) 

			size_box.extend (size_lock_btn)
			size_box.disable_item_expand (size_lock_btn)
			size_box.insert_free_space 

			detail.extend (space_after_box) 

			space_after_box.extend (space_after_label)
			space_after_label.align_text_right
			space_after_label.set_minimum_width (K_detail_label_width)
			space_after_box.disable_item_expand (space_after_label) 

			space_after_box.extend (space_after_ctl)
			space_after_ctl.set_minimum_width (K_detail_ctl_width)
			space_after_box.disable_item_expand (space_after_ctl) 

			space_after_box.extend (space_after_lock_btn)
			space_after_box.disable_item_expand (space_after_lock_btn)
			space_after_box.insert_free_space

			insert_free_space
		end

	--|------------------------------------------------------------------------

	initialize_interface_values
		do
			Precursor
			set_border_width (5)
			set_padding (5)

			--O    space_before_ctl.set_text (space_before)
			--O    size_ctl.set_text (size)
			--O    space_after_ctl.set_text (space_after)
		end

	--|------------------------------------------------------------------------

	initialize_interface_actions
		do
			Precursor

			graph.set_right_btn_down_procedure (agent on_figure_rtbtn_down)
			graph.set_selection_change_procedure (agent on_figure_select)
			graph.set_geometry_change_procedure (agent on_figure_move_resize)

			space_before_ctl.change_actions.extend (agent on_space_before_ctl_change)
			space_before_ctl.return_actions.extend (agent
			on_ctl_text_change(space_before_ctl))
			space_before_lock_btn.set_change_procedure (agent on_lock_change)

			size_ctl.change_actions.extend (agent on_size_ctl_change)
			size_ctl.return_actions.extend (agent on_ctl_text_change(size_ctl))
			size_lock_btn.set_change_procedure (agent on_lock_change)

			space_after_ctl.change_actions.extend (agent on_space_after_ctl_change)
			space_after_ctl.return_actions.extend (agent
			on_ctl_text_change(space_after_ctl))
			space_after_lock_btn.set_change_procedure (agent on_lock_change)

			--| To support disabling capture

			pointer_leave_actions.extend (agent on_mouse_leave)
			pointer_enter_actions.extend (agent on_mouse_enter)
			top_cell.pointer_leave_actions.extend (agent on_mouse_leave)
			top_cell.pointer_enter_actions.extend (agent on_mouse_enter)
			bottom_cell.pointer_leave_actions.extend (agent on_mouse_leave)
			bottom_cell.pointer_enter_actions.extend (agent on_mouse_enter)
		end

	--|========================================================================
feature -- Components
	--|========================================================================

	top_cell: EV_CELL
	graph: AEL_V2_SEGMENTED_BAR
	bottom_cell: EV_CELL

	detail: AEL_V2_FRAMED_VERTICAL_BOX

	space_before_box: AEL_V2_HORIZONTAL_BOX
	space_before_label: AEL_V2_LABEL
	space_before_ctl: EV_SPIN_BUTTON
	space_before_lock_btn: AEL_V2_LOCK_BUTTON

	size_box: AEL_V2_HORIZONTAL_BOX
	size_label: AEL_V2_LABEL
	size_ctl: EV_SPIN_BUTTON
	size_lock_btn: AEL_V2_LOCK_BUTTON

	space_after_box: AEL_V2_HORIZONTAL_BOX
	space_after_label: AEL_V2_LABEL
	space_after_ctl: EV_SPIN_BUTTON
	space_after_lock_btn: AEL_V2_LOCK_BUTTON

	--|------------------------------------------------------------------------

	popup_menu: detachable EV_MENU
	split_menu_item: detachable EV_MENU_ITEM
	
	delete_menu_item: detachable EV_MENU_ITEM
	zoom_in_menu_item: detachable EV_MENU_ITEM
	zoom_out_menu_item: detachable EV_MENU_ITEM
	props_menu_item: detachable EV_MENU_ITEM

	--|========================================================================
feature -- Status report
	--|========================================================================

	data_source: LINKED_LIST [ ANY ]
		do
			create Result.make
		end

	--|========================================================================
feature -- Status setting
	--|========================================================================

	set_data_source (v: detachable like data_source)
		do
			data_source.wipe_out
			if v /= Void then
				data_source.fill (v)
			end
		end

	--|------------------------------------------------------------------------

	selected_figure: AEL_V2_FIGURE_SEGMENT
		do
			Result := graph.selected_figure
		end

	--|========================================================================
feature {NONE} -- Agents and callbacks
	--|========================================================================

	on_mouse_enter
		do
			graph.kill_capture
		end

	on_mouse_leave
		do
			graph.kill_capture
		end

	--|------------------------------------------------------------------------

	on_lock_change (b: like size_lock_btn)
		local
			ctl: like size_ctl
		do
			if b = space_before_lock_btn then
				ctl := space_before_ctl
			elseif b = size_lock_btn then
				ctl := size_ctl
			elseif b = space_after_lock_btn then
				ctl := space_after_ctl
			else
				-- No such case
			end

			if b.is_locked then
				ctl.disable_sensitive
			else
				ctl.enable_sensitive
			end
		end

	--|------------------------------------------------------------------------

	on_figure_select (f: like selected_figure)
		do
			if f = Void then
				clear_data_fields
			else
				update_data_fields (f)
			end
		end

	on_figure_move_resize (f: like selected_figure)
		do
			if f = Void then
				clear_data_fields
			else
				update_data_fields (f)
			end
		end

	--|------------------------------------------------------------------------

	update_data_fields (f: like selected_figure)
		local
			tf: like selected_figure
			spc: INTEGER
		do
			space_before_ctl.enable_sensitive
			tf := graph.previous_figure (f)
			if tf = Void then
				-- Must be the first?
				spc := f.x_position
			else
				spc := f.x_position - tf.end_position
			end
			space_before_ctl.set_text (spc.out)

			size_ctl.enable_sensitive
			size_ctl.set_text (f.width.out)

			space_after_ctl.enable_sensitive
			space_after_ctl.set_text (f.end_position.out)
			tf := graph.next_figure (f)
			if tf = Void then
				-- Must be the last?
				spc := (graph.width - 2) - f.end_position
			else
				spc := tf.end_position - f.end_position
			end
			space_after_ctl.set_text (spc.out)
		end

	--|------------------------------------------------------------------------

	clear_data_fields
		do
			space_before_ctl.disable_sensitive
			size_ctl.disable_sensitive
			space_after_ctl.disable_sensitive
		end

	--|------------------------------------------------------------------------
	--|------------------------------------------------------------------------

	on_space_before_ctl_change (v: INTEGER)
		do
		end

	--|------------------------------------------------------------------------

	on_size_ctl_change (v: INTEGER)
		do
		end

	--|------------------------------------------------------------------------

	on_space_after_ctl_change (v: INTEGER)
		do
		end

	--|------------------------------------------------------------------------

	on_ctl_text_change (w: like size_ctl)
		do
			if w = space_before_ctl then
			elseif w = size_ctl then
			elseif w = space_after_ctl then
			else
			end
		end

	--|------------------------------------------------------------------------

	on_figure_rtbtn_down (f: like selected_figure)
		do 
			create popup_menu.make_with_text("Sample popup") 
			create split_menu_item.make_with_text ("Split")
			popup_menu.extend (split_menu_item) 
			create delete_menu_item.make_with_text ("Delete")
			popup_menu.extend (delete_menu_item) 
			create zoom_in_menu_item.make_with_text ("Zoom In")
			popup_menu.extend (zoom_in_menu_item) 
			create zoom_out_menu_item.make_with_text ("Zoom Out")
			popup_menu.extend (zoom_out_menu_item) 
			create props_menu_item.make_with_text ("Properties ...")
			popup_menu.extend (props_menu_item)
			popup_menu.item_select_actions.extend (agent on_popup_item_select(?,f))
			--popup_menu.set_capture_change_procedure (agent on_popup_menu_close)
			popup_menu.show
		end

	--|------------------------------------------------------------------------

	on_popup_item_select (w: EV_MENU_ITEM; f: like selected_figure)
		do
			if w = split_menu_item then
				--| Split the selected object into 2
			elseif w = delete_menu_item then
				--| Remove from working list, and from figure world
				graph.remove_figure (f)
			elseif w = zoom_in_menu_item then
			elseif w = zoom_out_menu_item then
			elseif w = props_menu_item then
			else
			end
		end

	--|========================================================================
feature {NONE} -- Constants
	--|========================================================================

	K_detail_label_width: INTEGER = - 150
	K_detail_ctl_width: INTEGER = 90
			-- K_bar_height: INTEGER = 40
	K_bar_height: INTEGER = 50
	K_graph_margin_height: INTEGER = 5

--|------------------------------------------------------------------------
invariant


end -- class AEL_V2_SEGMENT_CONTROL
