deferred class AEL_V2_XY_GRAPH
-- Graph/plot widget using an XY (coordinate system)

inherit
	AEL_V2_GRAPH
		redefine
			create_interface_objects, build_interface_components,
			purge,
			on_item_mouse_over, on_item_mouse_leave, on_item_mouse_btn_press,
			on_mouse_btn_press, on_mouse_btn_release
		end

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	create_interface_objects
		do
			Precursor
			create yhash_labels.make (0)
			create yhash_values.make (0)

			if has_connectors then
				create connectors.make
			end
		end

	build_interface_components
		do
			initialize_orientation
			Precursor
			projector.world.pointer_button_press_actions.extend (agent on_item_mouse_btn_press (Void,?,?,?,?,?,?,?,?))
		end

	--|--------------------------------------------------------------

	initialize_orientation
		do
			orientation := 'n'
		end

--|========================================================================
feature -- Components
--|========================================================================

	connectors: detachable TWO_WAY_LIST [EV_MODEL_LINE]
		note
			option: stable
			attribute
		end

	x_axis: detachable EV_MODEL_LINE
	y_axis: detachable EV_MODEL_LINE

--	point_label: AEL_V2_MODEL_TEXT
	point_label: detachable AEL_V2_GRAPH_POINT_LABEL
	h_hair_line: detachable EV_MODEL_GROUP
	h_hair_line_line: detachable EV_MODEL_LINE
	h_hair_line_text: detachable EV_MODEL_TEXT
	v_hair_line: detachable EV_MODEL_GROUP
	v_hair_line_line: detachable EV_MODEL_LINE
	v_hair_line_text: detachable EV_MODEL_TEXT

--|========================================================================
feature -- Status
--|========================================================================

	has_connectors: BOOLEAN
		do
		end

	axes_are_hidden: BOOLEAN
			-- Should x and y axes be hidden?

	x_axis_color: EV_COLOR
		do
			if attached private_x_axis_color as lc then
				Result := lc
			else
				Result := colors.black
			end
		end

	y_axis_color: EV_COLOR
		do
			if attached private_y_axis_color as lc then
				Result := lc
			else
				Result := colors.black
			end
		end

	axis_line_width: INTEGER
		do
			Result := private_axis_line_width
			if Result = 0 then
				Result := K_dflt_axis_line_width
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_orientation (v: like orientation)
		require
			valid: is_valid_orientation (v)
		do
			orientation := v
		end

	--|--------------------------------------------------------------

	hide_axes
		do
			axes_are_hidden := True
			if attached x_axis as lxa then
				lxa.hide
			end
			if attached y_axis as lya then
				lya.hide
			end
		end

	--|--------------------------------------------------------------

	set_x_axis_color (v: EV_COLOR)
		do
			private_x_axis_color := v
		end

	set_y_axis_color (v: EV_COLOR)
		do
			private_y_axis_color := v
		end

	set_axis_line_width (v: INTEGER)
		require
			not_negative: v >= 0
		do
			private_axis_line_width := v
		end

--|========================================================================
feature {NONE} -- Hidden status
--|========================================================================

	private_x_axis_color: detachable EV_COLOR
	private_y_axis_color: detachable EV_COLOR
	private_axis_line_width: INTEGER

--|========================================================================
feature -- Rendering
--|========================================================================

	update_graph
			-- Regenerate graph with updated geometry
		local
			si, i: INTEGER
			ti: AEL_V2_GRAPH_ITEM
			dl: like series
			til: LINKED_LIST [AEL_V2_GRAPH_ITEM]
		do
			purge
			data_source.do_all (agent factor_series_for_scale)
			update_geometry

			-- Axes should be layered below all other elements
			if not axes_are_hidden then
				draw_axes
			end

			check attached items as tl then
				from
					si := 1
					tl.start
					data_source.start
				until data_source.exhausted
				loop
					--til := tl.item (si)
					til := tl.item
					dl := data_source.item
					from i := dl.lower
					until i > dl.upper
					loop
						ti := new_item_at_position (dl.item (i), si, i)
						til.extend (ti)
						i := i + 1
					end
					-- For each series, add items last to force z-order
					from til.start
					until til.exhausted
					loop
						projector.world.extend (til.item.figure)
						til.forth
					end
					si := si + 1
					tl.forth
					data_source.forth
				end
			end
--RFO			draw_labels
			projector.full_project
		end

	--|--------------------------------------------------------------

	new_figure_at_position (si, i: INTEGER; pt: EV_COORDINATE): EV_MODEL
		deferred
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_item_at_position (sl: REAL; si, i: INTEGER): AEL_V2_GRAPH_ITEM
			-- Newly instantiated graph item at position 'i' representing
			-- value 'sl'
		local
			tfig: like new_figure_at_position
			pt: EV_COORDINATE
		do
			pt := new_item_coordinate (sl, i)
			tfig := new_figure_at_position (si, i, pt)
			create Result.make (tfig, sl, "Item " + si.out + "." + i.out)

			tfig.pointer_enter_actions.extend (agent on_item_mouse_over (Result))
			tfig.pointer_leave_actions.extend (agent on_item_mouse_leave (Result))
			tfig.pointer_button_press_actions.extend (agent on_item_mouse_btn_press (Result,?,?,?,?,?,?,?,?))
			tfig.pointer_button_release_actions.extend (agent on_mouse_btn_release)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_item_coordinate (sl: REAL; i: INTEGER): EV_COORDINATE
			-- Newly instantiated coordinate for an item in position 'i'
			-- with value 'sl'
		do
			inspect orientation
			when 'n', 's' then
				-- South is not supported currently
				create Result.make (
					y_axis_horizontal_margin + (chart_value_interval * (i - 1)),
					y_from_value (sl))
			when 'e', 'w' then
				-- West is not supported currently
				-- Eastern-pointing items start at just above X axis
				-- and are horizontally oriented, drawn from Y axis
				create Result.make (
					x_from_value (sl),
					x_axis_vertical_offset - (chart_value_interval * i) + 5)
				-- RFO, no else case; no match is an exception
			end
		end

	--|--------------------------------------------------------------

	add_connector (si: INTEGER; ppt, pt: EV_COORDINATE)
		require
			has_connectors: has_connectors
		local
			tl: EV_MODEL_LINE
		do
			create tl.make_with_points (ppt, pt)
			tl.set_foreground_color (item_colors.item (si).item (1))
			tl.pointer_button_release_actions.extend (agent on_mouse_btn_release)
			if attached connectors as lc then
				lc.extend (tl)
			end
			projector.world.extend (tl)
		end

--|========================================================================
feature {NONE} -- Rendering support
--|========================================================================

	draw_axes
		require
			not_hidden: not axes_are_hidden
		local
			i, lim, x_spos, x_epos, y_spos, y_epos, h_xp, h_yp, h1, h2: INTEGER
			th: EV_MODEL_LINE
			lb: AEL_V2_MODEL_TEXT
			lx: like x_axis
			ly: like y_axis
		do
			x_spos := y_axis_horizontal_margin
			x_epos := width - y_axis_horizontal_margin
			y_spos := x_axis_vertical_margin
			y_epos := x_axis_vertical_offset

			create lx.make_with_positions (x_spos, y_epos, x_epos, y_epos)
			x_axis := lx
			lx.set_line_width (axis_line_width)
			lx.set_foreground_color (x_axis_color)
			projector.world.extend (lx)
			lx.pointer_double_press_actions.extend (
				agent on_2click (lx,?,?,?,?,?,?,?,?))
			create ly.make_with_positions (x_spos, y_spos, x_spos, y_epos)
			y_axis := ly
			ly.set_line_width (axis_line_width)
			ly.set_foreground_color (y_axis_color)
			projector.world.extend (ly)
			ly.pointer_double_press_actions.extend (
				agent on_2click (ly,?,?,?,?,?,?,?,?))

			-- Draw X axis hashes
			lim := data_item_count.max (4)
			h1 := y_epos
			h2 := h1 + K_hash_mark_length
			from i := 1
			until i > lim
			loop
				h_xp := x_spos + (chart_value_interval * i)
				if h_xp < x_epos then
					create th.make_with_positions (h_xp, h1, h_xp, h2)
					th.set_foreground_color (y_axis_color)
					projector.world.extend (th)
				end
				i := i + 1
			end
			-- Draw Y axis hashes
			h1 := x_spos - K_hash_mark_length
			h2 := x_spos
			from i := 1
			until i > number_of_intervals
			loop
				h_yp := y_from_value (yhash_values.i_th (i))
				create th.make_with_positions (h1, h_yp, h2, h_yp)
				th.set_foreground_color (y_axis_color)
				projector.world.extend (th)
				lb := yhash_labels.i_th (i)
				lb.set_point_position (K_y_axis_label_margin, h_yp)
				projector.world.extend (lb)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	purge
		do
			Precursor

			point_label := Void
			h_hair_line := Void
			v_hair_line := Void
			x_axis := Void
			y_axis := Void
			if has_connectors and then attached connectors as lc then
				lc.wipe_out
			end
		end

	--|--------------------------------------------------------------

	hide_hair_lines
		do
			if attached h_hair_line as lhl then
				lhl.hide
				lhl.set_point_position (0, 0)
			end
			if attached v_hair_line as lhl then
				lhl.hide
				lhl.set_point_position (0, 0)
			end
		end

	hide_point_label
		do
			if attached point_label as lpl then
				lpl.hide
				lpl.set_point_position (width, height)
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_item_mouse_over (ti: AEL_V2_GRAPH_ITEM)
		local
			px, py: INTEGER
			is_new: BOOLEAN
			tpl: like point_label
		do
			tpl := point_label
			if tpl = Void then
				create tpl
				point_label := tpl
				tpl.set_foreground_color (colors.blue)
				projector.world.extend (tpl)
				is_new := True
			end
			if attached {EV_MODEL} ti.figure as f then
				px := f.x
			end
			-- Place text enough above bar to clear
			py := (y_from_value (ti.data) - K_mouse_over_text_v_offset).max (0)
			if attached ti.label as llb then
				tpl.set_text (llb)
			else
				tpl.set_text (ti.data.out)
			end
			px := px.min (width - tpl.overall_width)
			tpl.set_point_position (px, py)
			tpl.show
			if is_new then
				projector.project
			end
		end

	--|--------------------------------------------------------------

	on_item_mouse_leave (ti: AEL_V2_GRAPH_ITEM)
		do
			if attached point_label as lpl then
				lpl.set_point_position (width, height)
				lpl.hide
			end
		end

	--|--------------------------------------------------------------

	on_item_mouse_btn_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		local
			is_new: BOOLEAN
		do
			hide_point_label
			hide_hair_lines
			if b = 3 then
				if application.ctrl_pressed then
					is_new := h_hair_line = Void
					on_button_3_ctl_press (ti, xp, yp)
				else
					is_new := v_hair_line = Void
					on_button_3_noctl_press (ti, xp, yp)
				end
				if is_new then
					projector.project
				end
			else
			end
		end

	--|--------------------------------------------------------------

	on_button_3_noctl_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp: INTEGER)
		do
			show_horizontal_hairline (xp, yp)
		end

	--|--------------------------------------------------------------

	on_button_3_ctl_press (ti: detachable AEL_V2_GRAPH_ITEM; xp, yp: INTEGER)
		do
			show_vertical_hairline (xp, yp, False)
		end

	--|--------------------------------------------------------------

	on_mouse_btn_press (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
			hide_point_label
			hide_hair_lines
		end

	--|--------------------------------------------------------------

	on_mouse_btn_release (xp, yp, b: INTEGER; xt, yt, p: DOUBLE; sx, sy: INTEGER)
		do
			hide_point_label
			hide_hair_lines
			if b = 3 then
			else
			end
--RFO			projector.project
		end

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	show_vertical_hairline (xp, yp: INTEGER; lf: BOOLEAN)
		do
			if v_hair_line = Void then
				create v_hair_line
				create v_hair_line_line
				create v_hair_line_text
				if attached v_hair_line as lhl
					and then attached v_hair_line_line as lhll
						and then attached v_hair_line_text as lhlt
				 then
					 lhl.extend (lhll)
					 lhll.set_point_a_position (0, 5)
					 lhll.set_point_b_position (0, height - 15)
					 lhl.extend (lhlt)
					 lhlt.set_point_position (6, 3)
					 projector.world.extend (lhl)
					 projector.world.send_to_back (lhl)
				end
			end
			if attached v_hair_line as lhl
				and then attached v_hair_line_text as lhlt
			 then
				 lhl.set_point_position (xp, 0)
				if lf then
					lhlt.set_text (value_from_x (xp).out)
				else
					lhlt.set_text ("")
				end
				lhl.show
			end
		end

	--|--------------------------------------------------------------

	show_horizontal_hairline (xp, yp: INTEGER)
		do
			if h_hair_line = Void then
				create h_hair_line
				create h_hair_line_line
				create h_hair_line_text
				if attached h_hair_line as lhl
					and then attached h_hair_line_line as lhll
						and then attached h_hair_line_text as lhlt
				 then
					 lhl.extend (lhll)
					 lhll.set_point_a_position (0, 0)
					 lhll.set_point_b_position (width - right_margin - 15, 0)
					 lhl.extend (lhlt)
					 lhlt.set_point_position (width - right_margin - 12, -10)
					 projector.world.extend (lhl)
					 projector.world.bring_to_front (lhl)
				end
			end
			if attached h_hair_line as lhl
				and then attached h_hair_line_text as lhlt
			 then
				 lhl.set_point_position (0, yp)
				 lhlt.set_text (value_from_y (yp).out)
				 lhl.show
			end
		end

--|========================================================================
feature -- Orientation
--|========================================================================

	orientation: CHARACTER_8
			-- Chart orientation
			-- 'n' for vertical starting at bottom (i.e. pointing north)
			-- 's' for vertical starting at top (i.e. pointing south)
			-- 'e' for horizontal starting at left (i.e. pointing east)
			-- 'w' for horizontal starting at right (i.e. pointing west)

	is_valid_orientation (v: like orientation): BOOLEAN
		do
			inspect v
			when 'n', 's', 'e', 'w' then
				Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	is_vertical: BOOLEAN
		do
			inspect orientation
			when 'n', 's' then
				Result := True
			else
			end
		end

--|========================================================================
feature {NONE} -- Geometry support
--|========================================================================

	update_geometry
		local
			i, n, m, td, nd, dv, iv: INTEGER
			v: REAL
			isneg: BOOLEAN
			ts: STRING
			lb: AEL_V2_MODEL_TEXT
		do
			-- Set chart item interval as the distance, in pixels,
			-- between points on the value axis (y for vertical)
			v := largest_value
			if v < 0 then
				isneg := True
				v := -v
			end
			ts := v.truncated_to_integer.out
			nd := ts.count
			if nd = 1 then
				m := v.ceiling
				n := m - 1
				iv := 1
			else
				dv := (10^(nd - 1)).rounded
				n := (v / dv).ceiling
				m := n * dv
				n := n - 1
				iv := dv
			end
			if isneg then
				m := -m
			end
			number_of_intervals := n
			if is_vertical then
				chart_value_interval := (plot_area_height / n).rounded.max (1)
			else
				chart_value_interval := (plot_area_width / n).rounded.max (1)
			end

			yhash_labels.grow (n)
			yhash_values.grow (n)
			max_y_label_width := 0
			from i := 1
			until i > n
			loop
				create lb.make_with_text (i.out)
				max_y_label_width := max_y_label_width.max (lb.overall_width)
				yhash_labels.extend (lb)
				yhash_values.extend (i)
				i := i + iv
			end

			-- Set chart x interval as the distance, in pixels,
			-- between points on the X axis
			-- Interval = Bar + (shadow=bar*.25) + (space=bar*.75) == Bar*2
			if data_item_count = 0 then
				-- 10 pixel separate is reasonable when no data is 
				-- present, just to show something
			else
				-- Items (bar, shadow and space) must fit within 
				-- plot_area_width
				td := data_item_count
				if td = 0 then
					td := 1
				end
				if is_vertical then
					chart_value_interval := (plot_area_width / td).rounded
				else
					chart_value_interval := (plot_area_height / td).rounded
				end
			end

			if is_vertical then
				value_scale_factor := plot_area_height / largest_value
			else
				value_scale_factor := plot_area_width / largest_value
			end
		end

	--|--------------------------------------------------------------

	chart_value_interval: INTEGER
			-- Distance, in pixels, between points on the value axis

	--|--------------------------------------------------------------

	x_axis_vertical_offset: INTEGER
			-- Offset from top of graph area to x axis
		do
			Result := height - x_axis_vertical_margin
		end

	x_axis_vertical_margin: INTEGER
		do
			Result := bottom_margin
		end

	y_axis_horizontal_margin: INTEGER
			-- Distance, in pixels, between edge of area and y axis
		do
			-- Determine x axis margins
			Result := (width // 8).min (20).max (max_y_label_width + K_y_axis_label_margin)
		end

	value_scale_factor: REAL
			-- The factor by which the series values should be scaled

	--|--------------------------------------------------------------

	x_from_value (v: REAL): INTEGER
			-- X position for given value (in horizontal orientations)
		require
			is_horizontal: not is_vertical
		do
			if orientation = 'e' then
				Result := y_axis_horizontal_margin
			else
				Result := right_margin
			end
		end

	y_from_value (v: REAL): INTEGER
			-- Y position for given value
		do
			Result := x_axis_vertical_offset - (v * value_scale_factor).rounded
		end

	value_from_y (v: INTEGER): REAL
			-- Value at given Y position
		do
			Result := (x_axis_vertical_offset - v) / value_scale_factor
		end

	value_from_x (v: INTEGER): REAL
			-- Value at given X position
			-- Always zero when X is horizontal axis
		do
		end

	yhash_labels: ARRAYED_LIST [AEL_V2_MODEL_TEXT]
	yhash_values: ARRAYED_LIST [INTEGER]

	number_of_intervals: INTEGER

	max_y_label_width: INTEGER

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	dot_width: INTEGER = 5

	half_dot: INTEGER
		do
			Result := dot_width // 2
		end

	--|--------------------------------------------------------------

	K_hash_mark_length: INTEGER = 5

	K_dflt_axis_line_width: INTEGER = 2

	K_y_axis_label_margin: INTEGER = 3

	K_mouse_over_text_v_offset: INTEGER = 25

	K_color_x_axis: EV_COLOR
		do
			Result := colors.dark_green
		end

	K_color_y_axis: EV_COLOR
		do
			Result := colors.dark_green
		end

end -- class AEL_V2_XY_GRAPH
