note
	description: "GTK Impementation of the AEL_V2_CASING_TEXT widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."
	
class AEL_V2_CASING_TEXT_IMP

inherit
	EV_PASSWORD_FIELD_IMP
		rename
			make as pwf_make
		redefine
			interface
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (uf, lf: BOOLEAN)
		do
			is_upper := uf
			is_lower := lf
			pwf_make
			{GTK}.gtk_entry_set_visibility (entry_widget, True)
		end

 --|========================================================================
feature -- Interface
 --|========================================================================

	interface: detachable AEL_V2_CASING_TEXT note option: stable attribute end

	is_upper: BOOLEAN
	is_lower: BOOLEAN

--|========================================================================
feature {NONE} -- State setting
--|========================================================================

	set_is_upper
		do
			is_upper := True
			is_lower := False
		end

	set_is_lower
		do
			is_upper := False
			is_lower := True
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| This is an implemenation class is not be accessed directly by clients
--|----------------------------------------------------------------------

end -- class AEL_V2_CASING_TEXT_IMP
