note
	description: "Pixel buffer that replaces original image file.%
		%The original version of this class has been generated by Image Eiffel Code."

class
	CAPTURE_BTN_TW_RIGHT_PBUF

inherit
	EV_PIXEL_BUFFER

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization
		do
			make_with_size (25, 25)
			fill_memory
		end

feature {NONE} -- Image data

	c_colors_0 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,CA,CA,CA)A(FF,BF,BF,BF)A(FF,C0,C0,C0)A(FF,DD,DD,DD)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,EA,EA,EA)A(FF,68,68,68)A(FF,41,41,41)A(FF,44,44,44)A(FF,C0,C0,C0)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E9,E9,E9)A(FF,83,83,83)A(FF,4B,4B,4B)A(FF,4C,4C,4C)A(FF,41,41,41)A(FF,BF,BF,BF)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E9,E9,E9)A(FF,A6,A6,A6)A(FF,4D,4D,4D)A(FF,46,46,46)A(FF,4B,4B,4B)A(FF,68,68,68)A(FF,CA,CA,CA)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,EA,EA,EA)A(FF,A7,A7,A7)A(FF,55,55,55)A(FF,49,49,49)A(FF,4D,4D,4D)A(FF,82,82,82)A(FF,EA,EA,EA)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EA,EA,EA)A(FF,D8,D8,D8)A(FF,9E,9E,9E)A(FF,57,57,57)A(FF,44,44,44)A(FF,54,54,54)A(FF,A6,A6,A6)A(FF,E9,E9,E9)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E9,E9,E9)A(FF,CF,CF,CF)A(FF,93,93,93)A(FF,62,62,62)A(FF,4A,4A,4A)A(FF,58,58,58)A(FF,A7,A7,A7)A(FF,E9,E9,E9)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E9,E9,E9)A(FF,A5,A5,A5)A(FF,5E,5E,5E)A(FF,8B,8B,8B)A(FF,C5,C5,C5)A(FF,5C,5C,5C)A(FF,9D,9D,9D)A(FF,EA,EA,EA)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E9,E9,E9)A(FF,EA,EA,EA)A(FF,A3,A3,A3)A(FF,67,67,67)A(FF,AB,AB,AB)A(FF,E7,E7,E7)A(FF,85,85,85)A(FF,63,63,63)A(FF,D4,D4,D4)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,CE,CE,CE)A(FF,6E,6E,6E)A(FF,6A,6A,6A)A(FF,C9,C9,C9)
				A(FF,F0,F0,F0)A(FF,B0,B0,B0)A(FF,50,50,50)A(FF,A8,A8,A8)A(FF,EB,EB,EB)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,EA,EA,EA)A(FF,C2,C2,C2)A(FF,5F,5F,5F)A(FF,6B,6B,6B)A(FF,CD,CD,CD)A(FF,F0,F0,F0)A(FF,CA,CA,CA)A(FF,58,58,58)A(FF,80,80,80)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EA,EA,EA)A(FF,D4,D4,D4)A(FF,9C,9C,9C)A(FF,5D,5D,5D)A(FF,78,78,78)A(FF,E4,E4,E4)A(FF,EC,EC,EC)A(FF,D0,D0,D0)A(FF,63,63,63)A(FF,7D,7D,7D)A(FF,E8,E8,E8)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EA,EA,EA)A(FF,CF,CF,CF)A(FF,67,67,67)A(FF,5E,5E,5E)A(FF,B5,B5,B5)A(FF,E6,E6,E6)A(FF,E9,E9,E9)A(FF,E7,E7,E7)A(FF,6E,6E,6E)A(FF,66,66,66)A(FF,E9,E9,E9)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,CE,CE,CE)A(FF,64,64,64)A(FF,5E,5E,5E)A(FF,BE,BE,BE)A(FF,EA,EA,EA)A(FF,E7,E7,E7)A(FF,EA,EA,EA)A(FF,AA,AA,AA)A(FF,51,51,51)A(FF,A8,A8,A8)A(FF,EB,EB,EB)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E9,E9,E9)A(FF,CF,CF,CF)A(FF,64,64,64)A(FF,5A,5A,5A)A(FF,C0,C0,C0)A(FF,EA,EA,EA)A(FF,E7,E7,E7)A(FF,EA,EA,EA)A(FF,B9,B9,B9)A(FF,55,55,55)A(FF,80,80,80)A(FF,E9,E9,E9)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E9,E9,E9)A(FF,74,74,74)A(FF,52,52,52)A(FF,BB,BB,BB)A(FF,EA,EA,EA)A(FF,E7,E7,E7)A(FF,EA,EA,EA)A(FF,C0,C0,C0)A(FF,56,56,56)A(FF,72,72,72)A(FF,E9,E9,E9)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	c_colors_1 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,C1,C1,C1)A(FF,56,56,56)A(FF,A9,A9,A9)A(FF,EA,EA,EA)A(FF,E7,E7,E7)A(FF,EA,EA,EA)A(FF,C0,C0,C0)A(FF,5B,5B,5B)A(FF,66,66,66)A(FF,D1,D1,D1)A(FF,E9,E9,E9)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,D4,D4,D4)A(FF,60,60,60)A(FF,70,70,70)A(FF,E7,E7,E7)A(FF,E8,E8,E8)A(FF,EA,EA,EA)A(FF,BC,BC,BC)A(FF,5A,5A,5A)A(FF,64,64,64)A(FF,CF,CF,CF)A(FF,EA,EA,EA)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,9E,9E,9E)A(FF,66,66,66)A(FF,CE,CE,CE)A(FF,EC,EC,EC)A(FF,E7,E7,E7)A(FF,A9,A9,A9)A(FF,51,51,51)A(FF,64,64,64)A(FF,CE,CE,CE)A(FF,EB,EB,EB)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,76,76,76)A(FF,BA,BA,BA)
				A(FF,F0,F0,F0)A(FF,CD,CD,CD)A(FF,70,70,70)A(FF,56,56,56)A(FF,74,74,74)A(FF,CF,CF,CF)A(FF,EA,EA,EA)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,80,80,80)A(FF,7F,7F,7F)A(FF,EE,EE,EE)A(FF,B9,B9,B9)A(FF,64,64,64)A(FF,5F,5F,5F)A(FF,C0,C0,C0)A(FF,EA,EA,EA)A(FF,E9,E9,E9)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,D9,D9,D9)A(FF,83,83,83)A(FF,DB,DB,DB)A(FF,7E,7E,7E)A(FF,76,76,76)A(FF,9E,9E,9E)A(FF,D5,D5,D5)A(FF,EC,EC,EC)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,B9,B9,B9)A(FF,99,99,99)A(FF,83,83,83)A(FF,80,80,80)A(FF,E5,E5,E5)A(FF,EB,EB,EB)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EA,EA,EA)A(FF,AF,AF,AF)A(FF,BA,BA,BA)A(FF,D9,D9,D9)A(FF,EC,EC,EC)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EA,EA,EA)A(FF,EB,EB,EB)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	build_colors (a_ptr: POINTER)
			-- Build `colors'.
		do
			c_colors_0 (a_ptr, 0)
			c_colors_1 (a_ptr, 400)
		end

feature {NONE} -- Image data filling.

	fill_memory
			-- Fill image data into memory.
		local
			l_pointer: POINTER
		do
			if attached {EV_PIXEL_BUFFER_IMP} implementation as l_imp then
				l_pointer := l_imp.data_ptr
				if not l_pointer.is_default_pointer then
					build_colors (l_pointer)
					l_imp.unlock
				end
			end
		end

end -- CAPTURE_BTN_TW_RIGHT_PBUF
