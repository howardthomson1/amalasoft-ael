note
	description: "Pixel buffer that replaces original image file.%
		%The original version of this class has been generated by Image Eiffel Code."

class
	TB_SEARCH_16X15_PBUF

inherit
	EV_PIXEL_BUFFER

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization
		do
			make_with_size (16, 15)
			fill_memory
		end

feature {NONE} -- Image data

	c_colors_0 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,FF,FF,FF)A(FF,F4,F5,F5)A(FF,F5,F5,F5)A(FF,FF,FF,FF)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,EB,F3,F3)A(FF,FC,FF,FF)A(FF,F7,FF,FF)A(FF,FB,FF,FF)A(FF,FF,FF,FF)A(FF,F3,F3,F3)A(FF,4D,4D,4D)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EC,EC,EC)A(FF,4D,4D,4D)A(FF,FF,FF,FF)A(FF,F4,FF,FF)A(FF,EA,FF,FF)A(FF,ED,FF,FF)A(FF,F5,FF,FF)A(FF,FE,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,4D,4D,4D)A(FF,EB,EB,EB)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E7,E7,E7)A(FF,4D,4D,4D)A(FF,ED,F7,F7)A(FF,E9,FF,FF)A(FF,E1,FF,FF)A(FF,E8,FF,FF)A(FF,F2,FF,FF)A(FF,FC,FF,FF)A(FF,FF,FF,FF)A(FF,F6,F6,F6)A(FF,4D,4D,4D)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E7,E7,E7)A(FF,4D,4D,4D)A(FF,EF,F7,F7)A(FF,EE,FF,FF)A(FF,EA,FF,FF)A(FF,ED,FF,FF)A(FF,F5,FF,FF)A(FF,FE,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,4D,4D,4D)A(FF,E5,E5,E5)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EC,EC,EC)A(FF,4D,4D,4D)A(FF,FF,FF,FF)A(FF,FC,FF,FF)A(FF,F5,FF,FF)A(FF,F5,FF,FF)A(FF,FA,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,4D,4D,4D)A(FF,EA,EA,EA)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E8,E8,E8)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,F3,F3,F3)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,F8,F8,F8)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,EE,EE,EE)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,FF,FF,FF)A(FF,F5,F5,F5)A(FF,F5,F5,F5)A(FF,FF,FF,FF)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,CC,CC,CC)A(FF,4D,4D,4D)A(FF,EC,ED,ED)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,80,80,80)A(FF,80,80,80)A(FF,C2,C3,C3)A(FF,CC,CC,CC)A(FF,4D,4D,4D)A(FF,EC,ED,ED)A(FF,E6,E6,E6)A(FF,E6,E6,E6)
				A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,4D,4D,4D)A(FF,80,80,80)A(FF,C2,C3,C3)A(FF,CC,CC,CC)A(FF,4D,4D,4D)A(FF,EC,ED,ED)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,ED,EE,EE)A(FF,4D,4D,4D)A(FF,80,80,80)A(FF,C2,C3,C3)A(FF,CC,CC,CC)A(FF,4D,4D,4D)A(FF,E9,E9,E9)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,ED,EE,EE)A(FF,4D,4D,4D)A(FF,80,80,80)A(FF,BB,BC,BC)A(FF,4D,4D,4D)A(FF,E7,E7,E7)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,ED,EE,EE)A(FF,4D,4D,4D)A(FF,4D,4D,4D)A(FF,C0,C1,C1)A(FF,E9,E9,E9)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,E6,E6,E6)A(FF,EB,EB,EB)A(FF,EA,EA,EA)A(FF,E9,E9,E9)A(FF,E6,E6,E6);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	build_colors (a_ptr: POINTER)
			-- Build `colors'.
		do
			c_colors_0 (a_ptr, 0)
		end

feature {NONE} -- Image data filling.

	fill_memory
			-- Fill image data into memory.
		local
			l_pointer: POINTER
		do
			if attached {EV_PIXEL_BUFFER_IMP} implementation as l_imp then
				l_pointer := l_imp.data_ptr
				if not l_pointer.is_default_pointer then
					build_colors (l_pointer)
					l_imp.unlock
				end
			end
		end

end -- TB_SEARCH_16X15_PBUF
