note
	description: "Pixel buffer that replaces original image file.%
		%The original version of this class has been generated by Image Eiffel Code."

class
	TWEEZER_LEFT_PBUF

inherit
	EV_PIXEL_BUFFER

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization
		do
			make_with_size (29, 29)
			fill_memory
		end

feature {NONE} -- Image data

	c_colors_0 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(00,FF,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,80,80,80)A(F0,80,80,80)A(FF,B3,B3,B3)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(F2,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,B3,B3,B3)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,B3,B3,B3)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(CD,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,B3,B3,B3)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,B3,B3,B3)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)
				A(31,80,80,80)A(FF,B3,B3,B3)A(F5,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,B3,B3,B3)A(00,FF,FF,FF)A(5E,80,80,80)A(F1,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(F7,80,80,80)A(57,80,80,80)A(00,FF,FF,FF)A(20,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(1D,80,80,80)A(00,FF,FF,FF)A(04,80,80,80)A(FF,80,80,80)A(FB,80,80,80)A(FF,FF,FF,FF)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(03,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(52,80,80,80)A(F1,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(2E,80,80,80)A(FF,80,80,80)A(F0,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	c_colors_1 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(4F,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(1C,80,80,80)A(FF,80,80,80)A(FE,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(2B,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(0F,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(1A,80,80,80)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(0C,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(0E,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(0F,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(58,80,80,80)A(FF,00,00,00)A(FF,80,80,80)A(0B,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(1A,80,80,80)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(8D,80,80,80)A(FF,00,00,00)A(FF,80,80,80)A(0E,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(33,80,80,80)A(F7,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(AC,80,80,80)A(FF,00,00,00)A(FF,80,80,80)A(1A,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(67,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(B1,80,80,80)A(FF,00,00,00)A(FF,80,80,80)A(32,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(85,80,80,80)A(FF,00,00,00)A(F6,80,80,80)A(64,80,80,80)A(00,FF,FF,FF)A(00,FF,FF,FF)A(16,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(14,80,80,80)A(00,FF,FF,FF)A(65,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(61,80,80,80)
				A(04,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,80,80,80)A(6C,80,80,80)A(FF,80,80,80)A(FF,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,00,00,00)A(FF,80,80,80)A(FF,FF,00,00)A(FF,FF,00,00)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	c_colors_2 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(FF,FF,00,00)A(FF,FF,00,00)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF)A(00,FF,FF,FF);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	build_colors (a_ptr: POINTER)
			-- Build `colors'.
		do
			c_colors_0 (a_ptr, 0)
			c_colors_1 (a_ptr, 400)
			c_colors_2 (a_ptr, 800)
		end

feature {NONE} -- Image data filling.

	fill_memory
			-- Fill image data into memory.
		local
			l_pointer: POINTER
		do
			if attached {EV_PIXEL_BUFFER_IMP} implementation as l_imp then
				l_pointer := l_imp.data_ptr
				if not l_pointer.is_default_pointer then
					build_colors (l_pointer)
					l_imp.unlock
				end
			end
		end

end -- TWEEZER_LEFT_PBUF
