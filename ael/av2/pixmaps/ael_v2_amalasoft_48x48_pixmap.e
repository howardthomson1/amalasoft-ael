note
	description: "A programmatically created Amalasoft logo pixmap"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_AMALASOFT_48X48_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		do
			set_line_width (2)

			set_foreground_color (colors.dark_red)
			draw_rectangle (0, 0, 48, 48)

			set_foreground_color (colors.dark_blue)
			fill_rectangle (2, 2, 44, 44)

			set_foreground_color (colors.gray)

			-- Draw horizontal line
			draw_segment (2, 12, 45, 12)

			-- Draw vanishing lines
			set_foreground_color (colors.white)
			draw_segment (23, 13, 1, 45)
			draw_segment (23, 13, 12, 45)
			draw_segment (23, 13, 24, 45)
			draw_segment (23, 13, 35, 45)
			draw_segment (23, 13, 45, 45)
		end

--|========================================================================
feature -- Status
--|========================================================================

	pm_width: INTEGER = 48
	pm_height: INTEGER = 48

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make'
--|
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_AMALASOFT_48X48_PIXMAP
