note
	description: "A programmatically created equivalent to the Amalasoft %
	%application pixmap.  This does not require a pixmap file."
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_APPLICATION_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		do
			set_line_width (1)

			set_foreground_color (colors.dark_red)
			draw_rectangle (0, 0, 16, 16)

			set_foreground_color (colors.dark_blue)
			fill_rectangle (1, 1, 14, 14)

			set_foreground_color (colors.gray)

			-- Draw horizontal line
			draw_segment (1, 4, 14, 4)

			-- Draw vanishing lines
			set_foreground_color (colors.white)
			draw_segment (7, 5, 1, 14)
			draw_segment (7, 5, 4, 14)
			draw_segment (7, 5, 7, 14)
			draw_segment (7, 5, 10, 14)
			draw_segment (7, 5, 13, 14)
		end

	pm_width: INTEGER = 16
	pm_height: INTEGER = 16

end -- class AEL_V2_APPLICATION_PIXMAP
