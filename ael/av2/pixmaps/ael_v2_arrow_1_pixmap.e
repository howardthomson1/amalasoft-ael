note
	description: "{
A programmatically created pixmap showing as an arrow (L,R,U,D),
and a numeral 1, denoting shifting left/right one position
}"
	license: "Eiffel Forum License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_ARROW_1_PIXMAP

inherit
	AEL_V2_ARROW_PIXMAP
		redefine
			draw_up_down, draw_left_right
		end

create
	make_with_scale_and_direction,
	make_with_size_and_direction

--|========================================================================
feature -- Intialization
--|========================================================================

	draw_up_down (is_up: BOOLEAN)
			-- Draw an up or down arrow (up if 'is_up')
		do
			Precursor (is_up)
			draw_numeral
		end

	--|--------------------------------------------------------------

	draw_left_right (is_left: BOOLEAN)
			-- Draw a left or right arrow (left if 'is_left')
		do
			Precursor (is_left)
			draw_numeral
		end

	--|--------------------------------------------------------------

	draw_numeral
		local
			fnt: EV_FONT
			af: AEL_V2_FONTS
			xp, yp: INTEGER
		do
			-- Numeral needs to be centered in the space and be scaled 
			-- to size
			-- draw_text (x, y: INTEGER; a_text: READABLE_STRING_GENERAL)
			-- Draw `a_text' with left of baseline at (`x', `y') using `font'.

			create af
			fnt := af.times_new_roman_font (pm_height - 2, True, False)
			set_font (fnt)
			xp := ((pm_width // 2) - 2).max (0)
			yp := pm_height - 1

			set_foreground_color (numeral_color)
			draw_text (xp, yp, "1")
		end

	--|--------------------------------------------------------------

	numeral_color: EV_COLOR
		do
			Result := colors.cyan
		end

end -- class AEL_V2_ARROW_1_PIXMAP
