note
	description: "{
A programmatically created pixmap showing as a left arrow - INSENSITIVE
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_ARROW_LEFT_INS_PIXMAP

inherit
	AEL_V2_ARROW_LEFT_PIXMAP
		redefine
			fg_color
		end

create
	make_with_scale,
	make_with_width_and_height

--|========================================================================
feature -- Status
--|========================================================================

	fg_color: EV_COLOR
		do
			Result := colors.gray_70
		end

end -- class AEL_V2_ARROW_LEFT_INS_PIXMAP
