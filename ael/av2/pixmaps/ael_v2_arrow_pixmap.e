note
	description: "{
A programmatically created pixmap showing as an arrow,
Up, down, left or right
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_ARROW_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make_with_scale_and_direction,
	make_with_size_and_direction

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale_and_direction (v: INTEGER; d: CHARACTER)
			-- Create Current to be 'v' px wide and 'v' px high
		require
			valid_scale: v >= 3
			valid_direction: is_valid_direction (d)
		do
			direction := d.as_upper
			pm_width := v
			pm_height := v
			make
		end

	make_with_size_and_direction (w, h: INTEGER; d: CHARACTER)
		obsolete
			"Use make_with_scale_and_direction instead"
		require
			valid_direction: is_valid_direction (d)
		do
			make_with_scale_and_direction (w, d)
		end

	--|--------------------------------------------------------------

	direction: CHARACTER
			-- Single character denoting direction ('U', 'D', 'L', 'R')
			-- Set to upper in make

	draw
		do
			inspect direction
			when 'U' then
				draw_up_down (True)
			when 'D' then
				draw_up_down (False)
			when 'L' then
				draw_left_right (True)
			when 'R' then
				draw_left_right (False)
			else
			end
		end

	--|--------------------------------------------------------------

	draw_up_down (is_up: BOOLEAN)
			-- Draw an up or down arrow (up if 'is_up')
		local
			ax, bx, ay, by, cx, cy, mw: INTEGER
		do
			--             cx,cy
			--              UP
			--   ax,ay               bx,by
			--
			--   ax,ay               bx,by
			--             DOWN
			--             cx,cy
			--
			mw := pm_width - 1
			ax := 1
			bx := mw - 1
			cx := (pm_width/2).truncated_to_integer
			if is_up then
				ay := mw - 1
				by := mw - 1
				cy := 1
			else
				ay := 1
				by := 1
				cy := mw - 1
			end
			draw_arrow (ax, ay, bx, by, cx, cy)
		end

	--|--------------------------------------------------------------

	draw_left_right (is_left: BOOLEAN)
			-- Draw a left or right arrow (left if 'is_left')
		local
			ax, bx, ay, by, cx, cy, mw, mh: INTEGER
		do
			--   ax,ay
			--          RIGHT   cx,cy
			--   bx,by
			--
			--                 ax,ay
			--   cx,cy  LEFT   
			--                 bx,by
			--
			mw := pm_width - 1
			mh := pm_height - 1
			ay := 1
			by := mh
			cy := pm_height // 2
			if is_left then
				ax := mw
				bx := mw
				cx := 0
			else
				ax := 0
				bx := 0
				cx := mw
			end
			draw_arrow (ax, ay, bx, by, cx, cy)
		end

	draw_arrow (ax, ay, bx, by, cx, cy: INTEGER)
		local
			pt1, pt2, pt3: EV_COORDINATE
		do
			set_foreground_color (fg_color)
    
			create pt1.set (cx, cy)
			create pt2.set (bx, by)
			create pt3.set (ax, ay)

			fill_polygon (<< pt1, pt2, pt3 >>)
			set_foreground_color (hilite_color)
			draw_polyline (<< pt1, pt2, pt3 >>, True)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER
	pm_height: INTEGER

	fg_color: EV_COLOR
		do
			Result := colors.black
		end

	hilite_color: EV_COLOR
		do
			Result := colors.white
		end

--|========================================================================
feature -- Add Comment
--|========================================================================

	is_valid_direction (v: CHARACTER): BOOLEAN
			-- Is 'v' a valid direction?
		do
			inspect v.as_upper
			when 'U', 'D', 'L', 'R' then
				Result := True
			else
			end
		end

end -- class AEL_V2_ARROW_PIXMAP
