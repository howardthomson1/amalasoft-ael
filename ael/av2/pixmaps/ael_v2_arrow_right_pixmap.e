note
	description: "A programmatically created pixmap showing as a right arrow"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_ARROW_RIGHT_PIXMAP

inherit
	AEL_V2_ARROW_PIXMAP
		rename
			make as apm_make
		end

create
	make_with_scale,
	make_with_width_and_height

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale (v: INTEGER)
			-- Create Current to be 'v' px wide and 'v' px high
		require
			valid_scale: v >= 3
		do
			make_with_scale_and_direction (v, 'R')
		end

	make_with_width_and_height (w, h: INTEGER)
			-- Create Current to be 'w' px wide and 'h' px high
		obsolete
			"Use make_with_scale instead"
		require
			is_square: w = h
		do
			make_with_scale (w)
		end

end -- class AEL_V2_ARROW_RIGHT_PIXMAP
