note
	description: "{
A programmatically created application (main window) pixmap
for applications generated by the Amalasoft Vision Wizard
or anyone else who wants to use it
This does NOT require a pixmap file!
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2012/08/01 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_BOXES_APP_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		do
			set_line_width (1)

			-- Fill background
			set_foreground_color (colors.white)
			fill_rectangle (1, 1, 14, 14)

			-- Draw border
			set_foreground_color (colors.dark_blue)
			draw_rectangle (0, 0, 15, 15)

			-- Draw green box
			set_foreground_color (colors.dark_green)
			draw_rectangle (3, 3, 10, 10)
			draw_rectangle (4, 4, 8, 8)

			-- Draw red square
			set_foreground_color (colors.red)
			fill_rectangle (6, 6, 4, 4)
		end

	pm_width: INTEGER = 16
	pm_height: INTEGER = 16

end -- class AEL_V2_BOXES_APP_PIXMAPb
