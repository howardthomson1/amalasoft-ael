note
	description: "A programmatically created pixmap for use with a calendar btn"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_CALENDAR_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_line_width (1)

			set_foreground_color (navy)
			fill_rectangle (1, 1, 23, 16)

			set_foreground_color (light_sky_blue)
			fill_rectangle (5, 2, 17, 12)
			fill_rectangle (5, 14, 6, 2)

			set_foreground_color (colors.white)
			fill_rectangle (2, 2, 2, 2)
			fill_rectangle (5, 2, 2, 2)
			fill_rectangle (11, 14, 2, 2)
			fill_rectangle (14, 14, 2, 2)
			fill_rectangle (17, 14, 2, 2)
			fill_rectangle (20, 14, 2, 2)

			set_foreground_color (colors.red)
			fill_rectangle (2, 5, 2, 2)
			fill_rectangle (2, 8, 2, 2)
			fill_rectangle (2, 11, 2, 2)
			fill_rectangle (2, 14, 2, 2)

			set_foreground_color (navy)
			draw_segment (7, 1, 7, 16)
			draw_segment (10, 1, 10, 16)
			draw_segment (13, 1, 13, 16)
			draw_segment (16, 1, 16, 16)
			draw_segment (19, 1, 19, 16)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 24
	pm_height: INTEGER = 18

	navy: EV_COLOR
		once
			create Result.make_with_rgb(0, 0, 0.5)
		end

	light_sky_blue: EV_COLOR
		once
			create Result.make_with_rgb (.5294, .8078, .9804)
		end

end -- class AEL_V2_CALENDAR_PIXMAP
