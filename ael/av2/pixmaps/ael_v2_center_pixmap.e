note
	description: "A programmatically created pixmap to associate with a 'center' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_CENTER_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_foreground_color (colors.red)
			draw_ellipse (2, 2, 11, 11)
			draw_ellipse (4, 4, 7, 7)
			set_foreground_color (colors.black)
			draw_segment (7, 0, 7, 14)
			draw_segment (0, 7, 14, 7)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 15
	pm_height: INTEGER = 15

end -- class AEL_V2_CENTER_PIXMAP
