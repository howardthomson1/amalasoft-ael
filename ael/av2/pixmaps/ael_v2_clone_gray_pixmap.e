note
	description: "A programmatically created pixmap to associate with a 'clone' function - gray form"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_CLONE_GRAY_PIXMAP

inherit
	AEL_V2_CLONE_PIXMAP
		redefine
			edge_color, fill_color, lines_color
		end

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	edge_color: EV_COLOR
		once
			Result := colors.black
		end

	fill_color: EV_COLOR
		once
			Result := colors.gray
		end

	lines_color: EV_COLOR
		once
			Result := colors.dark_gray
		end

end -- class AEL_V2_CLONE_GRAY_PIXMAP
