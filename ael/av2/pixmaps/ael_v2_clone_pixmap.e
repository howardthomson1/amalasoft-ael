note
	description: "A programmatically created pixmap to associate with a 'clone' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_CLONE_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_line_width (1)

			-- Draw original file
			set_foreground_color (colors.black)
			draw_rectangle (0, 0, 11, 12)
			set_foreground_color (colors.gray)
			fill_rectangle (1, 1, 10, 11)
			set_foreground_color (colors.black)
			draw_segment (8, 0, 11, 3)
			draw_segment (8, 0, 8, 3)
			draw_segment (8, 3, 11, 3)

			-- Clean up corner
			set_foreground_color (colors.default_background_color)
			draw_segment (9, 0, 11, 2)
			draw_segment (10, 0, 11, 1)
			draw_point (11, 0)

			-- Set upper left pixel to bg
			set_foreground_color (colors.default_background_color)
			draw_point (0, 0)

			-- Draw clone file
			set_foreground_color (edge_color)
			draw_rectangle (3, 2, 14, 14)
			set_foreground_color (fill_color)
			fill_rectangle (4, 3, 13, 13)
			set_foreground_color (edge_color)
			draw_segment (11, 2, 14, 5)
			draw_segment (11, 2, 11, 5)
			draw_segment (11, 5, 14, 5)

			-- Clean up corner
			set_foreground_color (colors.default_background_color)
			draw_segment (12, 0, 14, 2)
			draw_segment (13, 0, 14, 1)
			draw_point (14, 0)

			-- Add text lines
			set_foreground_color (lines_color)
			draw_segment (6, 6, 9, 6)
			draw_segment (6, 8, 13, 8)
			draw_segment (6, 10, 13, 10)
			draw_segment (6, 12, 13, 12)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

	edge_color: EV_COLOR
		once
			create Result.make_with_rgb (.6275, 0.1294, 0.1687)
		end

	fill_color: EV_COLOR
		once
			create Result.make_with_rgb (.9686, 0.9686, 0.8667)
		end

	lines_color: EV_COLOR
		once
			create Result.make_with_rgb (.5647, 0.73725, 0.5412)
		end

end -- class AEL_V2_CLONE_PIXMAP
