note
	description: "A programmatically created pixmap to associate with a 'delete' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_DELETE_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_line_width (4)
			set_foreground_color (create {EV_COLOR}.make_with_rgb(1,0.7137,0.7569))
			draw_segment (2, 2, 13, 12)
			draw_segment (3, 13, 12, 3)

			set_line_width (2)
			set_foreground_color (colors.red)
			draw_segment (2, 2, 13, 12)
			draw_segment (2, 14, 13, 2)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

end -- class AEL_V2_DELETE_PIXMAP
