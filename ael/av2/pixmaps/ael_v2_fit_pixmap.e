note
	description: "A programmatically created pixmap to associate with a 'fit' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_FIT_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_foreground_color (colors.red)
			draw_rectangle (2, 2, 11, 11)
			draw_rectangle (3, 3, 9, 9)
			-- Draw arrows
			set_foreground_color (colors.black)
			draw_segment (0, 0, 5, 5)
			draw_segment (0, 1, 4, 5)
			draw_segment (1, 0, 5, 4)
			draw_segment (1, 5, 5, 5)
			draw_segment (5, 1, 5, 5)

			draw_segment (14, 0, 9, 5)
			draw_segment (14, 1, 10, 5)
			draw_segment (13, 0, 9, 4)
			draw_segment (9, 1, 9, 5)
			draw_segment (9, 5, 13, 5)

			draw_segment (14, 14, 9, 9)
			draw_segment (14, 13, 10, 9)
			draw_segment (13, 14, 9, 9)
			draw_segment (9, 9, 9, 13)
			draw_segment (9, 9, 13, 9)

			draw_segment (0, 14, 5, 9)
			draw_segment (0, 13, 4, 9)
			draw_segment (1, 14, 5, 10)
			draw_segment (1, 9, 5, 9)
			draw_segment (5, 13, 5, 9)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 15
	pm_height: INTEGER = 15

end -- class AEL_V2_FIT_PIXMAP
