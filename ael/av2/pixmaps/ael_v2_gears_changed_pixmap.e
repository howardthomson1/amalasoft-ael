note
	description: "{
A programmatically created pixmap to associate with a
'generate' function
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_GEARS_CHANGED_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP
		redefine
			colors
		end

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_line_width (1)

			-- Draw left gear
			set_foreground_color (colors.gold)
			fill_ellipse (2, 2, 8, 8)
			set_foreground_color (colors.dark_brown)
			draw_ellipse (1, 1, 10, 10)
			-- Draw hub
			set_foreground_color (colors.black)
			draw_ellipse (4, 4, 3, 3)

			-- Draw right gear
			set_foreground_color (colors.gold)
			fill_ellipse (7, 7, 8, 8)
			set_foreground_color (colors.dark_brown)
			draw_ellipse (5, 5, 10, 10)
			-- Draw hub
			set_foreground_color (colors.black)
			draw_ellipse (9, 9, 3, 3)
			-- Draw red star
			set_foreground_color (colors.red)
			fill_rectangle (8, 3, 7, 3)
			fill_rectangle (10, 1, 3, 7)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15
  
	colors: AEL_V2_COLORS
		once
			create Result
		end

end -- class AEL_V2_GEARS_CHANGED_PIXMAP
