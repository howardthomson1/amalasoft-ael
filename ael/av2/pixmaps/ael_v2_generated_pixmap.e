note
	description: "Pixmaps generated programmatically, without the use of files"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 002$"
	howto: "{
    To use this class, create a child class.  Implement the pm_width
    and pm_height features as constants representing the widht and
    height of the pixmap.
    Implement the draw routine by calling the various drawing procedures
    in EV_DRAWABLE (e.g. draw_segment) to create the image you want.
}"

deferred class AEL_V2_GENERATED_PIXMAP

inherit
	EV_PIXMAP
		rename
			make_with_size as make_pm_with_size
		end
	AEL_V2_CONSTANTS
		undefine
			default_create, copy, is_equal
		end

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			make_pm_with_size (pm_width, pm_height)
			size_initialized := True
			init_background
			draw
		end

	--|--------------------------------------------------------------

	init_background
		do
			set_background_color (colors.default_background_color)
			clear
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		deferred
		end

--|========================================================================
feature -- Attributes
--|========================================================================

	pm_width: INTEGER
		deferred
		end

	pm_height: INTEGER
		deferred
		end

	size_initialized: BOOLEAN
			-- Has pixmap size been initialized?

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant
	sized: (size_initialized) implies height = pm_height and width = pm_width

end -- class AEL_V2_GENERATED_PIXMAP
