note
	description: "{
A programmatically created pixmap showing as a right arrow and bar,
denoting "go to end" or "go to finish"
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_GOTO_END_PIXMAP

inherit
	AEL_V2_GOTO_PIXMAP

create
	make_with_scale

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale (v: INTEGER)
			-- Create Current to be 'v' px wide and 'v' px high
		do
			make_with_scale_and_direction (v, False)
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_GOTO_END_PIXMAP
