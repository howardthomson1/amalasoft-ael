note
	description: "{
A programmatically created pixmap showing as an arrow and bar,
denoting "go to start" (left) or "go to finish"(right)
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_GOTO_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make_with_scale_and_direction

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale_and_direction (v: INTEGER; lf: BOOLEAN)
			-- Create Current to be 'v' px wide and 'v' px high
			-- If 'lf' then point left, else point right
		do
			is_left := lf
			pm_width := v
			pm_height := v
			make
		end

	--|--------------------------------------------------------------

	draw
		local
			ax, bx, ay, by, cx, cy, dx, mw, mh: INTEGER
			bsw, asw, linew: INTEGER
		do
			-- Right form:
			--   ax,ay       dx,0
			--         cx,cy |
			--   bx,by       dx,h
			--
			-- Left form:
			--   dx,0        ax,ay
			--       | cx,cy
			--   dx,h        bx,by
			--
			-- Width is divided into arrow space (not aerospace) and bar 
			-- space.  Bar is at edge (left if left) separated from the 
			-- arrow space by a single pixel column

			linew := (pm_width // 6).max (1)
			bsw := linew + 1
			asw := pm_width - bsw
			mw := pm_width - 1
			mh := pm_height - 1
			ay := 1
			by := mh
			cy := pm_height // 2

			if is_left then
				ax := mw
				bx := mw
				cx := bsw
				dx := 0
			else
				ax := 0
				bx := 0
				cx := pm_width - bsw
				dx := pm_width - linew
			end
			draw_arrow (ax, ay, bx, by, cx, cy)
			draw_bar (dx, linew)
		end

	draw_arrow (ax, ay, bx, by, cx, cy: INTEGER)
		local
			pt1, pt2, pt3: EV_COORDINATE
		do
			set_foreground_color (fg_color)
    
			create pt1.set (cx, cy)
			create pt2.set (bx, by)
			create pt3.set (ax, ay)

			fill_polygon (<< pt1, pt2, pt3 >>)
		end

	--|--------------------------------------------------------------

	draw_bar (dx, linew: INTEGER)
			-- Draw vertical bar at xpos 'dx', with thickness 'linew'
		do
			set_foreground_color (bar_color)
			fill_rectangle (dx, 0, linew, pm_height - 1)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_left: BOOLEAN
	pm_width: INTEGER
	pm_height: INTEGER

	fg_color: EV_COLOR
		do
			Result := colors.black
		end

	bar_color: EV_COLOR
		do
			Result := fg_color
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_GOTO_PIXMAP
