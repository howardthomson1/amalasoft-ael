note
	description: "{
A programmatically created square pixmap showing as either highlighted
(perhaps implying selection state), or not
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_HILITE_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make_with_values

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_values (sz: INTEGER; sf: BOOLEAN; c: EV_COLOR)
			-- Create Current to be 'sz' px square, with a background 
			-- color 'c'.
			-- If selected (highlighted), show as such, else, not
		require
			large_enough: sz >= 6
		do
			pm_width := sz
			pm_height := sz
			is_selected := sf
			bg_color := c
			make
		end

	--|--------------------------------------------------------------

	draw
		local
			mw, mh: INTEGER
			--linew: INTEGER
		do
			-- Is a square
			-- If not selected/highlighted, draw a minimal (1 px) border
			-- If selected, draw a double border, with white and black
			-- for contrast (and to handle white or black bg colors)

			mw := pm_width - 1
			mh := pm_height - 1

			set_foreground_color (bg_color)
			fill_rectangle (0, 0, mw, mh)

			if is_selected then
				-- Draw double border, white outside, black inside
				-- Draw outer border in white
--TODO maybe use old-school shadow effect?
				set_line_width (1)
				set_foreground_color (colors.white)
				draw_rectangle (0, 0, pm_width, pm_height)
				-- Draw inner border in black
				set_line_width (1)
				set_foreground_color (colors.black)
				draw_rectangle (1, 1, mw, mh)
			else
				-- Draw outer border only
				set_line_width (1)
				set_foreground_color (colors.gray_40)
				draw_rectangle (0, 0, pm_width, pm_height)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_highlighted
			-- Redraw Current, but in highlighted state
		do
			is_selected := True
			draw
		end

	disable_highlighted
			-- Redraw Current, but in un-highlighted state
		do
			is_selected := False
			draw
		end

--|========================================================================
feature -- Status
--|========================================================================

	pm_width: INTEGER
	pm_height: INTEGER
	is_selected: BOOLEAN
	bg_color: EV_COLOR

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_HILITE_PIXMAP
