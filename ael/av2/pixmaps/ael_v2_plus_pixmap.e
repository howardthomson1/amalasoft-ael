note
	description: "{
A programmatically created pixmap showing as a plus sign,
denoting "add one here"
While scale is configurable, even sizes less than 20x20 can seem
unbalanced
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_PLUS_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make_with_scale

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale (v: INTEGER)
			-- Create Current to be 'v' px wide and 'v' px high
		require
			large_enough: v >= 5
		do
			pm_width := v
			pm_height := v
			make
		end

	--|--------------------------------------------------------------

	draw
		local
			vx, vy1, vy2, hx1, hx2, hy: INTEGER
			mw, mh, linew: INTEGER
		do
			--        vx,vy1
			--   hx1,hy    hx2,hy
			--        vx,vy2
			linew := (pm_width // 5).max (1)
			mw := pm_width - 1
			mh := pm_height - 1
			vx := mw // 2
			hy := mh // 2
			vy1 := 0
			vy2 := mh
			hx1 := 0
			hx2 := mw
			set_foreground_color (fg_color)
			set_line_width (linew)
			draw_segment (vx, vy1, vx, vy2)
			draw_segment (hx1, hy, hx2, hy)
		end

--|========================================================================
feature -- Status
--|========================================================================

	pm_width: INTEGER
	pm_height: INTEGER

	fg_color: EV_COLOR
		do
			Result := colors.black
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_PLUS_PIXMAP
