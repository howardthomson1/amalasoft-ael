note
	description: "A programmatically created pixmap to associate with a 'print' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_PRINT_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			-- 30% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (0.3, 0.3, 0.3))
			draw_segment (5, 1, 13, 1)
			draw_point (4, 2)
			draw_point (13, 2)
			draw_point (4, 3)
			draw_segment (6, 3, 10, 3)
			draw_point (12, 3)
			draw_point (3, 4)
			draw_point (12, 4)
			draw_point (3, 5)
			draw_segment (5, 5, 9, 5)
			draw_segment (11, 5, 14, 5)
			draw_point (2, 6)
			draw_point (11, 6)
			draw_point (13, 6)
			draw_point (15, 6)
			draw_segment (1, 7, 10, 7)
			draw_point (12, 7)
			draw_segment (14, 7, 15, 7)
			fill_rectangle (0, 8, 13, 5)
			draw_point (13, 8)
			draw_point (15, 8)
			draw_point (15, 9)
			draw_point (14, 10)
			draw_segment (12, 11, 14, 11)
			draw_point (14, 12)
			draw_point (1, 13)
			draw_point (11, 13)
			draw_point (13, 13)
			draw_segment (2, 14, 12, 14)

			set_foreground_color (colors.white)
			draw_segment (5, 2, 12, 2)
			draw_point (5, 3)
			draw_point (11, 3)
			draw_segment (4, 4, 11, 4)
			draw_point (4, 5)
			draw_point (10, 5)
			draw_segment (3, 6, 10, 6)

			-- 83% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (.83, .83, .83))
			draw_point (12, 6)
			draw_point (14, 6)
			draw_point (11, 7)
			draw_point (13, 7)
			draw_segment (1, 8, 10, 8)
			draw_point (12, 8)
			draw_point (14, 8)
			draw_segment (13, 9, 14, 9)
			draw_segment (1, 10, 11, 10)
			draw_point (13, 10)
			draw_segment (1, 11, 11, 11)
			draw_point (13, 12)
			draw_segment (2, 13, 10, 13)
			draw_point (12, 13)

			-- 65% white
			set_foreground_color (create {EV_COLOR}.make_with_rgb (.65, .65, .65))
			draw_segment (7, 10, 9, 10)

			-- Light yellow
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 1, .4))
			draw_segment (7, 11, 9, 11)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15
  
end -- class AEL_V2_PRINT_PIXMAP
