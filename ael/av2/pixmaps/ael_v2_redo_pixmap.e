note
	description: "A programmatically created pixmap to associate with a 'redo' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_REDO_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			-- Draw arrowhead
			set_foreground_color (colors.black)
			fill_rectangle (8, 6, 5, 5)
			set_foreground_color (colors.default_background_color)
			draw_segment (8, 9, 11, 6)
			draw_segment (8, 8, 10, 6)
			draw_segment (8, 7, 9, 6)

			-- Draw arrow arc
			set_foreground_color (colors.black)
			draw_point (9, 7)
			draw_segment (7, 6, 8, 6)
			draw_segment (3, 5, 6, 5)
			draw_point (2, 6)
			draw_segment (1, 7, 1, 9)
			draw_segment (2, 10, 2, 11)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

end -- class AEL_V2_REDO_PIXMAP
