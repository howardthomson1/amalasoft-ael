note
	description: "{
A programmatically created pixmap showing as a circular arrow,
denoting "rotate clockwise"
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_ROTATE_CW_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make_with_scale

--|========================================================================
feature -- Intialization
--|========================================================================

	make_with_scale (v: INTEGER)
			-- Create Current to be 'v' px wide and 'v' px high
			-- If 'lf' then point left, else point right
		require
			large_enough: v >= 15
		do
			pm_width := v
			pm_height := v
			make
		end

	--|--------------------------------------------------------------

	draw
		local
			ax, ay, aw, ah, cw, mw, mh: INTEGER
			sy1, sy2, sy3, sy4, sx1, sx2, sx3, sx4: INTEGER
			hst1, hst2, hst3, hst4: INTEGER
			vst1, vst2, vst3, vst4: INTEGER
		do
			-- ------------x------------
			-- ------------xxx----------
			-- --------xxx-xxxxx--------
			-- --------xxx-xxxxxx-------
			-- --------xxx-xxxxx--------
			-- ---xxx------xxx----------
			-- ---xxx------x------------
			-- ---xxx-------------------
			-- -------------------------
			-- -------------------------
			-- -xxx-----------------xxx-
			-- -xxx-----------------xxx-
			-- -xxx-----------------xxx-
			-- -------------------------
			-- -------------------------
			-- ---xxx-------------xxx---
			-- ---xxx-------------xxx---
			-- ---xxx-------------xxx---
			-- -------------------------
			-- --------xxx---xxx--------
			-- --------xxx---xxx--------
			-- --------xxx---xxx--------
			-- -------------------------
			-- -------------------------
			-- 1. Fill a black circle centered just below center of pixmap
			-- 2. Set fg color to bg color, then fill a smaller circle 
			--    centered on the larger one
			-- 3. Still w/ fg=bg, fill rectangle in top-right quadrant
			-- 4. Still w/ fg=bg, draw separator lines between rows and 
			--    cols
			-- 5. Set fg to black; draw arrow

			mw := ((pm_width-1) / 2.0).rounded
			mh := ((pm_height-1) / 2.0).rounded

			-- 1. Fill a black circle centered just below center of pixmap
			if pm_height < 17 then
				ax := 1
				ay := 1
				cw := 2
				hst1 := 0
				hst2 := 1
				hst3 := 1
				hst4 := 1
				vst1 := 1
				vst2 := 1
				vst3 := 1
				sy2 := (pm_height / 3.0).rounded
				sx1 := ax + cw + cw - 1
				sx2 := sx1 + vst1 + cw
				sx3 := sx2 + vst2 + cw
			else
				ax := 1
				ay := 3
				cw := 3
				hst1 := 1
				hst2 := 2
				hst3 := 2
				hst4 := 1
				vst1 := 2
				vst2 := 3
				vst3 := 2
				sy1 := ay + cw
				sy2 := mh - cw - 1
				sy3 := mh + cw + 1
				sy4 := (pm_height - 1) - cw
				sx1 := ax + cw + cw - 1
				sx2 := sx1 + vst1 + cw
				sx3 := sx2 + vst2 + cw
			end
			aw := pm_width - (ax * 2)
			ah := pm_height - (ay + 1)
			set_foreground_color (fg_color)
			fill_ellipse (ax, ay, aw, ah)

			-- 2. Set fg color to bg color, then fill a smaller circle 
			--    centered on the larger one
			set_foreground_color (background_color)
			ax := ax + cw
			ay := ay + cw
			aw := aw - (cw * 2)
			ah := ah - (cw * 2)
			fill_ellipse (ax, ay, aw, ah)

			-- 3. Still w/ fg=bg, fill rectangle in top-right quadrant
			fill_rectangle (mw - 1, 0, mw,  mh - 1)

			-- 4. Still w/ fg=bg, draw separator lines between rows and 
			--    cols
			if hst1 /= 0 then
				fill_rectangle (0, sy1, pm_width, hst1)
			end
			if hst2 /= 0 then
				fill_rectangle (0, sy2, pm_width, hst2)
			end
			if hst3 /= 0 then
				fill_rectangle (0, sy3, pm_width, hst3)
			end
			if hst4 /= 0 then
				fill_rectangle (0, sy4, pm_width, hst4)
			end
			if vst1 /= 0 then
				fill_rectangle (sx1, 0, vst1, pm_height)
			end
			if vst2 /= 0 then
				fill_rectangle (sx2, 0, vst2, pm_height)
			end
			if vst3 /= 0 then
				fill_rectangle (sx3, 0, vst3, pm_height)
			end
			if vst4 /= 0 then
				fill_rectangle (sx4, 0, vst4, pm_height)
			end

			-- Arrow begins at midpoint and is >= 24% of width, <= 32% ht
			aw := ((pm_width-1) * 0.24).rounded
			ah := ((pm_height-1) * 0.32).rounded
			draw_arrow (mw + 1, 1, aw, ah)
		end

	draw_arrow (xp, yp, w, h: INTEGER)
		local
			p1, p2, p3: EV_COORDINATE
		do
			create p1.make (xp, yp)
			create p2.make (xp, yp + h)
			create p3.make (xp + w, yp + (h / 2).rounded)
			set_foreground_color (fg_color)
			fill_polygon ({ARRAY[EV_COORDINATE]}<< p1, p2, p3 >>)
		end

--|========================================================================
feature -- Status
--|========================================================================

	pm_width: INTEGER
	pm_height: INTEGER

	fg_color: EV_COLOR
			-- For insensitive, use gray 50 or so
		do
			Result := colors.black
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_ROTATE_CW_PIXMAP
