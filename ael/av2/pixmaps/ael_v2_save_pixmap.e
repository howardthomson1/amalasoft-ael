note
	description: "A programmatically created pixmap to associate with a 'save' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_SAVE_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_foreground_color (colors.black)
			fill_rectangle (1, 1, 14, 14)

			set_foreground_color (colors.default_background_color)
			fill_rectangle (4, 2, 8, 6)
			draw_point (13, 2)
			draw_point (1, 14)
			fill_rectangle (10, 11, 2, 3)

			set_foreground_color (colors.dark_yellow)
			set_line_width (1)
			draw_segment (2, 2, 2, 13)
			draw_segment (3, 7, 3, 13)
			draw_segment (13, 4, 13, 13)
			draw_segment (4, 9, 12, 9)
			draw_point (12, 8)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

end -- class AEL_V2_SAVE_PIXMAP
