note
	description: "{
A programmatically created pixmap showing as a left arrow and bar,
denoting 'shift left' INSENSITIVE
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_SHIFT_LEFT_INS_PIXMAP

inherit
	AEL_V2_SHIFT_LEFT_PIXMAP
		redefine
			fg_color, bar_color
		end

create
	make_with_scale

--|========================================================================
feature -- Status
--|========================================================================

	fg_color: EV_COLOR
		do
			Result := colors.gray_70
		end

	bar_color: EV_COLOR
		do
			Result := colors.gray_50
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_SHIFT_LEFT_INS_PIXMAP
