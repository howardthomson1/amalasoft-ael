note
	description: "{
A programmatically created pixmap showing as a right arrow and bar,
denoting 'shift right'
}"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "Amalasoft Corporation"
	copyright: "Copyright (c) 2019 Amalasoft Copororation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_SHIFT_RIGHT_PIXMAP

inherit
	AEL_V2_GOTO_END_PIXMAP
		redefine
			draw_bar, bar_color
		end

create
	make_with_scale

--|========================================================================
feature -- Intialization
--|========================================================================

	draw_bar (dx, linew: INTEGER)
			-- Draw vertical bar at xpos 'dx', with thickness 'linew'
		local
			xp: INTEGER
		do
			set_foreground_color (bar_color)
			xp := (pm_width // 2) - 1
			fill_rectangle (xp, 0, linew, pm_height - 1)
		end

--|========================================================================
feature -- Status
--|========================================================================

	bar_color: EV_COLOR
		do
			Result := colors.cyan
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Mar-2019
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is instantiated by calling its make routine.
--| It can then be treated like any other Eiffel pixmap.
--|----------------------------------------------------------------------

end -- class AEL_V2_SHIFT_RIGHT_PIXMAP
