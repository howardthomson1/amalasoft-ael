note
	description: "A programmatically created pixmap representing a help feature in toolbar scale"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2008, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_TB_HELP_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		do
			set_background_color (colors.default_background_color)
			clear
			set_line_width (1)

			-- Draw the help box
			set_foreground_color (colors.black)
			draw_rectangle (0, 0, 16, 12)
			-- Pale yellow
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 1, .7))
			fill_rectangle (1, 1, 14, 10)
			-- Light yellow
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 1, .4))
			draw_segment (1, 2, 1, 9)
			draw_point (2, 1)
			draw_point (2, 10)
			draw_point (13, 1)
			draw_segment (14, 2, 14, 10)
			-- Pale yellow
			set_foreground_color (create {EV_COLOR}.make_with_rgb (1, 1, .7))
			draw_segment (11, 11, 13, 13)
			draw_segment (12, 11, 13, 12)
			set_foreground_color (colors.black)
			draw_segment (11, 12, 13, 14)
			draw_segment (14, 12, 14, 14)

			-- Clean up the corners
			set_foreground_color (colors.default_background_color)
			draw_point (0, 0)
			draw_point (0, 1)
			draw_point (1, 0)
			draw_point (14, 0)
			draw_point (15, 0)
			draw_point (15, 1)
			draw_point (0, 10)
			draw_point (0, 11)
			draw_point (1, 11)
			draw_point (15, 11)
			set_foreground_color (colors.black)
			draw_point (1, 1)
			draw_point (14, 1)
			draw_point (1, 10)

			-- Draw the question mark
			set_foreground_color (colors.black)
			fill_rectangle (5, 2, 2, 2)
			draw_point (7, 2)
			fill_rectangle (8, 2, 2, 4)
			fill_rectangle (7, 5, 2, 2)
			fill_rectangle (7, 8, 2, 2)
		end

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

end -- class AEL_V2_TB_HELP_PIXMAP
