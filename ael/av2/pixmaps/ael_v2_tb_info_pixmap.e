note
	description: "A programmatically created pixmap to associate with an 'Info' function"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2009, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_TB_INFO_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Intialization
--|========================================================================

	draw
		do
			set_foreground_color (light_gray)
			draw_ellipse (0, 0, 15, 15)
			set_foreground_color (colors.blue)
			fill_ellipse (1, 1, 13, 13)
			set_foreground_color (colors.black)
			draw_ellipse (1, 1, 13, 13)
			set_foreground_color (colors.white)
			draw_rectangle (7, 3, 2, 2)
			draw_rectangle (7, 6, 2, 6)
			draw_point (6, 6)
			draw_point (6, 11)
			draw_point (9, 11)
		end

	--|--------------------------------------------------------------

	pm_width: INTEGER = 15
	pm_height: INTEGER = 15

	light_gray: EV_COLOR
		do
			create Result.make_with_rgb (0.9, 0.9, 0.9)
		end

end -- class AEL_V2_TB_INFO_PIXMAP
