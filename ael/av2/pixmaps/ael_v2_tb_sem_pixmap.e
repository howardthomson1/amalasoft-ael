note
	description: "A programmatically created pixmap representing a semaphore flag, at toolbar scale"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2019, Amalasoft"
	date: "$Date: 2008/04/21 $"
	revision: "$Revision: 001$"
	howto: "{
    This class is instantiated by calling its make routine.
    It can then be treated like any other Eiffel pixmap.
}"

class AEL_V2_TB_SEM_PIXMAP

inherit
	AEL_V2_GENERATED_PIXMAP

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	draw
		local
			p1, p2, p3: EV_COORDINATE
		do
			set_background_color (colors.extra_pale_blue)
			clear

			-- Draw the staff
			set_line_width (2)
			set_foreground_color (colors.black)
			draw_segment (12, 1, 12, 14)
			-- Yellow flag bg
			set_foreground_color (colors.yellow)
			fill_rectangle (4, 1, 8, 8)
			-- flag outline
			set_line_width (1)
			set_foreground_color (colors.black)
			draw_rectangle (4, 1, 8, 8)
			-- Red upper right overlay on flag
			set_foreground_color (colors.red)
			create p1.make (4, 1)
			create p2.make (11, 1)
			create p3.make (11, 8)
			fill_polygon (
				{ARRAY[EV_COORDINATE]} << p1, p2, p3 >>)
		end

	pm_width: INTEGER = 16
	pm_height: INTEGER = 15

end -- class AEL_V2_TB_SEM_PIXMAP
