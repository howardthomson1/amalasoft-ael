note
	description: "{
A quasi-button with no text (separable)
implemented as a cell and not as a button and displaying a color
N.B. the select behavior is faked, with button up
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2020 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2020/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_COLOR_QBUTTON

inherit
 	AEL_V2S_QBUTTON
		redefine
			make
		end

create
	make, make_with_size, make_with_color

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_color (v: EV_COLOR)
			-- Create Current with background color 'v'
		do
			make
			set_background_color (v)
		end

	--|--------------------------------------------------------------

	make
			-- Create `Current'
		do
			Precursor
			set_background_color (ael_colors.black)
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================

	color: EV_COLOR
			-- Color presently display by Current
		do
			Result := background_color
		end

	color_rgb: STRING
			-- Color presently display by Current,
			-- as Hex RGB value (upper case, no leading '0x')
		do
			Result := ael_colors.color_to_rgb_hex (color, False, True)
		ensure
			valid: is_valid_rgb (Result)
		end

	is_valid_rgb (v: STRING): BOOLEAN
			-- Is 'v' a valid RGB hex value?
		do
			Result := ael_colors.is_valid_rgb_hex (v)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_color (v: EV_COLOR)
		do
			set_background_color (v)
		end

	set_rgb_color (v: STRING)
			-- Set current color to RGB hex value
		require
			valid: is_valid_rgb (v)
		local
			tc: EV_COLOR
		do
			tc := ael_colors.rgb_hex_string_to_color (v)
			set_color (tc)
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	ael_colors: AEL_V2_COLORS
		once
			create Result
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make_with_pixmaps'.
--|----------------------------------------------------------------------

end -- class AEL_V2S_QQBUTTON
