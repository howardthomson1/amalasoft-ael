note
	description: "{
A horizontal container widget
A separable version of AEL_V2_HORIZONTAL_BOX
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_HORIZONTAL_BOX

inherit
	EV_HORIZONTAL_BOX
      rename
			implementation as hb_implementation_attr
		undefine
			create_interface_objects
		redefine
			create_implementation
		select
			hb_implementation_attr
		end
 	AEL_V2S_WIDGET
 		redefine
 			implementation,
 			create_interface_objects
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
--			Precursor {EV_HORIZONTAL_BOX}
			Precursor {AEL_V2S_WIDGET}
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: like hb_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := hb_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_HORIZONTAL_BOX}
			implementation_attr := hb_implementation_attr
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-May-2019
--|     Created original module (for Eiffel 18.1)
--|     Adapted from AEL_V2_VERTICAL_BOX
--|----------------------------------------------------------------------

end -- class AEL_V2S_HORIZONTAL_BOX
