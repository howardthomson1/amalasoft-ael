note
	description: "{
Compound widget with a quasi spin button and a label (separable)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_LABELED_QSPINNER

inherit
	AEL_V2S_LABELED_WIDGET
		rename
			widget as spinner
		redefine
			create_interface_objects,
			create_widget, initialize_interface_actions, add_widget,
			initialize_interface_values, update_widget_rendition,
			enable_sensitive, disable_sensitive, post_initialize
		end

create
	make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (r1, r2: INTEGER; t: STRING; pf, cf: BOOLEAN)
			-- Create a new widget with a value range of r1 to r2 and a label
			-- of 't'.
			-- If position flag 'pf' is True, place the label to the left of
			-- the assembly, else place the label to the right.
			-- If 'cf' then btns (controls) are at left of text, else right
		require
			range_valid:  is_valid_range (r1, r2)
			label_exists: t /= Void and then not t.is_empty
		do
			step_size := 1
			minimum_value := r1
			maximum_value := r2
			controls_on_left := cf

			notifcations_suspended := True
			make_with_label (t, pf)

			set_gutter_width (1)
			set_value_range (r1, r2)
		end

	create_interface_objects
		do
			Precursor
			create change_actions
			create value_range.make (0, 100)
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create spinner_vb
			create spinner
			create up_btn.make_with_pixmaps (
				create {AEL_V2_ARROW_UP_PIXMAP}.make_with_scale (17),
				create {AEL_V2_ARROW_UP_INS_PIXMAP}.make_with_scale (17))
			create dn_btn.make_with_pixmaps (
				create {AEL_V2_ARROW_DOWN_PIXMAP}.make_with_scale (17),
				create {AEL_V2_ARROW_DOWN_INS_PIXMAP}.make_with_scale (17))
		end

	--|--------------------------------------------------------------

	initialize_interface_values
		do
			if has_valid_range then
				update_spinner_values
			end
			spinner.set_minimum_width (default_spinner_width)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			up_btn.select_actions.extend (agent on_up_select)
			dn_btn.select_actions.extend (agent on_down_select)
			spinner.return_actions.extend (agent on_value_change)
			Precursor
		end

	--|--------------------------------------------------------------

	add_widget
			-- Add widget to Current
		local
			vb: EV_VERTICAL_BOX
			--hb: EV_HORIZONTAL_BOX
			tc: EV_CELL
		do
--			if label_on_left then
			if not controls_on_left then
				-- Put buttons to right of text
				extend (spinner_vb)
				create tc
				spinner_vb.extend (tc)
				spinner_vb.extend (spinner)
				create tc
				spinner_vb.extend (tc)
			end

			create vb
			extend (vb)
			create tc
			vb.extend (tc)
			vb.extend (dn_btn)
			vb.disable_item_expand (dn_btn)
			dn_btn.set_tooltip ("Decrease value by " + step_size.out)
			create tc
			vb.extend (tc)

			create tc
			extend (tc)
			tc.set_minimum_width (1)
			disable_item_expand (tc)

			create vb
			extend (vb)
			create tc
			vb.extend (tc)
			vb.extend (up_btn)
			vb.disable_item_expand (up_btn)
			up_btn.set_tooltip ("Increase value by " + step_size.out)
			create tc
			vb.extend (tc)

--			if not label_on_left then
			if controls_on_left then
				-- Put text to right of buttons
				extend (spinner_vb)
				create tc
				spinner_vb.extend (tc)
				spinner_vb.extend (spinner)
				create tc
				spinner_vb.extend (tc)
			end
		end

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			Precursor
			notifcations_suspended := False
		end

--|========================================================================
feature {NONE} -- Widget rendition updates
--|========================================================================

	update_spinner_values
		do
			value_range.resize_exactly (minimum_value, maximum_value)
			set_value (initial_value)
		end

	--|--------------------------------------------------------------

	update_widget_rendition
		do
			update_label_text
			update_spinner_values
			Precursor
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	is_updating: BOOLEAN
	last_value: INTEGER
	controls_on_left: BOOLEAN

	on_value_change
		local
			ts: STRING
			tv: INTEGER
		do
			ts := spinner.text
			if not ts.is_integer then
				-- reject
				is_updating := True
				spinner.set_text (last_value.out)
				is_updating := False
			else
				tv := ts.to_integer
				if not value_is_in_range (tv) then
					-- reject
					is_updating := True
					spinner.set_text (last_value.out)
					is_updating := False
				else
					-- Accept, maybe
					if not is_updating then
						set_value (tv)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	on_up_select
		local
			val: INTEGER
		do
			-- Increment value then notify
			--is_updating := True
			val := value + step_size
			if not value_is_in_range (val) then
				-- reject
			else
				set_value (val)
--				spinner.set_text (val.out)
--				notify_on_change
			end
			--notify
			--is_updating := False
		end

	on_down_select
		local
			val: INTEGER
		do
			-- Decrement value then notify
			--is_updating := True
			val := value - step_size
			if not value_is_in_range (val) then
				-- reject
			else
				set_value (val)
--				spinner.set_text (val.out)
--				notify_on_change
			end
			--notify
			--is_updating := False
		end

--|========================================================================
feature -- Widget actions
--|========================================================================

	notifcations_suspended: BOOLEAN

 	change_actions: EV_VALUE_CHANGE_ACTION_SEQUENCE

	notify_on_change
		do
			if not notifcations_suspended then
				notifcations_suspended := True
				from change_actions.start
				until change_actions.after
				loop
					change_actions.call (value)
					change_actions.forth
				end
				notifcations_suspended := False
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	spinner_vb: EV_VERTICAL_BOX
	spinner: EV_TEXT_FIELD
	dn_btn: AEL_V2S_PM_QBUTTON
	up_btn: AEL_V2S_PM_QBUTTON

--|========================================================================
feature -- Values
--|========================================================================

	step_size: INTEGER

	value: INTEGER
		local
			ts: STRING
		do
			ts := spinner.text
			if ts.is_integer then
				Result := ts.to_integer
			end
		end

	--|--------------------------------------------------------------

	minimum_value: INTEGER
	maximum_value: INTEGER

	initial_value: INTEGER
		do
			Result := private_initial_value.max (minimum_value)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_range (minv, maxv: INTEGER): BOOLEAN
			-- Is the range delimited by 'minv' and 'maxv' valid?
		do
			Result := minv <= maxv
		end

	--|--------------------------------------------------------------

	has_valid_range: BOOLEAN
		do
			Result := is_valid_range (minimum_value, maximum_value)
		end

	--|--------------------------------------------------------------

	value_is_in_range (v: INTEGER): BOOLEAN
			-- Is value 'v' in range?
		do
			Result := v >= minimum_value and v <= maximum_value
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	enable_sensitive
		do
			up_btn.enable_sensitive
			Precursor
		end

	disable_sensitive
		do
			dn_btn.enable_sensitive
			Precursor
		end

	--|--------------------------------------------------------------

	set_value (v: INTEGER)
		require
			in_range: value_is_in_range (v)
		do
			spinner.set_text (v.out)
			last_value := v
			notify_on_change
		end

	set_text_font (v: EV_FONT)
		do
			spinner.set_font (v)
		end

	set_fonts (lf, tf: EV_FONT)
		do
			set_label_font (lf)
			set_text_font (tf)
		end

	align_text_right
			-- Right-align the text in the text field (NOT the label)
		do
			spinner.align_text_right
		end

	align_text_left
			-- Left-align the text in the text field (NOT the label)
		do
			spinner.align_text_left
		end

	align_text_center
			-- Center the text in the text field (NOT the label)
		do
			spinner.align_text_center
		end

	--|--------------------------------------------------------------

	set_initial_value (v: INTEGER)
		require
			in_range: value_is_in_range (v)
		do
			private_initial_value := v
			update_spinner_values
		end

	--|--------------------------------------------------------------

	set_value_range (minv, maxv: INTEGER)
		require
			valid: minv <= maxv
		do
			minimum_value := minv
			maximum_value := maxv
			value_range.resize_exactly (minimum_value, maximum_value)
			--update_spinner_values
		end

	set_step (v: INTEGER)
			-- Set size of step (1 btn click)
		require
			positive: v > 0
		do
			step_size := v
		end

	--|--------------------------------------------------------------

	set_spinner_width (v: INTEGER)
		require
			positive: v > 0
		do
			spinner_vb.enable_item_expand (spinner)
			spinner.set_minimum_width (v)
			spinner_vb.disable_item_expand (spinner)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	value_range: INTEGER_INTERVAL
			-- Range of `value'.
			-- Default: 0 |..| 100

	private_initial_value: INTEGER

	default_spinner_width: INTEGER
		do
			Result := 80
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_attributes', giving
--| it a minimum and maximum value, a label string and a position flag,
--| or create using 'make' and subsequently call 'set_attributes'.
--|
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--|
--| If you wish to receive a notification on change, you can register 
--| your agent using 'set_notification_procedure'.  In the agent, 
--| simply query the 'text' routine for the current value.
--|
--|----------------------------------------------------------------------

end -- class AEL_V2S_LABELED_QSPINNER
