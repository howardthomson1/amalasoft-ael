note
	description: "{
Compound widget with a quasi text field (label-in-frame) and a label
Label is programatically settable, but not user-settable, acting like
a read-only text widget, but having settable bg color, even on GTK
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2007-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_LABELED_QTEXT

inherit
	AEL_V2S_LABELED_WIDGET
		rename
			widget as textw,
			set_widget_bg_color as set_textw_bg_color,
			set_widget_fg_color as set_textw_fg_color
		redefine
			add_widget, initialize_interface_actions,
			set_widget_width
		end

create
	make_no_label, make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_no_label
			-- Create Current for label-free rendering
		do
			sans_label := True
			make
		end

	initialize_interface_actions
		do
			resize_actions.extend (agent on_resize)
			Precursor
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create framew
			create textw
		end

	add_widget
	-- Add widget to Current
		do
			extend (framew)
			framew.extend (textw)
		end

--|========================================================================
feature -- Components
--|========================================================================

	framew: EV_FRAME
	textw: EV_LABEL

--|========================================================================
feature -- Status
--|========================================================================

	text: STRING
		do
			Result := textw.text
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_text (v: STRING)
		do
			textw.set_text (v)
		end

	remove_text
			-- 	Remove text from the text widget
		do
			textw.remove_text
		end

	--|--------------------------------------------------------------

	set_text_font (v: EV_FONT)
		do
			textw.set_font (v)
		end

	set_fonts (lf, tf: EV_FONT)
		do
			set_label_font (lf)
			set_text_font (tf)
		end

	--|--------------------------------------------------------------

	set_text_width (v: INTEGER)
		require
			positive: v > 0
		do
			set_widget_width (v)
		end

	set_text_minimum_width (v: INTEGER)
		require
			positive: v > 0
		do
			enable_item_expand (textw)
			textw.set_minimum_width (v)
		end

	--|--------------------------------------------------------------

	set_widget_width (v: INTEGER)
		do
			textw.set_minimum_width (v)
			disable_item_expand (framew)
		end

	--|--------------------------------------------------------------

	align_text_right
			-- Right-align the text in the text field (NOT the label)
		do
			textw.align_text_right
		end

	align_text_left
			-- Left-align the text in the text field (NOT the label)
		do
			textw.align_text_left
		end

	align_text_center
			-- Center the text in the text field (NOT the label)
		do
			textw.align_text_center
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER)
		local
			ts: STRING
		do
			-- RFO
			-- There is an issue, at least on Windows, where the label
			-- and text are not being redrawn on resize, unless there's an
			-- intervening expose event.
			-- The set_text calls force lower-level invalidation
			ts := label.text.twin
			label.remove_text
			label.set_text (ts)

			ts := textw.text.twin
			textw.remove_text
			textw.set_text (ts)
	end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	default_text_width: INTEGER
		do
			Result := 80
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 04-July-2012
--|     Recompiled and tested original module for Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using
--| 'make' and subsequently calling set_label_text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--|
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--|
--| If you wish to receive a notification on commit (enter actions) of the
--| string in the text widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'text' routine for the committed value.
--|
--| If you wish to receive a notification on text change, you can
--| register your agent using 'set_change_notification_procedure'
--|
--| If you wish to receive a notification on getting focus (focus in)
--| you can register your agent using 'set_focus_in_notification_procedure'
--|----------------------------------------------------------------------

end -- class AEL_V2S_LABELED_QTEXT
