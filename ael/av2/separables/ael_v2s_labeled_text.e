note
	description: "{
Compound widget with a single-line text field and a label (separable)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_LABELED_TEXT

inherit
	AEL_V2S_LABELED_MULTILINE_TEXT
		redefine
			create_widget, textw, initialize_interface_actions, add_widget,
			set_text_width
		end

create
	make, make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	create_widget
		do
			create enclosing_vb
			create textw
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface
			-- components
		do
			textw.return_actions.extend (agent on_text_enter)
			Precursor
		end

	--|--------------------------------------------------------------

	add_widget
			-- Add widget to Current
		local
			tc: EV_CELL
		do
			extend (enclosing_vb)
			create tc
			enclosing_vb.extend (tc)
			enclosing_vb.extend (textw)
			create tc
			enclosing_vb.extend (tc)
		end

--|========================================================================
feature -- Components
--|========================================================================

	textw: EV_TEXT_FIELD

	change_actions: EV_NOTIFY_ACTION_SEQUENCE
			-- Actions to be performed when `text' changes.
		do
			Result := textw.change_actions
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text_font (v: EV_FONT)
		do
			textw.set_font (v)
		end

	set_fonts (lf, tf: EV_FONT)
		do
			set_label_font (lf)
			set_text_font (tf)
		end

	--|--------------------------------------------------------------

	set_text_width (v: INTEGER)
		do
			textw.set_minimum_width (v)
			disable_item_expand (enclosing_vb)
		end

	--|--------------------------------------------------------------

	align_text_right
			-- Right-align the text in the text field (NOT the label)
		do
			textw.align_text_right
		end

	align_text_left
			-- Left-align the text in the text field (NOT the label)
		do
			textw.align_text_left
		end

	align_text_center
			-- Center the text in the text field (NOT the label)
		do
			textw.align_text_center
		end

--|========================================================================
feature -- Components
--|========================================================================

	enclosing_vb: EV_VERTICAL_BOX
			-- vertical box enclosing textw

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_text_enter
			-- Respond to an Enter (CR) event in textw
		do
			notify
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using
--| 'make' and subsequently setting the label text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--|
--| If you wish to receive a notification on commit (enter actions) of the
--| string in the text widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'text' routine for the committed value.
--|
--| If you wish to receive notification on each character change, you
--| can register your agent using the 'set_change_notification_proc'
--| routine.
--|----------------------------------------------------------------------

end -- class AEL_V2S_LABELED_TEXT
