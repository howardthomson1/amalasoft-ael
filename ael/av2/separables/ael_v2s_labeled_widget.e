note
	description: "{
Compound widget with a primitive widget and a label (separable)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2007-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2S_LABELED_WIDGET

inherit
	EV_HORIZONTAL_BOX
      rename
			implementation as hb_implementation_attr
		undefine
			create_interface_objects
		redefine
			initialize, create_implementation,
			set_background_color
		select
			hb_implementation_attr
		end
	AEL_V2S_WIDGET
		redefine
			implementation,
			create_interface_objects,
			build_interface_components,
			post_initialize
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_label (v: STRING; pf: BOOLEAN)
			-- Create a new widget with the label string 'v', and with label
			-- aligned either to right of widget (default) or to left of the
			-- widget (if 'pf' is True)
		do
			label_on_left := pf
			make
			set_label_text (v)
		end

	make_with_label_and_options (v: STRING; pf, rx: BOOLEAN; gw: INTEGER)
			-- Create a new widget with the label string 'v', and with label
			-- aligned either to right of widget (default) or to left of the
			-- widget (if 'pf' is True)
			-- If 'rx', then allow right pad to expand (else is fixed at 
			-- gutter width)
			-- 'gw' is gutter width (space between label and widget and 
			-- at left and right ends of box)
		do
			private_gutter_width := gw
			right_is_expandable := rx
			make_with_label (v, pf)
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	implementation: like hb_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := hb_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_HORIZONTAL_BOX}
			implementation_attr := hb_implementation_attr
		end

	--|--------------------------------------------------------------

	initialize
			-- Initialize Current
		do
			Precursor {EV_HORIZONTAL_BOX}
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			-- Need to create label_text here to support default_create
			create label_text.make_empty
			create label.make_with_text (label_text)
			create cell_1
			create left_gutter_cell
			create right_gutter_cell
			create_widget
			Precursor {AEL_V2S_WIDGET}
		end

	--|--------------------------------------------------------------

	build_interface_components
		local
		do
			if label_on_left then
				extend (label)
				label.align_text_right

				extend (cell_1)
				cell_1.set_minimum_width (gutter_width)
				disable_item_expand (cell_1)
			elseif sans_label then
				-- No label at all, so no gutter either
			else
				-- Label on right, add edge gutter before widget
				extend (left_gutter_cell)
				left_gutter_cell.set_minimum_width (0)
				disable_item_expand (left_gutter_cell)
			end

			add_widget

			if (not label_on_left) and not sans_label then
				extend (cell_1)
				cell_1.set_minimum_width (gutter_width)
				disable_item_expand (cell_1)

				extend (label)
				label.align_text_left
			end

			if sans_label then
			else
				-- Add right-margin pad
				extend (right_gutter_cell)
				right_gutter_cell.set_minimum_width (5)
				if not right_is_expandable then
					disable_item_expand (right_gutter_cell)
				end
			end
			Precursor
		end

	--|--------------------------------------------------------------

	create_widget
		deferred
		ensure
			widget_exists: widget /= Void
		end

	add_widget
	-- Add widget to Current
		do
			extend (widget)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			set_border_width (1)
			Precursor
		end

	--|--------------------------------------------------------------

	update_label_text
		do
			if label_text.is_empty then
				label.remove_text
			else
				label.set_text (label_text)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	notification_proc: detachable PROCEDURE [TUPLE[like Current]]
	
	notify
		do
			if attached notification_proc as np then
				np.call ([Current])
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	widget: EV_WIDGET
		deferred
		end

	label: EV_LABEL

	cell_1: EV_CELL

	label_text: STRING
	label_on_left: BOOLEAN

	left_gutter_cell: EV_CELL
	right_gutter_cell: EV_CELL

--|========================================================================
feature -- Status
--|========================================================================

	sans_label: BOOLEAN
			-- Is Current supposed to be label-free?

	current_label_font: EV_FONT
		do
			Result := label.font
		end

	current_label_color: EV_COLOR
		do
			Result := label.foreground_color
		end

	--|--------------------------------------------------------------

	gutter_width: INTEGER
			-- Width of gutter between widget and label
		do
			Result := private_gutter_width
			if Result = 0 then
				Result := 2
			end
		end

	--|--------------------------------------------------------------

	right_is_expandable: BOOLEAN
			-- Is right margin to be left expandable? Dflt is fixed

	--|--------------------------------------------------------------

	widget_font: detachable EV_FONT
			-- Font presently in use in 'widget', if any
		do
			if attached {EV_FONTABLE} widget as fw then
				Result := fw.font
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_label_font (v: EV_FONT)
		do
			label.set_font (v)
		end

	set_label_color (v: EV_COLOR)
		do
			label.set_foreground_color (v)
		end

	set_label_bg_color (v: EV_COLOR)
			-- Set, to 'v', background color of label and spacing cell, 
			-- but not widget
		do
			label.set_background_color (v)
			cell_1.set_background_color (v)
		end

	--|--------------------------------------------------------------

	set_label_text (v: STRING)
		do
			label_text.wipe_out
			label_text.append (v)
			update_label_text
		end
	 
	set_label_width (v: INTEGER)
		do
			label.set_minimum_width (v)
			disable_item_expand (label)
		end
	
	disable_label_expand
		do
			disable_item_expand (label)
		end

	--|--------------------------------------------------------------	

	set_widget_width (v: INTEGER)
		do
			widget.set_minimum_width (v)
			disable_item_expand (widget)
		end

	set_widget_font (v: EV_FONT)
		do
			if attached {EV_FONTABLE} widget as fw then
				fw.set_font (v)
			end
		end

	set_widget_bg_color (v: EV_COLOR)
		do
			widget.set_background_color (v)
		end

	set_widget_fg_color (v: EV_COLOR)
		do
			widget.set_foreground_color (v)
		end

	--|--------------------------------------------------------------

	set_left_gutter_width (v: INTEGER)
		do
			left_gutter_cell.set_minimum_width (v)
			disable_item_expand (left_gutter_cell)
		end
	
	set_right_gutter_width (v: INTEGER)
		do
			right_gutter_cell.set_minimum_width (v)
			disable_item_expand (right_gutter_cell)
		end
	
	set_gutter_width (v: INTEGER)
		do
			private_gutter_width := v
			disable_item_expand (cell_1)
		end
	
	--|--------------------------------------------------------------	

	set_event_focus
		do
			widget.set_focus
		end

	set_background_color (v: EV_COLOR)
			-- Set the background color of Current, its label and 
			-- spacing cells but do not set color for actual widget
			-- UNLESS in label-free mode, in which case, set the bg
			-- color of ONLY the widget
		do
			Precursor (v)
			if sans_label then
				set_widget_bg_color (v)
			else
				set_label_bg_color (v)
				cell_1.set_background_color (v)
				left_gutter_cell.set_background_color (v)
				right_gutter_cell.set_background_color (v)
			end
		end

	--|--------------------------------------------------------------

	set_notification_procedure (v: like notification_proc)
		do
			notification_proc := v
		end

	--|--------------------------------------------------------------

	set_tooltip (v: STRING_GENERAL)
			-- Assign `a_tooltip' to `tooltip'.
		require
			not_destroyed: not is_destroyed
			tooltip: v /= Void
		do
			if attached {EV_TOOLTIPABLE}widget as tt then
				tt.set_tooltip (v)
			end
			if attached label then
				label.set_tooltip (v)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_gutter_width: INTEGER
			-- Non-default gutter width

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and so cannot be instantiated as-is. Instead,
--| use one of its descendents, like AEL_V2_LABELED_SPIN_BUTTON,
--| AEL_V2_LABELED_BUTTON, AEL_V2_LABELED_POSITIONER or AEL_V2_LABELED_TEXT.
--|----------------------------------------------------------------------

end -- class AEL_V2S_LABELED_WIDGET
