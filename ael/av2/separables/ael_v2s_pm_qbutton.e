note
	description: "{
A quasi-button displaying a pixmap in a cell, and no text (separable)
but implemented as a cell with a bg pixmap and not as a button
N.B. the select behavior is faked, with button up
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_PM_QBUTTON

inherit
	EV_CELL
      rename
			implementation as btn_implementation_attr
		undefine
			create_interface_objects
		redefine
			create_implementation,
			enable_sensitive, disable_sensitive
		select
			btn_implementation_attr
		end
 	AEL_V2S_WIDGET
		undefine
			enable_sensitive, disable_sensitive
 		redefine
 			implementation,
 			create_interface_objects,
 			build_interface_components, initialize_interface_actions
		end

create
	make_with_pixmaps

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_pixmaps (spm, ipm: EV_PIXMAP)
			-- Create `Current' with sensitive pixmap 'spm' and 
			-- insensitive pixmap 'ipm'
		do
			tooltip_text := ""
			create select_actions
			create sensitive_pbuf.make_with_pixmap (spm)
			create insensitive_pbuf.make_with_pixmap (ipm)
			make
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

	implementation: like btn_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := btn_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_CELL}
			implementation_attr := btn_implementation_attr
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor {AEL_V2S_WIDGET}
--			create cell
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		local
			pm: like new_sensitive_pixmap
		do
--			extend (cell)
			pm := new_sensitive_pixmap
--			cell.extend (pm)
			extend (pm)
			init_pm_item (pm)
--			cell.set_minimum_size (pm.width, pm.height)
			set_minimum_size (pm.width, pm.height)
		end

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	select_actions: EV_NOTIFY_ACTION_SEQUENCE
			-- Actions to be performed when button is pressed then released.

	new_sensitive_pixmap: EV_PIXMAP
		do 
			Result := sensitive_pbuf.to_pixmap
		end

	new_insensitive_pixmap: EV_PIXMAP
		do 
			Result := insensitive_pbuf.to_pixmap
		end

	tooltip_text: STRING
			-- Text of tooltip, if any

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_sensitive
		local
			pm: EV_PIXMAP
		do
			Precursor
--			cell.replace (new_sensitive_pixmap)
			pm := new_sensitive_pixmap
			replace (pm)
			init_pm_item (pm)
		end

	disable_sensitive
		local
			pm: EV_PIXMAP
		do
			Precursor
--			cell.replace (new_insensitive_pixmap)
			pm := new_insensitive_pixmap
			replace (pm)
			init_pm_item (pm)
		end

	--|--------------------------------------------------------------

	set_pixmaps (vs, vi: EV_PIXMAP)
			-- Set sensitive and insensitive pixmaps, respectively
		do
			create sensitive_pbuf.make_with_pixmap (vs)
			create insensitive_pbuf.make_with_pixmap (vi)
			if is_sensitive then
				enable_sensitive
			else
				disable_sensitive
			end
		end

	set_sensitive_pixmap (v: EV_PIXMAP)
		do
			create sensitive_pbuf.make_with_pixmap (v)
			if is_sensitive then
				enable_sensitive
			else
				disable_sensitive
			end
		end

	set_insensitive_pixmap (v: EV_PIXMAP)
		do
			create insensitive_pbuf.make_with_pixmap (v)
			if is_sensitive then
				enable_sensitive
			else
				disable_sensitive
			end
		end

	--|--------------------------------------------------------------

	set_tooltip (v: STRING)
		do
			tooltip_text := v
			if attached {EV_TOOLTIPABLE} item as ti then
				ti.set_tooltip (v)
			end
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	init_pm_item (pm: EV_PIXMAP)
		do
			pm.set_tooltip (tooltip_text)
			pm.pointer_button_press_actions.extend (agent on_mouse_down)
			pm.pointer_button_release_actions.extend (agent on_mouse_up)
		end

	mouse_pending: BOOLEAN
			-- Is a mouse up event likely to be coming?

	mouse_timer: detachable EV_TIMEOUT
		note
			option: stable attribute
		end

	clear_mouse_timer
		do
			mouse_pending := False
			if attached mouse_timer as wt then
				wt.actions.wipe_out
				wt.destroy
			end
		end

	on_mouse_down (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-down event on cell
		do
			if b = 1 then
				mouse_pending := True
				create mouse_timer.make_with_interval (200)
				mouse_timer.actions.extend (agent clear_mouse_timer)
			end
		end

	on_mouse_up (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-up event on cell
		do
			if b = 1 and mouse_pending then
				clear_mouse_timer
				from select_actions.start
				until select_actions.after
				loop
					select_actions.call
					select_actions.forth
				end
			end
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

--	cell: EV_CELL

	sensitive_pbuf: EV_PIXEL_BUFFER
	insensitive_pbuf: EV_PIXEL_BUFFER

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make_with_pixmaps'.
--|----------------------------------------------------------------------

end -- class AEL_V2S_PM_QBUTTON
