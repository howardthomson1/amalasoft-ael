note
	description: "{
A quasi-button with no text (separable)
implemented as a frame and not as a button
N.B. the select behavior is faked, with button up
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_QFBUTTON

inherit
	EV_FRAME
      rename
			implementation as btn_implementation_attr
		undefine
			create_interface_objects
		redefine
			create_implementation,
			enable_sensitive, disable_sensitive
		select
			btn_implementation_attr
		end
 	AEL_V2S_WIDGET
		undefine
			enable_sensitive, disable_sensitive
 		redefine
 			implementation, make,
 			create_interface_objects,
 			build_interface_components, initialize_interface_actions
		end

create
	make, make_with_size

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_size (w, h: INTEGER)
		do
			initial_width := w
			initial_height := h
			make
		end

	make
			-- Create `Current'
		do
			tooltip_text := ""
			create select_actions
			Precursor
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

	implementation: like btn_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := btn_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_FRAME}
			implementation_attr := btn_implementation_attr
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor {AEL_V2S_WIDGET}
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		do
			if initial_width /= 0 and initial_height /= 0 then
				set_minimum_size (initial_width, initial_height)
			end
		end

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			pointer_button_press_actions.extend (agent on_mouse_down)
			pointer_button_release_actions.extend (agent on_mouse_up)
		end

--|========================================================================
feature -- Status
--|========================================================================

	select_actions: EV_NOTIFY_ACTION_SEQUENCE
			-- Actions to be performed when button is pressed then released.

	tooltip_text: STRING
			-- Text of tooltip, if any

	initial_width: INTEGER
	initial_height: INTEGER

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_sensitive
		local
			--pm: EV_PIXMAP
		do
			Precursor
			-- Change rendition??
		end

	disable_sensitive
		local
			--pm: EV_PIXMAP
		do
			Precursor
			-- Change rendition??
		end

	--|--------------------------------------------------------------

	--|--------------------------------------------------------------

	set_tooltip (v: STRING)
		do
			tooltip_text := v
			if attached {EV_TOOLTIPABLE} Current as tc then
				tc.set_tooltip (v)
			elseif attached {EV_TOOLTIPABLE} item as ti then
				ti.set_tooltip (v)
			end
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	mouse_pending: BOOLEAN
			-- Is a mouse up event likely to be coming?

	mouse_timer: detachable EV_TIMEOUT
		note
			option: stable attribute
		end

	clear_mouse_timer
		do
			mouse_pending := False
			if attached mouse_timer as wt then
				wt.actions.wipe_out
				wt.destroy
			end
		end

	on_mouse_down (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-down event on frame
		do
			if b = 1 then
				mouse_pending := True
				create mouse_timer.make_with_interval (200)
				mouse_timer.actions.extend (agent clear_mouse_timer)
			end
		end

	on_mouse_up (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-up event on frame
		do
			if b = 1 and mouse_pending then
				clear_mouse_timer
				from select_actions.start
				until select_actions.after
				loop
					select_actions.call
					select_actions.forth
				end
			end
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make_with_pixmaps'.
--|----------------------------------------------------------------------

end -- class AEL_V2S_QFQBUTTON
