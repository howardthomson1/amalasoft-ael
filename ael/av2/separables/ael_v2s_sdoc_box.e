note
	description: "{
A container widget in which can be presented a structured document and index
Structured document must conform to AEL_STRUCTURED_DOCUMENT
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_SDOC_BOX

inherit
	AEL_V2S_VERTICAL_BOX
		redefine
			make, create_interface_objects, build_interface_components,
			initialize_interface_values, initialize_interface_actions,
			initialize_interface_images, post_initialize, show
		end
	AEL_USML_TYPES
		undefine
			default_create, copy, is_equal
		end

create
	make, make_with_document

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_document (v: like document)
		do
			make
			set_document (v)
		end

	make
		do
			create document.make
			Precursor
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create any auxilliary objects needed for MAIN_WINDOW.
			-- Initialization for these objects must be performed in 
			-- `user_initialization'.
		do
			Precursor

			if not attached document then
				-- Create an empty placeholder
				create document.make
			end

			create main_sa

			create toc_frame
			create toc_tree

			create textw
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		local
			tf: EV_FRAME
			vb: EV_VERTICAL_BOX
		do
			Precursor

			extend (main_sa)
			main_sa.extend (toc_frame)

			create vb
			toc_frame.extend (vb)

			create toc_tree
			vb.extend (toc_tree)

			create tf
			main_sa.extend (tf)
			tf.extend (textw)
			textw.disable_edit
		end

	--|--------------------------------------------------------------

	build_toc_tree
		local
			tmi: EV_TREE_ITEM
			doc: like document
			last_ti1, last_ti2, last_ti3, last_ti4: EV_TREE_ITEM
			last_ti5, last_ti6, last_ti7, pari: EV_TREE_ITEM
			sec: AEL_STRUCTURED_DOC_SECTION
		do
			doc := document

			from doc.start
			until doc.after
			loop
				sec := doc.item
				if sec.is_title then
					-- Skip
				elseif sec.is_heading then
					create tmi.make_with_text (sec.text)
					tmi.set_data (sec)
					inspect sec.level
					when 1 then
						--pari := toc_tree
						pari := Void
						last_ti1 := tmi
					when 2 then
						if attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti2 := tmi
						last_ti3 := Void
						last_ti4 := Void
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 3 then
						if attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti3 := tmi
						last_ti4 := Void
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 4 then
						if attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti4 := tmi
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 5 then
						if attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti5 := tmi
						last_ti6 := Void
						last_ti7 := Void
					when 6 then
						if attached last_ti5 then
							pari := last_ti5
						elseif attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti6 := tmi
						last_ti7 := Void
					when 7 then
						if attached last_ti6 then
							pari := last_ti6
						elseif attached last_ti5 then
							pari := last_ti5
						elseif attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							--pari := toc_tree
							pari := Void
						end
						last_ti7 := tmi
					else
						--pari := toc_tree
						pari := Void
					end
					if attached pari then
						pari.extend (tmi)
					else
						toc_tree.extend (tmi)
					end
					if sec.has_items then
						build_toc_branch (tmi, sec)
					end
				end
				doc.forth
			end
		end

	--|--------------------------------------------------------------

	build_toc_branch (tb: EV_TREE_ITEM; psec: AEL_STRUCTURED_DOC_SECTION)
		local
			tmi: EV_TREE_ITEM
			last_ti1, last_ti2, last_ti3, last_ti4: EV_TREE_ITEM
			last_ti5, last_ti6, last_ti7, pari: EV_TREE_ITEM
			sec: AEL_STRUCTURED_DOC_SECTION
		do
			from psec.start
			until psec.after
			loop
				sec := psec.item
				if sec.is_heading then
					create tmi.make_with_text (sec.text)
					tmi.set_data (sec)
					inspect sec.level
					when 1 then
						pari := Void
						last_ti1 := tmi
						last_ti2 := Void
						last_ti3 := Void
						last_ti4 := Void
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 2 then
						if attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti2 := tmi
						last_ti3 := Void
						last_ti4 := Void
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 3 then
						if attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti3 := tmi
						last_ti4 := Void
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 4 then
						if attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti4 := tmi
						last_ti5 := Void
						last_ti6 := Void
						last_ti7 := Void
					when 5 then
						if attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti5 := tmi
						last_ti6 := Void
						last_ti7 := Void
					when 6 then
						if attached last_ti5 then
							pari := last_ti5
						elseif attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti6 := tmi
						last_ti7 := Void
					when 7 then
						if attached last_ti6 then
							pari := last_ti6
						elseif attached last_ti5 then
							pari := last_ti5
						elseif attached last_ti4 then
							pari := last_ti4
						elseif attached last_ti3 then
							pari := last_ti3
						elseif attached last_ti2 then
							pari := last_ti2
						elseif attached last_ti1 then
							pari := last_ti1
						else
							pari := Void
						end
						last_ti7 := tmi
					else
						pari := Void
					end
					if attached pari then
						pari.extend (tmi)
					else
						tb.extend (tmi)
					end
					if sec.has_items then
						build_toc_branch (tmi, sec)
					end
				end
				psec.forth
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			Precursor
			toc_tree.select_actions.extend (agent on_toc_select)
		end

	--|--------------------------------------------------------------

	initialize_interface_images
		do
			Precursor
		end

	--|--------------------------------------------------------------

	post_initialize
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	document: AEL_STRUCTURED_DOCUMENT

--|========================================================================
feature -- Status setting
--|========================================================================

	set_document (v: like document)
		do
			document := v
			fill_rich_text
		end

	--|--------------------------------------------------------------

	show
		do
			Precursor
			toc_frame.hide
			build_toc_tree
			main_sa.set_proportion ({REAL}0.25)
			toc_frame.show
			expand_toc_tree
		end

	--|--------------------------------------------------------------

	fill_rich_text
			-- Fill textw with elements from 'document'
		local
			doc: like document
			ts: STRING
		do
			textw.remove_text
			textw.set_current_format (title_char_format)

			doc := document
			doc.start

			if not doc.is_empty and then doc.item.is_title then
				ts := doc.item.text
				doc.forth
			end
			if not doc.title.is_empty then
				-- Override embedded title
				ts := doc.title
			end
			if attached ts then
				textw.append_text (ts + "%N")
				textw.format_paragraph (1, textw.text_length, title_par_format)
			end

			from
			until doc.after
			loop
				write_doc_section (doc.item)
				doc.forth
			end
		end

	--|--------------------------------------------------------------

	write_doc_section (ds: AEL_STRUCTURED_DOC_SECTION)
			-- Write contents of 'd's to 'textw'
		local
			sp, ep, sec_start: INTEGER
			par_fmt: EV_PARAGRAPH_FORMAT
			char_fmt: EV_CHARACTER_FORMAT
			cfe: EV_CHARACTER_FORMAT_EFFECTS
			itags: TWO_WAY_LIST [AEL_USML_INLINE_TAG]
			itag: AEL_USML_INLINE_TAG
			tfmt: AEL_SDOC_FORMAT
		do
--DEBUG
			if ds.text ~ "The Renderer" then
				sp := sp
			end
			if attached ds.format as fmt then
				char_fmt := character_format_from_format (fmt, Void)
				par_fmt := paragraph_format_from_format (fmt)
				if fmt.is_underlined then
					create cfe
					cfe.enable_underlined
					char_fmt.set_effects (cfe)
				end
			else
				char_fmt := default_char_format (ds.level)
				par_fmt := default_par_format (ds.level)
			end
			tfmt := ds.default_format

			sp := textw.text_length + 1
			sec_start := sp
			if ds.is_heading then
				ds.set_start_index (sp)
			end
			if not ds.text.is_empty then
				textw.set_current_format (char_fmt)
				textw.append_text (ds.text + "%N")
				ep := textw.text_length
				textw.format_region (sp, ep, char_fmt)
				textw.format_paragraph (sp, ep, par_fmt)
				if ds.has_inline_tags then
					-- Re-format any text captured by inline tag
					itags := ds.inline_tags
					from itags.start
					until itags.after
					loop
						itag := itags.item
						-- Format captive text regions
						if attached itag.format as fmt then
							char_fmt := character_format_from_format (fmt, tfmt)
							textw.format_region (
								itag.captive_text_start + sec_start,
								itag.captive_text_end + sec_start,
								char_fmt)
						end
						itags.forth
					end
				end
			else
				sp := sp -- for breakpoint when debuggin
			end

			if not ds.is_empty then
				-- Is a compound element, with sub-elements
				from ds.start
				until ds.after
				loop
					write_doc_section (ds.item)
					ds.forth
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO section_text_font (ds: AEL_STRUCTURED_DOC_SECTION): EV_FONT
	--RFO 		-- Font described in doc section 'ds' for text
	--RFO 	local
	--RFO 		ps: INTEGER
	--RFO 		b, i: BOOLEAN
	--RFO 	do
	--RFO 		b := ds.text_is_bold
	--RFO 		i := ds.text_is_italic
	--RFO 		ps := ds.text_point_size
	--RFO 		if ps = 0 then
	--RFO 			ps := 12
	--RFO 		end
	--RFO 		if attached ds.text_font_family as ff then
	--RFO 			Result := section_font (ff, ps, b, i)
	--RFO 		else
	--RFO 			Result := sans_serif_font (ps, b, i)
	--RFO 		end
	--RFO 	end

	--RFO section_font (ff: STRING; ps: INTEGER; b, i: BOOLEAN): EV_FONT
	--RFO 		-- Font corresponding to values provided
	--RFO 	do
	--RFO 		if ff ~ Ks_ff_sans then
	--RFO 			Result := sans_serif_font (ps, b, i)
	--RFO 		elseif ff ~ Ks_ff_times then
	--RFO 			Result := times_font (ps, b, i)
	--RFO 		elseif ff ~ Ks_ff_fixed then
	--RFO 			Result := fixed_font (ps, b, i)
	--RFO 		else
	--RFO 			Result := sans_serif_font (ps, b, i)
	--RFO 		end
	--RFO 	end

--|========================================================================
feature -- Formats
--|========================================================================

	title_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_center_alignment
			Result.set_bottom_spacing (9)
		end

	hdr1_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_top_spacing (2)
			Result.set_bottom_spacing (3)
		end

	hdr2_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (2)
		end

	hdr3_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	hdr4_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	hdr5_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	normal_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
		end

	--|--------------------------------------------------------------

	code_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				fixed_font (14, True, False))
		end

	title_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				sans_serif_font (18, True, False))
		end

	hdr1_char_format: EV_CHARACTER_FORMAT
		local
			cfe: EV_CHARACTER_FORMAT_EFFECTS
		once
			create Result.make_with_font (
				times_font (14, True, False))
			create cfe
			cfe.enable_underlined
			Result.set_effects (cfe)
		end

	hdr2_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, True, False))
		end

	hdr3_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, True, True))
		end

	hdr4_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, False, True))
		end

	hdr5_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (11, False, True))
		end

	normal_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, False, False))
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

	font_shape_from_format (fmt: AEL_SDOC_FORMAT): INTEGER
		do
			if fmt.is_italic then
				Result := ev_font_constants.shape_italic
			else
				Result := ev_font_constants.shape_regular
			end
		end

	font_weight_from_format (fmt: AEL_SDOC_FORMAT): INTEGER
		do
			if fmt.is_bold then
				Result := ev_font_constants.weight_bold
			else
				Result := ev_font_constants.weight_regular
			end
		end

	font_color_from_format (fmt: AEL_SDOC_FORMAT): EV_COLOR
		local
			rs, gs, bs: STRING
			rv, gv, bv: INTEGER
		do
			if attached fmt.text_color as rc and then is_valid_rgb_color (rc) then
				rs := rc.substring (1, 2)
				gs := rc.substring (3, 4)
				bs := rc.substring (5, 6)
				rv := hex_8_bit_to_integer (rs)
				gv := hex_8_bit_to_integer (gs)
				bv := hex_8_bit_to_integer (bs)
				create Result.make_with_8_bit_rgb (rv, gv, bv)
			else
				Result := ev_colors.black
			end
		end

	char_effects_from_format (fmt: AEL_SDOC_FORMAT): EV_CHARACTER_FORMAT_EFFECTS
		do
			create Result
			if fmt.is_strike_through then
				Result.enable_striked_out
			end
			if fmt.is_underlined then
				Result.enable_underlined
			end
			Result.set_vertical_offset (fmt.vertical_offset)
		end

	font_family_from_format (fmt: AEL_SDOC_FORMAT): INTEGER
		do
			if fmt.font_family ~ Ks_ff_sans then
				Result := ev_font_constants.family_sans
			elseif fmt.font_family ~ Ks_ff_times then
				Result := ev_font_constants.family_roman
			elseif fmt.font_family ~ Ks_ff_fixed then
				Result := ev_font_constants.family_typewriter
			else
				Result := ev_font_constants.family_sans
			end
		end

	font_from_format (
		fmt: AEL_SDOC_FORMAT; rfmt: detachable AEL_SDOC_FORMAT): EV_FONT
			-- Attributes of 'FMT', as a font
		local
			ff, ps, wt, sh: INTEGER
		do
			ff := font_family_from_format (fmt)
			if fmt.point_size_is_relative and attached rfmt as rf then
				ps := rf.point_size + fmt.point_size
			else
				ps := fmt.point_size
			end
			wt := font_weight_from_format (fmt)
			sh := font_shape_from_format (fmt)
			create Result.make_with_values (ff, wt, sh, ps)
			if ff = K_ff_times then
				Result.preferred_families.extend ("Times New Roman")
			elseif ff = K_ff_fixed then
				Result.preferred_families.extend ("Courier New")
			else
				Result.preferred_families.extend ("Arial")
			end
		end

	character_format_from_format (
		fmt: AEL_SDOC_FORMAT;
		rfmt: detachable AEL_SDOC_FORMAT): EV_CHARACTER_FORMAT
		do
			create Result.make_with_font (font_from_format (fmt, rfmt))
			Result.set_color (font_color_from_format (fmt))
			Result.set_effects (char_effects_from_format (fmt))
		end

	paragraph_format_from_format (fmt: AEL_SDOC_FORMAT): EV_PARAGRAPH_FORMAT
		do
			create Result
			if fmt.alignment_has_changed then
				if fmt.is_right_aligned then
					Result.enable_right_alignment
				elseif fmt.is_center_aligned then
					Result.enable_center_alignment
				elseif fmt.is_justified then
					Result.enable_justification
				else
					Result.enable_left_alignment
				end
			end
			if fmt.left_margin_has_changed then
				Result.set_left_margin (fmt.left_margin)
			end
			if fmt.right_margin_has_changed then
				Result.set_right_margin (fmt.right_margin)
			end
			if fmt.top_margin_has_changed then
				Result.set_top_spacing (fmt.top_margin)
			end
			if fmt.bottom_margin_has_changed then
				Result.set_bottom_spacing (fmt.bottom_margin)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Agents and actions
--|========================================================================

	on_toc_select
			-- Respoond to select event in TOC tree
			-- Scroll to selection
		local
			lno: INTEGER
		do
			if attached toc_tree.selected_item as ti then
				if attached {AEL_STRUCTURED_DOC_SECTION}ti.data as sec then
					lno := textw.line_number_from_position (sec.start_index)
					if lno /= 0 then
						textw.scroll_to_line (lno)
					end
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	expand_toc_tree
			-- Expand each item in the tree
		do
			if not toc_tree.is_empty then
				toc_tree.recursive_do_all (agent expand_toc_node)
				toc_tree.ensure_item_visible (toc_tree.first)
			end
		end

	--|--------------------------------------------------------------

	expand_toc_node (tn: EV_TREE_NODE)
			-- Expand the tree to expose node 'tn'
		do
			toc_tree.ensure_item_visible (tn)
		end

--|========================================================================
feature -- Support
--|========================================================================

	hex_8_bit_to_integer (v: STRING): INTEGER
			-- Integer value represented by 2-digit hex string 'v'
		require
			small_enough: v.count <= 2
		local
			c: CHARACTER
		do
			c := v.item (1)
			if c.is_digit then
				Result := (c |-| '0')
			else
				Result := (c.upper |-| 'A') + 10
			end
			if v.count = 2 then
				Result := (Result * 16)
				c := v.item (2)
				if c.is_digit then
					Result := Result + (c |-| '0')
				else
					Result := (Result + c.upper |-| 'A') + 10
				end
			end
		end

--|========================================================================
feature -- Components
--|========================================================================

	toc_frame: EV_FRAME
	toc_tree: EV_TREE

	main_sa: EV_HORIZONTAL_SPLIT_AREA

	textw: EV_RICH_TEXT

--|========================================================================
feature -- Add Comment
--|========================================================================

	default_char_format (lvl: INTEGER): EV_CHARACTER_FORMAT
			-- Default character format for section level 'lvl'
		do
			inspect lvl
			when 1 then
				Result := hdr1_char_format
			when 2 then
				Result := hdr2_char_format
			when 3 then
				Result := hdr3_char_format
			when 4 then
				Result := hdr4_char_format
			when 5 then
				Result := hdr5_char_format
			else
				Result := normal_char_format
			end
		end

	default_par_format (lvl: INTEGER): EV_PARAGRAPH_FORMAT
			-- Default paragraph format for section level 'lvl'
		do
			inspect lvl
			when 1 then
				Result := hdr1_par_format
			when 2 then
				Result := hdr2_par_format
			when 3 then
				Result := hdr3_par_format
			when 4 then
				Result := hdr4_par_format
			when 5 then
				Result := hdr5_par_format
			else
				Result := normal_par_format
			end
		end

--|========================================================================
feature -- Fonts
--|========================================================================

	sans_serif_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (K_ff_sans, wt, it, ps)
			Result.preferred_families.extend ("Arial")
		end

	--|--------------------------------------------------------------

	times_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (K_ff_times, wt, it, ps)
			Result.preferred_families.extend ("Times New Roman")
		end

	--|--------------------------------------------------------------

	fixed_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (K_ff_fixed, wt, it, ps)
			Result.preferred_families.extend ("Courier New")
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	ev_font_constants: EV_FONT_CONSTANTS
			-- Family_screen, Family_roman, Family_sans,
			-- Family_typewriter, Family_modern
			-- Weight_thin, Weight_regular, Weight_bold, Weight_black
			-- Shape_regular, Shape_italic
		once
			create Result
		end

	ev_colors: EV_STOCK_COLORS
		once
			create Result
		end

	font_weight (bf: BOOLEAN): INTEGER
		do
			if bf then
				Result := K_fw_bold
			else
				Result := K_fw_regular
			end
		end

	font_style (itf: BOOLEAN): INTEGER
		do
			if itf then
				Result := K_fs_italic
			else
				Result := K_fs_regular
			end
		end

	K_ff_times: INTEGER once Result := ev_font_constants.Family_roman end
	K_ff_sans: INTEGER once Result := ev_font_constants.Family_sans end
	K_ff_fixed: INTEGER once Result := ev_font_constants.Family_typewriter end
	K_ff_modern: INTEGER once Result := ev_font_constants.Family_sans end

	K_fw_thin: INTEGER once Result := ev_font_constants.Weight_thin end
	K_fw_regular: INTEGER once Result := ev_font_constants.Weight_regular end
	K_fw_bold: INTEGER once Result := ev_font_constants.Weight_bold end
	K_fw_black: INTEGER once Result := ev_font_constants.Weight_black end

	K_fs_regular: INTEGER once Result := ev_font_constants.Shape_regular end
	K_fs_italic: INTEGER once Result := ev_font_constants.Shape_italic end

	--|--------------------------------------------------------------
invariant

end -- class AEL_V2S_SDOC_BOX
