note
	description: "{
A dialog in which can be presented a structured USML document and index
Structured document must conform to USML_DOCUMENT
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2S_SDOC_DIALOG

inherit
	AEL_V2S_VBOX_DIALOG
		redefine
			create_interface_objects, build_interface_components,
			initialize_interface_values, initialize_interface_actions,
			initialize_interface_images, post_initialize, on_show,
			control_frame, execute_ok_action, execute_cancel_action,
			window_initial_width, window_initial_height
		end
--execute_ok_action, execute_cancel_action, 
create
	make_with_document, make_no_toc

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_document (ts: STRING; v: like document)
			--
		do
			document := v
			make_with_title (ts)
		end

	make_no_toc (ts: STRING; doc: USML_DOCUMENT)
		do
			toc_disabled := True
			make_with_document (ts, doc)
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create any auxilliary objects needed for MAIN_WINDOW.
			-- Initialization for these objects must be performed in 
			-- `user_initialization'.
		do
			Precursor

			if toc_disabled then
				create doc_box.make_no_toc
			else
				create doc_box.make_with_document (document)
			end
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		do
			Precursor
			main_box.extend (doc_box)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			Precursor
			cancel_button.select_actions.extend (
				agent execute_cancel_action)
		end

	--|--------------------------------------------------------------

	initialize_interface_images
		do
			Precursor
			show_icon_and_closer
		end

	--|--------------------------------------------------------------

	post_initialize
		do
			Precursor
			hide_ok_button
			-- ensure that 'X' in title bar also closes window
			-- BUT, to do so also enables Escape to close window
			set_default_cancel_button (cancel_button)
			cancel_button.set_text ("Close")
		end

--|========================================================================
feature -- Status
--|========================================================================

	was_shown: BOOLEAN

	document: USML_DOCUMENT

	destroy_on_close_enabled: BOOLEAN
			-- Destroy window on close?
			-- Default is to hide

	toc_disabled: BOOLEAN

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_destroy_on_close
		do
			destroy_on_close_enabled := True
		end

	disable_destroy_on_close
		do
			destroy_on_close_enabled := False
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Agents and actions
--|========================================================================

	on_show
		do
			Precursor
			if not was_shown then
				was_shown := True
				doc_box.show
			end
			control_frame.show
		end

	--|--------------------------------------------------------------

	execute_cancel_action
		do
			if destroy_on_close_enabled then
				destroy
			else
				hide
			end
		end

	execute_ok_action
		do
			if destroy_on_close_enabled then
				destroy
			else
				hide
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature -- Components
--|========================================================================

	doc_box: USML_DOC_BOX
	control_frame: AEL_V2S_RELATIVE_DIALOG_CTL_FRAME

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	window_initial_width: INTEGER
		do
			Result := 750
		end

	window_initial_height: INTEGER
		do
			Result := 650
		end

	--|--------------------------------------------------------------
invariant

end -- class AEL_V2S_SDOC_DIALOG
