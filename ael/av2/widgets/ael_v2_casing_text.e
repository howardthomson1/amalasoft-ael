note
	description: "A text widget that forces case"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CASING_TEXT

inherit
	EV_PASSWORD_FIELD
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as pf_implementation_attr
		redefine
			default_create_interface_objects, create_implementation,
			pf_implementation_attr, check_text_modification,
			is_in_default_state
		select
			pf_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end

create
	make_for_lower, make_for_upper

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_for_lower
		do
			private_is_lower := True
			private_is_upper := False
			default_create
		end

	--|--------------------------------------------------------------

	make_for_upper
		do
			private_is_upper := True
			private_is_lower := False
			default_create
		end
  
--|========================================================================
feature {EV_ANY, EV_ANY_I} -- Creation and initialization (from default_create)
--|========================================================================

	implementation: like pf_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := pf_implementation_attr
		end

	pf_implementation_attr: AEL_V2_CASING_TEXT_IMP

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			create pf_implementation_attr.make (private_is_upper, private_is_lower)
			implementation_attr := pf_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_PASSWORD_FIELD}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|========================================================================
feature -- Implementation
--|========================================================================
	
	is_in_default_state: BOOLEAN
		do
			Result := True
		end

 --|========================================================================
feature
 --|========================================================================

	private_is_upper : BOOLEAN
	private_is_lower : BOOLEAN

	is_lower : BOOLEAN
			-- Are all chars forced to lower case?
		do
			if implementation /= Void then
				Result := implementation.is_lower
			end
		end

	is_upper : BOOLEAN
			-- Are all chars forced to upper case?
		do
			if implementation /= Void then
				Result := implementation.is_upper
			end
		end

 --|------------------------------------------------------------------------

	check_text_modification (old_text, added_text: STRING_32): BOOLEAN
			-- Ensure that `text' is equal to `old_text' + `added_text' with
			-- all %R removed.
		local
			s0, s1, s2: STRING
		do
			Result := Precursor (old_text, added_text)
			if not Result then
				s0 := text.twin
				s1 := old_text.twin
				s2 := added_text.twin
				s0.to_upper
				s1.to_upper
				s2.to_upper
				Result := s0.is_equal (s1 + s2)
			end
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using either make_for_lower or 
--| make_for_upper.
--| After creation, manipulate it as you would a EV_TEXT widget.
--|----------------------------------------------------------------------

end -- class AEL_V2_CASING_TEXT
