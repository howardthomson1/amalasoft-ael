note
	description: "A cell and label, with button-like behavior"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CELLBTN

inherit
	AEL_V2_CELL
		rename
			background_color as cell_background_color,
			foreground_color as cell_foreground_color,
			set_background_color as set_cell_background_color,
			set_foreground_color as set_cell_foreground_color
		redefine
			create_interface_objects, build_interface_components,
			initialize_interface_actions, update_widget_rendition,
			enable_sensitive, disable_sensitive
		end

create
	make, make_with_text, make_with_pbufs

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_text (v: READABLE_STRING_GENERAL)
			-- Create `Current' with text 'v'
		do
			make
			set_text (v)
		end

	make_with_pbufs (spm, ipm: EV_PIXEL_BUFFER)
			-- Create `Current' with sensitive pbuf 'spm' and 
			-- insensitive pbuf 'ipm'
		do
			in_pixmap_mode := True
			sensitive_pbuf := spm
			insensitive_pbuf := ipm
			make
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create select_actions.make
			create label
		end

	build_interface_components
			-- Assemble interface components previously created
		do
			Precursor
			if in_pixmap_mode then
				if attached sensitive_pixmap as pm then
					extend (pm)
					pm.pointer_button_release_actions.extend (agent on_mouse_up)
				else
					-- should never happen, but ..
					extend (label)
				end
			else
				extend (label)
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			label.pointer_button_press_actions.extend (agent on_mouse_down)
			label.pointer_button_release_actions.extend (agent on_mouse_up)
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	in_pixmap_mode: BOOLEAN
			-- Is Current in pixmap mode? (vs text mode)

	is_selected: BOOLEAN
			-- client-settable selection state
			-- N.B. this is set by client only, NOT autonomously by Current

	select_actions: LINKED_LIST [PROCEDURE]
			-- Actions to be performed when Current is selected.

	label: EV_LABEL
			-- Label presenting "button" text


	text: READABLE_STRING_GENERAL
		do
			Result := label.text
		end

	background_color: EV_COLOR
		do
			Result := label.background_color
		end

	foreground_color: EV_COLOR
		do
			Result := label.foreground_color
		end

--RFO 	sensitive_color: detachable EV_COLOR
--RFO 	insensitive_color: detachable EV_COLOR

	sensitive_pixmap: detachable EV_PIXMAP
		do
			if attached sensitive_pbuf as pb then
				create Result.make_with_pixel_buffer (pb)
			end
		end

	insensitive_pixmap: detachable EV_PIXMAP
		do
			if attached insensitive_pbuf as pb then
				create Result.make_with_pixel_buffer (pb)
			end
		end

	sensitive_pbuf: detachable EV_PIXEL_BUFFER
	insensitive_pbuf: detachable EV_PIXEL_BUFFER

--|========================================================================
feature -- Status setting
--|========================================================================

	align_text_left
		do
			label.align_text_left
		end

	align_text_right
		do
			label.align_text_right
		end

	align_text_center
		do
			label.align_text_center
		end

	--|--------------------------------------------------------------

	align_text_top
		do
			label.align_text_top
		end

	align_text_bottom
		do
			label.align_text_bottom
		end

	align_text_middle
		do
			label.align_text_vertical_center
		end

	--|--------------------------------------------------------------

	enable_select
		do
			is_selected := True
			call_select_actions
		end

	disable_select
		do
			is_selected := False
		end

	toggle_select
		do
			is_selected := not is_selected
			if is_selected then
				call_select_actions
			end
		end

	--|--------------------------------------------------------------

	enable_sensitive
		do
			if in_pixmap_mode then
				if attached sensitive_pixmap as pm then
					replace (pm)
					pm.pointer_button_release_actions.extend (agent on_mouse_up)
				else
				end
			else
				if attached sensitive_render_proc as proc then
					proc.call (True)
--RFO 			elseif attached sensitive_color as c then
--RFO 				label.set_background_color (c)
				end
			end
			Precursor
		end

	disable_sensitive
		do
			if in_pixmap_mode then
				if attached sensitive_pixmap as pm then
					replace (pm)
					pm.pointer_button_release_actions.extend (agent on_mouse_up)
				else
				end
			else
				if attached sensitive_render_proc as proc then
					proc.call (False)
--RFO 			elseif attached insensitive_color as c then
--RFO 				label.set_background_color (c)
				end
			end
			Precursor
		end

	--|--------------------------------------------------------------

	set_background_color (v: EV_COLOR)
		do
			label.set_background_color (v)
		end

	set_foreground_color (v: EV_COLOR)
		do
			label.set_foreground_color (v)
		end

	--|--------------------------------------------------------------

	--RFO set_sensitive_color (v: like sensitive_color)
	--RFO 	do
	--RFO 		sensitive_color := v
	--RFO 	end

	--RFO set_insensitive_color (v: like insensitive_color)
	--RFO 	do
	--RFO 		insensitive_color := v
	--RFO 	end

	--|--------------------------------------------------------------

	set_text (v: READABLE_STRING_GENERAL)
		do
			if in_pixmap_mode then
				in_pixmap_mode := False
				create label.make_with_text (v)
				replace (label)
			else
				label.set_text (v)
			end
		end

--RFO 	set_data (v: detachable ANY)
--RFO 		do
--RFO 			data := v
--RFO 		end

	set_tooltip (v: STRING)
		do
			label.set_tooltip (v)
		end

	set_font (v: EV_FONT)
		do
			label.set_font (v)
		end

	set_sensitive_render_proc (v: like sensitive_render_proc)
		do
			sensitive_render_proc := v
		end

	set_pixel_buffers (spm, ipm: EV_PIXEL_BUFFER)
			-- Set sensitive and insensitive pbufs to spm and ipm
			-- Reset mode to pixmap mode
		do
			in_pixmap_mode := True
			sensitive_pbuf := spm
			insensitive_pbuf := ipm
			if attached sensitive_pixmap as pm then
				replace (pm)
				pm.pointer_button_release_actions.extend (agent on_mouse_up)
			end
		end

	--|--------------------------------------------------------------

	update_widget_rendition
		do
			Precursor
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	sensitive_render_proc: detachable PROCEDURE [BOOLEAN]

	mouse_pending: BOOLEAN
			-- Is a mouse up event likely to be coming?

	mouse_timer: detachable EV_TIMEOUT
		note
			option: stable attribute
		end

	clear_mouse_timer
		do
			mouse_pending := False
			if attached mouse_timer as wt then
				wt.actions.wipe_out
				wt.destroy
			end
		end

	on_mouse_down (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-down event on cell
		do
			if b = 1 then
				mouse_pending := True
				create mouse_timer.make_with_interval (200)
				mouse_timer.actions.extend (agent clear_mouse_timer)
			end
		end

	on_mouse_up (
		xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to button-up event on cell
		do
			if b = 1 and mouse_pending then
				clear_mouse_timer
				call_select_actions
			end
		end

	processing_select_event: BOOLEAN

	call_select_actions
		do
			if not processing_select_event then
				processing_select_event := True
				from select_actions.start
				until select_actions.after
				loop
					select_actions.item.call
					select_actions.forth
				end
				processing_select_event := False
			end
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 13-Jul-2020
--|     Adapted from AEL_V2_PIXMAP_CELLBTN
--|     Compiled and tested using Eiffel 8.7
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make' or 'make_with_text'
--|----------------------------------------------------------------------

end -- class AEL_V2_CELLBTN
