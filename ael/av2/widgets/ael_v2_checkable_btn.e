note
	description: "{
A compound widget, with the appearance and behavior of a check button, but without label (and therefore w/o extra space for it)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2014 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_CHECKABLE_BTN

inherit
	AEL_V2_HORIZONTAL_BOX
		redefine
			create_interface_objects, build_interface_components,
			update_widget_rendition, initialize_interface_actions
		end

create
	make, make_selected

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_selected
		do
			is_selected := True
			make
		end
	
--|========================================================================
feature -- Status
--|========================================================================

	is_selected: BOOLEAN
			-- Is Current selected?

	selected_mark: STRING
			-- String denoting selected state of Current
		do
			if attached private_selected_mark as ts then
				Result := ts
			else
				Result := Ks_dflt_selected_mark
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_select
			-- Set Current's state to be selected
		do
			is_selected := True
			btn.set_text (selected_mark)
			btn.enable_select
		end

	disable_select
			-- Set Current's state to be selected
		do
			is_selected := True
			btn.set_text (Ks_dflt_deselected_mark)
			btn.disable_select
		end

	set_selected_mark (v: detachable STRING)
			-- Set, to 'v', a custom mark to denote selected state
			-- Void resets to default
		do
			if attached v as tv then
				private_selected_mark := tv.twin
			else
				private_selected_mark := Void
			end
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			create btn
			create select_actions
		end

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			btn.select_actions.extend (agent on_btn_select)
		end

--|========================================================================
feature {NONE} -- Interface initialization
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		local
			tc: EV_CELL
		do
			Precursor

			create tc
			extend (tc)

			extend (btn)
			btn.align_text_left
			btn.set_text (selected_mark)
			btn.set_minimum_height (20)
			disable_item_expand (btn)
			btn.set_text (Ks_dflt_deselected_mark)
			--btn.disable_sensitive

			create tc
			extend (tc)
		end

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
			Precursor
		end

--|========================================================================
feature -- Access
--|========================================================================

	select_actions: EV_NOTIFY_ACTION_SEQUENCE
			-- Actions to be performed when Current is selected.

--|========================================================================
feature -- Components
--|========================================================================

	btn: EV_TOGGLE_BUTTON

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Actions
--|========================================================================

	on_btn_select
			-- toggle representation to match state
			-- notify clients that have added select actions
		do
			if btn.is_selected then
				btn.set_text (selected_mark)
				is_selected := True
			else
				btn.set_text (Ks_dflt_deselected_mark)
				is_selected := False
			end
			from select_actions.start
			until select_actions.after
			loop
				select_actions.call
				select_actions.forth
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_selected_mark: detachable STRING
			-- Non-default, client-settable (via 'set_selected_mark')
			-- mark to use to denote selected state of Current

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_dflt_selected_mark: STRING = "X"
			-- Default mark denoting selected state

	Ks_dflt_deselected_mark: STRING = " "
			-- Default mark denoting DEselected state

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Sep-2014
--|     Created original module (Eiffel 14.05)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_CHECKABLE_BTN
