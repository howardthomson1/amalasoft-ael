note
	description: "Compound widget with a button and label, for selecting a font"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2007-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_FONT_BUTTON

inherit
	AEL_V2_LABELED_BUTTON
		redefine
			default_create_interface_objects,
			build_interface_components,
			on_button_press
		end

create
	make, make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (lb: STRING; f: EV_FONT; pf: BOOLEAN)
			-- Create a new widget with the label string 'lb', intial font 'f'
			-- and with text aligned either to right of button (default) or
			-- to left of button (if 'pf' is True)
		require
			label_exists: lb /= Void
		do
			button_font := f
			make_with_label (lb, pf)
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			-- Need to create private_button_color here to support
			-- default_create
			button_font := K_font_button_dflt
			Precursor {AEL_V2_LABELED_BUTTON}
		end

	--|--------------------------------------------------------------

	build_interface_components
		do
			draw_button
			Precursor
		end

	--|--------------------------------------------------------------

	draw_button
		local
			pm: EV_PIXMAP
			tf: EV_FONT
			sw: INTEGER
		do
			create pm.make_with_size (pixmap_width, pixmap_height)
			pm.set_background_color (colors.white)
			pm.set_foreground_color (colors.black)
			pm.clear
			tf := button_font.twin
			tf.set_height (pixmap_height)
			sw := tf.string_width ("A")
			pm.set_font (tf)
			pm.draw_text_top_left (0, 0, "A")
			button.set_pixmap (pm)
		end

--|========================================================================
feature -- Status
--|========================================================================

	button_font: EV_FONT
			-- Font used in button

	--|--------------------------------------------------------------

	pixmap_width: INTEGER
		do
			Result := private_pixmap_width
			if Result = 0 then
				Result := K_dflt_pixmap_width
			end
		end

	pixmap_height: INTEGER
		do
			Result := private_pixmap_height
			if Result = 0 then
				Result := K_dflt_pixmap_height
			end
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press
		local
			cd: EV_FONT_DIALOG
		do
			create cd.make_with_title (label_text)
			if attached button.font as f then
				cd.set_font (f)
			end
			cd.show_modal_to_window (nearest_window)
			set_button_font (cd.font)
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_button_font (f: EV_FONT)
		do
			button_font := f
			draw_button
		end

	--|--------------------------------------------------------------

	set_pixmap_size (w, h: INTEGER)
		do
			private_pixmap_width := w
			private_pixmap_height := h
			draw_button
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_pixmap_width: INTEGER
	private_pixmap_height: INTEGER

	K_dflt_pixmap_width: INTEGER = 12
	K_dflt_pixmap_height: INTEGER = 12

	K_font_button_dflt: EV_FONT
		once
			Result := fonts.times_new_roman_font (12, False, False)
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| The button has an inset showing a settable font.
--| Default action on the button invokes the font dialog, the selection
--| from which then becomes the inset's font.  The inset shows a capital
--| 'A' in the selected font, but with a height proportional to the
--| height of the button.
--|
--| To use this widget, create it using 'make_with_attributes',
--| giving it a label string, the current font, and a position flag, 
--| or by using 'make' and subsequently setting the attributes.
--|
--| Then add to a container and disable expansion of this widget if needed.
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--|----------------------------------------------------------------------

end -- class AEL_V2_FONT_BUTTON
