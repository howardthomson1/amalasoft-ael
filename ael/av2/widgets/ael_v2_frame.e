note
	description: "A container, with a visible fram, holding a single item"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_FRAME

inherit
	EV_FRAME
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as fr_implementation_attr
		redefine
			make_with_text,
			default_create_interface_objects, create_implementation
		select
			fr_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end

create
	make, make_with_text

--|========================================================================
feature {NONE} -- Creation and initialization (from default_create)
--|========================================================================

	make_with_text (v: STRING)
			-- Create Current with frame label text 'v'
		do
			make
			set_text (v)
		end

	--|--------------------------------------------------------------

	implementation: like fr_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := fr_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_FRAME}
			implementation_attr := fr_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_FRAME}
			create_implementation
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, treat as an EV_FRAME, except for using
--| and of the features exclusive to this class.
--|----------------------------------------------------------------------

end -- class AEL_V2_FRAME
