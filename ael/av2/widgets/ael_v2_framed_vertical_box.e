note
	description: "{
A container, with a visible frame, holding multiple items, stacked vertically
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_FRAMED_VERTICAL_BOX

inherit
	EV_FRAME
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as fr_implementation_attr,
			has as frame_has,
			border_width as frame_border_width,
			set_border_width as frame_set_border_width
		redefine
			default_create_interface_objects, create_implementation
		select
			fr_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization (from default_create)
--|========================================================================

	implementation: like fr_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := fr_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_FRAME}
			implementation_attr := fr_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_FRAME}
			Precursor {AEL_V2_WIDGET}
			create box
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	box: EV_VERTICAL_BOX
			-- Actual container of items

 --|========================================================================
feature -- Status
 --|========================================================================

	has (v: like item): BOOLEAN
			-- Does Current include `v'?
		do
			Result := box.has (v)
		end

	is_homogeneous: BOOLEAN
			-- Are all items forced to have same dimensions.
		do
			Result := box.is_homogeneous
		end

	border_width: INTEGER
			-- Width of border around container in pixels.
		do
			Result := box.border_width
		end

	padding_width, padding: INTEGER
			-- Space between children in pixels.
		do
			Result := box.padding
		end 

 --|========================================================================
feature -- Status report
 --|========================================================================

	is_item_expanded (an_item: EV_WIDGET): BOOLEAN
			-- Is `an_item' expanded to occupy available spare space?
		require
			not_destroyed: not is_destroyed
			has_an_item: has (an_item)
		do
			Result := box.is_item_expanded (an_item)
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================
	
	enable_homogeneous
			-- Force all items to have same dimensions.
		require
			not_destroyed: not is_destroyed
		do
			box.enable_homogeneous
		ensure
			is_homogeneous: is_homogeneous
		end

	disable_homogeneous
			-- Allow items to have different dimensions.
		require
			not_destroyed: not is_destroyed
		do
			box.disable_homogeneous
		ensure
			not_is_homogeneous: not is_homogeneous
		end

	set_border_width (value: INTEGER)
			-- Assign `value' to `border_width'.
		require
			not_destroyed: not is_destroyed
			positive_value: value >= 0
		do
			box.set_border_width (value)
		ensure
			border_width_assigned: border_width = value
		end

	set_padding_width, set_padding (value: INTEGER)
			-- Assign `value' to `padding_width'.
		require
			not_destroyed: not is_destroyed
			positive_value: value >= 0
		do
			box.set_padding (value)
		ensure
			padding_assigned: padding = value
		end

	enable_item_expand (an_item: EV_WIDGET)
			-- Expand `an_item' to occupy available spare space.
		require
			not_destroyed: not is_destroyed
			has_an_item: has (an_item)
		do
			box.enable_item_expand (an_item)
		ensure
			an_item_expanded: is_item_expanded (an_item)
		end

	disable_item_expand (an_item: EV_WIDGET)
			-- Do not expand `an_item' to occupy available spare space.
		require
			not_destroyed: not is_destroyed
			has_an_item: has (an_item)
		do
			box.enable_item_expand (an_item)
		ensure
			not_an_item_expanded: not is_item_expanded (an_item)
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, treat as an EV_VERTICAL_BOX, except for using
--| the features exclusive to this class.
--|----------------------------------------------------------------------

end -- class AEL_V2_FRAMED_VERTICAL_BOX
