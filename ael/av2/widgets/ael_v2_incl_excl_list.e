note
	description: "{
Compound widget with 2 parallel lists in scrolling windows,
width controls to move items between them
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2020 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2020/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_INCL_EXCL_LIST

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_values,
			initialize_interface_actions,
			post_initialize
		end

create
	make, make_with_labels

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	make_with_labels (lb1, lb2: STRING; lf: BOOLEAN)
			-- Create Current with left list label 'lb1'
			-- and right list label 'lb2'
			-- If 'lf' then left list is primary, else right list is
			-- Items are added first to the primary
		do
			left_is_primary := lf
			private_left_label_str := lb1
			private_right_label_str := lb2
			make
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create left_items.make (29)
			create right_items.make (29)
			create left_label.make_with_text (left_label_str)
			create right_label.make_with_text (right_label_str)
			create left_scrollwin
			create right_scrollwin
			create left_listw
			create right_listw
			create left_arrow_btn.make_with_pixmaps (
				new_left_arrow_pm, new_left_arrow_ins_pm)
			create right_arrow_btn.make_with_pixmaps (
				new_right_arrow_pm, new_right_arrow_ins_pm)
 			idle_action := agent update_geometry
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		local
			ohb, hb: EV_HORIZONTAL_BOX
			lvb, cvb, rvb: EV_VERTICAL_BOX
			ofr, tf: EV_FRAME
			llb, rlb: EV_LABEL
			tc: EV_CELL
		do
			-- start with outer hb, add vertical boxes for each major 
			-- element left, center, right
			create ofr
			extend (ofr)

			create ohb
			ofr.extend (ohb)
			--ohb.set_minimum_height (18)
			--			disable_item_expand (ohb)

			create lvb
			ohb.extend (lvb)
			--create llb.make_with_text (left_label_str)
			llb := left_label
			lvb.extend (llb)
			llb.set_minimum_height (18)
			lvb.disable_item_expand (llb)

			create tf
			lvb.extend (tf)
			tf.set_minimum_width (K_dflt_column_1_width)

			tf.extend (left_scrollwin)

			create hb
			left_scrollwin.extend (hb)
			left_scrollwin.set_item_size (150, 200)
			left_scrollwin.hide_horizontal_scroll_bar

			hb.extend (left_listw)

			left_listw.set_minimum_width (K_dflt_column_1_width)
			--hb.disable_item_expand (left_listw)

			-------------------------

			-- for movement controls
			create cvb
			ohb.extend (cvb)
			cvb.set_minimum_width (30)
			ohb.disable_item_expand (cvb)

			create tc
			cvb.extend (tc)

			cvb.extend (left_arrow_btn)
			cvb.disable_item_expand (left_arrow_btn)

			cvb.extend (right_arrow_btn)
			cvb.disable_item_expand (right_arrow_btn)

			create tc
			cvb.extend (tc)

			cvb.set_background_color (ael_colors.white)

			-------------------------

			create rvb
			ohb.extend (rvb)
			--create rlb.make_with_text (right_label_str)
			rlb := right_label
			rvb.extend (rlb)
			rlb.set_minimum_height (18)
			rvb.disable_item_expand (rlb)

			create tf
			rvb.extend (tf)
			tf.set_minimum_width (K_dflt_column_1_width)

			tf.extend (right_scrollwin)

			create hb
			right_scrollwin.extend (hb)
			right_scrollwin.set_item_size (150, 200)
			right_scrollwin.hide_horizontal_scroll_bar

			hb.extend (right_listw)

			right_listw.set_minimum_width (K_dflt_column_1_width)
			--hb.disable_item_expand (right_listw)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
 			resize_actions.extend (agent on_resize)

 			left_listw.select_actions.extend (agent on_select(left_listw))
 			right_listw.select_actions.extend (agent on_select(right_listw))

			left_arrow_btn.select_actions.extend (agent on_left_arrow_select)
			right_arrow_btn.select_actions.extend (agent on_right_arrow_select)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			item_height := 14
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
 			update_geometry
		end

--|========================================================================
feature -- Status
--|========================================================================

	left_is_primary: BOOLEAN
			-- Treat left list a primary? (dflt is right-as-primary)

	left_label_str: STRING
		do
			if attached private_left_label_str as ts then
				Result := ts
			else
				Result := Ks_dflt_left_label_str
			end
		end

	right_label_str: STRING
		do
			if attached private_right_label_str as ts then
				Result := ts
			else
				Result := Ks_dflt_right_label_str
			end
		end

	--|--------------------------------------------------------------

	number_of_items_displayable: INTEGER
			-- Number of items displayable at current height
			-- As scrollwins are same, both values same
		do
			Result := left_scrollwin.height // item_height
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_left_label_str (v: detachable STRING)
		do
			private_left_label_str := v
			left_label.set_text (left_label_str)
		end

	set_right_label_str (v: detachable STRING)
		do
			private_right_label_str := v
			right_label.set_text (right_label_str)
		end

	enable_multiple_selection
		do
			left_listw.enable_multiple_selection
			right_listw.enable_multiple_selection
		end

	--|--------------------------------------------------------------

	disable_multiple_selection
		do
			left_listw.disable_multiple_selection
			right_listw.disable_multiple_selection
		end

	set_selection_notify_proc (v: like selection_notify_proc)
		do
			selection_notify_proc := v
		end

	set_move_notify_proc (v: like move_notify_proc)
		do
			move_notify_proc := v
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_item (v: STRING; d: detachable ANY)
			-- Add a new row to the PRIMARY list, with the given name
			-- Optionally, associate data 'd' with the new item
			-- Item names ('v') must be unique!!!
		require
			fields_exist: v /= Void
 			is_unique: not has_item_by_name (v)
			not_destroyed: not is_destroyed
		local
			--tr: like item_anchor
		do
			if left_is_primary then
				add_item_left (v, d)
			else
				add_item_right (v, d)
			end
		ensure
 			item_added: has_primary_item_by_name (v)
		end

	--|--------------------------------------------------------------

	add_item_left (v: STRING; d: detachable ANY)
			-- Add a new row to the left list, with the given name
			-- Optionally, associate data 'd' with the new item
			-- Item names ('v') must be unique!!!
		require
			fields_exist: v /= Void
 			is_unique: not has_item_by_name (v)
			not_destroyed: not is_destroyed
		local
			tr: like item_anchor
		do
			create tr.make_with_text (v)
			if attached d as td then
				tr.set_data (td)
			end
			item_height := item_height.max (tr.height)
			left_listw.extend (tr)
			left_items.extend (tr, v)
 			update_geometry
		ensure
 			item_added: has_left_item_by_name (v)
		end

	--|--------------------------------------------------------------

	add_item_right (v: STRING; d: detachable ANY)
			-- Add a new row to the right list, with the given name
			-- Optionally, associate data 'd' with the new item
			-- Item names ('v') must be unique!!!
		require
			fields_exist: v /= Void
 			is_unique: not has_item_by_name (v)
			not_destroyed: not is_destroyed
		local
			tr: like item_anchor
		do
			create tr.make_with_text (v)
			if attached d as td then
				tr.set_data (td)
			end
			item_height := item_height.max (tr.height)
			right_listw.extend (tr)
			right_items.extend (tr, v)
 			update_geometry
		ensure
 			item_added: has_right_item_by_name (v)
		end

	--|--------------------------------------------------------------

	move_item_rl_by_name (v: STRING)
		local
			sl: ARRAYED_LIST [EV_LIST_ITEM]
		do
			if attached right_items.item (v) as ti then
				move_item_rl (ti)
				if attached move_notify_proc as proc then
					create sl.make (1)
					sl.extend (ti)
					proc.call (sl, True)
				end
			else
				-- not there;  ERROR?
			end
		end

	move_item_lr_by_name (v: STRING)
		local
			sl: ARRAYED_LIST [EV_LIST_ITEM]
		do
			if attached left_items.item (v) as ti then
				move_item_lr (ti)
				if attached move_notify_proc as proc then
					create sl.make (1)
					sl.extend (ti)
					proc.call (sl, False)
				end
			else
				-- not there;  ERROR?
			end
		end

	--|--------------------------------------------------------------

	move_item_rl (ti: like item_anchor)
			-- Move item represented by right list item 'ti' from right
			-- list to left list
		local
			tr: like item_anchor
		do
			create tr.make_with_text (ti.text)
			if attached ti.data as td then
				tr.set_data (td)
			end
			left_listw.extend (tr)
			right_listw.prune (ti)
		end

	move_item_lr (ti: like item_anchor)
			-- Move item represented by left list item 'ti' from left 
			-- list to right list
		local
			tr: like item_anchor
		do
			create tr.make_with_text (ti.text)
			if attached ti.data as td then
				tr.set_data (td)
			end
			right_listw.extend (tr)
			left_listw.prune (ti)
		end

--|========================================================================
feature -- Components
--|========================================================================

	left_scrollwin: 	EV_SCROLLABLE_AREA
	right_scrollwin: 	EV_SCROLLABLE_AREA

	left_items: HASH_TABLE [like item_anchor, STRING]
	right_items: HASH_TABLE [like item_anchor, STRING]

	primary_items: like left_items
		do
			if left_is_primary then
				Result := left_items
			else
				Result := right_items
			end
		end

	secondary_items: like left_items
		do
			if left_is_primary then
				Result := right_items
			else
				Result := left_items
			end
		end

	left_listw: EV_LIST
	left_label: EV_LABEL

	right_listw: EV_LIST
	right_label: EV_LABEL

	primary_listw: EV_LIST
		do
			if left_is_primary then
				Result := left_listw
			else
				Result := right_listw
			end
		end

	secondary_listw: EV_LIST
		do
			if left_is_primary then
				Result := right_listw
			else
				Result := left_listw
			end
		end

	left_arrow_btn: AEL_V2_PIXMAP_BUTTON
	right_arrow_btn: AEL_V2_PIXMAP_BUTTON

	new_left_arrow_pm: AEL_V2_ARROW_LEFT_PIXMAP
		do
			create Result.make_with_scale (K_arrow_btn_size)
		end

	new_left_arrow_ins_pm: AEL_V2_ARROW_LEFT_INS_PIXMAP
		do
			create Result.make_with_scale (K_arrow_btn_size)
		end

	new_right_arrow_pm: AEL_V2_ARROW_RIGHT_PIXMAP
		do
			create Result.make_with_scale (K_arrow_btn_size)
		end

	new_right_arrow_ins_pm: AEL_V2_ARROW_RIGHT_INS_PIXMAP
		do
			create Result.make_with_scale (K_arrow_btn_size)
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_left_arrow_select
			-- Move selections from right list to left list
		local
			sl: DYNAMIC_LIST [like item_anchor]
		do
			sl := right_listw.selected_items
			from sl.start
			until sl.after
			loop
				-- move item right to left
				move_item_rl (sl.item)
				sl.forth
			end
			if attached move_notify_proc as proc then
				proc.call (sl, True)
			end
		end

	on_right_arrow_select
			-- Move selections from left list to right list
		local
			sl: DYNAMIC_LIST [like item_anchor]
		do
			sl := left_listw.selected_items
			from sl.start
			until sl.after
			loop
				-- move item left to right
				move_item_lr (sl.item)
				sl.forth
			end
			if attached move_notify_proc as proc then
				proc.call (sl, False)
			end
		end

	--|--------------------------------------------------------------

	on_resize (a, ay, w, h: INTEGER)
		local
			rdt: like refresh_delay_timeout
		do
			if not refresh_delayed then
				create rdt
				rdt.actions.extend (agent setup_for_refresh)
				rdt.set_interval (50)
				refresh_delay_timeout := rdt
			end
		end

	--|--------------------------------------------------------------

	on_select (w: like primary_listw)
		local
		do
			if not updating_selections then
				updating_selections := True
				if attached selection_notify_proc as proc then
					proc.call (w)
				end
				updating_selections := False
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	selected_primary: detachable like item_anchor
		do
			Result := primary_listw.selected_item
		end

	selected_secondary: detachable like item_anchor
		do
			Result := secondary_listw.selected_item
		end

	item_anchor: EV_LIST_ITEM
			-- An attached 'like selected_item'
		require
			never: False
		do
			create Result.make_with_text ("dummy")
		end

	selected_items (pf: BOOLEAN): DYNAMIC_LIST [like item_anchor]
			-- Items selected from list; if 'pf', then from primary 
			-- list, else from secondary list
		do
			if pf then
				Result := primary_listw.selected_items
			else
				Result := secondary_listw.selected_items
			end
		end

	--|--------------------------------------------------------------

	list_has_item_by_name (w: like left_listw; v: STRING): BOOLEAN
			-- Does item 'v' exist in list 'w'?
		do
			if w = left_listw then
				Result := has_left_item_by_name (v)
			else
				Result := has_right_item_by_name (v)
			end
		end

	has_item_by_name (v: STRING): BOOLEAN
			-- Does item 'v' exist in EITHER list?
		do
			Result := has_left_item_by_name (v) or
				has_right_item_by_name (v)
		end

	has_primary_item_by_name (v: STRING): BOOLEAN
			-- Does item 'v' exist in the primary list?
		do
			if left_is_primary then
				Result := left_items.has (v)
			else
				Result := right_items.has (v)
			end
		end

	has_left_item_by_name (v: STRING): BOOLEAN
			-- Does item 'v' exist in the left list?
		do
			Result := left_items.has (v)
		end

	has_right_item_by_name (v: STRING): BOOLEAN
			-- Does item 'v' exist in the right list?
		do
			Result := right_items.has (v)
		end

	--|--------------------------------------------------------------

	item_by_name (v: STRING): detachable like item_anchor
			-- Item, from EITHER list with text value 'v'
		do
			if attached left_item_by_name (v) as li then
				Result := li
			else
				Result := right_item_by_name (v)
			end
		end

	left_item_by_name (v: STRING): detachable like item_anchor
		local
			oc: CURSOR
		do
			oc := left_listw.cursor
			from left_listw.start
			until left_listw.after or attached Result
			loop
				if left_listw.item.text.is_equal (v) then
					Result := left_listw.item
				else
					left_listw.forth
				end
			end
			left_listw.go_to (oc)
		end

	right_item_by_name (v: STRING): detachable like item_anchor
		local
			oc: CURSOR
		do
			oc := right_listw.cursor
			from right_listw.start
			until right_listw.after or attached Result
			loop
				if right_listw.item.text.is_equal (v) then
					Result := right_listw.item
				else
					right_listw.forth
				end
			end
			right_listw.go_to (oc)
		end

	--|--------------------------------------------------------------

	item_height: INTEGER
			-- Height, in pixels, of items in list

--|========================================================================
feature -- Operation
--|========================================================================

	scroll_to_item (w: like left_listw; v: like item_anchor)
		-- Scroll list 'w' to expose the item 'v'
		require
			list_has_it: list_has_item_by_name (w, v.text)
		local
			pos: INTEGER
			--sw: like left_scrollwin
		do
			pos := w.index_of (v, 1)
			if pos < number_of_items_displayable then
				scroll_to_top (w)
			elseif pos > (w.count - number_of_items_displayable) then
				scroll_to_bottom (w)
			else
				-- Scroll to expose position
				scroll_to_position (w, pos)
			end
		end

	scroll_left_to (v: like item_anchor)
		-- Scroll the left list to expose the given item
		require
			exists: v /= Void
			belongs: left_items.has (v.text)
		local
			pos: INTEGER
		do
			pos := left_listw.index_of (v, 1)
			if pos < number_of_items_displayable then
				scroll_to_top (left_listw)
			elseif pos > (count - number_of_items_displayable) then
				scroll_to_bottom (left_listw)
			else
				-- Scroll to expose position
				scroll_to_position (left_listw, pos)
			end
		end

	scroll_right_to (v: like item_anchor)
		-- Scroll the right list to expose the given item
		require
			exists: v /= Void
			belongs: right_items.has (v.text)
		local
			pos: INTEGER
		do
			pos := right_listw.index_of (v, 1)
			if pos < number_of_items_displayable then
				scroll_to_top (right_listw)
			elseif pos > (count - number_of_items_displayable) then
				scroll_to_bottom (right_listw)
			else
				-- Scroll to expose position
				scroll_to_position (right_listw, pos)
			end
		end

	--|--------------------------------------------------------------

	scroll_to_top (w: like left_listw)
		-- Scroll the window to the top
		local
			sw: like left_scrollwin
		do
			if w = left_listw then
				sw := left_scrollwin
			else
				sw := right_scrollwin
			end
			sw.set_y_offset (0)
		end

	--|--------------------------------------------------------------

	scroll_to_bottom (w: like left_listw)
		-- Scroll the window to the bottom (last item)
		local
			sw: like left_scrollwin
		do
			if w = left_listw then
				sw := left_scrollwin
			else
				sw := right_scrollwin
			end
			sw.set_y_offset ((item_height *
				(sw.count - number_of_items_displayable)).max (0))
		end

	--|--------------------------------------------------------------

	scroll_to_position (w: like left_listw; v: INTEGER)
		-- Scroll the window to expose the given item position
		-- Places 'v' at top of view
		local
			sw: like left_scrollwin
		do
			if w = left_listw then
				sw := left_scrollwin
			else
				sw := right_scrollwin
			end
			sw.set_y_offset ((v - 1) * item_height)
		end

	--|--------------------------------------------------------------

	scroll_to_show_position (w: like left_listw; v: INTEGER)
		-- Scroll the window to expose the given item position
		local
			cpos: INTEGER
			sw: like left_scrollwin
		do
			if w = left_listw then
				sw := left_scrollwin
			else
				sw := right_scrollwin
			end
			cpos := sw.y_offset // item_height
			if (v <= (cpos + 1)) or v >= (cpos + number_of_items_displayable) then
				scroll_to_position (w, v)
			else
				-- Do nothing
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_left_label_str: detachable STRING
	private_right_label_str: detachable STRING

	Ks_dflt_left_label_str: STRING = "Selected"
	Ks_dflt_right_label_str: STRING = "Pool"

	updating_selections: BOOLEAN
	update_pending: BOOLEAN
	update_on_move: BOOLEAN

	refresh_delayed: BOOLEAN
		do
			Result := attached refresh_delay_timeout
		end

	--|--------------------------------------------------------------

	select_items (w: like left_listw; vl: LIST [INTEGER])
		require
			widget_exists: w /= Void
			exists: vl /= Void
			not_destroyed: not is_destroyed
		do
			w.remove_selection
			from vl.start
			until vl.exhausted
			loop
				w.implementation.select_item (vl.item)
				vl.forth
			end
		end

 --|--------------------------------------------------------------

	select_item_at_position (w: like left_listw; p: INTEGER)
		require
			widget_exists: w /= Void
			valid_position: p > 0 and p <= count
			not_destroyed: not is_destroyed
		do
			w.remove_selection
			w.implementation.select_item (p)
		end

--|========================================================================
feature {NONE} -- Movement
--|========================================================================

--|========================================================================
feature {NONE} -- Geometry management
--|========================================================================

	setup_for_refresh
		do
			if refresh_delayed and then attached refresh_delay_timeout as rdt then
				rdt.set_interval (0)
			end
			if not update_pending then
				update_pending := True
				application_root.do_once_on_idle (idle_action)
			end
		end

	--|--------------------------------------------------------------

	update_geometry
		local
			swh, lih, rih: INTEGER
		do
			if refresh_delayed and then attached refresh_delay_timeout as rdt then
				rdt.set_interval (0)
			end
--			left_listw.set_minimum_width (width - right_listw.width)
			update_pending := False
			refresh_delay_timeout := Void
			if not left_listw.is_empty then
				lih := item_height.max (left_listw.first.height)
			end
			if not right_listw.is_empty then
				rih := item_height.max (right_listw.first.height)
			end
			item_height := item_height.max (lih).max (rih)

			swh := height.max(left_listw.count * (item_height + 1)).max (200)
			left_scrollwin.set_item_size (width, swh)
			swh := height.max(right_listw.count * (item_height + 1)).max (200)
			right_scrollwin.set_item_size (width, swh)
		end

	--|--------------------------------------------------------------

 	idle_action: PROCEDURE

	selection_notify_proc: detachable PROCEDURE [like left_listw]
			-- Client procedure to call when an item is selected
			-- Arg is list in which select occurred

	move_notify_proc: detachable PROCEDURE [LIST [like item_anchor], BOOLEAN]
			-- Client procedure to call when items are moved from one 
			-- list to another.
			--  2nd arg denotes direction; R-L if True, L-R if False

	refresh_delay_timeout: detachable EV_TIMEOUT

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_dflt_column_1_width: INTEGER = 30

	K_arrow_btn_size: INTEGER = 16

	K_items_per_page: INTEGER = 5

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v2_ranking list
--|     Compiled and tested using Eiffel 8.1
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| 
--| Items can be added to the list using the 'add_item' procedure.
--| Items are simple strings, but can have data associated with them if 
--| desired.
--| 
--| As items are added, they are added to the 'primary' list (either
--| left or right, depending on designation.
--| Items in either list can be moved to the other list using the
--| directional controls.
--|----------------------------------------------------------------------

end -- class AEL_V2_INCL_EXCL_LIST
