note
	description: "A text label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABEL

inherit
	EV_LABEL
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as lbl_implementation_attr
		redefine
			default_create_interface_objects, create_implementation,
			make_with_text
		select
			lbl_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end

create
	make, make_with_text

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_text (v: READABLE_STRING_GENERAL)
			-- Create `Current' and assign `v' to `text'
		do
			Precursor (v)
			complete_initialization
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

	implementation: like lbl_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := lbl_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_LABEL}
			implementation_attr := lbl_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_LABEL}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make' or 'make_with_text'.
--|----------------------------------------------------------------------

end -- class AEL_V2_LABEL
