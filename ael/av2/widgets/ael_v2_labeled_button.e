note
	description: "Compound widget with a button and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_BUTTON

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as button
		redefine
			initialize_interface_actions
		end

create
	make, make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	initialize_interface_actions
		do
			button.select_actions.extend (agent on_button_press)
			Precursor
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create button
		end

--|========================================================================
feature -- Components
--|========================================================================

	button: EV_BUTTON

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press
		do
			notify
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using 
--| 'make' and subsequently setting the label text.
--|
--| Then add to your container and disable expansion of this widget if needed.
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--| This widget by itself is not very interesting or useful, but its
--| descendents are.  Descendents include AEL_V2_COLOR_BUTTON and
--| AEL_V2_FONT_BUTTON.
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_BUTTON
