note
	description: "Compound widget with a text field and a label, where %
                %the text field forces case"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_CASING_TEXT

inherit
	AEL_V2_LABELED_TEXT
		redefine
			textw, create_widget
		end

create
	make_for_lower, make_for_upper

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_for_lower (lb: detachable STRING; pf: BOOLEAN)
		do
			if attached lb as alb then
				make_with_label (alb, pf)
			else
				make
			end
		end

	--|--------------------------------------------------------------

	make_for_upper (lb: detachable STRING; pf: BOOLEAN)
		do
			is_upper := True
			if attached lb as alb then
				make_with_label (alb, pf)
			else
				make
			end
		end

	--|--------------------------------------------------------------  

	create_widget
		do
			if is_upper then
				create textw.make_for_upper
			else
				create textw.make_for_lower
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_upper: BOOLEAN

	is_lower: BOOLEAN
		do
			Result := not is_upper
		end

--|========================================================================
feature -- Components
--|========================================================================

	textw: AEL_V2_CASING_TEXT

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 28-Jul-2018
--|     Original module;  Eiffel 18.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using either make_for_lower or 
--| make_for_upper, giving it a string to use for the label component,
--|  or using subsequently setting the label text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--| If you wish to receive a notification on commit (enter actions) of the
--| string in the text widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'text' routine for the committed value.
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_CASING_TEXT
