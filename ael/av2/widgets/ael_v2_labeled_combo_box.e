note
	description: "Compound widget with a combo box and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_COMBO_BOX

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as combo_box
		redefine
			default_create_interface_objects,
			initialize_interface_actions,
			initialize_interface_values
		end

create
	make, make_with_label, make_with_label_and_strings

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================
	
	make_with_label_and_strings (lb: STRING; sl: ARRAY [STRING]; pf: BOOLEAN)
		do
			make_with_label (lb, pf)
			set_strings (sl)
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			-- Need to create label_text here to support default_create
			create box_strings.make (0)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_values
		do
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			combo_box.select_actions.extend (agent on_select)
			Precursor
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create combo_box
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_strings (sa: INDEXABLE [READABLE_STRING_GENERAL, INTEGER])
			-- Wipe out and re-initialize with an item
			-- for each of `sa'.
		require
			exists: sa /= Void
		local
			i, lim: INTEGER
		do
			box_strings.wipe_out
			lim := sa.upper
			from i := sa.lower
			until i > lim
			loop
				box_strings.extend (sa.item (i))
				i := i + 1
			end
			combo_box.set_strings (box_strings)
		end

	set_selected_text (v: READABLE_STRING_GENERAL)
			-- Set the selected item to the one with text 'v'
		do
			combo_box.set_text (v)
		end

--|========================================================================
feature -- Components
--|========================================================================

	combo_box: EV_COMBO_BOX
			--	Combo box contained within Current

--	box_strings: INDEXABLE [READABLE_STRING_GENERAL, INTEGER]
	box_strings: ARRAYED_LIST [READABLE_STRING_GENERAL]
			-- Strings with which to initialize combo box items

	text: STRING
			-- Text of combo box current selection
		do
			Result := combo_box.text
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_select
			-- Respond to a select event from the combo box
		do
			notify
		end
	
--|========================================================================
feature -- Status setting
--|========================================================================

	disable_edit
			-- Disable editing text in the combo box
		do
			combo_box.disable_edit
		end

	enable_edit
			-- Enable editing text in the combo box
		do
			combo_box.enable_edit
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label', or using
--| 'make_with_label_and_strings', giving it a string to use as the
--| widget label and optionally as item labels.  You can create this
--| widget using 'make' and subsequently setting label and 
--| text string.
--|
--| Add to a container and disable expansion of this widget if needed.
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_COMBO_BOX
