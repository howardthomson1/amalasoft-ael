note
	description: "Compound widget with a horizontal range and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_HRANGE

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as range
		redefine
			create_widget, initialize_interface_actions,
			initialize_interface_values, update_widget_rendition
		end

create
	make, make_with_attributes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_attributes (r1, r2: INTEGER; t: STRING; pf: BOOLEAN)
			-- Create a new widget with a value range of r1 to r2 and a label
			-- of 't'.
			-- If position flag 'pf' is True, place the label to the left of
			-- the range, else place the label to the right.
		require
			range_valid:  is_valid_range (r1, r2)
			label_exists: t /= Void and then not t.is_empty
		do
			minimum_value := r1
			maximum_value := r2
			make_with_label (t, pf)
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create range
		end

	--|--------------------------------------------------------------

	initialize_interface_values
		do
			if has_valid_range then
				update_range_values
			end
			range.set_minimum_width (default_range_width)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			range.change_actions.extend (agent on_value_change)
			Precursor
		end

--|========================================================================
feature {NONE} -- Widget rendition updates
--|========================================================================

	update_range_values
		do
			range.value_range.resize_exactly (minimum_value, maximum_value)
			range.set_value (initial_value)
		end

	--|--------------------------------------------------------------

	update_widget_rendition
		do
			update_label_text
			update_range_values
			Precursor
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_value_change (v: INTEGER)
		do
			notify
		end

--|========================================================================
feature -- Widget actions
--|========================================================================

	change_actions: EV_VALUE_CHANGE_ACTION_SEQUENCE
		do
			Result := range.change_actions
		end

--|========================================================================
feature -- Components
--|========================================================================

	range: EV_HORIZONTAL_RANGE

--|========================================================================
feature -- Values
--|========================================================================

	value: INTEGER
		do
			Result := range.value
		end

	--|--------------------------------------------------------------

	minimum_value: INTEGER
	maximum_value: INTEGER

	initial_value: INTEGER
		do
			Result := private_initial_value.max (minimum_value)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_range (minv, maxv: INTEGER): BOOLEAN
			-- Is the range delimited by 'minv' and 'maxv' valid?
		do
			Result := minv <= maxv
		end

	--|--------------------------------------------------------------

	has_valid_range: BOOLEAN
		do
			Result := is_valid_range (minimum_value, maximum_value)
		end

	--|--------------------------------------------------------------

	value_is_in_range (v: INTEGER): BOOLEAN
			-- Is value 'v' in range?
		do
			Result := v >= minimum_value and v <= maximum_value
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_value (v: INTEGER)
		require
			in_range: value_is_in_range (v)
		do
			range.set_value (v)
		end

	--|--------------------------------------------------------------

	set_initial_value (v: INTEGER)
		require
			in_range: value_is_in_range (v)
		do
			private_initial_value := v
			update_range_values
		end

	--|--------------------------------------------------------------

	set_value_range (minv, maxv: INTEGER)
		require
			valid: minv <= maxv
		do
			minimum_value := minv
			maximum_value := maxv
			range.value_range.resize_exactly (minimum_value, maximum_value)
			--update_range_values
		end

	set_step (v: INTEGER)
		do
			range.set_step (v)
		end

	--|--------------------------------------------------------------

	set_range_width (v: INTEGER)
		require
			positive: v > 0
		do
			enable_item_expand (range)
			range.set_minimum_width (v)
			disable_item_expand (range)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_initial_value: INTEGER

	default_range_width: INTEGER
		do
			Result := 100
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_attributes', giving
--| it a minimum and maximum value, a label string and a position flag,
--| or create using 'make' and subsequently call 'set_attributes'.
--|
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--|
--| If you wish to receive a notification on change, you can register 
--| your agent using 'set_notification_procedure'.  In the agent, 
--| simply query the 'text' routine for the current value.
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_HRANGE
