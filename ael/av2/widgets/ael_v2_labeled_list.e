note
	description: "Compound widget with a list widget and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_LIST

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as listw,
			extend as box_extend
		redefine
			initialize_interface_actions
		end

create
	make, make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	initialize_interface_actions
		do
			Precursor
			listw.select_actions.extend (agent on_selection_change)
			listw.deselect_actions.extend (agent on_selection_change)
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create listw
		end

--|========================================================================
feature {EV_ANY} -- Components
--|========================================================================

	listw: EV_LIST
			-- List widget contained in Current

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: STRING)
			-- Add an item to listw with the label 'v'
		require
			exists: v /= Void
		do
			listw.extend (create {EV_LIST_ITEM}.make_with_text (v))
		ensure
			was_added: listw.count = old listw.count + 1
			at_end: listw.last.text.is_equal (v)
		end

	--|--------------------------------------------------------------

	selected_items: LINKED_LIST [STRING]
			-- Labels of items presently selected in listw
		local
			tl: DYNAMIC_LIST [EV_LIST_ITEM]
		do
			create Result.make
			tl := listw.selected_items
			from tl.start
			until tl.exhausted
			loop
				Result.extend (tl.item.text)
				tl.forth
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	all_items: LINKED_LIST [STRING]
			-- All items in listw, in sequence
		do
			create Result.make
			from listw.start
			until listw.exhausted
			loop
				Result.extend (listw.item.text)
				listw.forth
			end
		ensure
			exists: Result /= Void
		end
	
--|========================================================================
feature {NONE} -- Notification
--|========================================================================
	
	on_selection_change
		do
			notify
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_multiple_selection
		do
			listw.enable_multiple_selection
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label', or using
--| 'make_with_label_and_strings', giving it a string to use as the
--| widget label and optionally as item labels.  You can create this
--| widget using 'make' and subsequently setting label and 
--| text string.
--|
--| You can optionally set the width of the list widget.
--| Add the widget to your container and disable expansion of this 
--| widget if desired.
--| If you wish to receive a notification on selection change (select 
--| actions) of the list widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'selected_item' routine for the committed value.
--|
--| Add to a container and disable expansion of this widget if needed.
--|----------------------------------------------------------------------


end -- class AEL_V2_LABELED_LIST
