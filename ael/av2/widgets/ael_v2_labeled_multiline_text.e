note
	description: "Compound widget with a multiline text field and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2007-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_MULTILINE_TEXT

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as textw
		redefine
			initialize_interface_actions, set_focus
		end

create
	make, make_with_label, make_with_label_and_options

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	initialize_interface_actions
		do
			resize_actions.extend (agent on_resize)
			textw.change_actions.extend (agent on_text_change)
			textw.focus_in_actions.extend (agent on_focus_in)
			Precursor
		end

	--|--------------------------------------------------------------

	create_widget
		do
			create {EV_TEXT}textw
		end

--|========================================================================
feature -- Components
--|========================================================================

	textw: EV_TEXT_COMPONENT

--|========================================================================
feature -- Status
--|========================================================================

	text: STRING
		do
			Result := textw.text
		end

	updating: BOOLEAN

--|========================================================================
feature -- Value setting
--|========================================================================

	set_text (v: STRING)
		do
			updating := True
			textw.set_text (v)
			updating := False
		end

	remove_text
			-- 	Remove text from the text widget
		do
			textw.remove_text
		end

	--|--------------------------------------------------------------

   disable_edit
		do
			textw.disable_edit
		end

   set_read_only
		do
			textw.disable_edit
		end

   set_read_write
		do
			textw.enable_edit
		end

   enable_edit
		do
			textw.enable_edit
		end

	enable_line_wrap
		do
			if attached {EV_TEXT} textw as tw then
				tw.enable_word_wrapping
			end
		end

	disable_line_wrap
		do
			if attached {EV_TEXT} textw as tw then
				tw.disable_word_wrapping
			end
		end

	--|--------------------------------------------------------------

	set_text_width (v: INTEGER)
		require
			positive: v > 0
		do
			set_widget_width (v)
		end

	set_text_minimum_width (v: INTEGER)
		require
			positive: v > 0
		do
			set_widget_minimum_width (v)
		end

	--|--------------------------------------------------------------

	set_change_notification_procedure (v: like change_notification_proc)
		do
			change_notification_proc := v
		end

	set_focus_in_notification_procedure (v: like focus_in_notification_proc)
		do
			focus_in_notification_proc := v
		end

	--|--------------------------------------------------------------

	set_focus
		do
			textw.set_focus
		end

	--|--------------------------------------------------------------

	select_all
		do
			textw.select_all
		end

	deselect_all
		do
			textw.deselect_all
		end

	--|--------------------------------------------------------------

	set_bg_color (v: EV_COLOR)
			-- Set background color of text widget
		do
			textw.set_background_color (v)
		end

	set_text_color (v: EV_COLOR)
			-- Set foreground color of text widget
		do
			textw.set_foreground_color (v)
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER)
		local
			ts: STRING
		do
			-- RFO
			-- There is an issue, at last on Windows, where the label
			-- and text are not being redrawn on resize, unless there's an
			-- intervening expose event.
			-- The set_text calls force lower-level invalidation
			ts := label.text.twin
			label.remove_text
			label.set_text (ts)

			updating := True
			ts := textw.text.twin
			textw.remove_text
			textw.set_text (ts)
			updating := False
	end

	--|--------------------------------------------------------------

	on_text_change
		do
			if not updating then
				if attached change_notification_proc as cnp then
					cnp.call ([Current])
				end
			end
		end

	on_focus_in
		do
			if attached focus_in_notification_proc as fnp then
				fnp.call ([Current])
			end
		end

--|========================================================================
feature -- Widget Actions
--|========================================================================

	change_actions: EV_NOTIFY_ACTION_SEQUENCE
			-- Actions to be performed when `text' changes.
		do
			Result := textw.change_actions
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	default_text_width: INTEGER
		do
			Result := 80
		end

	--|--------------------------------------------------------------

--TODO update all procs to new syntax/semantics
	change_notification_proc: detachable PROCEDURE [TUPLE [like Current]]
	focus_in_notification_proc: detachable PROCEDURE [TUPLE [like Current]]
	
--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 04-July-2012
--|     Recompiled and tested original module for Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using
--| 'make' and subsequently calling set_label_text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--|
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--|
--| If you wish to receive a notification on commit (enter actions) of the
--| string in the text widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'text' routine for the committed value.
--|
--| If you wish to receive a notification on text change, you can
--| register your agent using 'set_change_notification_procedure'
--|
--| If you wish to receive a notification on getting focus (focus in)
--| you can register your agent using 'set_focus_in_notification_procedure'
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_MULTILINE_TEXT
