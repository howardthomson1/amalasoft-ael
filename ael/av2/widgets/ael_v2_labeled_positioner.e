note
	description: "Compound widget with a matrix of 9 buttons and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2007-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_POSITIONER

inherit
	AEL_V2_LABELED_WIDGET
		rename
			widget as matrix
		redefine
			build_interface_components
		end

create
	make, make_with_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	create_widget
		do
			create matrix
		end

	--|--------------------------------------------------------------

	build_interface_components
		do
			matrix.resize (3, 3)
			fill_matrix
			select_position (2)
		end

	--|--------------------------------------------------------------

	fill_matrix
		do
			add_button (position_constants.Ks_pos_top_left, 1, 1)
			add_button (position_constants.Ks_pos_top_center, 2, 1)
			add_button (position_constants.Ks_pos_top_right, 3, 1)
			add_button (position_constants.Ks_pos_middle_left, 1, 2)
			add_button (position_constants.Ks_pos_middle_center, 2, 2)
			add_button (position_constants.Ks_pos_middle_right, 3, 2)
			add_button (position_constants.Ks_pos_bottom_left, 1, 3)
			add_button (position_constants.Ks_pos_bottom_center, 2, 3)
			add_button (position_constants.Ks_pos_bottom_right, 3, 3)
		end

	--|--------------------------------------------------------------

	add_button (pos: STRING; c, r: INTEGER)
		local
			b: like new_btn
		do
			b := new_btn (pos)
			matrix.put_at_position (b, c, r, 1, 1)
			b.set_background_color (colors.default_background_color)
			b.set_foreground_color (colors.default_background_color)
		end

	--|--------------------------------------------------------------

	refill_matrix
		local
			idx: INTEGER
		do
			idx := selected_button_index
			matrix.wipe_out
			fill_matrix
			select_position (idx)
		end

	--|--------------------------------------------------------------

	new_btn (pos: STRING): EV_BUTTON
		require
			valid_position: is_valid_position_tag(pos)
		do
			create Result
			Result.select_actions.extend (agent on_button_press (Result,pos))
			Result.set_minimum_height (button_height)
			Result.set_minimum_width (button_width)
		end

--|========================================================================
feature -- Status
--|========================================================================

	matrix: EV_TABLE
	selected_button_index: INTEGER

	selected_position: STRING
		do
			Result := position_tag_by_index (selected_button_index)
		end

	position_out (v: like selected_position): STRING
		do
			Result := v
		end

--|========================================================================
feature {NONE} -- Private values
--|========================================================================

	button_index (b: like new_btn): INTEGER
		require
			button_exists: b /= Void
		local
			i, row, col: INTEGER
		do
			from row := 1
			until Result /= 0 or row > 3
			loop
				from col := 1
				until Result /= 0 or col > 3
				loop
					i := i + 1
					if b = matrix.item_at_position (col, row) then
						Result := i
					end
					col := col + 1
				end
				row := row + 1
			end
			if Result = 0 then
				-- Default is top center
				Result := 2
			end
		ensure
			valid_index: is_valid_position_tag_index (Result)
		end

	--|--------------------------------------------------------------

	selected_button: detachable EV_BUTTON
			-- Button corresponding to current selection, if any
		require
			has_selection: selected_button_index /= 0
		local
			idx, row, col: INTEGER
			rc: like pos_to_row_col
		do
			idx := selected_button_index
			rc := pos_to_row_col (idx)
			col := rc.integer_item (1)
			row := rc.integer_item (2)
			if attached {EV_BUTTON} matrix.item_at_position (col, row) as b then
				Result := b
			end
		ensure
			exists: selected_button_index /= 0 implies attached Result
		end

	--|--------------------------------------------------------------

	row_col_to_pos (row, col: INTEGER): INTEGER
		do
			Result := row * col
		end

	--|--------------------------------------------------------------

	pos_to_row_col (v: INTEGER): TUPLE [INTEGER, INTEGER]
		require
			valid_position: is_valid_position_tag_index(v)
		local
			row, col: INTEGER
		do
			create Result
			row := ((v - 1) // 3) + 1
			col := v - ((row - 1) * 3)
			Result.put (col, 1)
			Result.put (row, 2)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	position_tag_by_index (v: INTEGER): STRING
		require
			index_in_range: is_valid_position_tag_index (v)
		do
			Result := indexed_position_tags.item (v)
		ensure
			exists: Result /= Void
			valid_tag: is_valid_position_tag (Result)
		end

	--|--------------------------------------------------------------

	button_width: INTEGER
		do
			Result := private_button_width
			if Result = 0 then
				Result := K_dflt_button_width
			end
		end

	button_height: INTEGER
		do
			Result := private_button_height
			if Result = 0 then
				Result := K_dflt_button_height
			end
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_press (b: EV_BUTTON; pos: STRING)
		require
			button_exists: b /= Void
			valid_position: is_valid_position_tag (pos)
		do
			select_position (button_index (b))
			notify
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_button_size (w, h: INTEGER)
		require
			valid_sizes: w > 0 and h > 0
		do
			private_button_width := w
			private_button_height := h
			refill_matrix
		end

	--|--------------------------------------------------------------

	select_position (v: INTEGER)
		require
			valid_position: is_valid_position_tag_index (v)
		local
			row, col: INTEGER
			rc: like pos_to_row_col
		do
			rc := pos_to_row_col (v)
			col := rc.integer_item (1)
			row := rc.integer_item (2)
			if attached {like new_btn} matrix.item_at_position (col, row) as b then
				clear_selections
				set_button_selected (b, True)
			end
			selected_button_index := v
		end

--|========================================================================
feature {NONE} -- Private status setting
--|========================================================================

	set_button_selected (b: EV_BUTTON; tf: BOOLEAN)
		require
			exists: b /= Void
		do
			if tf then
				b.set_background_color (colors.black)
			else
				b.set_background_color (colors.default_background_color)
			end
		end

	--|--------------------------------------------------------------

	clear_selections
		local
			row, col: INTEGER
		do
			from row := 1
			until row > 3
			loop
				from col := 1
				until col > 3
				loop
					if attached {like new_btn} matrix.item_at_position (col, row) as b then
						set_button_selected (b, False)
					end
					col := col + 1
				end
				row := row + 1
			end
			selected_button_index := 0
		ensure
			has_no_selections: selected_button_index = 0
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_position_tag_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid position tag index?
		do
			Result := position_constants.is_valid_position_tag_index (v)
		end

	is_valid_position_tag (v: STRING): BOOLEAN
			-- Is 'v' a valid position tag?
		do
			Result := position_constants.is_valid_position_tag (v)
		end

	indexed_position_tags: ARRAY [ STRING ]
			-- Postition tag constants arranged in sequence as if traversing
			-- the position matrix left to right, then top top to bottom
		do
			Result := position_constants.indexed_position_tags
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	black_pixmap: EV_PIXMAP
		do
			create Result.make_with_size (button_width, button_height)
			Result.set_background_color (colors.black)
			Result.clear
		end

	gray_pixmap: EV_PIXMAP
		do
			create Result.make_with_size (button_width, button_height)
			Result.set_background_color (colors.default_background_color)
			Result.clear
		end

	private_button_width: INTEGER
	private_button_height: INTEGER

	K_dflt_button_width: INTEGER = 12
	K_dflt_button_height: INTEGER = 12

	--|--------------------------------------------------------------
invariant
	has_selected: selected_button_index /= 0

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| The buttons each show either selected or unselected, and are
--| mutually exclusive.
--| Selecting one of the buttons in the matrix sets the selected
--| position (top-left, top-center, ...) accordingly.
--|
--| To use this widget, create it using 'make_with_label', giving
--| it a string to use as the widget label, or using 'make'
--| and subsequently setting the label text.
--|
--| Then add to your container and disable expansion of this widget.
--|
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--| In your agent code, ask for the selected value via 'selected_position'.
--| This value is a 2-character string, from AEL_V2_POSITION_CONSTANTS,
--| and can be used to position an object relatively.
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_POSITIONER
