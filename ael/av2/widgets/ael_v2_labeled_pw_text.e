note
	description: "Compound widget with a password text field and a label"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2008-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/06 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LABELED_PW_TEXT

inherit
	AEL_V2_LABELED_TEXT
		redefine
			textw
		end

create
	make, make_with_label

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	textw: EV_PASSWORD_FIELD

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--| 001 27-Feb-2012
--|     Re-compiled and tested using Eiffel 6.7
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using 
--| 'make' and subsequently setting the label text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--| Add the widget to your container and disable expansion of this widget
--| if desired.
--| If you wish to receive a notification on commit (enter actions) of the
--| string in the text widget, you can register your agent using the
--| 'set_notification_procedure' routine.  In the agent, simply query
--| the 'text' routine for the committed value.
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_LABELED_PW_TEXT
