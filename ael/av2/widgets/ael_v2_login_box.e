note
	description: "A box widget containing login/logout button and text field"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LOGIN_BOX

inherit
	AEL_V2_HORIZONTAL_BOX
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_actions
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization (via default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create login_btn.make_with_text (Ks_login_btn_label)
			create user_textw
		end

--|========================================================================
feature {NONE} -- Initialization (post default_create)
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		do
			extend (login_btn)
			disable_item_expand (login_btn)

			---------------

			extend (user_textw)
			user_textw.set_minimum_width (180)
			disable_item_expand (user_textw)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			login_btn.select_actions.extend (agent execute)
			user_textw.return_actions.extend (agent execute)
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_login_function (v: like login_func)
		do
			login_func := v
		end

	set_logout_procedure (v: like logout_proc)
		do
			logout_proc := v
		end

	--|--------------------------------------------------------------

	set_event_focus
		do
			user_textw.set_focus
		end

	--|--------------------------------------------------------------

	execute
		do
			if login_btn.text ~ Ks_login_btn_label then
				execute_login (user_textw.text)
			else
				execute_logout
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	execute_login (nm: STRING)
			-- Login user 'nm' (may be empty)
		local
			emsg: STRING
		do
			if attached login_func as lf then
				lf.call ([nm])
				-- result is error message; empty if no error
				if attached lf.last_result as lr then
					emsg := lr
					if emsg.is_empty then
						-- What can we do here?
					end
				end
			end
		end

	--|--------------------------------------------------------------

	execute_logout
			-- Unset user
		do
			if attached logout_proc as lp then
				lp.call ([])
			end
			update_user_name ("")
		end

	--|--------------------------------------------------------------

	update_user_name (v: STRING)
		do
			if v.is_empty then
				login_btn.set_text (Ks_login_btn_label)
			else
				login_btn.set_text (Ks_logout_btn_label)
			end
			user_textw.set_text (v)
		end

--|========================================================================
feature -- Components and values
--|========================================================================

	login_btn: EV_BUTTON
	user_textw: EV_TEXT_FIELD

	login_func: detachable FUNCTION [STRING, STRING]
	logout_proc: detachable PROCEDURE

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_login_btn_label: STRING = "Login   "
	Ks_logout_btn_label: STRING = "Logout"

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make'
--|
--| Insert into a parent container as desired.
--| Set the login and logout agents by calling set_login_function and
--| set_logout_procedure from client.
--| A return event in the text field, or a select event on the button
--| will invoke a simple name/password dialog an call the the appropriate
--| procedures.
--| Login/loout can be triggered programmatically by calling the
--| 'execute' routine directly.
--|----------------------------------------------------------------------

end -- class AEL_V2_LOGIN_BOX
