note
	description: "Enhanced version of EV_MULTI_COLUMN_LIST"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_MULTI_COLUMN_LIST

inherit
	EV_MULTI_COLUMN_LIST
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as mcl_implementation_attr
		redefine
			default_create_interface_objects, create_implementation
		select
			mcl_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects,
			initialize_interface_actions
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization (from default_create)
--|========================================================================

	implementation: like mcl_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := mcl_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_MULTI_COLUMN_LIST}
			implementation_attr := mcl_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_MULTI_COLUMN_LIST}
			Precursor {AEL_V2_WIDGET}
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			column_width_proportions := {ARRAY [REAL_32]}<< >>
		end

--|========================================================================
feature -- Status
--|========================================================================

	column_width_proportions: ARRAY [REAL_32]
			-- Column widths expressed as proportion of overall width

--|========================================================================
feature -- Status setting
--|========================================================================

	set_column_width_proportions (v: like column_width_proportions)
			-- Set the per-column width as a proportion of the overall
			-- width of Current.
			-- Each width must be at least 0.0 (hidden) and less than 1.0
			-- (unless there is only one column).
			-- The sum of proportions should total 1.0.
			-- Setting to Void will disable proportional sizing
			--
			-- NOTE WELL that column count MUST be set before calling 
			-- this routine.  Setting column titles will suffice.
		do
			column_width_proportions := v
			update_column_widths
		end

	--|--------------------------------------------------------------

	column_width_proportions_are_valid: BOOLEAN
			-- Are there column width proportions defined, and if so,
			-- do they total 1.0?
		local
			i, lim: INTEGER
			tr: REAL_32
		do
			lim := column_width_proportions.upper
			from i := column_width_proportions.lower
			until i > lim
			loop
				tr := tr + column_width_proportions.item (i)
				i := i + 1
			end
			Result := (tr * 100.0) = 100.0
		end

	--|--------------------------------------------------------------

	update_column_widths
			-- Re-set columns widths based on defined proportions and 
			-- the current overall width of Current
			-- If proportional widths are not defined, do nothing
		do
			if last_overall_width /= width or
			 last_overall_height /= height
			 then
				--set_column_widths (column_width_proportions)
				set_column_widths (proportional_column_widths)
			end
			last_overall_width := width
			last_overall_height := height
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_row (va: ARRAY [READABLE_STRING_GENERAL])
			-- Add a row of items with cell values 'va'
		require
			valid_count: va.count = column_count
		local
			tr: EV_MULTI_COLUMN_LIST_ROW
		do
			tr := new_row (va)
			extend (tr)
		end

	new_row (va: ARRAY [READABLE_STRING_GENERAL]): EV_MULTI_COLUMN_LIST_ROW
			-- Newly minted row of items with cell values 'va'
		require
			valid_count: va.count = column_count
		local
			vl: LINKED_LIST [STRING_32]
			i, lim: INTEGER
		do
			create vl.make
			lim := va.upper
			from i := va.lower
			until i > lim
			loop
				vl.extend (va.item (i).as_string_32)
				i := i + 1
			end
			create Result
			Result.fill (vl)
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_resize (xp, yp, w, h: INTEGER)
			-- Handle a resize event
		do
			application_root.do_once_on_idle (agent update_column_widths)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			resize_actions.extend (agent on_resize)
			Precursor
		end

	--|--------------------------------------------------------------

	proportional_column_widths: ARRAY [INTEGER]
			-- Per-column widths (in pixels) derived from proportional 
			-- widths
			-- These are recalculated each time called
		local
			cwp: like column_width_proportions
			i, lim, w, lw: INTEGER
		do
			cwp := column_width_proportions
			create Result.make_filled (0, cwp.lower, cwp.upper)
			if rows_visible < count then
				lw := width - 20 -- for scrollbar
			else
				lw := width
			end
			lim := cwp.upper
			from i := cwp.lower
			until i > lim
			loop
				w := (lw * cwp.item (i)).truncated_to_integer
				Result.put (w, i)
				i := i + 1
			end
			cached_proportional_column_widths := Result
		ensure
			exists: Result /= Void
		end

	rows_visible: INTEGER
			-- Estimated number of visible rows, based on size of 
			-- Current and height of rows
			-- Can be off if horiz scrollbar is present
		local
			oh, rh: INTEGER
		do
			oh := height
			rh := row_height
			if rh /= 0 then
				Result := oh // rh
			end
			Result := Result.max (1)
		end

	--|--------------------------------------------------------------

	cached_proportional_column_widths: detachable ARRAY [INTEGER]
			-- Most recently calculated column widths

	last_overall_height: INTEGER
			-- Height of Current at last resize/recalculate action
	last_overall_width: INTEGER
			-- Width of Current at last resize/recalculate action

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| 
--| Treat as you would an EV_MULTI_COLUMN_LIST unless you wish to 
--| exploit the proportional column width feature.
--| If you wish to do so, set the column proportions by calling
--| 'set_column_width_proportions' with an ARRAY of REAL values, each
--| at least 0.0 (hidden) and less than 1.0 (unless there is only one 
--| column).  The sum of proportions should total 1.0.  Check 
--| proportion validity by calling 'column_width_proportions_are_valid'
--|----------------------------------------------------------------------

end -- class AEL_V2_MULTI_COLUMN_LIST
