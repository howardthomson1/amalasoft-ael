note
	description: "A button display a pixmap and no text"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_PIXMAP_BUTTON

inherit
	EV_BUTTON
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as btn_implementation_attr
		redefine
			default_create_interface_objects, create_implementation,
			enable_sensitive, disable_sensitive
		select
			btn_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects,
			build_interface_components
		end

create
	make_with_pixmaps

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_pixmaps (spm, ipm: EV_PIXMAP)
			-- Create `Current' with sensitive pixmap 'spm' and 
			-- insensitive pixmap 'ipm'
		do
			sensitive_pixmap := spm
			insensitive_pixmap := ipm
			make
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

	implementation: like btn_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := btn_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_BUTTON}
			implementation_attr := btn_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_BUTTON}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

	build_interface_components
			-- Assemble interface components previously created
		do
			set_pixmap (sensitive_pixmap)
		end

--|========================================================================
feature {NONE} -- Status
--|========================================================================

	sensitive_pixmap: EV_PIXMAP
	insensitive_pixmap: EV_PIXMAP

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_sensitive
		do
			Precursor
			set_pixmap (sensitive_pixmap)
		end

	disable_sensitive
		do
			Precursor
			set_pixmap (insensitive_pixmap)
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make_with_pixmaps'.
--|----------------------------------------------------------------------

end -- class AEL_V2_PIXMAP_BUTTON
