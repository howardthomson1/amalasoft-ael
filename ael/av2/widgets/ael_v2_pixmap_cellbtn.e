note
	description: "A cell with button-like behavior, displaying a pixmap"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_PIXMAP_CELLBTN

inherit
	AEL_V2_CELL
		redefine
			create_interface_objects, build_interface_components,
			initialize_interface_actions,
			enable_sensitive, disable_sensitive
		end

create
	make_with_pbufs

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_pbufs (spm, ipm: EV_PIXEL_BUFFER)
			-- Create `Current' with sensitive pbuf 'spm' and 
			-- insensitive pbuf 'ipm'
		do
			sensitive_pbuf:= spm
			insensitive_pbuf := ipm
			make
		end

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create select_actions.make
		end

	build_interface_components
			-- Assemble interface components previously created
		local
			pm: EV_PIXMAP
		do
			pm := sensitive_pixmap
			extend (pm)
			pm.pointer_button_release_actions.extend (agent on_mouse_up)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	select_actions: LINKED_LIST [PROCEDURE]
			-- Actions to be performed when Current is selected.

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_sensitive
		local
			pm: EV_PIXMAP
		do
			Precursor
			pm := sensitive_pixmap
			replace (pm)
			pm.pointer_button_release_actions.extend (agent on_mouse_up)
		end

	disable_sensitive
		local
			pm: EV_PIXMAP
		do
			Precursor
			pm := insensitive_pixmap
			replace (pm)
			pm.pointer_button_release_actions.extend (agent on_mouse_up)
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_mouse_up (xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to mouse-up event (mimicking selection)
			-- There is a case that is unhandled: when an up event 
			-- occurs but there is no corresponding down even in Current, 
			-- the action is still triggered.  Tying into down events 
			-- and focus events can avoid that potential issue, but adds 
			-- a lot of complexity
		do
			from select_actions.start
			until select_actions.after
			loop
				select_actions.item.call
				select_actions.forth
			end
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	sensitive_pixmap: EV_PIXMAP
		do
			create Result.make_with_pixel_buffer (sensitive_pbuf)
		end

	insensitive_pixmap: EV_PIXMAP
		do
			create Result.make_with_pixel_buffer (insensitive_pbuf)
		end

	sensitive_pbuf: EV_PIXEL_BUFFER
	insensitive_pbuf: EV_PIXEL_BUFFER

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make_with_pixmaps'.
--|----------------------------------------------------------------------

end -- class AEL_V2_PIXMAP_CELLBTN
