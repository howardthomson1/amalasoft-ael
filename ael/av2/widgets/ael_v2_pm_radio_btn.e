note
	description: "{
A button-like object, showing a pixmap in 1 of two states and
toggling between those states on select events
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2020 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_PM_RADIO_BTN

inherit
	AEL_V2_PIXMAP_CELLBTN
		rename
			sensitive_pixmap as off_pixmap,
			insensitive_pixmap as on_pixmap,
			sensitive_pbuf as off_pbuf,
			insensitive_pbuf as on_pbuf
		redefine
			make_with_pbufs, initialize_interface_actions,
			enable_sensitive, disable_sensitive,
			on_mouse_up
		end

create
	make_with_pbufs

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_pbufs (spm, ipm: EV_PIXEL_BUFFER)
		do
			create peers.make (2)
			Precursor (spm, ipm)
		end

	initialize_interface_actions
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_selected: BOOLEAN

	peers: ARRAYED_LIST [like Current]

--|========================================================================
feature -- Status setting
--|========================================================================

	disable_toggle_on_click
		do
			toggle_is_disabled := True
		end

	enable_toggle_on_click
		do
			toggle_is_disabled := False
		end

	enable_sensitive
		require else True
		do
			-- do nothing
--RFO 			if not is_sensitive then
--RFO 				Precursor
--RFO 			end
		end

	disable_sensitive
		require else True
		do
			-- do nothing
--RFO 			if is_sensitive then
--RFO 				Precursor
--RFO 			end
		end

	enable_select
			-- Set state of Current to selected, disabling select on peers
		local
			pm: EV_PIXMAP
		do
			from peers.start
			until peers.after
			loop
				peers.item.disable_select
				peers.forth
			end
--			if not is_selected then
				pm := on_pixmap
				replace (pm)
				pm.pointer_button_release_actions.extend (agent on_mouse_up)
--			end
			is_selected := True
		end

	disable_select
		local
			pm: EV_PIXMAP
		do
--			if is_selected then
				pm := off_pixmap
				replace (pm)
				pm.pointer_button_release_actions.extend (agent on_mouse_up)
--			end
			is_selected := False
		end

	toggle_select
		do
			if is_selected then
				disable_select
			else
				enable_select
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	selected_peer: detachable like Current
		do
			if is_selected then
				Result := Current
			else
				from peers.start
				until peers.after or attached Result
				loop
					if peers.item.is_selected then
						Result := peers.item
					else
						peers.forth
					end
				end
				peers.start
			end
		end

	toggle_is_disabled: BOOLEAN
			-- Is default toggle-on-click behavior disabled?

--|========================================================================
feature -- Agents and actions
--|========================================================================

	on_mouse_up (xp, yp, b: INTEGER; xt, yt, pr: DOUBLE; sx, sy: INTEGER)
			-- Respond to mouse-up event (mimicking selection)
			-- There is a case that is unhandled: when an up event 
			-- occurs but there is no corresponding down even in Current, 
			-- the action is still triggered.  Tying into down events 
			-- and focus events can avoid that potential issue, but adds 
			-- a lot of complexity
		do
			if not toggle_is_disabled then
				toggle_select
			end
			Precursor (xp, yp, b, xt, yt, pr, sx, sy)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	add_peer (v: like Current)
		do
			peers.extend (v)
		end

	set_peers (v: ARRAY [like Current])
		do
			peers.wipe_out
			peers.fill (v)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 09-Aug-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_PM_RADIO_BTN
