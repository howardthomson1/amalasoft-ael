note
	description: "A list widget whose items can be moved up or down to reflect priority"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."
	
class AEL_V2_PRIORITY_LIST

inherit
	EV_MULTI_COLUMN_LIST
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as mcl_implementation_attr,
			column_count as mcl_column_count,
			column_title as mcl_column_title,
			column_width as mcl_column_width,
			column_alignment as mcl_column_alignment,
			set_column_titles as mcl_set_column_titles,
			set_column_widths as mcl_set_column_widths,
			set_column_alignments as mcl_set_column_alignments,
			set_column_title as mcl_set_column_title,
			set_column_width as mcl_set_column_width,
			set_column_alignment as mcl_set_column_alignment,
			align_text_left as mcl_align_text_left,
			align_text_center as mcl_align_text_center,
			align_text_right as mcl_align_text_right,
			resize_column_to_content as mcl_resize_column_to_content
		export
			{NONE} force, extend
		redefine
			default_create_interface_objects, create_implementation
		select
			mcl_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects,
			initialize_interface_actions
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization (from default_create)
--|========================================================================

	implementation: like mcl_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := mcl_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_MULTI_COLUMN_LIST}
			implementation_attr := mcl_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_MULTI_COLUMN_LIST}
			Precursor {AEL_V2_WIDGET}
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create new_indices.make
			create defined_column_widths.make
			idle_action := agent idle_action_routine
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor

			set_default_key_processing_handler (agent key_uses_default_processing)
			key_press_actions.extend (agent on_pl_key_press)
			resize_actions.extend (agent on_resize)
			column_resized_actions.extend (agent on_column_resize)
		end

--|========================================================================
feature -- Status
--|========================================================================

	item_position (v: like item): INTEGER
			-- Position value in first field of given item
		require
			exists: v /= Void
			belongs: has (v)
			not_destroyed: not is_destroyed
		local
			ts: STRING
		do
			ts := v.first
			if ts.is_integer then
				Result := ts.to_integer
			end
		end

	--|--------------------------------------------------------------

	items_are_selected (vl: LIST [INTEGER]): BOOLEAN
			-- Are the items at the given positions all selected?
		require
			exists: vl /= Void
			not_destroyed: not is_destroyed
		local
			idx: INTEGER
		do
			Result := True
			from vl.start
			until vl.after or not Result
			loop
				idx := vl.item
				if idx <= 0 or idx > count then
					Result := False
				elseif not i_th (idx).is_selected then
					Result := False
				else
					vl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	column_count: INTEGER
			-- Number of client-defined columns
			-- Exclusive of built-in first column
		do
			Result := mcl_column_count - 1
		end

	column_title (c: INTEGER): STRING
			-- Title string of given client-defined column,
			-- index 1 being the first column after the position column
		do
			Result := mcl_column_title (c + 1)
		end

	column_width (c: INTEGER): INTEGER
			-- Width, in pixels, of given client-defined column,
			-- index 1 being the first column after the position column
		do
			Result := mcl_column_width (c + 1)
		end

	column_alignment (c: INTEGER): EV_TEXT_ALIGNMENT
			-- Text alignment of given client-defined column,
			-- index 1 being the first column after the position column
		do
			Result := mcl_column_alignment (c + 1)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_column_titles (a: ARRAY [STRING_GENERAL])
		require
			exists: a /= Void
			not_destroyed: not is_destroyed
		local
			i, lim: INTEGER
			ta: ARRAY [STRING_GENERAL]
			sw: BOOLEAN
		do
			lim := a.upper
			create ta.make_filled ("", a.lower, lim + 1)
			ta.put ("#", ta.lower)
			from i := a.lower
			until i > lim
			loop
				ta.put (a.item (i), i + 1)
				i := i + 1
			end
			mcl_set_column_titles (ta)
			sw := setting_widths
			setting_widths := True
			mcl_set_column_width (K_dflt_column_1_width, 1)
			setting_widths := sw
		ensure
			column_count_valid: column_count >= a.count
		end

	--|--------------------------------------------------------------

	set_column_widths (a: ARRAY [INTEGER])
		require
			exists: a /= Void
			not_destroyed: not is_destroyed
		local
			i, lim, tw: INTEGER
			ta: ARRAY [INTEGER]
			sw: BOOLEAN
		do
			sw := setting_widths
			setting_widths := True
			defined_column_widths.wipe_out
			lim := a.upper
			create ta.make_filled (0, a.lower, lim + 1)
			ta.put (K_dflt_column_1_width, ta.lower)
			tw := K_dflt_column_1_width
			from i := a.lower
			until i > lim
			loop
				defined_column_widths.extend (a.item (i))
				if a.item (i) < 0 then
					ta.put (50, i + 1)
					tw := tw + 50
				else
					ta.put (a.item (i), i + 1)
					tw := tw + a.item (i)
				end
				i := i + 1
			end
			mcl_set_column_widths (ta)
			setting_widths := sw
		ensure
			column_count_valid: column_count >= a.count
		end

	--|--------------------------------------------------------------

	set_column_alignments (al: LINKED_LIST [EV_TEXT_ALIGNMENT])
		require
			exists: al /= Void
			not_destroyed: not is_destroyed
		local
			tl: LINKED_LIST [EV_TEXT_ALIGNMENT]
			ta: EV_TEXT_ALIGNMENT
		do
			create tl.make
			create ta.make_with_right_alignment
			tl.extend (ta)
			tl.fill (al)
			mcl_set_column_alignments (tl)
		ensure
			column_count_valid: column_count >= al.count
		end

	--|--------------------------------------------------------------

	set_column_title (v: STRING; col: INTEGER)
		require
			valid_column: col > 0 and col <= column_count
			title_exists: v /= Void
			not_destroyed: not is_destroyed
		do
			mcl_set_column_title (v, col + 1)
		end

	--|--------------------------------------------------------------

	set_column_width (v, col: INTEGER)
		require
			valid_column: col > 0 and col <= column_count
			valid_width: v >= 0
			not_destroyed: not is_destroyed
		local
			sw: BOOLEAN
		do
			sw := setting_widths
			setting_widths := True
			mcl_set_column_width (v, col + 1)
			setting_widths := sw
		end

	--|--------------------------------------------------------------

	set_column_alignment (v: EV_TEXT_ALIGNMENT; col: INTEGER)
		require
			valid_column: col > 0 and col <= column_count
			exists: v /= Void
			not_destroyed: not is_destroyed
		do
			mcl_set_column_alignment (v, col + 1)
		end

	--|--------------------------------------------------------------

	align_text_left (c: INTEGER)
		do
			mcl_align_text_left (c + 1)
		end

	align_text_center (c: INTEGER)
		do
			mcl_align_text_center (c + 1)
		end

	align_text_right (c: INTEGER)
		do
			mcl_align_text_right (c + 1)
		end

	--|--------------------------------------------------------------

	resize_column_to_content (c: INTEGER)
			-- Resize column `a_column' to width of its widest text.
		do
			mcl_resize_column_to_content (c + 1)
		end

	--|--------------------------------------------------------------

	select_items (vl: LIST [INTEGER])
		require
			exists: vl /= Void
			not_destroyed: not is_destroyed
		do
			remove_selection
			from vl.start
			until vl.exhausted
			loop
				implementation.select_item (vl.item)
				vl.forth
			end
		ensure
			items_selected: items_are_selected (vl)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_item (vl: ARRAY [STRING])
			-- Add a new row to the list, with the given field values
		require
			fields_exist: vl /= Void
			not_destroyed: not is_destroyed
		local
			tr: like item
			tl: LINKED_LIST [STRING]
		do
			create tl.make
			tl.extend ((count + 1).out)
			tl.finish
			tl.fill (vl)
			create tr
			tr.fill_with_strings_8 (tl)
			extend (tr)
		ensure
			added: i_th (count).i_th (2).is_equal (vl.item (1))
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_pl_key_press (k: EV_KEY)
		require
			exists: k /= Void
		do
			if k.code = key_constants.key_page_up then
				if not selected_items.is_empty then
					if index_of (selected_items.first, 1) > 1 then
						move_selected_items (True)
					end
				end
			elseif k.code = key_constants.key_page_down then
				if selection_is_demotable then
					move_selected_items (False)
				end
			else
				-- Ignore
			end
		end

	--|--------------------------------------------------------------

	key_uses_default_processing (k: EV_KEY): BOOLEAN
			-- Does the given key use default key processing?
			-- If not, then it must be processed on key press events
		require
			exists: k /= Void
		do
			if k.code = key_constants.key_page_down or
				k.code = key_constants.key_page_up then
			else
				Result := True
			end
		end

	--|--------------------------------------------------------------

	on_resize (a, ay, w, h: INTEGER)
		local
			rdt: like refresh_delay_timeout
		do
			if not refresh_delayed then
				create rdt
				rdt.actions.extend (agent setup_for_refresh)
				rdt.set_interval (100)
				refresh_delay_timeout := rdt
			end
		end

	--|--------------------------------------------------------------

	on_column_resize (c: INTEGER)
		local
			rdt: like refresh_delay_timeout
		do
			if not refresh_delayed then
				create rdt
				rdt.actions.extend (agent setup_for_refresh)
				-- We can use a shorter delay than on the general resize event
				rdt.set_interval (50)
				refresh_delay_timeout := rdt
			end
		end

--|========================================================================
feature {NONE} -- Priority changing
--|========================================================================

	move_selected_items (up: BOOLEAN)
			-- For each selected item (if any), move one position
			-- in the priority list (up if 'up', else down)
		require
			has_selection: not selected_items.is_empty
			promotable: up implies index_of (selected_items.first,1) > 1
			demotable: (not up) implies selection_is_demotable
		local
			tl: like selected_items
			idx: INTEGER
			rdt : like refresh_delay_timeout
		do
			arranging := True
			tl := selected_items
			idx := index
			new_indices.wipe_out
			if up then
				from tl.start
				until tl.after
				loop
					idx := index_of (tl.item, 1)
					promote_item (tl.item, idx)
					new_indices.extend (idx - 1)
					tl.forth
				end
			else
				from tl.finish
				until tl.before
				loop
					idx := index_of (tl.item, 1)
					demote_item (tl.item, idx)
					new_indices.extend (idx + 1)
					tl.back
				end
			end
			renumber_items
			arranging := False
			-- Post an update to reselect the moved items
			if not refresh_delayed then
				create rdt
				rdt.actions.extend (agent setup_for_refresh)
				rdt.set_interval (100)
				refresh_delay_timeout := rdt
			end
		end

	--|--------------------------------------------------------------

	promote_item (v: like item; idx: INTEGER)
			-- Move up in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
			valid_index: idx = index_of (v, 1)
			promotable: idx > 1
		do
			go_i_th (idx)
			remove
			go_i_th (idx - 1)
			put_left (v)
		ensure
			promoted: index_of (v, 1) = (old index_of (v, 1) - 1)
		end

	--|--------------------------------------------------------------

	demote_item (v: like item; idx: INTEGER)
			-- Move down in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
			valid_index: idx = index_of (v, 1)
			demotable: idx < count
		do
			go_i_th (idx)
			remove
			go_i_th (idx)
			put_right (v)
		ensure
			demoted: index_of (v, 1) = (old index_of (v, 1) + 1)
		end

	--|--------------------------------------------------------------

	selection_is_demotable: BOOLEAN
			-- Is there a current selection and is it demotable?
		local
			tl: like selected_items
			idx: INTEGER
		do
			tl := selected_items
			if not tl.is_empty then
				Result := True
				from tl.start
				until tl.after or not Result
				loop
					idx := index_of (tl.item, 1)
					if idx >= count then
						-- Can't do it; would go off end of list
						Result := False
					else
						tl.forth
					end
				end
			end
		end

	--|--------------------------------------------------------------

	renumber_items
			-- Set the position number field of each item in the list
		local
			i: INTEGER
		do
			from
				start
				i := 1
			until after
			loop
				item.start
				item.replace (i.out)
				forth
				i := i + 1
			end
		end

--|========================================================================
feature -- Assertion support
--|========================================================================

	items_are_numbered: BOOLEAN
			-- Are all items in the list numbered correctly?
		local
			i: INTEGER
			oc: CURSOR
		do
			Result := True
			oc := cursor
			from
				start
				i := 1
			until after or not Result
			loop
				if item_position (item) /= i then
					Result := False
				else
					forth
					i := i + 1
				end
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	defined_column_widths: LINKED_LIST [INTEGER]
			-- Column numbers requested by client

	new_indices: LINKED_LIST [INTEGER]

	setting_widths: BOOLEAN
			-- Are we currently setting column widths?

	update_pending: BOOLEAN
	arranging: BOOLEAN
	column_widths_updated: BOOLEAN

	refresh_delayed: BOOLEAN
		do
			Result := attached refresh_delay_timeout
		end

	parent_width: INTEGER
		do
			if attached parent as pw then
				Result := pw.width
			end
		end

	--|--------------------------------------------------------------

	setup_for_refresh
		do
			if refresh_delayed then
				if attached refresh_delay_timeout as rdt then
					rdt.set_interval (0)
				end
			end
			if not update_pending then
				update_pending := True
				application_root.do_once_on_idle (idle_action)
			end
		end

	--|--------------------------------------------------------------

	idle_action_routine
		do
			if refresh_delayed then
				if attached refresh_delay_timeout as rdt then
					rdt.set_interval (0)
				end
			end
			if not new_indices.is_empty then
				select_items (new_indices)
				new_indices.wipe_out
				set_focus
			end
			if not column_widths_updated then
				update_column_widths
			end
			show
			update_pending := False
			refresh_delay_timeout := Void
		end

	--|--------------------------------------------------------------

	update_column_widths
		local
			i, dc, dw, tw: INTEGER
			wl: like defined_column_widths
		do
			if not setting_widths then
				column_widths_updated := True
				hide
				wl := defined_column_widths.twin
				-- First count all the non-wildcard widths
				from wl.start
				until wl.after
				loop
					if wl.item >= 0 then
						dw := dw + wl.item
						dc := dc + 1
					end
					wl.forth
				end
				if dc < mcl_column_count then
					tw := ((parent_width - K_dflt_column_1_width) - dw) // ((mcl_column_count - 1) - dc)
				end
				mcl_set_column_width (K_dflt_column_1_width, 1)
				from
					wl.start
					i := 2
				until wl.after
				loop
					if wl.item >= 0 then
						mcl_set_column_width (wl.item, i)
					else
						mcl_set_column_width (tw, i)
					end
					wl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	idle_action: PROCEDURE

	refresh_delay_timeout: detachable EV_TIMEOUT

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_dflt_column_1_width: INTEGER = 30

	--|--------------------------------------------------------------
invariant
	numbered: (not arranging) implies items_are_numbered

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| 
--| After creation, set the number of columns by calling set_column_titles
--| with the labels for the 2nd-thru-Nth columns.
--| The first column is always the item position (helpful for longer lists)
--| If multiple selection is desired, call enable_multiple_selection.
--| To promote/demote selected items, use the page-up and page-down keys.
--|----------------------------------------------------------------------

end -- class AEL_V2_PRIORITY_LIST
