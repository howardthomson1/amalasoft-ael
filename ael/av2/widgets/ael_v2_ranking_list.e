note
	description: "Compound widget with 2 parallel lists in a scrolling window"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_RANKING_LIST

inherit
	AEL_V2_VERTICAL_BOX
		rename
			index as vb_index,
			index_of as vb_index_of,
			has as vb_has,
			count as vb_count
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_values,
			initialize_interface_actions,
			post_initialize
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create position_label.make_with_text ("#")
			create item_label
			create scrollwin
			create position_list
			create name_list
			idle_action := agent update_geometry
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		local
			hb: EV_HORIZONTAL_BOX
			tf: EV_FRAME
		do
			create hb
			extend (hb)
			hb.set_minimum_height (18)
			disable_item_expand (hb)

			create tf
			hb.extend (tf)
			tf.set_minimum_width (K_dflt_column_1_width)
			hb.disable_item_expand (tf)
			tf.set_style (2)
			tf.extend (position_label)

			create tf
			hb.extend (tf)
			tf.set_style (2)
			tf.extend (item_label)

			-------------------------

			extend (scrollwin)

			create hb
			scrollwin.extend (hb)
			scrollwin.set_item_size (width, 200)
			scrollwin.hide_horizontal_scroll_bar

			hb.extend (position_list)

			position_list.set_minimum_width (K_dflt_column_1_width)
			hb.disable_item_expand (position_list)

			hb.extend (name_list)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			resize_actions.extend (agent on_resize)
			position_list.select_actions.extend (agent on_select(position_list))
			name_list.select_actions.extend (agent on_select(name_list))

			position_list.set_default_key_processing_handler (
				agent key_uses_default_processing)
			name_list.set_default_key_processing_handler (
				agent key_uses_default_processing)
			set_default_key_processing_handler (agent key_uses_default_processing)
			position_list.key_press_actions.extend (agent on_pl_key_press)
			name_list.key_press_actions.extend (agent on_pl_key_press)
			key_press_actions.extend (agent on_pl_key_press)
		end

	--|--------------------------------------------------------------

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
			item_height := 14
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			enable_update_on_move
			update_geometry
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_item (v: STRING; d: ANY)
			-- Add a new row to the list, with the given name
			-- Optionally, associate data 'd' with the new item
			-- Item names ('v') must be unique!!!
		require
			fields_exist: v /= Void
			is_unique: not has_item_by_name (v)
			not_destroyed: not is_destroyed
		local
			tr: like item_anchor
			ps, ts: STRING
			i: INTEGER
		do
			create tr.make_with_text (v)
			if attached d as td then
				tr.set_data (td)
			end
			item_height := item_height.max (tr.height)
			name_list.extend (tr)

			ps := (position_list.count + 1).out
			create ts.make (3)
			from i := ps.count
			until i > 2
			loop
				ts.extend (' ')
				i := i + 1
			end
			ts.append (ps)
			create tr.make_with_text (ts)
			if attached d as td then
				tr.set_data (td)
			end
			position_list.extend (tr)
			update_geometry
		ensure
			item_added: has_item_by_name (v)
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	scrollwin: 	EV_SCROLLABLE_AREA

	position_list: EV_LIST
	position_label: EV_LABEL

	name_list: EV_LIST
	item_label: EV_LABEL

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_pl_key_press (k: EV_KEY)
		require
			exists: k /= Void
		do
			if k.code = key_constants.key_page_up then
				if application_root.ctrl_pressed then
					if selection_is_promotable then
						move_selected_items (True)
					end
--RFO				elseif selected_item /= Void then
--RFO					scroll_to (selected_items.first)
				else
--					scroll_to_position ((index - (K_items_per_page - 1)).max (1))
					scroll_to_position (
						((scrollwin.y_offset // item_height) -
						(K_items_per_page - 1)).max (1))
				end
			elseif k.code = key_constants.key_page_down then
				if application_root.ctrl_pressed then
					if selection_is_demotable then
						move_selected_items (False)
					end
--RFO				elseif selected_item /= Void then
--RFO					scroll_to (selected_items.last)
				else
--					scroll_to_position ((index + (K_items_per_page - 1)).min (count))
					scroll_to_position (
						((scrollwin.y_offset // item_height) +
						(K_items_per_page - 1)).min (count))
				end
			elseif k.code = key_constants.key_home then
				if application_root.ctrl_pressed then
					if selection_is_promotable then
						move_selected_items_to_top
					end
				else
					scroll_to_top
				end
			elseif k.code = key_constants.key_end then
				if application_root.ctrl_pressed then
					if selection_is_demotable then
						move_selected_items_to_bottom
					end
				else
					scroll_to_bottom
				end
			elseif k.code = key_constants.key_up then
				if not selected_items.is_empty
					and then attached selected_items.first as sf then
						scroll_to (sf)
				else
					-- Scroll up one item
					scroll_to_position (
						((scrollwin.y_offset // item_height) - 1).max (1))
				end
			elseif k.code = key_constants.key_down then
				if not selected_items.is_empty
					and then attached selected_items.last as sl then
						scroll_to (sl)
				else
					-- Scroll down one item
					scroll_to_position (
						((scrollwin.y_offset // item_height) + 1).min (count))
				end
			else
				-- Ignore
			end
		end

	--|--------------------------------------------------------------

	key_uses_default_processing (k: EV_KEY): BOOLEAN
			-- Does the given key use default key processing?
			-- If not, then it must be processed on key press events
		require
			exists: k /= Void
		do
			Result := not (
				k.code = key_constants.key_page_down or
				k.code = key_constants.key_page_up or
				k.code = key_constants.key_home or
				k.code = key_constants.key_end)
		end

	--|--------------------------------------------------------------

	on_resize (a, ay, w, h: INTEGER)
		local
			rdt: like refresh_delay_timeout
		do
			if not refresh_delayed then
				create rdt
				rdt.actions.extend (agent setup_for_refresh)
				rdt.set_interval (50)
				refresh_delay_timeout := rdt
			end
		end

	--|--------------------------------------------------------------

	on_select (w: like position_list)
		local
			idx, pos: INTEGER
			ti: like item_anchor
			tl: LINKED_LIST [INTEGER]
			sl: DYNAMIC_LIST [like item_anchor]
		do
			if not updating_selections then
				updating_selections := True
				create tl.make
				sl := w.selected_items
				from sl.start
				until sl.exhausted
				loop
					ti := sl.item
					idx := w.index_of (ti, 1)
					tl.extend (idx)
					sl.forth
				end
				if w = position_list then
					select_items (name_list, tl)
				else
					select_items (position_list, tl)
				end
				idx := w.index_of (w.selected_items.first, 1)
				pos := ((idx + w.index_of (w.selected_items.last, 1)) // 2).max (1)
				scroll_to_show_position (pos)
				application_root.do_once_on_idle (agent name_list.set_focus)
				updating_selections := False
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	selected_item: detachable like item_anchor
		do
			Result := name_list.selected_item
		end

	item_anchor: EV_LIST_ITEM
			-- An attached 'like selected_item'
		require
			never: False
		do
			create Result.make_with_text ("dummy")
		end

	selected_items: DYNAMIC_LIST [like item_anchor]
		do
			Result := name_list.selected_items
		end

	--|--------------------------------------------------------------

	index: INTEGER
		do
			Result := name_list.index
		end

	--|--------------------------------------------------------------

	index_of (v: like item_anchor; spos: INTEGER): INTEGER
		do
			Result := name_list.index_of (v, spos)
		end

	--|--------------------------------------------------------------

	has (v: like item_anchor): BOOLEAN
		do
			Result := name_list.has (v)
		end

	--|--------------------------------------------------------------

	has_item_by_name (v: STRING): BOOLEAN
		local
			oc: CURSOR
		do
			oc := name_list.cursor
			from name_list.start
			until name_list.after or Result
			loop
				if name_list.item.text.is_equal (v) then
					Result := True
				else
					name_list.forth
				end
			end
			name_list.go_to (oc)
		end

	--|--------------------------------------------------------------

	count: INTEGER
		do
			Result := name_list.count
		end

	--|--------------------------------------------------------------

	item_position (v: STRING): INTEGER
			-- Position of item with given string
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
			not_destroyed: not is_destroyed
		local
			i: INTEGER
		do
			from
				name_list.start
				i := 1
			until name_list.after or Result /= 0
			loop
				if name_list.item.text.is_equal (v) then
					Result := i
				end
				name_list.forth
				i := i + 1
			end
		ensure
			valid_position: Result > 0 and Result <= count
		end

	--|--------------------------------------------------------------

	items_are_selected (vl: LIST [INTEGER]): BOOLEAN
			-- Are the items at the given positions all selected?
		require
			exists: vl /= Void
			not_destroyed: not is_destroyed
		local
			idx: INTEGER
		do
			Result := True
			from vl.start
			until vl.after or not Result
			loop
				idx := vl.item
				if idx <= 0 or idx > count then
					Result := False
				elseif not name_list.i_th (idx).is_selected then
					Result := False
				else
					vl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	item_by_data (d: ANY): like selected_item
		require
			exists: d /= Void
		do
			Result := name_list.retrieve_item_by_data (d, True)
		end

	--|--------------------------------------------------------------

	item_by_name (v: STRING): like selected_item
		local
			oc: CURSOR
		do
			oc := name_list.cursor
			from name_list.start
			until name_list.after or attached Result
			loop
				if name_list.item.text.is_equal (v) then
					Result := name_list.item
				else
					name_list.forth
				end
			end
			name_list.go_to (oc)
		end

	--|--------------------------------------------------------------

	item_is_demotable (v: STRING): BOOLEAN
			-- Is the item with the given name demotable?
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
		do
			if attached item_by_name (v) as ti then
				Result := index_of (ti, 1) < count
			end
		end

	--|--------------------------------------------------------------

	item_is_promotable (v: STRING):  BOOLEAN
			-- Is the item with the given name promotable?
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
		do
			if attached item_by_name (v) as ti then
				Result := index_of (ti, 1) > 1
			end
		end

	--|--------------------------------------------------------------

	item_height: INTEGER
			-- Height, in pixels, of items in list

	list_title: detachable STRING_GENERAL
			-- Title string of item list column heading

--|========================================================================
feature -- Status setting
--|========================================================================

	set_list_title (v: STRING_GENERAL)
		require
			exists: v /= Void
			not_destroyed: not is_destroyed
		do
			list_title := v
			item_label.set_text (v)
		end

	--|--------------------------------------------------------------

	enable_multiple_selection
		do
			position_list.enable_multiple_selection
			name_list.enable_multiple_selection
		end

	--|--------------------------------------------------------------

	disable_multiple_selection
		do
			position_list.disable_multiple_selection
			name_list.disable_multiple_selection
		end

	--|--------------------------------------------------------------

	number_of_items_in_view: INTEGER
			-- Number of items displayable at current height
		do
			Result := scrollwin.height // item_height
		end

	--|--------------------------------------------------------------

	scroll_to (v: like item_anchor)
		-- Scroll the window to expose the given item
		require
			exists: v /= Void
			belongs: has (v)
		local
			pos: INTEGER
		do
			pos := name_list.index_of (v, 1)
			if pos < number_of_items_in_view then
				scroll_to_top
			elseif pos > (count - number_of_items_in_view) then
				scroll_to_bottom
			else
				-- Scroll to expose position
--				scrollwin.set_y_offset ((item_height * (pos - 2)).max (0))
				scroll_to_position (pos)
			end
		end

	--|--------------------------------------------------------------

	scroll_to_top
		-- Scroll the window to the top
		do
			scrollwin.set_y_offset (0)
		end

	--|--------------------------------------------------------------

	scroll_to_bottom
		-- Scroll the window to the bottom (last item)
		do
			scrollwin.set_y_offset ((item_height *
				(count - number_of_items_in_view)).max (0))
		end

	--|--------------------------------------------------------------

	scroll_to_position (v: INTEGER)
		-- Scroll the window to expose the given item position
		-- Places 'v' at top of view
		do
			scrollwin.set_y_offset ((v - 1) * item_height)
		end

	--|--------------------------------------------------------------

	scroll_to_show_position (v: INTEGER)
		-- Scroll the window to expose the given item position
		local
			cpos: INTEGER
		do
			cpos := scrollwin.y_offset // item_height
			if (v <= (cpos + 1)) or v >= (cpos + number_of_items_in_view) then
				scroll_to_position (v)
			else
				-- Do nothing
			end
		end

	--|--------------------------------------------------------------

	promote_item_by_name (v: STRING)
			-- Move up in the list the item with the given name
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
			promotable: item_is_promotable (v)
		do
			if attached item_by_name (v) as ti then
				promote_item (ti)
			end
		end

	--|--------------------------------------------------------------

	demote_item_by_name (v: STRING)
			-- Move down in the list the item with the given name
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
			demotable: item_is_demotable (v)
		do
			if attached item_by_name (v) as ti then
				demote_item (ti)
			end
		end

	--|--------------------------------------------------------------

	select_item_by_name (v: STRING)
		require
			exists: v /= Void
			belongs: has_item_by_name (v)
		local
			idx: INTEGER
		do
			if attached item_by_name (v) as ti then
				idx := index_of (ti, 1)
				select_item_at_position (name_list, idx)
			end
		ensure
			selected: attached selected_item as i and then i.text.is_equal (v)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	updating_selections: BOOLEAN
	update_pending: BOOLEAN
	update_on_move: BOOLEAN

	refresh_delayed: BOOLEAN
		do
			Result := attached refresh_delay_timeout
		end

	--|--------------------------------------------------------------

	select_items (w: like position_list; vl: LIST [INTEGER])
		require
			widget_exists: w /= Void
			exists: vl /= Void
			not_destroyed: not is_destroyed
		do
			w.remove_selection
			from vl.start
			until vl.exhausted
			loop
				w.implementation.select_item (vl.item)
				vl.forth
			end
		end

	--|--------------------------------------------------------------

	select_item_at_position (w: like position_list; p: INTEGER)
		require
			widget_exists: w /= Void
			valid_position: p > 0 and p <= count
			not_destroyed: not is_destroyed
		do
			w.remove_selection
			w.implementation.select_item (p)
		end

--|========================================================================
feature {NONE} -- Priority changing
--|========================================================================

	move_selected_items (up: BOOLEAN)
			-- For each selected item (if any), move one position
			-- in the priority list (up if 'up', else down)
		require
			promotable: up implies selection_is_promotable
			demotable: (not up) implies selection_is_demotable
		local
			tl: like selected_items
			idx: INTEGER
			new_indices: LINKED_LIST [INTEGER]
		do
			tl := selected_items
			idx := index
			create new_indices.make
			if up then
				from tl.start
				until tl.after
				loop
					idx := index_of (tl.item, 1)
					promote_item_at_position (tl.item, idx)
					new_indices.extend (idx - 1)
					tl.forth
				end
			else
				from tl.finish
				until tl.before
				loop
					idx := index_of (tl.item, 1)
					demote_item_at_position (tl.item, idx)
					new_indices.extend (idx + 1)
					tl.back
				end
			end
			select_items (name_list, new_indices)
			if update_on_move then
				if up then
					scroll_to (tl.first)
				else
					scroll_to (tl.last)
				end
				application_root.do_once_on_idle (agent name_list.set_focus)
			end
		end

	--|--------------------------------------------------------------

	move_selected_items_to_top
			-- For each selected item (if any), move up in the priority
			-- list until the first selected item is at the top
		require
			promotable: selection_is_promotable
		local
			ti: like item_anchor
		do
			ti := selected_items.first
			from disable_update_on_move
			until ti = name_list.first
			loop
				move_selected_items (True)
			end
			scroll_to (ti)
			application_root.do_once_on_idle (agent name_list.set_focus)
			enable_update_on_move
		end

	--|--------------------------------------------------------------

	move_selected_items_to_bottom
			-- For each selected item (if any), move up in the priority
			-- list until the first selected item is at the bottom
		require
			promotable: selection_is_demotable
		local
			ti: like item_anchor
		do
			ti := selected_items.last
			from disable_update_on_move
			until ti = name_list.last
			loop
				move_selected_items (False)
			end
			scroll_to (ti)
			application_root.do_once_on_idle (agent name_list.set_focus)
			enable_update_on_move
		end

	--|--------------------------------------------------------------

	promote_item_at_position (v: like item_anchor; idx: INTEGER)
			-- Move up in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
			valid_index: idx = index_of (v, 1)
			promotable: idx > 1
		do
			name_list.go_i_th (idx)
			name_list.remove
			name_list.go_i_th (idx - 1)
			name_list.put_left (v)
		ensure
			promoted: index_of (v, 1) = (old index_of (v, 1) - 1)
		end

	--|--------------------------------------------------------------

	demote_item_at_position (v: like item_anchor; idx: INTEGER)
			-- Move down in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
			valid_index: idx = index_of (v, 1)
			demotable: idx < count
		do
			name_list.go_i_th (idx)
			name_list.remove
			name_list.go_i_th (idx)
			name_list.put_right (v)
		ensure
			demoted: index_of (v, 1) = (old index_of (v, 1) + 1)
		end

	--|--------------------------------------------------------------

	promote_item (v: like item_anchor)
			-- Move up in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
		local
			idx: INTEGER
		do
			idx := index_of (v, 1)
			promote_item_at_position (v, idx)
			select_item_at_position (name_list, idx - 1)
			scroll_to (v)
			application_root.do_once_on_idle (agent name_list.set_focus)
		ensure
			promoted: index_of (v, 1) = (old index_of (v, 1) - 1)
		end

	--|--------------------------------------------------------------

	demote_item (v: like item_anchor)
			-- Move down in the list the given item
		require
			exists: v /= Void
			belongs: has (v)
		local
			idx: INTEGER
		do
			idx := index_of (v, 1)
			promote_item_at_position (v, idx)
			select_item_at_position (name_list, idx + 1)
			scroll_to (v)
			application_root.do_once_on_idle (agent name_list.set_focus)
		ensure
			demoted: index_of (v, 1) = (old index_of (v, 1) + 1)
		end

	--|--------------------------------------------------------------

	selection_is_demotable: BOOLEAN
			-- Is there a current selection and is it demotable?
		local
			tl: like selected_items
			idx: INTEGER
		do
			tl := selected_items
			if not tl.is_empty then
				Result := True
				from tl.start
				until tl.after or not Result
				loop
					idx := index_of (tl.item, 1)
					if idx >= count then
						-- Can't do it; would go off end of list
						Result := False
					else
						tl.forth
					end
				end
			end
		end

	--|--------------------------------------------------------------

	selection_is_promotable:  BOOLEAN
			-- Is there a current selection and is it demotable?
		local
			tl: like selected_items
		do
			tl := selected_items
			if not tl.is_empty then
				Result := index_of (selected_items.first, 1) > 1
			end
		end

	--|--------------------------------------------------------------

	enable_update_on_move
		do
			update_on_move := True
		end

	disable_update_on_move
		do
			update_on_move := False
		end

--|========================================================================
feature {NONE} -- Geometry management
--|========================================================================

	setup_for_refresh
		do
			if refresh_delayed and then attached refresh_delay_timeout as rdt then
				rdt.set_interval (0)
			end
			if not update_pending then
				update_pending := True
				application_root.do_once_on_idle (idle_action)
			end
		end

	--|--------------------------------------------------------------

	update_geometry
		local
			swh: INTEGER
		do
			if refresh_delayed and then attached refresh_delay_timeout as rdt then
				rdt.set_interval (0)
			end
			name_list.set_minimum_width (width - position_list.width)
			update_pending := False
			refresh_delay_timeout := Void
			if not name_list.is_empty then
				item_height := item_height.max (name_list.first.height)
			end
			swh := height.max(name_list.count * (item_height + 1)).max (200)
			scrollwin.set_item_size (width, swh)
		end

	--|--------------------------------------------------------------

	idle_action: PROCEDURE

	refresh_delay_timeout: detachable EV_TIMEOUT

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_dflt_column_1_width: INTEGER = 30

	K_items_per_page: INTEGER = 5

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| 
--| Items can be added to the list using the 'add_item' procedure.
--| Items are simple strings, but can have data associated with them if 
--| desired.
--| 
--| The ranking behavior manifests in the position list to the left of the 
--| item list, and in the promotion/demotion behavior.
--| 
--| Pressing the Page-Up key, while pressing the Control key, with a 
--| selection promotes the selected item(s) by one position.
--| 
--| Pressing the End key, while pressing the Control key, moves the first
--| selected item to the top of the list, with the other selections,
--| if any, moving down relative to the first item.
--| 
--| Pressing the Page-Down key, while pressing the Control key, with a 
--| selection demotes the selected item(s) by one position.
--| 
--| Pressing the End key, while pressing the Control key, moves the last
--| selected item to the bottom of the list, with the other selections,
--| if any, moving down relative to the last item.
--| 
--| Arrow keys, mouse clicks and such all behave the same as with EV_LISTs.
--| The position and items lists are kept in lock-step.
--|----------------------------------------------------------------------

end -- class AEL_V2_RANKING_LIST
