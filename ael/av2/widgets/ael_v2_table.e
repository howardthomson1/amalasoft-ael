note
	description: "A container widget arranging items by row/column"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TABLE

inherit
	EV_TABLE
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as ev_implementation_attr
		redefine
			default_create_interface_objects, create_implementation
		select
			ev_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end


create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	implementation: like ev_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := ev_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_TABLE}
			implementation_attr := ev_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_TABLE}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|========================================================================
feature -- Component setting
--|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 29-Jul-2018
--|     Original, compile and tested using Eiffel 18.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, treat as an EV_TABLE
--|----------------------------------------------------------------------

end -- class AEL_V2_TABLE
