note
	description: "An abstract container of tiered button peers"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_TIER_BOX

inherit
	APPLICATION_ENV
		undefine
			default_create, copy, is_equal
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_strings (v: ARRAY [STRING]; pf: BOOLEAN)
			-- Create Current, adding peers with text from 'v'
			-- If 'pf', place label to left of button (dflt is right)
		local
			i, lim: INTEGER
		do
			make
			lim := v.upper
			from i := v.lower
			until i > lim
			loop
				extend_with_text (v.item (i), pf)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	make
			-- Create Current
		deferred
			-- Implementation from descendent
		end

	create_interface_objects
		do
			create peers.make
		end

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_sensitive
		local
			pl: like peers
		do
			pl := peers
			from pl.start
			until pl.after
			loop
				pl.item.enable_sensitive
				pl.forth
			end
		end

	disable_sensitive
		local
			pl: like peers
		do
			pl := peers
			from pl.start
			until pl.after
			loop
				pl.item.disable_sensitive
				pl.forth
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: ANY
		deferred
		end

	peers: TWO_WAY_LIST [AEL_V2_TIER_BUTTON]
			-- Tier button piers in Current

	peer_index (b: AEL_V2_TIER_BUTTON): INTEGER
			-- Index (1-based) of peer 'b' in peer list
			-- If not in peer list, zero
		local
			pl: like peers
			i: INTEGER
		do
			pl := peers
			from pl.start
			until Result > 0 or pl.after
			loop
				i := i + 1
				if pl.item = b then
					Result := i
				else
					pl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	first_selected_peer_index: INTEGER
			-- Index (1-based) of lowest-index peer in peer list
			-- that is presently selected
			-- If none is selected, then zero
		local
			pl: like peers
			i: INTEGER
		do
			pl := peers
			from pl.start
			until Result > 0 or pl.after
			loop
				i := i + 1
				if pl.item.is_selected then
					Result := i
				else
					pl.forth
				end
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend_with_text (v: STRING; pf: BOOLEAN)
			-- Add a new button, with text 'v', to Current
			-- If 'pf', place label to left of button (dflt is right)
		local
			tb: AEL_V2_TIER_BUTTON
		do
			create tb.make_with_label_and_options (v, True, 0)
			tb.disable_button_expand
			extend (tb)
		end

	extend (v: like item)
			-- Add `v' to end. Do not move cursor.
		do
			box_extend (v)
			if attached {AEL_V2_TIER_BUTTON}v as tb then
				tb.set_notification_procedure (agent on_button_select)
				peers.extend (tb)
			end
		end

	box_extend (v: like item)
			-- Add `v' to end. Do not move cursor.
		deferred
		end

	--|--------------------------------------------------------------

	update_peers
			-- Update peer list
		do
			peers.wipe_out
			if attached {EV_WIDGET_LIST}Current as cwl then
				from cwl.start
				until cwl.after
				loop
					if attached {AEL_V2_TIER_BUTTON}cwl.item as tb then
						peers.extend (tb)
					end
					cwl.forth
				end
			end
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	is_selecting: BOOLEAN
			-- Are we presently processing a selection even on one of 
			-- the enclosed tier buttons?  If so, ignore events until 
			-- done with that operation

	is_clearing: BOOLEAN
			-- Are we presently clearing selection states?

	--|--------------------------------------------------------------

	on_button_select (b: AEL_V2_TIER_BUTTON)
			-- Respond to select event on one of the enclosed tier 
			-- buttons
		local
			pl: like peers
			bp, np, si: INTEGER
		do
			if not (is_selecting or is_clearing) then
				update_peers
				is_selecting := True
				bp := peer_index (b)
				if bp = 0 then
					-- ERROR
				else
					if b.is_selected then
						-- Clear all, then select 'b' and any after it
						--  - X D D  -->  - S S S
						clear_selections
						pl := peers
						from pl.go_i_th (bp)
						until pl.after
						loop
							pl.item.enable_select
							pl.forth
						end
					else
						-- Has been de-selected OR re-selected, but for 
						-- re-selection, it must go through deslection (at 
						-- the widget, the reason we got this event) and 
						-- then re-selection here
						np := first_selected_peer_index
						if np = 0 then
							-- No peers remain selected; Done
							-- This happens when 'b' is last peer
						else
							if np < bp then
								-- 'b' was NOT first selected peer
								--  S S X S  -->  D D S S
								--    ^ ^             ^
								--   np bp            bp (si)
								-- 'bp' should be new first selected index
								si := bp
							else
								-- Cannot be same (it's deselected), so 'np' 
								-- must be higher than 'bp' (peer to its 
								-- right).  'np' should be the new first 
								-- selected index
								--  D X S S  -->  D D S S
								--    ^ ^             ^
								--   bp np            np (si)
								si := np
							end
							clear_selections
							pl := peers
							-- 'np' is index of NEW first-selected peer
							--  X S S S  -->  D S S S
							--  D X S S  -->  D D S S
							--  D S X S  -->  D D S S
							--  D D D X  -->  D D D D
							if si /= 0 then
								from pl.go_i_th (si)
								until pl.after
								loop
									pl.item.enable_select
									pl.forth
								end
							end
						end
					end
				end
				is_selecting := False
				notify
			end
		end

	--|--------------------------------------------------------------

	clear_selections
			-- Clear selection status of all enclosed tier buttons
		local
			pl: like peers
		do
			if not is_clearing then
				is_clearing := True
				pl := peers
				from pl.start
				until pl.after
				loop
					pl.item.disable_select
					pl.forth
				end
				is_clearing := False
			end
		end

	--|--------------------------------------------------------------

	select_peer_by_index (v: INTEGER)
			-- Select the 'v'th peer
		require
			valid: v > 0 and v <= peers.count
		do
			peers.i_th (v).enable_select
		end

	--|--------------------------------------------------------------

	set_notification_procedure (v: like notification_proc)
		do
			notification_proc := v
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	notification_proc: detachable PROCEDURE [TUPLE[like Current]]
	
	notify
		do
			if attached notification_proc as np then
				np.call ([Current])
			end
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 16-Aug-2019
--|     Original module
--|     Compiled and tested using Eiffel 18.7, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred.  Create an instance of a descendent of 
--| this class (horizontal or vertical), and add peers to it, where 
--| each peer is an instance of AEL_V2_TIER_BUTTON.
--|----------------------------------------------------------------------

end -- class AEL_V2_TIER_BOX
