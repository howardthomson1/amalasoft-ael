note
	description: "{
A labeled button that is a member of a tiered group.  Similar to radio
behavior in that one group member's selection status affects the others'
but selection is multiple and ordered.  If a member is selected (e.g. 2nd
one in group), then the subsequent members (e.g. 3rd and 4th) are selected
sympathetically.
Deselection is possible for only the lowest (ordinally) selected member,
but RE-selection is enabled for all button - effectively changing the selection
state of the group (akin to radio behavior)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2014 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TIER_BUTTON

inherit
	AEL_V2_HORIZONTAL_BOX
		redefine
			create_interface_objects, initialize_interface_actions,
			build_interface_components,
			set_background_color, set_foreground_color
		end

create
	make, make_with_label, make_with_label_and_options

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_label (v: STRING)
			-- Create a new widget with the label string 'v'
		do
			make
			set_label_text (v)
		end

	make_with_label_and_options (v: STRING; rx: BOOLEAN; gw: INTEGER)
			-- Create a new widget with the label string 'v'
			-- If 'rx', then allow right pad to expand (else is fixed at 
			-- gutter width)
			-- 'gw' is gutter width (space between label and widget and 
			-- at left and right ends of box)
		do
			private_gutter_width := gw
			right_is_expandable := rx
			make_with_label (v)
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	initialize_interface_actions
		do
			button.select_actions.extend (agent on_button_select)
			Precursor
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create label_text.make (0)
			create button.make_with_text (label_text)
			create left_pad
			create right_pad
			Precursor
		end

	--|--------------------------------------------------------------

	build_interface_components
		do
			-- Might want to offer left-pad
			extend (button)
			disable_item_expand (button)
			button.align_text_left

			extend (right_pad)
			Precursor
		end

--|========================================================================
feature -- Components
--|========================================================================

	button: EV_CHECK_BUTTON
	left_pad: EV_CELL
	right_pad: EV_CELL

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_button_select
		do
			notify
		end

--|========================================================================
feature -- Status
--|========================================================================

	label_text: STRING

	is_selected: BOOLEAN
			-- Is button presently in selected state?
		do
			Result := button.is_selected
		end

	--|--------------------------------------------------------------

	gutter_width: INTEGER
		do
			Result := private_gutter_width
			if Result = 0 then
				Result := 5
			end
		end

	--|--------------------------------------------------------------

	right_is_expandable: BOOLEAN
			-- Is right margin to be left expandable? Dflt is fixed

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_select
			-- Set selection state to 'selected'
		do
			button.enable_select
		end

	disable_select
			-- Set selection state to 'unselected'
		do
			button.disable_select
		end

	--|--------------------------------------------------------------

	set_label_text (v: STRING)
		do
			label_text.wipe_out
			label_text.append (v)
			update_label_text
		end
	 
	set_button_width (v: INTEGER)
		do
			button.set_minimum_width (v)
			disable_item_expand (button)
		end
	
	disable_button_expand
		do
			disable_item_expand (button)
		end

	--|--------------------------------------------------------------

	set_background_color (v: EV_COLOR)
		do
			Precursor (v)
			button.set_background_color (v)
			left_pad.set_background_color (v)
			right_pad.set_background_color (v)
		end

	set_foreground_color (v: EV_COLOR)
		do
			Precursor (v)
			button.set_foreground_color (v)
			left_pad.set_foreground_color (v)
			right_pad.set_foreground_color (v)
		end

	--|--------------------------------------------------------------

	set_notification_procedure (v: like notification_proc)
		do
			notification_proc := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_gutter_width: INTEGER
			-- Non-default gutter width

	--|--------------------------------------------------------------

	notification_proc: detachable PROCEDURE [TUPLE[like Current]]
	
	notify
		do
			if attached notification_proc as np then
				np.call ([Current])
			end
		end

	--|--------------------------------------------------------------

	update_label_text
		do
			if label_text.is_empty then
				button.remove_text
			else
				button.set_text (label_text)
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 16-Feb-2019
--|     Original module
--|     Re-compiled and tested using Eiffel 18.7, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using 
--| 'make' and subsequently setting the label text.
--|
--| Then add to an instance of AEL_V2_TIER_BOX (or proper descendent)
--| If you wish to receive a notification on each change to this widget,
--| you can register your agent using 'set_notification_procedure'.
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_TIER_BUTTON
