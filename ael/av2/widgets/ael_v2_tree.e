note
	description: "A simple tree widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2019/02/12 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TREE

inherit
	EV_TREE
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as tr_implementation_attr
		redefine
			default_create_interface_objects, create_implementation
		select
			tr_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization (from default_create)
--|========================================================================

	implementation: like tr_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := tr_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_TREE}
			implementation_attr := tr_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_TREE}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 12-Feb-2019
--|     Original module, compiled using Eiffel 18.7, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, treat as an EV_TREE
--|----------------------------------------------------------------------

end -- class AEL_V2_TREE
