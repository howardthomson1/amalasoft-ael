class AEL_V2_TREE_ITEM
-- An EV_TREE_ITEM that is comparable by its text value
-- and therefore sortable

inherit
	EV_TREE_ITEM
		undefine
			is_equal, copy
		redefine
			make_with_text, extend, force
		end
	COMPARABLE
		undefine
			default_create
		end

create
	make_with_text

--|========================================================================
feature {NONE} -- Creatin and initialization
--|========================================================================

	make_with_text (a_text: READABLE_STRING_GENERAL)
			-- Create `Current' and assign `a_text' to `text'
		do
			create lineage.make
			Precursor (a_text)
		end

--|========================================================================
feature -- Status
--|========================================================================

	data2: detachable ANY
			-- Reference to a secondary datum to associated with Current

	lineage: TWO_WAY_LIST [like Current]
			-- List of ancestors, from parent back

	conformant_item: detachable like Current
			-- Child of Current, compliant with class (vs EV_TREE_ITEM)
		do
			if attached {like Current} item as ti then
				Result := ti
			end
		end

	first_ancestor: detachable like Current
			-- Immediate parent item. FROM lineage
		do
			if not lineage.is_empty then
				Result := lineage.first
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_data2 (v: like data2)
			-- Assign data2 to 'v'
		do
			data2 := v
		ensure
			is_set: data2 = v
		end

--|========================================================================
feature -- Element change
--|========================================================================

	force (v: like item)
			-- Add `v' to end. Do not move cursor.
		do
			extend (v)
		end

	extend (v: like item)
			-- Add `v' to end. Do not move cursor.
		do
			Precursor (v)
			if attached {AEL_V2_TREE_ITEM} v as tv then
				tv.update_lineage_from_parent (Current)
			end
		end

	update_lineage_from_parent (v: like Current)
			-- Add lineage of Current from parent 'v'
		do
			lineage.wipe_out
			lineage.fill (v.lineage)
			lineage.put_front (v)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
			--
			-- COMPARISON IS BY TEXT VALUE ONLY, for sorting purposes
			--
			-- Note well that is_equal is also redefined this way!!!!
		do
			Result := text < other.text
		end

end -- class AEL_V2_TREE_ITEM
