note
  description: "{
	  Compound widget with a text field and a label and which provides 
	  a callback mechanism for validating the contents of the text field 
	  either as characters are entered, or on commit (tab out or return)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_VALIDATING_TEXT

inherit
	AEL_V2_LABELED_TEXT
		redefine
			create_interface_objects,
			initialize_interface_actions,
			set_text, create_widget,
			on_text_change
		end

create
	make, make_with_label, make_for_upper, make_for_lower

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================
	
	make_for_upper (lb: STRING; tf: BOOLEAN)
		do
			is_upper := True
			make_with_label (lb, tf)
		end

	make_for_lower (lb: STRING; tf: BOOLEAN)
		do
			is_lower := True
			make_with_label (lb, tf)
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
		do
			Precursor
			create original_text.make (0)
			create original_original_text.make (0)
		end

	--|--------------------------------------------------------------

	create_widget
		do
			if is_upper then
				create {AEL_V2_CASING_TEXT}textw.make_for_upper
			elseif is_lower then
				create {AEL_V2_CASING_TEXT}textw.make_for_lower
			else
				Precursor
			end
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	initialize_interface_actions
		do
--			textw.change_actions.extend (agent on_text_change)
			textw.key_press_actions.extend (agent on_text_keypress)
			Precursor
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	on_text_keypress (key: EV_KEY)
		require
			key_exists: key /= Void
		local
			kc: INTEGER
			ec: like key_constants
		do
			ec := key_constants
			kc := key.code
			if kc = ec.Key_escape then
				if text_has_changed then
					execute_cancel_action
				end
			elseif kc = ec.Key_enter or kc = ec.Key_tab then
				if text_has_changed then
					execute_ok_action
				else
					tab_out (False)
				end
			elseif text_has_changed then
				on_text_change
			else
				-- could be an arrow key
			end
		end
   
	--|--------------------------------------------------------------
   
	on_text_change
			-- Respond to textw changing content
		local
			ts: STRING
			cf: BOOLEAN
			--tf: BOOLEAN
		do
			if not (clearing or updating) then
				ts := textw.text
				if ts.is_empty then
					if original_text.is_empty then
						cf := False
					else
						cf := True
					end
				else
					if not ts.is_equal(original_text) then
						cf := True
					end
				end
				set_changed (cf)
			end
			if textw.is_displayed and textw.is_sensitive then
				textw.set_focus
			end
			if attached change_notification_proc as np then
				np.call ([Current])
			end
--			if (not text.is_empty) and attached live_validation_function as f then
--				f.call ([text])
--				tf := f.last_result
--				if tf then
			if text_is_valid then
					if cf then
						textw.set_background_color (in_flux_color)
					else
						textw.set_background_color (K_color_white_text_bg)
					end
				else
					set_invalid_text_color
				end
--			end
		end

	--|--------------------------------------------------------------

	tab_out (go_back: BOOLEAN)
		do
--RFO			if tab_out_proc /= Void then
--RFO				tab_out_proc.call ([Current, go_back])
--RFO			else
--RFO				--fake_tab_out
--RFO			end
		end

	--|--------------------------------------------------------------

	validation_function: detachable FUNCTION [STRING, BOOLEAN]
			-- validation function should have the form:
			-- func (v : STRING): BOOLEAN
			-- Where v is the string to be validated
 
	live_validation_function: detachable FUNCTION [STRING, BOOLEAN]
			-- validation function should have the form:
			-- func (v : STRING): BOOLEAN
			-- Where v is the string to be validated
 
	update_proc: detachable PROCEDURE [STRING]
			-- update function should have the form:
			-- proc (v : STRING)

 --|========================================================================
feature {NONE} -- Basic actions
 --|========================================================================
   
	execute_cancel_action
			-- Revert the text to the original value
		do
			if not original_original_text.is_empty then
				set_text (original_original_text)
			else
				set_text (original_text)
			end
		end
   
	--|--------------------------------------------------------------
   
	execute_ok_action
			-- Update values from the text widget
			-- set buttons insensitive
		local
			text_changed: BOOLEAN
		do
			if not executing_ok_action then
				executing_ok_action := True
				text_changed := not text.is_equal (original_text)
				if text_changed then
					if text_is_valid then
						update_text
						if not action_is_from_focus_out then
							tab_out (False )
							--fake_tab_out
						end
					else
						-- sound off at the user
						set_invalid_text_color
					end
				elseif  textw.background_color = in_flux_color   then
					-- Force to look changed, even though value matches original
					if text.is_equal (original_text) then
						if text_is_valid then
							update_text
							if not action_is_from_focus_out then
								tab_out (False)
								--fake_tab_out
							end
						else
							-- sound off at the user
							set_invalid_text_color
						end
					end
				else
					if not action_is_from_focus_out then
						-- simply tab out
						tab_out (False)
						--fake_tab_out
					end
				end
				executing_ok_action := False
			end
		end

	--|--------------------------------------------------------------

	notify_observer (is_changed: BOOLEAN)
		do
			if attached notification_proc as np then
				np.call ([Current])
			end
		end

--|========================================================================
feature -- Value setting
--|========================================================================
	
	ever_had_text: BOOLEAN

	set_text (v: STRING)
		do
			if not ever_had_text then
				original_text := v.twin
				original_original_text := v.twin
			end
			Precursor (v)
		end

 --|========================================================================
 feature -- Validation
 --|========================================================================
   
	 text_is_valid: BOOLEAN
		do
			if attached validation_function as vf then
				vf.call ([text])
				Result := vf.last_result
			else
				Result := True
			end
		end

	--|--------------------------------------------------------------

	set_validation_function (v: like validation_function)
		do
			validation_function := v
		end

	--|--------------------------------------------------------------

	set_live_validation_function (v: like live_validation_function)
		do
			live_validation_function := v
		end

	--|--------------------------------------------------------------

	set_update_procedure (v: like update_proc)
		do
			update_proc := v
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================
	
	set_changed (tf: BOOLEAN)
		do
			if is_read_only then
				textw.set_background_color (K_color_read_only)
			else
				if tf then
					textw.set_background_color (in_flux_color)
				else
					textw.set_background_color (K_color_white_text_bg)
				end
				notify_observer (tf)
			end
		end

	--|--------------------------------------------------------------

	set_change_committed
		do
			textw.set_background_color (K_color_committed_change)
			notify_observer (False)
		end
	
	disable_flux_color
		do
			flux_color_disabled := True
		end

--|========================================================================
feature -- Status
--|========================================================================
	
	is_upper: BOOLEAN
	is_lower: BOOLEAN
	
	flux_color_disabled: BOOLEAN

	--|--------------------------------------------------------------

	text_has_changed : BOOLEAN
		local
			ts: STRING
		do
			if not is_destroyed then
				ts := textw.text
				if ts.is_empty then
					Result := not original_text.is_empty
				else
					if (not ts.is_equal(original_text)) or 
						(textw.background_color = in_flux_color) then
							Result := True
					end
				end
			end
		end
   
	--|-------------------------------------------------------------
   
	text_is_original: BOOLEAN
		do
			Result := textw.text ~ original_original_text
		end

  --|--------------------------------------------------------------

	is_changing: BOOLEAN
		do
			Result := (textw.background_color = in_flux_color) and not updating
		end
  
  --|--------------------------------------------------------------

	clearing: BOOLEAN
	original_text: STRING
	original_original_text: STRING
	action_is_from_focus_out: BOOLEAN
	executing_ok_action: BOOLEAN
	is_read_only: BOOLEAN

 --|========================================================================
feature {NONE} -- Text change
 --|========================================================================
   
	update_text
		local
			ts: STRING
		do
			ts := textw.text
			check
				valid_text: text_is_valid
			end
			if attached update_proc as p then
				updating := True
				p.call ([text])
				updating := False
			end
			original_text := ts
			if not original_original_text.is_equal(ts) then
				set_change_committed
			else
				set_changed (False)
			end
		end

	--|--------------------------------------------------------------

	set_invalid_text_color
		do
			textw.set_background_color (K_color_error)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	K_color_white_text_bg: EV_COLOR
		once
			Result := ael_colors.white
		end

	K_color_read_only: EV_COLOR
		once
			Result := ael_colors.gray
		end

	K_color_error: EV_COLOR
		once
			Result := ael_colors.orange
		end
	
	in_flux_color: EV_COLOR
		do
			if flux_color_disabled then
				Result := K_color_in_flux2
			else
				Result := K_color_in_flux
			end
		end

	K_color_in_flux: EV_COLOR
		once
			Result := ael_colors.cyan
		end
  
	K_color_in_flux2: EV_COLOR
		once
			Result := ael_colors.extra_pale_blue
		end
  
	K_color_committed_change : EV_COLOR
		once
			Result := ael_colors.extra_pale_green
		end
	
--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this widget, create it using 'make_with_label',
--| giving it a string to use for the label component or using 
--| 'make' and subsequently setting the label text.
--|
--| You can optionally set the width of the text field, and the initial
--| text value.
--| Add the widget to your container and disable expansion of this
--| widget if desired.
--| If you wish to receive a notification on commit (enter actions)
--| of the string in the text widget, you can register your agent 
--| using the 'set_notification_procedure' routine.  In the agent,
--| simply query the 'text' routine for the committed value.
--| If you wish to receive notification on each character change, you
--| can register your agent using the 
--| 'set_change_notification_proc' routine.
--|----------------------------------------------------------------------

end -- class AEL_V2_VALIDATING_TEXT
