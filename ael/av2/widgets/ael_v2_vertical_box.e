note
	description: "A vertical container widget"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_VERTICAL_BOX

inherit
	EV_VERTICAL_BOX
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as vb_implementation_attr
		redefine
			default_create_interface_objects, create_implementation
		select
			vb_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects
		end


create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	implementation: like vb_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := vb_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_VERTICAL_BOX}
			implementation_attr := vb_implementation_attr
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_VERTICAL_BOX}
			Precursor {AEL_V2_WIDGET}
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
		end

--|========================================================================
feature -- Component setting
--|========================================================================

	add_separator
			-- Add a fixed-height vertical line into Current
		local
			sep: EV_HORIZONTAL_SEPARATOR
		do
			create sep
			extend (sep)
			sep.set_minimum_height (2)
			disable_item_expand (sep)
		end

	--|--------------------------------------------------------------

	add_space (sz: INTEGER)
			-- Add a fixed-size space of 'sz' pixels into Current
		require
			valid_size: sz > 0
		local
			tc: EV_CELL
		do
			create tc
			extend (tc)
			tc.set_minimum_height (sz)
			disable_item_expand (tc)
		end

	--|--------------------------------------------------------------

	add_free_space
			-- Add an unconstrained space into Current
		do
			extend (create {EV_CELL})
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, treat as an EV_VERTICAL_BOX, except for using
--| and of the features exclusive to this class.
--|----------------------------------------------------------------------

end -- class AEL_V2_VERTICAL_BOX
