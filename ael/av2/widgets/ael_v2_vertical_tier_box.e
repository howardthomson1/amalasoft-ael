note
	description: "A vertical container of tiered button peers"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_VERTICAL_TIER_BOX

inherit
	AEL_V2_VERTICAL_BOX
		rename
			extend as box_extend
		undefine
			enable_sensitive, disable_sensitive
		redefine
			create_interface_objects
		end
	AEL_V2_TIER_BOX
		redefine
			create_interface_objects
		end

create
	make, make_with_strings

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor {AEL_V2_VERTICAL_BOX}
			Precursor {AEL_V2_TIER_BOX}
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Component setting
--|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 16-Feb-2019
--|     Original module
--|     Re-compiled and tested using Eiffel 18.7, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| After creation, add tier buttons explicity or via 'extend_with_text'
--|----------------------------------------------------------------------

end -- class AEL_V2_VERTICAL_TIER_BOX
