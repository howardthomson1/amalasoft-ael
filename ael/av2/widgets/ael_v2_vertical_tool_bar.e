note
	description: "A vertically oriented toolbar"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_VERTICAL_TOOL_BAR

inherit
	AEL_V2_VERTICAL_BOX
		rename
			extend as box_extend
		end

create
	make

--|========================================================================
feature -- Components
--|========================================================================

	toolbars: LINKED_LIST [EV_TOOL_BAR]
			-- List of subordinate toolbar widgets
		do
			create Result.make
			from start
			until exhausted
			loop
				if attached {EV_TOOL_BAR} item as tb then
					Result.extend (tb)
				end
				forth
			end
		end

--|========================================================================
feature -- Component setting
--|========================================================================

	extend (v: EV_TOOL_BAR_ITEM)
			-- Add a new tool bar item
		require
			exists: v /= Void
		local
			tb: EV_TOOL_BAR
		do
			create tb
			box_extend (tb)
			disable_item_expand (tb)
			tb.extend (v)
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Create instances of this class by calling 'make'.
--| To add buttons or other tool bar items, simply call extend as you would
--| with the classic horizontal tool bar.
--|----------------------------------------------------------------------

end -- class AEL_V2_VERTICAL_TOOL_BAR
