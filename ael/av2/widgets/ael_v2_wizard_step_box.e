note
	description: "A box widget representing a step in an AEL_V2_WIZARD_DIALOG"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_WIZARD_STEP_BOX

inherit
	AEL_V2_VERTICAL_BOX
		redefine
			create_interface_objects,
			build_interface_components,
			initialize_interface_colors
		end

create
	make, make_with_inset_pixmap

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_inset_pixmap (v: EV_PIXMAP)
			-- Create Current with inset pixmap 'v'
		require
			pixmap_exists: v /= Void
		do
			panel_inset_pixmap := v
			make
		end

 --|========================================================================
feature {NONE} -- Initialization (during default_create)
 --|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			create upper_box
			create help_area
			create help_textw
			create inset
			create {EV_VERTICAL_BOX}widget_area
		end

 --|========================================================================
feature {NONE} -- Initialization (after default_create)
 --|========================================================================

	initialize_interface_colors
			-- Initialize colors for interface components
		do
			upper_box.set_background_color (K_color_help_bg)
			help_area.set_background_color (K_color_help_bg)
			help_textw.set_background_color (K_color_help_bg)
			if attached inset as ti then
				ti.set_background_color (K_color_help_bg)
			end
		end

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	build_interface_components
		local
			sep: EV_HORIZONTAL_SEPARATOR
			--tc: EV_CELL
		do
			extend (upper_box)
			upper_box.set_minimum_height (K_help_area_height)
			disable_item_expand (upper_box)

			build_upper_box

			create sep
			extend (sep)
			sep.set_minimum_height (3)
			disable_item_expand (sep)

			extend (widget_area)

			build_widget_area
		end

	--|--------------------------------------------------------------

	build_upper_box
		local
			--sep: EV_HORIZONTAL_SEPARATOR
			tc: EV_CELL
			ibw: INTEGER
		do
			upper_box.set_border_width (10)
			upper_box.extend (help_area)
			--help_area.set_minimum_height (K_help_area_height)
			--upper_box.disable_item_expand (help_area)
			if use_help_textw then
				help_area.extend (help_textw)
				help_area.set_item_position (help_textw, 0, 0)
				help_textw.align_text_left
				help_textw.align_text_vertical_center
			end
			if attached panel_inset_pixmap as pm then
				create tc
				upper_box.extend (tc)
				tc.set_background_color (K_color_help_bg)
				tc.set_minimum_width (10)
				upper_box.disable_item_expand (tc)

				upper_box.extend (inset)
				inset.extend (pm)
				inset.set_minimum_width (K_help_area_height)
				ibw := (((K_help_area_height - pm.width) // 2) - 20).max (0)
				inset.set_border_width (ibw)
				upper_box.disable_item_expand (inset)
			end
			build_help_area
		end

	--|--------------------------------------------------------------

	build_widget_area
			-- Add text and additional controls to widget area
		require
			widget_area_exists: widget_area /= Void
		do
		end

	--|--------------------------------------------------------------

	build_help_area
			-- Add text and additional controls to help (upper) area
		require
			help_area_exists: help_area /= Void
		do
		end


--|========================================================================
feature -- Status
--|========================================================================

	use_help_textw: BOOLEAN
			-- Use a text widget for help text (vs using labels in a fixed)

	was_skipped: BOOLEAN

	ok_to_advance: BOOLEAN
		do
			Result := True
		end

	--|--------------------------------------------------------------

	ok_to_show_next: BOOLEAN
			-- Should the next button be sensitive?
			-- Not necessarily the same as ok_to_advance, but typically is.
			-- This can be redefine to true in order to trip and error report
			-- for a less-than-obvious condition
		do
			Result := ok_to_advance
		end

	--|--------------------------------------------------------------

	ok_to_show_skip: BOOLEAN
			-- Should the skip button be sensitive?
		do
			Result := False
		end

	is_relevant: BOOLEAN
			-- Is Current relevant, given state of things?
		do
			Result := True
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	extend_help_area (v: EV_WIDGET; xp, yp: INTEGER)
		require
			widget_exists: v /= Void
			valid_positions: xp >= 0 and yp >= 0
		do
			if v /= Void then
				help_area.extend (v)
				help_area.set_item_position (v, xp, yp)
				v.set_background_color (K_color_help_bg)
			end
		end

	set_help_text (v: STRING)
		do
			help_textw.set_text (v)
		end

	extend_help_text (v: STRING)
		local
			ts: STRING
		do
			ts := help_textw.text
			if ts.is_empty then
				help_textw.set_text (v)
			else
				if ts.item (ts.count) /= '%N' then
					ts.append ("%N" + v)
					help_textw.set_text (ts)
				end
			end
		end

	--|--------------------------------------------------------------

	set_skipped (tf: BOOLEAN)
		do
			was_skipped := tf
		end

 --|========================================================================
feature -- Validation
 --|========================================================================

 --|========================================================================
feature -- Error Support
 --|========================================================================

	report_error_for_step
		do
		end

 --|========================================================================
feature {AEL_V2_WIZARD_DIALOG} -- Agents and Callbacks
 --|========================================================================

	on_select
			-- Actions to perform upon selection
		do
		end

	--|--------------------------------------------------------------

	take_focus
		do
			--O    if is_displayed and is_sensitive then
			--O      set_focus
			--O    end
		end

	--|--------------------------------------------------------------

	set_status_change_procedure (v: like status_change_proc)
		do
			status_change_proc := v
		end

	status_change_proc: detachable PROCEDURE

 --|========================================================================
feature {NONE}
 --|========================================================================

	notify_client_on_status_change
		do
			if attached status_change_proc as p then
				p.call ([])
			end
		end

 --|========================================================================
feature -- Components
 --|========================================================================

	upper_box: EV_HORIZONTAL_BOX
	inset: EV_VERTICAL_BOX
	help_area: EV_FIXED
			-- Use fixed to permit exact item placement
	help_textw: EV_LABEL

	widget_area: EV_CONTAINER
			-- In descendent, either redefine here, or use subclass create

	panel_inset_pixmap: detachable EV_PIXMAP

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	K_help_area_height: INTEGER
		do
			Result := 80
		end

	--|--------------------------------------------------------------

	K_color_help_bg: EV_COLOR
		do
			Result := ael_colors.white
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| This widget represents a step in a wizard sequence.
--| It contains an upper area and a lower area (aka widget_area).
--| The upper area includes an inset pixmap and a help area (for 
--| informative text)
--|
--| To use this box, create it using 'make_with_inset_pixmap',
--| giving it a pixmap, or by using 'make' and subsequently
--| setting the inset.
--|
--| The
--|----------------------------------------------------------------------

end -- class AEL_V2_WIZARD_STEP_BOX
