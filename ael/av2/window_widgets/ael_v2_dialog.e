note
	description: "A generic Eiffel Vision dialog window"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_DIALOG

inherit
	EV_DIALOG
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as ed_implementation_attr
		redefine
			make_with_title,
			initialize, default_create_interface_objects, create_implementation
		select
			ed_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			implementation,
			default_create_interface_objects,
			build_interface_components,
			initialize_interface_actions,
			initialize_interface_images,
			post_initialize
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_title (v: READABLE_STRING_GENERAL)
			-- Create Current with title string 'v'
		do
			make
			set_title (v)
		end

	--|--------------------------------------------------------------

	initialize
		do
			Precursor {EV_DIALOG}
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor {EV_DIALOG}
			Precursor {AEL_V2_WIDGET}
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			create main_container
			create working_area
			create control_frame.make
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		do
			Precursor
			build_main_container
			extend (main_container)
			fill_working_area
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			set_initial_size
			initialize_default_push_button
		end

	initialize_default_push_button
			-- Set the default dialog push button, if any
		do
 			set_default_push_button (control_frame.cancel_button)
		end

	--|--------------------------------------------------------------

	set_initial_size
		local
			w, h, wmw, wmh, mcw, mch, mcmw, mcmh: INTEGER
		do
			w := window_initial_width
			h := window_initial_height

			wmw := minimum_width
			wmh := minimum_height

			mcmw := main_container.minimum_width
			mcmh := main_container.minimum_height

			mcw := main_container.width
			-- Allow for title bar at least, redefine in child if 
			-- necessary (e.g. there is a toolbar, menu bar, etc), ??
			-- OR, just calculate the proper initial values
			mch := main_container.height

			w := w.max (wmw.max (mcmw.max (mcw)))
			h := h.max (wmh.max (mcmh.max (mch)))

			if w /= 0 and h /= 0 then
				set_size (w, h)
			end
		end

	--|--------------------------------------------------------------

	initialize_interface_images
			-- Initialize pixmaps and other images for interface components
		do
			set_icon_pixmap (new_application_pixmap)
			-- To have icon appear, also call 'show_icon_and_closer'
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	build_main_container
		do
			add_working_area
			add_mb_separator
			add_control_frame
		end

	--|--------------------------------------------------------------

	add_working_area
		do
			main_container.extend (working_area)
		end

	add_mb_separator
			-- Add to top of main container a horizontal separator
		local
			sep: EV_HORIZONTAL_SEPARATOR
		do
			create sep
			main_container.extend (sep)
			sep.set_minimum_height (2)
			main_container.disable_item_expand (sep)
		end

	add_control_frame
			-- Add to main container the control frame
		do
 			main_container.extend (control_frame)
			main_container.disable_item_expand (control_frame)
			control_frame.set_parent_dialog (Current)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			show_actions.extend (agent on_show)
			close_request_actions.extend (agent execute_cancel_action)
 			control_frame.set_cancel_action_proc (agent execute_cancel_action)
 			control_frame.set_ok_action_proc (agent execute_ok_action)
			Precursor
		end

	--|--------------------------------------------------------------

	fill_working_area
		-- Add the components that go into the dialog's working area
		require
			working_area_exists: working_area /= Void
			has_main_container: main_container.has (working_area)
			not_filled: working_area.is_empty
		deferred
		ensure
			filled: not working_area.is_empty
		end

--|========================================================================
feature -- Status
--|========================================================================

	committed_value: detachable ANY
		do
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update_committed_value
			-- Executed on OK, the committed value exists only on a commit
		do
		end

	set_apply_action_proc (v: like apply_action_proc)
		do
			apply_action_proc := v
		end

	show_icon_and_closer
		do
			implementation.enable_closeable
		end

	hide_ok_button
		do
			control_frame.hide_ok_button
		end

	show_ok_button
		do
			control_frame.show_ok_button
		end

	--|--------------------------------------------------------------

	disable_ok_button_sensitive
		do
			control_frame.ok_button.disable_sensitive
		end

	enable_ok_button_sensitive
		do
			control_frame.ok_button.enable_sensitive
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	main_container:  EV_VERTICAL_BOX
	working_area: EV_VERTICAL_BOX
	control_frame: AEL_V2_DIALOG_CTL_FRAME

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_show
		do
			control_frame.process_on_show_actions
		end

	--|--------------------------------------------------------------

	execute_ok_action
		require
			shown: is_displayed
			not_destroyed: not is_destroyed
		do
			update_committed_value
			if attached apply_action_proc as p then
				if attached committed_value as v then
					p.call ([v])
				end
			end
			destroy
		end

	--|--------------------------------------------------------------

	execute_cancel_action
		do
			destroy
		end

	--|--------------------------------------------------------------

	apply_action_proc: detachable PROCEDURE

 --|========================================================================
feature {NONE}	-- Component widgets
 --|========================================================================

	cancel_button: EV_BUTTON
		do
			Result := control_frame.cancel_button
		end

	ok_button: EV_BUTTON
		do
			Result := control_frame.ok_button
		end

 --|========================================================================
feature {NONE} -- Sizes
 --|========================================================================

	window_initial_width: INTEGER
		do 
			Result := 600
		end

	window_initial_height: INTEGER
		do
			Result := 450
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: like ed_implementation_attr
		do
			Result := ed_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
		do
			Precursor {EV_DIALOG}
			implementation_attr := ed_implementation_attr
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and cannot be instantiated.  It is 
--| inherited by special-purpose dialog classes.
--|----------------------------------------------------------------------

end -- class AEL_V2_DIALOG
