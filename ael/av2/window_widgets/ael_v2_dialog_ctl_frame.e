note
	description: "Control frame for an AEL_V2_DIALOG"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_DIALOG_CTL_FRAME

inherit
	AEL_V2_HORIZONTAL_BOX
		redefine
			create_interface_objects,
			build_interface_components,
			update_widget_rendition,
			post_initialize,
			initialize_interface_actions
		end

create
	make, make_with_parent_dialog

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_parent_dialog (wd: like parent_dialog)
		require
			dialog_exists: wd /= Void
		do
			parent_dialog := wd
			make
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			create ok_button.make_with_text (ok_label_string)
			create cancel_button.make_with_text (cancel_label_string)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			set_border_width (K_border_width)
			set_minimum_height (K_min_height)
			Precursor
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		do
			extend (create {EV_CELL})
			extend (ok_button)
			disable_item_expand (ok_button)
			extend (create {EV_CELL})
			extend (cancel_button)
			disable_item_expand (cancel_button)
			extend (create {EV_CELL})

			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			ok_button.select_actions.extend (agent on_ok_select)
			cancel_button.select_actions.extend (agent on_cancel_select)
			if attached parent_dialog as pd then
--RFO 			pd.set_default_push_button (ok_button)
--RFO 			pd.set_default_cancel_button (cancel_button)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	confirmed: BOOLEAN
			--	 Has the user confirmed a value or option?

	is_applicable: BOOLEAN
			-- Is it OK to apply the current value or option?
		do
			if attached confirmation_func as cf then
				Result := cf.item ([])
			else
				Result := True
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_default_button_focus
			-- Set focus to the default push button
		do
			cancel_button.set_focus
		end

 --|------------------------------------------------------------------------	

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
			if is_applicable then
				ok_button.enable_sensitive
			else
				ok_button.disable_sensitive
			end
		end

	--|--------------------------------------------------------------

	process_on_show_actions
			-- Respond to expose/show events
		do
			-- Likely needed only on first show
			if attached parent_dialog as pd then
				if pd.has_recursive (ok_button) then
					pd.set_default_push_button (ok_button)
				else
					-- OK button is hidden
					pd.set_default_push_button (cancel_button)
				end
				pd.set_default_cancel_button (cancel_button)
			end
			update_widget_rendition
			set_default_button_focus
		end

--|========================================================================
feature {AEL_V2_DIALOG} -- Components
--|========================================================================

	parent_dialog: detachable AEL_V2_DIALOG
			-- Dialog for which Current is the control frame

	cancel_button: EV_BUTTON
	ok_button: EV_BUTTON

	hide_ok_button
		do
			--TODO Rearrange to put Cancel btn in center
			wipe_out
			extend (create {EV_CELL})
--			extend (ok_button)
--			extend (create {EV_CELL})
			extend (cancel_button)
			disable_item_expand (cancel_button)
			extend (create {EV_CELL})
		end

	show_ok_button
		do
			--TODO
			wipe_out
			extend (create {EV_CELL})
			extend (ok_button)
			disable_item_expand (ok_button)
			extend (create {EV_CELL})
			extend (cancel_button)
			disable_item_expand (cancel_button)
			extend (create {EV_CELL})
		end

	disable_ok_button_sensitive
		do
			ok_button.disable_sensitive
		end

	enable_ok_button_sensitive
		do
			ok_button.enable_sensitive
		end

--|========================================================================
feature {AEL_V2_DIALOG} -- Status setting
--|========================================================================

	set_parent_dialog (v: like parent_dialog)
			-- Set parent dialoge to 'v'
		do
			parent_dialog := v
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_ok_select
			-- Responde to a select event on the OK button
		do
			if attached confirmation_func as cf then
				confirmed := cf.item ([])
				if confirmed then
					if attached ok_action_proc as op then
						op.call ([])
					end
				end
			else
				if attached ok_action_proc as op then
					op.call ([])
				end
			end
		end

	--|--------------------------------------------------------------

	on_cancel_select
			-- Respond to a select event on the Cance button
		do
			if attached cancel_action_proc as cp then
				cp.call ([])
			elseif attached parent_dialog as pd then
				pd.destroy
			end
		end

--|========================================================================
feature {AEL_V2_DIALOG} -- Agent setting
--|========================================================================

	set_cancel_action_proc (v: like cancel_action_proc)
			-- Set to 'v' the procedure to call on Cancel button selection
		do
			cancel_action_proc := v
		end

	set_ok_action_proc (v: like ok_action_proc)
			-- Set to 'v' the procedure to call on OK button selection
		do
			ok_action_proc := v
		end

	set_confirmation_function (v: like confirmation_func)
			-- Set to 'v' the function used for effecting confirmation
		do
			confirmation_func := v
		end

	--|--------------------------------------------------------------

	cancel_action_proc: detachable PROCEDURE
			-- The procedure to call on Cancel button selection

	ok_action_proc: detachable PROCEDURE
			-- The procedure to call on OK button selection

	--|------------------------------------------------------------------------

	confirmation_func: detachable FUNCTION [BOOLEAN]
			-- The function used for effecting confirmation

 --|========================================================================
feature {AEL_V2_DIALOG} -- Constants
 --|========================================================================

	cancel_label_string: STRING
		do
			Result := "Cancel"
		end

	ok_label_string: STRING
		do
			Result := "OK"
		end

	save_label_string: STRING
		do
			Result := "Save"
		end

	back_label_string: STRING
		do
			Result := "< Back"
		end

	next_label_string: STRING
		do
			Result := "Next >"
		end

	skip_label_string: STRING
		do
			Result := "Skip >>"
		end

	start_label_string: STRING
		do
			Result := "Start >>"
		end

	finish_label_string: STRING
		do
			Result := "Finish"
		end

	--|--------------------------------------------------------------

	K_min_height: INTEGER = 30
	K_border_width: INTEGER = 5

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make' or 'make_with_parent_dialog'
--| then add to main container in dialog
--|----------------------------------------------------------------------

end -- class AEL_V2_DIALOG_CTL_FRAME
