note
	description: "A top-level window for use with EiffelBuild"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_V2_EB_MAIN_WINDOW

inherit
	MAIN_WINDOW_IMP
      rename
			create_interface_objects as eb_create_interface_objects,
			initialize as eb_initialize,
			implementation as eb_implementation_attr
		undefine
			make_with_title
		redefine
			create_implementation
		select
			eb_create_interface_objects, eb_implementation_attr
		end
	AEL_V2_MAIN_WINDOW
		undefine
			is_in_default_state
		redefine
			initialize, implementation,
			create_implementation,
			new_application_pixmap
		select
			initialize
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	initialize
			-- Initialize Current
		do
			eb_initialize
		end

	--|--------------------------------------------------------------

	frozen user_initialization
			-- Perform any initialization on objects created by 
			-- `user_create_interface_objects' and from within current class 
			-- itself.
		do
--RFO 			complete_initialization
		end

	--|--------------------------------------------------------------

	frozen user_create_interface_objects
			-- This routine is called from the EiffelBuild-generated 
			-- MAIN_WINDOW_IMP and has a terrible name.  The AV2 cluster
			-- does what it can to remedy that by freezing this 
			-- redirection function and offering an empty routine
			-- for redefinition in the child called more appropriately
			-- 'create_interface_objects'
		do
			--| This instruction should be the last one in this routine
			--| From AEL_V2_WIDGET.default_create_interface_objects
			--| but lost by renaming
 			av2_create_interface_objects
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	new_application_pixmap: EV_PIXMAP
			-- Pixmap for use in title bar and in minimal icon
		do
			create {AEL_V2_BOXES_APP_PIXMAP}Result.make
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: like eb_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := eb_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {MAIN_WINDOW_IMP}
			tw_implementation_attr := eb_implementation_attr
			implementation_attr := eb_implementation_attr
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class using either the default_create 
--| or the make_with_title creation function
--|----------------------------------------------------------------------

end -- class AEL_V2_MAIN_WINDOW
