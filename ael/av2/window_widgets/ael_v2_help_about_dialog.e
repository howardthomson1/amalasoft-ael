note
	description: "A generic dialog window showing 'about' information"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_HELP_ABOUT_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions,
			initialize_interface_images,
			initialize_interface_strings,
			post_initialize, control_frame,
			window_initial_width, window_initial_height
		end

create
	make, make_with_title_and_message

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	make_with_title_and_message (t, m: STRING)
		do
			private_message := m
			make_with_title (t)
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor
			create message_label
--RFO 			create ok_button.make_with_text (Button_ok_item)
			create field_pixmap_box
		end

	--|--------------------------------------------------------------

	fill_working_area
		-- Create the components that go into the dialog's working area
		local
			main_horizontal_box: EV_HORIZONTAL_BOX
			left_vertical_box: EV_VERTICAL_BOX
			right_vertical_box: EV_VERTICAL_BOX
			sep: EV_HORIZONTAL_SEPARATOR
--RFO 			buttons_box: EV_HORIZONTAL_BOX
			tc: EV_CELL
		do
			create sep

--RFO 			ok_button.set_minimum_size (75, 24)
--RFO 
--RFO 			create buttons_box
--RFO 			buttons_box.extend (create {EV_CELL})
--RFO 			buttons_box.extend (ok_button)
--RFO 			buttons_box.disable_item_expand (ok_button)

			create left_vertical_box
			left_vertical_box.set_border_width (7)

			field_pixmap_box.extend (field_pixmap)

			left_vertical_box.extend (field_pixmap_box)
--			left_vertical_box.disable_item_expand (field_pixmap_box)
			left_vertical_box.extend (create {EV_CELL})

			create right_vertical_box
			right_vertical_box.set_padding (7)
			right_vertical_box.extend (message_label)
			right_vertical_box.extend (sep)
			right_vertical_box.disable_item_expand (sep)
--RFO 			right_vertical_box.extend (buttons_box)
--RFO 			right_vertical_box.disable_item_expand (buttons_box)

			create main_horizontal_box
			main_horizontal_box.set_border_width (7)
			create tc
			tc.set_minimum_width (21)
			main_horizontal_box.extend (tc)
			main_horizontal_box.disable_item_expand (tc)
			main_horizontal_box.extend (left_vertical_box)
			main_horizontal_box.disable_item_expand (left_vertical_box)
			create tc
			tc.set_minimum_width (28)
			main_horizontal_box.extend (tc)
			main_horizontal_box.disable_item_expand (tc)
			main_horizontal_box.extend (right_vertical_box)
			working_area.extend (main_horizontal_box)
		end

	--|------------------------------------------------------------------------

	initialize_interface_strings
			-- Initialize string for interface components
		do
			set_title (help_about_title)
			set_message (help_about_message)
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_images
			-- Initialize images for interface components
		do
			set_icon_pixmap (title_bar_pixmap)
		end

	--|------------------------------------------------------------------------

	initialize_interface_actions
			-- Define actions to be triggered based on certain events
		do
--RFO 			ok_button.select_actions.extend (agent destroy)
			Precursor
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			message_label.align_text_left
--RFO 			set_default_push_button (ok_button)
--RFO 			set_default_cancel_button (ok_button)
			set_size (400, 150)
			Precursor
		end

 --|========================================================================
feature -- Access
 --|========================================================================

	message: STRING
			-- Message displayed in the dialog box.
		do
			Result := message_label.text
		end

 --|========================================================================
feature -- Element change
 --|========================================================================

	set_message (msg: STRING)
		require
			exists: msg /= Void
		do
			message_label.set_text (msg)
		end

	set_field_pixmap (pm: like field_pixmap)
		do
			private_field_pixmap := pm
			field_pixmap_box.replace (field_pixmap)
		end

	set_title_bar_pixmap (pm: like title_bar_pixmap)
		do
			private_title_bar_pixmap := pm
			set_icon_pixmap (title_bar_pixmap)
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	field_pixmap: EV_PIXMAP
			-- Pixmap displayed on the left of the dialog.
		do
			if attached private_field_pixmap as pm then
				Result := pm
			else
				Result := create {AEL_V2_AMALASOFT_48X48_PIXMAP}.make
			end
			Result.set_minimum_size (Result.width, Result.height)
		end

	title_bar_pixmap: EV_PIXMAP
			-- Pixmap displayed in the title bar of the dialog.
		do
			if attached private_title_bar_pixmap as pm then
				Result := pm
			else
				Result := create {AEL_V2_APPLICATION_PIXMAP}.make
			end
			Result.set_minimum_size (Result.width, Result.height)
		end

	--|--------------------------------------------------------------

	message_label: EV_LABEL
			-- Label situated on the top of the dialog,
			-- contains the message.

	field_pixmap_box: EV_CELL

--RFO 	ok_button: EV_BUTTON
			-- "OK" button.

 --|========================================================================
feature {NONE} -- Implementation / Constants
 --|========================================================================

	help_about_title: STRING
		do
			Result := string_constants.help_about_title
			if attached private_title as t then
				Result := t
			else
			end
		end

	help_about_message: STRING
		do
			Result := string_constants.help_about_message
			if attached private_message as m then
				Result := m
			end
		end

	--|--------------------------------------------------------------

	private_title: detachable STRING
	private_message: detachable STRING
	private_field_pixmap: detachable EV_PIXMAP
	private_title_bar_pixmap: detachable EV_PIXMAP

	control_frame: AEL_V2_INFO_DIALOG_CTL_FRAME

 --|========================================================================
feature {NONE} -- Sizes
 --|========================================================================

	window_initial_width: INTEGER
		do 
			Result := 350
		end

	window_initial_height: INTEGER
		do
			Result := 175
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using 'make_with_title_and_message',
--| giving it a title string and a message string, or by using 
--| 'make' and subsequently setting the title and message.
--|
--| Typically, this dialog will be a local, created within a routine
--| that responds to a menu bar action (help->about).  Show the dialog
--| as modal (show_modal_to_window).
--|----------------------------------------------------------------------

end -- class AEL_V2_HELP_ABOUT_DIALOG
