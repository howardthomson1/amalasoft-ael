note
	description: "An AEL_V2_DIALOG with a message label and that launches another dialog"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_LAUNCHER_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions,
			initialize_interface_strings,
			post_initialize, control_frame, on_show,
			window_initial_width, window_initial_height
		end

create
	make_with_title_and_message

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	make_with_title_and_message (t, m: STRING)
		do
			message := m
			make_with_title (t)
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create message_label.make_with_text (message)
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		-- Add the components that go into the dialog's working area
		do
			working_area.extend (message_label)
			message_label.align_text_center
		end

	--|--------------------------------------------------------------

	initialize_interface_strings
			-- Initialize string for interface components
		do
			Precursor
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			--set_size (400, 150)
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text_font (v: EV_FONT)
		require
			exists: v /= Void
		do
			message_label.set_font (v)			
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_show
		do
			Precursor
			execute_launch_action
		end

	execute_launch_action
		require
			shown: is_displayed
			not_destroyed: not is_destroyed
		do
			if attached launch_action as tla then
				tla.call
			end
		end

 --|========================================================================
feature -- State and state setting
 --|========================================================================

	message: STRING

	set_launch_action (v: PROCEDURE)
		do
			launch_action := v
		end

	launch_action: detachable PROCEDURE

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	message_label: EV_LABEL
	control_frame: AEL_V2_LAUNCHER_DIALOG_CTL_FRAME

	window_initial_width: INTEGER
		do 
			Result := 350
		end

	window_initial_height: INTEGER
		do
			Result := 175
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Jul-2019
--|     Adapted from existing AEL_V2 dialogs
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_V2_LAUNCHER_DIALOG
