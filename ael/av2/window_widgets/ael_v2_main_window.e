note
	description: "A top-level window"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_MAIN_WINDOW

inherit
	EV_TITLED_WINDOW
      rename
			create_interface_objects as default_create_interface_objects,
			implementation as tw_implementation_attr
		redefine
			make_with_title, initialize, default_create_interface_objects,
			create_implementation
		select
			tw_implementation_attr
		end
	AEL_V2_WIDGET
		redefine
			make, implementation,
			default_create_interface_objects,
			initialize_interface_actions, initialize_interface_images,
			post_initialize
		end

create
	make, make_with_title

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_title (v: READABLE_STRING_GENERAL)
			-- Create Current with title string 'v'
		do
			make
			set_title (v)
		end

	make
		do
			default_create
			-- Do NOT call complete_initialization from here lest it
			-- subverts the application root startup sequence
		end

	--|--------------------------------------------------------------

	initialize
			-- Initialize Current
		do
			Precursor {EV_TITLED_WINDOW}
		end

	--|--------------------------------------------------------------

	default_create_interface_objects
			-- Create interface objects as part of the creation sequence
			-- for Current
		do
			Precursor {AEL_V2_WIDGET}
			Precursor {EV_TITLED_WINDOW}
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create interface objects as part of the creation sequence
			-- for Current
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			initialize_close_actions
		end

	initialize_close_actions
			-- Set up app exit (close) actions
			-- N.B. if so desired, redefine and/or assign to any 
			-- exit/close items or buttons (e.g. file_exit_item)
		do
			close_request_actions.wipe_out
			close_request_actions.extend (agent request_close_window)
		end

	initialize_interface_images
			-- Initialize pixmaps and other images for interface components
		do
			set_icon_pixmap (new_application_pixmap)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed
			-- after the other operations defined from
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			set_window_size_values
			set_initial_size
		end

	--|--------------------------------------------------------------

	set_initial_size
			-- Set size of Current at start up
		do
			if window_initial_width /= 0 and window_initial_height /= 0 then
				set_size (window_initial_width, window_initial_height)
			end
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	request_close_window
			-- Process user request to close the window.
		local
			qd: EV_CONFIRMATION_DIALOG
		do
			create qd.make_with_text ("Press OK to exit application.")
			qd.set_title ("Confirm application exit")
			qd.show_modal_to_window (Current)
			if qd.selected_button ~ (create {EV_DIALOG_CONSTANTS}).ev_ok then
				-- Destroy the window.
				destroy
				-- End the application.
				if attached (create {EV_ENVIRONMENT}).application as a then
					a.destroy
				end
			end
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	set_window_size_values
			-- Set the shared window size values from the current
			-- values
		do
			window_initial_width_cell.put (K_window_initial_width)
			window_initial_height_cell.put (K_window_initial_height)
		end

	K_window_initial_width: INTEGER
			-- Initial window width; redefine in child as desired
		do
			Result := 800
		end

	K_window_initial_height: INTEGER
			-- Initial window height; redefine in child as desired
		do
			Result := 600
		end

	--|--------------------------------------------------------------

	window_initial_width: INTEGER
			-- Initial window width via shared cell
			-- DO NOT edit or redefine; needs to be compatible with
			-- EiffelBuild and redefinable for adaptation only
			-- by av2_builder
		do
			Result := window_initial_width_cell.item
		end

	window_initial_width_cell: CELL [INTEGER]
			-- Shared cell holding initial window width
			-- DO NOT edit or redefine; needs to be compatible with
			-- EiffelBuild and redefinable for adaptation only
			-- by av2_builder
		once
			create Result.put (K_window_initial_width)
		end

	window_initial_height: INTEGER
			-- Initial window height via shared cell
			-- DO NOT edit or redefine; needs to be compatible with
			-- EiffelBuild and redefinable for adaptation only
			-- by av2_builder
		do
			Result := window_initial_height_cell.item
		end

	window_initial_height_cell: CELL [INTEGER]
			-- Shared cell holding initial window height
			-- DO NOT edit or redefine; needs to be compatible with
			-- EiffelBuild and redefinable for adaptation only
			-- by av2_builder
		once
			create Result.put (K_window_initial_height)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: like tw_implementation_attr
			-- Environment-specific implementation for Current
		do
			Result := tw_implementation_attr
		end

	--|--------------------------------------------------------------

	create_implementation
			-- Create the environment-specific implementation for Current
		do
			Precursor {EV_TITLED_WINDOW}
			implementation_attr := tw_implementation_attr
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class using either the 'make'
--| or the 'make_with_title' creation function
--|----------------------------------------------------------------------

end -- class AEL_V2_MAIN_WINDOW
