note
	description: "{
Control frame for an AEL_V2_DIALOG intended for relative showing,
with no OK button or action
On Cancel, dialog is hidden, NOT destroyed
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_RELATIVE_DIALOG_CTL_FRAME

inherit
	AEL_V2_DIALOG_CTL_FRAME
		redefine
			cancel_label_string, on_cancel_select,
			build_interface_components,
			update_widget_rendition,
			process_on_show_actions
		end

create
	make, make_with_parent_dialog

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		do
			extend (create {EV_CELL})
			extend (cancel_button)
			extend (create {EV_CELL})
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
			cancel_button.enable_sensitive
		end

	--|--------------------------------------------------------------

	process_on_show_actions
			-- Respond to expose/show events
		do
			-- Likely needed only on first show
			if attached parent_dialog as pd then
				pd.set_default_push_button (cancel_button)
			end
			update_widget_rendition
			set_default_button_focus
		end

	on_cancel_select
		do
			if attached parent_dialog as pd then
				pd.hide
			end
		end

 --|========================================================================
feature {AEL_V2_DIALOG} -- Constants
 --|========================================================================

	cancel_label_string: STRING
		do
			Result := "Close"
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make' or 'make_with_parent_dialog'
--| then add to main container in dialog
--|----------------------------------------------------------------------

end -- class AEL_V2_RELATIVE_DIALOG_CTL_FRAME
