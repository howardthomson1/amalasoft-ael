note
	description: "An AEL_V2_TEXT_DIALOG with a Rich Text widget as working area"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2018/04/05 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_RICH_TEXT_DIALOG

inherit
	AEL_V2_TEXT_DIALOG
		redefine
			textw
		end
create
	make, make_with_title

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	textw: EV_RICH_TEXT

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 05-May-2018
--|     Adapted from AEL_V2_TEXT_DIALOG
--|     Compiled and tested void-safe using Eiffel 18.1
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using 'make_with_title',
--| giving it a title string, or by using 'make' and subsequently
--| setting the title.
--|----------------------------------------------------------------------

end -- class AEL_V2_RICH_TEXT_DIALOG
