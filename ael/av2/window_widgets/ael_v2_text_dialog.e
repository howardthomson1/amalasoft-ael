note
	description: "An AEL_V2_DIALOG with a text widget as working area"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TEXT_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions,
			control_frame
		end

create
	make, make_with_title

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create textw
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		-- Add the components that go into the dialog's working area
		do
			working_area.extend (textw)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			textw.change_actions.extend (agent on_text_change)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_save
		do
			control_frame.set_save_action_proc (agent execute_save_action)
		end

	disable_save
		do
			control_frame.set_save_action_proc (Void)
		end

	--|--------------------------------------------------------------

	set_text_font (v: EV_FONT)
		require
			exists: v /= Void
		do
			textw.set_font (v)			
		end

	--|--------------------------------------------------------------

	set_text (v: STRING)
		do
			textw.set_text (v)
		end

	--|--------------------------------------------------------------

	set_preferred_save_path (v: like preferred_save_path)
		do
			preferred_save_path := v
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	textw: EV_TEXT
	control_frame: AEL_V2_TEXT_DIALOG_CTL_FRAME

	save_button: EV_BUTTON
		do
			Result := control_frame.save_button
		end

	preferred_save_path: detachable PATH

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	execute_save_action
		require
			shown: is_displayed
			not_destroyed: not is_destroyed
		local
			fd: EV_FILE_SAVE_DIALOG
			fn: STRING_8
			tf: PLAIN_TEXT_FILE
			cd: EV_CONFIRMATION_DIALOG
			ok_to_save: BOOLEAN
		do
			create fd
			if attached preferred_save_path as p then
				fd.set_start_path (p)
			end
			fd.show_modal_to_window (Current)
			if not fd.file_name.is_empty then
				fn := fd.file_name.to_string_8
				create tf.make_with_name (fn)
				if not tf.exists then
					ok_to_save := True
				else
					-- Overwrite existing file?
					create cd.make_with_text ("File exists.  OK to overwrite?")
					cd.show_modal_to_window (Current)
					if cd.selected_button ~ "OK" then
						ok_to_save := True
					end
				end
				if ok_to_save then
					tf.open_write
					if not tf.is_open_write then
						set_error_message ("Unable to open " + fn + " for write.")
					else
						tf.put_string (textw.text)
						tf.close
					end
				end
			end
		end

	--|--------------------------------------------------------------

	on_text_change
		do
			if attached default_push_button then
				remove_default_push_button
			end
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using 'make_with_title',
--| giving it a title string, or by using 'make' and subsequently
--| setting the title.
--|----------------------------------------------------------------------

end -- class AEL_V2_TEXT_DIALOG
