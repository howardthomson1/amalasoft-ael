note
	description: "Control frame for an AEL_V2_TEXT_DIALOG"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TEXT_DIALOG_CTL_FRAME

inherit
	AEL_V2_DIALOG_CTL_FRAME
		redefine
			parent_dialog,
			initialize_interface_actions, create_interface_objects,
			build_interface_components,
			update_widget_rendition, hide_ok_button, show_ok_button
		end

create
	make, make_with_parent_dialog


--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create save_button.make_with_text (save_label_string)
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	build_interface_components
		do
			Precursor

			--| Insert the save button between the OK and Cancel 
			--| buttons

			from start
			until item = cancel_button
			loop
				forth
			end

			put_left (save_button)
			put_left (create {EV_CELL})
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			save_button.select_actions.extend (agent on_save_select)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	hide_ok_button
		do
			wipe_out
			extend (create {EV_CELL})

			extend (save_button)
			extend (create {EV_CELL})

			extend (cancel_button)
			extend (create {EV_CELL})
		end

	show_ok_button
		do
			wipe_out
			extend (create {EV_CELL})

			extend (ok_button)
			extend (create {EV_CELL})

			extend (save_button)
			extend (create {EV_CELL})

			extend (cancel_button)
			extend (create {EV_CELL})
		end

--|========================================================================
feature {AEL_V2_TEXT_DIALOG} -- Components
--|========================================================================

	parent_dialog: detachable AEL_V2_TEXT_DIALOG
	save_button: EV_BUTTON

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_save_select
		do
			if attached save_action_proc as sp then
				sp.call ([])
			end
		end

--|========================================================================
feature {AEL_V2_TEXT_DIALOG} -- Agent setting
--|========================================================================

	set_save_action_proc (v: like save_action_proc)
		do
			save_action_proc := v
			update_widget_rendition
		end

	--|--------------------------------------------------------------

	save_action_proc: detachable PROCEDURE

--|========================================================================
feature {NONE} -- Status setting
--|========================================================================

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
			if attached save_action_proc then
				save_button.enable_sensitive
			else
				save_button.disable_sensitive
			end
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make' or 'make_with_parent_dialog'
--| then add to main container in dialog
--|----------------------------------------------------------------------

end -- class AEL_V2_TEXT_DIALOG_CTL_FRAME
