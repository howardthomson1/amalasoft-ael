note
	description: "{
An AEL_V2_DIALOG with a text field widget that accepts user input
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TEXT_FIELD_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions, update_widget_rendition,
			control_frame, window_initial_width, window_initial_height,
			committed_value, on_show
		end

create
	make, make_with_title, make_with_title_and_label

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_title_and_label (v, lb: STRING)
			-- Create Current with title string 'v' and text field label 'lb'
		do
			private_field_label_text := lb
			make
			set_title (v)
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create textw.make_with_label (field_label_text, True)
			create help_msg_label.make_with_text (help_msg_text)
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		-- Add the components that go into the dialog's working area
		local
			tc: EV_CELL
		do
			working_area.set_border_width (10)

			working_area.extend (help_msg_label)
--			help_msg_label.set_minimum_height (60)
--			help_msg_label.align_text_left
			working_area.disable_item_expand (help_msg_label)

			create tc
			working_area.extend (tc)
			tc.set_minimum_height (10)
			working_area.disable_item_expand (tc)

			working_area.extend (textw)
--			textw.set_label_width (80)
			textw.set_minimum_height (30)
			textw.set_right_gutter_width (10)
			working_area.disable_item_expand (textw)

			create tc
			working_area.extend (tc)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			textw.textw.change_actions.extend (agent on_text_change)
		end

--|========================================================================
feature -- Status
--|========================================================================

	committed_value: STRING
		do
			Result := text
		end

	text: STRING
			-- Contents of textw
		do
			Result := textw.text
		end

	field_label_text: STRING
			-- Label string for text field
		do
			if attached private_field_label_text as lb then
				Result := lb
			else
				Result := "Input:"
			end
		end

	help_msg_text: STRING
			-- Label string for text field
		do
			if attached private_field_label_text as lb then
				Result := lb
			else
				Result := "Input:"
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text_font (v: EV_FONT)
		require
			exists: v /= Void
		do
			textw.set_text_font (v)
		end

	--|--------------------------------------------------------------

	set_field_label_text (v: STRING)
			-- Set text field label to 'v'
		do
			private_field_label_text := v
			textw.set_label_text (v)
		end

	--|--------------------------------------------------------------

	set_help_msg_text (v: STRING)
			-- Set text of help message label to 'v'
		do
			private_help_msg_text := v
			help_msg_label.set_text (v)
		end

	--|--------------------------------------------------------------

	set_text (v: STRING)
		do
			textw.set_text (v)
		end

	--|--------------------------------------------------------------

	update_widget_rendition
		do
			--Precursor
			control_frame.update_widget_rendition
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	help_msg_label: EV_LABEL
	textw: AEL_V2_LABELED_TEXT
	control_frame: AEL_V2_TEXT_FIELD_DIALOG_CTL_FRAME

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_show
		do
			set_height (window_initial_height - 5)
			Precursor
			textw.textw.set_focus
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_text_change
		do
			update_widget_rendition
		end

 --|========================================================================
feature {NONE} -- Sizes
 --|========================================================================

	window_initial_width: INTEGER
		do 
			Result := 400
		end

	window_initial_height: INTEGER
		do
			Result := 165
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_field_label_text: detachable STRING
			-- Internal label string for text field
			-- Set by 'set_field_label_text'

	private_help_msg_text: detachable STRING
			-- Internal label string for help label

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2018
--|     Adapted from earlier ael_v2_text_dialog
--|     Compiled using Eiffel 18.07, void-safe
--|----------------------------------------------------------------------

end -- class AEL_V2_TEXT_FIELD_DIALOG
