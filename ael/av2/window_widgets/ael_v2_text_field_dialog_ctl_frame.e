note
	description: "Control frame for an AEL_V2_TEXT_FIELD_DIALOG"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_TEXT_FIELD_DIALOG_CTL_FRAME

inherit
	AEL_V2_DIALOG_CTL_FRAME
		redefine
			parent_dialog, is_applicable
		end

create
	make, make_with_parent_dialog


--|========================================================================
feature {AEL_V2_TEXT_DIALOG} -- Components
--|========================================================================

	parent_dialog: detachable AEL_V2_TEXT_FIELD_DIALOG

--|========================================================================
feature -- Status
--|========================================================================

	is_applicable: BOOLEAN
			-- Is it OK to apply the current value or option?
		do
			if Precursor then
				if attached parent_dialog as pd and then not pd.text.is_empty then
					Result := True
				end
			end
		end

--|========================================================================
feature {NONE} -- Status setting
--|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make' or 'make_with_parent_dialog'
--| then add to main container in dialog
--|----------------------------------------------------------------------

end -- class AEL_V2_TEXT_FIELD_DIALOG_CTL_FRAME
