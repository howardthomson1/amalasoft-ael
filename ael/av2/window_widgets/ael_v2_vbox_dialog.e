note
	description: "An AEL_V2_DIALOG with a vertical box as working area"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_VBOX_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			initialize_interface_actions
		end

create
	make, make_with_title

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create main_box
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		-- Add the components that go into the dialog's working area
		do
			working_area.extend (main_box)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	main_box: EV_VERTICAL_BOX
			-- Main widget within working area

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

 --|========================================================================
feature -- State and state setting
 --|========================================================================

 --|========================================================================
feature {NONE}	-- Component widgets
 --|========================================================================

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 17-Feb-2018
--|     Adapted from existing AEL_V2_TEXT_DIALOG
--|     Compiled and tested using Eiffel 17.05, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this dialog, create it using 'make_with_title',
--| giving it a title string, or by using 'make' and subsequently
--| setting the title.
--|----------------------------------------------------------------------

end -- class AEL_V2_VBOX_DIALOG
