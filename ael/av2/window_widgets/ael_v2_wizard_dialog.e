note
	description: "An AEL_V2_DIALOG containing a sequence of wizard boxes"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 002$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_WIZARD_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects, make_with_title,
			initialize_interface_actions,
			initialize_default_push_button, on_show,
			control_frame, window_initial_width, window_initial_height
		end

--TODO add reinitialization so window can be re-opened with clean 
--state

create
	make_with_title

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	make_with_title (v: STRING)
		do
			title_base := v.twin
			Precursor (v)
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor
			create step_sets.make
			create step_set_1.make
			step_sets.extend (step_set_1)
			steps := step_set_1
--			create steps.make
			create step_history.make
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	fill_working_area
		-- Add the components that go into the dialog's working area
		do
			--| this is step 1
			build_steps
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			control_frame.set_back_action_proc (agent execute_back_action)
			control_frame.set_next_action_proc (agent execute_next_action)
			control_frame.set_skip_action_proc (agent execute_skip_action)
		end

	--|--------------------------------------------------------------

	initialize_default_push_button
			-- Set the default dialog push button
		do
			set_default_push_button (control_frame.next_button)
			cancel_button.set_text ("Close")
		end

	--|--------------------------------------------------------------

	frozen build_steps
		require
			steps_exist: steps /= Void
		local
			tw: like new_opening_step
		do
			initialize_step_history
			tw := new_opening_step
			working_area.extend (tw)
			steps.extend (tw)

			build_latter_steps
			number_of_steps := steps.count
		end

	--|--------------------------------------------------------------

 	new_opening_step: EV_BOX
 		do
 			create {EV_HORIZONTAL_BOX}Result
 		end

	--|--------------------------------------------------------------

	build_latter_steps
		require
			only_first_exists: steps /= Void and then not steps.is_empty
		do
			add_step_widget (new_step_widget (1))
			add_step_widget (new_step_widget (2))
		ensure
			no_loss: steps.count >= old steps.count
		end

	--|--------------------------------------------------------------

	new_step_widget (idx: INTEGER): like current_widget
		do
			create Result.make
			Result.extend_help_area (
				create {EV_LABEL}.make_with_text("step" + idx.out), 10, 10)
			Result.extend_help_area (
				create {EV_LABEL}.make_with_text("more"), 20, 27)
		end

	--|--------------------------------------------------------------

	add_step_widget (w: like current_widget)
		require
			widget_exists: w /= Void
		do
			check attached w as tw then
				steps.extend (tw)
				tw.set_status_change_procedure (agent on_status_change)
			end
		ensure
			added: attached w as tw implies steps.has (tw)
			more_steps: steps.count = ((old steps.count) + 1)
		end

	--|--------------------------------------------------------------

	initialize_step_history 
		do
			step_index := 1
			step_history.extend (step_index)
		end

--|========================================================================
feature -- Status
--|========================================================================

	title_base: STRING

	--|------------------------------------------------------------------------

	-- TODO, support multiple step sets to enable multiple paths
	step_sets: TWO_WAY_LIST [like step_set_1]
	step_set_1: TWO_WAY_LIST [EV_WIDGET]
	steps: TWO_WAY_LIST [EV_WIDGET]
			-- defined sequence of widgets

	number_of_steps: INTEGER
			-- Total steps in collection

	number_of_actual_steps: INTEGER
			-- Total RELEVANT steps; might be reduced, depending on early 
			-- step options
			-- Does not include intro step, but does include finish
		do
			Result := number_of_steps - 1
		end

	step_history: TWO_WAY_LIST [INTEGER]
			-- Step (by index) actually visited (for support of branches and skips)

	--|------------------------------------------------------------------------

	start_label: STRING
		do
			Result := control_frame.start_label_string
		end

	next_label: STRING
		do
			Result := control_frame.next_label_string
		end

	finish_label: STRING
		do
			Result := control_frame.finish_label_string
		end

--|========================================================================
feature {AEL_V2_WIZARD_STEP_BOX} -- Components
--|========================================================================

	control_frame: AEL_V2_WIZARD_DIALOG_CTL_FRAME

	opening_step_splash_pixmap: detachable EV_PIXMAP
		do
			if attached Ks_opening_step_splash_pixmap_filename as fn then
				Result := ael_pixmaps.new_pixmap (fn)
			end
		end

	--|------------------------------------------------------------------------

	next_button: EV_BUTTON
		do
			Result := control_frame.next_button
		end

	back_button: EV_BUTTON
		do
			Result := control_frame.back_button
		end

	--|========================================================================
feature {NONE} -- Agents and actions
	--|========================================================================

	on_show
			-- Agent for handling a show even
		do
			Precursor
			update_button_rendition
		end

	on_status_change
			-- Agent for change notification from (latter) step widgets
		do
		end

	--|--------------------------------------------------------------

	execute_next_action
		local
			--next_ok: BOOLEAN
			--cw: like current_widget
			nsteps: INTEGER
		do
			--RFO if attached current_widget as w then
			--RFO 	cw := w
			--RFO end
			--RFO if at_last_step then
			--RFO 	--RFO TODO
			--RFO 	--RFO				execute_ok_action
			--RFO elseif at_first_step then
			--RFO 	next_ok := True
			--RFO elseif cw /= Void and then cw.ok_to_advance then
			--RFO 	next_ok := True
			--RFO else
			--RFO 	if cw /= Void then
			--RFO 		cw.report_error_for_step
			--RFO 	end
			--RFO end
			if attached current_widget as w then
				--RFO nsteps := steps_to_advance (w)
				--RFO if nsteps = 0 then
				--RFO 	if at_last_step then
				--RFO 		--RFO TODO
				--RFO 		--RFO				execute_ok_action
				--RFO 	else
				--RFO 		w.report_error_for_step
				--RFO 	end
				--RFO else
				--RFO 	w.set_skipped (False)
				--RFO 	push_step (nsteps)
				--RFO 	advancing := True
				--RFO 	set_view_to_current_step
				--RFO 	advancing := False
				--RFO end
				if false and at_last_step then
				else
					nsteps := steps_to_advance (w)
					if nsteps = 0 then
						w.report_error_for_step
					else
						w.set_skipped (False)
						push_step (nsteps)
						advancing := True
						set_view_to_current_step
						advancing := False
					end
				end
			end
		end

	--|--------------------------------------------------------------

	execute_skip_action
			-- Skip is a next without commitment
		local
			skip_ok: BOOLEAN
			cw: like current_widget
		do
			if attached current_widget as w then
				cw := w
			end
			if at_last_step then
			elseif at_first_step then
			elseif cw /= Void and then cw.ok_to_show_skip then
				skip_ok := True
			end

			if skip_ok then
				if cw /= Void then
					cw.set_skipped (True)
				end
				push_step (1)
				advancing := True
				set_view_to_current_step
				advancing := False
			end
		end

	--|--------------------------------------------------------------

	execute_back_action
		do
			pop_step
			set_view_to_current_step
			if attached current_widget as w then
				w.set_skipped (False)
			end
		end

	--|========================================================================
feature -- State and state setting
	--|========================================================================

	advancing: BOOLEAN

			--|--------------------------------------------------------------

	set_view_to_current_step
		local
			wb: like current_widget
		do
--TODO update window title to show 'n of m'
			if attached advance_end_notify_proc as p then
				p.call ([])
			end
			if not working_area.is_empty then
				if attached {like current_widget} working_area.first as w then
					wb := w
					wb.hide
				end
			end
			working_area.wipe_out
			if attached i_th_widget (step_index) as w then
				working_area.extend (w)
			end
			update_button_rendition
			if attached current_widget as w then
				wb := w
				wb.show
			end
			set_focus
			if wb /= Void then
				wb.on_select
				if step_is_relevant then
					wb.enable_sensitive
				else
					wb.disable_sensitive
				end
			end

			update_title_string
		end

	update_title_string
		local
			si, st: INTEGER
		do
			si := step_index
			if si = 1 or si = number_of_steps then
				set_title (title_base)
			else
				-- Show progress in title, but treat intro as non-step 
				-- when reporting count
				st := si - 1
				set_title (aprintf (
					"%%s  Step %%d of %%d",
					<< title_base, st, number_of_actual_steps >>))
			end
		end

	--|--------------------------------------------------------------

	update_button_rendition
		do
			if at_last_step then
				control_frame.disable_skip_button
				if not next_button.text.is_equal(finish_label) then
					next_button.set_text (finish_label)
				end
			elseif at_first_step then
				if not next_button.text.is_equal (start_label) then
					next_button.set_text (start_label)
				end
			else
				if not next_button.text.is_equal(next_label) then
					next_button.set_text (next_label)
				end
			end

			if transit_next_ok and step_index /= number_of_steps then
				next_button.enable_sensitive
				--      next_button.set_focus
			else
				next_button.disable_sensitive
				--O      if (current_widget /= Void) then
				--O	current_widget.take_focus
				--O      else
				--O	cancel_button.set_focus
				--O      end
			end

			if transit_back_ok then
				back_button.enable_sensitive
			else
				back_button.disable_sensitive
			end

			if transit_skip_ok then
				control_frame.enable_skip_button
			else
				control_frame.disable_skip_button
			end
		end

	--|--------------------------------------------------------------

	transit_next_ok: BOOLEAN
			-- Is it OK to proceed?
		do
			if at_first_step then
				Result := True
			else
				Result := attached current_widget as w and then
					w.ok_to_show_next
			end
		end

	--|--------------------------------------------------------------

	transit_back_ok: BOOLEAN
			-- Is it OK to go back?
		do
			if not at_first_step then
				Result := True
			end
		end

	--|--------------------------------------------------------------

	transit_skip_ok: BOOLEAN
			-- Is it OK to skip over the current step?
		do
			if at_first_step or at_last_step then
				Result := False
			else
				Result := attached current_widget as w and then
					w.ok_to_show_skip or not step_is_relevant
			end
		end

	--|--------------------------------------------------------------

	is_current_widget (w: like current_widget): BOOLEAN
		do
			Result := current_widget = w
		end

	--|--------------------------------------------------------------

	step_is_showable (w: detachable EV_WIDGET): BOOLEAN
		do
			if attached {like current_widget} w as tw then
				Result := True
			end
		end

	--|--------------------------------------------------------------

	advance_on_selection (w: like current_widget)
		require
			widget_exists: w /= Void
			widget_is_active: is_current_widget(w)
		do
			execute_next_action
		end

	--|========================================================================
feature {NONE} -- State transition support
	--|========================================================================

	step_index: INTEGER

			--|--------------------------------------------------------------

	at_last_step: BOOLEAN
			-- Is current step the last one in this logical sequence?
			-- Might NOT be last one in collection, depending on state 
			-- deriving from earlier steps
			-- Add any custom logic in descendent
		do
			Result := step_index = number_of_actual_steps
		end

	at_first_step: BOOLEAN
		do
			Result := step_index = 1
		end

	step_is_relevant: BOOLEAN
			-- Is the Current step (current widget) relevant?
			-- If not, then should skipable, and should be insensitive
		do
			if attached current_widget as w then
				Result := w.is_relevant
			end
		end

	--|--------------------------------------------------------------

	pop_step
		do
			--| Rather than decrement the index, this routine uses the
			--| transition history to go back to the previous step.
			--| Because steps are not added to the history unless they
			--| are showable during advancing, it is not necessary to
			--| to check for showability on retracing
			
			if step_history.count > 1 then
				step_history.finish
				step_history.remove
				step_history.finish
				step_index := step_history.item
			else
				step_index := 1
			end
		ensure
			showable: step_is_showable (current_widget)
		end

	--|--------------------------------------------------------------

	push_step (sc: INTEGER)
		local
			ti: INTEGER
			tw: detachable EV_WIDGET
		do
			if attached advance_begin_notify_proc as p then
				p.call ([])
			end
			step_index := step_index + sc
			ti := step_index
			if ti <= number_of_steps then
				from tw := steps.i_th (ti)
				until (tw = Void) or else
					(step_is_showable (tw) or ti > number_of_steps)
				loop
					ti := ti + 1
					tw := steps.i_th (ti)
				end
				step_index := ti
			end
			if step_index > number_of_steps then
				step_index := number_of_steps
			end
			step_history.extend (step_index)
		end

	--|--------------------------------------------------------------

	steps_to_advance (w: AEL_V2_WIZARD_STEP_BOX): INTEGER
		local
			si: INTEGER
		do
			if at_first_step then
				Result := 1
			elseif at_last_step then
				-- Last step might not be last step overall, but last 
				-- step in state-driven sequence
				si := number_of_steps - step_index
				Result := si
			elseif w.ok_to_advance then
				Result := 1
			end
		end

	--|========================================================================
feature {NONE}	-- Component widgets
	--|========================================================================

--RFO 	inset_pixmap_width: INTEGER
--RFO 		do
--RFO 			if attached panel_inset_pixmap as pm then
--RFO 				Result := pm.width
--RFO 			end
--RFO 		end

	--|========================================================================
feature {NONE}
	--|========================================================================

	first_widget: detachable EV_WIDGET
		do
			if (not steps.is_empty) and then attached steps.first as s then
				Result := s
			end
		end

	--|------------------------------------------------------------------------

	last_widget: detachable EV_WIDGET
		do
			if (not steps.is_empty) and then attached steps.last as s then
				Result := s
			end
		end

	--|------------------------------------------------------------------------

	i_th_widget (v: INTEGER): detachable EV_WIDGET
		require
			valid_index: v > 0 and v <= number_of_steps
		do
			if (number_of_steps >= v) and then attached steps.i_th (v) as s then
				Result := s
			end
		end

	current_widget: detachable AEL_V2_WIZARD_STEP_BOX
		do
			if (number_of_steps >= step_index) and then
				attached {AEL_V2_WIZARD_STEP_BOX} steps.i_th (step_index) as s then
					Result := s
			end
		end

 --|========================================================================
feature -- Traversal notification agent setting
 --|========================================================================

	set_advance_begin_notify_proc (v: like advance_begin_notify_proc)
		do
			advance_begin_notify_proc := v
		end

	set_advance_end_notify_proc (v: like advance_end_notify_proc)
		do
			advance_end_notify_proc := v
		end

 --|========================================================================
feature {NONE} -- Traversal notification agents
 --|========================================================================

	advance_begin_notify_proc: detachable PROCEDURE
	advance_end_notify_proc: detachable PROCEDURE

 --|========================================================================
feature {NONE}
 --|========================================================================

--RFO 	panel_inset_pixmap: detachable EV_PIXMAP
--RFO 		do
--RFO 		end

--RFO 	splash_panel_bg_color: EV_COLOR
--RFO 		do
--RFO 			Result := colors.navy
--RFO 		end
			
--RFO 	text_panel_bg_color: EV_COLOR
--RFO 		do
--RFO 			Result := colors.white
--RFO 		end
			
	Ks_opening_step_splash_pixmap_filename: detachable STRING
		do
--RFO			Result := "awiz_bg1.png"
		end

	window_initial_width: INTEGER
		do 
			Result := 500
		end

	window_initial_height: INTEGER
		do
			Result := 450
		end

	--|--------------------------------------------------------------
invariant
	valid_step_count: number_of_steps <= steps.count

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| The best way to use this class is to inherit it, and then add 
--| steps as needed to the new class.
--| To use this dialog, create it using 'make_with_title',
--| giving it a title string, or by using 'make' and subsequently
--| setting the title.
--|
--| 
--|----------------------------------------------------------------------

end -- class AEL_V_WIZARD_DIALOG
