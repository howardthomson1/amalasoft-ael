note
	description: "control frame for an AEL_V2_WIZARD_DIALOG"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_V2_WIZARD_DIALOG_CTL_FRAME

inherit
	AEL_V2_DIALOG_CTL_FRAME
		redefine
			parent_dialog,
			create_interface_objects,
			build_interface_components,
			initialize_interface_actions,
			post_initialize,
			set_default_button_focus, process_on_show_actions
		end

create
	make, make_with_parent_dialog

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	create_interface_objects
			-- Create (for subsequent use) the components that make up 
			-- the interface of Current
		do
			Precursor

			create back_button.make_with_text (back_label_string)
			create skip_button.make_with_text (skip_label_string)
			create next_button.make_with_text (next_label_string)
		end

--|========================================================================
feature {NONE} -- Initialization (after default_create)
--|========================================================================

	build_interface_components
			-- Assemble interface components previously created
		local
			tc: EV_CELL
		do
			-- N.B. OK and Cancel are added in Precusror.
			-- As there is no OK button in wizard, do not call Precursor
			-- Problem is that ok button is assumedm and has been 
			-- created before hand
			--| do not call precursor as that would add the ok button

			create tc
			extend (tc)
			extend (back_button)
			disable_item_expand (back_button)

			create tc
			extend (tc)
			extend (skip_button)
			disable_item_expand (skip_button)

			create tc
			extend (tc)
			extend (next_button)
			disable_item_expand (next_button)

			create tc
			extend (tc)
			extend (cancel_button)
			disable_item_expand (cancel_button)

			create tc
			extend (tc)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
			-- Initialize actions, agents and events for interface 
			-- components
		do
			Precursor
			back_button.select_actions.extend (agent on_back_select)
			skip_button.select_actions.extend (agent on_skip_select)
			next_button.select_actions.extend (agent on_next_select)
			cancel_button.select_actions.extend (agent on_cancel_select)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
			Precursor
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_default_button_focus
		do
			if next_button.is_sensitive then
				next_button.set_focus
			end
		end

	--|--------------------------------------------------------------

	disable_skip_button
		do
--			skip_button.hide
			skip_button.disable_sensitive
		end

	enable_skip_button
		do
--			skip_button.show
			skip_button.enable_sensitive
		end

	--|--------------------------------------------------------------

	set_skip_button_sensitive (tf: BOOLEAN)
		do
			if tf then
				skip_button.enable_sensitive
			else
				skip_button.disable_sensitive
			end
		end

--|========================================================================
feature {AEL_V2_WIZARD_DIALOG} -- Components
--|========================================================================

	parent_dialog: detachable AEL_V2_WIZARD_DIALOG
			-- Dialog for which Current is the control frame

	next_button: EV_BUTTON
	skip_button: EV_BUTTON
	back_button: EV_BUTTON

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_back_select
		do
			if attached back_action_proc as p then
				p.call ([])
			end
		end

	on_next_select
		do
			if attached next_action_proc as p then
				p.call ([])
			end
		end

	on_skip_select
		do
			if attached skip_action_proc as p then
				p.call ([])
			end
		end

--|========================================================================
feature
--|========================================================================
	process_on_show_actions
			-- Respond to expose/show events
		do
			--| Likely needed only on first show
			if attached parent_dialog as pd then
--RFO 				pd.set_default_push_button (ok_button)
				pd.set_default_cancel_button (cancel_button)
			end
			update_widget_rendition
			set_default_button_focus
		end

---|========================================================================
feature {AEL_V2_WIZARD_DIALOG} -- Agent setting
--|========================================================================

	set_next_action_proc (v: like next_action_proc)
		do
			next_action_proc := v
		end

	set_skip_action_proc (v: like skip_action_proc)
		do
			skip_action_proc := v
		end

	set_back_action_proc (v: like back_action_proc)
		do
			back_action_proc := v
		end

	--|--------------------------------------------------------------

	next_action_proc: detachable PROCEDURE
	skip_action_proc: detachable PROCEDURE
	back_action_proc: detachable PROCEDURE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Aug-2012
--|     Adapted from earlier ael_v_* version.
--|     Reworked for void-safety.
--|     Re-compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate with either 'make' or 'make_with_parent_dialog'
--| then add to main container in dialog
--|----------------------------------------------------------------------

end -- class AEL_V2_WIZARD_DIALOG_CTL_FRAME
