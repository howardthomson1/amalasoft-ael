note
	description: "{
Type definitions and constants for byte-mapped structures
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_BYTE_MAPPED_CONSTANTS

--|========================================================================
feature -- Type anchors
--|========================================================================

	Kta_block_index: NATURAL_16
			-- Type anchor for block indices
			-- Block indices reference data by number of blocks

	Kta_address: NATURAL_32
			-- Type anchor for byte addresses (aka pointers)

	Kta_index_array: ARRAY [like Kta_block_index]
			-- Type anchor for arrays of block indices
		require
			False
		do
			check False then end
		ensure
			False
		end

	Kta_byte: NATURAL_8
			-- Type anchor for bytes

	--|--------------------------------------------------------------

	to_block_index_type (v: INTEGER): like Kta_block_index
		do
			Result := v.as_natural_16
		end

--|========================================================================
feature -- Sizes
--|========================================================================

	K_size_block_index: INTEGER_32 = 2
			-- Number of bytes in Kta_block_index

	K_size_address: INTEGER_32 = 4
			-- Number of bytes in Kta_address

	--|--------------------------------------------------------------

	empty_index_array_1: like Kta_index_array
			-- Empty 1-based array of like Ktype_index
		do
			create Result.make_empty
		end

	empty_index_array_0: like Kta_index_array
			-- Empty 1-based array of like Ktype_index
		do
			create Result.make_empty
			Result.rebase (0)
		end

	--|--------------------------------------------------------------

	access_type_label (v: INTEGER): STRING
			-- Access type label for type value 'v'
		do
			if v >= 0 and v <= 7 then
				Result := access_type_labels [v]
			else
				Result := "UNDEFINED"
			end
		end

	access_type_labels: ARRAY [STRING]
		once
			Result := <<
				"Indexed",
				"Hashed",
				"RESERVED",
				"RESERVED",
				"Tree",
				"1-Way Linked",
				"2-Way Linked",
				"RESERVED"
				>>
				Result.rebase (0)
		end

	--|--------------------------------------------------------------

	i32_to_pointer (v: INTEGER_32): POINTER
			-- Convert integer 32 'v' to equivalent POINTER value
		do
			Result := default_pointer + v
		end

	n32_to_pointer (v: NATURAL_32): POINTER
			-- Convert natural 32 'v' to equivalent POINTER value
		do
			Result := default_pointer + v.as_integer_32
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original, adpated from AEL_DS_MAPPED_STRUCTURE_TYPES (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| Inherit by class that uses its features or instantiate by default
--|----------------------------------------------------------------------

end -- class AEL_BYTE_MAPPED_CONSTANTS
