note
	description: "{
Abstract notion of a byte-mapped component
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_MAPPED_COMPONENT

inherit
	AEL_BYTE_MAPPABLE
	AEL_SPRT_SIZEABLE

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_address (v: like address; sz: INTEGER_64)
			-- Create Current with starting BYTE address 'v',
			-- and size 'sz' in BYTEs
			-- Address should be initialized to standard fields
		require
			valid_address: v /= default_pointer
			valid_size: is_valid_size (sz)
		do
			is_from := True
			make (v, sz)
		end

	make_for_address (v: like address; sz: INTEGER_64)
			-- Create Current with starting BYTE address 'v',
			-- and size 'sz' in BYTEs
			-- Address should not have been initialized to standard 
			-- fields as that is done creation in this form
		require
			valid_address: v /= default_pointer
			valid_size: is_valid_size (sz)
		do
			make (v, sz)
		end

	--|--------------------------------------------------------------

	make (addr: like address; sz: INTEGER_64)
			-- Create Current with starting BYTE address 'v',
			-- and size 'sz' in BYTEs
		require
			valid_address: addr /= default_pointer
			valid_size: (not is_from) implies is_valid_size (sz)
		do
			address := addr
			size_in_bytes := sz
			initialize
		ensure
			addressed: address = addr
			sized: size_in_bytes = sz
		end

	--|--------------------------------------------------------------

	frozen initialize
			-- Initialize Current (structure, sizes, offsets, etc), in a 
			-- manner consistent with 'is_from'
		require
			has_address: address /= Default_pointer
			sized: is_valid_size (size_in_bytes)
		do
			initialize_common_pre
			if is_from then
				initialize_from
			else
				initialize_for
			end
			initialize_common_post
		end

	--|--------------------------------------------------------------

	initialize_common_pre
			-- Initialize Current for both 'for' and 'from' modes,
			-- _Before_ special initializations
		require
			has_address: address /= Default_pointer
			sized: is_valid_size (size_in_bytes)
			not_special: not special_was_initialized
		do
		end

	--|--------------------------------------------------------------

	initialize_common_post
			-- Initialize Current for both 'for' and 'from' modes,
			-- _After_ special initializations
		require
			has_address: address /= Default_pointer
			sized: is_valid_size (size_in_bytes)
			special_initialized: special_was_initialized
		do
		end

	--|--------------------------------------------------------------

	initialize_for
			-- Initialize Current when 'for' an address
		require
			not_from: not is_from
			not_special: not special_was_initialized
		do
			special_was_initialized := True
		ensure
			special_initialized: special_was_initialized
		end

	initialize_from
			-- Initialize Current when 'from' an address
		require
			is_from: is_from
			not_special: not special_was_initialized
		do
			-- Read block type, size and count from address
			get_initial_values
			special_was_initialized := True
		ensure
			special_initialized: special_was_initialized
		end

	--|--------------------------------------------------------------

	get_initial_values
			-- Read and set from address the critical values
			-- for Current
		require
			is_from: is_from
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	address: POINTER
			-- Starting BYTE address of Current

	is_from: BOOLEAN
			-- Was Current created _from_ and existing slab?

	size_in_bytes: INTEGER_64
			-- Defined size of Current, in BYTEs

	special_was_initialized: BOOLEAN
			-- Was specialized initialization completed yet? (i.e. for/from)

--|========================================================================
feature -- Status Setting
--|========================================================================

	set_defined_size (v: INTEGER_64)
			-- Set defined size (in BYTEs) to 'v'
		require
			is_valid_size: is_valid_size (v)
			initialized: special_was_initialized
		do
			set_size_in_bytes (v)
		ensure
			is_set: size_in_bytes = v
		end

	--|--------------------------------------------------------------

	set_size_in_bytes (v: INTEGER_64)
			-- Set size of Current (in BYTEs) to 'v'
		do
			size_in_bytes := v
		end

--|========================================================================
feature {NONE} -- Shared services
--|========================================================================

	anr: AEL_NUMERIC_INTEGER_ROUTINES
		once
			create Result
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original, derived from AEL_DS_MAPPED_COMPONENT
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--|----------------------------------------------------------------------

end -- class AEL_MAPPED_COMPONENT
