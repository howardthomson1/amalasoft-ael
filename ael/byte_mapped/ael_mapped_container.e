note
	description: "{
Container-like byte-mapped component
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_MAPPED_CONTAINER [G->AEL_SPRT_SIZEABLE]

inherit
	AEL_MAPPED_COMPONENT
	BOUNDED [G]

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_capacity (ic: INTEGER; addr: POINTER; ff: BOOLEAN)
			-- Create Current with capacity (max num items) 'ic',
			-- based at address 'addr'.
			-- If 'ff' then, then create _from_ 'addr', else _for_ 'addr'
		do
			capacity := ic
			if ff then
				make_from_address (addr, allocation_size)
			else
				make_for_address (addr, allocation_size)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	count: INTEGER
			-- Number of items presently in Current

	capacity: INTEGER
			-- Maxiumum number of items that Current can store

	header_size: INTEGER
			-- Number of BYTEs required by the header
		deferred
		ensure
			positive: Result > 0
		end

	item_size: INTEGER
			-- Size of items contained within Current
		deferred
			--| Can be zero for a header-only container
		ensure
			not_negative: Result >= 0
		end

	allocation_size: INTEGER_64
			-- Number of BYTEs that must be allocated for Current
		do
			Result := header_size + (item_size * capacity)
		ensure
			valid: Result >= header_size
		end

--|========================================================================
feature -- Status Setting
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original, derived from AEL_DS_MAPPED_COMPONENT
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--|----------------------------------------------------------------------

end -- class AEL_MAPPED_CONTAINER
