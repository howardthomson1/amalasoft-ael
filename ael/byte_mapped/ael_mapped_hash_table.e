note
	description: "{
A byte-mapped hash table in which items are also byte-mapped
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_MAPPED_HASH_TABLE [G->AEL_MAPPED_COMPONENT, K->STRING_GENERAL]

inherit
	AEL_MAPPED_CONTAINER [G]
		rename
			has as has_item
		end

create
	make_with_capacity

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================

--RFO TODO
	header_size: INTEGER = 512
			-- Number of BYTEs required by the table header

	item_size: INTEGER
			-- Size of items contained within Current
		do
--RFO TODO
			Result := 512
		end

	resizable: BOOLEAN
			-- May `capacity' be changed?
			-- NO

	found: BOOLEAN
			-- Did last operation find the item sought?

	not_found: BOOLEAN
			-- Did last operation fail to find the item sought?

	found_item: detachable G
			-- Item found during last search operation, if any

--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

	has_item (v: G): BOOLEAN
			-- Does structure include `v'?
			-- (Reference or object equality,
			-- based on `object_comparison'.)
		do
--RFO TODO
		end

	has (k: K): BOOLEAN
			-- Does Current contain and item by key 'k'?
		do
--RFO TODO
		end

	--|--------------------------------------------------------------

	item (k: K): detachable G
			-- Item in Current stored by key 'k', if any
		do
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	force (v: G; k: K)
			-- Add 'v' to Current, replacing any existing item by the
			-- same key 'k'
			--
			-- If there was an item for that key, set `found'
			-- and set `found_item' to that item.
			-- If there was none, set `not_found' and set
			-- `found_item' to Void.
		require
			valid_key: is_valid_key (k)
		local
			--addr: like found_item_address
			--ba: like next_available_blocks
			bc: INTEGER
			idx, a: like Kta_block_index
			oldi: like Kta_item
			olde: like Kta_element
		do
			--| Determine the size of the item, in blocks
			bc := content_blocks_required (v)

			internal_search (k)
			if not_found then
--RFO 				if soon_full then
--RFO 					add_space
--RFO 					internal_search (k)
--RFO 				end
--RFO 				if deleted_items_count /= 0 then
--RFO 					-- Allocate from free list
--RFO 				else
--RFO 				end
			else
				-- Replaces existing item by that key
			end
		ensure
			inserted: item (k) = v
			found_or_not: found or not_found
			found_if_had: (old has (k)) implies found
			counted_if_new: (not found) implies count = old count + 1
			not_counted_if_exits: found implies count = old count
			remembered_if_found: found implies found_item /= Void
			forgotten_if_not_found: (not found) implies found_item = Void
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Support
--|========================================================================

	content_blocks_required (v: G): INTEGER
			-- Number of blocks required to hold new content
		require
			exists: v /= Void
		do
			Result := anr.even_512_multiple (v.size_in_bytes.as_integer_32)
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	linear_representation: ARRAYED_LIST [G]
			-- Representation as a linear structure
		do
--RFO TODO
			create Result.make (count)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_key (v: K): BOOLEAN
			-- Is 'v' a valid key for Current?  Does not imply presence
		do
			Result := v /= Void and then not v.is_empty
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	internal_search (k: K)
			-- Search for an item by the key 'k'
			-- If found, preserve the item's full location information
			-- in 'found_item_location'
			-- If not found, set 'found_item_location' to Void
		local
--RFO 			addr: like found_item_address
			idx, bid: like Kta_block_index
			kl: like Kta_byte
			ks: STRING_8
--RFO 			fil: like found_item_location
		do
			--RFO create fil.make
			--RFO found_item_location := Void
			--RFO --found_item_address := 0
			--RFO found_item_string := Void
			--RFO if count /= 0 then
			--RFO 	idx := key_to_index (k)
			--RFO 	--| Record hashed index
			--RFO 	fil.extend (idx)
			--RFO 	--| Get block address field from master index map(s)
			--RFO 	bid := mapped_index_to_block_id (idx)
			--RFO 	if bid = 0 then
			--RFO 		--| No blocks hash to hashed index (or to key)
			--RFO 	else
			--RFO 		--| Record number of extensions
			--RFO 		fil.extend (0)
			--RFO 		--| Record block ID gathered from imap
			--RFO 		fil.extend (bid)
			--RFO 		--| A block already exists by hashed index,
			--RFO 		--| now find one that matches 'k'
			--RFO 		addr := block_to_byte (bid)
			--RFO 		kl := byte_at_offset (addr + const.K_off_sb_key_length)
			--RFO 		if kl = k.count then
			--RFO 			ks := string_n_at_offset (addr + const.K_off_sb_key, kl)
			--RFO 			if k.same_string (ks) then
			--RFO 				-- Is a match
			--RFO 				--found_item_address := addr
			--RFO 				found_item_location := fil
			--RFO 			else
			--RFO 				-- Keep looking
			--RFO 				-- remember to add each point in the traversal
			--RFO 				-- to found_item_location
			--RFO 			end
			--RFO 		end
			--RFO 	end
			--RFO end
		end

	--|--------------------------------------------------------------

	--RFO next_available_blocks (bc: INTEGER): like Kta_index_array
	--RFO 		-- Next 'n' available block addresses,
	--RFO 		-- either from a free list or from heap
	--RFO 		-- Array is 1-based !!!!!!!!!
	--RFO 	local
	--RFO 		--ca: like Kta_block_index
	--RFO 	do
	--RFO 		Result := const.empty_index_array_1
	--RFO 		--| Try for contiguous blocks first

	--RFO 		if deleted_block_count < bc then
	--RFO 			if count = 0 then
	--RFO 				Result := << defined_content_start_index >>
	--RFO 			else
	--RFO 				-- Allocate from next available content block
	--RFO 			end
	--RFO 		else
	--RFO 			-- Allocate from free lists
	--RFO 			Result := next_available_deleted_blocks (bc)
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	--RFO next_available_deleted_blocks (bc: INTEGER): like Kta_index_array
	--RFO 		-- Next 'n' available block addresses from free lists
	--RFO 	require
	--RFO 		has_enough: bc <= deleted_block_count
	--RFO 	do
	--RFO 		Result := const.empty_index_array_1
	--RFO 		if bc >= const.K_size_min_freelist_r1_range then
	--RFO 			-- Look for a suitable range
	--RFO 			if bc >= const.K_size_min_freelist_r2_range then
	--RFO 			else
	--RFO 			end
	--RFO 		end
	--RFO 		if Result = const.empty_index_array_1 then
	--RFO 			-- Range was not allocated
	--RFO 			-- Allocate individual blocks
	--RFO 		end
	--RFO 	end

--|========================================================================
feature {NONE} -- Type anchors
--|========================================================================

	Kta_element: ANY
		require False
		do
			check False then end
		ensure False
		end

	Kta_item: G
		require False
		do
			check False then end
		ensure False
		end

	K_element_size: INTEGER = 128
			-- Number of bytes per element
			-- Index tables contain element offsets, each of which is 
			-- the number of K_element_size chunks from the start of 
			-- content at which the element resides
			-- Elements are not items.  They are the pieces of the table 
			-- that reference the actual stored items.  All elements are 
			-- the same size.  Items are variable sized.

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-Oct-2013
--|     Created original module (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_MAPPED_HASH_TABLE
