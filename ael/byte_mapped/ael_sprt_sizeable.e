note
	description: "{
Abstract notion of having a comparable size
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_SPRT_SIZEABLE

--|========================================================================
feature -- Status
--|========================================================================

	size_in_bytes: INTEGER_64
			-- Number of bytes that Current occupies
		deferred
		end

	size_in_bytes_32: INTEGER_64
			-- Number of bytes that Current occupies, as integer_32, if 
			-- possible
		do
			Result := size_in_bytes.as_integer_32
		end

	maximum_size: INTEGER_64
			-- Largest size that Current can be
		do
			Result := Result.max_value
		end

	minimum_size: INTEGER_64
			-- Smallest size that Current can be
		do
			Result := 0
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_size_in_bytes (v: INTEGER_64)
			-- Set to 'v' the number of bytes that Current occupies
		require
			valid_size: is_valid_size (v)
		deferred
		ensure
			is_set: size_in_bytes = v
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	same_size (other: AEL_SPRT_SIZEABLE): BOOLEAN
			-- Does 'other' have the same size as Current?
		require
			exists: other /= Void
		do
			Result := size_in_bytes = other.size_in_bytes
		ensure
			reflexive: Result implies other.same_size (Current)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_size (v: INTEGER_64): BOOLEAN
			-- Is 'v' a valid size for Current?
		do
			Result := v >= minimum_size and v <= maximum_size
		end

	--|--------------------------------------------------------------
invariant
	not_negative: size_in_bytes >= 0

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Created original (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not instantiable.  Inherit in child that 
--| requires characteristics and features of this class.
--|----------------------------------------------------------------------

end -- class AEL_SPRT_SIZEABLE
