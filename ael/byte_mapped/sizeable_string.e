class SIZEABLE_STRING

inherit
	AEL_SPRT_SIZEABLE

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (sz: INTEGER_64)
			-- Create Current with initial size 'sz'
		do
			create string.make (sz.as_integer_32)
		end

--|========================================================================
feature -- Status
--|========================================================================

	size_in_bytes: INTEGER_64
			-- Number of bytes that Current occupies
		do
			Result := string.count.as_integer_64
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_size_in_bytes (v: INTEGER_64)
			-- Set to 'v' the number of bytes that Current occupies
		do
			string.resize (v.as_integer_32)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	string: STRING_8

end -- class SIZEABLE_STRING
