note
	description : "bytemap_test application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			mc: AEL_MAPPED_CONTAINER [SIZEABLE_STRING]
			ht: AEL_MAPPED_HASH_TABLE [MAPPED_DUMMY, STRING_GENERAL]
		do
		end

end
