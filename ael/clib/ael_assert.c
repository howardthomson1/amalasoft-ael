/*--------------------------------------------------------------------------
-- Copyright (c) 1995, All rights reserved by
--
-- Amalasoft
-- 126 New Estate Road
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
----------------------------------------------------------------------------
--
--	ael_assert.c - C assertion handling functions
--
-- Clients of this module must include ael_assert.h to define their assert
-- macros and such. The clients must be compiled with the assertion flags
-- on, else the assertions are no-ops (see ael_assert.h)
--
-- Note that the code in this module is _not_ conditional on the assert
-- macros being defined. This is because this module resides in a linkable
-- library and as such has no idea what flags its clients might be using.
--
----------------------------------------------------------------------------
-- Author Identification
--
--	RFO	Roger F. Osmond, Amalasoft
--
----------------------------------------------------------------------------
-- Audit Trail
--
-- 000 RFO 02-Apr-95	Original module adapted from older versions
--------------------------------------------------------------------------*/

static char *
 sccsid = "@(#) Copyright (c) 1995 Amalasoft: ael_assert.c, Version 000";

/*------------------------------------------------------------------------*/

#include <stdio.h>

#include "ael_gnl_defs.h"
#include "ael_types.h"
#include "ael_assert.h"

static char *
box_line = "============================================================";

static char *
internal_error_str = "INTERNAL ERROR !!!";

static int
do_not_exit = K_false;

/*---------------------------------------------
-- These labels are indexed using constants
-- defined in ael_assert.h
*/

static char *
asrt_type_labels[] = {
    "PRECONDITION VIOLATION",
    "POSTCONDITION VIOLATION",
    "ASSERTION VIOLATION",
    "INVARIANT VIOLATION"
};

/*--------------------------------------------------------------------------
--
--	exit_handler
--
--	Do what needs to be done to exit following an assertion violation
--
--	It would be nice to generate a stack trace at this point
--	but at least the abort() call creates a core dump
*/

#ifdef _NO_PROTO
static void
exit_handler()
#else
static void
exit_handler( void )
#endif
{
    if( ! do_not_exit ) {
	abort( 1 );
    }
    return;
}

/*--------------------------------------------------------------------------
--
--	ael_assert_set_exit
--
--	Set the value of the boolean variable do_not_exit
--	to be the inverse of the value of the boolean argument tf
*/

void
ael_assert_set_exit( tf )
int tf;
{
    do_not_exit = ! tf;
}

/*--------------------------------------------------------------------------
--
--	_as_assert
--
--	Print out assertion violation message and call the proper
--	follow up function
*/

#ifdef _NO_PROTO

void
_as_assert( asrt_type, lbl, filename, lineno, rescue )
int asrt_type;
char *lbl;
char *filename;
int lineno;
Fptr_void rescue;
#else

void
_as_assert( int asrt_type, char *lbl,
	   char *filename, int lineno, Fptr_void rescue )

#endif
{
    fprintf( stderr, "%s\n%s\n%s\n  LABEL: %s\n  FILE:  %s\n  LINE:  %d\n%s\n",
	    box_line,
	    internal_error_str,
	    asrt_type_labels[asrt_type],
	    lbl,
	    filename,
	    lineno,
	    box_line
	    );
    if( rescue ) {
	(*rescue)();
    } else {
	exit_handler();
    }
}

#if 0
#include <iostream>
#include <iomanip>
#include "context.hxx"

using std::cout;
using std::endl;
using std::hex;

context::context()
{
  __asm__ ("movl 4(%%ebp), %0\n\t"
    "movl (%%ebp), %1\n\t"
    : "=r" (_pc), "=r" (_fp)
    : /* no input */
    : "%esp" );

    _pc-=5;
}

context::context(unsigned long f, unsigned long p)
 : _fp(f), _pc(p)
 {
 }

unsigned long context::pc()
{
  return _pc;
}

unsigned long context::fp()
{
  return _fp;
}

// Pre-increment operator -- one level deeper into the stack
context& context::operator++ ()
{
  if(_fp != 0) {
    _pc = *reinterpret_cast<unsigned int*>(_fp + 4);
    _pc -= 5;
    _fp = *reinterpret_cast<unsigned int*>(_fp);
  }
  return *this;
}

// Are we in the last frame?
context::operator bool() const
{
  return _fp != 0;
}

void print_stack()
{
  context con;
  ++con;

  for(; con; ++con) {
    cout << "%eip: " << hex << con.pc() << " ";
    cout << "%ebp: " << hex << con.fp() << endl;
  }
}

#endif
