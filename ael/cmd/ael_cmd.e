note
	description: "{
Abstract concept of a command
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012-2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."


deferred class AEL_CMD

inherit
	AEL_SPRT_FALLIBLE
		redefine
			default_create, set_error, set_error_message, clear_error,
			has_error, error_message, error_code
		end
	APPLICATION_ENV
		undefine
			default_create,
			set_error, set_error_message, has_error
		redefine
			last_error_message, last_error_code, clear_errors
		end

--|========================================================================
feature {NONE} -- Creation and Initialization
--|========================================================================

	make
			-- Create current from context 'ctx'
		do
			default_create
		end

	--|--------------------------------------------------------------

	make_with_args (v: ARRAY [STRING])
		do
			make
			arg_array := v
		end

	--|--------------------------------------------------------------

	default_create
		do
			create arg_array.make_empty
			Precursor
		end

--|========================================================================
feature -- Request processing
--|========================================================================

	frozen execute
		require
			has_no_error: not has_error
		do
			cmd_result := Void
			prepare
			if is_prepared and not has_error then
				pre_exec
				if not has_error then
					create affected_components.make
					core_exec
				end
				if not has_error then
					has_executed := True
					post_exec
				end
				build_result
				set_response
			elseif not has_error then
				set_error_message ("Unable to execute")
			end
		ensure
			has_executed: not has_error implies has_executed
		end

--|========================================================================
feature {NONE} -- Preparation
--|========================================================================

	prepare
			-- Take steps to ensure successful execution after initial 
			-- analysis and prior to actual execution
		require
			no_prior_errors: not has_error
		do
			is_prepared := True
		ensure
			prepared: not has_error implies is_prepared
		end

	--|--------------------------------------------------------------

	notify_start
			-- Notify anyone who cares that execution has begun
		do
		end

	--|--------------------------------------------------------------

	set_response
			-- Give the context the response
		do
		end

	--|--------------------------------------------------------------

	pre_exec
			-- Perform the core logic of the command, after initial analysis
		require
			executable: is_prepared and not has_error
		do
		end

	--|--------------------------------------------------------------

	core_exec
			-- Perform the core logic of the command
		require
			executable: is_prepared and not has_error
		deferred
		end

	--|--------------------------------------------------------------

	post_exec
		require
			has_executed: has_executed
		do
			if has_affected_components then
				if not has_error then
					save_affected_components
				end
			end
		end

	--|--------------------------------------------------------------

	add_affected_component (v: DB_COMPONENT)
			-- Add to the affected components list 'v'
		require
			exists: v /= Void
			is_new: not affected_components.has (v)
			executable: is_prepared and not has_error
		do
			affected_components.extend (v)
		end

	--|--------------------------------------------------------------

	build_result
		do
			if has_error then
				cmd_result := default_cmd_failure_output
			else
				cmd_result := default_cmd_success_output
			end
		ensure
			has_result: not has_error implies cmd_result /= Void
		end

	--|--------------------------------------------------------------

	notify_complete
			-- Notify anyone who cares that execution has completed
		do
		end

--|========================================================================
feature {NONE} -- Commit support
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================

	arg_array: ARRAY [STRING]

	--|--------------------------------------------------------------

	is_prepared: BOOLEAN
			-- Has Current been prepared for execution?

	has_executed: BOOLEAN
			-- Has Current executed, or at least attempted to do so?

	--|--------------------------------------------------------------

	has_affected_components: BOOLEAN
			-- Does Current affect any components?
			-- Creating, updating and deleting will have a least one
		do
			Result := not affected_components.is_empty
		end

	affected_components: LINKED_LIST [DB_COMPONENT]
			-- Components affected (created, deleted or updated) by 
			-- execution of this command

--|========================================================================
feature -- Result/Response body
--|========================================================================

	cmd_result: detachable STRING
			-- Result of executing Current

--|========================================================================
feature -- Error recording
--|========================================================================

	error_message: STRING
		do
			Result := last_error_message
		end

	error_code: INTEGER
		do
			Result := last_error_code
		end

	--|--------------------------------------------------------------

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2019
--|     Adapted original module from legacy a_cmd
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_CMD
