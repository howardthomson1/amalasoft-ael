class AEL_SPRT_CRYPTO_ROUTINES
-- Cryptographic routines

inherit
	AEL_SPRT_CRYPTO_ROUTINES_IMP

--|========================================================================
feature -- SHA1 hash generation
--|========================================================================

	sha1sum_as_hex (v: STRING): STRING
			-- Hex string form of SHA1 sum of the given byte stream
			-- Alpha chars in result are lower case
		local
			i, lim: INTEGER
			ts: like sha1sum
		do
			ts := sha1sum (v)
			create Result.make (K_sha1_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_lower)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = K_sha1_output_len * 2
		end

	--|--------------------------------------------------------------

	sha1sum_as_uphex (v: STRING): STRING
			-- Hex string form of SHA1 sum of the given byte stream
			-- Alpha chars in result are upper case
		local
			i, lim: INTEGER
			ts: like sha1sum
		do
			ts := sha1sum (v)
			create Result.make (K_sha1_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_upper)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = 32
		end

	--|--------------------------------------------------------------

	sha1sum_as_base32 (v: STRING): STRING
			-- base32 string form of SHA1 sum of the given byte stream
			-- Output encodes each byte of the SHA1 sum separately
		local
			i, lim: INTEGER
			ts: like sha1sum
		do
			ts := sha1sum (v)
			create Result.make (K_sha1_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (b32.integer_to_base32 (ts.item (i)))
				i := i + 1
			end
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_sha1_output_len * 2
		end

	--|--------------------------------------------------------------

	sha1sum_as_base64 (v: STRING): STRING
			-- base64 string form of SHA1 sum of the given byte stream
		local
			tsh: like sha1sum
			ts: STRING
			tc: CELL [INTEGER]
		do
			tsh := sha1sum (v)
			ts := tsh.to_string
			create tc.put (ts.count)
			Result := b64.encoded_string (ts, tc)
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_sha1_output_len * 2
		end

--|========================================================================
feature -- MD5 hash generation
--|========================================================================

	md5sum_as_hex (v: STRING): STRING
			-- Hex string form of MD5 sum of the given byte stream
			-- Alpha chars in result are lower case
		local
			i, lim: INTEGER
			ts: like md5sum
		do
			ts := md5sum (v)
			create Result.make (K_md5_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_lower)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = K_md5_output_len * 2
		end

	--|--------------------------------------------------------------

	md5sum_as_uphex (v: STRING): STRING
			-- Hex string form of MD5 sum of the given byte stream
			-- Alpha chars in result are upper case
		local
			i, lim: INTEGER
			ts: like md5sum
		do
			ts := md5sum (v)
			create Result.make (K_md5_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_upper)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = K_md5_output_len * 2
		end

	--|--------------------------------------------------------------

	md5sum_as_base32 (v: STRING): STRING
			-- base32 string form of MD5 sum of the given byte stream
			-- Output encodes each byte of the md5 sum separately
		local
			i, lim: INTEGER
			ts: like md5sum
		do
			ts := md5sum (v)
			create Result.make (K_md5_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (b32.integer_to_base32 (ts.item (i)))
				i := i + 1
			end
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_md5_output_len * 2
		end

	--|--------------------------------------------------------------

	md5sum_as_base64 (v: STRING): STRING
			-- base64 string form of MD5 sum of the given byte stream
		local
			tsm: like md5sum
			ts: STRING
			tc: CELL [INTEGER]
		do
			tsm := md5sum (v)
			ts := tsm.to_string
			create tc.put (ts.count)
			Result := b64.encoded_string (ts, tc)
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_md5_output_len * 2
		end

--|========================================================================
feature -- HMAC routines
--|========================================================================

	hmacsha1_as_hex (v, skey: STRING): STRING
			-- Hex string form of HMCA_SHA digest of the given byte stream
			-- Alpha chars in result are lower case
		local
			i, lim: INTEGER
			ts: like hmacsha1_digest
		do
			ts := hmacsha1_digest (v, skey)
			create Result.make (K_hmac_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_lower)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = K_hmac_output_len * 2
		end

	--|--------------------------------------------------------------

	hmacsha1_as_uphex (v, skey: STRING): STRING
			-- Hex string form of HMAC_SHA1 digest of the given byte stream
			-- Alpha chars in result are upper case
		local
			i, lim: INTEGER
			ts: like hmacsha1_digest
		do
			ts := hmacsha1_digest (v, skey)
			create Result.make (K_hmac_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (ts.item (i).to_hex_string.as_upper)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			full_length: Result.count = K_hmac_output_len * 2
		end

	--|--------------------------------------------------------------

	hmacsha1_as_base32 (v, skey: STRING): STRING
			-- base32 string form of HMAC_SHA1 digest of the given byte stream
			-- Output encodes each byte of the SHA1 sum separately
		local
			i, lim: INTEGER
			ts: like hmacsha1_digest
		do
			ts := hmacsha1_digest (v, skey)
			create Result.make (K_hmac_output_len * 2)
			lim := ts.upper
			from i := ts.lower
			until i > lim
			loop
				Result.append (b32.integer_to_base32 (ts.item (i)))
				i := i + 1
			end
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_hmac_output_len * 2
		end

	--|--------------------------------------------------------------

	hmacsha1_as_base64 (v, skey: STRING): STRING
			-- base64 string form of HMAC_SHA1 digest of the given byte stream
		local
			td: like hmacsha1_digest
			ts: STRING
			tc: CELL [INTEGER]
		do
			td := hmacsha1_digest (v, skey)
			ts := td.to_string
			create tc.put (ts.count)
			Result := b64.encoded_string (ts, tc)
		ensure
			exists: Result /= Void
			shorter_than_hex: Result.count <= K_hmac_output_len * 2
		end

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	b64: AEL_SPRT_BASE64
		once
			create Result
		end

	b32: AEL_SPRT_BASE32
		once
			create Result
		end

end -- class AEL_SPRT_CRYPTO_ROUTINES
