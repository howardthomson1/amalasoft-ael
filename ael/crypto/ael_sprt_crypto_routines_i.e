deferred class AEL_SPRT_CRYPTO_ROUTINES_I
-- Abstract implementation class for (separate) platform-specific
-- implementations of crypto routines

inherit
	STRING_HANDLER

--|========================================================================
feature -- SHA1 hash generation
--|========================================================================

	sha1sum (v: STRING): AEL_DS_BYTE_STREAM
			-- SHA1 sum of the given byte stream, as octet stream
		deferred
		ensure
			exists: Result /= Void
			full_length: Result.count = K_sha1_output_len
		end

--|========================================================================
feature -- MD5 hash generation
--|========================================================================

	md5sum (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream
		deferred
		ensure
			exists: Result /= Void
			full_length: Result.count = K_md5_output_len
		end

	--|--------------------------------------------------------------

	md5sum_evp (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level EVP functions
		deferred
		ensure
			exists: Result /= Void
			full_length: Result.count = K_md5_output_len
		end

	--|--------------------------------------------------------------

	md5sum_crypto (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level MD5 crypto functions
		deferred
		ensure
			exists: Result /= Void
			full_length: Result.count = K_md5_output_len
		end

--|========================================================================
feature -- HMAC routines
--|========================================================================

	hmacsha1_digest (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		require
			input_exists: v /= Void
			secret_exists: skey /= Void
		deferred
		ensure
			exists: Result /= Void
			not_over_length: Result.count <= K_hmac_output_len
		end

	hmacsha1_digest_alt (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		require
			input_exists: v /= Void
			secret_exists: skey /= Void
		deferred
		ensure
			exists: Result /= Void
			not_over_length: Result.count <= K_hmac_output_len
		end

--|========================================================================
feature -- Digest lengths
--|========================================================================

	K_md5_output_len: INTEGER
		deferred
		end

	K_sha1_output_len: INTEGER
		deferred
		end

	K_hmac_output_len: INTEGER
		deferred
		end

end -- class AEL_SPRT_CRYPTO_ROUTINES_I
