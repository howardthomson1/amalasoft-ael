note
	description: "{
Encapsulation of the HMAC_CTX struct from the openssl library
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_HMAC_CTX

inherit
	AEL_STRUCT
		rename
			make as struct_make
		redefine
			dispose
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
			-- Create Current by calling C constructor and capturing, 
			-- for subsequent disposal, the address provide from the 
			-- constructor
		local
			p: POINTER
		do
			p := c_new_hmax_ctx
			make_disposable_from_c (p)
		end

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	dispose
		do
			c_free_hmac_ctx (address)
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	c_new_hmax_ctx: POINTER
			-- Address of newly allocated structure in memory, from C
		external
			"C inline use <openssl/hmac.h>"
		alias
		"{
			HMAC_CTX *h = HMAC_CTX_new();
			return (h);
		}"
		end

	c_free_hmac_ctx (p: POINTER)
			-- Free memory previously allocated for _This_ HMAC_CTX 
			-- struct
		require
			same_address: p = address
		external
			"C inline use <openssl/hmac.h>"
		alias
		"{
			HMAC_CTX_free($p);
		}"
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Sep-2014
--|     Created original module (Eiffel 14.05)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_HMAC_CTX
