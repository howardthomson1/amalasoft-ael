class AEL_SPRT_CRYPTO_ROUTINES_IMP
-- Linux/UNIX implementation of crypto routines

inherit
	AEL_SPRT_CRYPTO_ROUTINES_I

--|========================================================================
feature -- SHA1 hash generation
--|========================================================================

	sha1sum (v: STRING): AEL_DS_BYTE_STREAM
			-- SHA1 sum of the given byte stream, as octet stream
		local
			cstr, bstr: C_STRING
		do
			create cstr.make (v)
			create bstr.make_empty (K_sha1_output_len)
			c_sha1 (cstr.item, v.count, bstr.item)
			create Result.make_from_string (bstr.string)
		end

--|========================================================================
feature -- MD5 hash generation
--|========================================================================

	md5sum (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream
		local
			cstr, bstr: C_STRING
		do
			create cstr.make (v)
			create bstr.make_empty (K_md5_output_len)
			c_md5 (cstr.item, v.count, bstr.item)
			create Result.make_from_string (bstr.string)
		end

	--|--------------------------------------------------------------

	md5sum_evp (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level EVP functions
		local
			md: POINTER -- EVP_MD *
			mdctx: MANAGED_POINTER -- EVP_MD_CTX
			len: INTEGER
			cstr, bstr: C_STRING
		do
			create cstr.make (v)
			create bstr.make_empty (K_md5_output_len)

			c_init_openssl
			md := c_new_md5_context
			create mdctx.make (c_evp_md_ctx_struct_size)
			c_evp_md_ctx_init (mdctx.item)
			c_evp_digest_init_ex (mdctx.item, md, Default_pointer)
			c_evp_digest_update (mdctx.item, cstr.item, v.count)
			len := c_evp_digest_final_ex (mdctx.item, bstr.item)
			check
				digest_length: len = K_md5_output_len
			end
			c_evp_md_ctx_cleanup (mdctx.item)
			create Result.make_from_string (bstr.string)
		end

	--|--------------------------------------------------------------

	md5sum_crypto (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level MD5 crypto functions
		local
			hstate: MANAGED_POINTER
			cstr, bstr: C_STRING
		do
			create cstr.make (v)
			create bstr.make_empty (K_md5_output_len)

			create hstate.make (c_md5_state_struct_size)
			c_md5_init (hstate.item)
			c_md5_update(hstate.item, cstr.item, v.count)
			c_md5_final (hstate.item, bstr.item)
			create Result.make_from_string (bstr.string)
		end

--|========================================================================
feature -- HMAC routines
--|========================================================================

-- HMAC_CTX *h = HMAC_CTX_new();
-- HMAC_Init_ex(h, key, keylen, EVP_sha256(), NULL);
-- ...
-- HMAC_CTX_free(h);

	hmacsha1_digest (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		local
			md: POINTER -- EVP_MD *
			cstr, kstr: C_STRING
			ca: MANAGED_POINTER -- C array
			lp: MANAGED_POINTER -- int *
		do
			create ca.make (K_hmac_output_len)
			create cstr.make (v)
			create kstr.make (skey)
			create lp.make (4) -- for an integer_32

			c_init_openssl
			md := c_new_sha1_context
			c_hmacsha1 (
				md, kstr.item, skey.count, cstr.item, v.count, ca.item, lp.item)
			create Result.make_from_managed_pointer (ca, lp.read_integer_32 (0))
		end

	--|--------------------------------------------------------------

	hmacsha1_digest_alt (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		local
			md: POINTER -- EVP_MD *
			vp, kp, bp: C_STRING
			len: INTEGER_32
			lp: MANAGED_POINTER -- int *
		do
			create lp.make (4) -- for an integer_32
			-- Create a buffer into which to write the digest
			create bp.make_empty (K_hmac_output_len)
			-- vp will present the contents of 'v' as a C string
			create vp.make (v)
			-- kp will present the contents of 'skey' as a C string
			create kp.make (skey)

			c_init_openssl
			md := c_new_sha1_context
			c_hmacsha1 (
				md, kp.item, skey.count, vp.item, v.count, bp.item, lp.item)
			create Result.make_from_string (bp.string)
			len := lp.read_integer_32 (0)
			if len /= K_hmac_output_len then
				Result := Result.truncated_stream (len)
			end
		end

--|========================================================================
feature -- SHA-1 Externals
--|========================================================================

	c_sha1 (sp: POINTER; n: INTEGER; op: POINTER)
			-- Fill in buffer 'op' with unsigned bytes from the SHA1 sum
			-- of the the stream 'sp', to a stream length of 'n'
		external
			"C inline use <openssl/sha.h>"
		alias
			"[
				SHA1($sp, $n, $op);
			]"
		end

--|========================================================================
feature {NONE} -- MD5 Externals
--|========================================================================

	c_md5 (sp: POINTER; n: INTEGER; op: POINTER)
			-- Fill in buffer 'op' with unsigned bytes from the MD5 sum
			-- of the the stream 'sp', to a stream length of 'n'
		external
			"C inline use <openssl/md5.h>"
		alias
			"[
				MD5($sp, $n, $op);
			]"
		end

	c_md5_init (st: POINTER)
			-- Initialize the MD5 hash state 'sp'
		external
			"C inline use <openssl/md5.h>"
		alias
			"[
				MD5_Init($st);
			]"
		end

	c_md5_update (st, src: POINTER; len: INTEGER)
			-- Add the given stream 'src', for a length of 'len' to
			-- the given hash state 'st'
		external
			"C inline use <openssl/md5.h>"
		alias
			"[
				MD5_Update($st, $src, $len);
			]"
		end

	c_md5_final (st, buf: POINTER)
			-- Complete the hash with state 'st', writing completed
			-- hash into octet stream 'buf'
		external
			"C inline use <openssl/md5.h>"
		alias
			"[
				MD5_Final($buf, $st);
			]"
		end

	c_md5_state_struct_size: INTEGER
			-- Size of struct MD5state_st (hash state structure)
		external
			"C inline use <openssl/md5.h>"
		alias
			"[
				return sizeof(struct MD5state_st);
			]"
		end

--|========================================================================
feature {NONE} -- HMAC externals
--|========================================================================

	c_hmacsha1 (md, key: POINTER; kl: INTEGER; sp: POINTER; n: INTEGER; op, oplp: POINTER)
			-- Fill in buffer 'op' with up to 'ol' unsigned bytes from
			-- the HMAC_SHA1 digest of the the stream 'sp', to a stream
			-- length of 'n', using SHA1 context 'md' and secret 'key'
		external
			"C inline use <openssl/hmac.h>"
		alias
			"[
				HMAC($md, $key, $kl, $sp, $n, $op, $oplp);
			]"
		end

	--|--------------------------------------------------------------

-- HMAC_CTX *h = HMAC_CTX_new();
-- HMAC_Init_ex(h, key, keylen, EVP_sha256(), NULL);
-- ...
-- HMAC_CTX_free(h);

	--RFO c_hmac_ctx_struct_size: INTEGER
	--RFO 		-- Size of struct hmac_ctx_st
	--RFO 	external
	--RFO 		"C inline use <openssl/hmac.h>"
	--RFO 	alias
	--RFO 		"[
	--RFO 			return sizeof(struct hmac_ctx_st);
	--RFO 		]"
	--RFO 	end

--|========================================================================
feature {NONE} -- EVP Externals
--|========================================================================

	--RFO c_evp_md_ctx_struct_size: INTEGER
	--RFO 		-- Size of MD5 hash state structure in OpenSSL EVP lib
	--RFO 	external
	--RFO 		"C inline use <openssl/evp.h>"
	--RFO 	alias
	--RFO 		"[
	--RFO 			return sizeof(EVP_MD_CTX);
	--RFO 		]"
	--RFO 	end

	c_new_md5_context: POINTER
			-- A pointer to a newly created MD5 context
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				return EVP_md5();
			]"
		end

	c_new_sha1_context: POINTER
			-- A pointer to a newly created MD5 context
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				return EVP_sha1();
			]"
		end

	c_evp_md_ctx_init (ctx: POINTER)
			-- Initialize the given EVP MD5 context 'ctx'
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				EVP_MD_CTX_init($ctx);
			]"
		end

	c_evp_digest_init_ex (ctx, md, impl: POINTER)
			-- Initialize the digest identified by ctx and md,
			-- per the Engine identified by 'impl' (may be NULL)
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				EVP_DigestInit_ex($ctx, $md, NULL);
			]"
		end

	c_evp_digest_update (ctx, msg: POINTER; len: INTEGER)
			-- Add source content 'msg' up to 'len bytes, to the context 'ctx'
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				EVP_DigestUpdate($ctx, $msg, $len);
			]"
		end

	c_evp_digest_final_ex (ctx, buf: POINTER): INTEGER
			-- Finish encoding the hash for the context 'ctx' and
			-- write into 'buf' the hash as octet stream 
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
			int i;
				EVP_DigestFinal_ex($ctx, $buf, &i);
				return i;
			]"
		end

	c_evp_md_ctx_cleanup (ctx: POINTER)
			-- Free resources associated with context 'ctx'
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				EVP_MD_CTX_cleanup($ctx);
			]"
		end

	c_init_openssl
			-- Initialize the OpenSSL subsystem
		external
			"C inline use <openssl/evp.h>"
		alias
			"[
				OpenSSL_add_all_digests();
			]"
		end

--|========================================================================
feature -- Digest lengths
--|========================================================================

	K_md5_output_len: INTEGER
		do
			Result := c_md5_output_len
		end

	K_sha1_output_len: INTEGER
		do
			Result := c_sha1_output_len
		end

	K_hmac_output_len: INTEGER
		do
			Result := c_hmac_output_len
		end

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	c_md5_output_len: INTEGER
		external
			"C [macro <openssl/md5.h>] : EIF_INTEGER"
		alias
			"MD5_DIGEST_LENGTH"
		end

	c_sha1_output_len: INTEGER
		external
			"C [macro <openssl/sha.h>] : EIF_INTEGER"
		alias
			"SHA_DIGEST_LENGTH"
		end

	c_hmac_output_len: INTEGER
		external
			"C [macro <openssl/evp.h>] : EIF_INTEGER"
		alias
			"EVP_MAX_MD_SIZE"
		end

end -- class AEL_SPRT_CRYPTO_ROUTINES_IMP
