class AEL_SPRT_CRYPTO_ROUTINES_IMP
-- Windows implementation of crypto routines
--RFO TODO
-- NB this are useless stubs right now and need to be replaced with
-- actual code that calls external crypto libraries

inherit
	AEL_SPRT_CRYPTO_ROUTINES_I

--|========================================================================
feature -- SHA1 hash generation
--|========================================================================

	sha1sum (v: STRING): AEL_DS_BYTE_STREAM
			-- SHA1 sum of the given byte stream, as octet stream
		do
			create Result.make_filled (255, K_sha1_output_len)
		end

--|========================================================================
feature -- MD5 hash generation
--|========================================================================

	md5sum (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream
		do
			create Result.make_filled (255, K_md5_output_len)
		end

	--|--------------------------------------------------------------

	md5sum_evp (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level EVP functions
		do
			create Result.make_filled (255, K_md5_output_len)
		end

	--|--------------------------------------------------------------

	md5sum_crypto (v: STRING): AEL_DS_BYTE_STREAM
			-- MD5 sum of the given byte stream, as octet stream,
			-- constructed using lower-level MD5 crypto functions
		do
			create Result.make_filled (255, K_md5_output_len)
		end

--|========================================================================
feature -- HMAC routines
--|========================================================================

	hmacsha1_digest (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		do
			create Result.make_filled (255, K_hmac_output_len)
		end

	hmacsha1_digest_alt (v, skey: STRING): AEL_DS_BYTE_STREAM
			-- HMAC_SHA1 digest of the given byte stream, as octet stream
		do
			create Result.make_filled (255, K_hmac_output_len)
		end

--|========================================================================
feature -- Digest lengths
--|========================================================================

	K_md5_output_len: INTEGER = 16
	K_sha1_output_len: INTEGER = 20
	K_hmac_output_len: INTEGER = 64

end -- class AEL_SPRT_CRYPTO_ROUTINES_IMP
