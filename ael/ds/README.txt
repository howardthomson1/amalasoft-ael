README.txt - Intro to the AEL_DS cluster

The Amalasoft Eiffel data structure cluster is a collection of data structure
classes that typically extend existing ELKS data structures.
It depends on the Eiffel base and ELKS libraries (available in open source
or commercial form from Eiffel Software www.eiffel.com) and is, like other
Eiffel code, portable across platforms.

This cluster also depends on the Amalasoft Data Structures cluster, the
Amalasoft Support cluster, and for testing, the Amalasoft Printf cluster.
