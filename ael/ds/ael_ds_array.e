note
	description: "{
An array with additional features
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2016 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_ARRAY [G]

inherit
	ARRAY [G]

create
	make_empty,
	make_filled,
	make_from_array,
	make_from_special,
	make_from_cil

--|========================================================================
feature -- Status
--|========================================================================

	is_mirror (other: like Current): BOOLEAN
			-- Is Current the exact mirror of 'other'?
			-- MUST also have same lower/upper bounds
			-- Object comparison settings apply
		local
			i, j, lim: INTEGER
		do
			if count = other.count and
				lower = other.lower and
				upper = other.upper
			 then
				 Result := True
				lim := upper
				from
					i := lower
					j := upper
				until i > lim or not Result
				loop
					if object_comparison then
						Result := item (i) ~ (other.item (j))
					else
						Result := item (i) = other.item (j)
					end
					i := i + 1
					j := j - 1
				end
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	first: G
			-- First item in array
		require
			not_empty: not is_empty
		do
			Result := item (lower)
		end

	last: G
			-- Last item in array
		require
			not_empty: not is_empty
		do
			Result := item (upper)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	copy_values (va: ARRAY [G])
			-- Copy, individually, values from 'va'
		require
			same_size: va.count = count
		local
			i, lim, oi: INTEGER
		do
			-- Array.copy will clobber 'area'
			-- This routine will copy elements instead
			-- 'va' might not be based same as Current
			lim := upper
			from
				i := lower
				oi := va.lower
			until i > lim
			loop
				area.put (va.item (oi), i - lower)
				i := i + 1
				oi := oi + 1
			end
		end

	--|--------------------------------------------------------------

	shuffle
			-- Shuffle items in Current in pseudo-random manner
		local
			i, lim: INTEGER
		do
			lim := upper
			-- The fisher-yates algorithm, kinda
			from i := lower
			until i > upper
			loop
				-- Swap item with random later item
				--swap(a, i, i + prng (n - 1 - i))
				swap (i, random_index (i))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	mirror
			-- Reverse the order of items in Current
		local
			a: like area
			v: like item
			i, j: INTEGER
		do
			if not is_empty then
				-- Work with 'area' to ensure 0-based indexing
				a := area
				from i := count - 1
				until i <= j
				loop
					v := a.item (i)
					a.put (a.item (j), i)
					a.put (v, j)
					i := i - 1
					j := j + 1
				end
			end
		ensure
			same_count: count = old count
			mirrord: is_mirror (old Current)
		end

	--|--------------------------------------------------------------

	swap (p1, p2: INTEGER)
			-- Swap items at indices 'p1' and 'p2'
		require
			valid_indices: is_valid_index (p1) and is_valid_index (p2)
		local
			tv: like item
		do
			tv := item (p1)
			put (item (p2), p1)
			put (tv, p2)
		ensure
			swapped: item (p1) = old (item (p2)) and item (p2) = old (item (p1))
		end

	--|--------------------------------------------------------------

	swap_segments (p1, p2, n: INTEGER)
			-- Swap 'n' items beginning with indices 'p1' and 'p2'
		require
			valid_indices: is_valid_index (p1) and is_valid_index (p2)
			small_enough: p1 + n <= upper and p2 + n <= upper
		local
			lim, tp1, tp2: INTEGER
		do
			lim := tp1 + n
			tp2 := p2
			from tp1 := p1
			until tp1 > lim
			loop
				swap (tp1, tp2)
				tp1 := tp1 + 1
				tp2 := tp2 + 1
			end
		end

	--|--------------------------------------------------------------

	random_index (s: INTEGER): INTEGER
			-- Random number, within legit index range
		local
			i, lim, v, m: INTEGER
			tval: AEL_TIMEVAL
		do
			Result := -1
			create tval.make_now
			m := upper + 1
			lim := 10 -- 10 tries, then quit
			from i := 1
			until i > lim or Result >= lower
			loop
				v := (prng.next_random (s) + tval.milliseconds) \\ m
				if lower /= 0 then
					v := v + lower
				end
				if is_valid_index (v) then
					Result := v
				end
				tval.set_to_present
				i := i + 1
			end
			if Result = -1 then
				Result := s
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid index in Current?
		do
			Result := v >= lower and v <= upper
		end

--|========================================================================
feature {NONE} -- Utilities
--|========================================================================

	prng: RANDOM
			-- Pseudo-random number generator
		once
			create Result.make
		end

	--|--------------------------------------------------------------
invariant

end -- class AEL_DS_ARRAY

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 26-Jul-2018
--|     Original module; Eiffel 8.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class, and use it as you would a
--| normal ARRAY
--|----------------------------------------------------------------------

