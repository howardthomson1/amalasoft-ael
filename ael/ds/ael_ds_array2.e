note
	description: "{
A 2-dimensional array with additional features
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_ARRAY2 [G]

inherit
	ARRAY2 [G]
		rename
			make_from_array as make_array_from_array
		redefine
			item
		end

create
	make_filled, make_from_array

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_from_array (nr, nc: INTEGER; v: ARRAY [G])
			-- Create Current with height 'nr' and width 'nc',
			-- filled with default value 'dv'
		do
			height := nr
			width := nc
			lower := v.lower
			upper := v.upper
			area := v.area
			--make_array_from_array (v)
		end

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

	item alias "[]" (row, column: INTEGER): G assign put
			-- Entry at coordinates (`row', `column')
		do
			Result := area.item ((row*width) + column - width - lower)
		end

	--|--------------------------------------------------------------

	items_in_row (v: INTEGER): ARRAY [G]
			-- Items in row 'v' of Current 
		require
			valid_row: (1 <= v) and (v <= height)
		local
			roff, sp, ep: INTEGER
		do
			if is_empty then
				Result := {ARRAY [G]} <<>>
			else
				roff := (v - 1) * width
				sp := roff + 1
				ep := roff + width
				create Result.make_filled (item (v, 1), sp, ep)
				Result.subcopy (Current, sp, ep, sp)
				Result.rebase (1)
			end
		end

	items_in_column (v: INTEGER): ARRAY [G]
			-- Items in column 'v' of Current
			-- In an array with 3 rows and 4 cols, the layout
			-- looks like this:
			-- 1,1 1,2 1,3 1,4 2,1 2,2 2,3 2,4 3,1 3,2 3,3 3,4
			-- Each row is contiguous, but columns are 'width' 
			-- positions apart
			-- Items are presented as being in separate rows, as:
			-- 1,1 1,2 1,3 1,4
			-- 2,1 2,2 2,3 2,4
			-- 3,1 3,2 3,3 3,4
			-- Assume that values are added to array in sequence, such 
			-- that item at 1,1=1, 1,2=2, ... 2,1=5, 2,2=6, etc.
			-- Column 2 in this case, would have values: 2, 6, 10
			-- The first item would be at offset ((1-1)*width)+v=v
			-- The second item would be at offset ((2-1)*width)+v
			-- and so forth
			-- A minor optimization can be had by using the first 
			-- position as a base, then adding the width for each 
			-- successive item: v, v+width, v+width+width, etc.
		require
			valid_column: (1 <= v) and (v <= width)
		local
			r, c: INTEGER
		do
			if is_empty then
				Result := {ARRAY [G]} <<>>
			else
				create Result.make_filled (item (1, v), 1, height)
				c := v +  1 - lower
				from r := 1
				until r > height
				loop
					Result.put (array_item (c), r)
					r := r + 1
					c := c + width
				end
			end
		end
--|========================================================================
feature -- Element Change
--|========================================================================

	copy_values (va: ARRAY [G])
			-- Copy, individually, values from 'va'
			-- Not well that array2 is a kind of array
		require
			same_size: va.count = count
		local
			i, lim, oi: INTEGER
		do
			-- Array.copy will clobber 'area'
			-- This routine will copy elements instead
			-- 'va' might not be based same as Current
			lim := upper
			from
				i := lower
				oi := va.lower
			until i > lim
			loop
				area.put (va.item (oi), i - lower)
				i := i + 1
				oi := oi + 1
			end
		end

--|========================================================================
feature -- Transformations
--|========================================================================

	rotated_ccw_1: like Current
			-- Contents of Current rotated counterclockwise
			-- by 1 quadrant (90 deg)
		require
			has_content: not is_empty
		local
			sr, sc, dr, dc, max_sr, max_sc, max_dr, max_dc: INTEGER
			ti: like item
		do
			max_sr := height
			max_sc := width
			max_dr := max_sc
			max_dc := max_sr
			ti := item (1, 1)
			create Result.make_filled (ti, max_dr, max_dc)

			-- for 90 degree ccw, need:
			-- 1a 1b 1c      1c 2c 3c 4c
			-- 2a 2b 2c  --> 1b 2b 3b 4b
			-- 3a 3b 3c      1a 2a 3a 4a
			-- 4a 4b 4c 
			from
				sr := 1
				sc := max_sc
				dr := 1
				dc := 1
			until dr > max_dr or dc > max_dc
			loop
				-- sr: 1 2 3 4  1 2 3 4  1 2 3 4
				-- sc: c c c c  b b b b  a a a a
				-- dr: 1 1 1 1  2 2 2 2  3 3 3 3
				-- dc: 1 2 3 4  1 2 3 4  1 2 3 4
				ti := item (sr, sc)
				Result.put (ti, dr, dc)
				-- increment src row, dst col
				sr := sr + 1
				dc := dc + 1
				if sr > max_sr then
					-- decrement src col;
					-- increment dst row; reset dst col
					sr := 1
					sc := sc - 1
					dr := dr + 1
					dc := 1
				end
			end
		end

	--|--------------------------------------------------------------

	rotated_ccw_2: like Current
			-- Contents of Current rotated counterclockwise
			-- by 2 quadrants (180 deg)
		require
			has_content: not is_empty
		local
			sr, sc, dr, dc, max_sr, max_sc, max_dr, max_dc: INTEGER
			ti: like item
		do
			max_sr := height
			max_sc := width
			max_dr := max_sr
			max_dc := max_sc
			ti := item (1, 1)
			create Result.make_filled (ti, max_dr, max_dc)

			-- for 180 degree ccw, need:
			-- 1a 1b 1c      4c 4b 4a
			-- 2a 2b 2c  --> 3c 3b 3a
			-- 3a 3b 3c      2c 2b 2a
			-- 4a 4b 4c      1c 1b 1a
			from
				sr := max_sr
				sc := max_sc
				dr := 1
				dc := 1
			until dr > max_dr or dc > max_dc
			loop
				-- sr: 4 4 4  3 3 3  2 2 2  1 1 1
				-- sc: c b a  c b a  c b a  c b a
				-- dr: 1 1 1  2 2 2  3 3 3  4 4 4
				-- dc: 1 2 3  1 2 3  1 2 3  1 2 3
				ti := item (sr, sc)
				Result.put (ti, dr, dc)
				-- decrement src col, increment dst col
				sc := sc - 1
				dc := dc + 1
				if sc = 0 then
					-- reset src col; decrement src row
					-- increment dst row; reset dst col
					sc := max_sc
					sr := sr - 1
					dr := dr + 1
					dc := 1
				end
			end
		end

	--|--------------------------------------------------------------

	rotated_ccw_3: like Current
			-- Contents of Current rotated counterclockwise
			-- by 2 quadrants (180 deg)
		require
			has_content: not is_empty
		local
			sr, sc, dr, dc, max_sr, max_sc, max_dr, max_dc: INTEGER
			ti: like item
		do
			max_sr := height
			max_sc := width
			max_dr := max_sc
			max_dc := max_sr
			ti := item (1, 1)
			create Result.make_filled (ti, max_dr, max_dc)

			-- for 270 degree ccw, need:
			-- 1a 1b 1c      4a 3a 2a 1a
			-- 2a 2b 2c  --> 4b 3b 2b 1b
			-- 3a 3b 3c      4c 3c 2c 1c
			-- 4a 4b 4c 
			from
				sr := max_sr
				sc := 1
				dr := 1
				dc := 1
			until dr > max_dr or dc > max_dc
			loop
				-- sr: 4 3 2 1  4 3 2 1  4 3 2 1
				-- sc: a a a a  b b b b  c c c c
				-- dr: 1 1 1 1  2 2 2 2  3 3 3 3
				-- dc: 1 2 3 4  1 2 3 4  1 2 3 4
				ti := item (sr, sc)
				Result.put (ti, dr, dc)
				-- decrement src row; increment dst col
				sr := sr - 1
				dc := dc + 1
				if sr = 0 then
					-- reset src row; increment src col
					-- increment dst row; reset dst col
					sr := max_sr
					sc := sc + 1
					dr := dr + 1
					dc := 1
				end
			end
		end

	--|--------------------------------------------------------------

	flipped_vertically: like Current
			-- Contents of Current flipped vertically
		require
			has_conent: not is_empty
		local
			sr, sc, dr, dc, max_sr, max_sc, max_dr, max_dc: INTEGER
			ti: like item
		do
			max_sr := height
			max_sc := width
			max_dr := max_sr
			max_dc := max_sc
			ti := item (1, 1)
			create Result.make_filled (ti, max_dr, max_dc)

			-- for vertical flip, need:
			-- 1a 1b 1c      4a 4b 4c
			-- 2a 2b 2c  --> 3a 3b 3c
			-- 3a 3b 3c      2a 2b 2c
			-- 4a 4b 4c      1a 1b 1c
			from
				sr := max_sr
				sc := 1
				dr := 1
				dc := 1
			until dr > max_dr or dc > max_dc
			loop
				-- sr: 4 4 4  3 3 3  2 2 2  1 1 1
				-- sc: a b c  a b c  a b c  a b c
				-- dr: 1 1 1  2 2 2  3 3 3  4 4 4
				-- dc: 1 2 3  1 2 3  1 2 3  1 2 3
				ti := item (sr, sc)
				Result.put (ti, dr, dc)
				-- increment src col
				-- increment dst col
				sc := sc + 1
				dc := dc + 1
				if sc > max_sc then
					-- reset src col; decrement src row
					-- increment dst row; reset dst col
					sc := 1
					sr := sr - 1
					dr := dr + 1
					dc := 1
				end
			end
		end

	--|--------------------------------------------------------------

	flipped_horizontally: like Current
		require
			has_content: not is_empty
		local
			sr, sc, dr, dc, max_sr, max_sc, max_dr, max_dc: INTEGER
			ti: like item
		do
			max_sr := height
			max_sc := width
			max_dr := max_sr
			max_dc := max_sc
			ti := item (1, 1)
			create Result.make_filled (ti, max_dr, max_dc)

			-- for horizontal flip, need:
			-- 1a 1b 1c      1c 1b 1a
			-- 2a 2b 2c  --> 2c 2b 2a
			-- 3a 3b 3c      3c 3b 3a
			-- 4a 4b 4c      4c 4b 4a
			from
				sr := 1
				sc := max_sc
				dr := 1
				dc := 1
			until dr > max_dr or dc > max_dc
			loop
				-- sr: 1 1 1  2 2 2  3 3 3  4 4 4
				-- sc: c b a  c b a  c b a  c b a
				-- dr: 1 1 1  2 2 2  3 3 3  4 4 4
				-- dc: 1 2 3  1 2 3  1 2 3  1 2 3
				ti := item (sr, sc)
				Result.put (ti, dr, dc)
				-- decrement src col
				-- increment dst col
				sc := sc - 1
				dc := dc + 1
				if sc = 0 then
					-- reset src col; increment src row
					-- increment dst row; reset dst col
					sc := max_sc
					sr := sr + 1
					dr := dr + 1
					dc := 1
				end
			end
		end

	--|--------------------------------------------------------------

	to_string: STRING
			-- Not the official "out" but handy
		local
			rn, cn, rlim, clim: INTEGER
		do
			create Result.make (128)
			rlim := height
			clim := width
			from rn := 1
			until rn > rlim
			loop
				from cn := 1
				until cn > clim
				loop
					if attached item (rn, cn) as ti then
						Result.append (ti.out + " ")
					end
					cn := cn + 1
				end
				Result.append ("%N")
				rn := rn + 1
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put_row (a_row: ARRAY [G]; a_row_index: INTEGER)
			-- Put items in `a_row' into Current on `a_row_index'.
		require
			col_count: a_row.count = width
		do
			across
				a_row as ic
			loop
				put (ic.item, a_row_index, ic.cursor_index)
			end
		ensure
			row_put: across 1 |..| a_row.count as ic all
							item (a_row_index, ic.item) ~ a_row.item (ic.item)
						end
		end

	put_row_offset (a_items: ARRAY [G]; a_row_index, a_offset: INTEGER)
			-- Put `a_items' into Current, starting in column number `a_offset'.
		require
			non_empty: not a_items.is_empty
			col_count: a_items.count <= width
			valid_offset: a_offset >= 1 and then
							a_offset < width and then
							width >= (a_offset + a_items.count - 1)
		do
			across
				a_items as ic
			loop
				put (ic.item, a_row_index, ic.cursor_index + a_offset - 1)
			end
		ensure
			row_put: across 1 |..| a_items.count as ic all
							item (a_row_index, ic.item + a_offset - 1) ~ a_items.item (ic.item)
						end
		end

	put_column (a_column: ARRAY [G]; a_column_index: INTEGER)
			-- Put items in `a_column' into Current on `a_column_index'.
		require
			row_count: a_column.count = width
		do
			across
				a_column as ic
			loop
				put (ic.item, ic.cursor_index, a_column_index)
			end
		ensure
			col_put: across 1 |..| a_column.count as ic all
							item (ic.item, a_column_index) ~ a_column.item (ic.item)
						end
		end

	put_column_offset (a_items: ARRAY [G]; a_column_index, a_offset: INTEGER)
			-- Put `a_items' into Current, starting in row number `a_offset'.
		require
			non_empty: not a_items.is_empty
			row_count: a_items.count <= height
			valid_offset: a_offset >= 1 and then
							a_offset < height and then
							height >= (a_offset + a_items.count - 1)
		do
			across
				a_items as ic
			loop
				put (ic.item, ic.cursor_index + a_offset - 1, a_column_index)
			end
		ensure
			column_put: across 1 |..| a_items.count as ic all
							item (ic.item + a_offset - 1, a_column_index) ~ a_items.item (ic.item)
						end
		end

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Utilities
--|========================================================================

	--|--------------------------------------------------------------
invariant

end -- class AEL_DS_ARRAY2

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 09-Aug-2018
--|     Original module; Eiffel 8.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class, and use it as you would a
--| normal ARRAY2
--|
--|  The items_in_row and items_in_column routines can be exercised 
--|  as in the following example
--|
--| 			a2: AEL_DS_ARRAY2 [INTEGER]
--|			ar, ac: ARRAY [INTEGER]
--|
--|			create a2.make_filled (0, 3, 4)
--|			a2.put (1, 1, 1)
--|			a2.put (2, 1, 2)
--|			a2.put (3, 1, 3)
--|			a2.put (4, 1, 4)
--|			a2.put (5, 2, 1)
--|			a2.put (6, 2, 2)
--|			a2.put (7, 2, 3)
--|			a2.put (8, 2, 4)
--|			a2.put (9, 3, 1)
--|			a2.put (10, 3, 2)
--|			a2.put (11, 3, 3)
--|			a2.put (12, 3, 4)
--|			ar := a2.items_in_row (1)
--|			ac := a2.items_in_column (1)
--|			ar := a2.items_in_row (2)
--|			ac := a2.items_in_column (2)
--|			ar := a2.items_in_row (3)
--|			ac := a2.items_in_column (3)
--|			ac := a2.items_in_column (4)
--|
--|----------------------------------------------------------------------

