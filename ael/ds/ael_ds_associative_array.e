note
	description: "{
An associative array in which items can be access via position (index)
or by name (key).
A given name or key identified uniquely and exclusively a position.
Removal of an item does not remove the name associated with that slot.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/06/25 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_ASSOCIATIVE_ARRAY [G]

inherit
	FINITE [detachable G]
		redefine
			default_create, compare_objects, compare_references
		end

create
	default_create, make_with_size, make_with_labels

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_size (sz: INTEGER)
			-- Create Current with 'sz' empty slots
	require
		valid_size: sz >= 0
		do
			capacity := sz
			default_create
		end

	--|--------------------------------------------------------------

	make_with_labels (ll: ARRAY [STRING_GENERAL])
			-- Create Current with labels 'll', one slot per label
		require
			labels_exist: ll /= Void
		do
			make_with_size (ll.count)
			set_labels (ll)
		ensure
			valid_count: capacity = ll.count
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		local
			i, lim: INTEGER
		do
			create array.make (capacity)
			lim := capacity
			from
				array.start
				i := 1
			until i > lim
			loop
				array.extend (default_item)
				i := i + 1
			end
			create table.make (capacity)
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	capacity: INTEGER
			-- Number of slots in Current (not necessarily same as count)

	--|--------------------------------------------------------------

	count: INTEGER
			-- Number of items in Current

	--|--------------------------------------------------------------

	full, is_full: BOOLEAN
			-- Is Current filled to capacity?
			-- (does it have 'capacity' items?)
		do
			Result := count = capacity
		end

	--|--------------------------------------------------------------

	has (v: G): BOOLEAN
			-- Does Current have the given item?
 			-- (Reference or object equality,
			-- based on 'object_comparison'.)
		local
			i, nb: INTEGER
		do
			nb := capacity
			if object_comparison and v /= Void then
				from array.start
				until array.exhausted or Result
				loop
					if attached {G} array.item.item (2) as ti then
						Result := ti ~ v
					end
					array.forth
				end
			else
				from array.start
				until array.exhausted or Result
				loop
					if attached {G} array.item.item (2) as ti then
						Result := ti = v
					end
					array.forth
				end
			end
		end

	--|--------------------------------------------------------------

	has_by_index (v: INTEGER): BOOLEAN
			-- Does Current have an item in position 'v'?
		do
			Result := attached array.i_th (v).item (2)
		end

	has_by_label (v: STRING_GENERAL): BOOLEAN
			-- Does Current have an item in the position associated 
			-- with label 'v'?
		require
			known_label: has_label (v)
		do
			Result := has_by_index (label_to_index (v))
		end

	has_label (v: STRING_GENERAL): BOOLEAN
			-- Does Current have a position whose associated label is 'v'?
		do
			Result := table.has (v) 
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_label (v: STRING_GENERAL; i: INTEGER)
			-- Set to 'v' the label associated with the 'i'_th position
		require
			valid_label: is_valid_label (v)
			valid_index: is_valid_index (i)
			is_unique: not has_label (v)
		do
			array.i_th (i).put_reference (v, 1)
			table.force (i, v)
		ensure
			is_set: label_to_index (v) = i
			reflexive: v.same_string (label_at_index (i))
		end

	--|--------------------------------------------------------------

	set_labels (ll: ARRAY [STRING_GENERAL])
			-- Set the labels associated with positions to those in 'll'
		require
			labels_exist: ll /= Void
			valid_count: ll.count = capacity
		local
			i, lim: INTEGER
		do
			lim := ll.upper
			from i := ll.lower
			until i > lim
			loop
				set_label (ll.item (i), i)
				i := i + 1
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	i_th (v: INTEGER): detachable G
			-- Item at 'i'_th position, if any
		require
			valid_index: is_valid_index (v)
		do
			if attached {like item} array.i_th (v).item (2) as ti then
				Result := ti
			end
		end

	item (v: STRING_GENERAL): detachable G
			-- Item at position associated with label 'v', if any
		require
			known_label: has_label (v)
		do
			if attached {like item} array.i_th (label_to_index (v)).item (2) as ti
			 then
				 Result := ti
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	compare_objects
			-- Ensure that future search operations will use 'equal'
			-- rather than '=' for comparing references.
		do
			Precursor
			array.compare_objects
			table.compare_objects
		ensure then
			consistent: array.object_comparison = table.object_comparison
		end

	--|--------------------------------------------------------------

	compare_references
			-- Ensure that future search operations will use '='
			-- rather than 'equal' for comparing references.
		do
			Precursor
			array.compare_references
			table.compare_references
		ensure then
			consistent: array.object_comparison = table.object_comparison
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	linear_representation: ARRAYED_LIST [detachable G]
			-- Representation as a linear structure
		do
			create Result.make (capacity)
			from array.start
			until array.exhausted
			loop
				if attached {G} array.item.reference_item (2) as ti then
					Result.extend (ti)
				end
				array.forth
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put (v: G; i: INTEGER)
			-- Put 'v' at 'i'-th position.
		local
			tt: like default_item
		do
			tt := array.i_th (i)
			if not attached tt.reference_item (2) then
				count := count + 1
			end
			tt.put_reference (v, 2)
		ensure
			inserted: i_th (i) = v
			counted_if_new: not (attached (old (i_th (i))))
				implies count = (old count + 1)
			replace_not_counted: attached (old (i_th (i)))
				implies count = old count
		end

	put_by_label (v: G; lb: STRING_GENERAL)
			-- Put 'v' at position associated with label 'lb'.
		require
			known_label: has_label (lb)
		do
			put (v, label_to_index (lb))
		ensure
			inserted: item (lb) = v
			counted_if_new: not (attached (old (item (lb))))
				implies count = (old count + 1)
			replace_not_counted: attached (old (item (lb)))
				implies count = old count
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove (i: INTEGER)
			-- Remove item from 'i'-th position.
		require
			valid_index: is_valid_index (i)
		local
			tt: like default_item
			tg: detachable G -- deliberately Void
		do
			tt := array.i_th (i)
			if attached tt.reference_item (2) then
				count := count - 1
				tt.put_reference (tg, 2)
			end
		ensure
			removed: not has_by_index (i)
			has_decremented: (old has_by_index (i)) implies count = (old count - 1)
			not_has: (not old has_by_index (i)) implies count = old count
		end

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid index into Current?
		do
			--| array is 1-based, even if presented otherwise
			Result := v > 0 and v <= capacity
		end

	is_valid_label (v: STRING_GENERAL): BOOLEAN
			-- Is 'v' a valid label (syntactically) ?
		do
			Result := v /= Void and then not v.is_empty
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	array: ARRAYED_LIST [like default_item]
			-- Implementation the array

	default_item: TUPLE [STRING_GENERAL, detachable G]
			-- Newly generated (each call) itme for array
		do
			create Result
			Result.put ("", 1)
		end

	table: HASH_TABLE [INTEGER, STRING_GENERAL]
			-- Implementation of index table
			-- Items are the indics into 'array', accessible by 
			-- associated label as key

	label_to_index (v: STRING_GENERAL): INTEGER
			-- Index (position) associated with label 'v'
		require
			known_label: has_label (v)
		do
			Result := table.item (v)
		ensure
			is_valid_index (Result)
		end

	label_at_index (i: INTEGER): STRING_GENERAL
			-- Label associated with position i
		require
			valid_index: is_valid_index (i)
		do
			if attached {STRING_GENERAL} array.i_th (i).reference_item (1) as ti
			 then
				 Result := ti
			else
				Result := ""
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant
	consistent_array_capacity: capacity = array.capacity
	consistent_table_capacity: capacity <= table.capacity

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Jn-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_DS_ASSOCIATIVE_ARRAY [G]
