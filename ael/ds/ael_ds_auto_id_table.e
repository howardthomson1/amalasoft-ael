note
	description: "{
A dual-key table with auto-generated and managed indices
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/06/25 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_AUTO_ID_TABLE [G]

inherit
	AEL_DS_DUAL_KEY_TABLE [G]
		rename
			extend as dk_extend,
			force as dk_force,
			put as dk_put
		redefine
			create_stabile_attributes
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	create_stabile_attributes
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	highest_allocated_id: INTEGER_64
			-- Highest-valued item id already allocated
			-- integer.min_value (K_dk_invalid_id) if none

--|========================================================================
feature -- Status Setting
--|========================================================================

	set_highest_allocated_id (v: like highest_allocated_id)
			-- Set the highest_allocated_id to 'v'
			-- 'v' must be greater than or equal to the current value
			-- This routine can be handy for reserving ranges for 
			-- predefined values or to force starting ranges
		require
			not_less: v >= highest_allocated_id
		do
			highest_allocated_id := v
		ensure
			is_set: highest_allocated_id = v
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: like Kta_dk_key)
			-- Assuming there is no item of key 'k',
			-- insert 'v' with key 'k'
			-- Set 'inserted'.
		require
			exists: v /= Void
			valid_key: is_valid_key (k)
			is_new_key: not has (k)
		do
			force (v, k)
		end

	--|--------------------------------------------------------------

	force (new: G; key: like Kta_dk_key)
			-- Update table so that 'new' will be the item associated
			-- with 'key'.
			-- If there was an item for that key, set 'found'
			-- and set 'found_item' to that item.
			-- If there was none, autogen a new id  and store item by 
			-- key and id, set 'not_found' and set
			-- 'found_item' to the default item value.
			--
			-- To choose between various insert/replace procedures,
			-- see "instructions" in the Indexing (note) clause.
		require
			exists: new /= Void
			valid_key: is_valid_key (key)
		local
			idx: like Kta_dk_id
		do
			if attached ht_item (key) as ti then
				-- Replace item, reuse key and id
				idx := ti.id
			else
				idx := new_id
			end
			dk_force (new, key, idx)
		ensure
			added_by_key: has (key)
			added_by_id: attached ht_item (key) as ti
				implies has_item_by_id (ti.id)
			key_consistent: attached ht_item (key) as ti implies ti.key = key
			item_same_by_key: attached ht_item (key) as ti
				implies ti.item = item (key)
		end

--|========================================================================
feature {HASH_TABLE} -- Element change implementation
--|========================================================================

	put (new: G; key: like Kta_dk_key)
			-- Insert 'new' with 'key' if there is 
			-- no other item associated with the same key.
			-- Set 'inserted' if and only if an insertion has
			-- been made (i.e. 'key' was not present).
			-- If so, set 'position' to the insertion position.
			-- If not, set 'conflict'.
			-- In either case, set 'found_item' to the item
			-- now associated with 'key' (previous item if
			-- there was one, 'new' otherwise).
			--
			-- To choose between various insert/replace procedures,
			-- see "instructions" in the Indexing (note) clause.
		require
			exists: new /= Void
			valid_key: is_valid_key (key)
		local
			idx: like Kta_dk_id
		do
			if attached ht_item (key) as ti then
				-- Nope, use ht to record conflict
				idx := ti.id
			else
				idx := new_id
			end
			dk_put (new, key, idx)
		ensure
			inserted: inserted implies has (key)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	new_id: like Kta_dk_id
			-- Value for next id to be allocated
			-- Calling new_id forces increment of 
			-- highest_allocated_id
		do
			Result := highest_allocated_id + 1
			highest_allocated_id := Result
		ensure
			valid: is_valid_id (Result)
			one_more: Result = old highest_allocated_id + 1
			highest_incremented: highest_allocated_id = Result
		end

	--|--------------------------------------------------------------
invariant
	valid_highest: highest_allocated_id >= K_dk_invalid_id
	highest_consistent_with_count: ((not is_resizing) and (count /= 0))
		implies highest_allocated_id /= K_dk_invalid_id

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Jn-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_DS_AUTO_ID_TABLE
