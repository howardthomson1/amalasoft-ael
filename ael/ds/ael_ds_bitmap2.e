--|----------------------------------------------------------------------
--| Copyright (c) 1995-2012, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A bitmap data structure of (more or less) arbitrary size.
--| Bits are accessed by 0-based positions
--|
--| Implemented as an array of NATURAL_8 as if a contiguous 
--| big-endian sequence of bits.  The nth natural_8 is this the LEAST 
--| significant, though this is not visible from a client.
--|----------------------------------------------------------------------

class AEL_DS_BITMAP2

inherit
	AEL_NUMERIC_INTEGER_ROUTINES
		redefine
			out
		end

create
	make, make_from_string, make_from_compact_string, make_from_stream

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER_32)
			-- Create a new bitmap with 'sz' bit positions
			-- Actual number of bits will be rounded to the next nearest 
			-- even multiple of 8 bits
		require
			valid_size: sz > 0
		do
			count := even_8_multiple (sz)
			length := count // 8
			max_position := count - 1
			create octets.make_filled (0, 1, length)
			--create ranges.make (1)
			--ranges.extend (octets)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING_8)
			-- Create and initialize Current from the given string
			-- of characters
			-- String should contain only '1' and '0' characters, but
			-- only '1' chars are considered; all others are treated as 
			-- False
			-- String is inherently big-endian and so reflects well the 
			-- intended implementation of bits
		local
			i, lim: INTEGER_32
		do
			lim := v.count
			make (even_8_multiple (lim))
			from i := 1
			until i > lim
			loop
				if v.item (i) = '1' then
					set_bit (i)
				end
				i := i + 1
			end
		ensure
			bits_equivalent: number_of_set_positions = v.occurrences ('1')
		end

	--|--------------------------------------------------------------

	make_from_compact_string (v: STRING_8)
			-- Create and initialize Current from the given compact string
			-- form
			-- String contains a size field (number of BITS), and then
			-- a ':' separator followed by zero or more hexadecimal 
			-- digits, each 0-padded pair representing a byte in the map.
			-- Octets are arranged from MOST significant to LEAST
			-- significant (bits are, as expected, also big-endian;
			-- i.e. bit 7 is leftmost and most significant).
			--
			-- For example, the string "16:410c" would be encoded as
			-- 2 octets (16 bits):
			--              01000001 00001010
			--                4   1    0   c
			-- Parsing the compact string involves first getting the 
			-- count field, and then walking through the string 
			-- backwards (from right to left), 2 hex digits at a time to 
			-- extract the octets.  Working left-to-right would fail 
			-- when there is an odd number of digits.
		require
			valid: v /= Void and then not v.is_empty
			has_count: v.has (':')
		local
			i, bp, lim, ep, len: INTEGER_32
			tn: NATURAL_8
			ts: STRING_8
			d1, d2: CHARACTER
			emsg: detachable STRING_8
		do
			ep := v.index_of (':', 1)
			ts := v.substring (1, ep - 1)
			len := ts.to_integer
			make (even_8_multiple (len))
			lim := ep + 1
			from
				i := lim
				bp := 1
			until i = 0 or emsg /= Void
			loop
				d2 := v.item (i)
				if not d2.is_hexa_digit then
					emsg := "Character as position " +
						i.out + " is not a hex digit"
				else
					if i > 0 then
						d1 := v.item (i - 1)
						if not d1.is_hexa_digit then
							emsg := "Character as position " +
								(i-1).out + " is not a hex digit"
						end
					else
						d1 := '0'
					end
				end
				if emsg = Void then
					tn := hex_digits_to_byte (d1, d2)
					set_map_item (tn, bp)
				end
				i := i - 2
				bp := bp + 1
			end
		end

	--|--------------------------------------------------------------

	make_from_stream (v: ARRAY [NATURAL_8])
			-- Create and initialize Current from octet stream 'v'
		require
			exists: v /= Void
			not_empty: not v.is_empty
			-- Counts are signed integers, so map cannot exceed 2**31 / 8
			-- In reality, this is way too large a bitmap to handle for 
			-- a number of reasons
			small_enough: v.count <= 268_435_455
		do
			length := v.count
			count := length |<< 3
			max_position := count - 1
			octets := v
			--| count number of set bits
			number_of_set_positions := number_of_sets_in_range (0, max_position)
		end

--|========================================================================
feature -- Status
--|========================================================================

	octets: ARRAY [NATURAL_8]
			-- The actual map implemented as a collection of octets, 
			-- ordered from most to least significant

--RFO TODO
-- for very large bitmaps, break into regions

	--ranges: ARRAYED_LIST [like octets]
			-- Separate ranges of octets

	count: INTEGER_32
		-- Number of bits in map

	max_position: INTEGER_32
			-- Highest indexable position in Current (count - 1)

	length: INTEGER_32
		-- Number of octets (BYTES) in map

	--|--------------------------------------------------------------

	number_of_set_positions: INTEGER_32
			-- Number of bit positions that are set

	--|--------------------------------------------------------------

	number_of_bits_set_before_position (p: INTEGER_32): INTEGER_32
			-- Number of bits set from bit 0 up to but NOT including the 
			-- given bit position
		require
			in_range: is_valid_bit_position (p)
		do
			if p = max_position then
				Result := number_of_set_positions
				if Result /= 0 and bit_is_set (max_position) then
					Result := Result - 1
				end
			else
				Result := number_of_sets_in_range (0, p - 1)
			end
		end

	--|--------------------------------------------------------------

	number_of_bits_set_from_position (p: INTEGER_32): INTEGER_32
			-- Number of bits set from the given bit position to the map end,
			-- INCLUSIVE
		require
			in_range: is_valid_bit_position (p)
		do
			Result := number_of_sets_in_range (p, max_position)
		end

	--|--------------------------------------------------------------

	number_of_sets_in_range (s, p: INTEGER_32): INTEGER_32
			-- Number of bits set from position 's' to 'p', INCLUSIVE
		require
			in_range: is_valid_bit_position (p) and is_valid_bit_position (s)
		local
			i, lim: INTEGER_32
			sbyte, ebyte, sbit, ebit: INTEGER_32
			positions: like bit_to_positions
			byte: NATURAL_8
		do
			-- N.B. octets are arrange from most to least significant 
			-- and so must be traversed from highest index to lowest to 
			-- perserve order.  The starting octet (sbyte) will have a 
			-- higher value than the ending octet (ebyte)
			positions := bit_to_positions (s)
			sbyte := positions [1]
			sbit := positions [2]
			positions := bit_to_positions (p)
			ebyte := positions [1]
			ebit := positions [2]

			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 8 is last bit in first byte and
				-- sp = 9 is first bit in second byte
				-- Process partial byte first
				Result := Result + bits_set_in_byte (octets [sbyte], sbit, 7)
				-- Next octet index is 1 _LOWER_
				sbyte := sbyte - 1
			end
			if ebit /= 7 then
				-- End position is not on a byte boundary
				-- Loop through whole bytes first, then check partial
				-- byte if still unfound at the end
				-- Previous octet is 1 _HIGHER_
				lim := ebyte  + 1
			else
				lim := ebyte
			end
			from i := sbyte
			until i < lim
			loop
				byte := octets [i]
				if byte /= 0x00 then
					Result := Result + num_bits_set_8 (byte)
				end
				i := i - 1
			end
			-- Now check any partial bytes at the end of the range
			if lim /= ebyte then
				Result := Result + bits_set_in_byte (octets [ebyte], 0, ebit)
			end
		end

	--|--------------------------------------------------------------

	bits_set_in_byte (byte: NATURAL_8; sp, ep: INTEGER_32): INTEGER_32
			-- Number of bits, between sp and ep (inclusive) in the given
			-- byte that are currently set
			-- sp and ep are 0-based
		require
			valid_start: sp >= 0 and sp <= 7
			valid_end: ep >= sp and ep <= 7
		local
			tb: NATURAL_8
		do
			if byte /= 0x00 then
				-- Has at least one set bit, so worth a look
				-- Bits are numbered right to left
				if sp = 0 and ep = 7 then
					-- Whole byte
					Result := num_bits_set_8 (byte)
				else
					tb := byte & bit_positions_to_mask (sp, ep)
					Result := num_bits_set_8 (tb)
				end
			end
		end

	bit_positions_to_mask (sp, ep: INTEGER_32): NATURAL_8
			-- Byte mask that contains ones only within the bit range
			-- delimited by sp and ep (0-based), inclusive
			--
			-- To create an empty mask (why bother?), use an end position
			-- less than the start position
		require
			valid_start: sp >= 0 and sp <= 7
			valid_end: ep >= 0 and ep <= 7
		local
			i, lim, uno: NATURAL_8
		do
			uno := i.one
			lim := ep.to_natural_8
			from
				i := sp.to_natural_8
			until i > lim
			loop
				Result := Result | (uno |<< i)
				i := i + 1
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_bit_position (v: INTEGER_32): BOOLEAN
			-- Is the given number a valid bit position for this map?
		do
			Result := v >= 0 and v < count
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING_8
			-- String representation of bitmap (as a stream of 1s and 0s)
			--
			-- N.B. in String form, bits appear left to right, from MOST
			-- significant to least (7-0 per byte) and octets also 
			-- appear from MOST to LEAST
		local
			i, lim: INTEGER_32
		do
			create Result.make (count)
			lim := length
			from i := 1
			until i > lim
			loop
				Result.append (byte_out (octets [i]))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	byte_out (v: NATURAL_8): STRING_8
			-- String representation of the given byte
			-- (as a stream of 1s and 0s)
			--
			-- N.B. in String form, bits appear left to right, from MOST
			-- significant to LEAST (7-0 per byte)
		local
			i: INTEGER_32
		do
			create Result.make (8)
			from i := 7
			until i < 0
			loop
				if v.bit_test (i) then
					Result.extend ('1')
				else
					Result.extend ('0')
				end
				i := i - 1
			end
		ensure
			exists: Result /= Void
			filled: Result.count = 8
		end

	--|--------------------------------------------------------------

	compact_out: STRING_8
			-- String representation of bitmap as a size field followed 
			-- by a sequence of string-encoded bytes, each representing
			-- 8 bits of the map.
			-- Bytes are arranged from LEAST significant to MOST
			-- significant, but bits within each byte are arranged from
			-- MOST to LEAST (ie bit 7 is leftmost).
			--
			-- For example, a 16-bit map with value (msb-to-lsb) of:
			--              0100000100001010
			-- (bit 15)  MSB^               ^LSB (bit 0)
			-- would be represented by 2 encoded bytes, each with
			-- 2 hexadecmal digits, as:
			--              00001010 01000001
			--              0   c    4   1    --> 0c41
			--
			-- N.B. This is a platform-independent representation and as 
			-- such even though a stream might appear to be an integer,
			-- it is not.
		local
			i, lim: INTEGER_32
		do
			create Result.make (8 + length * 2)
			lim := length
			Result.append_integer (count)
			Result.extend (':')
			from i := 1
			until i > lim
			loop
				Result.append (byte_compact_out (octets [i]))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	byte_compact_out (v: NATURAL_8): STRING_8
			-- String representation of the given byte as a pair of
			-- hexadecimal digits
		do
			Result := v.to_hex_string
			if Result.count < 2 then
				Result := "0" + Result
			end
		ensure
			exists: Result /= Void
			filled: Result.count = 2
		end

--|========================================================================
feature -- Access
--|========================================================================

	bits_set: LINKED_LIST [INTEGER_32]
			-- List of 1-based bit positions that are set
			-- See bit_to_positions comment for more information
		local
			i, lim, bi: INTEGER_32
			byte: NATURAL_8
		do
			create Result.make
			lim := length
			--| Maintain bit sequence from least to most significant
			from i := length
			until i = 0
			loop
				byte := octets [i]
				if byte /= 0x00 then
					-- Has a set bit
					from bi := 0
					until bi > 7
					loop
						if byte.bit_test (bi) then
							-- Found a set number
							Result.extend (positions_to_bit (<< i, bi >>))
						end
						bi := bi + 1
					end
				end
				i := i - 1
			end
		end

	--|--------------------------------------------------------------

	lowest_unset_position: INTEGER_32
			-- First available (lowest) unset 0-based position
		do
			if number_of_set_positions = 0 then
				Result := 0
			else
				Result := lowest_unset_position_from_octet (length)
			end
		end

	--|--------------------------------------------------------------

	lowest_set_position: INTEGER_32
			-- First 0-based position that is set; -1 if none is set
		do
			if number_of_set_positions = 0 then
				Result := -1
			else
				Result := lowest_set_position_from_octet (length)
			end
		end

	--|--------------------------------------------------------------

	lowest_set_position_from_octet (idx: INTEGER_32): INTEGER_32
			-- Lowest 0-based position that is set from starting octet
			-- at index 'idx' through end of map, if any
			-- -1 if none is set
		local
			i: INTEGER_32
		do
			Result := -1
			from i := idx
			until i = 0 or Result >= 0
			loop
				Result := lowest_bit_set (octets [i])
				i := i - 1
			end
		end

	lowest_unset_position_from_octet (idx: INTEGER_32): INTEGER_32
			-- Lowest 0-based position that is unset from starting octet
			-- at index 'idx' through end of map, if any
			-- -1 if none is unset
		local
			i: INTEGER_32
		do
			Result := -1
			from i := idx
			until i = 0 or Result >= 0
			loop
				Result := lowest_bit_unset (octets [i])
				i := i - 1
			end
		end

	--|--------------------------------------------------------------

	next_set_position (v: INTEGER_32): INTEGER_32
			-- First 0-based position that is set at 'v' or after;
			-- -1 if none is set
		require
			in_range: is_valid_bit_position (v)
		do
			Result := next_position (v, True)
		end

	next_set_position_after (v: INTEGER_32): INTEGER_32
			-- First 0-based position that is set at AFTER 'v';
			-- -1 if none is set
		require
			in_range: is_valid_bit_position (v+1)
		do
			Result := next_position (v+1, True)
		end

	--|--------------------------------------------------------------

	next_unset_position (v: INTEGER_32): INTEGER_32
			-- First 0-based position that is unset at 'v' or after;
			-- -1 if all are set
		require
			in_range: is_valid_bit_position (v)
		do
			Result := next_position (v, False)
		end

	next_unset_position_after (v: INTEGER_32): INTEGER_32
			-- First 0-based position that is unset AFTER 'v';
			-- -1 if all are set
		require
			in_range: is_valid_bit_position (v+1)
		do
			Result := next_position (v+1, False)
		end

	--|--------------------------------------------------------------

	next_position (v: INTEGER_32; sf: BOOLEAN): INTEGER_32
			-- First 0-based position, at 'v' or higher, that matches 
			-- 'sf' where True denotes 'set' and False denotes 'unset'
			-- -1 if no matches are found
		require
			in_range: is_valid_bit_position (v)
		local
			positions: like bit_to_positions
			sbyte, sbit: INTEGER_32
			bf: BOOLEAN
		do
			Result := -1
			positions := bit_to_positions (v)
			sbyte := positions [1]
			sbit := positions [2]
			bf := octets [sbyte].bit_test (sbit)
			if bf = sf then
				Result := v
			else
				sbit := next_position_in_byte (sbyte, sbit, sf)
				if sbit >= 0 then
					Result := positions_to_bit (<<sbyte, sbit>>)
				end
			end
			if Result = -1 then
				Result := lowest_position_from_octet (sbyte - 1, sf)
			end
		end

	--|--------------------------------------------------------------

	lowest_set_position_in_range (sp, ep: INTEGER_32): INTEGER_32
			-- First 0-based position at or after position 'sp'
			-- and before or at position 'ep' with a value of '1'
			--
			-- If no bits are set in range, then Result is -1
		require
			in_range: is_valid_bit_position (sp)
			end_in_range: is_valid_bit_position (ep)
		local
			i, spos, lim, pos: INTEGER_32
			positions: like bit_to_positions
			sbyte, ebyte, sbit, ebit: INTEGER_32
		do
			Result := -1
			positions := bit_to_positions (sp)
			--| sbyte is octet in which start of range (sp) bit resides
			--| Its index should be HIGHER than that of ebyte because 
			--| octets are arranged most-to-least
			sbyte := positions [1]
			sbit := positions [2]
			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 7 is last bit in first byte and
				-- sp = 8 is first bit in second byte
				-- Process partial byte first

				pos := next_set_position_in_byte (sbyte, sbit)
				-- If pos=-1, then no bits past sbit in the start byte are set
				if pos /= -1 then
					Result := pos + ((sbyte-1) * 8)
				else
					-- Skip to the next byte, if any
					spos := sbyte + 1
				end
			else
				spos := sbyte
			end
			if Result = -1 then
				positions := bit_to_positions (ep)
				--| ebyte is octet in which end of range (sp) bit resides
				ebyte := positions [1]
				ebit := positions [2]
				if ebit /= 7 then
					-- End position is not on a byte boundary
					-- Loop through whole bytes first, then check partial
					-- byte if still unfound
					-- Octets, arranged most-to-least, will be traversed 
					-- least-to-most (back to front) to preserve ordering
					-- As such, the 'next' byte is 1 greater
					lim := ebyte + 1
				else
					lim := ebyte
				end
				--| Traverse the octets from least to most (back to front)
				--| lim is lower than spos and traversal should be from 
				--| lim to spos
				from i := spos
				until i < lim or Result /= -1
				loop
					pos := lowest_bit_set (octets [i])
					if pos /= -1 then
						Result := pos + ((length - i) * 8)
					end
					i := i - 1
				end
				-- Now check any partial bytes at the end of the range
				if Result = -1 and lim /= ebyte then
					pos := lowest_bit_set (octets [ebyte])
					-- If pos=-1, then no bits in the end byte are set
					-- If pos > ebit, then the first set is after the end
					-- and therefore of no interest
					if pos /= -1 and pos <= ebit then
						Result := pos + ((ebyte-1) * 8)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	i_th_set_position (i: INTEGER_32) : INTEGER_32
			-- 0-based position of the i_th bit that is set;
			-- -1 if none or fewer than 'i' are set
		local
			sp: INTEGER_32
		do
			from sp := lowest_set_position
			until sp = i or sp > count
			loop
				sp := next_set_position (sp+1)
			end
			Result := sp
		end

	--|--------------------------------------------------------------

	lowest_unset_position_in_range (sp, ep: INTEGER_32): INTEGER_32
			-- First 0-based position at or after position 'sp'
			-- and before or at position 'lim' with a value of '0'
			--
			-- Result is -1 if no unset bits are found.
		require
			in_range: is_valid_bit_position (sp)
			end_in_range: is_valid_bit_position (ep)
		local
			i, pos, lim: INTEGER_32
			positions: like bit_to_positions
			sbyte, ebyte, sbit, ebit: INTEGER_32
		do
			Result := -1
			positions := bit_to_positions (sp)
			sbyte := positions [1]
			sbit := positions [2]
			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 7 is last bit in first byte and
				-- sp = 8 is first bit in second byte
				-- Process partial byte first

				pos := next_unset_position_in_byte (sbyte, sbit)
				pos := lowest_unset_position_in_byte (sbyte)
				if pos = -1 then
					Result := -1
					-- sbyte is left-right index, but search is right-left
					sbyte := sbyte - 1
					-- sbit is no longer relevant
				else
					-- Found one
					Result := pos + ((length - sbyte) * 8)
				end
			end
			if Result = -1 then
				positions := bit_to_positions (ep)
				ebyte := positions [1]
				ebit := positions [2]
				if ebit /= 7 then
					-- End position is not on a byte boundary
					-- Loop through whole bytes first, then check partial
					-- byte if still unfound
					-- Octets, arranged most-to-least, will be traversed 
					-- least-to-most (back to front) to preserve ordering
					-- As such, the previous whole byte is 1 greater
					lim := ebyte + 1
				else
					lim := ebyte
				end
				--| Traverse the octets from least to most (back to front)
				--| lim is lower than sbyte and traversal should be from 
				--| sbyte to lim, descending
				from i := sbyte
				until i > lim or Result /= -1
				loop
					pos := lowest_unset_position_in_byte (i)
					if pos /= -1 then
						Result := pos + ((length - i) * 8)
					end
					i := i - 1
				end
				-- If no bit found yet, check any partial bytes at the end
				-- of the range
				if Result = -1 and lim /= ebyte then
					pos := lowest_unset_position_in_byte (ebyte)
					-- If pos=-1, then no bits in the end byte are unset
					-- If pos > ebit, then the first unset is after the end
					-- and therefore of no interest
					if pos /= -1 and pos <= ebit then
						Result := pos + ((length - ebyte) * 8)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	lowest_position_from_octet (idx: INTEGER_32; sf: BOOLEAN): INTEGER_32
			-- First 0-based position at octet at 'idx' or higher 
			-- octets, through end of map, with a state 'sf', where
			-- True denotes 'set' and False denotes 'unset'
			--
			-- Result is -1 if no matching bits are found.
		require
			valid_octet_index: idx > 0 and idx <= length
		local
			i: INTEGER_32
		do
			Result := -1
			from i := idx
			until i = 0 or Result >= 0
			loop
				Result := lowest_position_in_byte (i, sf)
				i := i - 1
			end
		end

	lowest_position_in_range (sp, ep: INTEGER_32; sf: BOOLEAN): INTEGER_32
			-- First 0-based position at or after position 'sp'
			-- and before or at position 'lim' with a state 'sf', where 
			-- True denotes 'set' and False denotes 'unset'
			--
			-- Result is -1 if no matching bits are found.
		require
			in_range: is_valid_bit_position (sp)
			end_in_range: is_valid_bit_position (ep)
		local
			i, pos, lim: INTEGER_32
			positions: like bit_to_positions
			sbyte, ebyte, sbit, ebit: INTEGER_32
		do
			Result := -1
			positions := bit_to_positions (sp)
			sbyte := positions [1]
			sbit := positions [2]
			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 7 is last bit in first byte and
				-- sp = 8 is first bit in second byte
				-- Process partial byte first

				pos := next_position_in_byte (sbyte, sbit, sf)
				pos := lowest_position_in_byte (sbyte, sf)
				if pos = -1 then
					Result := -1
					-- sbyte is left-right index, but search is right-left
					sbyte := sbyte - 1
					-- sbit is no longer relevant
				else
					-- Found one
					Result := pos + ((length - sbyte) * 8)
				end
			end
			if Result = -1 then
				positions := bit_to_positions (ep)
				ebyte := positions [1]
				ebit := positions [2]
				if ebit /= 7 then
					-- End position is not on a byte boundary
					-- Loop through whole bytes first, then check partial
					-- byte if still unfound
					-- Octets, arranged most-to-least, will be traversed 
					-- least-to-most (back to front) to preserve ordering
					-- As such, the previous whole byte is 1 greater
					lim := ebyte + 1
				else
					lim := ebyte
				end
				--| Traverse the octets from least to most (back to front)
				--| lim is lower than sbyte and traversal should be from 
				--| sbyte to lim, descending
				from i := sbyte
				until i > lim or Result /= -1
				loop
					pos := lowest_position_in_byte (i, sf)
					if pos /= -1 then
						Result := pos + ((length - i) * 8)
					end
					i := i - 1
				end
				-- If no bit found yet, check any partial bytes at the end
				-- of the range
				if Result = -1 and lim /= ebyte then
					pos := lowest_position_in_byte (ebyte, sf)
					-- If pos=-1, then no bits in the end byte are unset
					-- If pos > ebit, then the first unset is after the end
					-- and therefore of no interest
					if pos /= -1 and pos <= ebit then
						Result := pos + ((length - ebyte) * 8)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	lowest_set_position_in_byte (bn: INTEGER_32): INTEGER_32
			-- 0-based position in octet 'bn' of first '1' bit
			-- Result is -1 if no bits are set
		require
			in_range: bn > 0 and bn <= length
		do
			Result := lowest_bit_set (octets [bn])
		end

	--|--------------------------------------------------------------

	lowest_unset_position_in_byte (bn: INTEGER_32): INTEGER_32
			-- 0-based position in octet of first '0' bit
			-- Result is -1 if no bits are unset (all are set)
		require
			in_range: bn > 0 and bn <= length
		do
			Result := lowest_bit_unset (octets [bn])
		end

	--|--------------------------------------------------------------

	lowest_position_in_byte (bn: INTEGER_32; sf: BOOLEAN): INTEGER_32
			-- 0-based position in octet at'bn', of first bit matching 
			-- 'sf' where True denotes 'set' and False denotes 'unset'
			-- Result is -1 if no matching bits are found
		require
			in_range: bn > 0 and bn <= length
		do
			if sf then
				Result := lowest_bit_set (octets [bn])
			else
				Result := lowest_bit_unset (octets [bn])
			end
		end

	--|--------------------------------------------------------------

	next_set_position_in_byte (bn, sb: INTEGER_32): INTEGER_32
			-- 0-based position of first '1' bit AFTER bit position 'sb
			-- in byte 'bn'
			-- Result is -1 if no bits are set
		require
			in_range: bn > 0 and bn <= length
			valid_bit_pos: sb >= 0 and sb < 8
		do
			Result := next_position_in_byte (bn, sb, True)
		end

	--|--------------------------------------------------------------

	next_unset_position_in_byte (bn, sb: INTEGER_32): INTEGER_32
			-- One-based position of first '0' bit after bit position 'sb
			-- in byte 'bn'
			-- Result is -1 if no bits are unset (all bits are set)
		require
			in_range: bn > 0 and bn <= length
			valid_bit_pos: sb >= 0 and sb < 8
		do
			Result := next_position_in_byte (bn, sb, False)
		end

	--|--------------------------------------------------------------

	next_position_in_byte (bn, sb: INTEGER_32; sf: BOOLEAN): INTEGER_32
			-- 0-based position of first bit matching 'sf' AFTER bit 
			-- position 'sb' in octet nummber 'bn',
			-- where True denotes 'set' and False denotes 'unset'
			-- Result is -1 if no matching bits are found
		require
			in_range: bn > 0 and bn <= length
			valid_bit_pos: sb >= 0 and sb < 8
		local
			bi: INTEGER_32
			byte, bm: NATURAL_8
			bf: BOOLEAN
		do
			Result := -1
			if not sf then
				bm := 0xff
			end
			byte := octets [bn]
			if byte /= bm then
				-- Has at least one matching bit, so worth a look
				-- Bits are numbered right to left
				from bi := sb
				until bi > 7 or Result /= -1
				loop
					bf := not byte.bit_test (bi)
					if bf = sf then
						-- Found a matching bit
						Result := bi
					end
					bi := bi + 1
				end
			end
		end

	--|--------------------------------------------------------------

	highest_set_position_in_byte (bn: INTEGER_32): INTEGER_32
			-- 0-based position in byte of highest '1' bit
			-- Result is -1 if no bits are set
		require
			in_range: bn > 0 and bn <= length
		do
			Result := highest_bit_set (octets [bn])
		end

	--|--------------------------------------------------------------

	highest_unset_position_in_byte (bn: INTEGER_32): INTEGER_32
			-- 0-based position in byte of highest '0' bit
			-- Result is -1 if no bits are set
		require
			in_range: bn > 0 and bn <= length
		do
			Result := highest_bit_unset (octets [bn])
		end

	--|--------------------------------------------------------------

	highest_set_position: INTEGER_32
			-- Highest bit position that is set
			-- Result is -1 if no bits are set
		local
			i, lim, pos: INTEGER_32
		do
			Result := -1
			lim := length
			from i := 1
			until i > lim or Result /= -1
			loop
				pos := highest_set_position_in_byte (i)
				if pos /= -1 then
					Result := pos + ((length - i) * 8)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	highest_unset_position: INTEGER_32
			-- Highest bit position unset
			-- Result is -1 if no bits are unset (all are set)
		local
			i, lim, pos: INTEGER_32
		do
			Result := -1
			lim := length
			from i := 1
			until i > lim or Result /= -1
			loop
				pos := highest_unset_position_in_byte (i)
				if pos /= -1 then
					Result := pos + ((length - i) * 8)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	bit_is_set (v: INTEGER_32): BOOLEAN
			-- is the given 1-based position set?
		local
			cpos, bpos: INTEGER_32
			positions: like bit_to_positions
		do
			positions := bit_to_positions (v)
			cpos := positions [1]
			bpos := positions [2]
			Result := octets [cpos].bit_test (bpos)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_bit (v: INTEGER_32)
			-- Set the 'v-th' 1-based position in the map
		require
			in_range: is_valid_bit_position (v)
		do
			set_position_value (v, True)
		ensure
			is_set: bit_is_set (v)
		end

	--|--------------------------------------------------------------

	unset_bit (v: INTEGER_32)
			-- Unset the 'v-th' 1-based position in the map
		require
			in_range: is_valid_bit_position (v)
		do
			set_position_value (v, False)
		ensure
			is_set: not bit_is_set (v)
		end

	--|--------------------------------------------------------------

	grow (v: INTEGER_32)
			-- Increase the size of Current to 'v'
		require
			valid_size: v > 0
			larger: v > count
		local
			ts: like octets
		do
			ts := octets
			count := even_8_multiple (v)
			create octets.make_filled (0, 1, count)
			octets.subcopy (ts, 1, length, length + 1)
			length := count // 8
		end

	--|--------------------------------------------------------------

	clear_all
			-- Set all bits to '0'
		do
			number_of_set_positions := 0
			octets.clear_all
		end

	--|--------------------------------------------------------------

	set_map_item (v: NATURAL_8; idx: INTEGER_32)
			-- Reset the values of map item[idx] to bits in 'v'
		local
			i, lim, boff: INTEGER_32
		do
			octets.put (v, idx)
			if v /= 0 then
				boff := (idx - 1) * 8
				lim := 8
				from i := 1
				until i > lim
				loop
					if v.bit_test (i-1) then
						set_bit (boff + i)
					end
					i := i + 1
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	bit_to_positions (v: INTEGER_32): ARRAY [INTEGER_32]
			-- The calculated octet index and bit position
			-- corresponding to the given bit.
			--
			-- For example, '0' would be the first (zero-th) bit of the 
			-- first byte (byte #0) in the map and would have a character 
			-- (octet) position of (count - 0), and a bit position of 0
			-- (bit positions can also be used as offsets)
			--
			-- Example 2: 0-based bit 19 would lie within the 3rd least 
			-- significant byte (0-based byte #2 from least significant)
			-- because 19 // 8 = 2+.
			-- In a 64 bit map (having 8 bytes), that translates to
			-- a 1-based octet index (from left, or most significant) of
			-- (8 - 2) = 6
			-- It would correspond then to 0-based bit #3 in that octet
			-- because 19 \\ 8 = 3.
			--
			-- Example 3: 0-based bit 47 would lie within the 6th least 
			-- significant byte (0-based byte #5 from least significant)
			-- because 47 // 8 = 5+.
			-- In a 64 bit map (having 8 bytes), that translates to
			-- a 1-based octet index (from left, or most significant) of
			-- (8 - 5) = 3
			-- It would correspond then to 0-based bit #7 in that byte
			-- because 47 \\ 8 = 7.
			--
			-- Example 4: 0-based bit 48 would lie within the 7th least 
			-- significant byte (0-based byte #6 from least significant)
			-- because 48 // 8 = 6.
			-- In a 64 bit map (having 8 bytes), that translates to
			-- a 1-based octet index (from left, or most significant) of
			-- (8 - 6) = 2
			-- It would correspond then to 0-based bit #0 in that byte
			-- because 48 \\ 8 = 0.
		require
			in_range: is_valid_bit_position (v)
		local
			bytepos, bitpos: INTEGER_32
		do
			bytepos := v // 8
			bitpos := v \\ 8
			Result := << length - bytepos, bitpos >>
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	bit_to_octet_index (v: INTEGER_32): INTEGER_32
			-- The calculated octet index corresponding to the given bit 
			-- position.
			-- Octets are arranged from most to least significant. so 
			-- for example, bit '1' would be in the 'first' octet (index 
			-- = length), bit '8' would be in the 'second' byte (length 
			-- - 1) etc.
		require
			in_range: is_valid_bit_position (v)
		do
			Result := length - (v // 8)
		end

	--|--------------------------------------------------------------

	positions_to_bit (v: ARRAY [INTEGER_32]): INTEGER_32
			-- Bit number corresponding to the given byte and bit positions
		require
			exists: v /= Void
			valid_positions: v.count = 2
			valid_byte: v.item (1) <= length
			valid_bit: v.item (2) >= 0 and v.item (2) <= 7
		local
			cval: INTEGER_32
		do
			--| v[1] is 1-based octet index
			--| v[2] is 0-based bit within octet
			cval := (length - v [1]) * 8
			Result := cval + v [2]
		ensure
			in_range: is_valid_bit_position (Result)
		end

	--|--------------------------------------------------------------

	set_position_value (i: INTEGER_32; v: BOOLEAN)
			-- Set the 'i-th' bit in the map to 'v' (True or False)
		require
			in_range: is_valid_bit_position (i)
		local
			cpos, bpos: INTEGER_32
			positions: like bit_to_positions
			byte: NATURAL_8
		do
			positions := bit_to_positions (i)
			cpos := positions [1]
			bpos := positions [2]
			byte := octets [cpos]
			if byte.bit_test (bpos) /= v then
				-- Bit state is changing
				byte := byte.set_bit (v, bpos)
				octets.put (byte, cpos)
				if v then
					number_of_set_positions := number_of_set_positions + 1
				else
					number_of_set_positions := number_of_set_positions - 1
				end
			end
		ensure
			is_set: bit_is_set (i) = v
			not_counted_on_same: ((old bit_is_set (i)) = bit_is_set (i)) implies
				number_of_set_positions = old number_of_set_positions
			count_on_set: (v and ((old bit_is_set (i)) /= bit_is_set (i))) implies
				number_of_set_positions = old number_of_set_positions + 1
			count_on_unset: ((not v) and ((old bit_is_set (i)) /= bit_is_set (i)))
				implies number_of_set_positions = old number_of_set_positions - 1
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	hex_byte_to_natural_8 (v: STRING_8): NATURAL_8
		require
			exists: v /= Void and v.count = 2
			is_hex: v.item (1).is_hexa_digit and v.item (2).is_hexa_digit
		local
			c1, c2: CHARACTER
		do
			c1 := v.item (1)
			c2 := v.item (2)
			Result := hex_digits_to_byte (c1, c2)
		end

	hex_digits_to_byte (c1, c2: CHARACTER): NATURAL_8
			-- Hex digits 'c1' and 'c2' (most and least significant) as 
			-- a byte (natural_8)
		require
			is_hex: c1.is_hexa_digit and c2.is_hexa_digit
		local
			v1, v2: INTEGER_32
		do
			if c1.is_digit then
				v1 := c1.code - ('0').code
			else
				v1 := (c1.as_upper).code - ('A').code
			end
			v1 := v1 |<< 4
			if c2.is_digit then
				v2 := c2.code - ('0').code
			else
				v2 := (c2.as_upper).code - ('A').code
			end
			Result := (v1 + v2).as_natural_8
		end

	--|--------------------------------------------------------------
invariant
	has_octets: octets /= Void
	consistent_size: count = octets.count * 8

end -- class AEL_DS_BITMAP2

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 03-Nov-2012
--|     Created original as adaptation of existing AEL_DS_BITMAP
--|     Eiffel 7.1
--|     Removed all mid-point logic as it added little but confusion
--|     Reimplemented most searches to be flagged for set/unset
--|     Renamed routines to be more bitmap-like
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this class, create a new instance with a given MINIMUM 
--| number of bits.  Actual number of bits is rounded to next nearest 
--| even 8 bit boundary.
--|
--| All bits are unset on creation.
--|
--| Call 'set_bit' to set a given bit to non-zero and 'unset_bit'
--| to clear that given bit.
--| Call 'bit_is_set' with the desired bit position to see if that
--| bit position is currently set.
--|----------------------------------------------------------------------
