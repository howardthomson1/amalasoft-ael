--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A bitmap data structure of (more or less) arbitrary size.
--| N.B. This is NOT a picture; It is a map of bits!!
--|----------------------------------------------------------------------

class AEL_DS_BITMAP_64

inherit
	AEL_NUMERIC_INTEGER_ROUTINES
		redefine
			out
		end

create
	make, make_from_string, make_from_compact_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: NATURAL_64)
			-- Create a new bitmap with 'sz' positions
			-- 'sz' MUST be an even multiple of 8
		require
			valid_size: sz \\ 8 = 0
		do
			count := sz
			length := (count // 8).to_integer_32
			create map.make_filled (0, 1, length)
			if sz > 8 then
				midpoint := even_8_multiple_64 (sz // 2) + 1
				midpoint_byte := (length // 2)
				if midpoint_byte \\ 2 /= 0 then
					midpoint_byte := midpoint_byte + 1
				end
			end
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create and initialize Current from the given string
			-- of characters
			-- String should contain only '1' and '0' characters, but
			-- only '1' chars are considered; all others are treated as False
		local
			i, lim: INTEGER
		do
			lim := v.count
			make (even_8_multiple_64 (lim))
			from i := 1
			until i > lim
			loop
				if v.item (i) = '1' then
					set_position (i)
				end
				i := i + 1
			end
		ensure
			bits_equivalent: number_of_set_positions = v.occurrences ('1')
		end

	--|--------------------------------------------------------------

	make_from_compact_string (v: STRING)
			-- Create and initialize Current from the given compact string
			-- form
			-- String contains a size field (number of BITS), and then
			-- a ':' separator followed by zero or more hexadecimal 
			-- digits, each 0-padded pair representing a byte in the map.
			-- Bytes are arranged from LEAST significant to MOST
			-- significant, but bits within each byte are arranged from
			-- MOST to LEAST (ie bit 7 is leftmost).
			--
			-- For example, a 16-bit map with value (msb-to-lsb) of:
			--              0100000100001010
			-- (bit 15)  MSB^               ^LSB (bit 0)
			-- would be represented by 2 encoded bytes, each with
			-- 2 hexadecmal digits, as:
			--              00001010 01000001
			--              0   c    4   1    --> 0c41
		require
			valid: v /= Void and then not v.is_empty
			has_count: v.has (':')
		local
			i, j, lim, ep, len: INTEGER
			tn: NATURAL_8
			ts: STRING
		do
			lim := v.count
			ep := v.index_of (':', 1)
			ts := v.substring (1, ep - 1)
			len := ts.to_natural_64
			make (len)
			from
				i := ep + 1
				j := 1
			until i > lim
			loop
				tn := hex_byte_to_natural_8 (v.substring (i, i + 1))
				set_map_item (tn, j)
				i := i + 2
				j := j + 1
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	map: ARRAY [NATURAL_8]
			-- The actual map

	count: NATURAL_64
		-- Number of bits in map

	length: INTEGER
		-- Number of BYTES in map

	--|--------------------------------------------------------------

	midpoint: NATURAL_64
			-- Index in map of midpoint
			-- For 8 bit maps, midpoint is 0
			-- Midpoint is in the center of a byte
			-- when map uses odd number of bytes

	midpoint_byte: INTEGER
			-- Index in map of byte containing midpoint
			-- For 8 bit maps, midpoint_byte is 0

	number_of_set_positions: NATURAL_64
			-- Number of bit positions that are set
		do
			Result := bits_set_before_midpoint + bits_set_after_midpoint
		end

	--|--------------------------------------------------------------

	number_of_bits_set_before_position (p: NATURAL_64): NATURAL_64
			-- Number of bits set from start to the given bit position,
			-- EXCLUSIVE
		require
			in_range: is_valid_index (p)
		do
			if p /= 1 then
				if midpoint = 0 or p < midpoint then
					-- Count all the bits from start to p
					Result := number_of_sets_in_range (1, p-1)
				else
					-- P is in right half
					-- Need to count right half, then add known left half
					Result := number_of_sets_in_range (
						midpoint, p - 1) + bits_set_before_midpoint
				end
			end
		end

	--|--------------------------------------------------------------

	number_of_bits_set_from_position (p: NATURAL_64): NATURAL_64
			-- Number of bits set from the given bit position to the map end,
			-- INCLUSIVE
		require
			in_range: is_valid_index (p)
		do
			if midpoint = 0 then
				-- Count all the bits from p to end
				Result := number_of_sets_in_range (p, count)
			elseif p < midpoint then
				-- Need to count first half, then add known second half
				Result := number_of_sets_in_range (
					p, midpoint - 1) + bits_set_after_midpoint
			else
				-- Need to ignore first half, count from midpoint
				if p > ((count - midpoint) // 2) then
					-- Closer to end
					Result := number_of_sets_in_range (p, count)
				else
					-- Closer to midpoint
					Result := bits_set_after_midpoint -
						number_of_sets_in_range (midpoint, p)
				end
			end
		end

	--|--------------------------------------------------------------

	number_of_sets_in_range (s, p: NATURAL_64): NATURAL_64
			-- Number of bits set from position 's' to 'p', inclusive
		require
			in_range: is_valid_index (p) and is_valid_index (s)
		local
			i, lim: NATURAL_64
			sbyte, ebyte: NATURAL_64
			sbit, ebit: INTEGER
			positions: like index_to_positions
			byte: NATURAL_8
		do
			positions := index_to_positions (s)
			sbyte := positions.natural_64_item (1)
			sbit := positions.integer_item (2)
			positions := index_to_positions (p)
			ebyte := positions.natural_64_item (1)
			ebit := positions.integer_item (2)

			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 8 is last bit in first byte and
				-- sp = 9 is first bit in second byte
				-- Process partial byte first
				Result := Result + bits_set_in_byte (map.item (sbyte), sbit, 7)
				sbyte := sbyte + 1
			end
			if ebit /= 7 then
				-- End position is not on a byte boundary
				-- Loop through whole bytes first, then check partial
				-- byte if still unfound
				lim := ebyte  - 1
			else
				lim := ebyte
			end
			from i := sbyte
			until i > lim
			loop
				byte := map.item (i)
				if byte /= 0x00 then
					Result := Result + num_bits_set_8 (byte)
				end
				i := i + 1
			end
			-- Now check any partial bytes at the end of the range
			if lim /= ebyte then
				Result := Result + bits_set_in_byte (map.item (ebyte), 0, ebit)
			end
		end

	--|--------------------------------------------------------------

	bits_set_in_byte (byte: NATURAL_8; sp, ep: NATURAL_64): NATURAL_64
			-- Number of bits, between sp and ep (inclusive) in the given
			-- byte that are currently set
			-- sp and ep are 0-based
		require
			valid_start: sp >= 0 and sp <= 7
			valid_end: ep >= sp and ep <= 7
		local
			tb: NATURAL_8
		do
			if byte /= 0x00 then
				-- Has at least one set bit, so worth a look
				-- Bits are numbered right to left
				if sp = 0 and ep = 7 then
					-- Whole byte
					Result := num_bits_set_8 (byte)
				else
					tb := byte & bit_positions_to_mask (sp, ep)
					Result := num_bits_set_8 (tb)
				end
			end
		end

	bit_positions_to_mask (sp, ep: NATURAL_64): NATURAL_8
			-- Byte mask that contains ones only with in the bit range
			-- delimited by sp and ep (0-based), inclusive
			--
			-- To create an empty mask (why bother?), use an end position
			-- less than the start position
		require
			valid_start: sp >= 0 and sp <= 7
			valid_end: ep >= 0 and ep <= 7
		local
			i, lim, uno: NATURAL_8
		do
			uno := i.one
			lim := ep.to_natural_8
			from
				i := sp.to_natural_8
			until i > lim
			loop
				Result := Result | (uno |<< i)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	bits_set_before_midpoint: NATURAL_64
			-- Number of bits set between position 1 and the midpoint
			-- position, exclusive of the midpoint.
			-- For 8 bit maps, always 0

	bits_set_after_midpoint: NATURAL_64
			-- Number of bits set between the midpoint and the last
			-- position, INclusive of the midpoint

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (v: NATURAL_64): BOOLEAN
			-- Is the given number a valid index for this map?
		do
			Result := v <= count
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING
			-- String representation of bitmap (as a stream of 1s and 0s)
			--
			-- N.B. in String form, bits appear left to right, from least
			-- significant to most (1-8 per byte), not as addressed
			-- in memory.
		local
			i, lim: NATURAL_64
		do
			create Result.make (count)
			lim := length
			from i := 1
			until i > lim
			loop
				Result.append (byte_out (map.item (i)))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	byte_out (v: NATURAL_8): STRING
			-- String representation of the given byte
			-- (as a stream of 1s and 0s)
			--
			-- N.B. in String form, bits appear left to right, from least
			-- significant to most (1-8 per byte), not as addressed
			-- in memory.
		local
			i: INTEGER
		do
			create Result.make (8)
			from i := 0
			until i > 7
			loop
				if v.bit_test (i) then
					Result.extend ('1')
				else
					Result.extend ('0')
				end
				i := i + 1
			end
		ensure
			exists: Result /= Void
			filled: Result.count = 8
		end

	--|--------------------------------------------------------------

	compact_out: STRING
			-- String representation of bitmap as a size field followed 
			-- by a sequence of string-encoded bytes, each representing
			-- 8 bits of the  map.
			-- Bytes are arranged from LEAST significant to MOST
			-- significant, but bits within each byte are arranged from
			-- MOST to LEAST (ie bit 7 is leftmost).
			--
			-- For example, a 16-bit map with value (msb-to-lsb) of:
			--              0100000100001010
			-- (bit 15)  MSB^               ^LSB (bit 0)
			-- would be represented by 2 encoded bytes, each with
			-- 2 hexadecmal digits, as:
			--              00001010 01000001
			--              0   c    4   1    --> 0c41
			--
			-- N.B. This is a platform-independent representation and as 
			-- such even though a stream might appear to be an integer,
			-- it is not.
		local
			i, lim: NATURAL_64
		do
			create Result.make (8 + length * 2)
			lim := length
			Result.append_integer (count)
			Result.extend (':')
			from i := 1
			until i > lim
			loop
				Result.append (byte_compact_out (map.item (i)))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	byte_compact_out (v: NATURAL_8): STRING
			-- String representation of the given byte as a pair of
			-- hexadecimal digits
		do
			Result := v.to_hex_string
			if Result.count < 2 then
				Result := "0" + Result
			end
		ensure
			exists: Result /= Void
			filled: Result.count = 2
		end

--|========================================================================
feature -- Access
--|========================================================================

	bits_set: LINKED_LIST [NATURAL_64]
			-- List of 1-based bit positions that are set
			-- See index_to_positions comment for more information
		local
			i, lim, bi: NATURAL_64
			byte: NATURAL_8
		do
			create Result.make
			lim := length
			from i := 1
			until i > lim
			loop
				byte := map.item (i)
				if byte /= 0x00 then
					-- Has a set bit
					from bi := 7
					until bi < 0
					loop
						if byte.bit_test (bi) then
							-- Found a set number
							Result.extend (positions_to_index (<< i, bi >>))
						end
						bi := bi - 1
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	first_unset_position: NATURAL_64
			-- First available (unset) 1-based position
		local
			lim, sp, bph: NATURAL_64
		do
			sp := 1
			bph := count - midpoint
			if midpoint = 0 then
				lim := count
			elseif bits_set_before_midpoint = 0 then
				Result := 1
				lim := 0
			elseif bits_set_before_midpoint = (bph) then
				-- All left bits are set; don't bother with the left side
				sp := midpoint
				if bits_set_after_midpoint = (bph) then
					-- All right bits are set; don't bother with the left
					-- side
					lim := 0
				end
			else
				lim := count
			end
			if lim /= 0 then
				Result := first_unset_position_in_range (1, lim)
			end
		end

	--|--------------------------------------------------------------

	first_set_position: NATURAL_64
			-- First 1-based position that is set; 0 if none is set
		local
			lim, sp: NATURAL_64
		do
			sp := 1
			if bits_set_before_midpoint = 0 then
				if bits_set_after_midpoint = 0 then
					-- None is set, don't bother looking
					lim := 0
				else
					-- Start at midpoint
					sp := midpoint.max (1)
					lim := count
				end
			else
				if midpoint = 0 then
					lim := count
				else
					lim := midpoint - 1
				end
			end
			if lim /= 0 then
				Result := first_set_position_in_range (sp, lim)
			end
		end

	--|--------------------------------------------------------------

	next_set_position (v: NATURAL_64): NATURAL_64
			-- First 1-based position that is set at 'v' or after;
			-- 0 if none is set
		require
			in_range: is_valid_index (v)
		local
			lim, sp: NATURAL_64
		do
			sp := v
			lim := count
			if v >= midpoint then
				if bits_set_after_midpoint = 0 then
					-- None is set, don't bother looking
					lim := 0
				end
			elseif bits_set_before_midpoint = 0 then
				sp := midpoint
				if bits_set_after_midpoint = 0 then
					-- None is set, don't bother looking
					lim := 0
				end
			end
			if lim /= 0 then
				Result := first_set_position_in_range (sp, lim)
			end
		end

	--|--------------------------------------------------------------

	next_unset_position (v: NATURAL_64): NATURAL_64
			-- First 1-based position that is unset at 'v' or after;
			-- 0 if all are set
		require
			in_range: is_valid_index (v)
		local
			lim, sp: NATURAL_64
		do
			sp := v
			lim := count
			if v >= midpoint then
				if bits_set_after_midpoint = 0 then
					-- None is set, don't bother looking
					Result := v
				end
			elseif bits_set_before_midpoint = 0 then
				sp := v
				if bits_set_after_midpoint = 0 then
					lim := midpoint - 1
				end
			end
			if lim /= 0 then
				Result := first_unset_position_in_range (sp, lim)
			end
		end

	--|--------------------------------------------------------------

	first_set_position_in_range (sp, ep: NATURAL_64): NATURAL_64
			-- First 1-based position at or after position 'sp'
			-- and before or at position 'ep' with a value of '1'
		require
			in_range: is_valid_index (sp)
			end_in_range: ep = 0 or is_valid_index (ep)
		local
			i, spos, lim, pos: NATURAL_64
			positions: like index_to_positions
			sbyte, ebyte, sbit, ebit: NATURAL_64
		do
			positions := index_to_positions (sp)
			sbyte := positions.natural_64_item (1)
			sbit := positions.integer_item (2)
			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 8 is last bit in first byte and
				-- sp = 9 is first bit in second byte
				-- Process partial byte first

				pos := next_set_position_in_byte (sbyte, sbit)
				-- pos is one-based, sbit is zero-based!!!!
				-- e.g. pos 2 is set, sbit = 1; they denote same bit
				-- If pos=0, then no bits past sbit in the start byte are set
				if pos /= 0 then
					Result := pos + ((sbyte-1) * 8)
				else
					-- Skip to the next byte, if any
					Result := 0
					spos := sbyte + 1
				end
			else
				spos := sbyte
			end
			if Result = 0 then
				positions := index_to_positions (ep)
				ebyte := positions.natural_64_item (1)
				ebit := positions.integer_item (2)
				if ebit /= 7 then
					-- End position is not on a byte boundary
					-- Loop through whole bytes first, then check partial
					-- byte if still unfound
					lim := ebyte  - 1
				else
					lim := ebyte
				end
				from i := spos
				until i > lim or Result /= 0
				loop
					pos := first_set_position_in_byte (i)
					-- pos is one-based
					if pos /= 0 then
						Result := pos + ((i-1) * 8)
					end
					i := i + 1
				end
				-- Now check any partial bytes at the end of the range
				if Result = 0 and lim /= ebyte then
					pos := first_set_position_in_byte (ebyte)
					-- pos is one-based, ebit is zero-based!!!!
					-- e.g. pos 2 is set, ebit = 1; they denote same bit
					-- If pos=0, then no bits in the end byte are set
					-- If (pos-1) > ebit, then the first set is after the end
					if pos /= 0 and (pos - 1) <= ebit then
						Result := pos + ((ebyte-1) * 8)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	i_th_set_position (i: NATURAL_64) : NATURAL_64
			-- 1-based position of the i_th bit that is set;
			-- 0 if none or fewer than 'i' are set
		local
			sp: NATURAL_64
		do
			from sp := first_set_position
			until sp = i or sp > count
			loop
				sp := next_set_position (sp+1)
			end
		end

	--|--------------------------------------------------------------

	first_unset_position_in_range (sp, ep: NATURAL_64): NATURAL_64
			-- First 1-based position at or after position 'sp'
			-- and before or at position 'lim' with a value of '0'
		require
			in_range: is_valid_index (sp)
			end_in_range: ep = 0 or is_valid_index (ep)
		local
			i, pos, lim, spos: NATURAL_64
			positions: like index_to_positions
			sbyte, ebyte, sbit, ebit: NATURAL_64
		do
			positions := index_to_positions (sp)
			sbyte := positions.natural_64_item (1)
			sbit := positions.integer_item (2)
			-- Check for partial byte start position
			if sbit /= 0 then
				-- Start position is not on a byte boundary
				-- i.e. sp = 8 is last bit in first byte and
				-- sp = 9 is first bit in second byte
				-- Process partial byte first

				pos := next_unset_position_in_byte (sbyte, sbit)
				pos := first_unset_position_in_byte (sbyte)
				if pos = 0 then
					Result := 0
					spos := sbyte + 1
				else
					Result := pos + (sbyte * 8)
				end
			else
				spos := sbyte
			end
			if Result = 0 then
				positions := index_to_positions (ep)
				ebyte := positions.natural_64_item (1)
				ebit := positions.integer_item (2)
				if ebit /= 7 then
					-- End position is not on a byte boundary
					-- Loop through whole bytes first, then check partial
					-- byte if still unfound
					lim := ebyte  - 1
				else
					lim := ebyte
				end
				from i := spos
				until i > lim or Result /= 0
				loop
					pos := first_unset_position_in_byte (i)
					if pos /= 0 then
						Result := pos + ((i-1) * 8)
					end
					i := i + 1
				end
				-- Now check any partial bytes at the end of the range
				if Result = 0 and lim /= ebyte then
					pos := first_unset_position_in_byte (ebyte)
					if pos /= 0 and pos <= ebit then
						Result := pos + ((ebyte-1) * 8)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	first_set_position_in_byte (bn: NATURAL_64): NATURAL_64
			-- One-based position in byte of first '1' bit
		require
			in_range: bn > 0 and bn <= length
		do
			Result := lowest_bit_set (map.item (bn)) + 1
		end

	--|--------------------------------------------------------------

	first_unset_position_in_byte (bn: NATURAL_64): NATURAL_64
			-- One-based position in byte of first '0' bit
		require
			in_range: bn > 0 and bn <= length
		do
			Result := lowest_bit_unset (map.item (bn)) + 1
		end

	--|--------------------------------------------------------------

	next_set_position_in_byte (bn, sb: NATURAL_64): NATURAL_64
			-- One-based position of first '1' bit after bit position 'sb
			-- in byte 'bn'
		require
			in_range: bn > 0 and bn <= length
			valid_bit_pos: sb > 0 and sb < 8
		local
			bi: NATURAL_64
			byte: NATURAL_8
		do
			byte := map.item (bn)
			if byte /= 0x00 then
				-- Has a set bit, so worth a look
				-- Bits are numbered right to left
				from bi := sb
				until bi > 7 or (Result /= 0)
				loop
					if byte.bit_test (bi) then
						-- Found a set number
						-- Result is one-based position
						Result := bi + 1
					end
					bi := bi + 1
				end
			end
		end

	--|--------------------------------------------------------------

	next_unset_position_in_byte (bn, sb: NATURAL_64): NATURAL_64
			-- One-based position of first '0' bit after bit position 'sb
			-- in byte 'bn'
		require
			in_range: bn > 0 and bn <= length
			valid_bit_pos: sb > 0 and sb < 8
		local
			bi: NATURAL_64
			byte: NATURAL_8
		do
			byte := map.item (bn)
			if byte /= 0xff then
				-- Has an unset bit, so worth a look
				-- Bits are numbered right to left
				from bi := sb
				until bi > 7 or (Result /= 0)
				loop
					if not byte.bit_test (bi) then
						-- Found an unset number
						-- Result is one-based position
						Result := bi + 1
					end
					bi := bi + 1
				end
			end
		end

	--|--------------------------------------------------------------

	highest_set_position_in_byte (bn: NATURAL_64): NATURAL_64
			-- One-based position in byte of highest '1' bit
			-- Zero if none is set
		require
			in_range: bn > 0 and bn <= length
		do
			Result := highest_bit_set (map.item (bn)) + 1
		end

	--|--------------------------------------------------------------

	highest_unset_position_in_byte (bn: NATURAL_64): NATURAL_64
			-- One-based position in byte of highest '0' bit
		require
			in_range: bn > 0 and bn <= length
		do
			Result := highest_bit_unset (map.item (bn)) + 1
		end

	--|--------------------------------------------------------------

	highest_set_position: NATURAL_64
			-- Highest position number set
		local
			i, lim, pos, sp, mpb: NATURAL_64
		do
			mpb := midpoint_byte
			if bits_set_after_midpoint /= 0 then
				-- There are some set in the right side
				sp := mpb
				lim := length
			elseif bits_set_before_midpoint = 0 then
				-- There are none set; don't even look
				sp := length
				lim := 0
			else
				-- There are some set in the left side only
				sp := 0
				lim := mpb - 1
			end
			from i := lim
			until i < sp or Result /= 0
			loop
				pos := highest_set_position_in_byte (i)
				if pos /= 0 then
					Result := pos + ((i-1) * 8)
				end
				i := i - 1
			end
		end

	--|--------------------------------------------------------------

	highest_unset_position: NATURAL_64
			-- Highest position number unset
		local
			i, lim, pos, sp, mpb: NATURAL_64
		do
			mpb := midpoint_byte
			if bits_set_after_midpoint = 0 then
				-- There are no bits set on right side,
				-- so highest unset is highest bit
				Result := count
			elseif bits_set_after_midpoint = (count - midpoint) then
				-- There are no unset bits on the right side
				-- Start with the left side
				if bits_set_before_midpoint = 0 then
					if midpoint /= 0 then
						Result := midpoint - 1
					else
						Result := 8
					end
				elseif bits_set_after_midpoint = (count - midpoint) then
					-- There are no unset bits on the left side
					Result := 0
					sp := 0
				else
					-- There are some unset bits on the left side
					sp := (mpb - 1).max (1)
				end
			else
				-- There are some bits set on the right side
				sp := mpb
				lim := length
			end
			from i := lim
			until i < sp or Result /= 0
			loop
				pos := highest_unset_position_in_byte (i)
				if pos /= 0 then
					Result := pos + ((i-1) * 8)
				end
				i := i - 1
			end
		end

	--|--------------------------------------------------------------

	position_is_set (v: NATURAL_64): BOOLEAN
			-- is the given 1-based position set?
		local
			cpos, bpos: NATURAL_64
			positions: like index_to_positions
			byte: NATURAL_8
		do
			if v < midpoint and bits_set_before_midpoint = 0 then
				-- Don't bother; it's not set
			elseif v >= midpoint and bits_set_after_midpoint = 0 then
				-- ditto
			else
				positions := index_to_positions (v)
				cpos := positions.natural_64_item (1)
				bpos := positions.integer_item (2)
				byte := map.item (cpos)
				Result := byte.bit_test (bpos)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_position (v: NATURAL_64)
			-- Set the 'v-th' 1-based position in the map
		require
			in_range: is_valid_index (v)
		do
			if not position_is_set (v) then
				if v < midpoint then
					bits_set_before_midpoint := bits_set_before_midpoint + 1
				else
					bits_set_after_midpoint := bits_set_after_midpoint + 1
				end
			end
			set_position_value (v, True)
		ensure
			is_set: position_is_set (v)
		end

	--|--------------------------------------------------------------

	unset_position (v: NATURAL_64)
			-- Unset the 'v-th' 1-based position in the map
		require
			in_range: is_valid_index (v)
		do
			if position_is_set (v) then
				if v < midpoint then
					bits_set_before_midpoint := bits_set_before_midpoint - 1
				else
					bits_set_after_midpoint := bits_set_after_midpoint - 1
				end
			end
			set_position_value (v, False)
		ensure
			is_set: not position_is_set (v)
		end

	--|--------------------------------------------------------------

	grow (v: NATURAL_64)
			-- Increase the size of Current to 'v'
		require
			valid_size: v > 0 and (v \\8) = 0
			larger: v > count
		local
			ts: like map
			mp, bs: NATURAL_64
		do
			midpoint := even_8_multiple (v // 2) + 1
			if midpoint = 0 then
				-- Previous size had to be 8 bits and new size
				-- must be at least 16, so all previously set
				-- bits are now left of midpoint
				bits_set_before_midpoint := bits_set_after_midpoint
				bits_set_after_midpoint := 0
			else
				-- Midpoint moves, and so midpoint count changes
				-- Be smart about recalculating left/right counts
				bs := number_of_sets_in_range (midpoint, mp - 1)
				bits_set_before_midpoint := bits_set_before_midpoint + bs
				bits_set_after_midpoint := bits_set_after_midpoint - bs
			end
			ts := map
			create map.make_filled (0, 1, v)
			map.subcopy (ts, 1, length, length + 1)

			count := v
			length := count // 8
			midpoint := mp
			midpoint_byte := (length // 2)
			if midpoint_byte \\ 2 /= 0 then
				midpoint_byte := midpoint_byte + 1
			end
		end

	--|--------------------------------------------------------------

	clear_all
			-- Set all bits to '0'
		do
			bits_set_before_midpoint := 0
			bits_set_after_midpoint := 0
			map.clear_all
		end

	--|--------------------------------------------------------------

	set_map_item (v: NATURAL_8; idx: NATURAL_64)
			-- Reset the values of map item[idx] to bits in 'v'
		local
			i, lim, boff: NATURAL_64
		do
			map.put (v, idx)
			if v /= 0 then
				boff := (idx - 1) * 8
				lim := 8
				from i := 1
				until i > lim
				loop
					if v.bit_test (i-1) then
						set_position (boff + i)
					end
					i := i + 1
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	index_to_positions (v: NATURAL_64): TUPLE [NATURAL_64, INTEGER]
			-- The calculated byte and bit positions (in the map)
			-- corresponding to the given number.
			-- For example, '1' would be the first bit of the first
			-- byte in the map and would have a character (byte)
			-- position of 1, and a bit position of 0 (bit positions are
			-- offsets)
			-- Example 2: '512' would be the last bit of the 63rd byte
			-- in the map and would have a character (byte) position of
			-- 63, and a bit position of 7
		require
			in_range: is_valid_index (v)
		local
			cpos, bpos: NATURAL_64
		do
			cpos := (v // 8) + 1
			bpos := (v \\ 8)
			if bpos = 0 then
				-- Even mod indicates highest bit of previous byte
				cpos := cpos - 1
				bpos := 7
			else
				-- Normalize bit position to zero-based
				bpos := bpos - 1
			end
			Result := [ cpos, bpos ]
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	index_to_byte_position (v: NATURAL_64): NATURAL_64
			-- The calculated byte  positions (in the map)
			-- corresponding to the given position.
			-- For example, '1' would be in the first byte,
			-- '8' would be in the second byte, etc.
		require
			in_range: is_valid_index (v)
		do
			Result := (v // 8)
			if (v \\ 8) /= 0 then
				Result := Result + 1
			end
		end

	--|--------------------------------------------------------------

	positions_to_index (v: ARRAY [NATURAL_64]): NATURAL_64
			-- Index corresponding to the given byte and bit positions.
		require
			exists: v /= Void
			valid_positions: v.count = 2
			valid_byte: v.item (1) <= length
			valid_bit: v.item (2) >= 0 and v.item (2) <= 7
		local
			cval, bval: NATURAL_64
		do
			cval := (v.item (1) - 1) * 8
			bval := v.item (2) + 1
			Result := cval + bval
		ensure
			in_range: Result > 0 and Result <= count
		end

	--|--------------------------------------------------------------

	set_position_value (i: NATURAL_64; v: BOOLEAN)
			-- Set the 'i-th' bit in the map to 'v' (True or False)
		require
			in_range: is_valid_index (i)
		local
			cpos: NATURAL_64
			bpos: INTEGER
			positions: like index_to_positions
			byte: NATURAL_8
		do
			positions := index_to_positions (i)
			cpos := positions.natural_64_item (1)
			bpos := positions.integer_item (2)
			byte := map.item (cpos)
			byte := byte.set_bit (v, bpos)
			map.put (byte, cpos)
		ensure
			is_set: position_is_set (i) = v
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	hex_byte_to_natural_8 (v: STRING): NATURAL_8
		require
			exists: v /= Void and v.count = 2
			is_hex: v.item (1).is_hexa_digit and v.item (2).is_hexa_digit
		local
			c1, c2: CHARACTER
			v1, v2: INTEGER
		do
			c1 := v.item (1)
			c2 := v.item (2)
			if c1.is_digit then
				v1 := c1.code - ('0').code
			else
				v1 := (c1.as_upper).code - ('A').code
			end
			v1 := v1 |<< 4
			if c2.is_digit then
				v2 := c2.code - ('0').code
			else
				v2 := (c2.as_upper).code - ('A').code
			end
			Result := (v1 + v2).as_natural_8
		end

	--|--------------------------------------------------------------
invariant
	has_map: map /= Void
	consistent_size: count = map.count * 8

end -- class AEL_DS_BITMAP_64

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Oct-2012
--|     Adapted from exiting AEL_DS_BITMAP (V005)
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this class, create a new instance with a given number of 
--| bits.
--| All bits are unset on creation.
--| Call set_position to set a given bit to non-zero and 
--| unset_position to clear that given bit.
--| Call position_is_set with the desired bit position to see if that
--| bit position is current set.
--|----------------------------------------------------------------------
