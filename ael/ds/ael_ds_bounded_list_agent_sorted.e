--|----------------------------------------------------------------------
--| Copyright (c) 1995-2011, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A doubley-linked list supporting sorting using client-defined agents.
--| Items are not constrained to COMPARABLE
--| List can be bounded such that a maximum count is not exceeded 
--| (though there is no special minimum count)
--|----------------------------------------------------------------------

class AEL_DS_BOUNDED_LIST_AGENT_SORTED [G]

inherit
	AEL_DS_LIST_AGENT_SORTED [G]
		rename
			extend as adl_extend,
			merge as adl_merge
--,
--			make_sublist as adl_make_sublist
		redefine
			new_chain
		end

create
	make, make_default, make_bounded, make_multi

create {TWO_WAY_LIST}
	make_sublist

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_bounded (cf: like dummy_comparison_agent; sz: INTEGER)
			-- Create Current with comparison function 'cf' and a
			-- maximum count of 'sz'
		require
			valid_size: sz > 0
		do
			maximum_count := sz
			make (cf)
		end

 --|========================================================================
feature -- Element change
 --|========================================================================

	extend (v: like item)
			-- Put `v' at proper position in list.
			-- The cursor ends up on the newly inserted
			-- item.
			-- If bounded and insertion would be before last item, then 
			-- it maximum count would be exceeded, remove the last item.
			-- If bounded and insertion would be at end, do not insert
		local
			oc: like cursor
		do
			search_after (v)
			if is_bounded and islast then
				-- Do not insert
			else
				-- Make room if necessary
				if count = maximum_count then
					oc := cursor
					finish
					remove
					go_to (oc)
				end
				put_left (v)
				back
			end
		ensure
	 		remains_sorted: (old is_sorted) implies is_sorted
			bound_intact: maximum_count = old maximum_count
		end

	--|--------------------------------------------------------------

	merge (other: like Current)
			-- Add all items from `other' at their proper positions.
		local
			tc: INTEGER
		do
			if is_bounded then
				tc := maximum_count
				maximum_count := count + other.count
			else
				tc := count + other.count
			end
			from
				other.start
			until
				other.off
			loop
				extend (other.item)
				other.forth
			end
			if maximum_count /= 0 and count > tc then
				-- Remove all items > defined max
				from go_i_th (tc)
				until islast
				loop
					remove_right
				end
				maximum_count := tc
			end
		ensure
	 		remains_sorted: (old is_sorted) implies is_sorted
			bound_intact: maximum_count = old maximum_count
		end

 --|========================================================================
feature -- Status report
 --|========================================================================

	maximum_count: INTEGER
			-- Largest count permitted for Current (if non-zero)

	is_bounded: BOOLEAN
			-- Does Current has a defined maximum count?
		do
			Result := maximum_count /= 0
		end

 --|========================================================================
feature {AEL_DS_BOUNDED_LIST_AGENT_SORTED} -- Implementation
 --|========================================================================

	new_chain: like Current
			-- A newly created instance of the same type.
			-- This feature may be redefined in descendants so as to
			-- produce an adequately allocated and initialized object.
		do
			if is_bounded then
				create Result.make_bounded (comparison_function, maximum_count)
			else
				Result := Precursor
			end
		end

	--RFO make_sublist (
	--RFO 	first_item, last_item: like first_element; n: INTEGER)
	--RFO 		-- Create sublist
	--RFO 	do
	--RFO 		twl_make_sublist (first_item, last_item, n)
	--RFO 	end

	--|--------------------------------------------------------------
invariant
	valid_max: maximum_count >= 0
	bound_intact: is_bounded implies count <= maximum_count

end -- class AEL_DS_BOUNDED_LIST_AGENT_SORTED

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 000 08-Feb-2011
--|     Adapted from existing AEL_DS_LIST_AGENT_SORTED, built and 
--|     tested with Eiffel 6.6
--|----------------------------------------------------------------------
--| How-to
--| Create with an agent for comparing items, and optional upper limit
--| Use as you would a SORTED_TWO_WAY_LIST
--|----------------------------------------------------------------------
