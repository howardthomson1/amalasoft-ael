--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An octet stream; i.e. a sequence of unsigned 8 bit integers
--|----------------------------------------------------------------------

class AEL_DS_BYTE_STREAM

inherit
	ARRAY [NATURAL_8]
		rename
			make as array_make,
			make_filled as array_make_filled
		redefine
			subarray, out
		end

create
	make, make_from_string, make_from_substring,
	make_from_managed_pointer, make_filled

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
			-- Create Current with the 'sz' capacity
		require
			valid_size: sz >= 0
		do
			array_make_filled (0, 0, (sz - 1).max (0))
		end

	--|--------------------------------------------------------------

	make_filled (v: like item; sz: INTEGER)
			-- Create Current with 'sz' capacity and filled with 'sz' 
			-- instances of 'v'
		do
			array_make_filled (v, 0, (sz -1).max (0))
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create Current from the given string
		require
			exists: v /= Void
		do
			make_from_substring (v, 1, v.count)
		ensure
			same_as_string: v.is_equal (to_string)
		end

	--|--------------------------------------------------------------

	make_from_substring (v: STRING; sp, ep: INTEGER)
			-- Create Current from the given string
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep >= sp and ep <= v.count
		local
			i, j, lim: INTEGER
		do
			make ((ep - sp) + 1)
			lim := upper
			from
				i := 0
				j := sp
			until
				i > lim
			loop
				put (v.item (j).code.as_natural_8, i)
				i := i + 1
				j := j + 1
			end
		ensure
			same_as_string: v.substring (sp, ep).is_equal (to_string)
		end

	--|--------------------------------------------------------------

	make_from_managed_pointer (mp: MANAGED_POINTER; sz: INTEGER)
			-- Create Current with 'sz' capacity, from the managed
			-- pointer 'mp'
		require
			exists: mp /= Void
			valid_size: sz >= 0
		local
			i: INTEGER
		do
			make (sz)
			from i := 0
			until i >= sz
			loop
				put (mp.read_natural_8 (i), i)
				i := i + 1
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	to_string: STRING
			-- Content of stream as a string of 8-bit characters
		local
			i: INTEGER
		do
			create Result.make (count)
			from i := 0
			until i > upper
			loop
				Result.extend (item (i).to_character_8)
				i := i + 1
			end
		ensure
			exists: Result /= Void
			same_length: Result.count = count
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String (printable) representation of of stream
			-- Representation is in form of 2 hex digits each byte
		local
			i: INTEGER
		do
			create Result.make (count)
			from i := 0
			until i > upper
			loop
				Result.append (item (i).to_hex_string)
				i := i + 1
			end
		ensure then
			twice_length: Result.count = (count * 2)
		end

	--|--------------------------------------------------------------

	truncated_stream (len: INTEGER_32): AEL_DS_BYTE_STREAM
			-- Sub-stream from start, containing len items or count
			-- items if len is larger than count
		do
			Result := subarray (0, (len - 1).min (upper))
		end

	--|--------------------------------------------------------------

	subarray (spos, epos: INTEGER_32): AEL_DS_BYTE_STREAM
			-- Sub-stream from spos through epos, inclusive,
			-- where spos and epos are zero-based
		do
			create Result.make ((epos - spos) + 1)
			if spos <= epos then
				Result.subcopy (Current, spos, epos, spos)
			end
		end

	--|--------------------------------------------------------------
invariant
	zero_lower_bound: lower = 0
	zero_based: capacity = (upper + 1)

end -- class AEL_DS_BYTE_STREAM

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Added history
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this class, create a new instance with a given number of octets
--| using 'make', or from an existing stream using either 'make_from_string'
--| or 'make_from_managed_pointer'.
--| N.B. The lower bound is ALWAYS 0 !!!
--|----------------------------------------------------------------------
