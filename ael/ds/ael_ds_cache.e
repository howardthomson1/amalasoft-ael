--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A simple cache where items can be recalled from storage on demand
--|----------------------------------------------------------------------

class AEL_DS_CACHE [G -> AEL_DS_CACHEABLE create make_with_cache_key end]

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (v: INTEGER)
			-- Create Current with a capacity of 'v'
		do
			create queue.make
			create keys.make (v)
			queue.start
			capacity := v
		end

--|========================================================================
feature -- Element change
--|========================================================================

	insert_item (v: G)
			-- Add a new item identified by the item's key
			-- Move to back of LRU queue - making it most recently used
			-- and possibly purging least recently used item
			--
			-- Leave new item in 'retrieved_item'
		require
			item_exists: v /= Void
			item_is_new: not key_is_known (v.cache_key)
		do
			if count = capacity then
				remove_oldest_from_queue
			end
			queue.extend (v)
			if store_on_insert then
				v.save
			end
			keys.extend (v.cache_key, v.cache_key)
			retrieved_item := v
		ensure
			in_queue: retrieved_item = v
		end

	--|--------------------------------------------------------------

	retrieve_item_by_key (k: STRING)
			-- Retrieve the item identified by the key 'k'
			-- Fetch from cache if available; retrieve from storage if not
			--
			-- Move to back of LRU queue (making it most recently used)
			--
			-- Leave retrieved item in 'retrieved_item'
		require
			exists: k /= Void
		local
			ti: detachable G
		do
			ti := cached_item_by_key (k)
			if ti = Void then
				create ti.make_with_cache_key (k)
				retrieved_item := ti
			else
				remove_item_from_queue (ti)
			end
			if count = capacity then
				remove_oldest_from_queue
			end
			queue.extend (ti)
		ensure
			attached retrieved_item as t implies t.cache_key.is_equal (k)
		end

	--|--------------------------------------------------------------

	force_retrieve_item_by_key (k: STRING)
			-- Remove from the LRU queue the item identified by the key 'k'
			-- Then retrieve the item as usual
		require
			exists: k /= Void
		do
			if has_item_by_key (k) then
				remove_item_from_queue_by_key (k)
			end
			retrieve_item_by_key (k)
		ensure
			attached retrieved_item as t implies t.cache_key.is_equal (k)
		end

--|========================================================================
feature -- Access
--|========================================================================

	retrieved_item: detachable G
			-- Item most recently retrieved

	--|--------------------------------------------------------------

	cached_item_by_key (k: STRING): detachable G
			-- Item by key, if in cache (not retrieved automatically)
		require
			exists: k /= Void
		do
			from queue.start
			until queue.after or Result /= Void
			loop
				if queue.item.cache_key.is_equal (k) then
					Result := queue.item
				else
					queue.forth
				end
			end
			queue.start
		ensure
			exists: has_item_by_key (k) implies Result /= Void
		end

--|========================================================================
feature -- Status
--|========================================================================

	capacity: INTEGER
			-- Maximum number of items in Current at any given time

	count: INTEGER
			-- Number of items in Current presently
		do
			Result := queue.count
		end

	--|--------------------------------------------------------------

	store_on_insert: BOOLEAN
			-- Should items be saved to disk when added to cache?

	store_on_remove: BOOLEAN
			-- Should items be saved to disk when removed from cache?

 --|------------------------------------------------------------------------

	has_item_by_key (k: STRING): BOOLEAN
			-- Does Current contain, in cache, the given item
		require
			exists: k /= Void
		do
			from queue.start
			until queue.after or Result
			loop
				if queue.item.cache_key.is_equal (k) then
					Result := True
				else
					queue.forth
				end
			end
			queue.start
		end

	--|--------------------------------------------------------------

	key_is_known (k: STRING): BOOLEAN
			-- Does the cache know about the given key?
			-- i.e. has this key been seen before, associated with an
			-- item that has been in cache and not explicitly removed?
		do
			Result := keys.has (k)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_store_on_insert
		do
			store_on_insert := True
		end

	disable_store_on_insert
		do
			store_on_insert := False
		end

	--|--------------------------------------------------------------

	enable_store_on_remove
		do
			store_on_remove := True
		end

	disable_store_on_remove
		do
			store_on_remove := False
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	queue: TWO_WAY_LIST [G]
			-- Queue of items for LRU logic
			-- Front of queue is least recently accessed item

	keys: HASH_TABLE [STRING, STRING]

	--|--------------------------------------------------------------

	remove_oldest_from_queue
			-- Remove the oldest item from the LRU queue
		do
			if not queue.is_empty then
				queue.start
				remove_queue_item
			end
			queue.start
		ensure
			removed: (not old queue.is_empty) implies
				queue.count = (old queue.count - 1)
		end

	--|--------------------------------------------------------------

	remove_item_from_queue (v: G)
			-- Remove the given item from the LRU queue
		require
			exists: v /= Void
		do
			queue.start
			queue.search (v)
			if not queue.exhausted then
				remove_queue_item
			end
			queue.start
		end

	--|--------------------------------------------------------------

	remove_item_from_queue_by_key (k: STRING)
			-- Remove from the LRU queue, the item identified by 'k'
		require
			exists: k /= Void
		local
			found: BOOLEAN
		do
			from queue.start
			until queue.exhausted or found
			loop
				if queue.item.cache_key.is_equal (k) then
					remove_queue_item
					found := True
				else
					queue.forth
				end
			end
			queue.start
		end

	--|--------------------------------------------------------------

	remove_queue_item
			-- Remove the item on queue at the current cursor position
		require
			not_empty: not queue.is_empty
			not_off: not queue.off
		do
			if store_on_remove then
				queue.item.save
			end
			queue.remove
		end

 --|------------------------------------------------------------------------
invariant
	queue_exists: queue /= Void
	valid_queue_state:  queue.is_empty implies queue.after
	bounded: count <= capacity

end -- class AEL_DS_CACHE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| To use this class, create a new instance with a given number of items
--|----------------------------------------------------------------------
