--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An item that can be held in an AEL_DS_CACHE
--|----------------------------------------------------------------------

deferred class AEL_DS_CACHEABLE

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_with_cache_key (v: like cache_key)
			-- Create Current with the key 'v'
		require
			is_valid_cache_key: is_valid_cache_key (v)
		do
			cache_key := v
			initialize
		end

	--|--------------------------------------------------------------

	initialize
		deferred
		end

	--|--------------------------------------------------------------

	make_from_other (v: like Current)
		require
			exists: v /= Void
			keys_match: v.cache_key.is_equal (cache_key)
		do
			copy (v)
		ensure
			same: v.is_equal (Current)
		end

--|========================================================================
feature -- Status
--|========================================================================

	cache_key: STRING
			-- Key by which Current will be identified in a cache

	--|--------------------------------------------------------------

	filename: STRING
		deferred
		ensure
			exists: Result /= Void
		end

	--RFO path_name: FILE_NAME
	--RFO 	do
	--RFO 		create Result.make_from_string (directory_name)
	--RFO 		Result.extend (filename)
	--RFO 	ensure
	--RFO 		exists: Result /= Void
	--RFO 	end

	path_name: PATH
		do
			create Result.make_from_string (directory_name)
			Result := Result.extended (filename)
		ensure
			exists: Result /= Void
		end

	directory_name: DIRECTORY_NAME
		deferred
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_valid_cache_key (v: like cache_key): BOOLEAN
			-- Is the given value a valid cache key?
		deferred
		end

--|========================================================================
feature -- Persistence
--|========================================================================

	retrieve
		local
			tf: RAW_FILE
		do
			create tf.make_with_path (path_name)
			if not tf.exists then
				-- ERROR
			else
				tf.open_read
				if not tf.is_open_read then
					-- ERROR
				else
					if not attached {like Current} tf.retrieved as tant then
						-- ERROR?
					else
						make_from_other (tant)
					end
					tf.close
				end
			end
		end

 --|------------------------------------------------------------------------

	save
		local
			tf: RAW_FILE
			td: DIRECTORY
		do
			create tf.make_with_path (path_name)
			if not tf.exists then
				create td.make (directory_name)
				if not td.exists then
					td.create_dir
				end
			end
			tf.open_write
			if not tf.is_open_write then
				-- ERROR
			else
				tf.general_store (Current)
				tf.close
			end
		end

	--|--------------------------------------------------------------
invariant
	key_exists: cache_key /= Void
	key_valid: is_valid_cache_key (cache_key)

end -- class AEL_DS_CACHEABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class should be inherited by any class to be cached.
--|----------------------------------------------------------------------
