--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class resembling a two-way-list, but actually _having_ a list 
--| and being cacheable, using an AEL_DS_CACHE
--|----------------------------------------------------------------------

class AEL_DS_CACHEABLE_LIST [G]

inherit
	AEL_DS_CACHEABLE
		redefine
			make_with_cache_key
		end

create
	make_with_cache_key

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_with_cache_key (v: like cache_key)
			-- Create Current with the key 'v'
		do
			create list.make
			list.compare_objects
			Precursor (v)
		end

	initialize
		do
			filename := cache_key_to_filename
			retrieve
		end

--|========================================================================
feature -- Status
--|========================================================================

	filename: STRING

	list: TWO_WAY_LIST [G]

	directory_hash_code: INTEGER

	file_hash_code: INTEGER

	--|--------------------------------------------------------------

	is_valid_cache_key (v: like cache_key): BOOLEAN
			-- Valid hash key is a simple string
		do
			Result := v /= Void and then v.count >= 3
		end

	--|--------------------------------------------------------------

	cache_key_to_filename: STRING
		do
			Result := "tst-"
			Result.append (cache_key)
			Result.append (".dat")
		end

	--|--------------------------------------------------------------

	directory_name: DIRECTORY_NAME
		do
			create Result.make_from_string ("C:\Temp")
			Result.extend (directory_hash_code.out)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	add_item (v: G)
		do
			list.extend (v)
		end

	--|--------------------------------------------------------------

	remove_item (v: G)
		local
			oc: CURSOR
		do
			oc := list.cursor
			list.start
			list.search (v)
			if not list.exhausted then
				list.remove
			end
			if list.valid_cursor (oc) then
				list.go_to (oc)
			else
				list.start
			end
		end

end -- class CACHEABLE_LIST

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Define and create a type-specific instance of this class, giving it
--| a unique cache key (by which the cache can identify the instance)
--| inversions (alternate structures holding the same items).
--| Redefine cache_key_to_filename and directory_name as desired.
--|----------------------------------------------------------------------
