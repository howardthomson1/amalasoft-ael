--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class resembling a hash table, but actually having a hash table
--| and maintaining one or more inversions in synch
--|----------------------------------------------------------------------

class AEL_DS_COMPLEX_TABLE [G, H -> HASHABLE]

inherit
	AEL_DS_INDIRECT_TABLE [G, H]
		rename
			make as table_make,
			make_table as make_tables
		redefine
			make_tables, extend, remove, wipe_out
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (c, ni: INTEGER)
		do
			number_of_inversions := ni
			create inversions.make (ni)
			table_make (c)
		end

	--|--------------------------------------------------------------

	make_tables (n: INTEGER)
		local
			i, lim: INTEGER
		do
			Precursor (n)
			lim := number_of_inversions
			from i := 1
			until i > lim
			loop
				inversions.extend (create {AEL_DS_LIST_AGENT_SORTED[G]}.make (
					agent dummy_function))
				i := i + 1
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	number_of_inversions: INTEGER
			-- Number of inversion lists maintained for table

	--|--------------------------------------------------------------

	inversion_has (i: INTEGER; v: like item): BOOLEAN
			-- Does the 'i_th' inversion list have the given item?
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			--RFO Is this needed?
			if attached v as tv then
				Result := i_th_inversion (i).has (tv)
			end
		end

	--|--------------------------------------------------------------

	inv_compare_function (i: INTEGER): FUNCTION [G, G, INTEGER]
			-- Function used by inversion 'i' to compare items
			-- for sorting
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := i_th_inversion (i).comparison_function
		end

	dummy_function (v1, v2: G): INTEGER
		do
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_inv_compare_function (i: INTEGER; v: like inv_compare_function)
			-- Set, for inversion number 'i', the comparison function 'v'
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			i_th_inversion (i).set_comparison_function (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

	inversion_item (i: INTEGER): G
			-- Item at cursor position in inversion list number 'i'
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := i_th_inversion (i).item
		end

	--|--------------------------------------------------------------

	i_th_inversion (i: INTEGER): AEL_DS_LIST_AGENT_SORTED [G]
			-- 'i'th element in inversions list
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := inversions.i_th (i)
		end

--|========================================================================
feature -- Cursor movement and state in inversion lists
--|========================================================================

	inversion_cursor (i: INTEGER): CURSOR
			-- Cursor for given inversion list
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := i_th_inversion (i).cursor
		end

	inversion_start (i: INTEGER)
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			i_th_inversion (i).start
		end

	inversion_finish (i: INTEGER)
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			i_th_inversion (i).start
		end

	inversion_forth (i: INTEGER)
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			i_th_inversion (i).forth
		end

	inversion_back (i: INTEGER)
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			i_th_inversion (i).back
		end

	inversion_after (i: INTEGER): BOOLEAN
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := i_th_inversion (i).after
		end

	inversion_exhausted (i: INTEGER): BOOLEAN
		require
			valid_index: i > 0 and i <= number_of_inversions
		do
			Result := i_th_inversion (i).exhausted
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: H)
		do
			table.extend (v, k)
			add_to_inversions (v, k)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	wipe_out
			-- remove all items
		do
			Precursor
			wipe_out_inversions
		end

	--|--------------------------------------------------------------

	remove (k: H)
			-- remove item with given key
		do
			if attached {G} item (k) as ti then
				remove_item_from_inversions (ti, k)
				table.remove (k)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	add_to_inversions (v: G; k: H)
		require
			in_table: table.has (k)
		do
			inversions.do_all (agent add_item_to_inversion (v,?))
		end

	--|--------------------------------------------------------------

	remove_item_from_inversions (v: G; k: H)
		require
			exists: v /= Void
			belongs: has (k)
		do
			inversions.do_all (agent remove_item_from_inversion (v,?))
		end

	--|--------------------------------------------------------------

	wipe_out_inversions
		do
			inversions.do_all (agent wipe_out_inversion)
		end

--|========================================================================
feature {NONE} -- Agents for inversion iterators
--|========================================================================

	add_item_to_inversion (v: G; il: like i_th_inversion)
			-- Add the given item 'v' to the inversion list 'il'
		require
			list_exists: il /= Void
		do
			il.extend (v)
		end

	--|--------------------------------------------------------------

	remove_item_from_inversion (v: G; il: like i_th_inversion)
			-- Remove the given item 'v' from the inversion list 'il'
		require
			list_exists: il /= Void
		local
			oc: CURSOR
		do
			oc := il.cursor
			il.start
			il.search (v)
			if not il.exhausted then
				il.remove
			end
			if il.valid_cursor (oc) then
				il.go_to (oc)
			else
				il.start
			end
		end

	--|--------------------------------------------------------------

	wipe_out_inversion (il: like i_th_inversion)
		require
			list_exists: il /= Void
		do
			il.wipe_out
		end

--|========================================================================
feature {AEL_DS_INDIRECT_TABLE} -- Structure
--|========================================================================

	inversions: ARRAYED_LIST [like i_th_inversion]

	--|--------------------------------------------------------------
invariant
	inversions_exist: inversions /= Void

end -- class AEL_DS_COMPLEX_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class with a capacity and a number of
--| inversions (alternate structures holding the same items).
--| Add inversions as desired, up to the number defined at creation.
--| Once inversions are defined, use typical HASH_TABLE routines to
--| add, delete and access items.
--|----------------------------------------------------------------------
