--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A comma-separated list
--|----------------------------------------------------------------------
	
class AEL_DS_CSL

inherit
	TWO_WAY_LIST [STRING]
		redefine
			out
		end
	AEL_PF_TYPE_ROUTINES
		undefine
			out, is_equal, copy, default_create
		end

create
	make_from_string, make_from_container, make

create {TWO_WAY_LIST}
	make_sublist

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_from_string (v: STRING)
			-- Create Current from the string 'v'
		require
			valid_string: v /= Void
		do
			make
			init_from_string (v)
		end

	--|--------------------------------------------------------------

	make_from_container (tl: CONTAINER [STRING])
			-- Create Current from items in the list 'tl'
		do
			make
			init_from_container (tl)
		ensure
			valid_count: count_matches (tl)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	init_from_string (v: STRING)
			-- Clear any old content and fill from string 'v'
		require
			valid_string: v /= Void
		local
			tf: LIST [STRING]
		do
			wipe_out
			if not v.is_empty then
				tf := v.split (',')
				if not tf.is_empty then
					if tf.count = 1 then
						extend (v)
					else
						fill (tf)
					end
				end
			end
		ensure
			filled: (not v.is_empty) implies not is_empty
			valid_out: v.is_equal (out)
		end

	--|--------------------------------------------------------------

	init_from_container (tl: CONTAINER [STRING])
			-- Clear any old content and fill from container 'tl'
		require
			valid_container: tl /= Void
		do
			wipe_out
			if not tl.is_empty then
				fill (tl)
			end
		ensure
			valid_count: count_matches (tl)
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING
			-- String representation of Current
		local
			oc: CURSOR
		do
			create Result.make (128)
			oc := cursor
			from start
			until exhausted
			loop
				Result.append (item)
				forth
				if not exhausted then
					Result.extend (',')
				end
			end
			go_to (oc)
		ensure then
			valid_out: Result.occurrences (',') = (count - 1)
		end

--|========================================================================
feature -- Assertion support
--|========================================================================

	count_matches (tl: CONTAINER [like item]): BOOLEAN
		local
			tf: detachable FINITE [detachable ANY]
		do
			if attached tl as ltl then
				Result := True
				tf := any_to_finite (ltl)
				if attached tf as ltf then
					Result := ltf.count = count
				else
					-- Not finite, has no 'count' feature
				end
			end
		end

end -- class AEL_DS_CSL

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 000 15-Jul-2009
--|     Created original module
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate by creating with on of the make* creation routines
--| Manipulate as any other TWO_WAY_LIST.  Use 'out' to get CSL
--| representation
--|----------------------------------------------------------------------
