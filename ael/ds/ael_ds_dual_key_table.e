note
	description: "{
HASH_TABLE whose items can be accessed by either or both a generic 
(hashable) key and an integer key.
The integer ids need not be sequential/contiguous.
A special id is used to denote invalidity (K_dk_invalid_id)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/08/12 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--|----------------------------------------------------------------------

class AEL_DS_DUAL_KEY_TABLE [G]

inherit
	HASH_TABLE [AEL_DS_DUAL_KEY_TABLE_ITEM [G], STRING_GENERAL]
		rename
			extend as ht_extend,
			force as ht_force,
			replace as ht_replace,
			remove as ht_remove,
			put as ht_put,
			item as ht_item,
			item_for_iteration as ht_item_for_iteration,
			has as ht_has,
			content as ht_content,
			keys as ht_keys
		export
			{NONE} merge, copy
			{ANY} count, is_empty, extendible,
			deep_twin, same_type, is_deep_equal, start, after, forth, off,
			key_for_iteration
		redefine
			wipe_out, make, accommodate, empty_duplicate
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (n: INTEGER)
		do
			create indexed_items.make (n)
			create_stabile_attributes
			Precursor (n)
		end

	create_stabile_attributes
		do
		end

	--|--------------------------------------------------------------

	accommodate (n: INTEGER)
			-- Reallocate table with enough space for `n' items;
			-- keep all current items.
		do
			is_resizing := True
			Precursor (n)
			indexed_items.accommodate (n)
			is_resizing := False
		ensure then
			not_resizing: not is_resizing
		end

--|========================================================================
feature -- Status
--|========================================================================

	has (k: like Kta_dk_key): BOOLEAN
			-- Does Current have an item by key 'k;?
		require
			valid_key: is_valid_key (k)
		do
			Result := ht_has (k)
		end

	has_item_by_id (idx: like Kta_dk_id): BOOLEAN
			-- Does Current have an item by id 'idx'?
		require
			valid_id: is_valid_id (idx)
		do
			Result := indexed_items.has (idx)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (k: like Kta_dk_key): detachable G
			-- Item in Current stored by key 'k', if any
		require
			valid_key: is_valid_key (k)
		do
			if attached ht_item (k) as ti then
				Result := ti.item
			end
		end

	item_by_id (v: like Kta_dk_id): detachable G
			-- Item in Current stored by integer id 'v', if any
		do
			if attached indexed_items.item (v) as ti then
				Result := ti.item
			end
		end

	--|--------------------------------------------------------------

	item_for_iteration: detachable G
			-- Element at current iteration position
		require
			not_off: not off
		do
			if attached ht_content.item (iteration_position) as ti then
				Result := ti.item
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: like Kta_dk_key; idx: like Kta_dk_id)
			-- Assuming there is no item of key 'k' or integer id 'idx',
			-- insert 'v' with key 'k' and integer id 'idx'.
			-- Set 'inserted'.
		require
			exists: v /= Void
			valid_key: is_valid_key (k)
			valid_id: is_valid_id (idx)
			is_new_key: not has (k)
			is_new_id: not has_item_by_id (idx)
			not_resizing: not is_resizing
		do
			force (v, k, idx)
		end

	--|--------------------------------------------------------------

	force (new: G; key: like Kta_dk_key; idx: like Kta_dk_id)
			-- Update table so that 'new' will be the item associated
			-- with 'key' and with integer id 'idx'.
			-- If there was an item for that key (and 'idx'), set 'found'
			-- and set 'found_item' to that item.
			-- If there was none, set 'not_found' and set
			-- 'found_item' to the default item value.
			--
			-- To choose between various insert/replace procedures,
			-- see "instructions" in the Indexing (note) clause.
		require
			exists: new /= Void
			valid_key: is_valid_key (key)
			valid_id: is_valid_id (idx)
			not_resizing: not is_resizing
		local
			tp: AEL_DS_DUAL_KEY_TABLE_ITEM [G]
		do
			create tp.make (new, key, idx)
			ht_force (tp, key)
			indexed_items.force (tp, idx)
		ensure
			added_by_key: has (key)
			added_by_id: has_item_by_id (idx)
			key_consistent: attached ht_item (key) as ti implies ti.key = key
			id_consistent: attached ht_item (key) as ti implies ti.id = idx
			item_same_by_key: attached ht_item (key) as ti
				implies ti.item = item (key)
			item_same_by_id: attached indexed_items.item (idx) as ti
				implies ti.item = item (key)
		end

	--|--------------------------------------------------------------

	replace (v: G; k: like Kta_dk_key)
			-- Replace item at 'k', if present, with 'v'
			-- Do not change associated key, id or proxy item.
		require
			exists: v /= Void
			valid_key: is_valid_key (k)
			not_resizing: not is_resizing
		do
			if attached ht_item (k) as ti then
				ti.set_item (v)
			end
		ensure
			same_proxy: ht_item (k) = old ht_item (k)
			same_key: attached ht_item (k) as ti implies ti.key = k
			item_replaced: attached ht_item (k) as ti implies ti.item = v
		end

	replace_by_id (v: G; idx: like Kta_dk_id)
			-- Replace item at 'idx', if present, with 'v'
			-- Do not change associated key, id or proxy item.
		require
			exists: v /= Void
			valid_key: is_valid_id (idx)
			not_resizing: not is_resizing
		do
			if attached indexed_items.item (idx) as ti then
				ti.set_item (v)
			end
		end

	--|--------------------------------------------------------------

	remove_by_key (k: like Kta_dk_key)
			-- Remove item associated with `k', if present.
			-- Set `removed' if and only if an item has been
			-- removed (i.e. `k' was present);
			-- if so, set `position' to index of removed element.
			-- If not, set `not_found'.
			-- Reset `found_item' to its default value if `removed'.
		require
			belongs: has (k)
			valid_key: is_valid_key (k)
			not_resizing: not is_resizing
		do
			ht_remove (k)
			if attached ht_item (k) as ti then
				indexed_items.remove (ti.id)
			end
		ensure
			gone: not has (k)
			deducted_if_found: removed implies count = old count - 1
		end

	--|--------------------------------------------------------------

	remove_by_id (idx: like Kta_dk_id)
			-- Remove item associated with `k', if present.
			-- Set `removed' if and only if an item has been
			-- removed (i.e. `k' was present);
			-- if so, set `position' to index of removed element.
			-- If not, set `not_found'.
			-- Reset `found_item' to its default value if `removed'.
		require
			valid_id: is_valid_id (idx)
			not_resizing: not is_resizing
		do
			if attached indexed_items.item (idx) as ti then
				ht_remove (ti.key)
				indexed_items.remove (idx)
			end
		ensure
			gone: not has_item_by_id (idx)
			deducted_if_found: removed implies count = old count - 1
		end

	--|--------------------------------------------------------------

	wipe_out
			-- Reset all items to default values; reset status.
		do
			Precursor
			indexed_items.wipe_out
		end

--|========================================================================
feature {HASH_TABLE} -- Element change
--|========================================================================

	put (new: G; key: like Kta_dk_key; idx: like Kta_dk_id)
			-- Insert 'new' with 'key' and integer ID 'idx' if there is 
			-- no other item associated with the same key.
			-- Set 'inserted' if and only if an insertion has
			-- been made (i.e. 'key' was not present).
			-- If so, set 'position' to the insertion position.
			-- If not, set 'conflict'.
			-- In either case, set 'found_item' to the item
			-- now associated with 'key' (previous item if
			-- there was one, 'new' otherwise).
			--
			-- To choose between various insert/replace procedures,
			-- see "instructions" in the Indexing (note) clause.
		require
			exists: new /= Void
			valid_key: is_valid_key (key)
		local
			tp: AEL_DS_DUAL_KEY_TABLE_ITEM [G]
		do
			if attached ht_item (key) as ti then
				-- Nope, use ht to record conflict
				tp := ti
			else
				create tp.make (new, key, idx)
				indexed_items.put (tp, idx)
			end
			ht_put (tp, key)
		ensure
			inserted: inserted implies has (key)
		end

--|========================================================================
feature {NONE} -- Alternate representation
 --|========================================================================

	indexed_items: HASH_TABLE [AEL_DS_DUAL_KEY_TABLE_ITEM [G], like Kta_dk_id]
			-- Contents stored by integer ID

--|========================================================================
feature -- Status
--|========================================================================

	item_id (k: like Kta_dk_key): like Kta_dk_id
			-- ID associated with item with key 'k', if any
			-- If not item is stored in Current by key 'k', then Result 
			-- is 0
		do
			if attached ht_item (k) as ti then
				Result := ti.id
			end
		end

	is_resizing: BOOLEAN
			-- Is Current in the process of resizing?

--|========================================================================
feature {AEL_DS_DUAL_KEY_TABLE} -- Resizing support
--|========================================================================

	set_is_resizing (tf: BOOLEAN)
			-- Set the 'is_resizing' flag to 'tf'
		do
			is_resizing := tf
		ensure
			is_set: is_resizing = tf
		end

--|========================================================================
feature {NONE} -- Duplication
--|========================================================================

	empty_duplicate (n: INTEGER): like Current
			-- Create an empty copy of Current that can accommodate `n' items
		do
			Result := Precursor (n)
			Result.set_is_resizing (True)
		ensure then
			flagged: Result.is_resizing
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_key (v: STRING_GENERAL): BOOLEAN
			-- Is 'v' a valid key?
		do
			Result := v /= Void and then not v.is_empty
		end

	is_valid_id (v: like Kta_dk_id): BOOLEAN
			-- Is 'v' a valid id?
			-- True if valid w/r/t syntax and range;  Does not imply 
			-- that 'v' is in use or that Current has an item 
			-- corresponding to 'v'
		do
			Result := v /= K_dk_invalid_id
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	Kta_item: G
			-- Type anchor for items
		require False
		do
			check False then end
		ensure False
		end

	Kta_dk_key: STRING_GENERAL
			-- Type anchor for keys
		require False
		do
			check False then end
		ensure False
		end

	Kta_dk_id: INTEGER_64
			-- Type anchor for numeric IDs

	Kta_item_proxy: AEL_DS_DUAL_KEY_TABLE_ITEM [G]
			-- Type anchor for item proxies
		require False
		do
			check False then end
		ensure False
		end

	K_dk_invalid_id: INTEGER_64
			-- Special integer ID used to indicate invalidity
		once
			Result := {AEL_DS_DUAL_KEY_TABLE_ITEM[G]}.K_dk_invalid_id
		end

	--|--------------------------------------------------------------
invariant
	counts_consistent: (not is_resizing) implies count = indexed_items.count

--|----------------------------------------------------------------------
--| History
--|
--| 001 16-Aug-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create with an initial capacity.
--| Use as you would a normal HASH_TABLE except that initial insertions 
--| require two keys - a string and an integer.  They need not be 
--| similar in value but they need to be unique.
--| Lookups can use either form.
--|----------------------------------------------------------------------

end -- class AEL_DS_DUAL_KEY_TABLE
