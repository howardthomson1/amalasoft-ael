note
	description: "{
Proxy container for items in a Dual-Key Table
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/06/25 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_DUAL_KEY_TABLE_ITEM [G]

inherit
	ANY
		redefine
			default_create
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (v: G; k: STRING_GENERAL; idx: INTEGER_64)
		require
			item_exists: v /= Void
			valid_key: k /= Void and then not k.is_empty
		do
			item := v
			key := k
			id := idx
			default_create
		ensure
			item_assigned: item = v
			key_assigned: key.same_string (k)
			id_assigned: id = idx
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		do
			private_min_id_value := K_dk_invalid_id
			private_max_id_value := {INTEGER_64}.Max_value
		end

--|========================================================================
feature -- Status
--|========================================================================

	item: G
			-- Item for which Current is a proxy

	key: STRING_GENERAL
			-- String key with which item is stored

	id: INTEGER_64
			-- Integer key with which item is stored
			-- Integer keys are not necessarily contiguous or seqential

--|========================================================================
feature -- Status setting
--|========================================================================

	set_item (v: G)
			-- Set 'item' to 'v'
		require
			item_exists: v /= Void
		do
			item := v
		ensure
			is_set: item = v
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_id (v: INTEGER_64): BOOLEAN
			-- Is 'v' a valid id?
			-- True if valid w/r/t syntax and range;  Does not imply 
			-- that 'v' is in use or that Current has an item 
			-- corresponding to 'v'
		do
			Result := v >= min_legal_id_value and v <= max_legal_id_value
		ensure
			not_invalid: Result implies v /= K_dk_invalid_id
		end

	--|--------------------------------------------------------------

	min_legal_id_value: INTEGER_64
			-- Lowest value allowed for instances of Current
		do
			Result := private_min_id_value
			if Result = K_dk_invalid_id then
				Result := K_dk_dflt_min_id_value
			end
		ensure
			valid: is_valid_id (Result)
		end

	max_legal_id_value: INTEGER_64
			-- Default highest value allows for instances of Current
		do
			Result := private_max_id_value
			if Result = K_dk_invalid_id then
				Result := K_dk_dflt_max_id_value
			end
		ensure
			valid: is_valid_id (Result)
		end

--|========================================================================
feature {AEL_DS_DUAL_KEY_TABLE} -- Constants
--|========================================================================

	K_dk_invalid_id: INTEGER_64 = -9223372036854775808
			-- Special integer ID used to indicate invalidity
			-- Min value of integer 64

	K_dk_dflt_min_id_value: INTEGER_64
			-- Default lowest value allows for instances of Current
		once
			Result := {INTEGER_64}.Min_value + 1
		ensure
			valid_no_conflict: Result /= K_dk_invalid_id
		end

	K_dk_dflt_max_id_value: INTEGER_64
			-- Default highest value allows for instances of Current
		once
			Result := {INTEGER_64}.Max_value
		ensure
			valid_no_conflict: Result /= K_dk_invalid_id
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_min_id_value: INTEGER_64
			-- Lowest value allows for instances of Current

	private_max_id_value: INTEGER_64
			-- Highest value allows for instances of Current

	--|--------------------------------------------------------------
invariant
	has_item: item /= Void
	has_key: key /= Void and then not key.is_empty
	has_id: is_valid_id (id)

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Jn-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_DS_DUAL_KEY_TABLE_ITEM
