--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A sublcass of STRING allows clients to access 'item' at a
--| position beyond the current end.  When such an access occurs,
--| it attempts to read more from the input medium.  If
--| the read is successful, then last_item_succeeded is True.
--|----------------------------------------------------------------------

class AEL_DS_DYNAMIC_STRING

inherit
	STRING
		rename
			item as string_item,
			make as string_make
		export {AEL_DS_DYNAMIC_STRING}
			string_item
		redefine
			string_make
		end

create
	make

create {STRING}
	string_make

 --|========================================================================
feature {NONE}
 --|========================================================================

	make (sz: INTEGER; im: like input_medium)
		do
			string_make (sz)

			input_medium := im
			if attached {CONSOLE} input_medium then
				input_is_file := False
			else
				input_is_file := True
			end
			look_ahead_size := 4
		end

	--|--------------------------------------------------------------

	string_make (sz: INTEGER)
		do
			Precursor (sz)
			input_medium := io.input
		end

 --|========================================================================
feature -- Access
 --|========================================================================

	item (v: INTEGER): CHARACTER
		local
			old_count: INTEGER
		do
			last_item_succeeded := False
			old_count := count

			if valid_index (v) then
				Result := string_item (v)
				last_item_succeeded := True
			else
				-- We have run out of content, ask for more if any is available
				top_off
				if count > old_count then
					last_item_succeeded := True
					Result := string_item (v)
				else
					-- Hmmmmm
				end
			end
		end

	--|--------------------------------------------------------------

	is_dry: BOOLEAN
		do
			--    if is_empty then
			Result := input_medium.end_of_file
			--    end
		end

 --|========================================================================
feature
 --|========================================================================

	last_item_succeeded: BOOLEAN
	input_is_file: BOOLEAN
	input_medium: FILE
	look_ahead_size: INTEGER

	--|--------------------------------------------------------------

	set_look_ahead_size (v: INTEGER)
			-- Set the look_ahead size to a value possibly other than the default
		require
			not_negative: v >= 0
		do
			look_ahead_size := v
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	top_off
		do
			if not input_medium.end_of_file then
				input_medium.read_stream (look_ahead_size)
				append (input_medium.last_string)
			end
		end

	--|--------------------------------------------------------------
invariant

end -- class AEL_DS_DYNAMIC_STRING

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| Instantiate an object of this class using the make routine with an
--| argument denoting the number of characters expected
--|----------------------------------------------------------------------
