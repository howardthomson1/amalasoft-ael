--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A primitive hash table with a fixed number of (zero-based) slots.
--| Each slot contains a non-void linked list.
--|----------------------------------------------------------------------

class AEL_DS_FIXED_HASH_TABLE [G, H->HASHABLE]

inherit
	ANY

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
			-- Create Current with 'sz' slots
		require
			valid_size: sz > 0
		local
			i: INTEGER
			tl: like i_th_chain
		do
			create tl.make
			tl.compare_objects
			create slots.make_filled (tl, 0, sz - 1)
			number_of_slots := sz
			from i := minimum_index
			until i > maximum_index
			loop
				create tl.make
				tl.compare_objects
				slots.put (tl, i)
				i := i + 1
			end
		end

--|========================================================================
feature -- Structure
--|========================================================================

	slots: ARRAY [like i_th_chain]
			-- Slots allocated on creation

	number_of_slots: INTEGER
			-- Number of slots allocate

	--|--------------------------------------------------------------

	longest_chain_length: INTEGER
			-- Length of longest chain in table
		do
			Result := longest_chain.count
		end

	--|--------------------------------------------------------------

	longest_chain: like i_th_chain
			-- Longest chain in table
		local
			i: INTEGER
		do
			Result := slots.item (slots.lower)
			from i := slots.lower
			until i > slots.upper
			loop
				if slots.item (i).count > Result.count then
					Result := slots.item (i)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	i_th_chain (v: INTEGER): LINKED_LIST [like i_th_first]
			-- Chain (linked list) at slot 'v'
		require
			valid_index: is_valid_slot_index (v)
		do
			Result := slots.item (v)
		end

	--|--------------------------------------------------------------

	i_th_first (v: INTEGER): detachable TUPLE [G,H]
			-- First item (if any) in chain (linked list) at slot 'v'
		require
			valid_index: is_valid_slot_index (v)
		local
			tl: like i_th_chain
		do
			tl := i_th_chain (v)
			if not tl.is_empty then
				Result := tl.first
			end
		end

	--|--------------------------------------------------------------

	hashed_index (k: HASHABLE): INTEGER
		require
			exists: k /= Void
		local
			hc: INTEGER
		do
			hc := k.hash_code
			Result := hc \\ number_of_slots
		ensure
			valid: is_valid_slot_index (Result)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_item (v: G; k: H; ff: BOOLEAN)
			-- Add 'v' to Current at position determined by 'k'
			-- If an item by 'k' already exists, then if 'ff is True,
			-- then replace it, else do not replace it
		local
			tl: like i_th_chain
			was_found: BOOLEAN
			oc: CURSOR
		do
			tl := slots.item (hashed_index (k))
			oc := tl.cursor
			from tl.start
			until tl.exhausted or was_found
			loop
				if item_matches_key (tl.item, k) then
					was_found := True
					if ff then
						tl.replace ( [v, k] )
						count := count + 1
					end
				else
					tl.forth
				end
			end
			if not was_found then
				tl.extend ( [v, k] )
			count := count + 1
			end
			tl.go_to (oc)
		ensure
			added_if_new: (not old has (k)) implies count = count + 1
			found: has (k)
		end

--|========================================================================
feature -- Search and comparison
--|========================================================================

	search (k: H)
			-- Search for item correspond to 'k'
			-- Set found_item to item if found
		local
			tl: like i_th_chain
			oc: CURSOR
		do
			found_cell := Void
			tl := slots.item (hashed_index (k))
			oc := tl.cursor
			from tl.start
			until tl.exhausted or found_cell /= Void
			loop
				if item_matches_key (tl.item, k) then
					found_cell := tl.item
				else
					tl.forth
				end
			end
			tl.go_to (oc)
		ensure
			found_if_has: has (k) implies found_cell /= Void
			not_found_if_not_has: (not has (k)) implies found_cell = Void
		end

	--|--------------------------------------------------------------

	item_matches_key (v: like i_th_first; k: H): BOOLEAN
		do
			if attached v as tv and then attached tv.item (2) as tli then
				Result := tli ~ (k)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_slot_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid index into the slots array?
		do
			Result := v >= minimum_index and v <= maximum_index
		end

--|========================================================================
feature -- Measurement
--|========================================================================

	minimum_index: INTEGER
			-- Lowest slot index
		do
			Result := slots.lower
		end

	maximum_index: INTEGER
			-- Highest slot index
		do
			Result := slots.upper
		end

	--|--------------------------------------------------------------

	count: INTEGER
			-- Total number of items in all chains

--|========================================================================
feature -- Access
--|========================================================================

--	item (k: H): detachable G
	item alias "[]", at alias "@" (key: H): detachable G assign force
			-- Item associated with `key', if any
		note
			option: stable
		local
			tl: like i_th_chain
			oc: CURSOR
		do
			tl := slots.item (hashed_index (key))
			oc := tl.cursor
			from tl.start
			until tl.exhausted or Result /= Void
			loop
				if attached tl.item as tli then
					if item_matches_key (tli, key) then
						if attached {G} tli.item (1) as tlii then
							Result := tlii
						end
					end
				else
					tl.forth
				end
			end
			tl.go_to (oc)
		end

--|========================================================================
feature -- Status
--|========================================================================

	has (k: H): BOOLEAN
			-- Is there an item that matches 'k'
		local
			tl: like i_th_chain
		do
			tl := slots.item (hashed_index (k))
			Result := tl.there_exists (agent item_matches_key (?,k))
		end

	--|--------------------------------------------------------------

	found_item: detachable G
			-- Item found from last search, if any
		do
			if attached found_cell as lfc then
				if attached {G} lfc.item (1) as lfi then
					Result := lfi
				end
			end
		end

	--|--------------------------------------------------------------

	found_cell: like i_th_first
			-- Cell holding found item

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: H)
			-- Add 'v' to Current at position determined by 'k'
			-- It is an exception to extend with an item that already 
			-- exists in Current
		require
			is_new: not has (k)
		local
			tl: like i_th_chain
		do
			tl := slots.item (hashed_index (k))
			tl.extend ( [v, k] )
			count := count + 1
		ensure
			added: count = old count + 1
			found: has (k)
		end

	--|--------------------------------------------------------------

	force (v: G; k: H)
			-- Add 'v' to Current at position determined by 'k'
			-- If an item by 'k' already exists, then replace it
		do
			add_item (v, k, True)
		ensure
			added_if_new: (not old has (k)) implies count = old count + 1
			found: has (k)
		end

	--|--------------------------------------------------------------

	put (v: G; k: H)
			-- Add 'v' to Current at position determined by 'k'
			-- If an item by 'k' already exists, then DO NOT replace it
		do
			add_item (v, k, False)
		ensure
			added_if_new: (not old has (k)) implies count = count + 1
			found: has (k)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove (k: H)
			-- Remove item corresponding to 'k' (if any)
		local
			tl: like i_th_chain
			oc: CURSOR
			removed: BOOLEAN
		do
			tl := slots.item (hashed_index (k))
			oc := tl.cursor
			from tl.start
			until tl.exhausted or removed
			loop
				if item_matches_key (tl.item, k) then
					tl.remove
					removed := True
				else
					tl.forth
				end
			end
			if removed then
				count := count - 1
			end
		ensure
			removed: (old has (k)) implies count = (old count) - 1
			gone: not has (k)
		end

	--|--------------------------------------------------------------
invariant
	has_slots: slots /= Void

end -- class AEL_DS_FIXED_HASH_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class with a fixed number of slots.
--| Add, remove and access items as you would a HASH_TABLE
--|----------------------------------------------------------------------
