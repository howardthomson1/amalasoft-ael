--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A bounded priority queue whose items are accessible via hash keys
--|----------------------------------------------------------------------

class AEL_DS_HASH_QUEUE [G, H->HASHABLE]

inherit
	ANY
		redefine
			out
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
			-- Create Current with capacity 'sz'
		do
			capacity := sz
			create hash_table.make (sz)
			create queue.make
			queue.start
		end

--|========================================================================
feature -- Status
--|========================================================================

	capacity: INTEGER
			-- Maximum number of items in Current at any given time

	count: INTEGER
			-- Number of items in Current presently
		do
			Result := queue.count
		end

	--|--------------------------------------------------------------

	is_full: BOOLEAN
			-- Is the queue full?
		do
			Result := count = capacity
		end

	is_empty: BOOLEAN
			-- Is the queue empty?
		do
			Result := count = 0
		end

 --|------------------------------------------------------------------------

	has (k: H): BOOLEAN
			-- Does the queue have an item with the key 'k'?
		do
			Result := hash_table.has (k)
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of Current
			-- Items in queue order from highest priority to lowest
		do
			if is_empty then
				Result := ""
			else
				create Result.make (count * 32)
				from queue.start
				until queue.exhausted
				loop
					if attached queue.item.item (1) as ti then
--RFO can we use a void test instead?
						Result.append (ti.out)
						queue.forth
						if not queue.exhausted then
							Result.append (", ")
						end
					end
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	grow (v: INTEGER)
			-- Resize to hold 'v' items
		require
			not_smaller: v >= capacity
		do
			capacity := capacity.max (v)
		ensure
			grown: capacity = v
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: detachable G
			-- Highest priority (oldest) item in queue
		do
			if attached {G} queue.first.item (1) as t then
				Result := t
			end
		end

	--|--------------------------------------------------------------

	queue_item_key: detachable H
			-- Key associated with current queue item, if any
		do
			if not queue.is_empty and then
				attached {H} queue.item.item (2) as ti
			 then
				Result := ti
			end
		end

	--|--------------------------------------------------------------

	item_by_key (k: H): detachable G
			-- Item corresponding to the given key
		do
			Result := hash_table.item (k)
		end

	all_items: LINKED_LIST [G]
			-- Contents in the form of a singly linked list
		do
			create Result.make
			Result.fill (hash_table)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: H)
			-- Add item 'v' to queue if it's not already there
		require
			not_present: not has(k)
		do
			if is_full then
				prune_queue
			end
			queue.extend ( [ v, k ] )
			hash_table.extend (v, k)
		ensure
			added: has (k)
			larger_if_not_full: (not old is_full) implies count = old count + 1
		end

	--|--------------------------------------------------------------

	force (v: G; k: H)
			-- Add item 'v' to queue if it's already there or not
			-- If already there, demote it to lowest priority
		do
			if has (k) then
				remove (k)
			end
			extend (v, k)
		ensure
			added: has (k)
			larger_if_not_full: not (old has (k) or old is_full) implies
				count = old count + 1
		end

	--|--------------------------------------------------------------

	put (v: G; k: H)
			-- Add item 'v' to queue ONLY if it's not already there
			-- If already there, then do not replace the existing one
			-- and do not change the existing item's priority
		do
			if not has (k) then
				extend (v, k)
			end
		ensure
			added: has (k)
			larger_if_not_full: not (old has (k) or old is_full) implies
				count = old count + 1
		end

--|========================================================================
feature -- Element Removal
--|========================================================================

	remove (k: H)
			-- Remove the item matching key 'k'
		require
			belongs: has (k)
		local
			found: BOOLEAN
		do
			from queue.start
			until queue.exhausted or found
			loop
				if attached queue_item_key as tk and then tk.is_equal (k) then
					found := True
					queue.remove
					hash_table.remove (k)
				else
					queue.forth
				end
			end
			queue.start
		end

	--|--------------------------------------------------------------

	wipe_out
		-- Remove all items
		do
			queue.wipe_out
			hash_table.wipe_out
		end

--|========================================================================
feature {NONE} -- Structure
--|========================================================================

	hash_table: HASH_TABLE [G, H]
	queue: LINKED_LIST [TUPLE [G,H]]

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	prune_queue
			-- Remove the highest priority (oldest) item in the queue,
			-- and its corresponding reference in the hash table
		require
			queue_is_full: is_full
		do
			queue.start
			if attached queue_item_key as tk then
				queue.remove
				queue.start
				hash_table.remove (tk)
			end
		end

	--|--------------------------------------------------------------
invariant
	queue_exists: queue /= Void
	valid_queue_state:  queue.is_empty implies (queue.after or queue.before)
	bounded: count <= capacity
	consistent: queue.count = hash_table.count

end -- class AEL_DS_HASH_QUEUE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class with a queue depth.
--| Add, remove and access items as you would a HASH_TABLE
--|----------------------------------------------------------------------

