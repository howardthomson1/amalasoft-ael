--|----------------------------------------------------------------------
--| Copyright (c) 1995-2010, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| HASH_TABLE with enhancements
--|----------------------------------------------------------------------

class AEL_DS_HASH_TABLE [G, H-> HASHABLE]

inherit
	HASH_TABLE [G, H]
		rename
			fill as ht_fill
		end

create
	make, make_from_other

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_other (other: like Current)
		require
			other_exists: other /= Void
		do
			make (other.capacity)
			copy (other)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	last_fill_successful: BOOLEAN
			-- Was last call to 'fill' successful?

	fill (other: CONTAINER [G]; kf: FUNCTION [G, H]; sf: BOOLEAN)
			-- Fill with as many items of `other' as possible.
			-- The representations of `other' and current structure
			-- need not be the same.
			--
			-- For each item in 'src', call key function 'kf' to obtain 
			-- the key to use.  If result of 'kf' is Void, then an 
			-- exception is raised.
			--
			-- If 'kf' itself is Void, then the items in 'other' must be 
			-- themselves HASHABLE and compatible with the defined key 
			-- type. If they are not HASHABLE, then an exception is raised.
			--
			-- NOTE WELL:  Duplicate keys will be skipped unless flag 
			-- 'sf' is True
			-- If 'sf' and a duplicate is found, it is considered benign 
			-- and no error is recorded.  If 'sf' is False, then a 
			-- duplicate is an error and last_fill_successful will be False.
		require else
			other_exists: other /= Void
			hashable: kf = Void implies attached {CONTAINER [H]} other
		local
			k: H
			lin_rep: LINEAR [G]
			ex: EXCEPTIONS
		do
			last_fill_successful := True
			lin_rep := other.linear_representation
			from lin_rep.start
			until lin_rep.off or not last_fill_successful
			loop
				k := kf.item ([lin_rep.item])
				if k = Void then
					create ex
					ex.raise ("Void key return in has_table fill")
				elseif has (k) then
					if not sf then
						last_fill_successful := False
					end
				else
--RFO 					extend (lin_rep.item, k)
					force (lin_rep.item, k)
				end
				lin_rep.forth
			end
		end

end -- class AEL_DS_HASH_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 02-Apr-2010
--|     Original from existing routines elsewhere
--|----------------------------------------------------------------------
--| How-to
--| Create using one of the listed creation routines.
--| Use as you would a normal HASH_TABLE.
--|----------------------------------------------------------------------
