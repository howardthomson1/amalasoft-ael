note
	description: "{
A complex data structure consisting of an array of hash tables but
presenting only a hash interface.
Items of Current are items of the hash tables (not the array).
Array items are the hash tables themselves.
The motivation (and purpose) for this structure is to accommodate
larger capacities than a single hash table can do practically.
}"
	system: "Part of the Amalasoft Eiffel Library"
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/08/24 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_HASH_TABLE_ARRAY [G, K]

--RFO inherit
--RFO 	HASH_TABLE [G, STRING]
--RFO 		rename
--RFO 			extend as ht_extend
--RFO 		export
--RFO 			{NONE} merge, copy
--RFO 			{ANY} count, is_empty, extendible, has,
--RFO 			deep_twin, same_type, is_deep_equal, start, after, forth, off,
--RFO 			item_for_iteration, key_for_iteration
--RFO 		redefine
--RFO 			make, put, force, replace, remove, wipe_out
--RFO 		end

--RFO create
--RFO 	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (n: INTEGER)
			-- Create Current with total nominal capacity of 'n'
			-- Actual capacity might be larger and determined 
			-- programmatically.
		local
			sz: INTEGER
			td: DOUBLE
		do
			if n <= 10_000 then
				magnitude := 10_000
				sz := 1
			else
				td := {DOUBLE_MATH}.log10 (n)
				magnitude := td.truncated_to_integer
				sz := (((10.0).power(td)) / 10_000).truncated_to_integer
			end
			create cell_array.make (sz)
			-- No Precursor
		end

--|========================================================================
feature {NONE} -- Internal structure
--|========================================================================

	cell_array: AEL_DS_SPARSE_ARRAY [detachable like new_cell]
			-- Array containing tables

	new_cell (sz: INTEGER): HASH_TABLE [G, STRING]
			-- A newly created cell (hash table) with initial size 'sz'
		do
			create Result.make (sz)
		end

	key_to_index (v: STRING): INTEGER
			-- Index (into cell_array) corresponding to key 'v'
			-- DOES NOT ENSURE THAT THERE IS A CORRESPONDING ITEM!!!
		local
			ts: STRING
			zero: CHARACTER
		do
			ts := key_to_hash (v)
			if ts.count < 5 then
				Result := 1
			else
				zero := '0'
				Result := zero.code - ts.item (5).code
			end
		end

	key_to_hash (v: STRING): STRING
			-- Hash code corresponding to key 'v'
		do
			-- TODO, needs to be smarter
			Result := v.hash_code.out
		ensure
			is_decimal_string: Result.is_integer
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: K)
			-- Assuming there is no item of key `k',
			-- insert `v' with `k'.
			-- Set `inserted'.
		do
--			force (v, k)
		end

	--|--------------------------------------------------------------

	force (new: G; key: K)
			-- Update table so that `new' will be the item associated
			-- with `key'.
			-- If there was an item for that key, set `found'
			-- and set `found_item' to that item.
			-- If there was none, set `not_found' and set
			-- `found_item' to the default value.
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
		end

	--|--------------------------------------------------------------

	replace (v: G; k: K)
			-- Replace item at `key', if present,
			-- with `new'; do not change associated key.
			-- Set `replaced' if and only if a replacement has been made
			-- (i.e. `key' was present); otherwise set `not_found'.
			-- Set `found_item' to the item previously associated
			-- with `key' (default value if there was none).
		do
		end

	--|--------------------------------------------------------------

	remove (k: K)
			-- Remove item associated with `k', if present.
			-- Set `removed' if and only if an item has been
			-- removed (i.e. `k' was present);
			-- if so, set `position' to index of removed element.
			-- If not, set `not_found'.
			-- Reset `found_item' to its default value if `removed'.
		do
		end

	--|--------------------------------------------------------------

	wipe_out
			-- Reset all items to default values; reset status.
		do
		end

--|========================================================================
feature {HASH_TABLE} -- Element change
--|========================================================================

	put (new: G; key: K)
			-- Insert `new' with `key' if there is no other item
			-- associated with the same key.
			-- Set `inserted' if and only if an insertion has
			-- been made (i.e. `key' was not present).
			-- If so, set `position' to the insertion position.
			-- If not, set `conflict'.
			-- In either case, set `found_item' to the item
			-- now associated with `key' (previous item if
			-- there was one, `new' otherwise).
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
	end

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature -- Sizing support
--|========================================================================

	magnitude: INTEGER

	--|--------------------------------------------------------------
invariant

end -- class AEL_DS_HASH_TABLE_ARRAY

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 24-Aug-2012
--|     Original module derived from AEL_DS_SORTABLE_HASH_TABLE and
--|     other existing AEL classes.
--|
--|     Compiled and tested using Eiffel 7.0
--|----------------------------------------------------------------------
--| How-to
--| Create with an initial NOMINAL capacity.  Capacity is total number
--| of items, not number of tables or slots in array.
--| Use as you would a normal HASH_TABLE.
--|----------------------------------------------------------------------
