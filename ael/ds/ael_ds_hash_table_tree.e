--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A compact tree whose items are accessible via hash keys
--|----------------------------------------------------------------------

class AEL_DS_HASH_TABLE_TREE [G, H->HASHABLE]

inherit
	ANY
		redefine
			out
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
			-- Create Current with capacity 'sz'
		do
			capacity := sz
			create hash_table.make (sz)
			create tree.make (sz)
			tree.start
		end

--|========================================================================
feature -- Status
--|========================================================================

	capacity: INTEGER
			-- Maximum number of items in Current at any given time

	count: INTEGER
			-- Number of items in Current presently
		do
			Result := tree.count
		end

	total_hash_table_count: INTEGER
			-- Total number of items in the hash table's lists
		local
			tl: like ht_list
		do
			from hash_table.start
			until hash_table.off
			loop
				tl := hash_table.item_for_iteration
				Result := Result + tl.count
				hash_table.forth
			end
		end

	--|--------------------------------------------------------------

	is_empty: BOOLEAN
			-- Is the tree empty?
		do
			Result := count = 0
		end

	tree_off: BOOLEAN
			-- Is there no current item?
			-- (True if `is_empty')
		do
			Result := tree.off
		end

	tree_above: BOOLEAN
			-- Is there no valid position above the 
			-- current tree position?
		do
			Result := tree.above
		end

	tree_below: BOOLEAN
			-- Is there no valid position below the
			-- current tree position?
		do
			Result := tree.below
		end

	tree_after: BOOLEAN
			-- Is there no valid position to the right of the 
			-- current tree position?
		do
			Result := tree.after
		end

	tree_before: BOOLEAN
			-- Is there no valid position to the left of the 
			-- current tree position?
		do
			Result := tree.before
		end

	cursor: COMPACT_TREE_CURSOR
		do
			Result := tree_cursor
		end

	tree_cursor: COMPACT_TREE_CURSOR
		do
			Result := tree.cursor
		end

	is_leaf: BOOLEAN
			-- Is cursor on a leaf?
		do
			Result := tree.is_leaf
		end

 --|------------------------------------------------------------------------

	has_some (k: H): BOOLEAN
			-- Does the tree have at least one item with the key 'k'?
		do
			Result := occurrences (k) /= 0
		end

 --|------------------------------------------------------------------------

	has (v: G; k: H): BOOLEAN
			-- Does the tree have an item with the key 'k', whose 
			-- associated object is 'v'?
		local
			tl: detachable like ht_list
			oc: CURSOR
			ti: TUPLE [G,H, like tree_cursor]
		do
			tl := hash_table.item (k)
			if tl /= Void then
				oc := tl.cursor
				from tl.start
				until tl.exhausted or Result
				loop
					ti := tl.item
					if ti.item (1) = v then
						Result := True
					else
						tl.forth
					end
				end
				tl.go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	occurrences (k: H): INTEGER
			-- Number of items in the tree that have a key 'k'
		local
			tl: detachable like ht_list
		do
			tl := hash_table.item (k)
			if tl /= Void then
				Result := tl.count
			end
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of Current
			-- Items in tree order from top
		do
			if is_empty then
				Result := ""
			else
				create Result.make (count * 32)
				from tree.start
				until tree.off
				loop
					if attached tree.item.item (1) as ti then
						Result.append (ti.out)
						tree.forth
						if not tree.off then
							Result.append (", ")
						end
					end
				end
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: detachable G
			-- TREE item at present cursor position
		do
			if not tree.off then
				if attached {G} tree.item.item (1) as t then
					Result := t
				end
			end
		end

	--|--------------------------------------------------------------

	tree_item_key: detachable H
			-- Key associated with current tree item, if any
		do
			if not tree.off and not tree.is_empty and then
				attached {H} tree.item.item (2) as ti
			 then
				Result := ti
			end
		end

	--|--------------------------------------------------------------

	items_by_key (k: H): LINKED_LIST [detachable G]
			-- List of items corresponding to the given key
		local
			tl: detachable like ht_list
			oc: CURSOR
			ti: TUPLE [G, H]
		do
			create Result.make
			tl := hash_table.item (k)
			if tl /= Void then
				oc := tl.cursor
				from tl.start
				until tl.exhausted
				loop
					ti := tl.item
					if ti.item (2) = k then
						if attached {G} ti.item (1) as tgi then
							Result.extend (tgi)
						end
					end
				end
				tl.go_to (oc)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	all_items: LINKED_LIST [G]
			-- Contents in the form of a singly linked list
		local
			tl: detachable like ht_list
			oc: CURSOR
			ti: TUPLE [G, H, like tree_cursor]
		do
			create Result.make
			from hash_table.start
			until hash_table.after
			loop
				tl := hash_table.item_for_iteration
				oc := tl.cursor
				from tl.start
				until tl.exhausted or Result /= Void
				loop
					ti := tl.item
					if attached {G}ti.item (1) as tgi then
						Result.extend (tgi)
					end
					tl.forth
				end
				tl.go_to (oc)
				hash_table.forth
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	subtree: like tree
			-- Subtree rooted at current node
		require
			not_off: not tree_off
		do
			-- RFO TODO needs to be like Current
--			Result := tree.subtree
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: H)
			-- Add item 'v' to tree if it's not already there
		do
			tree.extend ( [v, k] )
			synch (v, k)
		ensure
			added: has (v, k)
			larger: count = old count + 1
		end

	--|--------------------------------------------------------------

	put_left (v: G; k: H)
			-- Put item 'v' into tree at left of current position
		require else
			not_above: not tree_above
		do
			tree.put_left ( [v, k] )
			synch (v, k)
		ensure
			added: has (v, k)
			larger: count = old count + 1
		end

	--|--------------------------------------------------------------

	put_right (v: G; k: H)
			-- Put item 'v' into tree at right of current position
		do
			tree.put_right ( [v, k] )
			synch (v, k)
		ensure
			added: has (v, k)
			larger: count = old count + 1
		end

	--|--------------------------------------------------------------

	put_front (v: G; k: H)
			-- Add a leaf `v' as first child.
			-- If `above' and `is_empty', make `v' the root value
		do
			tree.put_front ( [v, k] )
			synch (v, k)
		ensure
			added: has (v, k)
			larger: count = old count + 1
		end

	--|--------------------------------------------------------------

	put_parent (v: G; k: H)
			-- insert a new node, with value v, as parent of
			-- current node and
			-- with the same position
			--if above or on root, add a new root
		require
			in_tree: not (tree_after or tree_before)
		do
			tree.put_parent ([v, k])
			synch (v, k)
		ensure
			added: has (v, k)
			larger: count = old count + 1
		end

	--|--------------------------------------------------------------

	fill_from_active (other: like tree)
			-- Copy subtree of `other''s active node
			-- onto active node of current tree.
		require
			cursor_on_leaf: is_leaf
		do
			--RFO TODO
			if attached {like tree} other as tt then
				tree.fill_from_active (tt)
			end
		end

--|========================================================================
feature -- Element Removal
--|========================================================================

	remove (v: G; k: H)
			-- Remove the item matching key 'k' and having value 'v'
		require
			belongs: has (v, k)
		local
			found: BOOLEAN
			tc: like tree_cursor
			oc: CURSOR
			tl: like ht_list
			ti: TUPLE [G,H, like tree_cursor]
		do
			tc := tree.cursor
			from tree.start
			until tree.off or found
			loop
				if tree_item_key ~ k then
					found := True
					tl := hash_table.item (k)
					if tl.count = 1 then
						hash_table.remove (k)
					else
						oc := tl.cursor
						found := False
						from tl.start
						until tl.exhausted or found
						loop
							ti := tl.item
							if ti.item (1) = v then
								found := True
								tl.remove
							else
								tl.forth
							end
						end
						if tl.valid_cursor (oc) then
							tl.go_to (oc)
						elseif tl.is_empty then
							hash_table.remove (k)
						end
						found := True
					end
					tree.remove
				else
					tree.forth
				end
			end
			if tree.valid_cursor (tc) then
				tree.go_to (tc)
			else
				tree.start
			end
		end

	--|--------------------------------------------------------------

	remove_all_by_key (k: H)
			-- Remove all items matching key 'k'
		require
			belongs: occurrences (k) /= 0
		local
			found: BOOLEAN
			tc: like tree_cursor
		do
			tc := tree.cursor
			from tree.start
			until tree.off or found
			loop
				if tree_item_key ~ k then
					found := True
					hash_table.remove (k)
					tree.remove
				else
					tree.forth
				end
			end
			if tree.valid_cursor (tc) then
				tree.go_to (tc)
			else
				tree.start
			end
		end

	--|--------------------------------------------------------------

	wipe_out
		-- Remove all items
		do
			tree.wipe_out
			hash_table.wipe_out
		end

--|========================================================================
feature -- Cursor movement
--|========================================================================

	start
		do
			tree.start
		end

	go_to (c: like tree_cursor)
		do
			tree.go_to (c)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	synch (v: G; k: H)
		local
			tl: detachable like ht_list
			tc: like tree_cursor
		do
			tc := tree.cursor
			tl := hash_table.item (k)
			if tl = Void then
				create tl.make
				hash_table.extend (tl, k)
			end
			-- Hash table items have no particular order
			tl.extend ([v, k, tc])
		end

--|========================================================================
feature {NONE} -- Structure
--|========================================================================

	hash_table: HASH_TABLE [like ht_list, H]
	tree: COMPACT_CURSOR_TREE [TUPLE [G,H]]

	ht_list (k: H): LINKED_LIST [TUPLE [G,H, like tree_cursor]]
		do
			Result := hash_table.item (k)
		end

	--|--------------------------------------------------------------
invariant
	tree_exists: tree /= Void
	bounded: count <= capacity
	consistent: tree.count = total_hash_table_count

end -- class AEL_DS_HASH_TABLE_TREE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class with an expected capacity.
--| Add, remove and access items as you would a HASH_TABLE
--|----------------------------------------------------------------------

