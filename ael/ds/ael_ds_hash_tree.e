class AEL_DS_HASH_TREE
-- Tree-like node/structure implemented as a hash table

inherit
	COMPARABLE
		redefine
			default_create
		end

create
	make_as_top, make_as_parent, make_with_data

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_as_top (nm: STRING)
		require
			valid_name: is_valid_node_name (nm)
		do
			default_create
			is_top := True
			make_as_parent (nm, Void)
		end

	make_as_parent (nm: STRING; p: detachable like Current)
			-- Create Current as parent-capable, with name 'nm'
		require
			valid_name: is_valid_node_name (nm)
			has_parent: not is_top implies p /= Void
		do
			default_create
			initialize_as_parent (nm, p)
		end

	make_with_data (nm: STRING; p: detachable like Current; v: like data)
			-- Create Current as a leaf node with name 'nm, and data 'v'
		require
			valid_name: is_valid_node_name (nm)
			has_parent: not is_top implies p /= Void
		do
			default_create
			data := v
			initialize (nm, p)
		end

	--|--------------------------------------------------------------

	initialize_as_parent (nm: STRING; p: detachable like Current)
			-- Initialize Current as parent-capable, with name 'nm'
		require
			valid_name: is_valid_node_name (nm)
			has_parent: not is_top implies p /= Void
			not_self: p /= Void implies p /= Current
			not_cycle: p /= Void implies p.parent /= Current
		do
			create children.make (7)
			initialize (nm, p)
		end

	initialize (nm: STRING; p: detachable like Current)
		require
			valid_name: is_valid_node_name (nm)
			has_parent: not is_top implies p /= Void
			not_self: p /= Void implies p /= Current
			not_cycle: p /= Void implies p.parent /= Current
		do
			parent := p
			name := nm
			is_initialized := True
		ensure
			initialized: is_initialized
		end

	default_create
		do
			name := ""
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	name: STRING
	parent: detachable like Current
	data: detachable ANY
	children: detachable AEL_DS_SORTABLE_HASH_TABLE [like Current, STRING]

	is_top: BOOLEAN
			-- Is Current the top of a tree?

	is_initialized: BOOLEAN
			-- Has Current been initialized?

	--|--------------------------------------------------------------

	has_data: BOOLEAN
		do
			Result := data /= Void
		ensure
			not_both: Result implies not is_parent_capable
		end

	is_parent_capable: BOOLEAN
			-- Is Current capable of accepting children?
		do
			Result := children /= Void
		end

	has_children: BOOLEAN
		do
			Result := attached children as lc and then not lc.is_empty
		ensure
			not_both: Result implies data = Void
		end

	number_of_children: INTEGER
		do
			if attached children as lc then
				Result := lc.count
			end
		end

	has_child_by_name (v: STRING): BOOLEAN
			-- Does current have a child with name 'v'
		do
			Result := child_by_name (v) /= Void
		end

--|========================================================================
feature -- Access
--|========================================================================

	child_by_name (v: STRING): detachable like Current
		require
			capable: is_parent_capable
		do
			if attached children as lc then
				Result := lc.item (v)
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (c: like Current)
		require
			can_have_children: is_parent_capable
			child_exists: c /= Void
			is_new: not has_child_by_name (c.name)
		do
			if attached children as lc then
				lc.extend (c, c.name)
			end
		ensure
			added: has_child_by_name (c.name)
			counted: number_of_children = old number_of_children + 1
		end

	--|--------------------------------------------------------------

	remove_child_by_name (nm: STRING)
		require
			valid_name: nm /= Void
			has_child: has_child_by_name (nm)
		do
		ensure
			gone: number_of_children = old number_of_children - 1
			forgotten: not has_child_by_name (nm)
		end

--|========================================================================
feature -- Add Comment
--|========================================================================

	is_valid_node_name (v: STRING): BOOLEAN
			-- Is 'v' a valid node name?
		do
			Result := v /= Void and then not v.is_empty
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := name < other.name
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant
	has_parent: not is_top implies parent /= Void
	parent_has_child:  attached parent as invp implies
		invp.has_child_by_name (name) and
		invp.child_by_name (name) = Current

end -- class AEL_DS_HASH_TREE
