
INCOMPLETE  NEED TO FINISH MAKE ROUTINES, ETC.

note
	description: "{
An array-like collection that actually HAS an array (vs being one)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2016 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_INDIRECT_ARRAY [G]

create
	make_empty,
	make_filled,
	make_from_array

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_empty
			-- Create Current without bounds or content
		do
			create array.make_empty
			default_create
		end

	make_filled (v: G; lb, ub: INTEGER)
			-- Create Current with lower bound 'lb' and upper bound 'ub',
			-- filled with 'v'
		do
			create array.make_filled (v)
			default_create
		end

	--|--------------------------------------------------------------

	core_make
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	lower: INTEGER
			-- lower bound (lowest index)
		do
			Result := array.lower
		end

	upper: INTEGER
			-- Upper bound (highest index)
		do
			Result := array.upper
		end

	--|--------------------------------------------------------------

	count: INTEGER
			-- Number of items in array
		do
			Result := array.count
		end

	is_empty: BOOLEAN
			-- Is the array empty?
		do
			Result := count = 0
		end

	--|--------------------------------------------------------------

	has (v: like item): BOOLEAN
			-- Does the array have the given item?
			-- Object or reference; reference by default
		do
			Result := array.has (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item alias "[]", at alias "@" (i: INTEGER): G assign put
			-- Entry at index 'i', if in index interval.
		require
			valid_index: i >= lower and i <= upper
		do
			Result := array.item (i)
		end

	first: G
			-- First item in array
		require
			not_empty: not is_empty
		do
			Result := array.item (lower)
		end

	last: G
			-- Last item in array
		require
			not_empty: not is_empty
		do
			Result := array.item (upper)
		end

	i_th (i: INTEGER): G
			-- Item at 'i'-th position
		do
			Result := item (i)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put (v: like item; i: INTEGER)
			-- Replace 'i'-th entry, if in index interval, by 'v'.
		require
			valid_index: valid_index (i)
		do
			array.put (v, i)
		end

	--|--------------------------------------------------------------

	enter (v: like item; i: INTEGER)
			-- Replace 'i'-th entry, if in index interval, by 'v'.
		require
			valid_index: valid_index (i)
		do
			put (v, i)
		end

	--|--------------------------------------------------------------

	force (v: like item; i: INTEGER)
			-- Assign item 'v' to 'i'-th entry.
			-- Resize the array if 'i' falls out of currently defined 
			-- bounds, preserving existing items.
			-- In void-safe mode, if ({G}).has_default does not hold,
			-- then you can only insert between
			-- 'lower - 1' or 'upper + 1' position in the ARRAY.
		require
			has_default_if_too_low:
				(i < lower - 1 and lower /= {like lower}.min_value) implies
					({G}).has_default
			has_default_if_too_high:
				(i > upper + 1 and upper /= {like upper}.max_value) implies
					({G}).has_default
		do
			array.force (v, i)
		end

	--|--------------------------------------------------------------

	fill_with (v: G)
			-- Set items between 'lower' and 'upper' with 'v'.
		do
			array.fill_with (v)
		end

	--|--------------------------------------------------------------

	subcopy (other: ARRAY [like item]; spos, epos, index_pos: INTEGER)
			-- Copy items of 'other' within bounds 'spos' and 'epos'
			-- to current array starting at index 'index_pos'.
		require
			other_not_void: other /= Void
			valid_start_pos: start_pos >= other.lower
			valid_end_pos: end_pos <= other.upper
			valid_bounds: start_pos <= end_pos + 1
			valid_index_pos: index_pos >= lower
			enough_space: (upper - index_pos) >= (end_pos - start_pos)
		do
			array.subcopy (other, spos, epos, index_pos)
		end

	--|--------------------------------------------------------------

	shuffle
			-- Shuffle items in Current in pseudo-random manner
		local
			i, lim: INTEGER
		do
			-- The fisher-yates algorithm, kinda
			lim := upper
			from i := lower
			until i > upper
			loop
				-- Swap item with random later item
				--  swap(a, i, i+randn(n-1-i));
				i := i + 1
			end
		end

	swap (p1, p2: INTEGER)
			-- Swap items at indices 'p1' and 'p2'
		require
			valid_indices: is_valid_index (p1) and is_valid_index (p2)
		local
			tv: like item
		do
			tv := array.item (p1)
			array.put (array.item (p2), p1)
			array.put (tv, p2)
		ensure
			swapped: item (p1) = old (item (p2) and item (p2) = old (item (p1)
		end

 --|========================================================================
feature -- Iteration
 --|========================================================================

	do_all (action: PROCEDURE [G])
			-- Apply 'action' to every item, from first to last.
			-- Semantics not guaranteed if 'action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
		do
			array.do_all (action)
		end

	do_if (action: PROCEDURE [G]; test: FUNCTION [G, BOOLEAN])
			-- Apply 'action' to every item that satisfies 'test',
			-- from first to last.
			-- Semantics not guaranteed if 'action' or 'test' changes
			-- the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
			test_not_void: test /= Void
		do
			array.do_if (action, test)
		end

	there_exists (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is 'test' true for at least one item?
		require
			test_not_void: test /= Void
		do
			Result := array.there_exists (test)
		end

	for_all (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is 'test' true for all items?
		require
			test_not_void: test /= Void
		do
			Result := array.for_all (test)
		end

	do_all_with_index (action: PROCEDURE [G, INTEGER])
			-- Apply 'action' to every item, from first to last.
			-- 'action' receives item and its index.
			-- Semantics not guaranteed if 'action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		do
			array.do_all_with_index (action)
		end

	do_if_with_index (
		action: PROCEDURE [G, INTEGER];
		test: FUNCTION [G, INTEGER, BOOLEAN])
			-- Apply 'action' to every item that satisfies 'test',
			-- from first to last.
			-- 'action' and 'test' receive the item and its index.
			-- Semantics not guaranteed if 'action' or 'test' changes
			-- the structure;
			-- in such a case, apply iterator to clone of structure instead.
		do
			array.do_if_with_index (action, test)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	discard_items
			-- Reset all items to default values with reallocation.
		require
			has_default: ({G}).has_default
		do
			array.discard_items
		end

	clear_all
			-- Reset all items to default values.
		require
			has_default: ({G}).has_default
		do
			array.clear_all
		end

	keep_head (n: INTEGER)
			-- Remove all items except for the first 'n';
			-- do nothing if 'n' >= 'count'.
		require
			non_negative_argument: n >= 0
		do
			array.keep_head (n)
		end

	keep_tail (n: INTEGER)
			-- Remove all items except for the last 'n';
			-- do nothing if 'n' >= 'count'.
		require
			non_negative_argument: n >= 0
		do
			array.keep_tail (n)
		end

	remove_head (n: INTEGER)
			-- Remove first 'n' items;
			-- if 'n' > 'count', remove all.
		require
			n_non_negative: n >= 0
		do
			array.remove_head (n)
		end

	remove_tail (n: INTEGER)
			-- Remove last 'n' items;
			-- if 'n' > 'count', remove all.
		require
			n_non_negative: n >= 0
		do
			array.remove_tail (n)
		end

 --|========================================================================
feature -- Resizing
 --|========================================================================

	grow (i: INTEGER)
			-- Change the capacity to at least 'i'.
		do
			array.grow (i)
		end

	conservative_resize_with_default (dflt: G; min_index, max_index: INTEGER)
			-- Rearrange array so that it can accommodate
			-- indices down to 'min_index' and up to 'max_index'.
			-- Do not lose any previously entered item.
		require
			good_indices: min_index <= max_index
		do
			array.conservative_resize_with_default (dflt, min_index, max_index)
		end

	trim
		do
			array.trim
		end

	rebase (a_lower: like lower)
			-- Without changing the actual content of 'Current',
			-- set 'lower' to 'a_lower' and 'upper' accordingly,
			-- to 'a_lower + count - 1'.
		do
			array.rebase (a_lower)
		end

 --|========================================================================
feature -- Conversion
 --|========================================================================

	to_c: ANY
			-- Address of actual sequence of values in array,
			-- for passing to external (non-Eiffel) routines.
		require
			not_is_dotnet: not {PLATFORM}.is_dotnet
		do
			Result := array.to_c
		end

	to_cil: NATIVE_ARRAY [G]
			-- Address of actual sequence of values,
			-- for passing to external (non-Eiffel) routines.
		require
			is_dotnet: {PLATFORM}.is_dotnet
		do
			Result := array.to_cil
		end

	--|--------------------------------------------------------------

	to_special: SPECIAL [G]
			-- 'area' of array.
		do
			Result := array.to_special
		end

	linear_representation: LINEAR [G]
			-- Representation as a linear structure.
		do
			Result := array.linear_representation
		end

 --|========================================================================
feature -- Duplication
 --|========================================================================

	copy (other: like Current)
			-- Reinitialize by copying all the items of 'other'.
			-- (This is also used by 'clone'.)
		do
			array.copy (other.array)
		end

	--|--------------------------------------------------------------

	subarray (start_pos, end_pos: INTEGER): ARRAY [G]
			-- Array made of items of current array within
			-- bounds 'start_pos' and 'end_pos'.
		require
			valid_start_pos: valid_index (start_pos)
			valid_end_pos: end_pos <= upper
			valid_bounds: (start_pos <= end_pos) or (start_pos = end_pos + 1)
		do
			Result := array.subarray (start_pos, end_pos)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid index in Current?
		do
			Result := v >= array.lower and v <= array.upper
		end

--|========================================================================
feature {AEL_DS_INDIRECT_ARRAY} -- Structure
--|========================================================================

	array: ARRAY [G]
			-- Actual array object

	--|--------------------------------------------------------------
invariant
	array_exists: array /= Void

end -- class AEL_DS_INDIRECT_ARRAY

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 26-Jul-2018
--|     Original module; Eiffel 8.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class as you would a normal ARRAY
--| Use typical ARRAY  routines to add, delete and access items.
--|----------------------------------------------------------------------

