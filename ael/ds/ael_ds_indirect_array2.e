note
	description: "{
A 2-dimensional array implemented as an array of columns, each of which is an array of items
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2016 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_INDIRECT_ARRAY2 [G]

inherit
	CONTAINER [G]
		redefine
			default_create, has
		end

create
	make_filled, make_filled_bounded

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_filled (v: G; nr, nc: INTEGER)
			-- Make a 2D Array with 'nr' rows and 'nc columns, with each 
			-- slot filled with 'v'
		require
			valid_row_count: nr > 0
			valid_column_count: nc > 0
		do
			make_filled_bounded (v, 1, nr, 1, nc)
		end

	--|--------------------------------------------------------------

	make_filled_bounded (v: G; rl, ru, cl, cu: INTEGER)
			-- Make a 2D Array with rows having lower bound 'rl' and 
			-- upper bound 'ru", with each column in each row having 
			-- lower bound 'cl' and upper bound 'cu', with each 
			-- slot filled with 'v'
			-- Must have at least 1 row and at least one column per row
		require
			valid_row_bounds: rl <= ru
			valid_column_bounds: cl <= cu
		local
			nr, nc, ri: INTEGER
		do
			nr := ru - rl + 1
			nc := cu - cl + 1
			default_value := v
			create dummy_column.make_filled (default_value, cl, cu)
			create rows.make_filled (dummy_column, rl, ru)
			height := nr
			width := nc
			default_create
			-- Now fill the rows with individual columns, replacing 
			-- the dummy used to create the rows array
			from ri := rl
			until ri > ru
			loop
				rows.put (new_column, ri)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	height: INTEGER
			-- Number of Rows in Current

	width: INTEGER
			-- Number of Columns in each Row of Current

	capacity, count: INTEGER
			-- Total number of cells in all rows and columns
		do
			Result := height * width
		end

	is_empty: BOOLEAN
			-- Are there no items in the table? NEVER

	is_resizing: BOOLEAN
			-- Is Current in the process of resizing right now?

	--|--------------------------------------------------------------

	rows_lower: INTEGER
			-- Lower bound of rows
		do
			Result := rows.lower
		end

	columns_lower: INTEGER
			-- Lower bound of columns
		do
			Result := rows.item (rows.lower).lower
		ensure
			matches_dummy: dummy_column.lower = Result
		end

	rows_upper: INTEGER
			-- Upper bound of rows
		do
			Result := rows.upper
		end

	columns_upper: INTEGER
			-- Upper bound of columns
		do
			Result := rows.item (rows.lower).upper
		ensure
			matches_dummy: dummy_column.upper = Result
		end

	--|--------------------------------------------------------------

	has (v: G): BOOLEAN
			-- Does Curent have 'v' in any cells?
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until Result or ri > rlim
			loop
				Result := rows.item (ri).has (v)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	deep_out: STRING
			-- String representation of Current
		local
			ri, rlim: INTEGER
		do
			create Result.make (capacity * 32)
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				Result.append (rows.item (ri).out)
				ri := ri + 1
			end
		ensure
			exists: attached Result
		end

	--|--------------------------------------------------------------

	dense_out: STRING
			-- A more descriptive string representation of Current
		local
			ri, rlim, ci, clow, clim: INTEGER
		do
			create Result.make (capacity * 32)
			clow := columns_lower
			clim := columns_upper
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				Result.append ("Row " + ri.out + "%N")
				if attached rows.item (ri) as tc then
					from ci := clow
					until ci > clim
					loop
						Result.append ("  Column " + ci.out + "%N")
						if attached tc.item (ci) as tcell then
							Result.append ("    Item: " + tcell.out + "%N")
						else
							Result.append ("    Item: Void%N")
						end
						ci := ci + 1
					end
				end
				ri := ri + 1
			end
		ensure
			exists: attached Result
		end

--|========================================================================
feature -- Resizing
--|========================================================================

	resize (dv: G; nr, nc, rlo, clo: INTEGER)
			-- Reconfigure Current to have 'nr' rows and 'nc' colummns,
			-- with lower row bound of 'rlo' and lower column bound of 
			-- 'clo' and filling previously unfilled (Void) slots 
			-- with default value 'dv'
			-- Do not lose any previously entered item.
			--
			-- New dimensions must be at least as large as the old ones
		require
			valid_row_count: nr >= height
			valid_column_count: nc >= width
		local
			ri, rlim, chi: INTEGER
		do
			is_resizing := True
			default_value := dv
			-- First, resize the rows with the existing colums
			chi := nc + clo - 1
			create dummy_column.make_filled (dv, clo, chi)
			-- Need to re-base rows before resizing to allow for 
			-- shifting
			rows.rebase (rlo)
			rows.conservative_resize_with_default (
				dummy_column, rlo, rlo + nr - 1)
			-- rows should now include old columns, at old sizes
			-- Now resize each column
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				if attached rows.item (ri) as tc then
					if tc = dummy_column then
						-- A new one, already has new size
					else
						-- Preexisting, needs resize
						-- Rebase first, else bounds violate rules for 
						-- resizing
						tc.rebase (clo)
						tc.conservative_resize_with_default (dv, clo, chi)
					end
				end
				ri := ri + 1
			end
			height := nr
			width := nc
			is_resizing := False
		end

	--|--------------------------------------------------------------

	rebase (rlower, clower: INTEGER)
			-- Without changing the actual content of Current,
			-- reset the lower bound of rows, to 'rlower' and reset each
			-- column in each row to 'clower'
			-- Accordingly, reset the upper bound of rows to (rlower + 
			-- height - 1) and the upper bound of each column in each 
			-- row to (clower + width - 1)
		do
			rebase_rows (rlower)
			rebase_columns (clower)
		end

	--|--------------------------------------------------------------

	rebase_rows (v: INTEGER)
			-- Without changing the actual content of Current,
			-- reset the lower bound of rows to 'v' and the upper bound
			-- of rows to 'v' + height - 1
		do
			rows.rebase (v)
		ensure
			rebased: rows.lower = v and rows.upper = (v + height - 1)
		end

	rebase_columns (v: INTEGER)
			-- Without changing the actual content of Current,
			-- reset the lower bound of each column in each row to 'v'
			-- and the upper bound of each column in row to 'v' + width - 
			-- 1
		local
			ri, rlim: INTEGER
		do
			dummy_column.rebase (v)
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				rows.item (ri).rebase (v)
				ri := ri + 1
			end
		ensure
			rebased: attached rows.item (rows.lower) as tc implies
				tc.lower = v and tc.upper = (v + width - 1)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (r, c: INTEGER): G
			-- Item in cell at row='r', col='c'
		require
			valid_row: is_valid_row_index (r)
			valid_column: is_valid_column_index (c)
		do
			Result := rows.item (r).item (c)
		end

	--|--------------------------------------------------------------

	linear_representation: ARRAYED_LIST [G]
			-- Representation as a linear structure
			-- Effective unrolls each column
			-- r1 [c1, c2, c3 ...]
			-- r2 [c1, c2, c3 ...]
			-- Becomes:
			-- {r1,c1} {r2,c2} .. {r2,c1} {r2,c2} .. {r3,c1} {r3,c2} ..
		local
			ri, rlim, ci, clim: INTEGER
		do
			create Result.make (height * width)
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				clim := columns_upper
				from ci := columns_lower
				until ci > clim
				loop
					Result.extend (item (ri, ci))
					ci := ci + 1
				end
				ri := ri + 1
			end
		ensure then
			oomplete: attached Result as tr implies tr.count = count
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put (v: G; r, c: INTEGER)
			-- Insert 'v' at row='r', column='c'
		require
			valid_row: is_valid_row_index (r)
			valid_column: is_valid_column_index (c)
		do
			rows.item (r).put (v, c)
		ensure
			added: item (r, c) = v
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	discard_items
			-- Remove all items
		require
			has_default: ({G}).has_default
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				rows.item (ri).discard_items
				ri := ri + 1
			end
		end

--|========================================================================
feature -- Iteration
--|========================================================================

	do_all (action: PROCEDURE [G])
			-- Apply `action' to every item, from first to last (r1,c1 r1,c2 ..
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				rows.item (ri).do_all (action)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	do_if (action: PROCEDURE [G]; test: FUNCTION [G, BOOLEAN])
			-- Apply `action' to every item that satisfies `test', from 
			-- first to last (r1,c1 r1,c2 .).
			-- Semantics not guaranteed if `action' or `test' changes 
			-- the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
			test_not_void: test /= Void
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				rows.item (ri).do_if (action, test)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	there_exists (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is `test' true for at least one item?
		require
			test_not_void: test /= Void
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until Result or ri > rlim
			loop
				Result := rows.item (ri).there_exists (test)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	for_all (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is `test' true for all items?
		require
			test_not_void: test /= Void
		local
			ri, rlim: INTEGER
		do
			rlim := rows.upper
			from ri := rows.lower
			until Result or ri > rlim
			loop
				Result := rows.item (ri).for_all (test)
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	do_all_with_indices (action: PROCEDURE [G, INTEGER, INTEGER])
			-- Apply `action' to every item, from first to last.
			-- `action' receives item and its row and column indices.
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure 
			-- instead.
		local
			ri, rlim, ci, clow, clim: INTEGER
		do
			clow := columns_lower
			clim := columns_upper
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				from ci := clow
				until ci > clim
				loop
					action.call ([rows.item (ri).item (ci), ri, ci])
					ci := ci + 1
				end
				ri := ri + 1
			end
		end

	--|--------------------------------------------------------------

	do_if_with_indices (
		action: PROCEDURE [G, INTEGER, INTEGER];
		test: FUNCTION [G, INTEGER, INTEGER, BOOLEAN])
			-- Apply `action' to every item that satisfies `test', from 
			-- first to last ( r1,c1, r1,c2 ..).
			-- `action' and `test' receive the item and its row/col indices.
			-- Semantics not guaranteed if `action' or `test' changes 
			-- the structure; in such a case, apply iterator to clone of 
			-- structure instead.
		local
			ri, rlim, ci, clow, clim: INTEGER
		do
			clow := rows.item (rows.lower).lower
			clim := rows.item (rows.lower).upper
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim
			loop
				from ci := clow
				until ci > clim
				loop
					if attached rows.item (ri) as tc then
						if test.item ([tc.item (ci), ri, ci]) then
							action.call ([tc.item (ci), ri, ci])
						end
					end
					ci := ci + 1
				end
				ri := ri + 1
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_row_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid row index for Current?
		do
			Result := v >= rows.lower and v <= rows.upper
		end

	is_valid_column_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid column index for Current?
		do
			Result := v >= dummy_column.lower and v <= dummy_column.upper
		end

	--|--------------------------------------------------------------

	columns_match_dummy: BOOLEAN
			-- Do the columsn in each row have the same bounds as the 
			-- dummy column?
		local
			ri, rlim, dlower, dupper: INTEGER
		do
			Result := True
			dlower := dummy_column.lower
			dupper := dummy_column.upper
			rlim := rows.upper
			from ri := rows.lower
			until ri > rlim or not Result
			loop
				Result := rows.item (ri).lower = dlower and
					rows.item (ri).upper = dupper
				ri := ri + 1
			end
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	rows: ARRAY [ARRAY [G]]
			-- Row dimension of Current
			-- Each row contains 'height' columns, each of which 
			-- contains 'width' cells

	dummy_column: ARRAY [G]
			-- Stand-in for any columns in Current

	new_column: ARRAY [G]
			-- Newly generated unique dummy-like column
		local
			dc: like dummy_column
		do
			dc := dummy_column
			create Result.make_filled (default_value, dc.lower, dc.upper)
		end

	default_value: G
			-- Value to which Current is initialized if created filled

	--|--------------------------------------------------------------
invariant
	dummy_matches: not is_resizing implies columns_match_dummy

end -- class AEL_DS_INDIRECT_ARRAY2

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 09-Nov-2016
--|     Created original module, adapting model of earlier 'indirect' 
--|     structures.  Still a bit thin at this point but the basics 
--|     are there on which to build (and to fix)
--|     Compiled and minimally tested using Eiffel 16.05,
--|     complete Void-safety, standard syntax
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class with a row count and column count
--| Access using row and colum indices
--|----------------------------------------------------------------------
