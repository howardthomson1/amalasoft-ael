--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class resembling a list, but actually having a list
--|----------------------------------------------------------------------

class AEL_DS_INDIRECT_LIST [G]

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (tl: like list)
			-- Create Current
		do
			list := tl
			core_make
		end

	--|--------------------------------------------------------------

	core_make
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	count: INTEGER
			-- Number of items in list
		do
			Result := list.count
		end

	is_empty: BOOLEAN
			-- Is the list empty?
		do
			Result := count = 0
		end

	--|--------------------------------------------------------------

	has (v: like item): BOOLEAN
			-- Does the list have the given item?
			-- Object or reference; reference by default
		do
			Result := list.has (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: G
			-- Item at cursor
		do
			Result := list.item
		end

	first: G
			-- First item in list
		do
			Result := list.first
		end

	last: G
			-- Last item in list
		do
			Result := list.last
		end

	i_th alias "[]", at alias "@" (i: INTEGER): G
			-- Item at `i'-th position
		do
			Result := list.i_th (i)
		end

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

	cursor: CURSOR do Result := list.cursor end
			-- List cursor

	valid_cursor (v: like cursor): BOOLEAN
			-- Is the given cursor valid?
		do
			Result := list.valid_cursor (v)
		end

	start do list.start end
			-- Set the list cursor to the start of the list

	finish do list.finish end
			-- Set the list cursor to the end of the list

	forth do list.forth end
			-- Advance the list cursor one position

	back do list.back end
			-- Retreat the list cursor one position

	after: BOOLEAN do Result := list.after end
			-- Is the list cursor past the end?

	before: BOOLEAN do Result := list.before end
			-- Is the list cursor off the beginning?

	exhausted: BOOLEAN do Result := list.exhausted end
			-- Is the list exhausted?

	go_to (oc: CURSOR) do list.go_to (oc) end
			-- Reset the list cursor to 'oc'

	go_i_th (v: INTEGER) do list.go_i_th (v) end
			-- Reset the list cursor to position 'v'

	search (v: G) do list.search (v) end
			-- Search for item 'v' (object or reference) from current
			-- position

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G)
			-- Add item 'v' to end of list
		do
			list.extend (v)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	wipe_out
			-- Remove all items
		do
			list.wipe_out
		end

	--|--------------------------------------------------------------

	remove
			-- Remove item at cursor
		do
			list.remove
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	compare_objects
			-- Use object comparson for searches
		do
			list.compare_objects
		end

	compare_references
			-- Use reference comparson for searches
		do
			list.compare_references
		end

--|========================================================================
feature {AEL_DS_INDIRECT_LIST} -- Structure
--|========================================================================

	list: LIST [G]
			-- Actual list object

--|========================================================================
feature -- Testing support
--|========================================================================

	list_is_same_object_as (v: LIST [G]): BOOLEAN
			-- Is 'list' the same object as 'v'?
		do
			Result := list = v
		end

	--|--------------------------------------------------------------
invariant
	list_exists: list /= Void

end -- class AEL_DS_INDIRECT_LIST

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class with an existing proper descendent 
--| of LIST.  Use typical LIST  routines to add, delete and access items.
--|----------------------------------------------------------------------

