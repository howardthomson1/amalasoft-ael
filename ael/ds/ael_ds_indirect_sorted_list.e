--|----------------------------------------------------------------------
--| Copyright (c) 1995-2013, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class resembling a sorted list, but actually having a sorted list
--|----------------------------------------------------------------------

class AEL_DS_INDIRECT_SORTED_LIST [G->COMPARABLE]

inherit
	AEL_DS_INDIRECT_LIST [G]
		redefine
			list
		end

create
	make

--|========================================================================
feature {AEL_DS_INDIRECT_SORTED_LIST} -- Structure
--|========================================================================

	list: SORTED_LIST [G]
			-- Actual list object

end -- class AEL_DS_INDIRECT_SORTED_LIST

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 05-Jun-2013
--|     Created original module
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class with an existing proper descendent 
--| of SORTED_LIST.  Use typical SORTED_LIST  routines to add, delete 
--| and access items.
--|----------------------------------------------------------------------

