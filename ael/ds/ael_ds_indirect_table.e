--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class resembling a hash table, but actually having a hash table
--|----------------------------------------------------------------------

class AEL_DS_INDIRECT_TABLE [G, H -> HASHABLE]

inherit
	CONTAINER [G]
		rename
			has as has_item
		redefine
			default_create, out
		end

create
	make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (n: INTEGER)
			-- Make a table with a capacity of at least 'n' items
		require
			valid_size: is_valid_size (n)
		do
			default_create
			make_table (n)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		do
			Precursor
		end

	--|--------------------------------------------------------------

	make_table (n: INTEGER)
		require
			valid_size: n >= 0
		do
			create table.make (n)
		end

--|========================================================================
feature -- Status
--|========================================================================

	count: INTEGER
			-- Number of items in table
		do
			Result := table.count
		end

	is_empty: BOOLEAN
			-- Are there no items in the table?
		do
			Result := count = 0
		end

	off: BOOLEAN
			-- Is cursor before start or after end?
		do
			Result := table.off
		end

	--|--------------------------------------------------------------

	has (k: H): BOOLEAN
			-- Does the table have an item with key 'k'?
		do
			Result := table.has (k)
		end

	has_item (v: G): BOOLEAN
			-- Does the table have item 'v'?
		do
			Result := linear_representation.has (v)
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of Current
		do
			create Result.make (1024)
			Result.append ("Count: " + count.out + "%N")
			from start
			until after
			loop
				forth
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (k: H): detachable G
			-- Item in table with key 'k'
		do
			Result := table.item (k)
		end

	--|--------------------------------------------------------------

	item_for_iteration: detachable G
			-- Item at current cursor position
		do
			Result := table.item_for_iteration
		end

	key_for_iteration: detachable H
			-- Key at current iteration position
		require
			not_off: not off
		do
			Result := table.key_for_iteration
		end

	linear_representation: ARRAYED_LIST [G]
			-- Representation as a linear structure
		do
			Result := table.linear_representation
		end

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

	cursor: CURSOR do Result := table.cursor end
			-- Table cursor

	new_cursor: HASH_TABLE_ITERATION_CURSOR [G, H]
		do
			Result := table.new_cursor
		end

	start do table.start end
			-- Set table to start position

	forth do table.forth end
			-- Advance table one position

	after: BOOLEAN do Result := table.after end
			-- Is table after?

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: H)
			-- Add item 'v' to table using key 'k'
		require
			exits: v /= Void
			key_exits: k /= Void
			is_new: not has (k)
		do
			table.extend (v, k)
		ensure
			added: has (k)
		end

	--|--------------------------------------------------------------

	force (v: G; k: H)
			-- Add a new item, even if one exists with same key
		require
			exits: v /= Void
			key_exits: k /= Void
		do
			if has (k) then
				remove (k)
			end
			extend (v, k)
		end

	--|--------------------------------------------------------------

	put (v: G; k: H)
			-- Add a new item if it's not already in the table
		require
			exits: v /= Void
			key_exits: k /= Void
		do
			if not has (k) then
				extend (v, k)
			end
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	wipe_out
			-- Remove all items
		do
			table.wipe_out
		end

	--|--------------------------------------------------------------

	remove (k: H)
			-- Remove item with given key
		require
			belongs: has (k)
		do
			table.remove (k)
		ensure
			removed: not has (k)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_size (v: INTEGER): BOOLEAN
			-- Is 'v' a valid size for Current?
		do
			Result := v >= 0
		end

--|========================================================================
feature {AEL_DS_INDIRECT_TABLE} -- Structure
--|========================================================================

	table: HASH_TABLE [G, H]
			-- Actual table
	--|--------------------------------------------------------------
invariant
	table_exists: table /= Void

end -- class AEL_DS_INDIRECT_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class with a capacity
--| Use typical HASH_TABLE routines to add, delete and access items.
--|----------------------------------------------------------------------
