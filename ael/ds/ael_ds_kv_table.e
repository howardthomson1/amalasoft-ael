--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A table of tag-value pairs, searchable by tag, and with a
--| projection sorted by tag
--|----------------------------------------------------------------------

class AEL_DS_KV_TABLE [G->AEL_SPRT_TV_PAIR create make_from_tag_and_value end]

inherit
	AEL_DS_SORTABLE_HASH_TABLE [G, STRING]
		rename
			make as aht_make,
			extend as aht_extend,
			replace as aht_replace,
			force as aht_force,
			put as aht_put
		export
			{NONE} All
			{AEL_DS_KV_TABLE} indexes_map,
			aht_extend, aht_put, aht_replace, aht_force, new_cursor
			{ANY} count, is_empty, extendible, has, contents_sorted, contents,
			twin, deep_twin, same_type, is_deep_equal, start, after, forth, off,
			item_for_iteration, key_for_iteration, merge, wipe_out, prunable,
			is_equal, compare_objects, object_comparison,
			changeable_comparison_criterion, search, found, found_item
			{AEL_DS_SORTABLE_HASH_TABLE} keys, content, deleted_marks,
			standard_is_equal, has_default, soon_full, capacity,
			iteration_position, valid_key,
			item_position, deleted_item_position
		redefine
			out
--, items_by_range
--			item, at,
-- aht_extend, aht_replace, linear_representation, aht_put, aht_force, content
		end

create
	make, make_from_stream

create {AEL_DS_KV_TABLE}
	aht_make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			aht_make (7)
		end

	--|--------------------------------------------------------------

	make_from_stream (v, sep: STRING; qf: BOOLEAN)
			-- Create Current from string 'v' where 'sep' separates
			-- individual tag value pairs.
			-- if qf then strip off enclosing quotes from pairs.
		require
			exists: v /= Void
			sep_exists: sep /= Void and then not sep.is_empty
		local
			tl: LIST [STRING]
			ti: G
			tv: AEL_SPRT_TV_PAIR
			asr: AEL_SPRT_STRING_ROUTINES
		do
			make
			create asr
			if sep.count = 1 then
				tl := v.split (sep.item (1))
			else
				tl := asr.substring_split (v, sep)
			end
			from tl.start
			until tl.exhausted
			loop
				if is_string_valid_tv_pair (tl.item) then
					create tv.make_from_string (tl.item)
					if qf then
						create ti.make_from_tag_and_value (
							tv.tag, asr.quotes_stripped (tv.value))
					else
						create ti.make_from_tag_and_value (tv.tag, tv.value)
					end
					extend (ti)
				end	
				tl.forth
			end
		end

--|========================================================================
feature -- Queries
--|========================================================================

	value_for_tag (v: STRING): STRING
			-- Value associated with given tag, if any
			-- If no items by that tag exist, an empty string
		do
			Result := ""
			if attached item (v) as ti then
				Result := ti.value
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	has_tag (v: STRING): BOOLEAN
			-- Does Current include the tag 'v'?
		do
			Result := has (v)
		end

	--|--------------------------------------------------------------

	first_value: STRING
			-- The value associated with the first item
			-- 'first' refers to sort order, not chronology
		require
			not_empty: not is_empty
		do
			check attached first_item as ti then
				Result := ti.value
			end
		end

	--|--------------------------------------------------------------

	first_item: like item_for_iteration
			-- The 'first' item in Current
			-- 'first' refers to SORT order, not chronology
		require
			not_empty: not is_empty
		local
			cs: like contents_sorted
		do
			cs := contents_sorted
			check attached cs.first as ti then
				Result := ti
			end
		end

	--|--------------------------------------------------------------

	out: STRING
			-- Contents of table in string form; order undefined
		do
			Result := decorated_out ("","")
		end

	--|--------------------------------------------------------------

	decorated_out (pfx, sfx: STRING): STRING
			-- String representation of contents of Current, with
			-- optional prefix and suffix strings wrapping each
			-- item in the output
			-- Order is undefined
		require
			prefix_exists: pfx /= Void
			suffix_exists: sfx /= Void
		local
			ti: like item_for_iteration
		do
			create Result.make (16*count)
			from start
			until after
			loop
				ti := item_for_iteration
				Result.append (item_out (ti, pfx, sfx))
				forth
				if not after then
					Result.extend ('%N')
				end
			end
		end

	--|--------------------------------------------------------------

	out_in_order (pfx, sfx: STRING): STRING
			-- String representation of contents of Current, with
			-- optional prefix and suffix strings wrapping each
			-- item in the output
			-- Order is sorted by tag
		require
			prefix_exists: pfx /= Void
			suffix_exists: sfx /= Void
		local
			cs: like contents_sorted
		do
			create Result.make (16*count)
			cs := contents_sorted
			from cs.start
			until cs.exhausted
			loop
				if attached cs.item as ti then
					Result.append (item_out (ti, pfx, sfx))
				end
				cs.forth
				if not cs.exhausted then
					Result.extend ('%N')
				end
			end
		end

	--|--------------------------------------------------------------

	out_by_agent (
		fa: FUNCTION [TUPLE [like item_for_iteration], STRING]): STRING
			-- Contents of Current, formatted by supplied agent 'fa'
		local
			cs: like contents_sorted
		do
			create Result.make (16*count)
			cs := contents_sorted
			from cs.start
			until cs.exhausted
			loop
				if attached cs.item as ti then
					Result.append (item_agent_out (ti, fa, cs.islast))
				end
				cs.forth
			end
		end

--RFO 	linear_representation: ARRAYED_LIST [like item_for_iteration]
--RFO 			-- Representation as a linear structure
--RFO 		do
--RFO 			check attached {like linear_representation} Precursor as llr then
--RFO 				Result := llr
--RFO 			end
--RFO 		end

--|========================================================================
feature -- Alternate access
--|========================================================================

	--RFO items_by_range (sp, ep: INTEGER): TWO_WAY_LIST [like item_for_iteration]
	--RFO 		-- Items, as stored in Current, from positions
	--RFO 		-- 'sp' through 'ep'
	--RFO 		-- If 'sp' is greater than count, result is empty, not Void
	--RFO 		-- If 'ep' is greater than count, the end of range is 'count'
	--RFO 	do
	--RFO 		check attached Precursor (sp, ep) as tir then
	--RFO 			Result := tir
	--RFO 		end
	--RFO 	end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	item_out (ti: like item_for_iteration; pfx, sfx: STRING): STRING
		require
			item_exists: ti /= Void
			prefix_exists: pfx /= Void
			suffix_exists: sfx /= Void
		do
			if pfx.is_empty and sfx.is_empty then
				Result := ti.out
			else
				Result := ti.decorated_out (pfx, sfx)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	item_agent_out (
		ti: like item_for_iteration;
		fa: FUNCTION [TUPLE [like item_for_iteration], STRING];
		lf: BOOLEAN): STRING
			-- An item in Current, formatted by supplied agent 'fa'
		require
			item_exists: ti /= Void
			agent_exists: fa /= Void
		do
			Result := fa.item ([ti, lf])
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Content change
--|========================================================================

	extend (v: like item_for_iteration)
			-- Add item 'v' with v's tag as key
			-- Replace item with that tag if one exists
		do
			if v /= Void then
				if has (v.tag) then
					aht_replace (v, v.tag)
				else
					aht_extend (v, v.tag)
				end
			end
		end

	--|--------------------------------------------------------------

	add_item (t, v: STRING)
			-- Add a new item to Current, with tage 't' and value 'v'
		require
			valid: tag_and_value_are_valid (t, v)
		local
			ti: G
		do
			create ti.make_from_tag_and_value (t, v)
			extend (ti)
		end

--|========================================================================
feature -- Item removal
--|========================================================================

	remove_item_by_tag (v: STRING)
			-- Remove from Current the item with the given tag
		require
			tag_exists: v /= Void
			belongs: has (v)
		do
			remove (v)
		end

 --|========================================================================
feature {HASH_TABLE, HASH_TABLE_ITERATION_CURSOR} -- Implementation: content attributes and preservation
 --|========================================================================

--RFO 	content: SPECIAL [like item_for_iteration]
--RFO 			-- Array of contents

--|========================================================================
feature -- Validation
--|========================================================================

	tag_and_value_are_valid (t, v: STRING): BOOLEAN
			-- Is 't' a valid tag and is 'v' a valid value?
		do
			if t /= Void and v /= Void then
				Result := default_item.is_valid_tag (t)
			end
		end

	--|--------------------------------------------------------------

	is_string_valid_tv_pair (v: STRING): BOOLEAN
			-- Is the given string a valid tv pair?
		do
			if v /= Void then
				Result := default_item.is_tv_pair (v)
			end
		end

--|========================================================================
feature {AEL_DS_KV_TABLE} -- Compliant redefinitions
--|========================================================================

	--RFO item alias "[]" (key: like key_for_iteration): detachable like item_for_iteration
	--RFO 		-- Item associated with `key', if present
	--RFO 		-- otherwise default value
	--RFO 	note
	--RFO 		option: stable
	--RFO 	do
	--RFO 		Result := Precursor (key)
	--RFO 	end

	--RFO at alias "@" (key: like key_for_iteration): like item
	--RFO 		-- Item associated with `key', if present
	--RFO 		-- otherwise default value
	--RFO 	note
	--RFO 		option: stable
	--RFO 	do
	--RFO 		Result := Precursor (key)
	--RFO 	end

	--|--------------------------------------------------------------

--RFO 	aht_extend (v: like item_for_iteration; k: like key_for_iteration)
--RFO 			-- Assuming there is no item of key `k',
--RFO 			-- insert `v' with `k'.
--RFO 			-- Set `inserted'.
--RFO 		do
--RFO 			Precursor (v, k)
--RFO 		end

	--RFO aht_replace (v: like item_for_iteration; k: like key_for_iteration)
	--RFO 		-- Replace item at 'k', if present,
	--RFO 		-- with 'v'; do not change associated key.
	--RFO 		-- Set `replaced' if and only if a replacement has been made
	--RFO 		-- (i.e. 'k' was present); otherwise set `not_found'.
	--RFO 		-- Set `found_item' to the item previously associated
	--RFO 		-- with 'k' (default value if there was none).
	--RFO 	do
	--RFO 		Precursor (v, k)
	--RFO 	end

	--RFO aht_put (v: like item_for_iteration; k: like key_for_iteration)
	--RFO 		-- Insert `new' with `key' if there is no other item
	--RFO 		-- associated with the same key.
	--RFO 		-- Set `inserted' if and only if an insertion has
	--RFO 		-- been made (i.e. `key' was not present).
	--RFO 		-- If so, set `position' to the insertion position.
	--RFO 		-- If not, set `conflict'.
	--RFO 		-- In either case, set `found_item' to the item
	--RFO 		-- now associated with `key' (previous item if
	--RFO 		-- there was one, `new' otherwise).
	--RFO 		--
	--RFO 		-- To choose between various insert/replace procedures,
	--RFO 		-- see `instructions' in the Indexing clause.
	--RFO 	do
	--RFO 		Precursor (v, k)
	--RFO 	end

	--RFO aht_force (v: like item_for_iteration; k: like key_for_iteration)
	--RFO 		-- Update table so that 'v' will be the item associated
	--RFO 		-- with 'k'.
	--RFO 		-- If there was an item for that key, set `found'
	--RFO 		-- and set `found_item' to that item.
	--RFO 		-- If there was none, set `not_found' and set
	--RFO 		-- `found_item' to the default value.
	--RFO 		--
	--RFO 		-- To choose between various insert/replace procedures,
	--RFO 		-- see `instructions' in the Indexing clause.
	--RFO 	do
	--RFO 		Precursor (v, k)
	--RFO 	end

--|========================================================================
feature -- Support
--|========================================================================

--RFO 	new_item_from_string (s: detachable STRING; qf: BOOLEAN): G
--RFO 		do
--RFO 			if s = Void then
--RFO 				create {AEL_SPRT_TV_PAIR} Result.make
--RFO 			else
--RFO 				if qf then
--RFO 					create {AEL_SPRT_TV_PAIR} Result.make_from_string_unquote (s)
--RFO 				else
--RFO 					create {AEL_SPRT_TV_PAIR} Result.make_from_string (s)
--RFO 				end
--RFO 			end
--RFO 		end

--RFO 	new_item_from_tag_and_value (t, v: STRING): G
--RFO 		do
--RFO 			create {AEL_SPRT_TV_PAIR} Result.make_from_tag_and_value (t, v)
--RFO 		end

	--|--------------------------------------------------------------

	default_item: G
		do
			create Result.make_from_tag_and_value ("default_item", "default_item")
		end

	--|--------------------------------------------------------------
invariant

end -- class AEL_DS_KV_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using the make routine.
--| Add items using extend.
--| Use value_for_tag query (with tag string) to find items in the
--| list that match the tag.
--| Output format comes from multiple routines and offers some
--| tweaking by taking output prefix and/or suffix arguments.
--| If you want to mark local time, use either make or make_fine_grained.
--| If you want to mark UTC time, use either make_utc or make_fine_utc.
--| Because this is a descendent of STRING, the string representation
--| is simply Current and references to objects of this class can be
--| treated as if they were strings.
--| If you set the associated date bia set_from_date_time, be sure to
--| update the string rendition every time the external data object is
--| updated (if at all).  The string representation is kept up-to-date
--| automatically in all other cases.
--| This class depends on the Eiffel Base libraries
--|----------------------------------------------------------------------
