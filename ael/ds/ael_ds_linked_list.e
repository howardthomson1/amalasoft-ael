--|----------------------------------------------------------------------
--| Copyright (c) 1995-2011, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A linked list with additional features
--|----------------------------------------------------------------------

class AEL_DS_LINKED_LIST [G]

inherit
	LINKED_LIST [G]

create
	make

 --|========================================================================
feature -- Element change
 --|========================================================================

	extend_unique (v: like item)
			-- Add 'v' to Current if 'v' not already present
			-- Check follows object/reference comparison as defined
		do
			if not has (v) then
				extend (v)
			end
		end

	fill_unique (other: CONTAINER [G])
			-- Fill with as many items of `other' as are not already in Current
			-- Check follows object/reference comparison as defined
		require
			other_exists: other /= Void
		local
			lin: LINEAR [G]
			cc, oc: CURSOR
		do
			if is_empty then
				fill (other)
			else
				cc := cursor
				if attached {CURSOR_STRUCTURE [G]} other as cs and
					attached {LINEAR [G]} other as OL
				 then
					 oc := cs.cursor
					from ol.start
					until ol.after
					loop
						if not has (ol.item) then
							extend (ol.item)
						end
						ol.forth
					end
					if cs.valid_cursor (oc) then
						cs.go_to (oc)
					end
				else
					lin := other.linear_representation
					from lin.start
					until lin.off
					loop
						extend (lin.item)
						finish
						lin.forth
					end
				end
				go_to (cc)
			end
		end

end -- class AEL_DS_LINKED_LIST [G]

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 09-Mar-2011
--|     Created original
--|----------------------------------------------------------------------
--| How-to
--|
--| Create and manipulate an instance of this class as you would a 
--| linked list.  Use additional features as desired.
--|----------------------------------------------------------------------

