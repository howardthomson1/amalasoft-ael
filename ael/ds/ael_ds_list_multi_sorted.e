note
	description: "{
A two-way linked list supporting sorting by client-defined
item comparison agent or agents, and queries by client-defined
query parameters.
Items are not constrained to COMPARABLE
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2021 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_LIST_MULTI_SORTED [G]

inherit
	AEL_DS_LIST_AGENT_SORTED [G]
		rename
			make as make_single,
			comparison_function as solo_comparison_function
		redefine
			default_create, make_default, make_single,
			extend, split
		end

create
	make, make_default, make_single

create {TWO_WAY_LIST}
	make_sublist

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (cfl: like comparison_functions)
			-- Create Current, setting the comparison functions to 'cfl'
		do
			default_create
			set_comparison_functions (cfl)
		end

	make_single (cf: like dummy_comparison_agent)
		do
			default_create
			comparison_functions.wipe_out
			add_comparison_function (cf)
		end

	make_default
		do
			create comparison_functions.make
			Precursor
		end

	default_create
		do
			create comparison_functions.make
			Precursor
			comparison_functions.extend (agent dummy_comparison_function)
		end

	--|--------------------------------------------------------------

	make_sublist (
		first_item, last_item: like first_element; n: INTEGER;
		cfl: like comparison_functions)
			-- Create sublist
		do
			create comparison_functions.make
			twl_make_sublist (first_item, last_item, n)
			set_comparison_functions (cfl)
		end

 --|========================================================================
feature -- Access
 --|========================================================================

	has (v: G): BOOLEAN
			-- Does structure include `v'?
 			-- (Reference or object equality, based on `object_comparison'.)
		local
			pos: CURSOR
		do
			if not is_empty then
				pos := cursor
				start
				search (v)
				Result := not after
				go_to (pos)
			end
		end

	--|--------------------------------------------------------------

	search_after (v: like item)
			-- Go to first position with item greater
			-- than or equal to `v'.
		do
			from
				start
			until
				after or else less_or_equal (v, item)
			loop
				forth
			end
		ensure
			argument_less_than_item: (not after) implies less_or_equal (v, item)
		end

	--|--------------------------------------------------------------

	search_before (v: like item)
			-- Go to last position with item less
			-- than or equal to `v'.
		do
			from
				finish
			until
				before or else greater_or_equal (v, item)
			loop
				back
			end
		ensure
			(not off) implies less_or_equal (item, v)
		end

	--|--------------------------------------------------------------

	items_matching (mf: FUNCTION [G, BOOLEAN]): like Current
			-- Items in Current that comply with matching function 'mf'
		local
			oc: like cursor
		do
			create Result.make (comparison_functions)
			oc := cursor
			from start
			until after
			loop
				if mf.item (item) then
					Result.extend (item)
				end
				forth
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	items_by_query (
		mf: FUNCTION [G, BOOLEAN]
		cfl: like comparison_functions
						): like Current
			-- Items in Current that comply with matching function 'mf',
			-- and sorted according to comparison functions 'cfl'
		local
			oc: like cursor
		do
			create Result.make (cfl)
			oc := cursor
			from start
			until after
			loop
				if mf.item (item) then
					Result.extend (item)
				end
				forth
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

 --|========================================================================
feature -- Element change
 --|========================================================================

	extend (v: like item)
			-- Put `v' at proper position in list.
			-- The cursor ends up on the newly inserted
			-- item.
		do
			if is_empty then
				Precursor (v)
				start
			elseif not has_comparison_functions then
				Precursor (v)
				finish
			else
				-- Put new item directly before first existing item 
				-- that is not less than new item
				search_after (v)
				put_left (v)
				back
			end
		ensure then
	 		remains_sorted: (old is_sorted) implies is_sorted
			item_inserted: item = v
		end

	--|--------------------------------------------------------------

	merge (other: LINEAR [G])
			-- Add all items from `other' at their proper positions.
		do
			from
				other.start
			until
				other.off
			loop
				extend (other.item)
				other.forth
			end
		ensure then
	 		remains_sorted: (old is_sorted) implies is_sorted
		end


	--|--------------------------------------------------------------

	prune_all (v: like item)
			-- Remove all items identical to `v'.
			-- (Reference or object equality,
			-- based on `object_comparison'.)
			-- Leave cursor `off'.
		do
			from
				start
				search (v)
			until
				off or else less (v, item)
			loop
				remove
			end
			if not off then finish; forth end
		end


	--|--------------------------------------------------------------

	split (n: INTEGER)
			-- Remove from current list
			-- min (`n', `count' - `index' - 1) items
			-- starting at cursor position.
			-- Move cursor right one position.
			-- Make extracted sublist accessible
			-- through attribute `sublist'.
		local
			actual_number, active_index: INTEGER
			p_elem, s_elem, e_elem, n_elem: like first_element
		do
			if n = 0 then
					--just create new empty sublist
				create sublist.make (comparison_functions)
			else
					-- recognize first breakpoint
				active_index := index
				if active_index + n > count + 1 then
					actual_number := count + 1 - active_index
				else
					actual_number := n
				end
				s_elem := active
				p_elem := previous
					-- recognize second breakpoint
				move (actual_number - 1)
				e_elem := active
				n_elem := next
					-- make sublist
				if s_elem /= Void then
					s_elem.forget_left
				end
				if e_elem /= Void then
					e_elem.forget_right
				end
				create sublist.make_sublist (
					s_elem, e_elem, actual_number, comparison_functions)
					-- fix `Current'
				count := count - actual_number
				if p_elem /= Void then
					p_elem.put_right (n_elem)
				else
					first_element := n_elem
				end
				if n_elem /= Void then
					active := n_elem
				else
					last_element := p_elem
					active := p_elem
					after := True
				end
			end
		end

 --|========================================================================
feature -- Transformation
 --|========================================================================

	sort
			-- Sort all items.
			-- Has O(`count' * log (`count')) complexity.
			--| Uses comb-sort (BYTE February '91)
		local
			no_change: BOOLEAN
			gap: INTEGER
			left_cell, cell: like first_element
			left_cell_item, cell_item: like item
		do
			if not is_empty then
				from
					gap := count * 10 // 13
				until
					gap = 0
				loop
					from
						no_change := False
						go_i_th (1 + gap)
					until
						no_change
					loop
						no_change := True
						from
							left_cell := first_element
							cell := active	-- position of first_element + gap
						until
							cell = Void
						loop
							if left_cell /= Void then
								left_cell_item := left_cell.item
								cell_item := cell.item
								if less (cell_item, left_cell_item) then
									-- Swap `left_cell_item' with `cell_item'
									no_change := False
									cell.put (left_cell_item)
									left_cell.put (cell_item)
								end
								left_cell := left_cell.right
								cell := cell.right
							end
						end
					end
					gap := gap * 10 // 13
				end
			end
		end

 --|========================================================================
feature -- Status report
 --|========================================================================

	has_comparison_functions: BOOLEAN
		do
			if comparison_functions.count > 1 then
				Result := True
			elseif not comparison_functions.is_empty then
				Result := comparison_functions.first /= dummy_comparison_agent
			end
		end

	is_sorted: BOOLEAN
			-- Is the structure sorted?
		local
			c: CURSOR
			prev: like item
		do
			Result := True
			if count > 1 then
				from
					c := cursor
					start
					check not off end
					prev := item
					forth
				until
					after or not Result
				loop
					Result := less_or_equal (prev, item)
					prev := item
					forth
				end
				go_to (c)
			end
		end

	--|--------------------------------------------------------------

	min: like item
			-- Minimum item
		require
			not_empty: not is_empty
		do
			Result := first
		ensure
			min_is_first: Result = first
			-- smallest: For every item `it' in list, `Result' <= `it'
		end

	--|--------------------------------------------------------------

	max: like item
			-- Maximum item
		require
			not_empty: not is_empty
		do
			Result := last
		ensure
			max_is_last: Result = last
			-- largest: For every item `it' in list, `it' <= `Result'
		end

	--|--------------------------------------------------------------

	median: like item
			-- Median item
		require
			not_empty: not is_empty
		do
			Result := i_th ((count + 1) // 2)
		ensure
			median_definition: Result = i_th ((count + 1) // 2)
		end

--|========================================================================
feature -- Element comparison
--|========================================================================

	less (v1, v2: like item): BOOLEAN
			-- Is v1 less than v2?
		require
			agents_exist: attached comparison_functions
		do
			from comparison_functions.start
			until comparison_functions.after or Result
			loop
				comparison_functions.item.call (v1, v2)
				Result := comparison_functions.item.last_result < 0
				comparison_functions.forth
			end
		ensure then
			asymmetric: Result implies not less (v2, v1)
		end

	--|--------------------------------------------------------------

	less_or_equal (v1, v2: like item): BOOLEAN
			-- Is v1 less than or equal to v2?
		require
			exist: v1 /= Void and v2 /= Void
		do
			Result := not less (v2, v1)
		ensure then
			definition: Result = less (v1, v2) or are_equal (v1, v2)
		end

	are_equal (v1, v2: like item): BOOLEAN
			-- Is v1 less than or equal to v2?
		require
			exist: v1 /= Void and v2 /= Void
		do
			Result := True
			from comparison_functions.start
			until comparison_functions.after or not Result
			loop
				comparison_functions.item.call (v1, v2)
				Result := comparison_functions.item ([v1, v2]) = 0
				comparison_functions.forth
			end
		end

	--|--------------------------------------------------------------

	greater_or_equal (v1, v2: like item): BOOLEAN
			-- Is v1 greater than or equal to v2?
		do
			Result := not less (v1, v2)
 		ensure then
			definition: Result = less_or_equal (v2, v1)
		end

--|========================================================================
feature -- Sorting agents
--|========================================================================

	comparison_functions: LINKED_LIST [FUNCTION [G, G, INTEGER]]
			-- Functions by which sorting comparison is performed
			-- If first item is less than second (by whatever measure), 
			-- then it is placed before second, else is placed after.
			-- Result of each function is:
			--    Negative if first is less than second,
			--    Positive if first is greater than second, and
			--    Zero if first and second are equal
			--    (though less suffices, for sorting purposes)
			-- When list items are compared, functions are called in 
			-- order and the first call that produces a definitive 
			-- result terminates iteration of the functions


	dummy_comparison_function (v1, v2: G): INTEGER
			-- Do nothing; as if all items were equivalent
		do
		end

	dummy_comparison_agent: FUNCTION [G, G, INTEGER]
		do
			Result := agent dummy_comparison_function
		end

--|========================================================================
feature -- Sorting agent setting
--|========================================================================

	set_comparison_functions (v: detachable like comparison_functions)
		do
			comparison_functions.wipe_out
			if attached v as tv then
				comparison_functions.fill (tv)
			else
				comparison_functions.extend (agent dummy_comparison_function)
			end
		end

	set_comparison_function (v: like dummy_comparison_agent)
		do
			comparison_functions.wipe_out
			if attached v as tv then
				comparison_functions.extend (tv)
			else
				comparison_functions.extend (agent dummy_comparison_function)
			end
		end

	add_comparison_function (v: like dummy_comparison_agent)
		do
			comparison_functions.extend (v)
		end

 --|========================================================================
feature {AEL_DS_LIST_AGENT_SORTED} -- Implementation
 --|========================================================================

	new_chain: like Current
			-- A newly created instance of the same type.
			-- This feature may be redefined in descendants so as to
			-- produce an adequately allocated and initialized object.
		do
			create Result.make (comparison_functions)
		end

invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Mar-2021
--|     Adpapted original module from AEL_DS_LIST_AGENT_SORTED
--|     Compiled and tested, void-safe, using Eiffel 19.05
--|----------------------------------------------------------------------
--| How-to
--| Create with an agent or agents for comparing items
--| Use as you would a SORTED_TWO_WAY_LIST
--|
--| While it is possible to provide a single comparison function 
--| that, itself, conducts multiple compaisons, it is often a 
--| simpler, and more generally applicable, option to provide the
--| multiple comparison functions and let this class handle the 
--| iteration and consequences.
--|----------------------------------------------------------------------

end -- class AEL_DS_LIST_MULTI_SORTED
