note
	description: "{
A circular bounded priority queue implemented as an array.
Items are inserted from front to back (1 to capacity) and when full,
the oldest item is replaced by the next insertion and the cursors
(indices) are advanced.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_LRU_QUEUE

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (sz: INTEGER)
			-- Create Current with a capacity of 'sz'
		require
			valid_size: sz > 0
		do
			lower := 1
			upper := sz
			next_available_index := 1
			create array.make_filled (Void, 1, sz)
		ensure
			capacity_set: capacity = sz
			valid_lower: lower = 1
			valid_upper: upper >= lower
			initialized: is_in_initial_state
		end

--|========================================================================
feature -- Status
--|========================================================================

	lower: INTEGER
			-- Lower bound of acceptable indices (not related to count)

	upper: INTEGER
			-- Upper bound of acceptable indices (not related to count)

	capacity: INTEGER
			-- Number of items (max) that Current can hold
		do
			Result := upper - lower + 1
		end

	count: INTEGER
			-- Number of items in queue

	is_empty: BOOLEAN
			-- Is Current without any items?
		do
			Result := count = 0
		ensure
			extremes_consistent: Result implies not is_full
		end

	off: BOOLEAN
			-- Is Current's position presently off either end?
		do
			Result := oldest_index = 0
		ensure
			never_with_content: Result implies is_empty
		end

	is_full: BOOLEAN
			-- Does Current have 'capacity' items already?
		do
			Result := count = capacity
		ensure
			extremes_consistent: Result implies not is_empty
		end

	object_comparison: BOOLEAN
			-- Should item comparison be done by comparing objects?

--|========================================================================
feature -- Status Setting
--|========================================================================

	compare_objects
			-- Use object comparison when comparing items
		do
			object_comparison := True
		end

	compare_references
			-- Use reference comparison when comparing items
		do
			object_comparison := False
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: detachable like Kta_item
			-- Oldest item (Item at front of queue)
			-- N.B. is detachable to permit empty items; must not be 
			-- called when empty, so Result should always be attached.
		require
			not_off: not off
			not_empty: not is_empty
		do
			if oldest_index > 0 then
				Result := array.item (oldest_index)
			end
		ensure
			exists_if_not_empty: not is_empty implies Result /= Void
			none_if_empty: is_empty implies Result = Void
			is_oldest: Result /= Void implies Result.queue_index = oldest_index
		end

	--|--------------------------------------------------------------

	item_at_index (v: INTEGER): like item
			-- Item at index position 'v', if any
		require
			valid_index: is_valid_index (v)
		do
			Result := array [v]
		ensure
			valid_for_index: attached Result as tr implies tr.queue_index = v
		end

	--|--------------------------------------------------------------

	youngest: like item
			-- Youngest item (Item at back of queue) if any
		do
			if youngest_index > 0 then
				Result := array.item (youngest_index)
			end
		ensure
			non_empty_exists: (not is_empty) implies attached Result
			valid_exists: attached Result as ti
				implies ti.queue_index = youngest_index
		end

	--|--------------------------------------------------------------

	has_item (v: like Kta_item): BOOLEAN
			-- Does Current have item 'v'?
		require
			exists: v /= Void
		local
			i, lim: INTEGER
		do
			if not is_empty then
				lim := upper
				from i := 1
				invariant lim = upper
				until i > lim or Result
				loop
					Result := items_equal (array [i], v)
					i := i + 1
				variant (lim - i) + 1
				end
			end
		ensure
			item_has_index: Result implies
				items_equal (item_at_index (v.queue_index), v)
		end

	--|--------------------------------------------------------------

	has_item_by_key (v: STRING_GENERAL): BOOLEAN
			-- Does Current have item with key 'v'?
		require
			exists: v /= Void
		local
			i, lim: INTEGER
		do
			if not is_empty and then not v.is_empty then
				lim := upper
				from i := 1
				invariant lim = upper
				until i > lim or Result
				loop
					if attached array [i] as ti then
						Result := v.same_string (ti.key)
					end
					i := i + 1
				variant (lim - i) + 1
				end
			end
		end

	--|--------------------------------------------------------------

	has_item_ai (v: like Kta_item): BOOLEAN
			-- Does Current have item 'v'?
			-- Agent-based implementation example
		require
			exists: v /= Void
		do
			if not is_empty then
				Result := array.there_exists (agent items_equal (v, ?))
			end
		ensure
			item_has_index: Result implies item_at_index (v.queue_index) = v
		end

--RFO 	item_same (ti, v: like Kta_item): BOOLEAN
--RFO 			-- Is item 'ti' the same as item 'v'?
--RFO 		do
--RFO 			Result := ti = v
--RFO 		end

	--|--------------------------------------------------------------

	has_item_at_position (v: like Kta_item): BOOLEAN
			-- Does Current have item 'v' at v's queue index ?
		require
			exists: v /= Void
		do
			Result := is_valid_index (v.queue_index) and then
				items_equal (item_at_index (v.queue_index), v)
		ensure
			item_has_index: Result implies
				items_equal (item_at_index (v.queue_index), v)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	force (v: like Kta_item)
			-- Add 'v' to tail of queue (make it the youngest)
			-- If 'v' is already in the queue, then move it from its 
			-- current position to the tail, adjusting the positions of 
			-- the other items as needed to maintain consistency.
		require
			exists: v /= Void
		do
			if has_item (v) then
				promote (v)
			else
				extend (v)
			end
		ensure
			inserted: has_item (v)
			is_youngest: v = youngest
			counted_new: old has_item (v) implies count = old count + 1
			not_counted_not_new: (not old has_item (v)) implies count = old count
		end

	--|--------------------------------------------------------------

	extend (v: like Kta_item)
			-- Add 'v' to tail of queue (make it the youngest)
			-- New item must be unique.
			-- If desire is to re-insert an item already in the queue,
			-- then use 'promote' instead
		require
			exists: v /= Void
			is_unique: not has_item (v)
		local
			idx: INTEGER
		do
			idx := next_available_index
			if is_full then
				-- Replace oldest with newest, advance cursor
				check
					next_is_last: next_available_index = oldest_index
				end
				if attached item as ti then
					ti.clear_queue_index
				end
				if attached cache_eviction_proc as cp then
					cp.call ([v])
				end
				advance_oldest
			else
				if is_empty then
					oldest_index := 1
				end
				count := count + 1
			end
			next_available_index := index_incremented (next_available_index)
			last_insertion_index := idx
			youngest_index := idx

			array.put (v, idx)
			v.set_queue_index (idx)
		ensure
			inserted: has_item (v)
			is_youngest: v = youngest
			inserted_at_position: has_item_at_position (v)
			next_advanced: (next_available_index = old next_available_index + 1)
				or (next_available_index = 1)
			counted: (not old is_full) implies count = old count + 1
		end

	--|--------------------------------------------------------------

	promote (v: like Kta_item)
			-- Promote 'v' from its current position
			-- to tail of queue (make it the youngest),
			-- shifting other items as needed to maintain consistency.
		require
			exists: v /= Void
			belongs: has_item (v)
		local
			opos: INTEGER
		do
			opos := v.queue_index
			if opos = youngest_index then
				-- It's a noop;  Item is already youngest.
			elseif opos = oldest_index and is_full then
				-- Evict v before adding as youngest
				remove
				extend (v)
			else
				remove_item (v)
				extend (v)
					-- Restore next_available_index ????
			end
		ensure
			promoted: youngest = v
			index_reset: v.queue_index = last_insertion_index
			count_unchanged: count = old count
			last_inserted_youngest: last_insertion_index = youngest_index
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove, remove_oldest
			-- Remove the oldest item from Current
		require
			not_empty: not is_empty
			not_off: not off
		do
			if attached item as ti then
				remove_item (ti)
			end
		ensure
			gone: item /= old item
			forgotten: attached old item as oi implies oi.queue_index = 0
			counted: count = (old count - 1)
			cursors_adjusted: not is_empty implies
				is_advanced (old oldest_index, oldest_index)
			index_set: is_valid_index (last_removal_index)
		end

	--|--------------------------------------------------------------

	wipe_out
			-- Remove all items from Current
		do
			array.discard_items
			count := 0
			reset_cursors_and_markers
		ensure
			empty: is_empty
		end

	--|--------------------------------------------------------------

	cache_eviction_proc: detachable PROCEDURE [TUPLE[like Kta_item]]
			-- Procedure to call when an item is evicted, if any

	set_cache_eviction_procedure (v: like cache_eviction_proc)
			-- Set the procedure to call on item eviction
		do
			cache_eviction_proc := v
		ensure
			is_set: cache_eviction_proc = v
		end

--|========================================================================
feature -- Validation
--|========================================================================

	cursors_are_consistent: BOOLEAN
			-- Are queue cursors self-consistent?
		do
			--| If neither next_available_index nor oldest_index is within bounds,
			--| then cursor are certainly not valid (and therefore not
			--| consistent). Something is wrong.
			if next_available_index <= upper and oldest_index <= upper then
				--| next and oldest are each valid, w/r/t the queue
				--| Now check to see if they are consistent, based on the
				--| state of the queue
				if is_empty then
					--| If the queue is empty then all cursors must be in
					--| their initial state
					Result := next_available_index = 1
						and oldest_index = 0
							and last_insertion_index = 0
					if not Result then
						print (Void)
					end
				elseif is_full then
					--| If the queue is full, then next and oldest must be
					--| the same
					Result := next_available_index > 0 and
						next_available_index = oldest_index
					if not Result then
						print (Void)
					end
				else
					--| In normal operation, neither empty nor full, next
					--| and oldest must not collide, and they must be
					--| non-zero to indicate that they refer to an actual
					--| position in the queue
					Result := next_available_index > 0
						and oldest_index > 0
							and next_available_index /= oldest_index
					if not Result then
						print (Void)
					end
				end
				if Result and then count = 1 then
					Result := youngest_index = oldest_index
				end
			else
				--| for breakpoint convenience only
				print (Void)
			end
		end

	--|--------------------------------------------------------------

	is_valid_index (v: INTEGER): BOOLEAN
			-- Is 'v' a valid index into Current?
		do
			Result := v >= lower and v <= upper
		end

--|========================================================================
feature -- Resizing
--|========================================================================

	resize (sz: INTEGER)
			-- Resize (up or down) Current to the new size 'sz'
			-- Resizing down is possible only if there are few enough 
			-- items that they will fit within the new size.
			-- If desire is to resize down and there are too many items 
			-- to fit in the new smaller size, then call 'truncate' 
			-- instead.
		require
			valid_size: sz > 0
			contents_fits: count <= sz
		local
			old_array: detachable like array
			old_old, old_next, nidx: INTEGER
		do
			if is_empty then
				make (sz)
			else
				--| Recreate the contents at the new size, maintaining the 
				--| order and cursor consistency (not necessarily the 
				--| same cursor values).
				old_array := array
				create array.make_filled (Void, 1, sz)
				nidx := 1
				old_old := oldest_index
				old_next := next_available_index
				array [nidx] := old_array [old_old]
				old_old := index_incremented (old_old)
				from nidx := nidx + 1
				invariant
					old_next = next_available_index and nidx <= sz
				until old_old = old_next
				loop
					array [nidx] := old_array [old_old]
					old_old := index_incremented (old_old)
					nidx := nidx + 1
				variant (sz - nidx) + 1
				end
				youngest_index := nidx - 1
				lower := 1
				upper := sz
				oldest_index := 1
				if is_full then
					next_available_index := oldest_index
				else
					next_available_index := nidx - 1
				end
			end
		ensure
			capacity_set: capacity = sz
			valid_lower: lower = 1
			valid_upper: upper >= lower
			remade_empty: is_empty implies is_in_initial_state
			remade_occupied: not is_empty implies oldest_index = 1
			unwrapped: not is_wrapped
			same_count: count = old count
			contiguous: range_is_occupied (youngest_index, oldest_index)
		end

	--|--------------------------------------------------------------

	truncate (sz: INTEGER)
			-- Resize down Current to the new size 'sz'
			-- If there are too many items to fit in the new smaller 
			-- size, then remove enough items (from oldest to youngest) 
			-- to accommodate the new capacity.
		require
			valid_size: sz > 0
			smaller: sz < capacity
		do
			from
			invariant sz < capacity
			until count <= sz
			loop
				remove
			variant count + 1
			end
			resize (sz)
		ensure
			capacity_set: capacity = sz
			valid_count: count <= capacity
			valid_lower: lower = 1
			valid_upper: upper >= lower
			unwrapped: not is_wrapped
			contiguous: range_is_occupied (youngest_index, oldest_index)
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	is_advanced (c1, c2: INTEGER): BOOLEAN
			-- Is cursor postion 'c2' logically advanced from 'c1'?
		require
			valid_from: is_valid_index (c1)
			valid_to: is_valid_index (c2)
		do
			if c1 = upper then
				Result := c2 = 1
			else
				Result := c2 = (c1 + 1)
			end
		end

	--|--------------------------------------------------------------

	range_is_occupied (c1, c2: INTEGER): BOOLEAN
			-- Are positions between c1 and c2 (inclusive) occupied?
		require
			valid_start: is_valid_index (c1)
			valid_end: is_valid_index (c2)
		local
			idx, ep: INTEGER
		do
			if item_at_index (c1) /= Void then
				Result := True
				ep := index_incremented (c2)
				from idx := index_incremented (c1)
				until idx = ep or not Result
				loop
					Result := item_at_index (idx) /= Void
					idx := index_incremented (idx)
				end
			end
		ensure
			lower_at_least: Result implies item_at_index (c1) /= Void
			upper_at_least: Result implies item_at_index (c2) /= Void
		end

	--|--------------------------------------------------------------

	items_equal (i1, i2: detachable like Kta_item): BOOLEAN
			-- Are items 'i1' and 'i2' equal?
			-- If object_comparison, then are they equivalent,
			-- else, are they the same object
		do
			if i1 /= Void and i2 /= Void then
				if object_comparison then
					if i1 /= Void and i2 /= Void then
						Result := i1.is_equal (i2)
					end
				else
					Result := i1 = i2
				end
			end
		end

--|========================================================================
feature {NONE} -- Cursor management
--|========================================================================

	last_insertion_index: INTEGER
			-- Index of most recently inserted item

	last_removal_index: INTEGER
			-- Index of most recently removed item

	oldest_index: INTEGER
			-- Index of oldest item (zero if empty)

	youngest_index: INTEGER
			-- Index of youngest item (zero if empty)

	next_available_index: INTEGER
			-- Index at which to insert next item

	--|--------------------------------------------------------------

	is_in_initial_state: BOOLEAN
			-- Is Current in its initial state?
		do
			Result := is_in_reset_state and
				last_insertion_index = 0 and
				last_removal_index = 0 and
				count = 0
		end

	is_in_reset_state: BOOLEAN
			-- Is Current in its 'reset' state?
			-- i.e. are all cursors reset?
		do
			Result := 
				next_available_index = 1 and
				oldest_index = 0 and
				youngest_index = 0
		end

	--|--------------------------------------------------------------

	is_wrapped: BOOLEAN
			-- Are items wrapping around the end of the array?
		do
			if is_full then
				Result := oldest_index /= 1
			elseif not is_empty then
				Result := next_available_index < oldest_index
			end
		end

	--|--------------------------------------------------------------

	reset_cursors
			-- Reset cursors to initial values
		do
			next_available_index := 1
			oldest_index := 0
			youngest_index := 0
		ensure
			reset: is_in_reset_state
		end

	--|--------------------------------------------------------------

	reset_cursors_and_markers
			-- Reset cursors and markers to initial values
		do
			reset_cursors
			last_insertion_index := 0
			last_removal_index := 0
		ensure
			initialized: is_in_initial_state
		end

	--|--------------------------------------------------------------

	advance_oldest
			-- Advance the 'oldest' index to next item, if any
		require
			not_empty: not is_empty
			valid_before: is_valid_index (oldest_index)
		local
			idx: INTEGER
		do
			from idx := index_incremented (oldest_index)
			until array.item (idx) /= Void
			loop
				idx := index_incremented (idx)
			end
			oldest_index := idx
		ensure
			valid_after: is_valid_index (oldest_index)
			advanced: is_advanced (old oldest_index, oldest_index)
		end

	--|--------------------------------------------------------------

	advance_youngest
			-- Advance the 'youngest' index to next item, if any
		require
			not_empty: not is_empty
			valid_before: is_valid_index (youngest_index)
		local
			idx: INTEGER
		do
			from idx := index_incremented (youngest_index)
			until array.item (idx) /= Void
			loop
				idx := index_incremented (idx)
			end
			youngest_index := idx
		ensure
			valid_after: is_valid_index (youngest_index)
			advanced: is_advanced (old youngest_index, youngest_index)
		end

	--|--------------------------------------------------------------

	index_incremented (v: INTEGER): INTEGER
			-- Index 'v' advanced to next position
			-- Does not ensure that Result refers to a filled slot, 
			-- simply to a valid one
		do
			if v = upper then
				Result := 1
			else
				Result := v + 1
			end
		ensure
			incremented: is_advanced (v, Result)
		end

	--|--------------------------------------------------------------

	index_decremented (v: INTEGER): INTEGER
			-- Index 'v' regressed to previous position
			-- Does not ensure that Result refers to a filled slot, 
			-- simply to a valid one
		do
			if v = 1 then
				Result := upper
			else
				Result := v - 1
			end
		ensure
			decremented: is_advanced (Result, v)
		end

	--|--------------------------------------------------------------

	shift_items_backward (sp: INTEGER)
			-- Shift items in queue logically left, starting at index 
			-- 'sp' (an empty slot) maintaining cursor consistency.
			-- Shifting is necessary when an item is removed for promotion.
			--
			-- Because the queue is logically circular, arithmetic gets
			-- a bit more interesting.  As such, index arithmetic is done
			-- using functions.  This lets the logic act as if the list 
			-- is linear and unbounded (almost).
			--
			-- Example 1:
			--  Before removal (was full):
			--              youngest     oldest (=next)
			--                      |   |
			--                      |   |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  e   a   b   c   d   E   A   B   C   D   e   a   b   c   d
			--                          |
			--                           next
			--
			--  After removal (when this function is called called):
			--              youngest     oldest
			--                      |   |        sp = 4
			--                      |   |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  e   a   b   -   d   E   A   B   -   D   e   a   b   -   d
			--                          |
			--                           next
			--
			--  After shifting:
			--              youngest     oldest
			--                   <<     |
			--                  |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  -   a   b   d   E < -   A   B   D   e   -   a   b   d   e
			--                      | <
			--                       next
			-- -------------------------
			-- Example 2:
			--  Before removal (was NOT full):
			--          youngest         oldest (=next)
			--                  |       |
			--                  |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   b   c   d   _   A   B   C   D   _   a   b   c   d
			--                      |
			--                       next
			--
			--  After removal (when this function is called called):
			--          youngest         oldest (=next)
			--                  |       |        sp = 4
			--                  |       |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   b   -   d   _   A   B   -   D   _   a   b   -   d
			--                      |
			--                       next
			--
			--  After shifting:
			--          youngest         oldest (=next)
			--               <<         |
			--              |           |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   b   d < -   _   A   B   D   -   _   a   b   d   -
			--                  | <
			--                   next
			--
			-- N.B. Example 2 illustrates a shift on a queue that was 
			-- not full at the time of the promotion, but does not 
			-- illustrate that case when multiple items exist between the
			-- vacated slot (hole) and the oldest.  Example 3 does this.
			--
			-- -------------------------
			-- Example 3:
			--  Before removal (was NOT full):
			--          youngest         oldest (=next)
			--                  |       |
			--                  |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   b   c   d   _   A   B   C   D   _   a   b   c   d
			--                      |
			--                       next
			--
			--  After removal (when this function is called called):
			--          youngest         oldest (=next)
			--                  |       |    sp = 3
			--                  |       |   |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   -   c   d   _   A   -   C   D   _   a   -   c   d
			--                      |
			--                       next
			--
			--  After shifting:
			--          youngest         oldest (=next)
			--               <<         |
			--              |           |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   c < d < -   _   A   C   D   -   _   a   c   d   -
			--                  | <
			--                   next
			--
			-- -------------------------
			-- After shifting, 'hole' becomes logically one greater than 
			-- youngest and next becomes the same as the shifted 'hole'.
			-- Oldest remains unchanged.
			-- Youngest << 1
			-- Next becomes youngest + 1
			-- No holes between youngest and oldest (looking forward)
		require
			not_empty: not is_empty
			not_full: not is_full
			valid_last_removal: is_valid_index (last_removal_index)
			valid_start: sp = last_removal_index
			empty_start: item_at_index (sp) = Void
			not_oldest: sp /= oldest_index
			not_youngest: sp /= last_insertion_index
		local
			idx, ep, nidx: INTEGER
		do
			ep := oldest_index
			from idx := sp
			until idx = ep
			loop
				nidx := index_incremented (idx)
				array.put (array.item (nidx), idx)
				if attached array.item (idx) as ti then
					--| update the shifted item's index
					ti.set_queue_index (idx)
				end
				idx := nidx
			end
			last_removal_index := 0
			youngest_index := index_decremented (youngest_index)
			next_available_index := index_decremented (next_available_index)
		ensure
			count_unchanged: count = old count
			removal_index_cleared: last_removal_index = 0
			youngest_shifted_left:
				youngest_index = index_decremented (old youngest_index)
			oldest_unchanged: oldest_index = old oldest_index
			next_above_youngest: is_advanced (youngest_index, next_available_index)
			packed: range_is_occupied (youngest_index, oldest_index)
		end

	--|--------------------------------------------------------------

	shift_items_forward (sp: INTEGER)
			-- Shift items in queue logically right, starting at index 
			-- 'sp' (an empty slot) maintaining cursor consistency.
			-- Shifting is necessary when an item is removed for promotion.
			--
			-- Because the queue is logically circular, arithmetic gets
			-- a bit more interesting.  As such, index arithmetic is done
			-- using functions.  This lets the logic act as if the list 
			-- is linear and unbounded (almost).
			--
			-- Example 1:
			--  Before removal (was full):
			--              youngest     oldest (=next)
			--                      |   |
			--                      |   |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  e   a   b   c   d   E   A   B   C   D   e   a   b   c   d
			--                          |
			--                           next
			--
			--  After removal (when this function is called called):
			--              youngest     oldest
			--                      |   |        sp = 3
			--                      |   |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  e   a   b   -   d   E   A   B   -   D   e   a   b   -   d
			--                          |
			--                           next (invalid)
			--
			--  After shifting:
			--              youngest     oldest
			--                      |     >>
			--                      |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  e   -   a   b   d   e   - > A > B   D   e   -   a   b   d
			--                          | <
			--                           next
			-- -------------------------
			-- Example 2:
			--  Before removal (was NOT full):
			--          youngest         oldest (=next)
			--                  |       |
			--                  |       |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   b   c   d   _   A   B   C   D   _   a   b   c   d
			--                      |
			--                       next
			--
			--  After removal (when this function is called called):
			--          youngest         oldest (=next)
			--                  |       |    sp = 3
			--                  |       |   |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   a   -   c   d   _   A   -   C   D   _   a   -   c   d
			--                      |
			--                       next
			--
			--  After shifting:
			--          youngest         oldest (=next)
			--                  |         >>
			--                  |           |
			--  1   2   3   4   5   1   2   3   4   5   1   2   3   4   5
			--  _   - > a   c   d   _   - > A   C   D   _   -   a   c   d
			--                  | <
			--                   next
			--
			-- -------------------------
			-- After shifting, 'hole' becomes logically one greater than 
			-- youngest and next becomes the same as the shifted 'hole'.
			-- Youngest remains unchanged.
			-- Oldest >> 1
			-- Next remains inchanged
			-- No holes between youngest and oldest (looking forward)
		require
			not_empty: not is_empty
			not_full: not is_full
			valid_last_removal: is_valid_index (last_removal_index)
			valid_start: sp = last_removal_index
			empty_start: item_at_index (sp) = Void
			not_oldest: sp /= oldest_index
			not_youngest: sp /= last_insertion_index
		local
			idx, ep, nidx: INTEGER
		do
			ep := oldest_index
			from idx := sp
			until idx = ep
			loop
				nidx := index_decremented (idx)
				array.put (array.item (nidx), idx)
				if attached array.item (idx) as ti then
					--| update the shifted item's index
					ti.set_queue_index (idx)
				end
				idx := nidx
			end
			last_removal_index := 0
			oldest_index := index_incremented (oldest_index)
		ensure
			count_unchanged: count = old count
			removal_index_cleared: last_removal_index = 0
			oldest_shifted_right:
				oldest_index = index_incremented (old oldest_index)
			youngest_unchanged: youngest_index = old youngest_index
			next_above_youngest: is_advanced (youngest_index, next_available_index)
			packed: range_is_occupied (youngest_index, oldest_index)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	Kta_item: AEL_DS_LRU_QUEUE_ITEM
			-- Type anchor for items in Current
		require
			never: False
		do
			check False then end
		ensure
			never: False
		end

	--|--------------------------------------------------------------

	array: ARRAY [like item]
			--- Array of queue items

	--|--------------------------------------------------------------

	remove_item (v: like Kta_item)
			-- Remove from Current item 'v', adjusting indices as needed.
			-- last_removal_index will NOT be set as a result of 
			-- execution unless the item being removed is the oldest.
			--
			-- NOTE WELL: this is for internal use only!!
			-- As a proper dispenser, queue does not allow client 
			-- manipulation of contents except according to policy.
		require
			exists: v /= Void
			not_empty: not is_empty
			belongs: has_item (v)
		local
			idx, d_old, d_next: INTEGER
		do
			idx := v.queue_index
			remove_item_at_index (idx)
			if is_empty then
				reset_cursors_and_markers
			else
				--| Items must be reordered to maintain a contiguous sequence
				if idx = oldest_index then
					advance_oldest
				elseif idx = youngest_index then
					advance_youngest
				else
					--| Removed an item that was neither oldest nor 
					--| youngest, and result is a hole between youngest 
					--| and oldest that needs to be filled by shifting 
					--| items left or right to coalesce into a contiguous 
					--| range.
					--| If more items lie between N and O in the forward 
					--| direction, then it is cheaper to shift forward the 
					--| items to the left of the hole.  If there are more 
					--| items between N and O in the backward direction, 
					--| then it is cheaper to shift backwards the items to 
					--| the right of the hole.
					d_old := last_removal_index - oldest_index
					if d_old < 0 then
						d_old := -d_old
					end
					d_next := next_available_index - last_removal_index
					if d_next < 0 then
						d_next := -d_next
					end
					--| next_available_index is 'lowest' position that is 
					--| unoccupied at this time.
					--| If the number of items between the hole and the 
					--| next index is greater than the number between the 
					--| hole and the oldest, then shift items toward the 
					--| next index (forward), else shift items toward the 
					--| oldest index (backward)
					if d_old < d_next then
						shift_items_backward (last_removal_index)
					else
						shift_items_forward (last_removal_index)
					end
				end
			end
			v.clear_queue_index
		ensure
			counted: count = old count - 1
			reset: v.queue_index = 0
		end

	--|--------------------------------------------------------------

	remove_item_at_index (v: INTEGER)
			-- Remove from Current the item at index 'v'
			-- Do NOT adjust any cursors; this is a primitive!
			-- NOTE WELL: this is for internal use only!!
			-- As a proper dispenser, queue does not allow client 
			-- manipulation of contents except according to policy.
		require
			valid_index: is_valid_index (v)
			removable: array.item (v) /= Void
		do
			array [v] := Void
			count := count - 1
			last_insertion_index := 0
			last_removal_index := v
		ensure
			counted: count = old count - 1
			removed:  item_at_index (v) = Void
			recorded: last_removal_index = v
		end

	--|--------------------------------------------------------------
invariant
	valid_lower: lower = 1
	valid_upper: upper >= lower
	valid_capacity: capacity > 0
	array_exists: array /= Void
	cursors_consistent: cursors_are_consistent
	non_empty_has_oldest: not is_empty implies item /= Void
	oldest_valid: not is_empty implies oldest_index /= 0
	youngest_valid: not is_empty implies youngest_index /= 0

--|----------------------------------------------------------------------
--| History
--|
--| 003 01-Jun-2013 RFO
--|     Major rework of removal and promotion; significantly 
--|     bolstered contracts; separated remove_item, adding primitive 
--|     remove_item_at_index to limit scope and tighten assertions.
--|     Added text to How-to section.
--|     Added resizing support.
--| 002 21-May-2013 SKS
--|     Added logic to handle deletions and promotions correctly; 
--|     added or enhanced contracts (void-safe, 7.2)
--| 001 14-Jan-2013 RFO
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class by calling 'make' with a value 
--| for the size of the queue.  The queue capacity is fixed and does 
--| not resize automatically, but can be resized explicitly.
--|
--| To resize, call either resize, or truncate (see feature comments).
--|
--| Items are inserted from front to back (1 to capacity) and when 
--| full, the oldest item is replaced by the next insertion and the 
--| cursors (indices) are advanced.
--|
--| Items in the queue are unique.
--| Reinserting an item (one currently in the queue) promotes the 
--| item from its current position to the youngest.
--|
--| To insert an item known to be new to the queue, call 'extend'.  
--| If the item is know to exist in the queue aleady, reinsert it 
--| using 'promote'.  If unsure about existence in the queue, insert 
--| the item using 'force'.
--|
--| Items are dispensed, and only the oldest item is accessible by a 
--| client. To access the oldest item in the queue, call 'item'.
--|
--| Items other than the oldest cannot be removed specifically, but 
--| are removed automatically on when-full insertion (oldest=LRU is 
--| removed).
--| To remove the oldest item, call 'remove'.
--|
--| Contents can be purged by calling wipe_out, which in turn resets 
--| all indices to the empty state.
--|----------------------------------------------------------------------

end -- class AEL_DS_LRU_QUEUE
