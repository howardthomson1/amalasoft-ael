note
	description: "{
A cursor for a circular bounded priority queue, implemented as an array.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_LRU_QUEUE_CURSOR

inherit
	CURSOR

create
	make_with_position_and_range

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_position_and_range (p, l, u: INTEGER)
			-- Create Current with position 'p' and range denoted by 
			-- lower 'l' and upper 'u' values.
		require
			valid_range: l <= u
			position_high_enough: p >= l
			position_low_enough: p <= u
		do
			position := p
			lower := l
			upper := u
		ensure
			position_set: position = p
			lower_set: lower = l
			upper_set: upper = u
		end

--|========================================================================
feature -- Status
--|========================================================================

	position: INTEGER
			-- Posistion (index) of Current

	lower: INTEGER
			-- Lower bound of acceptable position values

	upper: INTEGER
			-- Upper bound of acceptable position values

--|========================================================================
feature -- Access
--|========================================================================

	next: like Current
			-- Newly created cursor one position logically forward of 
			-- Current
		do
			Result := Current.twin
			Result.forth
		end

	previous: like Current
			-- Newly created cursor one position logically backward of 
			-- Current
		do
			Result := Current.twin
			Result.back
		end

	--|--------------------------------------------------------------

	next_index
			-- Value of position one (logical) position forward of Current
		do
			if position = upper then
				Result := lower
			else
				Result := position + 1
			end
		end

	previous_index
			-- Value of position one (logical) position backward of Current
		do
			if position = lower then
				Result := upper
			else
			 	Result:= position - 1
			end
		end

--|========================================================================
feature -- Movement
--|========================================================================

	forth
			-- Move Current one (logical) position forward
		do
			if position = upper then
				position := lower
			else
				position := position + 1
			end
		end

	back
			-- Move Current one (logical) position backward
		do
			if position = lower then
				position := upper
			else
				position := position - 1
			end
		end

	--|--------------------------------------------------------------
invariant
	has_valid_range: lower <= upper
	position_high_enough: position >= lower
	position_low_enough: position <= upper

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-May-2013 RFO
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_DS_LRU_QUEUE_CURSOR
