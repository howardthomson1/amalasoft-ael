note
	description: "{
An item in an AEL_DS_LRU_QUEUE.
Serves as kind of proxy for generic objects to be stored in the queue.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_LRU_QUEUE_ITEM

inherit
	ANY
		redefine
			is_equal
		end

create
	make_with_data

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_data (v: like data; k: STRING)
			-- Create Current with the actual object to be stored 'v',
			-- and the key by which the object is identified 'k'
		require
			exists: v /= Void
			valid_key: k /= Void and then not k.is_empty
		do
			key := k
			data := v
		ensure
			key_same: key = k
			data_same: data = v
		end

--|========================================================================
feature -- Identification
--|========================================================================

	key: STRING
			-- Key used to identify the data item within queue

	data: ANY
			-- Actual data to be stored in the queue

--|========================================================================
feature {AEL_DS_LRU_QUEUE} -- Status
--|========================================================================

	queue_index: INTEGER
			-- Index in queue at which Current resides
			-- (not queue relative position)

--|========================================================================
feature {AEL_DS_LRU_QUEUE} -- Status setting
--|========================================================================

	set_queue_index (v: INTEGER)
			-- Record the index of Current in the queue.
			-- This is used only by the queue itself.
			-- N.B. this is for recording only and does not affect the queue
		require
			valid_index: v > 0
		do
			queue_index := v
		ensure
			is_set: queue_index = v
		end

	--|--------------------------------------------------------------

	clear_queue_index
			-- Unset the index of Current in the queue
			-- This is used only by the queue itself.
			-- N.B. this is for recording only and does not affect the queue
		do
			queue_index := 0
		ensure
			cleared: queue_index = 0
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_equal (other: like Current): BOOLEAN
			-- Is `other' attached to an object considered
			-- equal to current object?
		do
			Result := data = other.data and then key.same_string (other.key)
		end

	--|--------------------------------------------------------------
invariant
	valid_key: key /= Void and then not key.is_empty
	valid_data: data /= Void
	valid_queue_index: queue_index >= 0

--|----------------------------------------------------------------------
--| History
--|
--| 002 01-Jun-2013 RFO
--|     Added History and How-to; Tightened exports; added invariants.
--| 001 14-Jan-2013 RFO
--|     Created original module (for void-safe Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class by calling 'make_with_data' with 
--| the actual object to be stored, and the key by which the object 
--| is identified.
--|
--| Once created, only the associated queue has access to features, 
--| with the exception of 'key', used by a client to identify the item.
--|----------------------------------------------------------------------

end -- class AEL_DS_LRU_QUEUE_ITEM
