--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| HASH_TABLE that notifies subscribers on changes
--|----------------------------------------------------------------------

class AEL_DS_NOTIFYING_HASH_TABLE [G, H-> HASHABLE]

inherit
	HASH_TABLE [G, H]
		export
			{NONE} merge, copy
		redefine
			make,
			put, force, extend, replace, replace_key, merge,
			remove, wipe_out, copy, fill
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
		do
			create subscribers.make
			Precursor (sz)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put (v: G; k: H)
			-- Insert `v' with `k' if there is no other item
			-- associated with the same key.
			-- Set `inserted' if and only if an insertion has
			-- been made (i.e. `k' was not present).
			-- If so, set `position' to the insertion position.
			-- If not, set `conflict'.
			-- In either case, set `found_item' to the item
			-- now associated with `k' (previous item if
			-- there was one, `v' otherwise).
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (v, k)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	force (v: G; k: H)
			-- Update table so that `v' will be the item associated
			-- with `k'.
			-- If there was an item for that key, set `found'
			-- and set `found_item' to that item.
			-- If there was none, set `not_found' and set
			-- `found_item' to the default value.
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (v, k)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	extend (v: G; k: H)
			-- Assuming there is no item of key `k',
			-- insert `v' with `k'.
			-- Set `inserted'.
		do
			Precursor (v, k)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	replace (v: G; k: H)
			-- Replace item at `k', if present,
			-- with `v'; do not change associated key.
			-- Set `replaced' if and only if a replacement has been made
			-- (i.e. `k' was present); otherwise set `not_found'.
			-- Set `found_item' to the item previously associated
			-- with `k' (default value if there was none).
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (v, k)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	replace_key (new_key: H; old_key: H)
			-- If there is an item of key `old_key' and no item of key
			-- `new_key', replace the former's key by `new_key',
			-- set `replaced', and set `found_item' to the item
			-- previously associated with `old_key'.
			-- Otherwise set `not_found' or `conflict' respectively.
			-- If `conflict', set `found_item' to the item previously
			-- associated with `new_key'.
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (new_key, old_key)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	merge (other: HASH_TABLE [G, H])
			-- Merge `other' into Current. If `other' has some elements
			-- with same key as in `Current', replace them by one from
			-- `other'.
		do
			Precursor (other)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	fill (other: CONTAINER [G])
			-- Fill with as many items of `other' as possible.
			-- The representations of `other' and current structure
			-- need not be the same.
		do
			Precursor (other)
			notify_subscribers
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove (k: H)
			-- Remove item associated with `k', if present.
			-- Set `removed' if and only if an item has been
			-- removed (i.e. `k' was present);
			-- if so, set `position' to index of removed element.
			-- If not, set `not_found'.
			-- Reset `found_item' to its default value if `removed'.
		do
			Precursor (k)
			notify_subscribers
		end

	--|--------------------------------------------------------------

	wipe_out
			-- Reset all items to default values; reset status.
		do
			Precursor
			notify_subscribers
		end

 --|========================================================================
feature -- Duplication
 --|========================================================================

	copy (other: like Current)
			-- Re-initialize from `other'.
		do
			Precursor (other)
			notify_subscribers
		end

--|========================================================================
feature -- Subscription support
--|========================================================================

	subscribe (sa: like first_subscriber)
			-- Resister subscriber agent 'sa' for notification on change
		require
			is_new: not subscribers.has (sa)
		do
			subscribers.extend (sa)
		ensure
			subscribed: subscribers.has (sa)
			added: subscribers.count = (old subscribers.count) + 1
		end

	--|--------------------------------------------------------------

	subscribers: TWO_WAY_LIST [like first_subscriber]
			-- List of subscribers (agents) to call on a change

	notifying: BOOLEAN
			-- Is Current in the process of notifying subscribers?

	first_subscriber: detachable PROCEDURE [TUPLE [like Current]]
		do
			if not subscribers.is_empty then
				Result := subscribers.first
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	notify_subscribers
		local
			sl: like subscribers
			oc: CURSOR
		do
			if not notifying then
				notifying := True
				sl := subscribers
				oc := sl.cursor
				from sl.start
				until sl.exhausted
				loop
					if attached sl.item as lsi then
						lsi.call ([Current])
					end
					sl.forth
				end
				notifying := False
			end
		end

end -- class AEL_DS_NOTIFYING_HASH_TABLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 17-Sep-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create with an initial capacity.
--| Use as you would a normal HASH_TABLE.
--| To subscribe to change notifications, call subscribe with a valid
--| agent as argument
--|----------------------------------------------------------------------
