--|----------------------------------------------------------------------
--| Copyright (c) 1995-2014, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An octet stream; i.e. a sequence of unsigned 8 bit integers
--|----------------------------------------------------------------------

class AEL_DS_OCTET_STREAM

inherit
	AEL_MAPPED_COMPONENT
		rename
			make as make_mapped
		redefine
			out
		end

create
	make_with_size, make_from_address, make_for_address

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_size (sz: INTEGER)
			-- Create Current with the 'sz' capacity
		require
			valid_size: sz >= 0
		do
			create mp.make (sz)
			make_mapped (mp.item, sz)
		ensure
			has_memory: mp /= Void
			addressed: address = mp.item
			sized: count = sz
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	count: INTEGER
			-- Number of bytes in Current 
		do
			Result := size_in_bytes.as_integer_32
		ensure
			valid: Result = size_in_bytes.as_integer_32
		end

	--|--------------------------------------------------------------

	to_string: STRING
			-- Content of stream as a string of 8-bit characters
		local
			i: INTEGER
		do
			create Result.make (count)
--RFO 			from i := 0
--RFO 			until i > upper
--RFO 			loop
--RFO 				Result.extend (item (i).to_character_8)
--RFO 				i := i + 1
--RFO 			end
		ensure
			exists: Result /= Void
--			same_length: Result.count = count
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String (printable) representation of of stream
			-- Representation is in form of 2 hex digits each byte
		do
			Result := apf.amemdump (address, count, "", 0, 0)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	mp: detachable MANAGED_POINTER
			-- Managed pointer that holds address of current, if any
			-- Current can be create with raw pointer, in which case mp 
			-- is Void
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------
invariant
	addressed: address /= Default_pointer

end -- class AEL_DS_OCTET_STREAM

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 28-Jan-2014
--|     Create original (to replace AEL_DS_BYTE_STREAM)
--|     Compiled and tested using Eiffel 13.11
--|----------------------------------------------------------------------
--| How-to
--|
--| To use this class, create a new instance with a given number of octets
--| using 'make_with_size', or from an existing stream using 'make_for_address'
--| or 'make_from_address' as appropriate.
--|----------------------------------------------------------------------
