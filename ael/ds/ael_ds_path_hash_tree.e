class AEL_DS_PATH_HASH_TREE
-- Tree-like node/structure containing objects

inherit
	AEL_DS_HASH_TREE

create
	make_with_path, make_with_path_parts, make_with_object, make_from_table

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_as_root (pn: STRING)
			-- Create Current as the root of a tree
		require
			valid_path: is_valid_path (pn)
		do
			make_as_top (pn)
		end

	make_with_path (pn: STRING; p: like parent; o: like data)
		require
			valid_path: is_valid_path (pn)
		local
			pl: LIST [STRING]
		do
			pl := pathname_to_parts (pn)
			make_with_path_parts (pl, p, o)
		end

	make_with_path_parts (pl: LIST [STRING]; p: like parent; o: like data)
		require
			parts_exist: pl /= Void
			not_empty: not pl.is_empty
			valid_path: is_valid_path (pl.first)
		local
			nm: STRING
			c: like Current
		do
			pl.start
			nm := pl.item
			pl.remove
			if pl.is_empty then
				if o = Void then
					make_as_parent (nm, p)
				else
					make_with_object (nm, p, o)
				end
			else
				make_as_parent (nm, p)
				create c.make_with_path_parts (pl, Current, o)
				extend (c)
			end
		end

	make_with_object (nm: STRING; p: like parent; obj: like data)
		do
			make_with_data (nm, p, obj)
		end

	--|--------------------------------------------------------------

	make_from_table (nm: STRING; ot: HASH_TABLE [like data, STRING])
			-- Create Current from an object hash table
		require
			valid_path: is_valid_path (nm)
			table_exists: ot /= Void
		local
			oc: CURSOR
		do
			make_as_root (nm)
			oc := ot.cursor
			from ot.start
			until ot.after
			loop
				add_child_from_path (
					ot.key_for_iteration, ot.item_for_iteration)
				ot.forth
			end
			ot.go_to (oc)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_child_from_path (p: STRING; o: like data)
		local
			pl: LIST [STRING]
		do
			pl := pathname_to_parts (p)
			add_child_from_path_parts (pl, o)
		end

	add_child_from_path_parts (pl: LIST [STRING]; o: like data)
		local
			tn: detachable like Current
			nm: STRING
		do
			nm := pl.first
			if attached child_by_name (nm) as ltn then
				-- Add as child of 'ltn'
				pl.start
				pl.remove
				ltn.add_child_from_path_parts (pl, o)
			else
				-- Add as child of Current
				create tn.make_with_path_parts (pl, Current, o)
				extend (tn)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_path (pn: STRING): BOOLEAN
			-- Is 'pn' a valid object path?
		do
			Result := pn /= Void and then not pn.is_empty
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	pathname_to_parts (v: STRING): LIST [STRING]
		local
			tp: AEL_SPRT_UX_PATHNAME
		do
			if attached {AEL_SPRT_UX_PATHNAME} v as uxp then
				tp := uxp
			elseif attached {PATH_NAME} v as p then
				create tp.make_from_pathname (p)
			else
				create tp.make_from_string (v)
			end
			Result := tp.to_components
		end

end -- class AEL_DS_PATH_HASH_TREE
