note
	description: "{
Encapsulation of matching and comparison functions to be applied
to an instance of AEL_DS_SORTABLE_HASH_TABLE or AEL_DS_LIST_AGENT_SORTED
Items neeD NOT be COMPARABLE, but if they are, the default sorting
behavior conforms to that of COMPARABLE (and other sorted structures
in the Eiffel libraries, BUT other sorting criteria are also
supported via settable comparison agent or agents.
Also, the contents of this structure are query-able in a
manner similar to the alternate sorting feature, via
client-defined query parameters.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2021 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_QUERY [G]

inherit
	ANY
		redefine
			default_create
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (mf: like matcher; cfl: like comparators)
		do
			default_create
			set_matcher (mf)
			set_comparators (cfl)
		end

	default_create
		do
			create comparators.make
			matcher := dummy_matcher_agent
			Precursor
			comparators.extend (agent dummy_comparator)
		end

--|========================================================================
feature -- Access
--|========================================================================

	matcher: FUNCTION [G, BOOLEAN]

	comparators: LINKED_LIST [FUNCTION [G, G, INTEGER]]
			-- Functions by which sorting comparison is performed

--|========================================================================
feature -- Status setting
--|========================================================================

	set_matcher (mf: like matcher)
		do
			matcher := mf
		end

	set_comparator (v: detachable like dummy_comparator_agent)
		do
			comparators.wipe_out
			if attached v as tv then
				comparators.extend (tv)
			else
				comparators.extend (agent dummy_comparator)
			end
		end

	set_comparators (v: detachable like comparators)
			-- Replace any existing comparison function with those from 'v'
		do
			comparators.wipe_out
			if attached v as tv then
				comparators.fill (tv)
			else
				comparators.extend (agent dummy_comparator)
			end
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	dummy_matcher (v: G): BOOLEAN
			-- Match all
		do
			Result := True
		end

	dummy_matcher_agent: FUNCTION [G, BOOLEAN]
		do
			Result := agent dummy_matcher
		end

	dummy_comparator (v1, v2: G): INTEGER
			-- Do nothing
		do
		end

	dummy_comparator_agent: FUNCTION [G, G, INTEGER]
		do
			Result := agent dummy_comparator
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 02-Apr-2021
--|     Create original module to exploit features added to
--|     AEL_DS_LIST_AGENT_SORTED
--|     Compiled and tested, void-safe, with 19.05
--|----------------------------------------------------------------------
--| How-to
--|----------------------------------------------------------------------

end -- class AEL_DS_QUERY
