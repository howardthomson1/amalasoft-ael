--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Contiguous collection of values, all of a conforming type,
--| accessible through 0-based integer indices.
--|----------------------------------------------------------------------

class AEL_DS_RAW_ARRAY [G]

inherit
	RESIZABLE [G]
		redefine
			full, copy, is_equal, resizable
		end
	INDEXABLE [G, INTEGER]
		rename
			item as item alias "[]"
		redefine
			copy, is_equal
		end
	TO_SPECIAL [G]
		export
			{AEL_DS_RAW_ARRAY} set_area
		redefine
			copy, is_equal, item, put, at, valid_index
		end

create
	make,
	make_filled,
	make_from_array

convert
	to_special: {SPECIAL [G]}

 --|========================================================================
feature -- Creation and Initialization
 --|========================================================================

	make_filled (a_default_value: G; sz: INTEGER)
			-- Allocate array; set index interval to
			-- `min_index' .. `max_index'; set all values to default.
			-- (Make array empty if `min_index' = `max_index' + 1).
		require
			valid_size: sz >= 0
		do
			make_filled_area (a_default_value, sz)
			initialize_bounds (sz)
		ensure
			items_set: filled_with (a_default_value)
		end

	--|--------------------------------------------------------------

	make (sz: INTEGER)
			-- Create Current with 'sz' positions
		require
			valid_size: sz >= 0
		do
			if sz > 0 then
				make_filled_area (({G}).default, sz)
			else
				make_empty_area (0)
			end
			initialize_bounds (sz)
		ensure
			upper_set: upper = count - 1
			items_set: all_default
		end

	--|--------------------------------------------------------------

	make_from_array (a: ARRAY [G])
			-- Initialize from the items of `a'.
			-- (Useful in proper descendants of class `ARRAY',
			-- to initialize an array-like object from a manifest array.)
		require
			array_exists: a /= Void
		do
			set_area (a.area)
			initializing_from_array := True
			initialize_bounds (a.count)
			initializing_from_array := False
		end

	--|--------------------------------------------------------------

	initialize_bounds (sz: INTEGER)
			-- Initialize Current with 'sz' positions
		require
			valid_size: sz >= 0
			have_area: area /= Void
		do
			lower := 0
			upper := sz - 1
		ensure
			upper_set: upper = count - 1
			items_set: (not initializing_from_array) implies all_default
		end

 --|========================================================================
feature -- Access
 --|========================================================================

	item alias "[]", at alias "@" (i: INTEGER): G assign put
			-- Entry at index `i', if in index interval
		do
			Result := area.item (i)
		end

	entry (i: INTEGER): G
			-- Entry at index `i', if in index interval
		require
			valid_key: valid_index (i)
		do
			Result := item (i)
		end

	has (v: G): BOOLEAN
			-- Does `v' appear in array?
 			-- (Reference or object equality,
			-- based on `object_comparison'.)
		local
			i, lim: INTEGER
			l_area: like area
		do
			l_area := area
			lim := upper
			if object_comparison and v /= Void then
				from
				until
					i > lim or Result
				loop
					Result := l_area.item (i) ~ v
					i := i + 1
				end
			else
				from
				until
					i > lim or Result
				loop
					Result := l_area.item (i) = v
					i := i + 1
				end
			end
		end

 --|========================================================================
feature -- Measurement
 --|========================================================================

	lower: INTEGER
			-- Minimum index (always 0)

	upper: INTEGER
			-- Maximum index (always count - 1)

	count, capacity: INTEGER
			-- Number of available indices
		do
			Result := upper + 1
		ensure then
			consistent_with_bounds: Result = upper - lower + 1
		end

	--|--------------------------------------------------------------

	occurrences (v: G): INTEGER
			-- Number of times `v' appears in structure
		local
			i, lim: INTEGER
		do
			if object_comparison and then v /= Void then
				lim := upper
				from
				until i > lim
				loop
					if item (i) ~ v then
						Result := Result + 1
					end
					i := i + 1
				end
			else
				lim := upper
				from
				until i > lim
				loop
					if item (i) = v then
						Result := Result + 1
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO index_set: INTEGER_INTERVAL
	--RFO 		-- Range of acceptable indexes
	--RFO 	do
	--RFO 		create Result.make (0, upper)
	--RFO 	ensure then
	--RFO 		same_count: Result.count = count
	--RFO 		same_bounds: (Result.lower = 0) and (Result.upper = upper)
	--RFO 	end

 --|========================================================================
feature -- Comparison
 --|========================================================================

	is_equal (other: like Current): BOOLEAN
			-- Is array made of the same items as `other'?
		local
			i, lim: INTEGER
			oc: BOOLEAN
		do
			oc := object_comparison
			if other = Current then
				Result := True
			elseif count = other.count and then oc = other.object_comparison then
				lim := upper
				if oc then
					from Result := True
					until not Result or (i > lim)
					loop
						Result := item (i) ~ other.item (i)
						i := i + 1
					end
				else
					Result := area.same_items (other.area, 0, lim, count)
				end
			end
		end

 --|========================================================================
feature -- Status report
 --|========================================================================

	initializing_from_array: BOOLEAN
			-- Is this array initializing its content from another?

	all_default: BOOLEAN
			-- Are all items set to default values?
		do
			if count > 0 then
				Result := ({G}).has_default and then
					area.filled_with (({G}).default, 0, upper)
			else
				Result := True
			end
		ensure
			definition: Result = (count = 0 or else
				((not attached item (upper) as i or else i = i.default) and
				subarray (lower, upper - 1).all_default))
		end

	--|--------------------------------------------------------------

	filled_with (v: G): BOOLEAN
			-- Are all itms set to `v'?
		do
			Result := area.filled_with (v, 0, upper)
		ensure
			definition: Result = (count = 0 or else
				(item (upper) = v and subarray (0, upper - 1).filled_with (v)))
		end

	--|--------------------------------------------------------------

	full: BOOLEAN = True
			-- Is structure filled to capacity?

	--|--------------------------------------------------------------

	same_items (other: like Current): BOOLEAN
			-- Do `other' and Current have same items?
		require
			other_not_void: other /= Void
		do
			if count = other.count then
				Result := area.same_items (other.area, 0, 0, count)
			end
		ensure
			definition: Result = ((count = other.count) and then
				(count = 0 or else (item (upper) = other.item (other.upper)
				and subarray (0, upper - 1).same_items
				(other.subarray (0, other.upper - 1)))))
		end

	--|--------------------------------------------------------------

	valid_index (i: INTEGER): BOOLEAN
			-- Is `i' within the bounds of the array?
		do
			Result := i >= 0 and i <= upper
		end

	extendible: BOOLEAN
			-- May items be added?
			-- (Answer: no, although array may be resized.)

	prunable: BOOLEAN
			-- May items be removed? (Answer: no.)

	resizable: BOOLEAN
			-- Can array be resized automatically?
		do
			Result := ({G}).has_default
		end

	valid_index_set: BOOLEAN
		do
			--Result := index_set.count = count
			Result := count = (upper - lower) + 1
		end

 --|========================================================================
feature -- Element change
 --|========================================================================

	put (v: like item; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
		do
			area.put (v, i)
		end

	--|--------------------------------------------------------------

	enter (v: like item; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
		require
			valid_key: valid_index (i)
		do
			area.put (v, i)
		end

	--|--------------------------------------------------------------

	force (v: like item; i: INTEGER)
			-- Assign item `v' to `i'-th entry.
			-- Always applicable if larger:
			-- Resize the array if `i' falls past current upper bound,
			-- preserve existing items.
			-- In void-safe mode, if ({G}).has_default does not hold,
			-- then you can only insert at `upper + 1' position in the ARRAY.
		require
			large_enough: i >= 0
			has_default: ({G}).has_default or else i=lower - 1 or else i=upper + 1
		local
			old_size, new_size: INTEGER
			new_upper: INTEGER
			l_increased_by_one: BOOLEAN
		do
			if valid_index (i) then
				put (v, i)
			else
				new_upper := upper.max (i)
				new_size := new_upper + 1
				l_increased_by_one := i = upper + 1
				if empty_area then
					-- Array is empty.
					-- First we create a new empty SPECIAL of the right capacity.
					make_empty_area (new_size.max (additional_space))
					if not l_increased_by_one then
						-- We need to fill the SPECIAL for `0' to `new_size - 2'
						-- with the default value.
						area.fill_with (({G}).default, 0, new_size - 2)
					end
					area.extend (v)
				else-- Not an empty array
					old_size := area.capacity
					check
						increased: new_size > old_size
					end
					set_area (area.aliased_resized_area (
						new_size.max (old_size + additional_space)))
				end
				check
					grown: new_size > area.count
				end
				-- We are adding to the new `upper' position.
				-- First we fill the non-initialized
				-- elements if any up to `new_size - 2'
				-- (i.e. up the the item prior to `upper').
				if not l_increased_by_one then
					area.fill_with (({G}).default, area.count, new_size - 2)
				end
				-- Add `v' at upper position.
				area.extend (v)
			end
			upper := new_upper
		ensure
			inserted: item (i) = v
			higher_count: count >= old count
			lower_set: lower = (old lower).min (i)
			upper_set: upper = (old upper).max (i)
		end

	--|--------------------------------------------------------------

	fill_with (v: G)
			-- Set all items in bounds with `v'.
		do
			area.fill_with (v, 0, upper)
		ensure
			same_capacity: capacity = old capacity
			count_definition: count = old count
			filled: filled_with (v)
		end

	--|--------------------------------------------------------------

	subcopy (other: like Current; start_pos, end_pos, index_pos: INTEGER)
			-- Copy items of `other' within bounds `start_pos' and `end_pos'
			-- to current array starting at index `index_pos'.
		require
			other_not_void: other /= Void
			valid_start_pos: start_pos >= 0
			valid_end_pos: end_pos <= other.upper
			valid_bounds: start_pos <= end_pos + 1
			valid_index_pos: index_pos >= 0
			enough_space: (upper - index_pos) >= (end_pos - start_pos)
		do
			area.copy_data (
				other.area, start_pos, index_pos, end_pos - start_pos + 1)
		ensure
			-- copied: forall `i' in 0 .. (`end_pos'-`start_pos'),
			--     item (index_pos + i) = other.item (start_pos + i)
		end

 --|========================================================================
feature -- Iteration
 --|========================================================================

	do_all (action: PROCEDURE [G])
			-- Apply `action' to every item, from first to last.
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
		do
			area.do_all_in_bounds (action, 0, count - 1)
		end

	--|--------------------------------------------------------------

	do_if (action: PROCEDURE [G];
	       test: FUNCTION [G, BOOLEAN])
			-- Apply `action' to every item that satisfies `test',
			-- from first to last. Semantics not guaranteed if `action'
			-- or `test' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
			test_not_void: test /= Void
		do
			area.do_if_in_bounds (action, test, 0, count - 1)
		end

	--|--------------------------------------------------------------

	there_exists (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is `test' true for at least one item?
		require
			test_not_void: test /= Void
		do
			Result := area.there_exists_in_bounds (test, 0, count - 1)
		end

	--|--------------------------------------------------------------

	for_all (test: FUNCTION [G, BOOLEAN]): BOOLEAN
			-- Is `test' true for all items?
		require
			test_not_void: test /= Void
		do
			Result := area.for_all_in_bounds (test, 0, count - 1)
		end

	--|--------------------------------------------------------------

	do_all_with_index (action: PROCEDURE [G, INTEGER])
			-- Apply `action' to every item, from first to last.
			-- `action' receives item and its index.
			-- Semantics not guaranteed if `action' changes the structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
		local
			i, j, nb: INTEGER
			l_area: like area
		do
			from
				i := 0
				j := 0 -- lower
				nb := count - 1
				l_area := area
			until
				i > nb
			loop
				action.call ([l_area.item (i), j])
				j := j + 1
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	do_if_with_index (action: PROCEDURE [G, INTEGER];
	                  test: FUNCTION [G, INTEGER, BOOLEAN])
			-- Apply `action' to every item that satisfies `test',
			-- from first to last.
			-- `action' and `test' receive the item and its index.
			-- Semantics not guaranteed if `action' or `test' changes the
			-- structure;
			-- in such a case, apply iterator to clone of structure instead.
		require
			action_not_void: action /= Void
			test_not_void: test /= Void
		local
			i, j, nb: INTEGER
			l_area: like area
		do
			from
				i := 0
				j := 0 -- lower
				nb := count - 1
				l_area := area
			until
				i > nb
			loop
				if test.item ([l_area.item (i), j]) then
					action.call ([l_area.item (i), j])
				end
				j := j + 1
				i := i + 1
			end
		end

 --|========================================================================
feature -- Removal
 --|========================================================================

	wipe_out
			-- Make array empty.
		obsolete
			"Not applicable since not `prunable'. Use `discard_items' instead."
		do
			discard_items
		end

	discard_items
			-- Reset all items to default values with reallocation.
		require
			has_default: ({G}).has_default
		do
			create area.make_filled (({G}).default, capacity)
		ensure
			default_items: all_default
		end

	--|--------------------------------------------------------------

	clear_all
			-- Reset all items to default values.
		require
			has_default: ({G}).has_default
		do
			area.fill_with (({G}).default, 0, area.count - 1)
		ensure
			stable_lower: lower = 0
			stable_upper: upper = old upper
			default_items: all_default
		end

	--|--------------------------------------------------------------

	keep_head (n: INTEGER)
			-- Remove all items except for the first `n';
			-- do nothing if `n' >= `count'.
		require
			non_negative_argument: n >= 0
		do
			if n < count then
				upper := n - 1
				area := area.aliased_resized_area (n)
			end
		ensure
			new_count: count = n.min (old count)
			same_lower: lower = 0
		end

	--|--------------------------------------------------------------

	remove_tail (n: INTEGER)
			-- Remove last `n' items;
			-- if `n' > `count', remove all.
		require
			n_non_negative: n >= 0
			n_not_too_large: n <= count
		do
			keep_head (count - n)
		ensure
			new_count: count = (old count - n).max (0)
			same_lower: lower = 0
		end

 --|========================================================================
feature -- Resizing
 --|========================================================================

	grow (i: INTEGER)
			-- Change the capacity to at least `i'.
		do
			if i > capacity then
				conservative_resize (i)
			end
		end

	--|--------------------------------------------------------------

	conservative_resize (max_index: INTEGER)
			-- Rearrange array so that it can accommodate
			-- indices down to `min_index' and up to `max_index'.
			-- Do not lose any previously entered item.
		require
			valid_size: max_index > 0
			has_default: ({G}).has_default
		do
			conservative_resize_with_default (({G}).default, max_index)
		ensure
			no_low_lost: lower = 0
			no_high_lost: upper = max_index or else upper = old upper
		end

	--|--------------------------------------------------------------

	conservative_resize_with_default (a_default_value: G; max_index: INTEGER)
			-- Rearrange array so that it can accommodate
			-- indices down to 0 and up to `max_index'.
			-- Do not lose any previously entered item.
		require
			valid_max: max_index >= 0
		local
			new_size: INTEGER
			new_upper: INTEGER
		do
			if empty_area then
				set_area (area.aliased_resized_area_with_default (
					a_default_value, max_index + 1))
				upper := max_index
			else
				new_upper := max_index.max (upper)
				new_size := new_upper + 1
				if new_size > area.count then
					set_area (area.aliased_resized_area_with_default (a_default_value, new_size))
				end
				upper := new_upper
			end
		ensure
			no_low_lost: lower = 0
			no_high_lost: upper = max_index or else upper = old upper
		end

	--|--------------------------------------------------------------

	trim
			-- <Precursor>
		local
			n: like count
		do
			n := count
			if n < capacity then
				area := area.aliased_resized_area (n)
			end
		ensure then
			same_items: same_items (old twin)
		end

 --|========================================================================
feature -- Conversion
 --|========================================================================

	to_c: ANY
			-- Address of actual sequence of values,
			-- for passing to external (non-Eiffel) routines.
		require
			not_is_dotnet: not {PLATFORM}.is_dotnet
		do
			Result := area
		end

	--|--------------------------------------------------------------

	to_c_pointer: POINTER
			-- Address of actual sequence of values,
			-- for passing to external (non-Eiffel) routines.
		require
			not_is_dotnet: not {PLATFORM}.is_dotnet
		do
			Result := $area
		end

	--|--------------------------------------------------------------

	to_cil: NATIVE_ARRAY [G]
			-- Address of actual sequence of values,
			-- for passing to external (non-Eiffel) routines.
		require
			is_dotnet: {PLATFORM}.is_dotnet
		do
			Result := area.native_array
		ensure
			to_cil_not_void: Result /= Void
		end

	--|--------------------------------------------------------------

	to_special: SPECIAL [G]
			-- 'area'.
		do
			Result := area
		ensure
			to_special_not_void: Result /= Void
		end

	--|--------------------------------------------------------------

	linear_representation: LINEAR [G]
			-- Representation as a linear structure
		local
			temp: ARRAYED_LIST [G]
			i, lim: INTEGER
		do
			create temp.make (capacity)
			lim := upper
			from
			until i > lim
			loop
				temp.extend (item (i))
				i := i + 1
			end
			Result := temp
		end

 --|========================================================================
feature -- Duplication
 --|========================================================================

	copy (other: like Current)
			-- Reinitialize by copying all the items of `other'.
			-- (This is also used by `clone'.)
		do
			if other /= Current then
				standard_copy (other)
				set_area (other.area.twin)
			end
		ensure then
			equal_areas: area ~ other.area
		end

	--|--------------------------------------------------------------

	subarray (start_pos, end_pos: INTEGER): like Current
			-- Raw array made of items of current array within
			-- bounds `start_pos' and `end_pos'.
		require
			valid_start_pos: valid_index (start_pos)
			valid_end_pos: end_pos <= upper
			valid_bounds: (start_pos <= end_pos) or (start_pos = end_pos + 1)
		do
			create Result.make (end_pos - start_pos + 1)
			if start_pos <= end_pos then
					-- Only copy elements if needed.
				Result.subcopy (Current, start_pos, end_pos, 0)
			end
		ensure
			lower: Result.lower = start_pos
			upper: Result.upper = end_pos
			-- copied: forall `i' in `start_pos' .. `end_pos',
			--     Result.item (i) = item (i)
		end

 --|========================================================================
feature {NONE} -- Inapplicable
 --|========================================================================

	prune (v: G)
			-- Remove first occurrence of `v' if any.
			-- (Precondition is False.)
		do
		end

	extend (v: G)
			-- Add `v' to structure.
			-- (Precondition is False.)
		do
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

--RFO	auto_resize (max_index: INTEGER)
--RFO			-- Rearrange array so that it can accommodate
--RFO			-- indices down to `min_index' and up to `max_index'.
--RFO			-- Do not lose any previously entered item.
--RFO			-- If area must be extended, ensure that space for at least
--RFO			-- additional_space item is added.
--RFO		require
--RFO			valid_index: max_index >= 0
--RFO		local
--RFO			old_size, new_size: INTEGER
--RFO			new_upper: INTEGER
--RFO		do
--RFO			if empty_area then
--RFO				new_upper := max_index
--RFO			else
--RFO				new_upper := max_index.max (upper)
--RFO			end
--RFO			new_size := new_upper + 1
--RFO			if not empty_area then
--RFO				old_size := area.count
--RFO				if new_size > old_size and new_size - old_size < additional_space
--RFO				 then
--RFO					new_size := old_size + additional_space
--RFO				end
--RFO			end
--RFO			if empty_area then
--RFO				make_empty_area (new_size)
--RFO			else
--RFO				if new_size > old_size then
--RFO					area := area.aliased_resized_area (new_size)
--RFO				end
--RFO			end
--RFO			upper := new_upper
--RFO		end

	--|--------------------------------------------------------------

	empty_area: BOOLEAN
			-- Is `area' empty?
		do
			Result := area = Void or else area.capacity = 0
		end

	--|--------------------------------------------------------------
invariant
	area_exists: area /= Void
	consistent_size: capacity = upper + 1
	non_negative_count: count >= 0
	index_set_has_same_count: valid_index_set
	not_initializing: not initializing_from_array

end -- class AEL_DS_RAW_ARRAY

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| To use this class, create a new instance with a given number of places.
--| All places are default on creation (0 for integer, Void for reference).
--| Use as any ARRAY, but always bear in mind that this is ZERO-BASED!!!
--|----------------------------------------------------------------------
