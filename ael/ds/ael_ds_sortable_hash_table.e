note
	description: "{
A HASH_TABLE with alternate sortable representation
Items need NOT be COMPARABLE, but if they are, the default sorting
behavior conforms to that of COMPARABLE (and other sorted structures
in the Eiffel libraries, BUT other sorting criteria are also
supported via settable comparison agent or agents.
Also, the contents of this structure are query-able in a
manner similar to the alternate sorting feature, via
client-defined query parameters.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2021 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_SORTABLE_HASH_TABLE [G, K->HASHABLE]

inherit
	HASH_TABLE [G, K]
		rename
			extend as ht_extend
		export
			{ANY} count, is_empty, extendible, has,
			deep_twin, same_type, is_deep_equal, start, after, forth, off,
			item_for_iteration, key_for_iteration
		redefine
			make, put, force, replace, remove, wipe_out
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (n: INTEGER)
			-- Create Current with beginning capacity of 'n' itemes
		do
			last_inserted_position := -1
			last_deleted_position := -1
			Precursor (n)
			create_stabile_attributes
		end

	--|--------------------------------------------------------------

	create_stabile_attributes
		do
			create cached_contents_sorted.make_default
			create comparison_functions.make
			cached_contents_sorted.set_comparison_function (dflt_comparison_agent)
			comparison_functions.extend (dflt_comparison_agent)
		end


--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature -- Sorting agent setting
--|========================================================================

	set_item_comparison_function (v: detachable like dflt_comparison_agent)
			-- Set, for subsequent use for default sorting, the item 
			-- comparison function to 'v'
			-- If Void, the default comparison function is used
			--
			-- For backward compatibility; else use 'set_comparison_function'
		do
			set_comparison_function (v)
		ensure
			is_set: item_comparison_function = v
		end

	--|--------------------------------------------------------------

	set_comparison_functions (v: detachable like comparison_functions)
			-- Replace any existing comparison function with those from 'v'
		do
			comparison_functions.wipe_out
			if attached v as tv then
				comparison_functions.fill (tv)
			else
				comparison_functions.extend (dflt_comparison_agent)
			end
		end

	--|--------------------------------------------------------------

	set_comparison_function (v: detachable like dflt_comparison_agent)
		do
			comparison_functions.wipe_out
			if attached v as tv then
				comparison_functions.extend (tv)
			else
				comparison_functions.extend (dflt_comparison_agent)
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: G; k: K)
		do
			force (v, k)
		end

	--|--------------------------------------------------------------

	force (new: G; key: K)
			-- Update table so that `new' will be the item associated
			-- with `key'.
			-- If there was an item for that key, set `found'
			-- and set `found_item' to that item.
			-- If there was none, set `not_found' and set
			-- `found_item' to the default value.
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (new, key)
			last_inserted_position := position
			last_deleted_position := -1
			sort_is_coherent := False
		end

	--|--------------------------------------------------------------

	replace (v: G; k: K)
			-- Replace item at `key', if present,
			-- with `new'; do not change associated key.
			-- Set `replaced' if and only if a replacement has been made
			-- (i.e. `key' was present); otherwise set `not_found'.
			-- Set `found_item' to the item previously associated
			-- with `key' (default value if there was none).
		do
			Precursor (v, k)
			last_inserted_position := position
			last_deleted_position := -1
			sort_is_coherent := False
		end

	--|--------------------------------------------------------------

	remove (k: K)
			-- Remove item associated with `k', if present.
			-- Set `removed' if and only if an item has been
			-- removed (i.e. `k' was present);
			-- if so, set `position' to index of removed element.
			-- If not, set `not_found'.
			-- Reset `found_item' to its default value if `removed'.
		do
			Precursor (k)
			last_inserted_position := -1
			last_deleted_position := position
			sort_is_coherent := False
		end

	--|--------------------------------------------------------------

	wipe_out
			-- Reset all items to default values; reset status.
		do
			Precursor
			last_inserted_position := -1
			last_deleted_position := -1
			sort_is_coherent := False
		end

--|========================================================================
feature {HASH_TABLE} -- Element change
--|========================================================================

	put (new: G; key: K)
			-- Insert `new' with `key' if there is no other item
			-- associated with the same key.
			-- Set `inserted' if and only if an insertion has
			-- been made (i.e. `key' was not present).
			-- If so, set `position' to the insertion position.
			-- If not, set `conflict'.
			-- In either case, set `found_item' to the item
			-- now associated with `key' (previous item if
			-- there was one, `new' otherwise).
			--
			-- To choose between various insert/replace procedures,
			-- see `instructions' in the Indexing clause.
		do
			Precursor (new, key)
			last_inserted_position := position
			last_deleted_position := -1
			sort_is_coherent := False	
	end

--|========================================================================
feature -- Alternate access
--|========================================================================

	items_by_range (sp, ep: INTEGER): TWO_WAY_LIST [like item_for_iteration]
			-- Items, as stored in Current, from positions
			-- 'sp' through 'ep'
			-- If 'sp' is greater than count, result is empty, not Void
			-- If 'ep' is greater than count, the end of range is 'count'
		require
			large_enough: sp > 0
			valid_range: ep >= sp
		local
			pos, epos: INTEGER
			ti: like item
		do
			create Result.make
			if sp <= count then
				-- 'content' is 0-based, so indices need to be biased
				if ep > count then
					epos := count - 1
				else
					epos := ep - 1
				end
				from pos := sp - 1
				until pos > epos
				loop
					ti := content.item (pos)
					if attached ti then
						Result.extend (ti)
					end
					pos := pos + 1
				end
			end
		end

	--|--------------------------------------------------------------

	items_matching_unsorted (mf: FUNCTION [G, BOOLEAN]): TWO_WAY_LIST [G]
			-- Items in Current that comply with matching function 'mf'
			-- Result is unsorted
		local
			oc: like cursor
			ti: like item_for_iteration
		do
			create Result.make
			oc := cursor
			from start
			until after
			loop
				ti := item_for_iteration
				if mf.item (ti) then
					Result.extend (ti)
				end
				forth
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	items_matching (mf: FUNCTION [G, BOOLEAN]): like contents_sorted
			-- Items in Current that comply with matching function 'mf'
			-- Result is sorted according to current sorting criteria
		local
			oc: like cursor
			ti: like item_for_iteration
		do
			create Result.make_multi (comparison_functions)
			oc := cursor
			from start
			until after
			loop
				ti := item_for_iteration
				if mf.item (ti) then
					Result.extend (ti)
				end
				forth
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	items_by_query (mf: FUNCTION [G, BOOLEAN]; cfl: like comparison_functions): like contents_sorted
			-- Items in Current that comply with matching function 'mf',
			-- and sorted according to comparison functions 'cfl'
			-- N.B. Does NOT Change existing sorting criteria
		local
			oc: like cursor
			ti: like item_for_iteration
		do
			create Result.make_multi (cfl)
			oc := cursor
			from start
			until after
			loop
				ti := item_for_iteration
				if mf.item (ti) then
					Result.extend (ti)
				end
				forth
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	query_results (q: AEL_DS_QUERY [G]): like contents_sorted
			-- Items in Current that result from executing query 'q'
			-- N.B. Does NOT Change existing sorting criteria
		local
			mf: FUNCTION [G, BOOLEAN]
			cfl: like comparison_functions
		do
			mf := q.matcher
			cfl := q.comparators
			Result := items_by_query (mf, cfl)
		end

--|========================================================================
feature -- Alternate representation
 --|========================================================================

	contents_sorted_to_list: TWO_WAY_LIST [G]
			-- Contents of this table sorted by item comparison, in the 
			-- form of a simple two-way list.
		do
			create Result.make
			Result.fill (contents_sorted_special (agent item_comparison))
		ensure
			exists: Result /= Void
			equivalent: Result.count = count
		end

	--|--------------------------------------------------------------

	contents_sorted: AEL_DS_LIST_AGENT_SORTED [G]
			-- Contents of this table sorted by item comparison, in the 
			-- form of a sortable list.
			-- If a generic two-way list result, though sorted, use the 
			-- 'contents_sorted_to_list' feature
		do
			Result := contents_sorted_special (agent item_comparison)
		ensure
			exists: Result /= Void
			equivalent: Result.count = count
		end

	--|--------------------------------------------------------------

	contents_sorted_special (sa: like dflt_comparison_agent): like contents_sorted
		-- Contents of this table sorted by item, using special 
		-- comparator 'sa'
		require
			comparator_exists: sa /= Void
		local
			cc: like cached_contents_sorted
		do
			cc := cached_contents_sorted
			if sort_is_coherent and cc /= Void then
				Result := cc
			else
				create Result.make (sa)
				cached_contents_sorted := Result
				from start
				until after
				loop
					Result.extend (item_for_iteration)
					forth
				end
				sort_is_coherent := True
			end
		ensure
			exists: Result /= Void
			equivalent: Result.count = count
		end

	--|--------------------------------------------------------------

	contents: LIST [like item_for_iteration]
			-- Contents of this table in linear form, unsorted
			-- N.B. This is not the same structure as 'content' but
			-- has the same values
		do
			if attached cached_contents_unsorted as ccu and sort_is_coherent then
				Result := ccu
			else
				Result := linear_representation
			end
			cached_contents_unsorted := Result
		ensure
			exists: Result /= Void
			equivalent: Result.count = count
		end

--|========================================================================
feature -- Status
--|========================================================================

	last_inserted_position: INTEGER
			-- Position, in content (and keys) or most recently inserted 
			-- item (0-based; -1 denotes 'none')
			-- Reliable only immediately following an insertion

	last_deleted_position: INTEGER
			-- Position, in content (and keys) of most recently deleted
			-- item (0-based; -1 denotes 'none')
			-- Reliable only immediately following a deletion

	item_comparison_function: like dflt_comparison_agent
			-- Optional special item comparison function
		do
			-- First comparison function
			if not comparison_functions.is_empty then
				Result := comparison_functions.first
			else
				Result := dflt_comparison_agent
			end
		end

	dflt_comparison_agent: FUNCTION [G, G, INTEGER]
			-- Type anchor reflecting agent for dflt_item_comparison
		do
			Result := agent dflt_item_comparison
		end

	comparison_functions: LINKED_LIST [FUNCTION [G, G, INTEGER]]
			-- Functions by which sorting comparison is performed

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	sort_is_coherent: BOOLEAN
			-- Are the cached sorted contents coherent with the table contents?

	cached_contents_sorted: like contents_sorted
			-- Contents of this table at last sorting event, sorted by item

	cached_contents_unsorted: detachable like contents
			-- Contents of this table at last cohernt point

	--|--------------------------------------------------------------

	item_comparison (v1, v2: like item_for_iteration): INTEGER
			-- Comparison of items v1 and v2
			-- If v1 is greater than v2 then Result is positive
			-- If v1 is less than v2 then Result is negative
			-- If v1 is equal to v2 then Result is 0
		require
			exist: v1 /= Void and v2 /= Void
		local
			cf: like item_comparison_function
		do
			cf := item_comparison_function
			if cf = Void then
				cf := dflt_comparison_agent
			end
			Result := cf.item ([v1, v2])
		end

	--|--------------------------------------------------------------

	dflt_item_comparison (v1, v2: like item_for_iteration): INTEGER
			-- Comparison of items v1 and v2
			-- If v1 is greater than v2 then Result is positive
			-- If v1 is less than v2 then Result is negative
			-- If v1 is equal to v2 then Result is 0
		require
			exist: v1 /= Void and v2 /= Void
		do
			if attached {COMPARABLE} v1 as c1
				and attached {COMPARABLE} v2 as c2
			 then
				if c1 < c2 then
					Result := -1
				elseif c1 > c2 then
					Result := 1
				end
			else
				-- not comparable
			end
		end

	--|--------------------------------------------------------------

	Kta_item: G
			-- Type anchor for items
		require False
		do
			check False then end
		ensure False
		end

	--|--------------------------------------------------------------

	Kta_key: K
			-- Type anchor for keus
		require False
		do
			check False then end
		ensure False
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 003 31-Mar-2021
--|     Updated to support multiple and alternate sorting criteria
--|     to exploit updates to AEL_DS_LIST_AGENT_SORTED
--|     Added basic query support (match+sort)
--|     Remove COMPARABLE constraint for items, to be consistent with
--|     AEL_DS_LIST_AGENT_SORTED
--|     Compiled and tested, void-safe, with 19.05
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| Create with an initial capacity.
--| Use as you would a normal HASH_TABLE.
--| As elements change, sorting becomes incoherent and is marked as such.
--| If sorted contents are desired, first check for coherence, then sort
--| if necessary.
--| If sorting by criteria other than that built into COMPARABLE, set
--| alternate comparison agents
--|
--| To execute a query whose result will be un-sorted, use the 
--| 'items_matching_unsorted' function, providing it with a matching 
--| function.
--| To execute a query whose result will be sorted according to the 
--| criteria already defined, use 'items_matching', providing it 
--| with a matching function.
--| To execute a query whose result will be sorted in a specific 
--| manner (not necessarily per the defined criteria), use 
--| 'items_by_query', providing both a matching function and a list
--| of sorting functions.
--|----------------------------------------------------------------------

end -- class AEL_DS_SORTABLE_HASH_TABLE
