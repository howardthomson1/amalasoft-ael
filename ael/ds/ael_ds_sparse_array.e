--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A sparse, indexable list structure, like a sparse ARRAY
--| Indexing is ALWAYS 1-based.
--| Unlike ARRAYs, 'count' denotes number of items contained, not capacity.
--| Structure supports paging and caching, and very large scale.
--|----------------------------------------------------------------------

class AEL_DS_SPARSE_ARRAY [G]

inherit
	CONTAINER [G]

create
	make

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	make (sz: INTEGER)
			-- Create Current with capacity at least 'sz
		require
			valid_size: sz > 0
		do
			create items.make
			capacity := even_8_multiple (sz)
			upper := capacity
			create cell_map.make (capacity)
		end

--|========================================================================
feature -- Measurement
--|========================================================================

	lower: INTEGER = 1
			-- lower bound

	upper: INTEGER
			-- upper bound

	capacity: INTEGER
			-- Total number of items that are _expected_

	count: INTEGER
			-- Number of items in structure (all might not be present)
			-- NOTE WELL!! This is NOT the same semantics as a fixed
			-- ARRAY;  Count is not necessarily (and seldom is) the
			-- same as capacity!!
		do
			Result := items.count
		end

	--|--------------------------------------------------------------

	is_empty: BOOLEAN
		-- Is Current empty?
		do
			Result := count = 1
		end

	--|--------------------------------------------------------------

	position_map: STRING
			-- String representation of cell map
		do
			Result := cell_map.out
		end

	--|--------------------------------------------------------------

	last_extend_position: INTEGER
			-- Index of item last inserted into Current by way of 
			-- 'extend'

--|========================================================================
feature -- Resizing
--|========================================================================

	grow_to (v: INTEGER)
			-- Grow capacity of Current to at least 'v'
		require
			not_smaller: v >= capacity
		do
			if v > capacity then
				cell_map.grow (even_8_multiple (v))
				--| No need to rearrange existing items
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	item alias "[]", at alias "@" (i: INTEGER): detachable G
			-- Entry at index `i', if in index interval
		require
			valid_index: i > 0 and i <= capacity
		local
			lp: INTEGER
		do
			if cell_map.position_is_set (i) then
				-- Item exists
				lp := cell_map.number_of_bits_set_before_position (i)
				Result := items.i_th (lp+1)
			end
		end

	--|--------------------------------------------------------------

	has (v: G): BOOLEAN
			-- Does `v' appear in array?
 			-- (Reference or object equality,
			-- based on `object_comparison'.)
		local
			tl: like items
			oc: CURSOR
		do
			tl := items
			oc := tl.cursor
			if object_comparison then
				from tl.start
				until tl.exhausted or Result
				loop
					if attached tl.item as ti then
						if attached v and then ti.is_equal (v) then
							Result := True
						end
					end
					if not Result then
						tl.forth
					end
				end
			else
				from tl.start
				until tl.exhausted or Result
				loop
					if v = tl.item then
						Result := True
					else
						tl.forth
					end
				end
			end
			tl.go_to (oc)
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	linear_representation: TWO_WAY_LIST [G]
			-- Representation as a linear structure
			-- Contains ONLY non-empty items
		do
			Result := items
		end

	new_cursor: TWO_WAY_LIST_ITERATION_CURSOR [G]
		do
			Result := items.new_cursor
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put (v: G; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
			-- Maintain map and item order
		require
			value_exists: v /= Void
			valid_index: i > 0 and i <= capacity
		local
			lp: INTEGER
			tl: like items
			oc: CURSOR
		do
			lp := cell_map.number_of_bits_set_before_position (i)
			tl := items
			oc := tl.cursor
			if not cell_map.position_is_set (i) then
				-- No item at that position yet
				cell_map.set_position (i)
				if lp = 0 then
					-- Is first
					tl.put_front (v)
				else
					tl.go_i_th (lp)
					tl.put_right (v)
				end
			else
				-- Item exists; replace it
				if lp = 0 then
					-- Is first
					tl.start
				else
					tl.go_i_th (lp + 1)
				end
				tl.replace (v)
			end
			tl.go_to (oc)
		ensure
			inserted: item (i) = v
		end

	--|--------------------------------------------------------------

	force (v: G; i: INTEGER)
			-- Replace `i'-th entry, if in index interval, by `v'.
			-- Resize current if needed
		require
			value_exists: v /= Void
			index_large_enough: i > 0
		do
			if i > capacity then
				grow_to (i)
			end
			put (v, i)
		ensure
			inserted: item (i) = v
		end

	--|--------------------------------------------------------------

	extend (v: G)
			-- Add 'v' to Current at first available position
			-- Resize if needed
		require
			value_exists: v /= Void
			is_new: not has (v)
		local
			p: INTEGER
		do
			last_extend_position := 0
			if count >= capacity then
				grow_to (count * 2)
			end
			p := cell_map.first_unset_position
			put (v, p)
			last_extend_position := p
		ensure
			extended: count = old count + 1
			added: has (v)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	clear_all
			-- Remove all items but leave capacity intact
		do
			items.wipe_out
			cell_map.clear_all
		end

	--|--------------------------------------------------------------

	remove_i_th (v: INTEGER)
			-- Remove from Current the item at position 'v'
		require
			valid_index: v > 0 and v <= capacity
			has_it: item (v) /= Void
		local
			lp: INTEGER
			oc: CURSOR
			tl: like items
		do
			tl := items
			lp := cell_map.number_of_bits_set_before_position (v)
			oc := tl.cursor
			if lp = 0 then
				tl.start
			else
				tl.go_i_th (lp + 1)
			end
			tl.remove
			if tl.valid_cursor (oc) then
				tl.go_to (oc)
			else
				tl.start
			end
			cell_map.unset_position (v)
		ensure
			removed: item (v) = Void
			reduced: count = old count - 1
		end

	--|--------------------------------------------------------------

	remove (v: G)
			-- Remove given item from Current
		require
			exists: v /= Void
			has_it: has (v)
		local
			tl: like items
			oc: CURSOR
			i, bp: INTEGER
			removed: BOOLEAN
		do
			tl := items
			oc := tl.cursor
			from tl.start
			until tl.exhausted or removed
			loop
				i := i + 1
				if tl.item = v then
					bp := cell_map.i_th_set_position (i)
					cell_map.unset_position (bp)
					tl.remove
					removed := True
				else
					tl.forth
				end
			end
			if tl.valid_cursor (oc) then
				tl.go_to (oc)
			end
		ensure
			removed: not has (v)
			reduced: count = old count - 1
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	items: TWO_WAY_LIST [G]
			-- Actual items

	cell_map: AEL_DS_BITMAP
			-- Bitmap denoting non-empty items

	--|--------------------------------------------------------------

	even_8_multiple (v: INTEGER): INTEGER
		-- Nearest value at or greater than 'v' that is an even 
		-- multiple of 8
		local
			nr: AEL_NUMERIC_ROUTINES
		do
			create nr
			Result := nr.rounded_to_next_even_mod (v, 8, False)
		end

 --|------------------------------------------------------------------------
invariant
	items_exist: items /= Void
	has_map: cell_map /= Void
	consistent_count: cell_map.number_of_set_positions = count

end -- class AEL_DS_SPARSE_ARRAY

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| To use this class, create a new instance with a given expected size.
--| Use much as you would an ARRAY, though with slightly different features
--|----------------------------------------------------------------------
