--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A large scale bitmap (bit set) structure that can be sparsely populated
--| for caching/paging support
--| For smaller bitmaps (e.g. less than 128k bits, use AEL_DS_BITMAP
--| 
--| Each bit in the cell map represents a bitmap cell of 131,072 bits 
--| (16384 bytes).
--| Each 8 bit byte in the map then represents 1,048,576 bits.
--| 
--| If the bit in the cell map is set, then the actual bitmap it represents
--| resides in the cell list at a position associated with the number of set
--| bits in the cell map.
--| 
--| For example, if bit 5 in the cell map is set and is the first bit set
--| (i.e. bits 1-4 are unset), then the first bitmap in the cell list
--| corresponds to bit position 5 in the cell map.
--| Because each cell is 131072 bits (16384 bytes), cell #5 represents overall
--| bit positions 4194305-5242880.
--| The bit (5) would be set in the cell map only if there were bits set in
--| cell #5.  Because each bit in the map represents a cell of 131,072 bits,
--| a byte in the map represents 1,048,576 bits (8 cells)
--|----------------------------------------------------------------------

class AEL_DS_SPARSE_BITMAP_64

inherit
	ANY
		redefine
			out
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER_64)
			-- Create Current with the given size (in bit positions)
		require
			valid_size: sz > 0 and (sz \\ bits_per_map_byte) = 0
		local
			nr: AEL_NUMERIC_INTEGER_ROUTINES
			bpb, cs, half_bits: INTEGER_64
		do
			create cell_list.make
			create nr
			cs := cell_size
			bpb := K_bits_per_byte.as_integer_64

			count := sz
			length := sz // bpb

			map_size := (sz // cs).as_integer_32
			width := (length // cs).as_integer_32

			half_bits := sz // 2
			create cell_map.make (map_size)
			if width > 1 then
				midpoint := nr.rounded_to_next_64_even_mod (half_bits, bpb, False) + 1
				midpoint_byte := half_bits // bpb
				if midpoint_byte \\ 2 /= 0 then
					midpoint_byte := midpoint_byte + 1
				end
			end
		end

--|========================================================================
feature -- Measurement
--|========================================================================

	count: INTEGER_64
		-- Number of bits in bitmap

	length: INTEGER_64
		-- Number of BYTES in bitmap

	--|--------------------------------------------------------------

	map_size: INTEGER_32
			-- Number of bits in the cell map

	width: INTEGER_32
			-- Number of bytes in the cell map

	bits_per_map_bit, cell_size: INTEGER = 131072
			-- Number of bits in a cell, and therefore the number of
			-- bits represented by a map bit

	bits_per_map_byte: INTEGER = 1048576
			-- Number of bits represented by a map Byte

	--|--------------------------------------------------------------

	midpoint: INTEGER_64
			-- Index in map of midpoint
			-- For 8 bit maps, midpoint is 0
			-- Midpoint is in the center of a byte
			-- when map uses odd number of bytes

	midpoint_byte: INTEGER_64
			-- Index in map of byte containing midpoint
			-- For 8 bit maps, midpoint_byte is 0

	--|--------------------------------------------------------------

	bits_set_before_midpoint: INTEGER_64
			-- Number of bits set between position 1 and the midpoint 
			-- position, exclusive of the midpoint.
			-- For 8 bit maps, always 0

	bits_set_after_midpoint: INTEGER_64
			-- Number of bits set between the midpoint and the last
			-- position, INclusive of the midpoint

	--|--------------------------------------------------------------

	number_of_set_positions: INTEGER_64
			-- Number of bit positions that are set
		do
			Result := bits_set_before_midpoint + bits_set_after_midpoint
		end

--|========================================================================
feature -- Status
--|========================================================================

	position_is_set (v: INTEGER_64): BOOLEAN
			-- Is the given 1-based position set?
		local
			cbit: INTEGER
			tc: detachable like cell_map
		do
			if v < midpoint and bits_set_before_midpoint = 0 then
				-- Don't bother; it's not set
			elseif v >= midpoint and bits_set_after_midpoint = 0 then
				-- ditto
			else
				tc := cell_for_position (v)
				if tc /= Void then
					cbit := index_to_position_in_cell (v)
					Result := tc.position_is_set (cbit)
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_position (v: INTEGER_64)
			-- Set the 'v-th' 1-based position in the map
		require
			in_range: is_valid_index (v)
		do
			if not position_is_set (v) then
				if v < midpoint then
					bits_set_before_midpoint := bits_set_before_midpoint + 1
				else
					bits_set_after_midpoint := bits_set_after_midpoint + 1
				end
			end
			set_position_value (v, True)
		ensure
			is_set: position_is_set (v)
		end

	--|--------------------------------------------------------------

	unset_position (v: INTEGER_64)
			-- Unset the 'v-th' 1-based position in the map
		require
			in_range: is_valid_index (v)
		do
			if position_is_set (v) then
				if v < midpoint then
					bits_set_before_midpoint := bits_set_before_midpoint - 1
				else
					bits_set_after_midpoint := bits_set_after_midpoint - 1
				end
			end
			set_position_value (v, False)
		ensure
			is_set: not position_is_set (v)
		end

--|========================================================================
feature -- External represtation
--|========================================================================

	out: STRING
			-- String representation of whole bitmap
			-- Limitations to string size prevent map from being 
			-- represented in bit-by-bit form, so only a summary is 
			-- presented
		do
			Result := "Size=" + count.out + " bits; " +
				number_of_set_positions.out + " set"
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (v: INTEGER_64): BOOLEAN
			-- Is the given number a valid index for this map?
		do
			Result := v > 0 and v <= count
		end

--|========================================================================
feature -- Access
--|========================================================================

	bits_set: LINKED_LIST [INTEGER_64]
			-- List of 1-based bit positions that are set
		local
			bl: LINKED_LIST [INTEGER_32]
			bc: CURSOR
			coff, i, lim: INTEGER_32
			tc: detachable like cell_for_position
		do
			create Result.make
			lim := map_size
			from i := 1
			until i > lim
			loop
				if cell_map.position_is_set (i) then
					tc := i_th_cell (i)
					if tc /= Void then
						bl := tc.bits_set
						bc := bl.cursor
						coff := (i - 1) * cell_size
						from bl.start
						until bl.exhausted
						loop
							Result.extend (bl.item + coff)
							bl.forth
						end
						bl.go_to (bc)
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	first_set_position: INTEGER_64
			-- First 1-based position that is set; 0 if none is set
		local
			cp, pos: INTEGER_64
		do
			cp := cell_map.first_set_position
			if cp /= 0 then
				pos := cell_list.first.first_set_position
				Result := ((cp - 1) * cell_size) + pos
			end
		end

	--|--------------------------------------------------------------

	first_unset_position: INTEGER_64
			-- First available (unset) 1-based position
		local
			lim, sp, bytes_half: INTEGER_64
		do
			if cell_list.is_empty then
				Result := 1
			else
				sp := 1
				bytes_half := count - midpoint
				if midpoint = 0 then
					lim := count
				elseif bits_set_before_midpoint = 0 then
					Result := 1
					lim := 0
				elseif bits_set_before_midpoint = bytes_half then
					-- All left bits are set; don't bother with the left side
					sp := midpoint
					if bits_set_after_midpoint = bytes_half then
						-- All right bits are set; don't bother with the right side
						lim := 0
					end
				else
					lim := count
				end
				if lim /= 0 then
					Result := first_unset_position_in_range (1, lim)
				end
			end
		end

	--|--------------------------------------------------------------

	first_set_position_in_range (sp, ep: INTEGER_64): INTEGER_64
			-- First 1-based position at or after position 'sp'
			-- and before or at position 'ep' with a value of '1'
		require
			in_range: is_valid_index (sp)
			end_in_range: ep = 0 or is_valid_index (ep)
		local
			i, pos, lim, cs, ce: INTEGER_32
			spos, epos, sbit, ebit: INTEGER_32
			tc: detachable like cell_for_position
			tl: like cell_numbers_in_range
		do
			tl := cell_numbers_in_range (sp, ep)
			sbit := index_to_position_in_cell (sp)
			ebit := index_to_position_in_cell (ep)
			cs := tl.first
			if tl.count = 1 then
				-- Range resides within a single cell
				if cell_map.position_is_set (cs) then
					tc := i_th_cell (cs)
					if tc /= Void then
						pos := tc.first_set_position_in_range (sbit, ebit)
						Result := ((cs - 1) * cell_size) + pos
					end
				end
			else
				-- Range spans multiple cells
				ce := tl.last
				lim := ce
				from i := cs
				until i > lim or Result /= 0
				loop
					if i = cs then
						spos := sbit
						epos := cell_size
					else
						spos := 1
						if i = ce then
							epos := ebit
						else
							epos := cell_size
						end
					end
					if cell_map.position_is_set (i) then
						tc := i_th_cell (i)
						if tc /= Void then
							pos := tc.first_set_position_in_range (spos, epos)
							if pos /= 0 then
								Result := ((i - 1) * cell_size) + pos
							end
						end
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	first_unset_position_in_range (sp, ep: INTEGER_64): INTEGER_64
			-- First 1-based position at or after position 'sp'
			-- and before or at position 'lim' with a value of '0'
		require
			in_range: is_valid_index (sp)
			end_in_range: ep = 0 or is_valid_index (ep)
		local
			i, pos, lim: INTEGER_32
			epos, sbit, ebit: INTEGER_32
			tc: detachable like cell_for_position
			tl: like cell_numbers_in_range
		do
			tl := cell_numbers_in_range (sp, ep)
			i := tl.first
			sbit := index_to_position_in_cell (sp)
			ebit := index_to_position_in_cell (ep)
			-- Check the first cell separately
			if tl.count = 1 then
				-- Range resides within a single cell
				if not cell_map.position_is_set (i) then
					-- There are no set positions, use the first position
					pos := 1
				else
					tc := i_th_cell (i)
					if tc /= Void then
						pos := tc.first_unset_position_in_range (sbit, ebit)
					end
				end
				Result := ((i - 1) * cell_size) + pos
				-- bump cell index to prevent additional checks
			else
				-- Range spans cells
				-- Check start of range in first cell
				if tc /= Void then
					tc := i_th_cell (i)
					if tc /= Void then
						pos := tc.first_unset_position_in_range (sbit, cell_size)
						if pos /= 0 then
							-- Cell has an unset position
							Result := ((i - 1) * cell_size) + pos
						else
							-- No unset positions in this cell in range
						end
					end
				end
			end
			-- First cell in range has been checked
			-- If we didn't find an unset position, try the other cells
			lim := tl.last
			from i := i + 1
			until i > lim or Result /= 0
			loop
				if i = lim then
					-- Looking at last cell
					epos := ebit
				else
					epos := cell_size
				end
				tc := i_th_cell (i)
				if tc /= Void then
					pos := tc.first_unset_position_in_range (1, epos)
					if pos /= 0 then
						-- Cell has an unset position
						Result := ((i - 1) * cell_size) + pos
					else
						-- No unset positions in this cell in range
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	next_set_position (v: INTEGER_64): INTEGER_64
			-- First 1-based position that is set at 'v' or after;
			-- 0 if none is set
		require
			in_range: is_valid_index (v)
		local
			cnum, bnum, i, lim, pos: INTEGER_32
			tc: detachable like cell_for_position
		do
			bnum := index_to_position_in_cell (v)
			cnum := index_to_cell_number (v)
			lim := map_size
			from i := cnum
			until i > lim or Result /= 0
			loop
				if cell_map.position_is_set (i) then
					tc := i_th_cell (i)
					if tc /= Void then
						if i /= cnum then
							bnum := 1
						end
						pos := tc.next_set_position (bnum)
						if pos /= 0 then
							Result := ((i - 1) * cell_size) + pos
						end
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	next_unset_position (v: INTEGER_64): INTEGER_64
			-- First 1-based position that is unset at 'v' or after;
			-- 0 if all are set
		require
			in_range: is_valid_index (v)
		local
			bnum: INTEGER_32
			cnum, i, lim, pos: INTEGER_32
			tc: detachable like cell_for_position
		do
			bnum := index_to_position_in_cell (v)
			cnum := index_to_cell_number (v)
			lim := map_size
			from i := cnum
			until i > lim or Result /= 0
			loop
				if not cell_map.position_is_set (i) then
					-- No bits set, use first one
					Result := ((i - 1) * cell_size) + 1
				else
					-- Some bits are set
					tc := i_th_cell (i)
					if tc /= Void then
						if i /= cnum then
							bnum := 1
						end
						pos := tc.next_unset_position (bnum)
						if pos /= 0 then
							Result := ((i - 1) * cell_size) + pos
						end
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	highest_set_position: INTEGER_64
			-- Highest 1-based bit position set
		local
			i, pos: INTEGER_32
			tc: detachable like cell_for_position
		do
			from i := map_size
			until i = 0 or Result /= 0
			loop
				if cell_map.position_is_set (i) then
					-- Worth a look
					tc := i_th_cell (i)
					if tc /= Void then
						pos := tc.highest_set_position
						if pos /= 0 then
							Result := ((i - 1) * cell_size) + pos
						end
					end
				end
				i := i - 1
			end
		end

	--|--------------------------------------------------------------

	highest_unset_position: INTEGER_64
			-- Highest 1-based position number unset
		local
			i, pos: INTEGER_32
			tc: detachable like cell_for_position
		do
			from i := map_size
			until i = 0 or Result /= 0
			loop
				if not cell_map.position_is_set (i) then
					-- None set, take the highest bit
					Result := ((i - 1) * cell_size) + cell_size
				else
					-- Worth a look
					tc := i_th_cell (i)
					if tc /= Void then
						pos := tc.highest_unset_position
						if pos /= 0 then
							Result := ((i - 1) * cell_size) + pos
						end
					end
				end
				i := i - 1
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	index_to_position_in_cell (v: INTEGER_64): INTEGER_32
			-- The bit positions (in the hypothetical cell)
			-- corresponding to the given overall index.
			-- 1-based
		require
			in_range: is_valid_index (v)
		do
			Result := (v \\ cell_size).as_integer_32
			if Result = 0 then
				-- Even mod indicates highest bit of previous byte
				Result := cell_size
			end
		end

	--|--------------------------------------------------------------

	index_to_cell_number (v: INTEGER_64): INTEGER_32
			-- The cell in which the given overall index would reside
			-- 1-based
		require
			in_range: is_valid_index (v)
		local
			tr, bpos: INTEGER_64
		do
			tr := (v // cell_size) + 1
			bpos := (v \\ cell_size)
			if bpos = 0 then
				-- Even mod indicates highest bit of previous byte
				tr := tr - 1
			end
			Result := tr.as_integer_32
		end

	--|--------------------------------------------------------------

	cell_numbers_in_range (sp, ep: INTEGER_64): LINKED_LIST [INTEGER_32]
			-- The cell numbers in which the given bit ranges would reside
			-- 1-based
		require
			in_range: is_valid_index (sp)
			end_in_range: ep = 0 or is_valid_index (ep)
		local
			i, lim: INTEGER_32
		do
			create Result.make
			lim := index_to_cell_number (ep)
			from i := index_to_cell_number (sp)
			until i > lim
			loop
				Result.extend (i)
				i := i + 1
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	set_position_value (p: INTEGER_64; v: BOOLEAN)
			-- Set the 'p-th' bit in the map to 'v' (True or False)
		require
			in_range: is_valid_index (p)
		local
			tc: detachable like cell_map
			lim: INTEGER_32
			cnum: INTEGER_32
			cl: like cell_list
			oc: CURSOR
		do
			cnum := index_to_cell_number (p)
			tc := cell_for_position (p)
			if tc = Void then
				-- No cell yet for that position
				create tc.make (cell_size)
				cl := cell_list
				if cl.is_empty then
					cl.extend (tc)
				else
					lim := cell_map.number_of_bits_set_before_position (cnum)
					if lim = 0 then
						cl.put_front (tc)
					else
						oc := cl.cursor
						cl.go_i_th (lim)
						-- Cell list should be at position before the new cell
						cl.put_right (tc)
						cl.go_to (oc)
					end
				end
			end
			if v then
				tc.set_position (index_to_position_in_cell (p))
				cell_map.set_position (cnum)
			else
				tc.unset_position (index_to_position_in_cell (p))
				if tc.number_of_set_positions = 0 then
					-- If no bits remain set in tc, then remove it
					-- from list and update bitmap
					cell_map.unset_position (cnum)
					cl := cell_list
					oc := cl.cursor
					cl.start
					cl.search (tc)
					cl.remove
					if cl.valid_cursor (oc) then
						cl.go_to (oc)
					else
						cl.start
					end
				end
			end
		ensure
			is_set: v implies position_is_set (p)
			is_unset: (not v) implies not position_is_set (p)
		end

	--|--------------------------------------------------------------

	cell_for_position (v: INTEGER_64): detachable AEL_DS_BITMAP
			-- The cell that would hold the 'v-th' bit in the map
			-- Void if map is unallocated
		require
			in_range: is_valid_index (v)
		do
			Result := i_th_cell (index_to_cell_number (v))
		ensure
			exists_if_set: position_is_set (v) implies Result /= Void
		end

	--|--------------------------------------------------------------

	i_th_cell (v: INTEGER_32): detachable AEL_DS_BITMAP
			-- The cell represented by the the i_th position in the cell 
			-- map
			-- Void if not in the cell list
		require
			in_range: v > 0 and v <= map_size
		local
			pos: INTEGER_32
			cl: like cell_list
			oc: CURSOR
		do
			if cell_map.position_is_set (v) then
				-- A cell exists that is represented by that bit position
				-- Find its place in the cell list
				pos := cell_map.number_of_bits_set_before_position (v)
				cl := cell_list
				if pos = 0 then
					-- It's first in line
					Result := cl.first
				else
					oc := cl.cursor
					cl.go_i_th (pos + 1)
					-- Cell list should be at correct item
					Result := cl.item
					cl.go_to (oc)
				end
			end
		ensure
			exists_if_set: cell_map.position_is_set (v) implies Result /= Void
		end

--|========================================================================
feature {NONE} -- Structures
--|========================================================================

	cell_map: AEL_DS_BITMAP
			-- Map of the cells that make up the bitmap

	cell_list: TWO_WAY_LIST [like cell_map]
			-- List of actual bitmaps (those with set bits)

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_bits_per_byte: INTEGER = 8

end -- class AEL_DS_SPARSE_BITMAP_64

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Reformatted for polite consumption.
--|----------------------------------------------------------------------
--| How-to
--| To use this class, create a new instance with a given expected size.
--| Use much as you would an AEL_DS_BITMAP
--|----------------------------------------------------------------------
