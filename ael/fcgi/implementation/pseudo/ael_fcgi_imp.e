class AEL_FCGI_IMP

inherit
	AEL_HTTP_APP_CONSTANTS
	AEL_HTTP_STATUS_CODES
	AEL_FCGI_I

--|========================================================================
feature -- FCGI interface
--|========================================================================

	fcgi_listen: INTEGER
			-- Listen to the FCGI input stream
			-- Return 0 for successful calls, -1 otherwise.
		do
			-- Set state on first call to prevent looping indefinitely
			if fcgi_has_run then
				Result := -1
			elseif not is_interactive then
				fcgi_has_run := True
			end
		end

	fcgi_has_run: BOOLEAN
			-- For emulation only; Has fcgi_listen been called?

	--|--------------------------------------------------------------

	fcgi_finish
			-- Finish current request from HTTP server started from
			-- the most recent call to `fcgi_accept'.
		do
		end

	--|--------------------------------------------------------------

	put_string (a_str: STRING)
			-- Put `a_str' on the FastCGI stdout.
		do
			io.put_string (a_str)
		end

 --|--------------------------------------------------------------

	fcgi_printf (fmt: STRING; args: FINITE[ANY])
			-- Put args, formatted per 'fmt' on the FastCGI stdout.
		do
			apf.printf (fmt, args)
		end

	--|--------------------------------------------------------------

	set_fcgi_exit_status (v: INTEGER)
		do
		end

--|========================================================================
feature -- I/O Routines
--|========================================================================

	read_from_stdin (n: INTEGER)
			-- Read up to n bytes from stdin and store in input buffer
		do
		end

 --|--------------------------------------------------------------

	copy_from_stdin (n: INTEGER; tf: FILE)
			-- Read up to n bytes from stdin and write to given file
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	buffer_contents: STRING
		do
			Result := private_input_buffer
			if Result = Void then
				Result := ""
			end
		end

	buffer_capacity: INTEGER
		do
			if private_input_buffer /= Void then
				Result := private_input_buffer.capacity
			end
		end

end -- class AEL_FCGI_IMP
