note
	description: "Wrappers around FastCGI C API."
	date: "$Date$"
	revision: "$Revision$"

class
	FCGI

feature

	accept: INTEGER
			-- Accept a Fast CGI connection.
			-- Return 0 for successful calls, -1 otherwise.
		external
			"dll libfcgi.dll signature (): EIF_INTEGER use fcgi_stdio.h "
		alias
			"FCGI_Accept"
		end

 --|--------------------------------------------------------------

	finish
			-- Finished current request from HTTP server started from
			-- the most recent call to `fcgi_accept'.
		external
			"dll libfcgi.dll signature () use fcgi_stdio.h "
		alias
			"FCGI_Finish"
		end


 --|--------------------------------------------------------------

--	test (v: INTEGER)
--			--
--		external
--			"C inline use %"fcgi_stdio.h%""
--		alias
--			"[
--				char *s = getenv("SERVER_HOSTNAME");

--				if (!s) {
--					s = "Cannot get Host";
--				}
--				FCGI_printf(
--  		  			"Content-type: text/html\r\n"
--					"\r\n"
--					"<title>FastCGI Hello! (C, fcgi_stdio library)</title>"
--					"<h1>FastCGI Hello! (C, fcgi_stdio library)</h1>"
--					"Request number %d running on host <i>%s</i>\n",
--					$v, s);
--			]"
--		end

 --|--------------------------------------------------------------

	put_string (v: POINTER; n: INTEGER)
--		external
--			"C inline use %"fcgi_stdio.h%""
--		alias
--			"[
--				FCGI_fwrite($v, 1, $n, FCGI_stdout);
--			]"
--		end
		local
			i: INTEGER
		do
			i := fcgi_fwrite (v, 1, n, fcgi_stdout)
		end

 --|--------------------------------------------------------------

	fcgi_fwrite (v: POINTER; a_size: INTEGER; n: INTEGER; fp: POINTER): INTEGER
			-- FCGI_fwrite() ouput `v' to `fp'
		external
--			"C [dll32 %"libfcgi.dll%"] (EIF_POINTER, EIF_INTEGER, EIF_INTEGER, EIF_POINTER): EIF_INTEGER"
			"dll libfcgi.dll signature (EIF_POINTER, EIF_INTEGER, EIF_INTEGER, EIF_POINTER): EIF_INTEGER use fcgi_stdio.h "
		alias
			"FCGI_fwrite"
		end

	fcgi_fread (v: POINTER; a_size: INTEGER; n: INTEGER; fp: POINTER): INTEGER
			-- FCGI_fread() read from input `fp' and put into `v'
		external
			"dll libfcgi.dll signature (EIF_POINTER, EIF_INTEGER, EIF_INTEGER, EIF_POINTER): EIF_INTEGER use fcgi_stdio.h "
		alias
			"FCGI_fread"
		end

	fcgi_feof (v: POINTER): INTEGER
			-- FCGI_feof()
		external
			"dll libfcgi.dll signature (EIF_POINTER): EIF_INTEGER use fcgi_stdio.h "
		alias
			"FCGI_feof"
		end

	fcgi_stdout: POINTER
			-- FCGI_stdout() return pointer on output FCGI_FILE
--		external
--			"dll libfcgi.dll signature (): EIF_POINTER use fcgi_stdio.h "
		external
			"C inline use %"fcgi_stdio.h%""
		alias
			"FCGI_stdout"
		end

	fcgi_stdin: POINTER
			-- FCGI_stdin() return pointer on input FCGI_FILE
--		external
--			"dll libfcgi.dll signature (): EIF_POINTER use fcgi_stdio.h "
		external
			"C inline use %"fcgi_stdio.h%""
		alias
			"FCGI_stdin"
		end


	frozen read_content_into (a_buffer: POINTER; a_length: INTEGER): INTEGER
			-- Read content stream into `a_buffer' but no more than `a_length' character.
--		external
--			"C inline use %"fcgi_stdio.h%""
--		alias
--			"[
--				{
--					size_t n;
--					if (! FCGI_feof(FCGI_stdin)) {
--						n = FCGI_fread($a_buffer, 1, $a_length, FCGI_stdin);
--					} else {
--						 n = 0;
--					}
--					return n;
--				}
--			]"
--		end
		local
			i: INTEGER
			l_stdin: POINTER
		do
			l_stdin := fcgi_stdin
			i := fcgi_feof (l_stdin)
			if i = 0 then
				Result := 0
			else
				Result := fcgi_fread(a_buffer, 1, a_length, l_stdin)
			end
		end


 --|--------------------------------------------------------------

	gets (s: POINTER): POINTER
			-- gets() reads a line from stdin into the buffer pointed to
			-- by s until either a terminating newline or EOF, which it
			-- replaces with '\0'
			-- No check for buffer overrun is performed
		external
			"dll libfcgi.dll signature (EIF_POINTER): EIF_POINTER use fcgi_stdio.h "
		alias
			"FCGI_gets"
		end

 --|--------------------------------------------------------------

	set_exit_status (v: INTEGER)
			-- Set the exit status for the most recent request
		external
			"dll libfcgi.dll signature (EIF_INTEGER) use fcgi_stdio.h "
		alias
			"FCGI_SetExitStatus"
		end

end
