class MAIN_PAGE
-- Placeholder main html page

inherit
	APPLICATION_ENV
		rename
			valid_code as valid_exception_code
		undefine
			is_equal, copy, out
		end
	STRING
		rename
			make as str_make
		end

create
	make

create {STRING}
	str_make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			str_make (1024)
			build_page
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	build_page
			-- Build page contents
		do
			wipe_out
			append ("<html><head><title>")
			append (application_name)
			append (" Main Page</title></head>%N<body><h1>")
			append (application_name)
			append (" Main Page</h1><p>Welcome to ")
			append (application_name)
			append ("!</p><ul><li><a href='/signup'>Sign Up</a></li><li><a href='/signin'Sign In</a></li></ul></body></html>")
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

--RFO 	apf: AEL_PRINTF
--RFO 		once
--RFO 			create Result
--RFO 		end

	--|--------------------------------------------------------------
invariant
	not_empty: not is_empty

end -- class MAIN_PAGE
