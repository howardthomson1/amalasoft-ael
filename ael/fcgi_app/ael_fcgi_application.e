deferred class AEL_FCGI_APPLICATION
-- Template for application that uses FCGI I/O

inherit
	AEL_HTTP_SERVER_APPLICATION
		redefine
			setup_for_debug
		end

--|========================================================================
feature -- I/O Connector routines
--|========================================================================

	read_input_stream
			-- Read input stream
			-- Set input_stream_state to reflect result of read action
			-- Read from the connector
		do
			if is_interactive then
				-- Must be in a pseudo/emulation mode
				input_stream_state := 0
			else
				input_stream_state := fcgi.fcgi_listen
			end
		end

	--|--------------------------------------------------------------

	put_string (v: STRING)
			-- Put `v' on the FastCGI stdout.
		do
			fcgi.put_string (v)
		end

	--|--------------------------------------------------------------

	terminate_input_stream
			-- Shut down input from connector
		do
			fcgi.fcgi_finish
		end

	--|--------------------------------------------------------------

	set_connector_exit_status (v: INTEGER)
			-- Convey exit status 'v' to the input connector
		do
			fcgi.set_fcgi_exit_status (v)
		end

--|========================================================================
feature {NONE} -- Debug support
--|========================================================================

	setup_for_debug (arg: STRING)
		do
			Precursor (arg)
			if not has_error then
				-- FCGI will clobber env vars on each successive call
				xenv.put ("Responder", "FCGI_ROLE")
			end
		end

--|========================================================================
feature {NONE} -- FCGI Connector
--|========================================================================

	fcgi: AEL_FCGI
		once
			create Result
		end

end -- class AEL_FCGI_APPLICATION
