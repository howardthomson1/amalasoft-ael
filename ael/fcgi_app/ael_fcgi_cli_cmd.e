deferred class AEL_FCGI_CLI_CMD

inherit
	AEL_SPRT_CLI_CMD

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_cmd_list: INTEGER = 10
	K_cmd_create: INTEGER = 11
	K_cmd_set: INTEGER = 12
	K_cmd_get: INTEGER = 13
	K_cmd_delete: INTEGER = 14
	K_cmd_show_version: INTEGER = 15

end -- class AEL_FCGI_CLI_CMD
