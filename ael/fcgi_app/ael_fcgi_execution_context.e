class AEL_FCGI_EXECUTION_CONTEXT

inherit
	AEL_HTTP_EXECUTION_CONTEXT
		redefine
			extract_variables, to_tv_pairs,
			fmt_out_arg_list, to_html, input
		end
--	AEL_HTTP_APP_LOGGING

create
	make

--|========================================================================
feature -- Analysis
--|========================================================================

	extract_variables
		do
			fcgi_role := env_var ("FCGI_ROLE")
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	input: AEL_FCGI_SERVER_INPUT
			-- Input from FCGI connector

	fcgi_role: STRING
			-- FCGI-specific env var

--|========================================================================
feature -- External representation
--|========================================================================

	to_html: STRING
		-- Context info in HTML table form
		local
			fmt: STRING
		do
			fmt := "{
<TABLE BORDER=1>
<TR><TD>PID</TD><TD>%d</TD</TR>
<TR><TD>REQ ID</TD><TD>%d</TD></TR>
<TR><TD>Method</TD><TD>%s</TD></TR>
<TR><TD>Role</TD><TD>%s</TD></TR>
<TR><TD>URI</TD><TD>%s</TD></TR>
<TR><TD>Auth</TD><TD>%s</TD></TR>
<TR><TD>Cookie</TD><TD>%s</TD></TR>
</TABLE>
				}"
			Result := formatted_out (fmt)
		end

 --|--------------------------------------------------------------

	to_tv_pairs: STRING
		-- Context info in tag-value pairs form
		local
			fmt: STRING
		do
			fmt := "{
PID=%d
REQ ID=%d
Method=%s
Role=%s
URI=%s
Auth=%s
Cookie=%s
				}"
			Result := formatted_out (fmt)
		end

 --|--------------------------------------------------------------

	fmt_out_arg_list: ARRAY [ANY]
		do
			Result := << process_id, request_id,
			var_out (request_method), var_out (fcgi_role),
			var_out (uri_string), var_out (authorization), var_out (cookie) >>
		end

end -- class AEL_FCGI_EXECUTION_CONTEXT
