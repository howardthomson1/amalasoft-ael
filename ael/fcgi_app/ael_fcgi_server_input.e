class AEL_FCGI_SERVER_INPUT

inherit
	AEL_FCGI
		undefine
			private_input_buffer
		redefine
			buffer_contents
		end
	AEL_HTTP_SERVER_INPUT
		undefine
			buffer_capacity, read_from_stdin, copy_from_stdin, apf
		redefine
			buffer_contents
		end

create
	make

--|========================================================================
feature -- Status
--|========================================================================

	buffer_contents: STRING
		do
			Result := Precursor{AEL_HTTP_SERVER_INPUT}
			if Result = Void or else Result.is_empty then
				Result := Precursor{AEL_FCGI}
			end
		end

end -- class AEL_FCGI_AGENT_INPUT
