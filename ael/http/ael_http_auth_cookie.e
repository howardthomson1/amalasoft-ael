class AEL_HTTP_AUTH_COOKIE
-- HTTP cookie used for authentication
-- exp=t&data=D&digest=MAC(exp=t&data=D)
-- Expiration time is 't' and is UTC sectonds
-- Data string D denotes arbitrary data the server associates
-- with the client (user ID).
-- The MAC is a non-malleable MAC of the clear text expiration
-- and the data
-- The server has a secret key.  The client never has the key
-- and relies solely on the cookie to gain access.  The cookie
-- expires at the time defined in the Cookie.  If the client fails
-- to expire the cookie in time, the server will fail requests
-- using the stale cooking with a 401 status.
-- The MAC we use is HMAC-SHA1
--
-- Cookie header from servers has the form:
--   Set-Cookie2: aelhttpauth = <encoded-cookie>

inherit
	AEL_SPRT_MULTI_FALLIBLE

create
	make_for_name, make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_for_name (nm: STRING; skey: STRING)
			-- Create a cookie for the given user (account) name
			-- Secret key is common to all servers in a realm and
			-- changes periodically
			-- Cookie has the form:
			-- <cookie> ::= <clear_part>':'<digest>
			-- <clear_part> ::= <zero-filled hex utc>'-'<name>
			-- <digest> ::= hmacsha1 (<clear_part>,<secret_key>)
		require
			name_exists: nm /= Void and then not nm.is_empty
			key_exists: skey /= Void and then not skey.is_empty
		local
			ts: STRING
		do
			user_name := nm
			secret_key := skey
			create ts.make (nm.count + 36)
			utc := {AEL_UNIX_DATE}.current_utc
			ts.append (clear_part (utc, user_name))
			digest := to_digest (ts, skey)
		ensure
			valid_cookie: is_valid_cookie_string (as_string)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create a cookie from the given string.
			-- The secret key cannot be extracted, but the user name
			-- and UTC can be
		require
			exists: v /= Void and then not v.is_empty
		local
			spos, epos: INTEGER
		do
			spos := v.index_of ('-', 1)
			if spos = 0 then
				set_error_message ("Malformed.  Missing '-' separator.")
			else
				if not asr.substring_is_hex_integer (v, 1, spos - 1) then
					set_error_message ("Malformed.  Timestamp is not hexadecimal.")
				end
			end
			if not has_error then
				utc := asr.hex_string_to_natural_32 (v.substring (1,spos-1))
				epos := v.index_of (':', spos)
				if epos = 0 then
					set_error_message ("Malformed.  Missing ':' separator.")
				elseif epos = spos + 1 then
					set_error_message ("Malformed.  Missing user name field.")
				end
			end
			if not has_error then
				user_name := v.substring (spos + 1, epos - 1)
				digest := v.substring (epos + 1, v.count)
			end
		ensure
			made: not has_error implies is_valid_cookie_string (as_string)
		end

--|========================================================================
feature -- Status
--|========================================================================

	user_name: STRING
			-- Name of authorized user

	secret_key: STRING
			-- Key used to generate this cookie

	utc: NATURAL_32
			-- UTC time at which cookie was created

	digest: STRING
			-- Base64 form of digest

	--|--------------------------------------------------------------

	as_string: STRING
			-- Assembled cookie as a string
			-- Cookie has the form:
			-- <cookie> ::= <clear_part>':'<digest>
			-- <clear_part> ::= <zero-filled hex utc>'-'<name>
		do
			create Result.make (user_name.count + 10 + digest.count)
			if not has_error then
				Result.append (clear_part (utc, user_name))
				Result.extend (':')
				Result.append (digest)
			end
		ensure
			exists: Result /= Void
			is_valid: not has_error implies is_valid_cookie_string (Result)
		end

--|========================================================================
feature -- Comparison and validation
--|========================================================================

	cookie_is_consistent (other: like Current; tp, tm: INTEGER): BOOLEAN
			-- Is the given cookie consistent with this cookie, within
			-- the time range allowed?
			-- tp = time plus = number of seconds that the given string
			-- can be newer than current
			-- tm = time minus = number of seconds that the given string
			-- can be older than current
		require
			exists: other /= Void
			other_valid: not other.has_error
			have_secret: secret_key /= Void
		do
			if user_name.is_equal (other.user_name) then
				if other.utc <= (utc + tp.to_natural_32) and
					other.utc >= utc - tm.to_natural_32
				 then
					 -- Time stamps are in range
					 -- Now check digests
					 -- Generate a new digest from the user name and the
					 -- other cookie's UTC and our secret key.
					 -- It should match the other cookie's digest.
					 Result := digest_matches_values (
						 other.digest, user_name, other.utc)
				end
			end
		end

	--|--------------------------------------------------------------

	string_is_consistent_cookie (v: STRING; tp, tm: INTEGER): BOOLEAN
			-- Does the given cookie string match this cookie within
			-- the time range allowed?
			-- tp = time plus = number of seconds that the given string
			-- can be newer than current
			-- tm = time minux = number of seconds that the given string
			-- can be older than current
		require
			exists: v /= Void and then not v.is_empty
		local
			tc: like Current
		do
			if is_valid_cookie_string (v) then
				create tc.make_from_string (v)
				Result := cookie_is_consistent (tc, tp, tm)
			end
		end

	--|--------------------------------------------------------------

	digest_matches_values (d, nm: STRING; u: like utc): BOOLEAN
			-- Does the given digest 'd' match the given values for user
			-- name 'nm' and utc 'u', using our secret key?
		require
			digest_exists: d /= Void
			name_exists: nm /= Void
			have_secret: secret_key /= Void
		local
			td: STRING
		do
			td := to_digest (clear_part (u, nm), secret_key)
			Result := td.is_equal (d)
		end

	--|--------------------------------------------------------------

 	is_valid_cookie_string (v: STRING): BOOLEAN
			-- Is the given string a well-formed cookie?
		local
			spos, epos: INTEGER
		do
			if v /= Void and then not v.is_empty then
				spos := v.index_of ('-', 1)
				if spos > 1 and spos < v.count then
					if asr.substring_is_hex_integer (v, 1, spos - 1) then
						epos := v.index_of (':', spos)
						if epos > spos then
							Result := True
						end
					end
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	clear_part (tu: like utc; nm: STRING): STRING
			-- The cleartext field made from the give components
			-- Concatenation of UTC as hex, and name with '-' separator
		require
			name_exists: nm /= Void and then not nm.is_empty
		do
			Result := tu.to_hex_string + "-" + nm
		end

	--|--------------------------------------------------------------

	to_digest (cp, skey: STRING): STRING
			-- Base64 form of HMAC/SHA1 digest of given clear part 'cp'
			-- using given secrete key 'skey'
		require
			clear_part_exists: cp /= Void and then not cp.is_empty
			is_clear_part: cp.index_of ('-',1) /= 0
			key_exists: skey /= Void and then not skey.is_empty
		do
--RFO TODO			Result := crypto.hmacsha1_as_base64 (cp, skey)
		ensure
			exists: Result /= Void and then not Result.is_empty
			is_base64: b64.is_decodable_string (Result)
		end

--|========================================================================
feature {NONE} -- Support routines
--|========================================================================

	crypto: AEL_SPRT_CRYPTO_ROUTINES
		once
			create Result
		end

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	b64: AEL_SPRT_BASE64
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant
	named: not has_error implies
		(user_name /= Void and then not user_name.is_empty)
	timed: not has_error implies utc /= 0
	has_digest: not has_error implies digest /= Void

end -- class AEL_HTTP_AUTH_COOKIE
