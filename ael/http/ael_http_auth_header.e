note
	description: "Encapsulation of an HTTP Authentication header (rfc 2617)"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create an instance of this class either empty (using make), or in the
form of a response by using make_as_response_from_string and providing
a string argument.
}"

class AEL_HTTP_AUTH_HEADER

create
	make, make_as_response_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
		end

	--|--------------------------------------------------------------

	make_as_response_from_string (v: STRING)
		require
			exists: v /= Void and then not v.is_empty
		do
			make
			init_as_response_from_string (v)
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	init_as_response_from_string (v: STRING)
		require
			exists: v /= Void and then not v.is_empty
			is_auth_header: is_valid_response_header (v)
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	uses_digest: BOOLEAN
			-- Does this header use a digest challenge?
		do
			Result := challenge = Void or else challenge.is_equal ("Digest")
		end

	include_stale: BOOLEAN
			-- Does this header include the 'stale' parameter?

	--|--------------------------------------------------------------

	as_respone_header: STRING
			-- Assembled values constucted as a response header
			-- (a.k.a. challenge) from server to client
			--
			-- Response header has the form:
			-- <challenge> ::= "Digest" <digest-challenge>
			-- <digest-challenge> ::= 1#( <realm> | [<domain>] |
			--                        <nonce> | [<opaque>] | [<stale>] |
			--                        <algorithm> | [<qop-options>] |
			--                        [<auth-param>] )
			-- Where:
			--  <realm>         ::= <server defined realm tag>
			--  <domain>        ::= "domain=" <URI-list>
			--  <URI-list>      ::= '"' <URI> (1*SP <URI>) '"'
			--    <URI>         ::= <absoluteURI> | <abs_path>
			--  <nonce>         ::= "nonce=" <nonce-value>
			--    <nonce-value> ::= <quoted-string>
			--  <opaque>        ::= "opaque=" <quoted-string>
			--  <stale>         ::= "stale=" ("true" | "false")
			--  <algorithm>     ::= "algorithm=" ("MD5" | "MD5-sess" | <token>)
			--  <qop-options>   ::= "qop=" '"' <qop-value> '"'
			--  <qop-value>     ::= "auth" | "auth-int" | <token>
			--
		do
			create Result.make (128)
			Result.append (Ks_authenticate_header_tag)
			Result.append (challenge)
			if uses_digest then
				Result.append (digest_challenge)
			else
			end
		end

	--|--------------------------------------------------------------

	as_request_header: STRING
			-- Assembled values constucted as a request header
			-- (a.k.a. challenge response) from client to server
			--
			-- Request header has the form:
			-- <credentials> ::= "Digest" <digest-response>
			--
			-- <digest-response> ::= 1#( <name> | <realm> | <nonce> |
			--                        <digest-uri> | [<response>] |
			--                        [<algorithm>] | [<cnonce>] |
			--                        [<opaque>] | [<message_qop>] |
			--                        [<nonce-count>] | [<auth-param>] )
			-- Where:
			--  <name>           ::= "username=" <quoted-string>
			--  <realm>          ::= <server defined realm tag>
			--  <nonce>          ::= "nonce=" <quoted-string>
			--  <digest-uri>     ::= "uri=" '"' <request-uri> '"'
			--  <response>       ::= "response=" <request-digest>
			--  <request-digest> ::= '"' <32 digit lower case hex value> '"'
			--  <algorithm>      ::= "algorithm=" ("MD5" | "MD5-sess" | <token>)
			--  <cnonce>         ::= "cnonce=" <nonce-value>
			--  <opaque>         ::= "opaque=" <quoted-string>
			--  <message-qop>    ::= "qop=" <qop-value>
			--  <qop-value>      ::= "auth" | "auth-int" | <token>
			--  <nonce-count>    ::= "nc=" <8-digit lower case hex value>
			--
			-- Values for opaque and algorithm must be the same as 
			-- response.
			--
			-- cnonce and nonce-count are required when qop is specfied,
			-- and forbidden when qop is not specified
		do
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_response_header (v: STRING): BOOLEAN
		-- is the given string a valid WWW-Authenticate response header?
		do
			if v /= Void and then not v.is_empty then
				if v.substring_index (Ks_authenticate_header_tag, 1) = 1 then
					--RFO TODO need to beef this up a lot
					Result := True
				end
			end
		end

--|========================================================================
feature -- Header components
--|========================================================================

	challenge: STRING = "Digest"
			-- Keyword identifying nature of challenge
			-- e.g. "Digest"
			--RFO TODO make settable

	digest_challenge: STRING
			-- Roll-up of realm, domain, nonce, opaque, stale,
			-- algorithm, qop_options, and auth_param
		local
			ts: STRING
		do
			create ts.make (64)
			ts.extend ('"')
			ts.append (realm)
			ts.extend ('"')

			if domain /= Void then
				ts.append (",domain=%"")
				ts.append (domain)
				ts.extend ('"')
			end

			ts.append (",nonce=%"")
			ts.append (nonce)
			ts.extend ('"')

			if opaque /= Void then
				ts.append (",opaque=%"")
				ts.append (opaque)
				ts.extend ('"')
			end
			if include_stale then
				ts.append (",stale=&")
				ts.append (stale.out)
				ts.extend ('"')
			end
			if algorithm /= Void then
				ts.append (",algorithm=%"")
				ts.append (algorithm)
				ts.extend ('"')
			end
			if qop_options /= Void then
				ts.append (",qop=%"")
				ts.append (qop_values)
				ts.extend ('"')
			end
			Result := b64.encoded_string (ts, vOID)
		ensure
			exists: Result /= Void
		end

	realm: STRING
			-- Identification of sub-space to which authentication 
			-- applies.  Should contain at least the host name/address
			--
			-- In Cirrus, this can be "accounts@<cirrus_realm>" or
			-- "admins@<cirrus_realm>"
			-- The <accounts> and "/adm/" subtrees always require
			-- authentication
			-- The "/anon/" subtree never requires authentication and so
			-- is not relevant.
		do
			--RFO TODO
			Result := "users@cirrus"
		end

	domain: STRING
			-- A quoted, blank-separated list of URIs that define the
			-- space for which this authentication applies.
			-- URIs can be absolute (including references to other 
			-- servers) or relative to the canonical root URL of the
			-- server being accessed
			-- Has the form:  domain="<URI list>"
			-- Optional

	uri: STRING
	nonce: STRING
			-- Server-generated string, unique to each 401 challenge

	nonce_value: STRING
	stale: BOOLEAN
	algorithm: STRING
	qop_options: LINKED_LIST [STRING]

	qop_values: STRING
		local
			oc: CURSOR
		do
			create Result.make (32)
			if qop_options /= Void then
				oc := qop_options.cursor
				from qop_options.start
				until qop_options.exahusted
				loop
					Result.append (qop_options.item)
					qop_options.forth
					if not qop_options.exhausted then
						Result.extend (',')
					end
				end
				qop_options.go_to (oc)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	request_digest: STRING
			-- A string of 32 lower-case hex digits, to demonstrate that
			-- the client knows the password
		do
			if uses_qop then
				Result := request_digest_qop
			else
				Result := request_digest_no_qop
			end
		end

	--|--------------------------------------------------------------

	request_digest_qop: STRING
			-- A string of 32 lower-case hex digits, to demonstrate that
			-- the client knows the password
			-- This form is for use when qop is used
			--
			-- If qop is auth or auth-init then the syntax is:
			-- '"' < KD ( H(A1), unq(nonce-value) ':' nc-value ':'
			--                   unq(cnone-value) ':' H(A2) ) > '"'
			--
			-- Where KD is the key data derived from the enclosed values
			-- and H is the hash derived from its enclosed values
			-- A1 and  A2 are context-dependent values
		do
		end

	--|--------------------------------------------------------------

	request_digest_no_qop: STRING
			-- A string of 32 lower-case hex digits, to demonstrate that
			-- the client knows the password
			-- This form is for use when qop is NOT used
			--
			-- If qop is undefined then the syntax is:
			-- '"' < KD ( H(A1), unq(nonce-value) ':' H(A2) ) > '"'
			--
			-- Where KD is the key data derived from the enclosed values
			-- and H is the hash derived from its enclosed values
			-- A1 and  A2 are context-dependent values
		do
		end

	--|--------------------------------------------------------------

	rq_digest_a1: STRING
		do
			if algorithm = Void or else algorithm.is_equal ("MD5") then
				Result := name + ":" + realm + ":" + password
			elseif algorithm.is_equal ("MD5-sess") then
				Result := md5sum_as_hex (name + ":" + realm + ":" + password) + ":" + nonce + ":" + cnonce
			else
				--RFO TODO
				-- raise exception
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	rq_digest_a2: STRING
		do
			Result := method + ":" + digest_uri
			if qop /= Void and then qop.is_equal ("auth-int") then
				Result.extend (':')
				Result.append (hash (entity_body))
			end
		end

	--|--------------------------------------------------------------

	server_key: STRING
			-- Private key used by server to create nonce values
		do
			Result := private_server_key
			if Result.is_empty then
				Result := Ks_dflt_server_key
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_server_key (v: STRING)
		require
			valid: v /= Void and then not v.is_empty
		do
			private_server_key.wipe_out
			private_server_key.append (v)
		end

	--|--------------------------------------------------------------

	generate_nonce
		-- Generate a new nonce value
		local
			ts: STRING
		do
			create ts.make (32)
			ts.append (create {AEL_TIMESTAMP}.make_now)
			ts.extend (':')
			ts.append (server_private_key)
			nonce := b64.encoded_string (ts, Void)
		ensure
			nonce_exists: nonce /= Void and then not nonce.is_empty
			new_object: nonce /= old nonce
			new_value: not nonce.is_equal (old nonce)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_server_key: STRING
			-- Server key is shared by all instances of auth headers
		once
			create Result.make (8)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_dflt_server_key: STRING = "12345678"

	Ks_authenticate_header_tag: STRING = "WWW-Authenticate: :"

	--|--------------------------------------------------------------

	b64: AEL_STR_BASE64
		once
			create Result
		end

end -- class AEL_HTTP_AUTH_HEADER
