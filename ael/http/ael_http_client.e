note
	description: "An HTTP protocol client"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create an instance of this class using the make routine and passing it
as argument the  string to use for the USER_AGENT value.
Once made, object can be used to construct a variety of simple of 
complex HTTP requests.  The simplest method is to use the 'get'
routine, though more complex requests begin with call to 
prepare_request, followed by execute_current_request.
Responses are available in 'response' but the get routine also 
accepts a response buffer argument for compactness.
This class depends on other classes in the Amalasoft http cluster 
as well other Amalasoft library clusters, the Eiffel Net cluster
and the Eiffel Base libraries.  The Eiffel Net cluster requires
linking with an external library (see Net documentation)
}"

class AEL_HTTP_CLIENT

inherit
	AEL_HTTP_CONSTANTS
	AEL_SPRT_FALLIBLE

create
	make

 --|========================================================================
feature -- Creation
 --|========================================================================

	make (anm: STRING) 
			-- Create an HTTP client object with user agent string 'anm'
		require
			valid_name: anm /= Void and then not anm.is_empty
		do
			user_agent := anm.twin
			create transactions.make
		end

--|========================================================================
feature -- Values
--|========================================================================

	user_agent: STRING
			-- String identifying this HTTP Agent

 --|------------------------------------------------------------------------

	url: detachable HTTP_URL
			-- Full target URL
		do
			if current_request /= Void then
				Result := current_request.url
			end
		end

	current_request: detachable AEL_HTTP_REQUEST
			-- Request either under construction or most recently
			-- executed

	--|--------------------------------------------------------------

	response: STRING
			-- Response text from most recent request, if nay
		do
			if not transactions.is_empty then
				Result := transactions.last.response
			end
			if Result = Void then
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	method: INTEGER
			-- Request method (default is GET)
		do
			if current_request /= Void then
				Result := current_request.method
			end
		end

	method_tag: STRING
			-- String tag denoting HTTP method
		do
			Result := http_method_labels.item (method)
		end

 --|------------------------------------------------------------------------	

	is_prepared: BOOLEAN
			-- Is this client prepared to execute a transaction?
		do
			Result := current_request /= Void
		end

	is_get: BOOLEAN
		do
			Result := not (is_post or is_put or is_delete)
		end

	is_post: BOOLEAN
		do
			Result := method_tag.is_equal ("POST")
		end

	is_put: BOOLEAN
		do
			Result := method_tag.is_equal ("PUT")
		end

	is_delete: BOOLEAN
		do
			Result := method_tag.is_equal ("DELETE")
		end

	most_recent_transaction: detachable AEL_HTTP_TRANSACTION
			-- Most recent actual message exchange session, if any
		do
			if not transactions.is_empty then
				Result := transactions.last
			end
		ensure
			most_recent_if_any: (not transactions.is_empty)
				implies Result = transactions.last
		end

--|========================================================================
feature -- Value Setting
--|========================================================================

	set_user_agent (v: STRING)
			-- Set the name of the Agent to include in the headers
		require
			exists: v /= Void and then not v.is_empty
		do
			user_agent := v
		ensure
			assigned: user_agent = v
		end

 --|------------------------------------------------------------------------

	set_content (v: STRING)
			-- Set the content of the outbound message (for POST
			-- requests)
			-- This will clobber any existing content
		require
			prepared: current_request /= Void
			is_post_or_put: is_post or is_put
		do
			current_request.set_content (v)
		end

 --|------------------------------------------------------------------------

	append_content (v: STRING)
			-- Add to the content of the outbound message
		require
			prepared: current_request /= Void
			is_post_or_put: is_post or is_put
			has_content: current_request.content /= Void
		do
			current_request.append_content (v)
		end

	--|--------------------------------------------------------------

	set_path_translated (v: STRING)
		require
			prepared: current_request /= Void
		do
			current_request.set_path_translated (v)
		end

	set_content_location (v: STRING)
		require
			prepared: current_request /= Void
		do
			current_request.set_content_location (v)
		end

	--|--------------------------------------------------------------

	add_special_header (v: STRING)
		require
			prepared: current_request /= Void
		do
			current_request.add_special_header (v)
		end

	--|--------------------------------------------------------------

	set_user_name (v: STRING)
		require
			url_exists: url /= Void
		do
			url.set_username (v)
		end

	set_password (v: STRING)
		require
			url_exists: url /= Void
		do
			url.set_password (v)
		end

	--|--------------------------------------------------------------

	set_transaction_trace_proc (v: detachable like transaction_trace_proc)
		do
			transaction_trace_proc := v
		end

--|========================================================================
feature -- Execution
--|========================================================================

	get (ha, upath, rsp: STRING)
			-- Issue a simple get request of host 'ha' with
			-- host-relative path 'upath', via port 80
			-- Leave response in 'rsp'
		require
			valid_host: ha /= Void and then not ha.is_empty
			path_exists: upath /= Void
			response_exists: rsp /= Void
		do
			get_via_port (ha, upath, rsp, 80)
		end

	get_via_port (ha, upath, rsp: STRING; pn: INTEGER)
			-- Issue a simple get request of host 'ha' with
			-- host-relative path 'upath', via port number 'pn'
			-- Leave response in 'rsp'
		require
			valid_host: ha /= Void and then not ha.is_empty
			path_exists: upath /= Void
			valid_port_number: pn > 0
			response_exists: rsp /= Void
		local
			uri_path: STRING
		do
			uri_path := upath
			if not upath.is_empty and then upath.item (1) /= '/' then
				uri_path :=  "/" + upath
			end
			prepare_request (pn, ha, uri_path, "GET", "")
			execute_current_request
			rsp.wipe_out
			rsp.append (response)
		end

	--|--------------------------------------------------------------

	prepare_request (a_port: INTEGER; a_host, loc, mt, ct: STRING)
			-- Prepare a new request with the given values
		require
			valid_host: a_host /= Void and then not a_host.is_empty
			valid_method: is_valid_method_label (mt)
			valid_port: a_port >= 0
		local
			ts: STRING
			tu : like url
		do
			ts := a_host.twin
			if a_port /= 0 then
				ts.extend (':')
				ts.append (a_port.out)
			end
			if loc /= Void then
				ts.append (loc)
			end
			create tu.make ("http://"+ts)
			inspect method_label_to_value (mt)
			when K_htmethod_post then
				create {AEL_HTTP_POST_REQUEST}current_request.make (Current,tu)
				current_request.set_content (ct)
			when K_htmethod_put then
				create {AEL_HTTP_PUT_REQUEST}current_request.make (Current,tu)
				current_request.set_content (ct)
			when K_htmethod_delete then
				create {AEL_HTTP_DELETE_REQUEST}current_request.make (Current,tu)
			when K_htmethod_download then
				create {AEL_HTTP_DOWNLOAD_REQUEST}current_request.make (Current,tu)
			else
				create {AEL_HTTP_GET_REQUEST}current_request.make (Current,tu)
			end
		ensure
			have_request: current_request /= Void
			is_new: current_request /= old current_request
		end

 --|------------------------------------------------------------------------

	execute_current_request
			-- Execute an HTTP transaction per current settings
		require
			is_prepared: is_prepared
		local
			session: like most_recent_transaction
		do
			create session.make_with_request (current_request)
			transactions.extend (session)
			if session.is_prepared then
				-- RFO, we do this several times
				if current_request.assembled_string = Void then
					current_request.assemble
				end
				if transaction_trace_proc /= Void then
					transaction_trace_proc.call ([session])
				end
				session.execute
				if transaction_trace_proc /= Void then
					transaction_trace_proc.call ([session])
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	transactions: TWO_WAY_LIST [like most_recent_transaction]
			-- History or transactions executed by this client

	--|--------------------------------------------------------------

	transaction_trace_proc:
--		detachable PROCEDURE [TUPLE[like most_recent_transaction]]
		detachable PROCEDURE
			-- Procedure to call on each request

	--|--------------------------------------------------------------
invariant
	valid_agent: user_agent /= Void and then not user_agent.is_empty

end -- class AEL_HTTP_CLIENT
