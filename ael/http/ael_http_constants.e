note
	description: "Constants used byAEL_ HTTP_CLIENT and related classes"
	legal: "Copyright (c) 2008, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"

class AEL_HTTP_CONSTANTS

--|========================================================================
feature -- Constants
--|========================================================================

	K_dflt_http_port: INTEGER = 80
	K_dflt_https_port: INTEGER = 443

	K_htmethod_undefined: INTEGER = 0
	K_htmethod_get: INTEGER = 1
	K_htmethod_post: INTEGER = 2
	K_htmethod_put: INTEGER = 3
	K_htmethod_delete: INTEGER = 4
	K_htmethod_head: INTEGER = 5
	K_htmethod_download: INTEGER = 6
	
	K_htmethod_minimum: INTEGER = 1
	K_htmethod_maximum: INTEGER = 6

	Ks_htmethod_get: STRING = "GET"
	Ks_htmethod_post: STRING = "POST"
	Ks_htmethod_put: STRING = "PUT"
	Ks_htmethod_delete: STRING = "DELETE"
	Ks_htmethod_head: STRING = "HEAD"
	Ks_htmethod_download: STRING = "DOWNLOAD"

	Ks_ct_octet_stream: STRING = "application/octet-stream"
			-- Octet stream content-type header
	Ks_ct_multipart_form: STRING = "multipart/form-data"
			-- Starting chars of multipart form data content-type header
	Ks_ct_form_encoded: STRING = "application/x-www-form-urlencoded"
			-- Starting chars of form url-encoded data content-type header
	Ks_ct_xml_text: STRING = "text/xml"
			-- XML text content-type header
	Ks_ct_html_text: STRING = "text/html"
			-- HTML text content-type header
	Ks_ct_json_text: STRING = "text/json"
			-- JSON text content-type header
	Ks_ct_json_app: STRING = "application/json"
			-- JSON application content-type header
	Ks_ct_js_text: STRING = "text/javascript"
			-- Javascript text content-type header
	Ks_ct_js_app: STRING = "application/javascript"
			-- JavaScript application content-type header
	Ks_ct_plain_text: STRING = "text/plain"
			-- Plain text content-type header


 --|------------------------------------------------------------------------	

	Ks_http_version: STRING = "HTTP/1.0"
	Ks_http_host_header: STRING = "Host"
	Ks_http_authorization_header: STRING = "Authorization: "
	Ks_http_end_of_header_line: STRING = "%R%N"
	Ks_http_end_of_command: STRING = "%R%N%R%N";
	Ks_http_content_length: STRING = "Content-Length: "
	Ks_http_content_type: STRING = "Content-Type: "
	Ks_http_content_location: STRING = "Content-Location: "
	Ks_http_content_disposition: STRING = "Content-Disposition: "
	Ks_http_path_translated: STRING = "Path-Translated: "
	Ks_http_agent: STRING = "User-agent: "
	Ks_http_from: STRING = "From: "

 --|------------------------------------------------------------------------	

	Ks_hthdr_host: STRING = "Host"
	Ks_hthdr_authorization: STRING = "Authorization"
	Ks_hthdr_content_length: STRING = "Content-Length"
	Ks_hthdr_content_type: STRING = "Content-Type"
	Ks_hthdr_content_location: STRING = "Content-Location"
	Ks_hthdr_content_disposition: STRING = "Content-Disposition"
	Ks_hthdr_cache_control: STRING = "Cache-Control"
	Ks_hthdr_path_translated: STRING = "Path-Translated"
	Ks_hthdr_agent: STRING = "User-Agent"
	Ks_hthdr_referer: STRING = "Referer" -- Officially mispelled in std
	Ks_hthdr_location: STRING = "Location"
	Ks_hthdr_from: STRING = "From"
	Ks_hthdr_status: STRING = "Status"
	Ks_hthdr_multipart_tag_value_separator: STRING = "; "

	Ks_ht_status_ok: STRING = "200 OK"

 --|------------------------------------------------------------------------	

	K_ht_dflt_bufsize: INTEGER = 16384

 --|------------------------------------------------------------------------	

	http_method_labels: ARRAY [STRING]
			-- Tags denoting HTTP methods
			-- N.B. This has zero-based indexing!!!
		once
			create Result.make_filled (
				"undefined", K_htmethod_minimum, K_htmethod_maximum)
			Result.put (Ks_htmethod_get, K_htmethod_get)
			Result.put (Ks_htmethod_post, K_htmethod_post)
			Result.put (Ks_htmethod_put, K_htmethod_put)
			Result.put (Ks_htmethod_delete, K_htmethod_delete)
			Result.put (Ks_htmethod_head, K_htmethod_head)
			Result.put (Ks_htmethod_download, K_htmethod_download)
			Result.compare_objects
		ensure
			exists: Result /= Void
			valid_lower: Result.lower = K_htmethod_minimum
			valid_upper: Result.upper = K_htmethod_maximum
		end

	http_methods: HASH_TABLE [INTEGER, STRING_GENERAL]
		once
			create Result.make (5)
			Result.extend (K_htmethod_get, Ks_htmethod_get)
			Result.extend (K_htmethod_put, Ks_htmethod_put)
			Result.extend (K_htmethod_post, Ks_htmethod_post)
			Result.extend (K_htmethod_delete, Ks_htmethod_delete)
			Result.extend (K_htmethod_head, Ks_htmethod_head)
			Result.extend (K_htmethod_download, Ks_htmethod_download)
		ensure
			exists: Result /= Void
			filled: Result.count = 5
		end

 --|------------------------------------------------------------------------	

	method_label_to_value (v: STRING): INTEGER
			-- Numeric equivalent of given method tag
		require
			is_valid: is_valid_method_label (v)
		do
			Result := http_methods.item (v)
		end

	method_to_label (m: INTEGER): STRING
			-- Method label constant corresponding to method 'm'
		require
			valid: is_valid_method (m)
		do
			Result := http_method_labels.item (m)
		ensure
			valid: is_valid_method (m) implies is_valid_method_label (Result)
		end

 --|------------------------------------------------------------------------	

	is_valid_method (v: INTEGER): BOOLEAN
			-- Is the given method valid?
		do
			inspect v
			when K_htmethod_get, K_htmethod_put, K_htmethod_post,
				K_htmethod_delete, K_htmethod_head
			 then
				 Result := True
			else
			end
		ensure
			in_range: Result implies
				v >= K_htmethod_minimum and v <= K_htmethod_maximum
		end

 --|------------------------------------------------------------------------	

	is_valid_method_label (v: detachable STRING): BOOLEAN
			-- Does given string denote a valid method?
		do
			if v /= Void and then not v.is_empty then
				Result := is_valid_method (http_methods.item (v))
			end
		end

	is_valid_multipart_content_type (ct: detachable STRING): BOOLEAN
			-- Is the given string a valid multipart content type?
		local
			ts: STRING
		do
			if ct /= Void then
				ts := Ks_ct_multipart_form
				if ct.starts_with (ts) and ct.count > ts.count then
					Result := ct.substring_index ("; boundary=", 1) /= 0
				end
			end
		end

 --|========================================================================
feature {NONE} -- Encoder Implementation
 --|========================================================================

	base64_encoded (s: STRING): STRING
			-- base64 encoded value of `s'.
		require else
			exists: s /= Void
		local
			abr: AEL_SPRT_BASE64
			tc: CELL [INTEGER]
		do
			create abr
			create tc.put (0)
			Result := abr.encoded_string (s, tc)
		end

end -- class AEL_HTTP_CONSTANTS
