note
	description: "Encapsulation of an HTTP content disposition header"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create an instance of this class either empty (using default_create), or
with a value using make_with_value. If created without a value, call
set_value after creation.  Upon value setting, either explicitly or as
a result of creating with a value, the value is decomposed into fields.
}"

class AEL_HTTP_CONTENT_DISPOSITION_HEADER

inherit
	AEL_SPRT_MULTIPART_PART_HEADER
		redefine
			default_create, make_from_stream, set_value
		end

create
	default_create, make_with_value

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	default_create
			-- Create and initialize Current.
		do
			Precursor
			tag.append ("Content-Disposition")
			create parameters.make
		end

	--|--------------------------------------------------------------

	make_with_value (v: STRING)
			-- Create and initialize Current from the stream 'v'
		require
			exists: v /= Void
		do
			default_create
			set_value (v)
		ensure
			assigned: value = v
		end

	make_from_stream (v: STRING; spos: INTEGER)
			-- Create an instance from the given stream starting at spos and through the end of line
		local
			i, lim: INTEGER
			ts: STRING
		do
			default_create
			ts := tag_from_stream (v, spos)
			if ts /= Void then
				tag := ts
				lim := v.count.min (spos + max_header_length)
				-- Skip over whitespace
				from
					i := spos + ts.count + 1
				until
					i > lim or else not v.item (i).is_space
				loop
					i := i + 1
				end
				-- Extract value, if any
				from
				until
					i > lim or else v.item (i) = '%R'
				loop
					value.extend (v.item (i))
					i := i + 1
				end
			end
			last_parse_position := spos.max (i - 1)
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	parameters: AEL_SPRT_TV_PAIR_LIST

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_value (v: STRING)
			-- From given value, blah
		do
			Precursor (v)
			decompose_value
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	decompose_value
			--
		do
			if is_form_data then
				decompose_form_data
			end
		end

	--|--------------------------------------------------------------

	decompose_form_data
			--
		require
			is_form_data: is_form_data
		local
			spos, epos, i, lim: INTEGER
			tvp: AEL_SPRT_TV_PAIR
		do
			lim := value.count
			spos := Ks_form_data.count + 1
			from
				i := spos
			until
				i > lim
			loop
				if value.item (i) = ';' or i = lim then
					if i = lim then
						epos := i
					else
						epos := i - 1
					end
					create tvp.make_from_string_unquote (value.substring (spos, epos))
					parameters.extend (tvp)
					i := i + 1
					spos := i + 1
				end
				i := i + 1
--				spos := i
			end
		end

	--|--------------------------------------------------------------

	is_form_data: BOOLEAN
		do
			Result := value.starts_with (Ks_form_data)
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	Ks_form_data: STRING = "form-data; "

	--|--------------------------------------------------------------
invariant
	params_exist: parameters /= Void

end -- class AEL_HTTP_CONTENT_DISPOSITION_HEADER
