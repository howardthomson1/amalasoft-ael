note
	description: "An HTTP GET request specifically for file download"
	legal: "Copyright (c) 2008, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"

class AEL_HTTP_DOWNLOAD_REQUEST

inherit
	AEL_HTTP_GET_REQUEST
		redefine
			assemble
		end

create
	make
	
--|========================================================================
feature -- Assembly
--|========================================================================
	
	assemble
			-- Build full string representation, for transfer
			-- Leave result in assembled_string
		local
			tstr, ls, ps: STRING
		do
			create tstr.make (content_length + 1024)
			if url.is_proxy_used then
				ls := url.location
			else
				ls := "/" + url.path
			end
			if url.port /= url.default_port then
				ps := ":" + url.port.out
			else
				ps := ""
			end
			
			tstr.append (to_header_line(method_tag+" "+ls+" "+Ks_http_version))

			tstr.append (Ks_http_host_header)
			tstr.append (": ")
			tstr.append (url.host)
			tstr.append (ps)

			if not url.username.is_empty then
				tstr.append (Ks_http_end_of_header_line)
				tstr.append (Ks_http_authorization_header)
				tstr.append( ": Basic ")
				tstr.append (base64_encoded (url.username + ":" + url.password))
			end
			tstr.append (Ks_http_end_of_header_line)

			tstr.append (to_header_line(Ks_http_agent + client.user_agent))
			
			tstr.append (to_header_line(Ks_http_from+content_location))

			tstr.append (Ks_http_end_of_header_line)
			
			assembled_string := tstr
		end
	
end -- class AEL_HTTP_DOWNLOAD_REQUEST
