note
	description: "An HTTP GET request"
	legal: "Copyright (c) 2008, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"

class AEL_HTTP_GET_REQUEST

inherit
	AEL_HTTP_REQUEST

create
	make

 --|========================================================================
feature -- Status
 --|========================================================================

	method_tag: STRING = "GET"

--|========================================================================
feature -- Assembly
--|========================================================================

	assemble
			-- Build full string representation, for transfer
			-- Leave result in assembled_string
		local
			tstr, ls: STRING
		do
			create tstr.make (content_length + 1024)
			if url.is_proxy_used then
				ls := url.location
			else
				ls := "/" + url.path
			end

			tstr.append (to_header_line(method_tag+" "+ls+" "+Ks_http_version))

			tstr.append (host_header)


--			if not url.username.is_empty then
				tstr.append (auth_header)
--			end

				tstr.append (to_header_line(Ks_http_agent + client.user_agent))
			if path_translated /= Void and then not path_translated.is_empty then
				tstr.append (to_header_line(Ks_http_path_translated+path_translated))
			end
				tstr.append (special_headers_assembled)
			tstr.append (Ks_http_end_of_header_line)
			assembled_string := tstr
		end

end -- class AEL_HTTP_GET_REQUEST
