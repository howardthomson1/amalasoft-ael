note
	description: "Multi-threaded implementation of an HTTP protocol client"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create an instance of this class using the make routine and passing it
as argument the  string to use for the USER_AGENT value.
Once made, object can be used to construct a variety of simple of 
complex HTTP requests.  The simplest method is to use the 'get'
routine, though more complex requests begin with call to 
prepare_request, followed by execute_current_request.
Responses are available in 'response' but the get routine also 
accepts a response buffer argument for compactness.
This class depends on other classes in the Amalasoft http cluster 
as well other Amalasoft library clusters, the Eiffel Net cluster
and the Eiffel Base libraries.  The Eiffel Net cluster requires
linking with an external library (see Net documentation)
}"

class AEL_HTTP_MT_CLIENT

inherit
	AEL_HTTP_CLIENT
		rename
			execute_current_request as execute_pending_request
		export
			{NONE} execute_pending_request
		redefine
			make
		end
	THREAD
		rename
			make as thread_make
		end

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (anm: STRING)
		do
			thread_make
			Precursor {AEL_HTTP_CLIENT} (anm)
		end

--|========================================================================
feature -- Notification
--|========================================================================

	notify_on_end
		do
			if notify_on_end_proc /= Void then
				notify_on_end_proc.call ([Current])
			end
		end

	--|--------------------------------------------------------------

	notify_on_end_proc: PROCEDURE [TUPLE [like Current]]

	--|--------------------------------------------------------------

	set_notify_on_end_proc (v: like notify_on_end_proc)
		do
			notify_on_end_proc := v
		end

--|========================================================================
feature -- Execution
--|========================================================================

	execute_current_request
			-- Execute an HTTP transaction per current settings
		require
			is_prepared: is_prepared
		do
			launch
		end
	
--|========================================================================
feature {THREAD_CONTROL} -- Thread execution
--|========================================================================
	
	execute
		do
			execute_pending_request
			notify_on_end
		end

end -- class AEL_HTTP_MT_CLIENT
