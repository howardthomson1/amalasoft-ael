note
	description: "An HTTP PUT request"
	legal: "Copyright (c) 2008, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"

class AEL_HTTP_PUT_REQUEST

inherit
	AEL_HTTP_REQUEST
		redefine
			assembled_header
		end

create
	make
	
 --|========================================================================
feature -- Status
 --|========================================================================
	
	method_tag: STRING = "PUT"
	
 --|------------------------------------------------------------------------

	assembled_header: STRING
			-- Fully assembled header (less content, if any)
			-- Can be Void or out of synch unless "assemble" is called

--|========================================================================
feature -- Assembly
--|========================================================================
	
	assemble
			-- Build full string representation, for transfer
			-- Leave result in assembled_string
		local
			tstr, ls: STRING
		do
			create tstr.make (content_length + 1024)
			if url.is_proxy_used then
				ls := url.location
			else
				ls := "/" + url.path
			end
			
			tstr.append (to_header_line(method_tag+" "+ls+" "+Ks_http_version))
			tstr.append (host_header)
			
			-- RFO TODO
--			if not url.username.is_empty then
				tstr.append (auth_header)
--			end

			tstr.append (to_header_line(Ks_http_agent + client.user_agent))
			tstr.append (to_header_line(Ks_http_content_type+"application/http"))
			tstr.append (to_header_line(Ks_http_content_length+content_length.out))
			if path_translated /= Void then
				tstr.append (to_header_line(Ks_http_path_translated+path_translated))
			end
			tstr.append (Ks_http_end_of_header_line)
			assembled_header := tstr.twin

			if content_length > 0 then
				tstr.append (content)
			end
			assembled_string := tstr
		ensure then
			has_length: assembled_string.substring_index (Ks_http_content_length,1) /= 0
		end
	
end -- class AEL_HTTP_PUT_REQUEST
