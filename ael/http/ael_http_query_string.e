note
	description: "Encapsulation of CGI/FASTCFGI QUERY_STRING syntax"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create and instance of this class by calling the make_from_string routine
with the full text of the query string as argument.  Creating the object
triggers decomposition which then leaves the query arguments in arg_pairs.
To find a specific value by its tag, call value_for_arg.
}"

class AEL_HTTP_QUERY_STRING

inherit
	STRING
		export
-- RFO TODO, need to close up exports
			{AEL_HTTP_QUERY_STRING} all
			{ANY} is_empty, out
		redefine
			make, make_from_string
		end

create
	make_from_string

create {STRING}
	make

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_from_string (v: STRING)
		do
			create arg_pairs.make
			Precursor (v)
			decompose
		end;

	--|--------------------------------------------------------------

	make (n: INTEGER)
			-- Allocate space for at least `n' characters.
		do
			create arg_pairs.make
			Precursor (n)
		end

--|========================================================================
feature {NONE} -- Analysis
--|========================================================================

	decompose
		local
			tv: AEL_SPRT_TV_PAIR
			raw_pairs: LIST [STRING]
			pos: INTEGER
			ti, tt, ts, tval, ttag: STRING
		do
			raw_pairs := split ('&')
			from raw_pairs.start
			until raw_pairs.exhausted
			loop
				create tv.make
				ti := raw_pairs.item
				if not tv.is_tv_pair (ti) then
					-- ERROR
				else
					pos := ti.index_of ('=',1)
					tt := ti.substring (1, pos - 1)
					ttag := i18n.string_url_decoded (tt)
					tv.set_tag (ttag)
					if pos < ti.count then
						ts := ti.substring (pos + 1, ti.count)
						tval := i18n.string_url_decoded (ts)
						tv.set_value (asr.quotes_stripped (tval))
						arg_pairs.extend (tv)
					end
				end
				raw_pairs.forth
			end
		end

--|========================================================================
feature -- Queries
--|========================================================================

	arg_pairs: AEL_SPRT_TV_PAIR_LIST

 --|------------------------------------------------------------------------

	html_out: STRING
		do
			Result := arg_pairs.decorated_out ("", "<br>%R%N")
		end

 --|------------------------------------------------------------------------

	value_for_arg (v: STRING): STRING
			-- Value for given argument, if any
		do
			Result := arg_pairs.value_for_tag (v)
		end

	--|--------------------------------------------------------------

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	--|--------------------------------------------------------------

	i18n: AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_HTTP_QUERY_STRING
