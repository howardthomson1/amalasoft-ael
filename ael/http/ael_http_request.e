note
	description: "An HTTP protocol request"
	legal: "Copyright (c) 2010, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"
	howto: "{
This is a deferred class an cannot be instantiated.  It is the 
ancestor of specialized subclasses like HTTP_GET_REQUEST also in 
the Amalasoft http cluster.
}"

deferred class AEL_HTTP_REQUEST

inherit
	AEL_HTTP_CONSTANTS
		redefine
			default_create
		end

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (cl: like client; u: like url)
		require
			client_exists: cl /= Void
			valid_url: u /= Void
		do
			default_create
			client := cl
			url := u
		end

	--|--------------------------------------------------------------

	default_create
		do
			method := method_label_to_value (method_tag)
			create content_list.make
			create special_headers.make
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	content_list: LINKED_LIST [STRING]
			-- List of zero or more outgoing content strings
			-- Built as a list to allow appending additional content
			-- without the need for excessive string cloning
			-- Items are concatenated on 'content' query

 --|------------------------------------------------------------------------

	content: STRING
			-- Outbound request content
		do
			if not content_list.is_empty then
				create Result.make (content_list.first.count * content_list.count)
				from content_list.start
				until content_list.exhausted
				loop
					Result.append (content_list.item)
					content_list.forth
				end
			end
		ensure
			exists_if_exists: (not content_list.is_empty)
				implies Result /= Void
		end

 --|------------------------------------------------------------------------	

	content_length: INTEGER
			-- Length, in bytes, of content section
		do
			if not content_list.is_empty then
				Result := content.count
			end
		end

	has_content: BOOLEAN
			-- Does the current request (if any) have any content?
		do
			Result := not content_list.is_empty
		end

 --|------------------------------------------------------------------------

	client: AEL_HTTP_CLIENT
			-- The HTTP client initiating this request

	url: HTTP_URL
			-- URL of target

	method_tag: STRING
		-- Tag representing the HTTP method
		deferred
		end

	method: INTEGER

 --|------------------------------------------------------------------------

	assembled_string: STRING
			-- Fully assembled string representation, for transfer
			-- Can be Void or out of synch unless "assemble" is called

 --|------------------------------------------------------------------------

	assembled_header: STRING
			-- Fully assembled header (less content, if any)
			-- Can be Void or out of synch unless "assemble" is called
		do
			Result := assembled_string
		end

	special_headers_assembled: STRING
			-- Any special/custom headers assembled into a single string
			-- with crlf at end
			do
				create Result.make (special_headers.count * 32)
				from special_headers.start
				until special_headers.exhausted
				loop
					Result.append (special_headers.item)
					Result.append (Ks_http_end_of_header_line)
					special_headers.forth
				end
			ensure
				exists: Result /= Void
			end

	--|--------------------------------------------------------------

	path_translated: STRING
	content_location: STRING

	special_headers: LINKED_LIST [STRING]

--|========================================================================
feature -- URL values
--|========================================================================

	host: STRING
		do
			Result := url.host
		end

	location: STRING
		do
			Result := url.location
		end

	port: INTEGER
		do
			Result := url.port
		end

	uri: STRING
		local
			ts: STRING
		do
			create Result.make (64)

			if url.is_proxy_used then
				ts := url.location
			else
				ts := "/" + url.path
			end
			Result := ts
--RFO			Result.append (to_header_line(method_tag+" "+ts+" "+Ks_http_version))
		ensure
			exists: Result /= Void
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_path_translated (v: STRING)
		require
			exists: v /= Void
		do
			path_translated := v
		ensure
			assigned: path_translated = v
		end

	--|--------------------------------------------------------------

	set_content_location (v: STRING)
		require
			exists: v /= Void
		do
			content_location := v
		ensure
			assigned: content_location = v
		end

	add_special_header (v: STRING)
		require
			exists: v /= Void and then v.index_of(':',1) /= 0
		do
			special_headers.extend (v)
		ensure
			added: special_headers.count = old special_headers.count + 1
			added_arg: special_headers.last = v
		end

	--|--------------------------------------------------------------

	set_content (v: STRING)
			-- Set outbound request content
		do
			content_list.wipe_out
			if v /= Void then
				content_list.extend (v)
			end
		end

 --|------------------------------------------------------------------------	

	append_content (v: STRING)
			-- Add to the outbound request content
		require
			new_content_exists: v /= Void
			has_content: not content_list.is_empty
		do
			content_list.extend (v)
		end

	--|--------------------------------------------------------------

	set_auth_header_assembly_proc (v: like auth_header_assembly_proc)
		do
			auth_header_assembly_proc := v
		end

	--|--------------------------------------------------------------

	set_host_assembly_proc (v: like host_assembly_proc)
		do
			host_assembly_proc := v
		end

--|========================================================================
feature -- Assembly
--|========================================================================

	assemble
			-- Build full string representation, for transfer
			-- Leave result in assembled_string
		deferred
		ensure
			assembled: assembled_string /= Void
		end

	--|--------------------------------------------------------------

	auth_header: STRING
			-- Assembled authorization header based on current values
		do
			create Result.make (128)
			if attached auth_header_assembly_proc as ahap then
				ahap.call ([url.username, url.password])
				Result.append_string (ahap.last_result)
			else
				Result.append (Ks_http_authorization_header)
				Result.append ("Basic ")
				Result.append (base64_encoded (url.username))
				Result.extend (':')
				Result.append (url.password)
				Result.append (Ks_http_end_of_header_line)
			end
		end

	--|--------------------------------------------------------------

	host_header: STRING
			-- Assembled Host header based on current values
		local
			ps: STRING
			hst: STRING
		do
			create Result.make (128)
			hst := url.host
			if attached host_assembly_proc as hap then
				hap.call ([url])
				if attached {like host_header} hap.last_result as hlr then
					hst := hlr
				end
			end
			if url.port /= url.default_port then
				ps := ":" + url.port.out
			else
				ps := ""
			end

			Result.append (Ks_http_host_header)
			Result.append (": ")
			Result.append (hst)
			Result.append (ps)
			Result.append (Ks_http_end_of_header_line)
		end

--|========================================================================
feature -- Support routines
--|========================================================================

	to_header_line (v: STRING): STRING
			-- Add the leader/trailer characters necessary make the
			-- given string a proper header line
		require
			exists: v /= Void
		do
			Result := v + Ks_http_end_of_header_line
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

--	auth_header_assembly_proc: FUNCTION [STRING, STRING, STRING]
	auth_header_assembly_proc: FUNCTION [STRING, STRING]
			-- Function to construct non-basic auth header

	host_assembly_proc: FUNCTION [HTTP_URL, STRING]
			-- Function to construct the host string for the Host header

	--|--------------------------------------------------------------
invariant
	has_url: url /= Void

end -- class AEL_HTTP_REQUEST
