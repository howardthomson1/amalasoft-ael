class AEL_HTTP_REQUEST_FORMATS

--|========================================================================
feature -- Constants
--|========================================================================

	K_st_auth_untried: INTEGER = 0
	K_st_auth_passed: INTEGER = 1
	K_st_auth_failed: INTEGER = 2

	K_rqfmt_xml: INTEGER = 0
	K_rqfmt_html: INTEGER = 1
	K_rqfmt_json: INTEGER = 2

end -- class AEL_HTTP_REQUEST_FORMATS
