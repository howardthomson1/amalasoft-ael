note
	description: "A Resource applicable to HTTP protocol transfers."
	legal: "Copyright (c) 2010, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"
	howto: "{
Create and instance of this class by using the 'make' routine.
This class is typically used as a supplier to AEL_HTTP_TRANSACTION
}"

class AEL_HTTP_RESOURCE

inherit
	HTTP_PROTOCOL
		rename
			open as protocol_open
		undefine
			base64_encoded
		redefine
			address, initiate_transfer, get_content_length, check_error
		end
	AEL_HTTP_CONSTANTS
		undefine
			is_equal
		end

create
	make

 --|========================================================================
feature -- Status report
 --|========================================================================
	
	current_request: AEL_HTTP_REQUEST
			-- Request about to ge or just executed

	address: HTTP_URL

	status_code: INTEGER
			-- Actual code returned

	has_error: BOOLEAN
			-- Has an error occurred?
		do
			Result := error_code /= 0
		end

 --|------------------------------------------------------------------------

	first_header: STRING
			-- First result header, if any
		do
			if headers /= Void and then not headers.is_empty then
				Result := headers.first
			end
			if Result = Void then
				Result := ""
			end;
		ensure
			exists: Result /= Void
		end

 --|------------------------------------------------------------------------

	header_list: LINKED_LIST [STRING]
			--- List headers
		do
			create Result.make
			if headers /= Void then
				Result.fill (headers)
				from Result.start
				until Result.exhausted
				loop
					Result.item.prune_all_trailing ('%R')
					Result.forth
				end
				Result.start
			end
		end

	all_headers: STRING
			-- Result headers, if any
		do
			if headers = Void or else headers.is_empty then
				Result := ""
			else
				create Result.make (1024)
				from headers.start
				until headers.exhausted
				loop
					Result.append (headers.item)
					Result.prune_all_trailing ('%R')
					Result.extend ('%N')
					headers.forth
				end
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Transfer execution
--|========================================================================

	open
		local
			retrying: BOOLEAN
		do
			if not retrying then
				protocol_open
			end
		rescue
			retrying := True
			error_code := Connection_refused
			retry
		end

	initiate_transfer
			-- Initiate transfer.
		do
			execute_current_request
		end

--|========================================================================
feature {NONE} -- Execute implementation
--|========================================================================

	execute_current_request
		require
			request_exists: current_request /= Void
		do
			if not error then
				main_socket.put_string (current_request.assembled_string)
					debug ("http")
						Io.error.put_string (current_request.assembled_string)
					end
				get_headers
				transfer_initiated := True
				is_packet_pending := True
			end
		rescue
			error_code := Transfer_failed
		end

--|========================================================================
feature {AEL_HTTP_TRANSACTION} -- Execution
--|========================================================================

	execute_request (rq: AEL_HTTP_REQUEST)
		require
			exists: rq /= Void
			assembled: rq.assembled_string /= Void
		do
			current_request := rq
			execute_current_request
		end
	
--|========================================================================
feature {NONE} -- From HTTP_PROTOCOL
--|========================================================================
	
	get_content_length
			-- Get content length from HTTP headers
		local
			str: STRING
			pos: INTEGER
		do
			from
				headers.start
			until
				headers.after or else
					headers.item.substring_index ("Content-Length:", 1) > 0
			loop
				headers.forth
			end
			if not headers.after then
				str := headers.item.twin
				pos := str.index_of (' ', 1)
				str.remove_head (pos)
					-- Remove trailing and heading white spaces.
				str.right_adjust
				str.left_adjust
				if str.is_integer then
					content_length := str.to_integer
					is_count_valid := True
				else
					content_length := 0
					is_count_valid := False
				end
			else
				content_length := 0
				is_count_valid := False
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	check_error
			-- Check for error.
		local
			hs, ts: STRING
			st: INTEGER
		do
			if is_open and headers.count > 0 then
				from
					headers.start
				until
					(st /= 0) or headers.after
				loop
					hs := headers.item
					if hs.starts_with ("HTTP/1") then
						-- First header
						ts := hs.substring (10, 12)
						if not ts.is_integer then
							-- ERROR, we can't parse the header
						else
							st := ts.to_integer
						end
					elseif hs.starts_with ("Status: ") then
						ts := hs.substring (9, hs.index_of (' ', 9) - 1)
						if ts.is_integer then
							st := ts.to_integer
						end
					end
					headers.forth
				end
			end
			status_code := st
			if st >= 400 then
				error_code := ts.to_integer
			end
		end

end -- class AEL_HTTP_RESOURCE
