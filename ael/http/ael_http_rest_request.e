deferred class AEL_HTTP_REST_REQUEST

inherit
	AEL_SPRT_STRING_ROUTINES

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
		end

	--|--------------------------------------------------------------

	make_from_stream (v: STRING)
		require
			exists: v /= Void
		do
			make
			init_from_stream (v)
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	init_from_stream (v: STRING)
		require
			exists: v /= Void
		deferred
		end

	--|--------------------------------------------------------------

	get_content_length (v: STRING; spos: INTEGER)
			-- Extract the content-length header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		local
			ts: STRING
		do
			ts := header_from_stream (
				K_rexp_content_length_hdr, v, spos, 16, False)
			if not ts.is_integer then
				-- ERROR
			else
				content_length := ts.to_integer
			end
		end

	--|--------------------------------------------------------------

	get_content_type (v: STRING; spos: INTEGER)
			-- Extract the content-type header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			-- Content-type: count = 13
			content_type := header_from_stream (K_rexp_content_type_hdr,
			v, spos, 13, False)
		end

	--|--------------------------------------------------------------

	get_cache_control (v: STRING; spos: INTEGER)
			-- Extract the Cache-Control header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			-- Cache-Control: count = 14
			cache_control_arg := header_from_stream (K_rexp_cache_control_hdr,
			v, spos, 14, True)
		end

	--|--------------------------------------------------------------

	get_host (v: STRING; spos: INTEGER)
			-- Extract the host header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			-- Host: count = 5
			host := header_from_stream (K_rexp_host_hdr, v, spos, 5, False)
		end

	--|--------------------------------------------------------------

	get_authorization (v: STRING; spos: INTEGER)
			-- Extract the host header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			-- Authorization: count = 14
			authorization := header_from_stream (
				K_rexp_auth_hdr, v, spos, 14, True)
		end

	--|--------------------------------------------------------------

	get_date (v: STRING; spos: INTEGER)
			-- Extract the date header from the given stream
    require
		 exists: v /= Void
		 valid_start: spos > 0 and spos < v.count
		do
			-- Date: count = 5
			date := header_from_stream (K_rexp_date_hdr, v, spos, 5, True)
		end

	--|--------------------------------------------------------------

	get_expiration (v: STRING; spos: INTEGER)
			-- Extract the Expires header from the given stream
		require
      exists: v /= Void
      valid_start: spos > 0 and spos < v.count
		do
			-- Expires: count = 8
			expiration := header_from_stream (K_rexp_expire_hdr, v, spos, 8, True)
		end

	--|--------------------------------------------------------------

	get_meta (v: STRING; spos: INTEGER)
			-- Extract the x-amz-meta header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			meta_tag := header_from_stream (
				K_rexp_amz_meta_hdr, v, spos, -1, False)
		end

	--|--------------------------------------------------------------

	get_acl (v: STRING; spos: INTEGER)
			-- Extract the x-amz-acl header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			acl_tag := header_from_stream (K_rexp_amz_acl_hdr, v, spos, -1, False)
		end

	--|--------------------------------------------------------------

	get_etag (v: STRING; spos: INTEGER)
			-- Extract the Etag: header from the given stream
		require
			exists: v /= Void
			valid_start: spos > 0 and spos < v.count
		do
			-- Etag: count = 5
			e_tag := header_from_stream (K_rexp_etag_hdr, v, spos, 5, False)
		end

	--|--------------------------------------------------------------

	header_from_stream (
		rexp: SEARCHER; v: STRING; start_pos,
		hlen: INTEGER; ews: BOOLEAN): STRING
			-- Extract the given header from the given stream
		require
			expression_exists: rexp /= Void
			stream_exists: v /= Void
			valid_start: start_pos > 0 and start_pos < v.count
		local
			spos, epos, hl: INTEGER
		do
			rexp.search_forward (v, start_pos, v.count)
			if not rexp.found then
				-- ERROR
			else
				if hlen > 0 then
					hl := hlen
				else
					hl := v.index_of (':',rexp.start_position) + 1
				end
				spos := index_of_next_non_whitespace (
					v, rexp.start_position + hlen, True)
				if ews then
					epos := v.index_of ('%N', spos)
				else
					epos := index_of_next_whitespace (v, spos, True)
				end
				Result := substring_trimmed (v, spos, epos)
			end
		end

--|========================================================================
feature -- Values
--|========================================================================

	is_write_required: BOOLEAN
			-- Is WRITE access required for this request?
		deferred
		end

	content_length: INTEGER
	content_type: STRING
	content_disposition: STRING
	host: STRING
	authorization: STRING
	cache_control_arg: STRING
	date: STRING
	expiration: STRING
	meta_tag: STRING
	acl_tag: STRING
	e_tag: STRING

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_rexp_put_line: REGEXP
		once
			create Result.make_sensitive ("{
PUT[ \t]+.*HTTP/1.1\n
}")
    end

	 K_rexp_content_length_hdr: REGEXP
		once
			create Result.make_sensitive ("{
Content-Length:[ \t]*[0-9]+\n
}")
		end

	K_rexp_content_type_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Content-Type:")
		end

	K_rexp_content_disposition_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Content-Disposition:")
		end

	K_rexp_host_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Host:")
		end

	K_rexp_auth_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Authorization:")
		end

	K_rexp_cache_control_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Cache-Control:")
		end

	K_rexp_date_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Date:")
		end

	K_rexp_expire_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Expires:")
		end

	K_rexp_etag_hdr: SUBSTRING
		once
			create Result.make_sensitive ("Etag:")
		end

	K_rexp_amz_meta_hdr: REGEXP
		once
			create Result.make_sensitive ("x-amz-meta-[a-zA-Z0-9]+:")
		end

	K_rexp_amz_acl_hdr: REGEXP
		once
			create Result.make_sensitive ("x-amz-acl-[a-zA-Z0-9]+:")
		end

	K_rexp_header_end: REGEXP
			-- Header is done when a blank line is found
		once
			create Result.make ("{
\n\n
}")
		end

end -- class AEL_HTTP_REST_REQUEST
