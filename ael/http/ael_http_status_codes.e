note
	description: "Status code constants pertaining to the HTTP protocol"
	legal: "Copyright (c) 2010, Amalasoft Corporation"
	license: "Eiffel Forum License Version 2."
	status: "See notice at end of class."
	source: "Amalasoft Corporation"
	date: "$Date: 2008-04-22 $"
	revision: "$Revision: 001 $"
	howto: "{
Inherit as needed or instantiate using default create
}"

class  AEL_HTTP_STATUS_CODES

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_http_status_code (v: INTEGER): BOOLEAN
			-- Is the given value a valid http status code?
		do
			Result := v >= K_htsc_continue and v <= K_htsc_not_extended
		end


--|========================================================================
feature -- HTTP Status Codes
--|========================================================================

	K_htsc_continue: INTEGER = 100
	K_htsc_switching_protocols: INTEGER = 101
	K_htsc_processing_wd: INTEGER = 102

	K_htsc_ok: INTEGER = 200
	K_htsc_created: INTEGER = 201
	K_htsc_accepted: INTEGER = 202
	K_htsc_nonauthoritative_info: INTEGER = 203
	K_htsc_no_content: INTEGER = 204
	K_htsc_reset_content: INTEGER = 205
	K_htsc_partial_content: INTEGER = 206
	K_htsc_multistatus_wd: INTEGER = 207

	K_htsc_multiple_choices: INTEGER = 300
	K_htsc_moved_permanently: INTEGER = 301
	K_htsc_found: INTEGER = 302
	K_htsc_see_other: INTEGER = 303
	K_htsc_not_modified: INTEGER = 304
	K_htsc_use_proxy: INTEGER = 305
	K_htsc_switch_proxy: INTEGER = 306
	K_htsc_temp_redirect: INTEGER = 307

	K_htsc_bad_request: INTEGER = 400
	K_htsc_unauthorized: INTEGER = 401
	K_htsc_payment_required: INTEGER = 402
	K_htsc_forbidden: INTEGER = 403
	K_htsc_not_found: INTEGER = 404
	K_htsc_method_not_allowed: INTEGER = 405
	K_htsc_not_acceptable: INTEGER = 406
	K_htsc_proxy_auth_required: INTEGER = 407
	K_htsc_request_timeout: INTEGER = 408
	K_htsc_conflict: INTEGER = 409
	K_htsc_gone: INTEGER = 410
	K_htsc_length_required: INTEGER = 411
	K_htsc_precondition_failed: INTEGER = 412
	K_htsc_req_entity_too_large: INTEGER = 413
	K_htsc_req_uri_too_long: INTEGER = 414
	K_htsc_unsupported_media_type: INTEGER = 415
	K_htsc_req_range_not_satisfiable: INTEGER = 416
	K_htsc_expectation_failed: INTEGER = 417
	K_htsc_teapot: INTEGER = 418

	-- WebDAV errors
	K_htsc_too_many_connections: INTEGER = 421
	K_htsc_unprocessable_entity: INTEGER = 422
	K_htsc_locked: INTEGER = 423
	K_htsc_failed_dependency: INTEGER = 424
	K_htsc_unordered_collection: INTEGER = 425
	K_htsc_upgrade_required: INTEGER = 426

	K_htsc_retry_with: INTEGER = 449
	K_htsc_blocked_parental: INTEGER = 450

	K_htsc_internal_server_error: INTEGER = 500
	K_htsc_not_implemented: INTEGER = 501
	K_htsc_bad_gateway: INTEGER = 502
	K_htsc_service_unavailable: INTEGER = 503
	K_htsc_gateway_timeout: INTEGER = 504
	K_htsc_http_version_not_supported: INTEGER = 505
	K_htsc_variant_also_negotiates: INTEGER = 506
	K_htsc_insufficient_storage_wd: INTEGER = 507

	K_htsc_bandwidth_exceeded: INTEGER = 509
	K_htsc_not_extended: INTEGER = 510
	K_htsc_user_access_denied: INTEGER = 530

--|========================================================================
feature -- Status messages
--|========================================================================

	http_status_code_message (v: INTEGER): STRING
		do
			Result := ""
			if attached http_status_code_messages.item (v) as lm then
				Result := lm
			end
		end

	http_status_code_messages: HASH_TABLE [STRING, INTEGER]
			-- Header messages correspionding to HTTP status codes
		once
			create Result.make (59)
			Result.extend ("Continue", K_htsc_continue)
			Result.extend ("Switching Protocols", K_htsc_switching_protocols)
			Result.extend ("Processing", K_htsc_processing_wd)
			Result.extend ("OK", K_htsc_ok)
			Result.extend ("Created", K_htsc_created)
			Result.extend ("Accepted", K_htsc_accepted)
			Result.extend ("Non-Authoritative Information", K_htsc_nonauthoritative_info)
			Result.extend ("No Content", K_htsc_no_content)
			Result.extend ("Reset Content", K_htsc_reset_content)
			Result.extend ("Partial Content", K_htsc_partial_content)
			Result.extend ("Multi-Status", K_htsc_multistatus_wd)
			Result.extend ("Multiple Choices", K_htsc_multiple_choices)
			Result.extend ("Moved Permanently", K_htsc_moved_permanently)
			Result.extend ("Found", K_htsc_found)
			Result.extend ("See Other", K_htsc_see_other)
			Result.extend ("Not Modified", K_htsc_not_modified)
			Result.extend ("Use Proxy", K_htsc_use_proxy)
			Result.extend ("Switch Proxy", K_htsc_switch_proxy)
			Result.extend ("Temporary Redirect", K_htsc_temp_redirect)
			Result.extend ("Bad Request", K_htsc_bad_request)
			Result.extend ("Unauthorized", K_htsc_unauthorized)
			Result.extend ("Payment Required", K_htsc_payment_required)
			Result.extend ("Forbidden", K_htsc_forbidden)
			Result.extend ("Not Found", K_htsc_not_found)
			Result.extend ("Method Not Allowed", K_htsc_method_not_allowed)
			Result.extend ("Not Acceptable", K_htsc_not_acceptable)
			Result.extend ("Proxy Authentication Required", K_htsc_proxy_auth_required)
			Result.extend ("Request Timeout", K_htsc_request_timeout)
			Result.extend ("Conflict", K_htsc_conflict)
			Result.extend ("Gone", K_htsc_gone)
			Result.extend ("Length Required", K_htsc_length_required)
			Result.extend ("Precondition Failed", K_htsc_precondition_failed)
			Result.extend ("Request Entity Too Large", K_htsc_req_entity_too_large)
			Result.extend ("Request-URI Too Long", K_htsc_req_uri_too_long)
			Result.extend ("Unsupported Media Type", K_htsc_unsupported_media_type)
			Result.extend ("Requested Range Not Satisfiable", K_htsc_req_range_not_satisfiable)
			Result.extend ("Expectation Failed", K_htsc_expectation_failed)
			Result.extend ("I'm a teapot", K_htsc_teapot)
			Result.extend ("There are too many connections from your internet address", K_htsc_too_many_connections)
			Result.extend ("Unprocessable Entity", K_htsc_unprocessable_entity)
			Result.extend ("Locked", K_htsc_locked)
			Result.extend ("Failed Dependency", K_htsc_failed_dependency)
			Result.extend ("Unordered Collection", K_htsc_unordered_collection)
			Result.extend ("Upgrade Required", K_htsc_upgrade_required)
			Result.extend ("Retry With", K_htsc_retry_with)
			Result.extend ("Blocked by Windows Parental Controls", K_htsc_blocked_parental)
			Result.extend ("Internal Server Error", K_htsc_internal_server_error)
			Result.extend ("Not Implemented", K_htsc_not_implemented)
			Result.extend ("Bad Gateway", K_htsc_bad_gateway)
			Result.extend ("Service Unavailable", K_htsc_service_unavailable)
			Result.extend ("Gateway Timeout", K_htsc_gateway_timeout)
			Result.extend ("HTTP Version Not Supported", K_htsc_http_version_not_supported)
			Result.extend ("Variant Also Negotiates", K_htsc_variant_also_negotiates)
			Result.extend ("Insufficient Storage", K_htsc_insufficient_storage_wd)
			Result.extend ("Bandwidth Limit Exceeded", K_htsc_bandwidth_exceeded)
			Result.extend ("Not Extended", K_htsc_not_extended)
			Result.extend ("User access denied", K_htsc_user_access_denied)
		end

end -- class AEL_HTTP_STATUS_CODES
