note
	description: "An actual back-and-forth exchange using the HTTP protocol"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create and instance of this class by using the 'make_with_request' routine.
This class is typically used as a supplier to HTTP_CLIENT
}"

class AEL_HTTP_TRANSACTION

inherit
	AEL_HTTP_CONSTANTS

create
 make_with_request

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_request (rq: like request)
		require
			exists: rq /= Void
		do
			request := rq
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_use_ssl (tf: BOOLEAN)
			-- If True, then use https; else use http
		do
			use_ssl := tf
		ensure
			is_set: use_ssl = tf
		end

--|--------------------------------------------------------------

	set_end_procedure (v: like end_proc)
			-- Set the procedure to call upon completion of an exchange,
			-- successfully or not
		do
			end_proc := v
		ensure
			assigned: end_proc = v
		end

--|========================================================================
feature -- Value
--|========================================================================

	use_ssl: BOOLEAN
			-- Should this transaction use SSL (i.e. https)?

	url: HTTP_URL
			-- Full URL of target
		do
			Result := request.url
		end

 --|------------------------------------------------------------------------

	content_length: INTEGER
			-- Length, in bytes, of the body of the outbound message (if any)
		do
			Result := request.content_length
		end

	content: STRING
			-- Body of outbound message (POST)
		do
			Result := request.content
		end

 --|------------------------------------------------------------------------
	
	request: AEL_HTTP_REQUEST
			-- The request for which this transaction was created

	response: STRING
			-- Body of inbound message (all modes)

	response_headers: LINKED_LIST [STRING]
			-- Headers from response, one per list item

	response_headers_concatenated: STRING
			-- All headers in the response concatenated

	user_agent: STRING
			-- Name of Agent to use in header
		do
			Result := request.client.user_agent
		end

	--|--------------------------------------------------------------

	port: INTEGER
			-- Destination port and target host
		do
			Result := request.port
		end

	--|--------------------------------------------------------------

	has_open_failure: BOOLEAN
			-- Did this transaction experience and open failure?
		
	is_prepared: BOOLEAN
			-- Is this transaction prepared for execution?
		do
			--			Result := url /= Void
			Result := True
		end

	--|--------------------------------------------------------------

	is_successful: BOOLEAN
			-- Was this transaction successful?
			-- It was at least partially successful of the status
			-- code is lower than 400
		local
--RFO 			fh: STRING
		do
			if http /= Void then
				Result := http.error_code = 0
--RFO 				fh := http.first_header
--RFO 				if fh /= Void and then not fh.is_empty then
--RFO 					Result := fh.substring_index ("200 OK", 1) /= 0
--RFO 				end
			end
		end

	status: INTEGER
			-- HTTP Status code of last execution attempt
		do
			if http /= Void then
				Result := http.status_code
			end
		end

	--|--------------------------------------------------------------

--	end_proc: PROCEDURE [detachable TUPLE[like Current]]
	end_proc: PROCEDURE
			-- Optional procedure to be called at transaction end

--|========================================================================
feature -- Execution
--|========================================================================

	execute
			-- Execute the actual transaction
		require
			is_prepared: is_prepared
		local
			opened: BOOLEAN
		do
--RFO			create http.make (url)
--RFO			http.set_read_mode
--RFO			http.open
			has_open_failure := False
			create response.make (K_ht_dflt_bufsize)
			-- RFO, we do this several times
			if request.assembled_string = Void then
				request.assemble
			end
			create http.make (url)
			http.set_read_mode
			-- Open will raise an exception if the host is unreachable
			-- This is stupid, so we have to take precautions before
			-- calling open
			http.reset_error
			http.open
			if http.has_error then
				-- Report it
				has_open_failure := True
			else
				opened := True
				http.execute_request (request)
				response_headers := http.header_list
				response_headers_concatenated := http.all_headers
				if http.error then
					response.append (http.first_header)
				else
					http.read
					from response.append (http.last_packet)
					until not http.is_packet_pending
					loop
						http.read
						response.append( http.last_packet )
					end
				end
				http.close
				if end_proc = Void then
					debug ("http")
						if response.is_empty then
							io.put_string ("Empty response%N")
						else
							io.put_string (response)
						end
					end
				else
					end_proc.call ([Current])
				end
			end
		ensure
			response_exists: response /= Void
		rescue
			if http /= Void and then http.has_error then
				has_open_failure := True
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	http: AEL_HTTP_RESOURCE

end -- class AEL_HTTP_TRANSACTION
