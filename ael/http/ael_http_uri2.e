class AEL_HTTP_URI2
-- URI as interpreted in an HTTP server-side application environment
-- Supports syntactic decomposition only
-- Does not include host, port or service fields because the
-- perspective is from the service application receiving info
-- from the http server which has already dealt with the host, service
-- and port fields.
--
-- URI structure is:
-- <oid>[<param_list>][<query_string>]
-- Where
--   <oid>          includes all fields from the beginning, up to but
--                  not including any params or query string
--   <param_list>   optional; begins with ';' and ends at end of URI
--                  string or start of query string
--   <query_string> optional; begins with '?' and ends at end of URI
--
-- Body of <param_list> must not have an embedded query string, but
-- can have an embedded URI, as in:
-- <uri> ::= <basic_uri>["?"<query_string>]
-- <basic_uri> ::= <oid>[";"<param_list>]
-- <param_list> ::= <param>["&"<param>]
-- <param> ::= <tv_pair>|<basic_uri>

inherit
	AEL_HTTP_STATUS_CODES
	AEL_SPRT_MULTI_FALLIBLE

create
	make, make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_string (v: STRING)
		require
			valid: is_valid_uri (v)
		do
			make
			set_uri_string (v)
		end

	--|--------------------------------------------------------------

	make
		do
			create parameters.make
			object_id := ""
			create {LINKED_LIST [STRING]} obj_fields.make
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_uri_string (v: STRING)
			-- Set the full URI string to 'v'
			-- This also triggers analysis/decomposition
		require
			valid: is_valid_uri (v)
		do
			original_uri := v.twin
			decompose
		end

	--|--------------------------------------------------------------

--RFO	set_oid_first_field_analyzer (v: like oid_first_field_analyzer)
--RFO			-- Set the agent to call for analyzing the contents of the
--RFO			-- first OID field to 'v'
--RFO		do
--RFO			oid_first_field_analyzer := v
--RFO		end

--RFO	set_forbidden_uris (v: like forbidden_uris)
--RFO			-- Set the list of forbidden (but syntactically legal) URIs
--RFO			-- to 'v'
--RFO		do
--RFO			forbidden_uris := v
--RFO		end

--RFO	set_oid_target_types (v: like oid_target_types)
--RFO			-- Set the list of OID target types to 'v'
--RFO		do
--RFO			oid_target_types := v
--RFO		end

--|========================================================================
feature -- Status
--|========================================================================

	original_uri: detachable STRING
			-- Full text of URI from which Current was created

	object_id: STRING
			-- Object ID part of URI

	query_string: detachable AEL_HTTP_QUERY_STRING
			-- Query string part of URI

	raw_params: detachable LIST [STRING]

	parameters: LINKED_LIST [AEL_HTTP_URI_PARAM]
			-- Parameters part of URI as list of strings
			-- each item represents a field separated by ';'
			-- A given field might be simple or complex

	is_top: BOOLEAN
			-- Is URI simply "/", the top of the uri tree?

	obj_fields: LIST [STRING]
			-- Fields in the OID portion of the URI

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_uri (v: STRING): BOOLEAN
			-- A valid URI must begin with a forward slash ('/')
			-- RFO TODO - better validation
		do
			if v /= Void and then not v.is_empty then
				Result := v.item (1) = '/'
			end
		end

--|========================================================================
feature -- Analysis
--|========================================================================

	decompose
			-- Decompose the URI string into its constituent parts
			-- If the URI has a query string then save it as query_string
			-- If the URI has parameters, then save them into parameters
			-- The text to the left of the first '?' or ';' (if any) is
			-- saved as object_id.
			-- The object_id string is further broken into object_fields.
			--
			-- N.B. this routine is called on set_uri_string, but can be
			-- called separately to allow for creation with string and
			-- subsequent context setting (e.g. target types)
		require
			has_text: original_uri /= Void
		local
			spos, epos, lim: INTEGER
		do
			if original_uri.count = 1 then
				-- Valid URI begins with '/'
				-- This must be simply '/'
				is_top := True
			else
				-- Extract the query string from the URI if one exists
				spos := original_uri.index_of ('?', 1)
				epos := original_uri.count
				if spos /= 0 then
					create query_string.make_from_string (
						original_uri.substring (spos + 1, epos))
					epos := spos - 1
				end
				-- Now break out any parameters
				-- Extract the text following the first semicolon (if any)
				-- All text from this point to the end of the uri text,
				-- or to the query string part, denotes parameters or
				-- resources subordinate to parameters.

				spos := original_uri.index_of (';', 1)
				if spos /= 0 then
					raw_params := original_uri.substring (spos+1, epos).split (';')
					epos := spos - 1
				end

				if raw_params /= Void then
					-- Decompose parameters
					from
						raw_params.start
					until
						raw_params.exhausted
					loop
						parameters.extend (
							create {AEL_HTTP_URI_PARAM}.make_from_string (
								raw_params.item))
						raw_params.forth
					end
				end

				-- What remains is the object ID part
				-- Trim all leading and trailing '/' characters from
				-- the object id string

				lim := epos
				from spos := 1
				until spos > lim or original_uri.item (spos) /= '/'
				loop
					spos := spos + 1
				end
				if spos <= lim then
					object_id := original_uri.substring (spos, epos)
					object_id.prune_all_trailing ('/')
				end
				if not object_id.is_empty then
					obj_fields := object_id.split ('/')
				end
--RFO				if object_id.is_empty then
--RFO					-- The URI was nothing but slashes
--RFO					uri_target_type := K_tt_top
--RFO				else
--RFO					-- Now look for specific object IDs (in first field of
--RFO					-- URI) to determine target type (if any)
--RFO					obj_fields := object_id.split ('/')
--RFO					obj_fields.start
--RFO					toid := obj_fields.item
--RFO					if obj_fields.count = 1 then
--RFO						if forbidden_uris /= Void and then forbidden_uris.has (toid)
--RFO						 then
--RFO							 set_error (K_htsc_forbidden, "Illegal URI")
--RFO						end
--RFO					end
--RFO					if not has_error then
--RFO						if oid_target_types /= Void and then
--RFO							oid_target_types.has (toid)
--RFO						 then
--RFO							 uri_target_type := oid_target_types.item (toid)
--RFO						elseif oid_first_field_analyzer /= Void then
--RFO							oid_first_field_analyzer.call ([obj_fields])
--RFO						end
--RFO					end
--RFO				end
			end
		end

--|========================================================================
feature -- URI and OID classification
--|========================================================================

--RFO	forbidden_uris: HASH_TABLE [STRING, STRING]
--RFO			-- URI first-fields that are explicitly forbidden
--RFO			-- e.g. "/admin" might be forbidden as it might invite
--RFO			-- snooping
--RFO
--RFO	oid_target_types: HASH_TABLE [INTEGER, STRING]
--RFO			-- URI target types for specific first-field OIDs
--RFO
--RFO	oid_first_field_analyzer: PROCEDURE [ANY, TUPLE [LIST[STRING]]]
--RFO			-- Procedure to call to analyze the first field of an OID/URI

	--|--------------------------------------------------------------
invariant
	has_oid: object_id /= Void
	has_obj_fields: obj_fields /= Void
	has_parameters_list: parameters /= Void

end -- class AEL_HTTP_URI2
