class AEL_HTTP_URI_PARAM
-- Parameter portion of an AEL_HTTP_URI

inherit
	ANY
		redefine
			default_create, out
		end

create
	default_create, make_from_string

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make_from_string (v: STRING)
		require
			valid: is_valid_param_string (v)
		local
			ti: INTEGER
		do
			ti := v.index_of ('/', 1)
			if ti /= 0 then
				top := v.substring (1, ti - 1)
				sub_param := v.substring (ti + 1, v.count)
				depth := 1 + v.occurrences ('/')
			else
				top := v
				depth := 1
			end
			is_valid := True
		ensure
			valid: is_valid
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		do
			top := ""
			Precursor
		end

--|========================================================================
feature -- Measurement
--|========================================================================

	depth: INTEGER
			-- Number of subordinate elements

--|========================================================================
feature -- Status
--|========================================================================

	top: STRING
			-- Top-most element in param (before '/')

	sub_param: detachable STRING
			-- Subordinate elements (after '/')

	has_sub_param: BOOLEAN
		do
			Result := sub_param /= Void
		end

	out: STRING
			-- String representation of Current
		do
			Result := "top: " + top + ", depth=" + depth.out
			if attached sub_param as sp then
				Result.append (", sub=" + sp)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid: BOOLEAN
			-- Is Current a valid URI param?

	is_valid_param_string (v: STRING): BOOLEAN
			-- Does the given string represent a valid URI parameter?
		do
			if v /= Void and then not v.is_empty then
				Result := not v.item (1).is_equal ('/') and
					not v.item (v.count).is_equal ('/')
			end
		end

	--|--------------------------------------------------------------
invariant
	has_top: is_valid implies (top /= Void and then not top.is_empty)
	valid_depth: is_valid implies depth >= 1
	has_sub_if_deep: depth > 1 implies sub_param /= Void

end -- class AEL_HTTP_URI_PARAM
