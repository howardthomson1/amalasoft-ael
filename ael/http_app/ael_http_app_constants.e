deferred class AEL_HTTP_APP_CONSTANTS

--|========================================================================
feature -- Platform-specific values
--|========================================================================

	is_windows: BOOLEAN
			-- Is this runtime executing in a Windows environment?
		local
			pf: PLATFORM
		do
			create pf
			Result := pf.is_windows
		end

	ks_platform_type: STRING_8
		once
			if is_windows then
				Result := "Windows"
			else
				inspect eif_os
				when 2 then
					Result := "Linux"
				when 3 then
					Result := "SunOS"
				when 6 then
					Result := "FreeBSD"
				when 7 then
					Result := "HPUX"
				when 9 then
					Result := "OpenBSD"
				else
					Result := "UNIX/Linux"
				end
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Constants
--|========================================================================

	ks_alphabet: STRING_8 = "abcdefghijklmnopqrstuvwxyz"
			-- Lower case (Latin-1/English) alphabet

	ks_hex_digits_lower: STRING_8 = "0123456789abcdef"
			-- Lower case hex digits, in sequence

	ks_http_user_agent: STRING_8
		once
			Result := "AELhttp/1.0 (" + ks_platform_type + ")"
		end

	--|--------------------------------------------------------------

	K_access_level_read: INTEGER = 0
	K_access_level_write: INTEGER = 1
	K_access_level_admin: INTEGER = 2

 --|========================================================================
feature {NONE} -- Externals
 --|========================================================================

	eif_os: INTEGER_32
			-- The Eiffel OS value from the runtime
		external
			"C inline use %"eif_config.h%""
		alias
			"return EIF_OS;"
		end

end -- class AEL_HTTP_APP_CONSTANTS

