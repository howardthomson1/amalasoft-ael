deferred class AEL_HTTP_APP_LOGGING
-- Application logging support
--
--	APPLICATION_ENV is provided by the specific application

inherit
	AEL_SPRT_LOGGING
		undefine
			asr, afr, new_log_entry
		end
--RFO 	APPLICATION_ENV

end -- class AEL_HTTP_APP_LOGGING
