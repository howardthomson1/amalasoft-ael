deferred class AEL_HTTP_APP_RULES
-- Validation and other rules and functions

inherit
	AEL_HTTP_APP_CONSTANTS

--|========================================================================
feature -- Validation
--|========================================================================

--	is_valid_access_mode (v: INTEGER): BOOLEAN
--			-- Is the given value a valid access mode?
--		do
--			inspect v
--			when
--				K_amode_read, K_amode_add, K_amode_delete, K_amode_replace,
--				K_amode_chain, K_amode_list, K_amode_manage, K_amode_link
--			 then
--				 Result := True
--			else
--			end
--		end

	--|--------------------------------------------------------------

--	is_valid_access_string (v: STRING): BOOLEAN
--			-- Is the given string a valid access condition?
--		local
--			i, lim: INTEGER
--		do
--			if v /= Void and then v.count = K_num_access_modes * 2 then
--				Result := True
--				lim := v.count
--				from i := 1
--				until i > lim or not Result
--				loop
--					if not v.item (i).is_hexa_digit then
--						Result := False
--					else
--						i := i + 1
--					end
--				end
--			end
--		end

	--|--------------------------------------------------------------

	is_valid_auth_level (v: INTEGER): BOOLEAN
		do
			inspect v
			when
				K_access_level_read,
				K_access_level_write,
				K_access_level_admin
			 then
				 Result := True
			else
			end
		end

end -- class AEL_HTTP_APP_RULES
