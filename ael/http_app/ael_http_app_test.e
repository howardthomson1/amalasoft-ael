class AEL_HTTP_APP_TEST

create
	make_with_id, make_with_variables

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_id (v: STRING)
		require
			exists: v /= Void and then not v.is_empty
		do
			id := v
			create variables.make
		end

	make_with_variables (v: STRING; va: ARRAY [ARRAY [STRING]])
		require
			id_exists: v /= Void and then not v.is_empty
		do
			make_with_id (v)
			set_variables (va)
		end

--|========================================================================
feature -- Status
--|========================================================================

	id: STRING
	variables: TWO_WAY_LIST [AEL_SPRT_TV_PAIR]

	input_filename: STRING
			-- Name of optional input file (if any)

	input_buffer: STRING
			-- Optional input stream (if any)

	is_interactive: BOOLEAN
			-- Is setup for this test interactive?
		do
			Result := content_prompt /= Void
		end

	content_prompt: STRING
			-- Prompt to display when interactive

	driver: AEL_HTTP_DEBUG_DRIVER
			-- Optional debug driver

--|========================================================================
feature -- Element change
--|========================================================================

	set_input_buffer (v: STRING)
		do
			input_buffer := v
		end

	set_content_prompt (v: STRING)
		do
			content_prompt := v
		end

	--|--------------------------------------------------------------

	set_variables (va: ARRAY [ARRAY [STRING]])
			-- Create tv pairs from the given value/tag variables
			-- N.B. variables have reverse order of tv pairs,
			-- with value appearing before tag!!!
		require
			exists: va /= Void
		local
			i, lim: INTEGER
			tv: ARRAY [STRING]
			ti: AEL_SPRT_TV_PAIR
		do
			variables.wipe_out
			lim := va.upper
			from i := va.lower
			until i > lim
			loop
				tv := va.item (i)
				if tv /= Void and then tv.count = 2 then
					create ti.make_from_tag_and_value (tv[2], tv[1])
					variables.extend (ti)
				else
					-- ERROR, or ignore?
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	add_variable (v, t: STRING)
			-- Add to the variables list the given value by its tag
			-- N.B. variables have reverse order of tv pairs,
			-- with value appearing before tag!!!
		do
			variables.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (t,v))
		end

	set_input_filename (v: STRING)
		do
			input_filename := v
		end

--|========================================================================
feature -- Interactive support
--|========================================================================

	prompt_for_content
		require
			interactive: is_interactive
		local
			ts: STRING
		do
			print (content_prompt)
			io.put_string ("%N> ")
			io.read_line
			ts := io.last_string
			io.new_line
			set_input_buffer (ts)
			create driver.make_with_content (ts)
		end

	--|--------------------------------------------------------------
invariant
	has_id: id /= Void and then not id.is_empty

end -- class AEL_HTTP_APP_TEST
