deferred class AEL_HTTP_APPLICATION
-- Abstract template for applications that use HTTP, either as
-- servers or as clients
-- APPLICATION_ENV is provided by specific application and must
-- inherit AEL_HTTP_APPLICATION_ENV or appropriate descendent

inherit
	AEL_APPLICATION
		redefine
			make, create_stable_attributes, reset_context_on_retry,
			initialize_application, complete_app_initialization
		end
	AEL_HTTP_APP_CONSTANTS
		undefine
			default_create
		end
	AEL_HTTP_STATUS_CODES
		undefine
			default_create
		end
--RFO 	AEL_SPRT_LOGGING
--RFO 		undefine
--RFO 			default_create
--RFO 		end

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	make
			-- Create and run this application.
		do
			Precursor {AEL_APPLICATION}
			if not has_error then
				if quick_test_enabled then
					execute_quick_test
				else
					logit (0, application_name + " started")
					-- Call main processing loop here
					begin_execution
				end
			end
			logit (0, application_name + " ended")
			logfile.close
		end

	--|--------------------------------------------------------------

	reset_context_on_retry
			-- Reset running context
		do
			logit (1, application_name + " reinitializing after signal")
			clear_errors
		end

 --|========================================================================
feature {NONE} -- User Initialization
 --|========================================================================

	create_stable_attributes
			-- Create any attached or stabile attributes.
			-- Feature name is deliberately mispelled for conformance
		do
		end

	--|--------------------------------------------------------------

	initialize_application
			-- Complete default_create initialization sequence
			-- Called DURING default_create sequence
		do
		end

	--|--------------------------------------------------------------

	complete_app_initialization
			-- Complete the initialization process, AFTER arg parsing 
			-- is completed
		do
			Precursor {AEL_APPLICATION}

			if root_required then
				if user_id /= 0 then
					set_user_id (0)
					if not setuid_successful then
						raise ("Unable to setuid to root")
					end
				end
			end
		end

--|========================================================================
feature -- Core execution
--|========================================================================

	begin_execution
			-- Begin execution of this application (e.g. begin main
			-- event loop)
		do
		end

	--|--------------------------------------------------------------

	execute_quick_test
			-- Execute a simple, quick test before the application
			-- starts in earnest (and in place of actual application logic)
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	quick_test_enabled: BOOLEAN

--|========================================================================
feature -- Logging and error recording
--|========================================================================

	dblogit (lvl: INTEGER; msg: STRING)
			-- Write the given message to the log file as a debug message
			-- conditionally (on debug_enabled) and with a log level 'lvl'
		require
			message_exists: msg /= Void
			valid_level: lvl >= 0
		do
			if debug_enabled then
				logit (lvl, "DEBUG: " + msg)
			end
		end

	traceit (lvl: INTEGER; msg: STRING)
			-- Write the given message to the log file as a trace message,
			-- with a log level of 'lvl'
		require
			message_exists: msg /= Void
			valid_level: lvl >= 0
		do
			logit (lvl, "TRACE: " + msg)
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	load_db
			-- Load the application db into memory
		do
		end

--|========================================================================
feature -- Support
--|========================================================================

	unescaped (str: STRING): STRING
			-- Unescaped form of the given string
		do
			Result := i18n.string_url_decoded (str)
		end

--|========================================================================
feature {NONE} -- Shared
--|========================================================================

--RFO 	i18n: AEL_I18N_STRING_ROUTINES
--RFO 		do
--RFO 			create Result
--RFO 		end

end -- class AEL_HTTP_APPLICATION
