deferred class AEL_HTTP_APPLICATION_ENV
-- Application environment
--
-- Application must provide APPLICATION_ENV class to implement
-- deferred features

inherit
	EXCEPTIONS
	AEL_SPRT_PLATFORM


--|========================================================================
feature -- Operating directory creation
--|========================================================================

	make_common_directories
		do
--RFO 			make_basedir
--RFO 			make_logdir
		end

	--|--------------------------------------------------------------

--RFO 	make_basedir
--RFO 		once
--RFO --RFO 			make_dir_from_parts (base_dir_path_parts)
--RFO 		end

	--|--------------------------------------------------------------

--RFO 	make_logdir
--RFO 			-- Create directory to hold log files
--RFO 		once
--RFO 			make_dir_from_parts (log_dir_path_parts)
--RFO 		end

	--|--------------------------------------------------------------

--RFO 	make_dir_from_parts (pa: ARRAY [STRING])
--RFO 			-- Create directory on disk from pathname components
--RFO 		require
--RFO 			exists: pa /= Void
--RFO 		local
--RFO 			i, lim: INTEGER
--RFO 			dn: DIRECTORY_NAME
--RFO 			td: DIRECTORY
--RFO 		do
--RFO 			lim := pa.upper
--RFO 			from
--RFO 				i := pa.lower
--RFO 				create dn.make_from_string (pa.item (i))
--RFO 				create td.make (dn)
--RFO 				if not td.exists then
--RFO 					td.create_dir
--RFO 				end
--RFO 				i := i + 1
--RFO 			until i > lim
--RFO 			loop
--RFO 				dn.extend (pa.item (i))
--RFO 				create td.make (dn)
--RFO 				if not td.exists then
--RFO 					td.create_dir
--RFO 				end
--RFO 				i := i + 1
--RFO 			end
--RFO 		end

--|========================================================================
feature -- Paths
--|========================================================================

	--RFO dir_name_from_parts (pa: ARRAY [STRING]): DIRECTORY_NAME
	--RFO 		-- Pathname derived from pathname parts 'pa'
	--RFO 	require
	--RFO 		exists: pa /= Void
	--RFO 	local
	--RFO 		i, lim: INTEGER
	--RFO 	do
	--RFO 		create Result.make
	--RFO 		lim := pa.upper
	--RFO 		from i := pa.lower
	--RFO 		until i > lim
	--RFO 		loop
	--RFO 			Result.extend (pa.item (i))
	--RFO 			i := i + 1
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	Ks_trace_log_name: FILE_NAME
			-- Pathname of exception trace log
		once
			create Result.make_from_string (Ks_log_dir_name)
			Result.extend ("exception_trace.log")
		end

	--|--------------------------------------------------------------

--RFO 	Ks_base_dir_name: DIRECTORY_NAME
--RFO 		-- Pathname of GRM base directory
--RFO 		once
--RFO 			Result := dir_name_from_parts (base_dir_path_parts)
--RFO 		end

	--|--------------------------------------------------------------

	Ks_log_dir_name: STRING
		-- Pathname of log directory
		deferred
		end

--|========================================================================
feature -- Debug support
--|========================================================================

--RFO 	dbprint (v: STRING)
--RFO 		do
--RFO 			debug_env.dbprint (v)
--RFO 		end

--RFO 	set_dbprint_proc (v: PROCEDURE [ANY, TUPLE [STRING]])
--RFO 		do
--RFO 			debug_env.set_dbprint_proc (v)
--RFO 		end

--RFO 	debug_env: AEL_SPRT_DEBUG_ENV
--RFO 		once
--RFO 			create Result
--RFO 		end

 --|--------------------------------------------------------------

--RFO 	enable_debug
--RFO 		do
--RFO 			debug_env.enable_debug
--RFO 		end

--RFO 	disable_debug
--RFO 		do
--RFO 			debug_env.disable_debug
--RFO 		end

--RFO 	set_debug_level (v: INTEGER)
--RFO 		do
--RFO 			debug_env.set_debug_level (v)
--RFO 		end

 --|--------------------------------------------------------------

--RFO 	enable_trace
--RFO 		do
--RFO 			debug_env.enable_trace
--RFO 		end

--RFO 	disable_trace
--RFO 		do
--RFO 		debug_env.enable_trace
--RFO 		end

--RFO 	set_trace_level (v: INTEGER)
--RFO 		do
--RFO 			debug_env.set_trace_level (v)
--RFO 		end

 --|--------------------------------------------------------------

--RFO 	enable_dbprint
--RFO 		do
--RFO 			debug_env.enable_dbprint
--RFO 		end

--RFO 	disable_dbprint
--RFO 		do
--RFO 			debug_env.disable_dbprint
--RFO 		end

 --|--------------------------------------------------------------

--RFO 	debug_enabled: BOOLEAN
--RFO 		do
--RFO 			Result := debug_env.debug_enabled
--RFO 		end

--RFO 	dbprint_enabled: BOOLEAN
--RFO 		do
--RFO 			Result := debug_env.dbprint_enabled
--RFO 		end

--RFO 	trace_enabled: BOOLEAN
--RFO 		do
--RFO 			Result := debug_env.trace_enabled
--RFO 		end

--|========================================================================
feature -- Execution environment
--|========================================================================

	root_required: BOOLEAN
			-- Does application require running as root?
		do
		end

	working_directory: STRING
		do
			Result := xenv.current_working_directory
		end

	default_content_type: STRING
		do
			Result := "text/html"
		end

--|========================================================================
feature -- Crash dump support
--|========================================================================

	--RFO dump_stack_trace
	--RFO 	local
	--RFO 		tf: PLAIN_TEXT_FILE
	--RFO 	do
	--RFO 		create tf.make_open_append (Ks_trace_log_name)
	--RFO 		tf.put_string (text_for_stack_trace)
	--RFO 		tf.close
	--RFO 	end

	--RFO text_for_stack_trace: STRING
	--RFO 		-- Text to put into stack trace
	--RFO 	do
	--RFO 		Result := exception_trace
	--RFO 	ensure
	--RFO 		exists: Result /= Void
	--RFO 	end

--|========================================================================
feature -- Platform features
--|========================================================================

--RFO 	base_dir_path_parts: ARRAY [STRING]
--RFO 		-- Pathname components of the GRM base directory path
--RFO 		deferred
--RFO 		ensure
--RFO 			exists: Result /= Void
--RFO 		end

--RFO 	log_dir_path_parts: ARRAY [STRING]
--RFO 		-- Pathname components of log directory
--RFO 		deferred
--RFO 		ensure
--RFO 			exists: Result /= Void
--RFO 		end

--|========================================================================
feature -- Shared utils
--|========================================================================

--RFO 	apf: AEL_PRINTF
--RFO 		once
--RFO 			create Result
--RFO 		end

	i18n: AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

--RFO 	asr: AEL_SPRT_STRING_ROUTINES
--RFO 		once
--RFO 			create Result
--RFO 		end

--RFO 	afr: AEL_SPRT_FILE_ROUTINES
--RFO 		once
--RFO 			create Result
--RFO 		end

	dtr: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

	xenv: EXECUTION_ENVIRONMENT
		once
			create Result
		end

	openv: OPERATING_ENVIRONMENT
		once
			create Result
		end

end -- AEL_HTTP_APPLICATION_ENV
