deferred class AEL_HTTP_CLIENT_APPLICATION
-- Template for client application that uses HTTP I/O
--
--	APPLICATION_ENV is provided by the specific application

inherit
	AEL_HTTP_APPLICATION

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	parse_cmd_line
		local
			cli: ARGUMENTS
			argv: ARRAY [STRING]
			arg: STRING
			i, argc: INTEGER
		do
			cli := xenv.command_line
			argv := cli.argument_array
			argc := cli.argument_count
			from i := 1
			until i > argc
			loop
				arg := argv.item (i)
				if not arg.is_empty then
					if arg.item (1) = '-' then
						if arg.count = 1 then
							set_error_message ("Malformed argument " + arg)
						else
							inspect arg.item (2)
							when '-' then
								-- A word arg --<arg>
							when 'd' then
								-- Enable debug
							when 'D' then
								-- Enable dbprint
							when 'i' then
								-- Interactive mode; prompt for tests
							when 't' then
								-- Enable trace
							when 'q' then
								-- Enable quick-test code
								quick_test_enabled := True
							else
								set_error_message ("Unrecognized argument " + arg)
							end
						end
					end
				end
				i := i + 1
			end
		end

end -- class AEL_HTTP_CLIENT_APPLICATION
