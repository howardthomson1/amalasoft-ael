deferred class AEL_HTTP_CMD
-- General notion of a command, specifically relating to servicing
-- HTTP requests
--
--	APPLICATION_ENV is provided by the specific application

inherit
	APPLICATION_ENV
		redefine
			set_error_message, set_error, has_error, last_error_code,
			last_error_message
		end
	AEL_HTTP_CONSTANTS
--	AEL_HTTP_APP_RULES
	AEL_HTTP_STATUS_CODES

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (ctxt: like context)
		require
			context_exists: ctxt /= Void
			valid_context: ctxt.is_valid
		do
			context := ctxt
			create response_body.make (1024)
			private_status_code := 201
			private_status_msg := ""
		end

--|========================================================================
feature -- Status
--|========================================================================

	context: AEL_HTTP_EXECUTION_CONTEXT

	env_vars: HASH_TABLE [STRING, STRING]
		do
			Result := context.env_vars
		end

	query_string: AEL_HTTP_QUERY_STRING
		do
			Result := context.query_string
		end

	content_type: STRING
		do
			Result := context.content_type
		end

	content_length: INTEGER
		do
			Result := context.content_length
		end

	content: STRING
		do
			Result := context.content
		end

	uri_target_type: INTEGER
			-- Type of target from the OID part of the URI,
			-- can be undefined
		do
			Result := context.uri_target_type
		end

	uri_target: STRING
			-- OID field corresponding to target type
			-- Typically the last field in the OID part
		do
			Result := context.uri_target
		end

	uri_string: STRING
		do
			Result := context.uri_string
		end

	parameters: LINKED_LIST [AEL_HTTP_URI_PARAM]
		do
			Result := context.parameters
		end

	has_parameters: BOOLEAN
		do
			Result := parameters /= Void and then
				not parameters.is_empty
		end

	input_stream: AEL_HTTP_SERVER_INPUT
		do
			Result := context.input
		end

	--|--------------------------------------------------------------

	is_authorized: BOOLEAN
			-- Has this request been duly authorized?

	authorization_checked: BOOLEAN
			-- Has the authorization level been checked yet?

	authorization_error_msg: STRING

	success_status_code: INTEGER
			-- Status code for successful execution
			-- Redefine in client as needed.  e.g. if is a create cmd,
			-- then change to K_htsc_created
		do
			Result := K_htsc_ok
		end

 --|--------------------------------------------------------------

	is_get: BOOLEAN
		do
			Result := context.is_get
		end

	is_head: BOOLEAN
		do
			Result := context.is_head
		end

	is_post: BOOLEAN
		do
			Result := context.is_post
		end

	is_put: BOOLEAN
		do
			Result := context.is_put
		end

	is_delete: BOOLEAN
		do
			Result := context.is_delete
		end

 --|--------------------------------------------------------------

	is_execute_ready: BOOLEAN
			-- Is the request ready for execution?
		do
			Result := is_authorized and not has_error
		end

	has_executed: BOOLEAN
			-- Has Current executed, or at least attempted to do so?

--|========================================================================
feature -- Status setting
--|========================================================================

	set_status (sc: INTEGER; sm: STRING)
		require
			message_exists: sm /= Void
		do
			private_status_code := sc
			private_status_msg := sm
		end

	--|--------------------------------------------------------------

	set_error_message (v: STRING)
		do
			set_error (K_htsc_internal_server_error, v)
		end

	set_error (ec: INTEGER; msg: STRING)
		do
			context.set_error (ec, msg)
		end

	--|--------------------------------------------------------------

	has_error: BOOLEAN
		do
			Result := context.has_error
		end

	last_error_code: INTEGER
		do
			Result := context.last_error_code
		end

	last_error_message: STRING
		do
			Result := context.last_error_message
		end

	error_code: INTEGER
		do
			Result := context.last_error_code
		end

	error_message: STRING
		do
			Result := context.last_error_message
		end

--|========================================================================
feature -- Access control
--|========================================================================

	minimum_access_level: INTEGER
			-- Access level needed to process this request.
			-- Possible values include K_access_level_read
			-- and K_access_level_write.
			-- e.g. If this level is "read" then if the object(s)
			-- involved have K_access_private, then this operation
			-- is not allowed.
			-- If object(s) has K_access_authenticated_read, then this
			-- is allowed only if the current user is registered with
			-- the storage service.
		deferred
		end

--|========================================================================
feature -- Request processing
--|========================================================================

	frozen process_request
		do
			analyze_request
			if not has_error then
				if is_execute_ready then
					execute_request
				elseif not is_authorized then
					if authorization_error_msg /= Void then
						set_error (K_htsc_unauthorized, authorization_error_msg)
					else
						set_error (K_htsc_unauthorized, "Authorization failed")
					end
				else
					-- What kind of error is it?
					set_error (K_htsc_bad_request, "Not able to execute")
				end
			end

			if has_error then
				set_status (last_error_code, last_error_message)
			else
				set_status (success_status_code, "")
			end
			clear_response
			build_response_content
			-- Application is responsbile for assembling and returning
			-- the response
		end

	--|--------------------------------------------------------------

	analyze_request
		do
			if not has_error then
				-- If URI type was determined during URI decomposition,
				-- then process the request accordingly
				if is_defined_target_type (uri_target_type) then
					-- Target type defined but not handled
					analyze_defined_tt_request
				elseif undefined_targets_are_supported then
					-- Defer analysis to specialzed descendent function
					analyze_undefined_tt_request
				else
					-- RFO Is this an internal error, assertable?
					set_error_message (generator +
						" does not support request of unknown target type")
				end
			end
			if not has_error then
				check_authorization (minimum_access_level)
			end
			analyze_request_target
		ensure
			auth_checked: (not has_error) implies authorization_checked
		end

	analyze_request_target
		do
		end

	--|--------------------------------------------------------------

	analyze_defined_tt_request
			-- Analyze the request that has a defined URI target type
		require
			defined_type: is_defined_target_type (uri_target_type)
		do
			if has_parameters then
				analyze_parameters
			end
		end

	--|--------------------------------------------------------------

	analyze_undefined_tt_request
			-- Analyze the request that has an undefined URI target type
		require
			undefined_type: uri_target_type = K_tt_undefined or
				not is_defined_target_type (uri_target_type)
		do
		end

 --|--------------------------------------------------------------

	analyze_parameters
			-- Check for correctness, applicability of paramaeters in URI
		require
			has_params: has_parameters
		local
			pl: like parameters
			oc: CURSOR
		do
			pl := parameters
			oc := pl.cursor
			from pl.start
			until pl.exhausted or has_error
			loop
				analyze_parameter (pl.item)
				pl.forth
			end
			pl.go_to (oc)
		end

	--|--------------------------------------------------------------

	analyze_parameter (p: AEL_HTTP_URI_PARAM)
		require
			exists: p /= Void
		do
			if not is_valid_parameter (p) then
				set_error (K_htsc_bad_request, "Invalid parameter in URI")
			end
		ensure
			error_if_invalid: not is_valid_parameter (p) implies has_error
		end

	--|--------------------------------------------------------------

	is_valid_parameter (v: AEL_HTTP_URI_PARAM): BOOLEAN
			-- Is 'v' a valid parameter for the command?
		do
			Result := v /= Void and then
				v.top /= Void and then not v.top.is_empty
		end

	--|--------------------------------------------------------------

	execute_request
			-- Perform the core logic of the command, after initial analysis
		require
			executable: is_execute_ready
		deferred
		ensure
			has_executed: has_executed
		end

	--|--------------------------------------------------------------

	build_response_content
			-- Construct the components of the HTTP response
			-- Build the response body
		deferred
			--append_response ("some text")
		end

 --|--------------------------------------------------------------

	check_authorization (lvl: INTEGER)
			-- Check that the given authorization level is satisfied
			-- Leave result in is_authorized
		require
			unchecked: not authorization_checked
--RFO 			valid_level: is_valid_auth_level (lvl)
		do
			-- RFO TODO
			is_authorized := True
		ensure
			checked: authorization_checked
		end

	--|--------------------------------------------------------------

	check_admin_authorization
		do
		end

--|========================================================================
feature -- Response construction
--|========================================================================

	append_response (v: STRING)
			-- Append 'v' to the body of the response
		do
			response_body.append (v)
		end

	--|--------------------------------------------------------------

	clear_response
			-- Clear contents of response body
		require
			response_exists: response_body /= Void
		do
			response_body.wipe_out
		ensure
			empty: response_body /= Void and then response_body.is_empty
		end

--|========================================================================
feature -- Response body and header values
--|========================================================================

	response_body: STRING
			-- Body of response (exclusive of headers)

	response_status: STRING
			-- status header for response (less newline)
		local
			ts: STRING
		do
			ts := http_status_code_messages.item (success_status_code)
			if ts = Void then
				ts := "OK"
			end
			Result := "Status: " + success_status_code.out + " " + ts
		end

	response_content_type: STRING
			-- Response content type header, less newline
			-- Redefine in descendent if needed, else leave to application
		do
			Result := private_response_content_type
			if Result = Void then
				Result := context.content_type
			end
		ensure
			exists: Result /= Void
		end

	response_cache_control: STRING
			-- Response Cache-Control header, less newline
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	response_redirect_file: STRING
			-- Response redirect header, less newline
			-- e.g. X-LIGHTTPD-send-file
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	response_content_disposition: STRING
			-- Response content disposition header, less newline
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	http_status_code: INTEGER
		do
			Result := private_status_code
		end

	http_status_msg: STRING
		do
			Result := private_status_msg
		end

	--|--------------------------------------------------------------

	application_specific_headers: STRING
			-- Application-specific header extensions
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_defined_target_type (t: INTEGER): BOOLEAN
			-- Is the given target type defined for this command?
		do
		end

	undefined_targets_are_supported: BOOLEAN
			-- Are requests with as-yet undefined target types supported by
			-- Current?
		do
			Result := True
		end

--|========================================================================
feature {NONE} -- Private implementation
--|========================================================================

	private_status_code: INTEGER
	private_status_msg: STRING

	private_response_content_type: STRING
			-- Response content type, if not same as context

 --|--------------------------------------------------------------
invariant
	has_vars: env_vars /= Void
	has_response_body: response_body /= Void

end -- class AEL_HTTP_CMD
