--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Driver to define different execution contexts for debugging
--| http (server) applications
--|----------------------------------------------------------------------

class AEL_HTTP_DEBUG_DRIVER

create
	make, make_with_content

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	make (fn: STRING)
			-- Create `Current' from the debug driver file with name 'fn'
		do
			create debug_file.make (fn)
			if debug_file.exists then
				init_from_file
			end
		end

	make_with_content (v: STRING)
		do
			set_content (v)
		end

	--|--------------------------------------------------------------

	init_from_file
			-- Initialize context information
		require
			file_exists: debug_file /= Void and then debug_file.exists
		local
			ex: EXECUTION_ENVIRONMENT
			el: like env_vars
			oc: CURSOR
		do
			debug_file.load
			if debug_file.is_loaded then
				create ex
				el := env_vars
				oc := el.cursor
				from el.start
				until el.exhausted
				loop
					ex.put (el.item.value, el.item.tag)
					el.forth
				end
				el.go_to (oc)
				is_initialized := True
			end
		ensure
			initialized: debug_file.is_loaded implies is_initialized
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	debug_file: AEL_HTTP_DEBUG_DRIVER_FILE
			-- File containing context information

	is_initialized: BOOLEAN
			-- Has this context been initialized?

	debug_file_is_loaded: BOOLEAN
		do
			Result := debug_file /= Void and then debug_file.is_loaded

		end

	--|--------------------------------------------------------------

	env_vars: LINKED_LIST [AEL_SPRT_TV_PAIR]
			-- List of environment variables
		do
			if debug_file_is_loaded then
				Result := debug_file.env_vars
			end
		ensure
			exists: debug_file_is_loaded implies Result /= Void
		end

	--|--------------------------------------------------------------

	input_filename: STRING
			-- Optional input filename
		do
			if debug_file /= Void then
				Result := debug_file.input_filename
			end
		end

	--|--------------------------------------------------------------

	content: STRING
			-- Optional request content
		do
			Result := private_content
			if Result = Void then
				if debug_file /= Void then
					Result := debug_file.content
				end
			end
			if Result = Void then
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

feature -- Status setting

	set_content (v: STRING)
		do
			private_content := v
		end

feature {NONE} -- Implementation

	private_content: STRING

end -- class AEL_HTTP_DEBUG_DRIVER

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 15-Mar-2010
--|     Renamed (was debug_driver); added audit trail
--|----------------------------------------------------------------------
--| How-to
--|
--| As the debug driver for AEL_HTTP_APPLICATION, debug driver mimics
--| the input stream coming from an HTTP server interface like CGI or
--| FCGI.
--| This is especially handy when you want to test requests that have
--| content,  rather than simple GET requests that use env vars and
--| URIs only.
--| Instantiate an object of this class by using the 'make' routine.
--| Object is initialized on creation with contents of named file.
--| The debug driver file can contain environment variable definitions
--| and/or a file name denoting a file to use place of standard input.
--| Refer to the AEL_HTTP_DEBUG_DRIVER_FILE class for structure info.
--|----------------------------------------------------------------------
