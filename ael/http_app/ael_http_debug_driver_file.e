--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Structured file containing information used to mimic that from an
--| HTTP interface like CGI or FCGI for debugging.
--|----------------------------------------------------------------------

class AEL_HTTP_DEBUG_DRIVER_FILE

inherit
	PLAIN_TEXT_FILE
	AEL_SPRT_FALLIBLE

create
	make

--|========================================================================
feature -- Initialization
--|========================================================================

	load
			-- Read file contents
		require
			file_exists: exists
		do
			create env_vars.make
			open_read
			if not is_open_read then
				set_error (1, "Unable to open file")
			else
				read_header
				if not end_of_file then
					if last_string.is_equal (Ks_content_start) then
						read_content
					elseif last_string.starts_with (Ks_input_tag) then
						get_input_filename
					else
						-- Error
					end
				end
				close
			end
		ensure
			is_closed: is_closed
			is_loaded: is_loaded
		end

	--|--------------------------------------------------------------

	read_header
				-- Parse headers for comments, env_vars, misc. trash
		require
			readable: is_open_read
		local
			done: BOOLEAN
		do
			from
				read_line
				if is_valid_env_var (last_string) then
					add_env_var_from_string (last_string)
				elseif is_comment (last_string) then
					-- Skip comments
				else
					if last_string.is_equal (Ks_content_start) or last_string.starts_with (Ks_input_tag) then
						set_error (2, "File does not have headers")
					else
						set_error (3, "Malformed header line")
					end
				end
			until
				done or end_of_file or has_error
			loop
				read_line
				if is_valid_env_var (last_string) then
					add_env_var_from_string (last_string)
				elseif last_string.is_equal (Ks_content_start) or last_string.starts_with (Ks_input_tag) then
					if env_vars.is_empty then
						set_error (2, "File does not have headers")
					else
						done := True
					end
				end
			end
		end

	get_input_filename
			-- Extract input filename from last_string
		require
			is_input_tag: last_string /= Void and then last_string.starts_with (Ks_input_tag)
		do
			input_filename := last_string.substring (Ks_input_tag.count + 1, last_string.count)
		end

	read_content
				-- Read content
		require
			readable: is_open_read
		do
			read_stream (count)
			content := last_string
		end

	add_env_var_from_string (v: STRING)
				-- Add one environment variable
		require
			valid: is_valid_env_var (v)
		local
			tv: AEL_SPRT_TV_PAIR
		do
			create tv.make_from_string (v)
			env_vars.extend (tv)
		ensure
			item_added: env_vars.last.out.is_equal (v)
			added: env_vars.count = old env_vars.count + 1
		end


feature -- Status

	is_loaded: BOOLEAN
			-- Is the file loaded?
			do
				Result := env_vars /= Void
			end

	env_vars: LINKED_LIST [AEL_SPRT_TV_PAIR]
			-- List of environment variables

	is_valid_env_var (v: STRING): BOOLEAN
			-- Is the given string a valid env_var?
		do
			Result := K_dummy_tv_pair.is_tv_pair (v)
		end

	is_comment (v: STRING): BOOLEAN
			-- Is the given string a comment?
		local
			sr: AEL_SPRT_STRING_ROUTINES
			ti: INTEGER
		do
			if v /= Void and then not v.is_empty then
				create sr
				ti := sr.index_of_next_non_whitespace (v, 1, False)
				Result := ti /= 0 and then v.item (ti) = '#'
			end
		end

	content: STRING
			-- File contents after headers

	input_filename: STRING
			-- Optional request body

feature -- Constants

	K_dummy_tv_pair: AEL_SPRT_TV_PAIR
		once
			create Result.make_from_string ("a=b")
		end

	Ks_content_start: STRING = "START OF CONTENT"

	Ks_input_tag: STRING = "INPUT FILENAME: "

end -- class AEL_HTTP_DEBUG_DRIVER_FILE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 15-Mar-2010
--|     Renamed (was debug_driver); added audit trail
--|----------------------------------------------------------------------
--| How-to
--|
--|The debug driver file is structured as follows.
--|  N lines of env vars as tag-value pairs (a=b), one pair per line
--|  Optionally, after all env var lines, a section of request content
--|  or the name of a file containing request content.
--|
--| The content tag line has the form:
--|START OF CONTENT
--|... content ...
--|
--| The input tag line has the form:
--|INPUT FILENAME: <fname>
--|
--|  Where <fname> is the full pathname of the input file.
--|----------------------------------------------------------------------

