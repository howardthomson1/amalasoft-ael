class AEL_HTTP_DELETE_CMD
-- A command to service a DELETE request within an http server
--
-- DELETE specifies a URI to be deleted
-- URI denotes a unique resource

inherit
	AEL_HTTP_CMD

create
	make

--|========================================================================
feature -- Status
--|========================================================================

	minimum_access_level: INTEGER
		do
			Result := K_access_level_write
		end

--|========================================================================
feature -- Request processing
--|========================================================================

	analyze_request
		do
			if not has_error then
				-- Find the URI target
				inspect uri_target_type
				when K_tt_object then
					-- Delete an object
				else
					set_error (K_x_bad_request, "URI malformed for DELETE request")
				end
			end
			authorization_checked := True
			is_authorized := True
		end

 --|--------------------------------------------------------------

	execute_request
		do
			delete_object
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	delete_object
		do
		end

end -- class AEL_HTTP_DELETE_CMD

