class AEL_HTTP_EXECUTION_CONTEXT

inherit
	AEL_HTTP_STATUS_CODES
	AEL_HTTP_REQUEST_FORMATS
--RFO 	AEL_SPRT_MULTI_FALLIBLE
--RFO 		undefine
--RFO 			apf
--RFO 		end
	APPLICATION_ENV
		rename
			user_name as system_user_name
		end

	EXECUTION_ENVIRONMENT

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (ev: like env_vars; rid: INTEGER; fn: STRING; dd: AEL_HTTP_DEBUG_DRIVER)
		require
			vars_exist: ev /= Void
		do
			env_vars := ev
			request_id := rid
			create input.make (fn, dd)
			create uri_stack.make
			set_content_type (default_content_type)
		end

 --|--------------------------------------------------------------

	extract_variables
			-- Extract relevant environment variables
		local
			ts: STRING
		do
			uri_string := env_var ("REQUEST_URI")
			if uri_string = Void or else uri_string.is_empty then
				set_error (K_htsc_bad_request, "Missing URI")
			end
			if not has_error then
				request_method := env_var ("REQUEST_METHOD")
				if request_method = Void or else request_method.is_empty then
					set_error (K_htsc_bad_request, "Missing request method")
				end
			end
			if not has_error then
				user_agent := env_var ("HTTP_USER_AGENT")
				authorization := env_var ("HTTP_AUTHORIZATION")
--				analyze_authorization
			end
			if not has_error then
				host := env_var ("HTTP_HOST")
				if host = Void or else host.is_empty then
					set_error (K_htsc_bad_request, "Missing host header")
				else
--RFO					analyze_host
				end
			end
			if not has_error then
				ts := env_var ("QUERY_STRING")
				if ts /= Void then
					create env_query_string.make_from_string (ts)
				end
				ts := env_var ("CONTENT_TYPE")
				if ts /= Void and then not ts.is_empty then
					set_content_type (ts)
				end
				ts := env_var ("CONTENT_LENGTH")
				if ts /= Void and then ts.is_integer then
					set_content_length (ts.to_integer)
				end
				path_translated := env_var ("HTTP_PATH_TRANSLATED")
--RFO				analyze_path
				cookie := env_var ("HTTP_COOKIE")
				from_id := env_var ("HTTP_FROM")
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	login_passed: BOOLEAN
	last_request: STRING

	input: AEL_HTTP_SERVER_INPUT

	env_vars: HASH_TABLE [STRING, STRING]
			-- Environment variables acquired from the server

	request_id: INTEGER

	uri: AEL_HTTP_URI
		do
			if not uri_stack.is_empty then
				Result := uri_stack.item
			end
		end

	uri_stack: LINKED_STACK [like uri]

	uri_string: STRING
			-- Full URI string

	original_uri: like uri
			-- URI directly from request

	object_id: STRING
			-- Object ID portion of URI

	oid_fields: LIST [STRING]
			-- OID portion of URI, broken into fields

	request_method: STRING
	alternate_method: STRING
	user_agent: STRING
	host: STRING
	host_address: STRING

	env_query_string: AEL_HTTP_QUERY_STRING
			-- QUERY_STRING extracted from env vars

	query_string: AEL_HTTP_QUERY_STRING
		do
			Result := env_query_string
			if Result = Void and uri /= Void then
				Result := uri.query_string
			end
		end

	content_length: INTEGER
	content_type: STRING

	path_translated: STRING
	from_id: STRING
	authorization: STRING
	cookie: STRING

 	user_name: STRING
			-- User name provided in header
	password: STRING
			-- Encrypted password provide in header

	--|--------------------------------------------------------------

	uri_target: detachable STRING
			-- Actual OID field extracted from URI, if any,
			-- after all prefix fields are analyzed and stripped away

	uri_target_type: INTEGER
			-- Type of target from the OID part of the URI,
			-- can be undefined

	has_parameters: BOOLEAN
			-- Does the URI have a parameters part?

	parameters: LINKED_LIST [AEL_HTTP_URI_PARAM]
			-- Params extracted from the parameters part of the URI, if any

	raw_parameters: LIST [STRING]
			-- Param string from the parameters part of the URI, if any

	request_format: INTEGER
			-- Format of request, and by default, of response
		do
			Result := private_request_format
		end

	is_valid_request_format (v: INTEGER): BOOLEAN
			-- Is 'v' a valid request format (e.g. XML, JSON, ..)
		do
			inspect v
			when K_rqfmt_xml, K_rqfmt_json, K_rqfmt_html then
				Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	is_xml: BOOLEAN
			-- Is this request in XML format?
		do
			Result := request_format = K_rqfmt_xml
		end

	--|--------------------------------------------------------------

	is_html: BOOLEAN
			-- Is this request in HTML format?
		do
			Result := request_format = K_rqfmt_html
		end

	--|--------------------------------------------------------------

	is_json: BOOLEAN
			-- Is this request in JSON format?
		do
			Result := request_format = K_rqfmt_json
		end

	--|--------------------------------------------------------------

	is_valid: BOOLEAN
			-- Is this context valid?
		do
			if uri_string /= Void and then not uri_string.is_empty then
				Result := is_valid_request_method (request_method)
			end
		end

 --|--------------------------------------------------------------

	is_valid_request_method (v: STRING): BOOLEAN
		do
			if v /= Void and then not v.is_empty then
				Result := is_get or is_put or is_post or is_delete or is_head
			end
		end

 --|--------------------------------------------------------------

	is_get: BOOLEAN
		do
			Result := request_method /= Void and then
				request_method.is_equal ("GET")
		end

	is_head: BOOLEAN
		do
			Result := request_method /= Void and then
				request_method.is_equal ("HEAD")
		end

	is_post: BOOLEAN
		do
			Result :=  request_method /= Void and then
				request_method.is_equal ("POST")
		end

	is_put: BOOLEAN
		do
			Result := request_method /= Void and then
				request_method.is_equal ("PUT")
		end

	is_delete: BOOLEAN
		do
			Result := request_method /= Void and then
				request_method.is_equal ("DELETE")
		end

	--|--------------------------------------------------------------

	is_special_request: BOOLEAN
			-- Is the current URI a special, built-in request?
		do
		end

	--|--------------------------------------------------------------

	is_reserved_word (v: STRING): BOOLEAN
			-- Is 'v' a reserved word?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := reserved_words.has (v)
		end

	reserved_words: HASH_TABLE [STRING, STRING]
		once
			create Result.make (7)
		end

	--|--------------------------------------------------------------

	forbidden_uris: HASH_TABLE [STRING, STRING]
			-- URI first-fields that are explicitly forbidden
			-- e.g. "/admin" might be forbidden as it might invite
			-- snooping
		once
			create Result.make (1)
		end

	--|--------------------------------------------------------------

	authorization_required: BOOLEAN

	set_authorization_required (s: BOOLEAN)
		do
			authorization_required := s
		end

	authorization_state: INTEGER

	is_authorized: BOOLEAN
		do
			Result := authorization_state = K_st_auth_passed
		end

	authenticated: BOOLEAN
	anonymous: BOOLEAN

	authenticate
			-- Check to see that user and password (if supplied) are
			-- known to this system
			-- Set authentication_error_msg
		do
			if user_name /= Void and then password /= Void then
				-- RFO TODO
				--Execute authentication
				authenticated := True
			else
				authenticated := False
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	analyze
			-- Analyze the execution context, identifying object IDs,
			-- parameters, query string, etc
		local
			turi: like uri
		do
			extract_variables
			if input.buffer_length /= 0 then
				set_content_length (input.buffer_length)
			elseif input.input_file /= Void then
				set_content_length (input.input_file.count)
			end
			if not has_error then
				if cookie /= Void and then not cookie.is_empty then
					validate_cookie
				end
				original_uri := uri
				create turi.make_from_string (uri_string)
				uri_stack.extend (turi)
				oid_fields := turi.obj_fields
				raw_parameters := uri.raw_params
				parameters := uri.parameters
				has_parameters := parameters /= Void and then not parameters.is_empty
				object_id := uri.object_id

				-- Determine target type if possible
				if turi.object_id.is_empty then
					-- The URI was nothing but slashes
					uri_target_type := K_tt_top
					-- Assume that this field is intended as a component ID
					uri_target := "/"
				else
					analyze_uri
				end
			end
		end

	--|--------------------------------------------------------------

	read_input (count: INTEGER)
		do
			input.read_stream (count)
		end

	content: STRING
		do
			Result := input.buffer_contents
		end

	set_last_request (v: STRING)
		require
			exists: not v.is_empty
		do
			last_request := v
		end

	set_content_length (v: INTEGER)
		require
			valid_length: V >= 0
		do
			content_length := v
		end

	set_content_type (v: STRING)
		require
			valid: v /= Void and then not v.is_empty
		do
			content_type := v
		end

	set_path_translated (v: STRING)
		do
			path_translated := v
		end

	set_path_from_uri
		-- Set the path_translated value from the URI
		do
--RFO TODO not sure if this is still a good idea
--RFO It should be unnecessary with the new uri decompose logic
			path_translated := uri_string
--RFO TODO
--RFO			analyze_path
		end

	--|--------------------------------------------------------------

	set_request_format (v: INTEGER)
		require
			valid_format: is_valid_request_format (v)
		do
			private_request_format := v
		ensure
			is_set: request_format = v
		end

	reset_request_format
		do
			private_request_format := 0
		end

	set_request_format_html
		do
			private_request_format := K_rqfmt_html
		ensure
			is_set: is_html
		end

	set_request_format_xml
		do
			private_request_format := K_rqfmt_xml
		ensure
			is_set: is_xml
		end

	set_request_format_json
		do
			private_request_format := K_rqfmt_json
		ensure
			is_set: is_json
		end

--|========================================================================
feature -- External representation
--|========================================================================

	var_out (v: STRING): STRING
		do
			if v = Void then
				Result := "Undefined"
			else
				Result := v
			end
		end

 --|--------------------------------------------------------------

	to_html: STRING
		-- Context info in HTML table form
		local
			fmt: STRING
		do
			fmt := "{
<TABLE BORDER=1>
<TR><TD>PID</TD><TD>%d</TD</TR>
<TR><TD>REQ ID</TD><TD>%d</TD></TR>
<TR><TD>Method</TD><TD>%s</TD></TR>
<TR><TD>URI</TD><TD>%s</TD></TR>
<TR><TD>Auth</TD><TD>%s</TD></TR>
<TR><TD>Cookie</TD><TD>%s</TD></TR>
</TABLE>
				}"
			Result := formatted_out (fmt)
		ensure
			exists: Result /= Void
		end

 --|--------------------------------------------------------------

	to_tv_pairs: STRING
		-- Context info in tag-value pairs form
		local
			fmt: STRING
		do
			fmt := "{
PID=%d
REQ ID=%d
Method=%s
URI=%s
Auth=%s
Cookie=%s
				}"
			Result := formatted_out (fmt)
		ensure
			exists: Result /= Void
		end

 --|--------------------------------------------------------------

	formatted_out (fmt: STRING): STRING
		-- Context info in formatted output
		require
			format_exists: fmt /= Void
			valid_format: fmt.occurrences ('%%') = fmt_out_arg_list.count
		do
			Result := aprintf (fmt, fmt_out_arg_list)
		ensure
			exists: Result /= Void
		end

	fmt_out_arg_list: ARRAY [ANY]
		do
			Result := << process_id, request_id,
			var_out (request_method),
			var_out (uri_string), var_out (authorization), var_out (cookie) >>
		end

--|========================================================================
feature -- Analysis
--|========================================================================

	analyze_uri
			-- Analyze the given URI for context-specific semantics
		require
			uri_exists: uri /= Void
			is_recorded: not uri_stack.is_empty
			has_oid: not object_id.is_empty
		local
			oid, toid: STRING
			flds: like oid_fields
		do
			oid := object_id
			-- Now look for specific object IDs (in first field of URI)
			-- to determine target type (if any)
			flds := oid_fields
			flds.start
			toid := flds.item
			if flds.count = 1 then
				if forbidden_uris /= Void and then forbidden_uris.has (toid) then
					set_error (K_htsc_forbidden, "Illegal URI")
				end
			end
			if not has_error then
				uri_target_type := oid_to_target_type (toid)
				uri_target := toid
				if uri_target_type = K_tt_undefined then
					analyze_unknown_uri
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_unknown_uri
			-- Analyze the given URI, already checked and not found to
			-- match a predefined target type
			--
			-- Should be redefined to be meaningful in a descendent
		require
			no_errors: not has_error
			uri_exists: uri /= Void
			is_recorded: not uri_stack.is_empty
			has_oid: not object_id.is_empty
			no_type_yet: uri_target_type = K_tt_undefined
		do
		end

	--|--------------------------------------------------------------

	analyze_authorization
			-- The authorization field can be missing for some browser
			-- based requests.  In such cases, the user name is
			-- considered to be "anonymous"
			-- When the authorization field is present, it can have
			-- different forms.  In each case, the form is denoted by a
			-- keyword following the Authorization tag, as in:
			--    Authorization: aelhttp <rest of authorization header>
			--
			-- The actual HTTP_AUTHORIZATION variable does not include
			-- the tag, but does include the keyword
		local
			astr, atag, abody: STRING
			pos: INTEGER
		do
			-- TODO, use real digest
			-- check encrypted password
			-- check permission level
			if not authorization_required then
				authorization_state := K_st_auth_passed
			else
				if authorization = Void or else authorization.is_empty then
					user_name := "anonymous"
				else
					astr := authorization
					pos := asr.index_of_next_whitespace (astr, 1, True)
					if pos = 0 then
						-- Empty auth content
						user_name := "anonymous"
					else
						authorization_state := K_st_auth_failed
						atag := astr.substring (1, pos - 1)
						pos := asr.index_of_next_non_whitespace (astr, pos + 1, True)
						if pos = 0 then
							-- Missing the body
							set_error (K_htsc_bad_request, "Missing authorization body")
						else
							abody := astr.substring (pos, astr.count)
							abody.prune_all_trailing ('%R')
							abody.prune_all_trailing ('%N')
							abody.left_adjust
						end
					end
				end
				if (not has_error) and atag /= Void and abody /= Void then
					atag.to_lower
					if atag.is_equal ("user") then
						analyze_user_authorization (abody)
					elseif atag.is_equal ("basic") then
						analyze_basic_authorization (abody)
					else
						analyze_special_authorization (abody)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_form_authorization (body: STRING)
			-- Analyze form data passed (method: POST) to URI "/login"
			-- 'body' is the request body,
			-- Content-Type should be "application/x-www-form-urlencoded"
		local
			user, pass: STRING
			tqs: AEL_HTTP_QUERY_STRING
		do
			create tqs.make_from_string (body)
			user := tqs.value_for_arg ("username")
			pass := tqs.value_for_arg ("password")
			if user /= Void and then not user.is_empty then
			-- and then not pass.is_empty then
				user_name := user
				password := pass
				authenticate
				if authenticated then
					-- RFO TODO
					-- Login was good, this should go somewhere else eventually
					login_passed := True
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_user_authorization (body: STRING)
		do
		end

	--|--------------------------------------------------------------

	analyze_basic_authorization (body: STRING)
		local
			ts: STRING
			pos: INTEGER
			b64: AEL_SPRT_BASE64
		do
			if body.is_equal ("Og==") then
				-- RFO TODO
				authenticated := True
			else
				pos := body.index_of (':', 1)
				if pos = 0 then
					create b64
					if b64.is_decodable_string (body) then
						ts := b64.decoded_string (body)
						pos := ts.index_of (':', 1)
					else
						ts := body
					end
				else
					ts := body
				end
				if pos = 0 then
					-- No password
					--JAH Probably an error?
					user_name := ts
				else
					user_name := ts.substring (1, pos - 1)
					password := ts.substring (pos + 1, ts.count)
				end
			end
			if not has_error then
				authorization_state := K_st_auth_passed
			end
		end

	--|--------------------------------------------------------------

	analyze_special_authorization (body: STRING)
		do
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	cookie_tag: STRING
		do
			Result := "AELHTTPCOOKIESTRING"
		end

	--|--------------------------------------------------------------

	validate_cookie
		require
			exists: cookie /= Void
		local
			spos, epos: INTEGER
			ts, ts2: STRING
			tc: AEL_HTTP_AUTH_COOKIE
		do
			ts2 := cookie_tag + "="
			-- Validate the cookie by generating a temporary one.
			spos := cookie.substring_index (ts2, 1) + ts2.count
			if spos /= 0 then
				epos := cookie.index_of ('&', spos)
				if epos < spos then
					epos := cookie.count
				end
				ts := cookie.substring (spos, epos)
				if ts /= Void then
					ts := i18n.string_url_decoded (ts)
					create tc.make_from_string (ts)
					if tc.is_valid_cookie_string (ts) then
						authenticated := True
					else
						set_error_message ("Cookied is invalid")
					end
				end
			end
		end

	--|--------------------------------------------------------------

	env_var (v: STRING): STRING
			-- Execution environment variable by the given tag
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := env_vars.item (v)
		end

	--|--------------------------------------------------------------

	private_request_format: INTEGER

 --|--------------------------------------------------------------
invariant
	has_vars: env_vars /= Void
	input_exists: input /= Void

end -- class AEL_HTTP_EXECUTION_CONTEXT
