deferred class AEL_HTTP_GET_CMD
-- A command to service GET request in an HTTP server

inherit
	AEL_HTTP_CMD

--|========================================================================
feature -- Status
--|========================================================================

	minimum_access_level: INTEGER
		do
			Result := K_access_level_read
		end

end -- class AEL_HTTP_GET_CMD
