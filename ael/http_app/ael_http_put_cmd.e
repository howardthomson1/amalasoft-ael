class AEL_HTTP_PUT_CMD
-- A command to service a PUT request within an HTTP service
--
-- PUT requests set the value for the resource identified by the URI
-- When the identified resource is an object, the value is the new
-- object contents (i.e. an upload).
-- When the identified resource is a container, the value is the
-- set of characteristics defining the container
-- When the identified resources is an attribute of an object, the
-- attribute's value is set.
--
-- Arguments appear in the body of the request, in standard form
--

inherit
	AEL_HTTP_CMD
		redefine
			analyze_defined_tt_request
		end

create
	make

--|========================================================================
feature -- Status
--|========================================================================

	minimum_access_level: INTEGER
		do
			Result := K_access_level_write
		end

--|========================================================================
feature -- Request processing
--|========================================================================

	analyze_defined_tt_request
			-- PUT requests are used for object uploads and creations,
			-- and for setting attribute values directly
			--
			-- The execution context has already analyzed the URI to the
			-- point where we know the intended target type, and in
			-- some cases, the target resource itself
		do
			Precursor
		end

 --|--------------------------------------------------------------

	execute_request
		do
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	build_response_content
		do
		end

end -- class AEL_HTTP_PUT_CMD
