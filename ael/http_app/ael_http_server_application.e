deferred class AEL_HTTP_SERVER_APPLICATION
-- Template for server application that uses HTTP I/O
-- Application needs a connector for i/o with the http server
-- Connectors include FastCGI (AEL_FCGI)
--
--	APPLICATION_ENV is provided by the specific application

inherit
	AEL_HTTP_APPLICATION
		redefine
			create_stable_attributes,
			prepare_abnormal_termination, initialize_application,
			begin_execution, complete_app_initialization,
			has_error, set_error, set_error_message, clear_errors,
			last_error, last_error_code, last_error_message,
			reset_context_on_retry
		end

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	prepare_abnormal_termination
			-- Set up for exit from abnormal termination
			-- From make's rescue clause
		do
			set_connector_exit_status (-2)
			set_http_status (
				K_htsc_internal_server_error,
				application_name + " terminated abnormally")
			Precursor {AEL_HTTP_APPLICATION}
		end

	--|--------------------------------------------------------------

	create_stable_attributes
			-- Create any attached or stabile attributes.
			-- Feature name is deliberately mispelled for conformance
		do
			Precursor {AEL_HTTP_APPLICATION}
			create private_response_body.make (1024)
			create private_response_header.make (1024)
			set_response_content_type ("text/html")
		ensure then
			valid_content_type: response_content_type /= Void
		end

 --|------------------------------------------------------------------------

	parse_cmd_line
		local
			cli: ARGUMENTS
			argv: ARRAY [STRING]
			arg, ts: STRING
			i, argc: INTEGER
		do
			cli := xenv.command_line
			ts := cli.coalesced_word_option_value ("-input=")
			if ts /= Void then
			end

			argv := cli.argument_array
			argc := cli.argument_count
			from i := 1
			until i > argc
			loop
				arg := argv.item (i)
				if not arg.is_empty then
					logit (1, "parse_cmd_line: arg=" + arg + "%N")
					if arg.item (1) = '-' then
						if arg.count = 1 then
							set_error_message ("Malformed argument " + arg)
						else
							inspect arg.item (2)
							when '-' then
								-- A word arg --<arg>
								if arg.substring_index ("--input=", 1) = 1 then
									if arg.count = 8 then
										set_error_message ("Malformed argument " + arg)
									else
										input_filename := arg.substring (9, arg.count)
									end
								elseif arg.substring_index ("--dd=", 1) = 1 then
									if arg.count = 5 then
										set_error_message ("Malformed argument " + arg)
									else
										dd_filename := arg.substring (6, arg.count)
									end
								else
								end
							when 'd' then
								-- Enabled debug
								if arg.count > 2 then
									ts := arg.substring (3, arg.count)
									if not ts.is_integer then
										-- ERROR
									else
										set_debug_level (ts.to_integer)
									end
								else
									set_debug_level (1)
								end
							when 'D' then
								-- Enabled dbprint
								enable_dbprint
							when 'i' then
								-- Interactive mode; prompt for tests
								is_interactive := True
							when 'T' then
								-- Enable built-in test
								if arg.count > 2 then
									setup_for_debug (arg.substring (3, arg.count))
								else
									setup_for_debug (Void)
								end
							when 't' then
								-- Enable trace
								if arg.count > 2 then
									ts := arg.substring (3, arg.count)
									if not ts.is_integer then
										-- ERROR
									else
										set_trace_level (ts.to_integer)
									end
								else
									set_trace_level (1)
								end
							when 'q' then
								-- Enable quick-test code
								quick_test_enabled := True
							else
								set_error_message ("Unrecognized argument " + arg)
							end
						end
					end
				end
				i := i + 1
			end
			if input_filename /= Void and dd_filename /= Void then
				set_error_message ("Input_filename is set from within dd_filename")
			end
		end

	--|--------------------------------------------------------------

	reset_context_on_retry
			-- Reset running context in case of
			-- an exception-triggered retry
		do
			Precursor {AEL_HTTP_APPLICATION}
			reset_context
		end

	reset_context
			-- Reset running context (not just per-request) in case of
			-- a restart
		do
			dd_filename := Void
			debug_driver := Void
			debug_is_setup := False
			input_filename := Void
			reset_execution_env_vars
			response_status := Void
			response_content_length := 0
			response_cache_control := Void
			response_redirect_file := Void
			content_disposition := Void
			http_status := 0
			http_status_msg := Void
			set_response_content_type ("text/html")
		end

	--|--------------------------------------------------------------

--RFO	initialize
--RFO		-- Establish the starting state
--RFO		do
--RFO			if user_id /= 0 then
--RFO				set_user_id (0)
--RFO				if not setuid_successful then
--RFO					raise ("Unable to setuid to root")
--RFO				end
--RFO			end
--RFO			dblogit (2, "Before recording PID")
--RFO			record_pid
--RFO			dblogit (2, "After recording PID")
--RFO
--RFO			dblogit(2,"before IPC socket setup")
--RFO--RFO			create ipc_thread.make (process_id)
--RFO--RFO			ipc_thread.launch
--RFO			-- sleep for a half second to let the others record their sockets
--RFO			usleep (500_000)
--RFO			generate_peer_list
--RFO
--RFO			dblogit(2,"before load_db")
--RFO			if is_interactive then
--RFO				set_dbprint_proc (agent print_debug)
--RFO			else
--RFO				set_dbprint_proc (agent put_debug)
--RFO			end
--RFO			load_db
--RFO			dblogit (2, "After initialization")
--RFO			create initial_env_vars.make (40)
--RFO			update_environment_variables (initial_env_vars)
--RFO		end

	initialize_application
			-- Complete default_create initialization sequence
			-- Called DURING default_create sequence
		do
			Precursor {AEL_HTTP_APPLICATION}
			if is_interactive then
				set_dbprint_proc (agent print_debug)
			else
				set_dbprint_proc (agent put_debug)
			end

			dblogit (2, "Before recording PID")
			record_pid
			dblogit (2, "After recording PID")

			dblogit(2,"before IPC socket setup")
--RFO			create ipc_thread.make (process_id)
--RFO			ipc_thread.launch
			-- sleep for a half second to let the others record their sockets
			usleep (500_000)
			generate_peer_list
		end

	complete_app_initialization
			-- Complete the initialization process, AFTER arg parsing 
			-- is completed
		do
			Precursor {AEL_HTTP_APPLICATION}
			create initial_env_vars.make (40)
			update_environment_variables (initial_env_vars)
		end

	--|--------------------------------------------------------------

	record_pid
			-- Create a new file whose name is the current PID
		local
--RFO			tf: RAW_FILE
--RFO			fn: FILE_NAME
		do
--RFO			create fn.make_from_string (Ks_pids_dir_name)
--RFO			fn.extend (process_id.out)
--RFO			create tf.make_create_read_write (fn)
--RFO			tf.close
		end

	--|--------------------------------------------------------------

	generate_peer_list
		-- Look for other agents, saving their respective PIDs in peer_pids
		local
			pid: INTEGER
--RFO			td: DIRECTORY
--RFO			ts: STRING
--RFO			ti: INTEGER
		do
			pid := process_id
			create peer_pids.make

			-- Look for all other agents to get their PIDs
			-- From the PID, find the socket
--RFO			create td.make (Ks_pids_dir_name)
--RFO			if not td.exists then
--RFO				raise ("PIDs directory is missing")
--RFO			end
--RFO			lowest_peer := ti.max_value
--RFO			highest_peer := 0
--RFO			td.open_read
--RFO			from td.readentry
--RFO			until td.lastentry = Void
--RFO			loop
--RFO				if not is_dot (td.lastentry) then
--RFO					-- PID is file name
--RFO					ts := td.lastentry
--RFO					if not ts.is_integer then
--RFO						raise ("PID file does have have a PID name.")
--RFO					end
--RFO					ti := ts.to_integer
--RFO					if ti /= pid then
--RFO						peer_pids.extend (ti)
--RFO						lowest_peer := lowest_peer.min (ti)
--RFO						highest_peer := highest_peer.max (ti)
--RFO					end
--RFO				end
--RFO				td.readentry
--RFO			end
--RFO			td.close
		ensure
			peers_exist: peer_pids /= Void
		end

--|========================================================================
feature -- Core execution
--|========================================================================

	begin_execution
			-- Begin execution of this application (e.g. begin main
			-- event loop)
		do
			process_requests_from_server
		end

--|========================================================================
feature -- I/O Connector routines
--|========================================================================

	put_string (v: STRING)
			-- Put string 'v' to connector output
		deferred
		end

	--|--------------------------------------------------------------

	read_input_stream
			-- Read input stream
			-- Set input_stream_state to reflect result of read action
			-- Read from the connector
		deferred
		end

	--|--------------------------------------------------------------

	terminate_input_stream
			-- Shut down input from connector
		deferred
		end

	--|--------------------------------------------------------------

	set_connector_exit_status (v: INTEGER)
			-- Convey exit status 'v' to the input connector
		deferred
		end

--|========================================================================
feature -- Status
--|========================================================================

	debug_is_setup: BOOLEAN
			-- Has debug (emulation) been setup already?

	is_interactive: BOOLEAN
			-- Is execution interactive?
			-- Prompts for tests, obviates recompiles per-test

	input_stream_state: INTEGER
			-- Most recent http input stream state

	input_stream_has_ended: BOOLEAN
			-- Has the HTTP input stream ended?
		do
			Result := input_stream_state = -1
		end

	login_pending: BOOLEAN
			-- A request requiring authorization is pending
		do
--			Result := http_status = K_htsc_see_other and
--				current_context /= Void and then
--				current_context.authorization_required
			Result := False
		end

	--|--------------------------------------------------------------

	ipc_thread: AEL_IPC_THREAD
			-- Thread responsible for IPC messaging

	peer_pids: LINKED_LIST [INTEGER]
			-- List of PIDs of peers at last discovery

	lowest_peer: INTEGER
			-- Peer with lowest PID

	highest_peer: INTEGER
			-- Peer with lowest PID

   has_lowest_pid: BOOLEAN
			-- Does this agent have the lowest PID of its peers?
		do
			Result := process_id < lowest_peer
		end

 --|========================================================================
feature -- IPC Support
 --|========================================================================

	check_ipc_queue
		-- Look for pending messages in the IPC inbound queue
		do
			dblogit(1, "Checking IPC queue")
		end

--|========================================================================
feature -- HTTP request processing
--|========================================================================

	process_requests_from_server
		local
			dd: like debug_driver
		do
			dblogit (2, "Before connection with input connector")
			from
			until input_stream_has_ended
			loop
				if is_interactive then
					debug_is_setup := False
					clear_errors
					prompt_for_test
				end
--RFO 				if is_emulator and not debug_is_setup then
--RFO 					if dd_filename = Void then
--RFO 						setup_for_debug (Void)
--RFO 					end
--RFO 				end
				if dd_filename /= Void then
					create dd.make (dd_filename)
					input_filename := dd.input_filename
				else
					dd := debug_driver
				end
				-- Before blocking on http input, check the IPC queue
				-- to be sure the cache and other states are up-to-date
				check_ipc_queue
				clear_response
				read_input_stream
				-- Before processing the new request, check the IPC queue
				-- to be sure the cache and other states are up-to-date
				logit (1, "After connection with input connector")
				check_ipc_queue
				if not input_stream_has_ended then
					http_status := 0
					dblogit (1, "After connection with input connector, post IPC")
					-- Increment number of times current process got a request
					call_count := call_count + 1

					set_execution_env_vars

					--RFO
					-- Create an execution context for analysis
					create current_context.make (
						execution_env_vars, call_count, input_filename, dd)
					current_context.analyze

					if has_error then
						if current_context.authorization_required and
							(not current_context.authenticated) and
							last_error_code = K_htsc_unauthorized
						 then
							 -- Error comes from unauthorized request
							 -- (normal)
							 -- Trigger login now
							 clear_errors
							 set_http_status (K_htsc_see_other, "See Other")
						else
							append_response (last_error_message)
							set_connector_exit_status (-1)
							if is_valid_http_status_code (last_error_code) then
								set_http_status (last_error_code, last_error_message)
							else
								set_http_status (K_htsc_bad_request, last_error_message)
							end
						end
					end
					if not has_error and not login_pending then
						logit (1, "Received " + current_request_method + " request")
						if is_special_request then
							process_special_request
						elseif is_get then
							process_get_request
						elseif is_put then
							process_put_request
						elseif is_delete then
							process_delete_request
						elseif is_head then
							process_head_request
						elseif is_post then
							process_post_request
						else
							-- RFO - What is it?
							-- Is this an exception?
							put_header (True)
							print_env_vars
						end
					end
					assemble_response_headers
					if debug_enabled then
						put_string (response_header)
						put_string (response_body)
					elseif has_http_error then
						put_string (response_header)
						if login_pending then
							put_string (response_body)
						end
					else
						put_string (response_header)
						put_string (response_body)
					end
					if not is_interactive then
						terminate_input_stream
					end
--RFO	multicast_ipc_message (ipc_ping_message)
--RFOusleep (20_000_000)
					reset_context
				end -- not input_stream_has_ended
			end
--RFO		rescue
				-- Perhaps some connector cleanup logic is appropriate here
--RFO			end
		end

	--|--------------------------------------------------------------

	is_special_request: BOOLEAN
			-- Is the current URI a special, built-in request?
		do
			Result := current_context.is_special_request
		end

	--|--------------------------------------------------------------

	process_special_request
			-- Request is one of a few built-ins that bypass regular
			-- processing
		require
			is_special: is_special_request
		do
			dblogit (1, "Processing special request")
			if current_uri.is_equal ("/") then
				if is_get then
					set_response_content_type ("text/html")
					response_body.append (create {MAIN_PAGE}.make)
				else
					-- Error?
				end
			elseif current_uri.is_equal ("/debugon") then
				enable_debug
				set_http_status (K_htsc_ok, "OK")
			elseif current_uri.is_equal ("/debugoff") then
				disable_debug
				set_http_status (K_htsc_ok, "OK")
			elseif current_uri.substring_index ("/echo",1) = 1 then
				dblogit (1, " Received echo request")
				put_header (True)
				print_env_vars
				set_http_status (K_htsc_ok, "OK")
			end
		end

 --|--------------------------------------------------------------

	put_context_info
		-- Write helpful context info to output stream for debugging
		do
			append_response (current_context.to_html)
		end

 --|--------------------------------------------------------------

	process_request_from_file
		require
			has_input_file: input_file /= Void
		local
			tf: like input_file
		do
			tf := input_file
			if not tf.exists then
				set_error_message ("Input file does not exist")
			else
				tf.open_read
				if not tf.is_open_read then
					set_error_message ("Cannot open input file")
				else
					tf.read_stream (tf.count)
					tf.close

					put_header (True)
					append_response (tf.last_string)
					append_response ("<br>%N")
				end
			end
		end

	--|--------------------------------------------------------------

	process_get_request
		require
			is_get: is_get
		do
			dblogit (1, "Processing a GET request")
		end

	--|--------------------------------------------------------------

	process_download_request
			-- The path-translated header of the request is the fully
			-- qualified path to the object.  Parsing of this is done
			-- during initialization of current_context.
			-- The URI for a download is the key only
			--
			-- To use GET for file download, we don't touch the files here,
			-- instead, we let lighttpd do the work directly.
			-- The client needs to build a header with:
			--   Content-Disposition: attachment; filename=<apparent_filename>
			--   X-LIGHTTPD-send-file: <actual_filename_on_disk>
		do
--RFO			do_redirect := False
			dblogit (1, "Download request for " + current_oid)
		end

	--|--------------------------------------------------------------

	process_head_request
		do
			put_header (True)
			dbprint ("Processing a HEAD request%N")
		end

	--|--------------------------------------------------------------

	process_post_request
			-- Process a POST request
			-- Used for setting attribute(s) of a target, the target should know
			-- how to handle this data. Also used for actions that PUT and DELETE
			-- could be used for, when request includes an "alternate method."
		require
			valid_context: current_context.is_valid
		local
--RFO			rq: AEL_HTTP_POST_CMD
		do
			logit (1, "Processing a POST request")
--RFO			create rq.make (current_context)
--RFO			dbprint (current_context.to_tv_pairs)
--RFO			rq.process_request
--RFO			if rq.has_error then
--RFO				dblogit("POST request had error: " + rq.error_message)
--RFO				set_http_status (rq.error_code, rq.error_message)
--RFO			else
--RFO				set_response_content_type ("text/xml")
--RFO--RFO				append_response ("Content-type: text/xml%R%N%R%N")
--RFO				-- TODO, send back an OK response to drive updates, etc
--RFO				-- It appears the server does this as long as we don't
--RFO				-- send back an error ????
--RFO			end
		end

 --|------------------------------------------------------------------------

	process_put_request
			-- Process a PUT request
			-- Used for file uploads to a server and for Area creation
		require
			valid_context: current_context.is_valid
		local
--RFO			rq: AEL_HTTP_PUT_CMD
		do
			logit (1, "Processing a PUT request")
--RFO			create rq.make (current_context)
--RFO			dbprint (current_context.to_tv_pairs)
--RFO			rq.process_request
--RFO			if rq.has_error then
--RFO				dblogit("PUT request had error: " + rq.error_message)
--RFO				set_http_status (rq.error_code, rq.error_message)
--RFO			else
--RFO				set_response_content_type ("text/xml")
--RFO--RFO				append_response ("Content-type: text/xml%R%N%R%N")
--RFO				-- TODO, send back an OK response to drive updates, etc
--RFO				-- It appears the server does this as long as we don't
--RFO				-- send back an error ????
--RFO			end
		end

 --|------------------------------------------------------------------------

	process_delete_request
			-- DELETE specifies a URI to be deleted
		local
--RFO			rq: AEL_HTTP_DELETE_CMD
		do
			logit (1, "Processing a DELETE request")
			dbprint ("Processing a DELETE request%N")

--RFO			create rq.make (current_context)
--RFO			rq.process_request
--RFO			if rq.has_error then
--RFO				-- TODO, send back an error response
--RFO			else
--RFO				set_response_content_type ("text/xml")
--RFO--RFO				append_response ("Content-type: text/xml%R%N%R%N")
--RFO				-- TODO, send back an OK response to drive updates, etc
--RFO				-- It appears the server does this as long as we don't
--RFO				-- send back an error ????
--RFO			end
		end

 --|------------------------------------------------------------------------

	put_header (cf: BOOLEAN)
		do
			append_response (
				"<title>Eiffel FastCGI</title>%N<h1>Hello from " +
				application_name + "!</h1>")

			-- For debug; remove for real use
			if cf then
				put_context_info
			end
		end

 --|------------------------------------------------------------------------

	print_env_vars
			-- Put all known env variables to stdout
		do
			append_response (env_vars_out)
		end

 --|------------------------------------------------------------------------

	env_vars_out: STRING
			-- Put all known env variables to stdout
		local
			l_table: like initial_env_vars
		do
			create Result.make (2048)
			Result.append ("<h1>Session environment variables</h1>%N")
			Result.append ("CONTENT_LENGTH=" + current_content_length.out +
				"<br>%N")
			l_table := execution_env_vars
			from
				l_table.start
			until
				l_table.after
			loop
				Result.append (l_table.key_for_iteration + "=" +
					l_table.item_for_iteration + "<br>%N")
				l_table.forth
			end
			Result.append ("<br>%N")
			Result.append ("<h1>Initial environment variables</h1>%N")
			l_table := initial_env_vars
			from
				l_table.start
			until
				l_table.after
			loop
				Result.append (l_table.key_for_iteration +
					"=" + l_table.item_for_iteration + "<br>%N")
				l_table.forth
			end
			Result.append ("<br>%N")
		end

 --|------------------------------------------------------------------------

	initial_env_vars: HASH_TABLE [STRING, STRING]
	execution_env_vars: like initial_env_vars
	debug_env_vars: like initial_env_vars

	update_environment_variables (et: like initial_env_vars)
			-- Update the given table of environment variables
			-- with the values from the current environment
			-- Should have no impact on initial env vars
		require
			table_exists: et /= Void
		local
			ex: EXECUTION_ENVIRONMENT
		do
			create ex
			if attached ex.starting_environment as vars then
				from
					vars.start
				until
					vars.after
				loop
					et.force ( vars.item_for_iteration, vars.key_for_iteration)
					vars.forth
				end
			end
		end

	--|--------------------------------------------------------------

	clear_debug_env_vars
			-- Delete all of current debug environment variables, if any
		local
			vl: like debug_env_vars
		do
			vl := debug_env_vars
			if vl /= Void and then not vl.is_empty then
				from vl.start
				until vl.after
				loop
					xenv.put ("", vl.key_for_iteration)
					vl.forth
				end
			end
		end

 --|------------------------------------------------------------------------

	env_var_pair_to_tuple (a_var: STRING): TUPLE [value: STRING; key: STRING]
			-- Given an environment variable `a_var' in form of "key=value",
			-- return separated key and value.
			-- Return Void if `a_var' is in incorrect format.
		require
			a_var_attached: a_var /= Void
		local
			i, j: INTEGER
			done: BOOLEAN
		do
			j := a_var.count
			from
				i := 1
			until
				i > j or done
			loop
				if a_var.item (i) = '=' then
					done := True
				else
					i := i + 1
				end
			end
			if i > 1 and then i < j then
				Result := [a_var.substring (i + 1, j), a_var.substring (1, i - 1)]
			end
		end

 --|------------------------------------------------------------------------

	call_count: INTEGER
			-- Number of times this agent received requests

	input_filename: STRING
			-- Name of optional input file

	debug_driver: AEL_HTTP_DEBUG_DRIVER

	dd_filename: STRING
			-- Name of optional debug driver file

--|========================================================================
feature -- Execution Context
--|========================================================================

	current_context: AEL_HTTP_EXECUTION_CONTEXT

 --|--------------------------------------------------------------

	current_request_method: STRING
		do
			if current_context = Void then
				Result := "GET"
			else
				Result := current_context.request_method
			end
		end

	is_get: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.is_get
			end
		end

	is_post: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.is_post
			end
		end

	is_put: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.is_put
			end
		end

	is_delete: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.is_delete
			end
		end

	is_head: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.is_head
			end
		end

	--|--------------------------------------------------------------

	current_uri: STRING
		do
			if current_context /= Void then
				Result := current_context.uri_string
			end
		end

	current_uri_target_type: INTEGER
		do
			if current_context /= Void then
				Result := current_context.uri_target_type
			end
		end

--RFO	current_uri_target: ANY
--RFO		do
--RFO			if current_context /= Void then
--RFO				Result := current_context.uri_target
--RFO			end
--RFO		end

	current_content_length: INTEGER
		do
			if current_context /= Void then
				Result := current_context.content_length
			end
		end

	current_query_string: AEL_HTTP_QUERY_STRING
		do
			if current_context /= Void then
				Result := current_context.query_string
			end
		end

	has_parameters: BOOLEAN
		do
			if current_context /= Void then
				Result := current_context.has_parameters
			end
		end

	current_parameters: LINKED_LIST [AEL_HTTP_URI_PARAM]
		do
			if current_context /= Void then
				Result := current_context.parameters
			end
		end

	current_path: STRING
		do
			if current_context /= Void then
				Result := current_context.path_translated
			end
		end

	current_oid: STRING
		do
			if current_context /= Void then
--RFO				Result := current_context.object_id
			end
		end

	input_file: RAW_FILE
		do
			if current_context /= Void then
				Result := current_context.input.input_file
			end
		end

 --|--------------------------------------------------------------

	has_query_string: BOOLEAN
		local
			qs: like current_query_string
		do
			qs := current_query_string
			Result :=  qs /= Void and then not qs.is_empty
		end

--|========================================================================
feature -- Error status
--|========================================================================

	has_error: BOOLEAN
		do
			if attached current_context as ctx then
				Result := ctx.has_error
			end
			if not Result then
				Result := Precursor
			end
		end

	last_error: AEL_SPRT_ERROR_INSTANCE
		do
			if attached current_context as ctx then
				Result := ctx.last_error
			end
			if Result = Void then
				Result := Precursor
			end
		end

	last_error_message: STRING
		do
			if attached current_context as ctx then
				Result := ctx.last_error_message
			end
			if Result = Void then
				Result := Precursor
			end
		end

	last_error_code: INTEGER
		do
			if attached current_context as ctx then
				Result := ctx.last_error_code
			end
			if Result = 0 then
				Result := Precursor
			end
		end

	set_error (v: INTEGER; msg: STRING)
		do
			if attached current_context as ctx then
				ctx.set_error (v, msg)
			end
			Precursor (v, msg)
		end

	set_error_message (msg: STRING)
		do
			if attached current_context as ctx then
				ctx.set_error_message (msg)
			end
			Precursor (msg)
		end

	clear_errors
		do
			if attached current_context as ctx then
				ctx.clear_errors
			end
			Precursor
		end

--|========================================================================
feature -- Support
--|========================================================================

	is_rh_builtin (v: STRING): BOOLEAN
			-- Is the given string the same object as the builtin
			-- response header?
		do
			Result := v = private_response_header
		end

	is_rb_builtin (v: STRING): BOOLEAN
			-- Is the given string the same object as the builtin
			-- response body?
		do
			Result := v = private_response_body
		end

--|========================================================================
feature -- Environment variable support
--|========================================================================

	set_execution_env_vars
		do
			create execution_env_vars.make (40)
			update_environment_variables (execution_env_vars)
		end

	reset_execution_env_vars
		local
			ev: like initial_env_vars
			oc: CURSOR
		do
			clear_debug_env_vars
			ev := initial_env_vars
			oc := ev.cursor
			from ev.start
			until ev.after
			loop
				xenv.put (ev.item_for_iteration, ev.key_for_iteration)
				ev.forth
			end
			if ev.valid_cursor (oc) then
				ev.go_to (oc)
			end
		end

 --|--------------------------------------------------------------

	xenv_var (v: STRING): STRING
			-- Execution environment variable by the given tag
		require
			exists: v /= Void and then not v.is_empty
			env_vars_exist: execution_env_vars /= Void
		do
			Result := execution_env_vars.item (v)
		end

--|========================================================================
feature -- I/O Routines
--|========================================================================

	put_debug (v: STRING)
			-- Write a debug message to ouput stream
		local
			ll: LIST [STRING]
		do
			ll := v.split ('%N')
			from ll.start
			until ll.exhausted
			loop
--				append_response ("<p><b>DEBUG:</b> " + ll.item + "%N")
				append_response ("DEBUG: " + ll.item + "%R%N")
				ll.forth
			end
		end

	print_debug (v: STRING)
			-- Write debug message directly to stdout
		do
			print (v)
		end

 --|--------------------------------------------------------------

	append_response (v: STRING)
		do
			response_body.append (v)
		end

 --|--------------------------------------------------------------

	response_body: STRING
			-- Body of response (exclusive of headers)
		do
			Result := private_temp_response_body
			if Result = Void then
				Result := private_response_body
			end
		end

	--|--------------------------------------------------------------

	clear_response
			-- Clear contents of response body
		do
			private_temp_response_body := Void
			private_response_body.wipe_out
			private_temp_response_header := Void
			private_response_header.wipe_out
		ensure
			empty: response_body /= Void and then response_body.is_empty
		end

	--|--------------------------------------------------------------

	set_response_body (v: STRING)
			-- Set the reponse body object to be 'v'
		require
			exists: v /= Void
			not_current: not is_rb_builtin (v)
		do
			private_temp_response_body := v
		ensure
			is_set: response_body = v
		end

	set_response_header (v: STRING)
			-- Set the reponse header object to be 'v'
		require
			exists: v /= Void
			not_current: not is_rh_builtin (v)
		do
			private_temp_response_header := v
		ensure
			is_set: response_header = v
		end

	--|--------------------------------------------------------------

	set_response_status (v: STRING)
			-- Set the text of the Status header to 'v'
		do
			response_status := v
		end

	set_response_cache_control (v: STRING)
			-- Set the value to use for the Cache-Control header to 'v'
		do
			response_cache_control := v
		end

	set_content_disposition (v: STRING)
			-- Set the value to use for the Content-Disposition header to 'v'
		do
			content_disposition := v
		end

	set_response_content_type (v: STRING)
			-- Set the value to use for the Content-Type header to 'v'
		require
			exists: v /= Void and then not v.is_empty
		do
			response_content_type := v
		end

	set_response_content_length (v: INTEGER)
			-- Set the value to use for the Content-Length header to 'v'
		do
			response_content_length := v
		end

	set_redirect_file (v: STRING)
			-- Set the value to use for the X-LIGHTTPD-send-file header to 'v'
		do
			response_redirect_file := v
		end

	--|--------------------------------------------------------------

	set_http_status (v: INTEGER; msg: STRING)
			-- Set the value to use for the Status header to 'v'
		require
			valid_status: is_valid_http_status_code (v)
		do
			http_status := v
			if msg /= Void and then not msg.is_empty then
				http_status_msg := msg
			elseif v = K_htsc_ok then
				http_status_msg := "OK"
			else
				http_status_msg := Void
			end
		end

	--|--------------------------------------------------------------

	response_header: STRING
			-- Full text of assembled response headers
		do
			Result := private_response_header
		end

	response_status: STRING
	response_content_type: STRING
	response_content_length: INTEGER
	response_cache_control: STRING
	response_redirect_file: STRING
	content_disposition: STRING

	http_status: INTEGER
			-- Status code to return to the http server for this request

	http_status_msg: STRING
			-- Message accompanying http status (may be Void)

 	has_http_error: BOOLEAN
			-- Has an http error status been set?
		do
			Result := not (http_status = 0 or http_status = K_htsc_ok)
		end

	--|--------------------------------------------------------------

	application_specific_headers: STRING
			-- Application-specific header extensions
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------


	set_response_from_cmd (cmd: AEL_HTTP_CMD)
			-- Set the values for the response headers from the
			-- command 'cmd'
		require
			exists: cmd /= Void
			has_run: cmd.has_executed or cmd.has_error
		do
			set_response_body (cmd.response_body)
			set_response_status (cmd.response_status)
			set_response_content_type (cmd.response_content_type)
			set_http_status (cmd.http_status_code, cmd.http_status_msg)
		end

	--|--------------------------------------------------------------

	assemble_response_headers
			-- Assemble the individual headers for the response
		do
			if has_error then
				http_status := last_error_code
			end
			if has_http_error then
				response_header.append ("Status: ")
				response_header.append_integer (http_status)
				if http_status_msg /= Void then
					response_header.extend (' ')
					response_header.append (http_status_msg)
				end
				response_header.append ("%R%N")
			else -- no http error
				if response_status /= Void then
					response_header.append (response_status)
					response_header.append ("%R%N")
				else
					response_header.append ("Status: 200 OK%R%N")
				end
				if response_cache_control /= Void then
					response_header.append ("Cache-Control: ")
					response_header.append (response_cache_control)
					response_header.append ("%R%N")
				end
				if content_disposition /= Void then
					response_header.append ("Content-Disposition: ")
					response_header.append (content_disposition)
					response_header.append ("%R%N")
				end
				if response_redirect_file /= Void then
					response_header.append ("X-LIGHTTPD-send-file: ")
					response_header.append (response_redirect_file)
					response_header.append ("%R%N")
				end
				response_header.append ("Content-Type: ")
				response_header.append (response_content_type)
				response_header.append ("%R%N")
				if not response_content_type.starts_with ("text") then
					--RFO TODO it seems this method of calculating
					-- content length is wrong and that the http
					-- server fills in the value if we don't in most
					-- cases
					--JAH Firefox and curl seem to miss some of the
					-- content when we DO specify a content length.
					-- For now, we'll only set content length if
					-- the content_type is _not_ "text/html"
					--JAH	Caching may have also had something to do
					-- with the previously noted issue, so we've
					-- disabled caching in the http server config

					if response_content_length = 0 then
						set_response_content_length (response_body.count)
					end
				end
			end
			response_header.append (application_specific_headers)
			response_header.append ("%R%N")
		ensure
			header_assembled: not response_header.is_empty
		end

--|========================================================================
feature {NONE} -- Private implementation
--|========================================================================

	private_response_body: STRING
			-- Full text of response, less headers

	private_temp_response_body: STRING
			-- Full text of response, less headers
			-- Temporary (from cmds, for example), helps eliminate extra
			-- text copying

	private_response_header: STRING
			-- Full text of assembled response headers

	private_temp_response_header: STRING
			-- Full text of assembled response headers
			-- Temporary (from cmds, for example), helps eliminate extra
			-- text copying

--|========================================================================
feature {NONE} -- Debug support
--|========================================================================

	test_uris: TWO_WAY_LIST [STRING]

	interactive_state: INTEGER

	K_ist_initial: INTEGER = 0
	K_ist_inline: INTEGER = 1
	K_ist_driver: INTEGER = 2
	K_ist_builtin: INTEGER = 3

	interactive_ok_to_quit: BOOLEAN

	prompt_for_test
			-- Prompt the user to input the next test to execute
		local
			ts, ms: STRING
			test_chosen: BOOLEAN
			vl: TWO_WAY_LIST [AEL_SPRT_TV_PAIR]
			tu: AEL_HTTP_URI
		do
			from
			until test_chosen or interactive_ok_to_quit
			loop
				test_chosen := False
				if interactive_state = K_ist_initial then
					prompt_for_interaction_state
				end
				if not interactive_ok_to_quit then
					inspect interactive_state
					when K_ist_inline then
						if test_uris = Void then
							create test_uris.make
						end
						io.put_string ("%N----- In-line URI Test -----%N")
						io.put_string ("Please enter the URI you want to test%N> ")
						io.read_line
						ts := io.last_string
						io.new_line
						if ts.is_empty then
							-- Try again
						elseif ts ~ "?" then
							-- Show help message
							show_interactive_help
						elseif ts ~ "^" or ts ~ "<" then
							interactive_state := K_ist_initial
						elseif ts.as_lower ~ "q" then
							interactive_ok_to_quit := True
						else
							if ts.item (1) /= '/' then
								if ts.as_upper.starts_with ("PUT ") then
									asr.crop_start (ts, 4)
									ms := "PUT"
								elseif ts.as_upper.starts_with ("POST ") then
									asr.crop_start (ts, 5)
									ms := "POST"
								elseif ts.as_upper.starts_with ("DELETE ") then
									asr.crop_start (ts, 7)
									ms := "DELETE"
								elseif ts.as_upper.starts_with ("GET ") then
									asr.crop_start (ts, 4)
									ms := "GET"
								else
									io.put_string ( ts +
										" is not a valid URI. Please try again%N")
								end
							end
							create tu.make
							if not tu.is_valid_uri (ts) then
								io.put_string ( ts +
									" is not a valid URI. Please try again%N")
							else
								vl := uri_to_env_vars (ts, ms)
								set_debug_env_vars (vl)
								test_chosen := True
							end
						end
					when K_ist_driver then
						io.put_string ("%N----- Driver File Test -----%N")
						io.put_string ("Please enter the path of the driver file%N> ")
						io.read_line
						ts := io.last_string
						io.new_line
						if ts.is_empty then
							-- Try again
						elseif ts ~ "?" then
							-- Show help message
							show_interactive_help
						elseif ts ~ "^" or ts ~ "<" then
							interactive_state := K_ist_initial
						elseif ts.as_lower ~ "q" then
							interactive_ok_to_quit := True
						else
							dd_filename := ts
							test_chosen := True
						end
					when K_ist_builtin then
						io.put_string ("%N----- Built-in Test -----%N")
						io.put_string ("Please enter the name of the built-in test%N> ")
						io.read_line
						ts := io.last_string
						io.new_line
						if ts.is_empty then
							-- Try again
						elseif ts ~ "?" then
							-- Show help message
							show_interactive_help
						elseif ts ~ "^" or ts ~ "<" then
							interactive_state := K_ist_initial
						elseif ts.as_lower ~ "q" then
							interactive_ok_to_quit := True
						else
							setup_for_debug (ts)
							test_chosen := True
						end
					end -- No else case in inspect; should raise exception
				end
			end
			if interactive_ok_to_quit then
				io.put_string ("%NDone%N")
				die (0)
			end
			debug_is_setup := True
		end

	--|--------------------------------------------------------------

	prompt_for_interaction_state
			-- Prompt the user to input the interactive state to use
		local
			ts: STRING
			selection_ok: BOOLEAN
		do
			from
			until selection_ok or interactive_ok_to_quit
			loop
				io.put_string ("{
--------------------------------------------------
Please select the debug mode.
  1. In-line URI (default)
  2. Driver file
  3. Built-in test
  'q' to quit
> 1
}")
				io.read_line
				ts := io.last_string
				inspect ts.count
				when 0 then
					-- Default mode
					selection_ok := True
					interactive_state := K_ist_inline
				when 1 then
					if ts.item (1).is_digit then
						inspect ts.item (1) |-| '0'
						when K_ist_inline, K_ist_driver, K_ist_builtin then
							selection_ok := True
							interactive_state := ts.item (1) |-| '0'
						else
							io.put_string ("ERROR: Selection is not in range.%N")
						end
					elseif ts.item (1).as_lower = 'q' then
						interactive_ok_to_quit := True
					elseif ts.item (1).as_lower = '?' then
						show_interactive_help
					else
						io.put_string ("ERROR: selection must be a number.%N")
					end
				else
					-- ERROR, not a valid response
					io.put_string ("ERROR: selection must be a digit.%N")
				end
			end
		end

	--|--------------------------------------------------------------

	show_interactive_help
		do
			inspect interactive_state
			when K_ist_initial then
				io.put_string ("{
--------------------------------------------------
Interactive Mode Help

There are 3 test modes for interactive operation.
  1. In-line URI (default)
  2. Driver file
  3. Built-in test

At the initial prompt, you select the interactive mode to use.
At that point, the per-mode prompt appears, and will appear as
long as you don't quit, or decide to return to the mode menu.

To quit, simply enter 'q'
To return to the mode menu from one of the modes, enter '<' or '^'
To see mode-specific help, enter '?' while in one of the modes

In In-line mode, you can enter a URI to be processed, optionally
preceded by a request method.

In Driver file mode, you can enter the full pathname of a debug
driver file to use.

In Built-in mode, you can enter the name of a known built-in test.
}")
			when K_ist_inline then
				io.put_string ("{
--------------------------------------------------
Interactive Mode Help - In-line Mode

In In-line mode, you can enter a URI to be processed.  The URI
should not include the protocol or host parts (they are known).
For example, the full URI "http://my_host/my_path" would need
only "/my_path" for the interactive test.

The URI is subject to CGI escaping rules, so if you want to use
a '+' character, for example, you must use the escaped value
("%2b") instead, lest the '+' be interpreted as an escaped blank.

The default request method in this mode is GET.  You can optionally
set a different request method by preceding the URI with the method
string and blank separator.
For example, to do a PUT, you might enter the following:
PUT /foo/bar
To test a PUT request to create object 'bar' under container 'foo'.

You can also enter 'q' at the prompt to quit the application.
At each prompt, you can also request help by entering '?'.

To return to the mode selection menu, enter '<' or '^'
}")
			when K_ist_driver then
				io.put_string ("{
--------------------------------------------------
Interactive Mode Help - Driver Mode

In Driver file mode, you can enter the full pathname of a debug
driver file to use.

You can also enter 'q' at the prompt to quit the application.
At each prompt, you can also request help by entering '?'.

To return to the mode selection menu, enter '<' or '^'
}")
			when K_ist_builtin then
				io.put_string ("{
--------------------------------------------------
Interactive Mode Help - Built-in Mode

In Built-in mode, you can enter the name of a known built-in test.
If you enter a blank line, the default test is selected.
If the test name is not know, an error displays and you can try again.

You can also enter 'q' at the prompt to quit the application.
At each prompt, you can also request help by entering '?'.

To return to the mode selection menu, enter '<' or '^'
}")
			end -- no else case
			io.put_string ("%NPress Enter to continue%N")
			io.read_line
			io.put_string ("%N%N")
		end

	--|--------------------------------------------------------------

	setup_for_debug (arg: STRING)
		local
			tt: AEL_HTTP_APP_TEST
		do
			debug_is_setup := True
			if arg /= Void then
				tt := built_in_tests.item (arg)
				if tt = Void then
					tt := built_in_tests.item ("default")
				end
				if tt.is_interactive then
					tt.prompt_for_content
				end
				set_debug_env_vars (tt.variables)
				input_filename := tt.input_filename
				debug_driver := tt.driver
			end
		end

	--|--------------------------------------------------------------

	set_debug_env_vars (vl: TWO_WAY_LIST [AEL_SPRT_TV_PAIR])
		require
			exists: vl /= Void
		local
			oc: CURSOR
		do
			if vl.is_empty then
				set_error_message ("Environment variables missing from test")
			else
				-- Add to the shell-created environment variables
				-- those from the chosen test set to mimic them
				-- coming from the server.
				-- Input connector will clobber this on each successive
				-- call
				create debug_env_vars.make (vl.count)
				oc := vl.cursor
				from vl.start
				until vl.exhausted
				loop
					debug_env_vars.extend (vl.item.value, vl.item.tag)
					xenv.put (vl.item.value, vl.item.tag)
					vl.forth
				end
				if vl.valid_cursor (oc) then
					vl.go_to (oc)
				end
			end
		end

	--|--------------------------------------------------------------

	uri_to_env_vars (v, ms: STRING): TWO_WAY_LIST [AEL_SPRT_TV_PAIR]
		require
			exists: v /= Void and then not v.is_empty
		local
			tu: AEL_HTTP_URI
			method_str: STRING
		do
			create Result.make
			if ms = Void then
				method_str := "GET"
			else
				method_str := ms
			end
			create tu.make_from_string (v)
			Result.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
					"REQUEST_URI", v))
			Result.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
					"REQUEST_METHOD", method_str))
			Result.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
					"HTTP_HOST", dummy_host_addr))
			Result.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
					"CONTENT_TYPE", "text/xml"))
			Result.extend (
				create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
					"CONTENT_LENGTH", "0"))
			if tu.query_string /= Void then
				Result.extend (
					create {AEL_SPRT_TV_PAIR}.make_from_tag_and_value (
						"QUERY_STRING", tu.query_string))
			end
		ensure
			exists: Result /= Void
			at_least_uri: not Result.is_empty
		end


	--|--------------------------------------------------------------

	add_built_in_test (tst: AEL_HTTP_APP_TEST)
		require
			test_exists: tst /= Void
		do
			if not built_in_tests.has (tst.id) then
				built_in_tests.extend (tst, tst.id)
			end
			ensure
				added: built_in_tests.has (tst.id)
		end

	--|--------------------------------------------------------------

	built_in_tests: HASH_TABLE [AEL_HTTP_APP_TEST, STRING]
		once
			create Result.make (11)
			Result.extend (
				create {AEL_HTTP_APP_TEST}.make_with_variables (
					"echotest",
					<<
					<<"/echotest", "REQUEST_URI">>,
					<<"0", "CONTENT_LENGTH">>,
					<<dummy_host_addr, "HTTP_HOST">>,
					<<"GET", "REQUEST_METHOD">>,
					<<"0000.12345678", "HTTP_AUTHORIZATION">>
					>>), "echotest")
			Result.extend (
				create {AEL_HTTP_APP_TEST}.make_with_variables (
					"posttest",
					<<
					<<"/posttest", "REQUEST_URI">>,
					<<"0", "CONTENT_LENGTH">>,
					<<dummy_host_addr, "HTTP_HOST">>,
					<<"POST", "REQUEST_METHOD">>,
					<<dummy_auth_string, "HTTP_AUTHORIZATION">>
					>>), "posttest")
			Result.extend (
				create {AEL_HTTP_APP_TEST}.make_with_variables (
					"default",
					<<
					<<"/defaulttest/mytest;parameter1", "REQUEST_URI">>,
					<<"0", "CONTENT_LENGTH">>,
					<<dummy_host_addr, "HTTP_HOST">>,
					<<"GET", "REQUEST_METHOD">>,
					<<dummy_auth_string, "HTTP_AUTHORIZATION">>
					>>), "default")
		ensure
			exists: Result /= Void and then not Result.is_empty
			has_default: Result.has ("default")
		end

	--|--------------------------------------------------------------

	dummy_host_addr: STRING
		do
			Result := "192.168.1.6"
		end

	dummy_auth_string: STRING
		do
			Result := "aelhttp Og=="
		end

end -- class AEL_HTTP_SERVER_APPLICATION
