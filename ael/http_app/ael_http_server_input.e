class AEL_HTTP_SERVER_INPUT

inherit
	AEL_HTTP_STATUS_CODES
	AEL_SPRT_MULTI_FALLIBLE
	MEMORY
		redefine
			dispose
		end

create
	make

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	make (fn: STRING_8; dd: AEL_HTTP_DEBUG_DRIVER)
			-- Initialize `Current'.
		local
			ifn: STRING
		do
			create private_input_buffer.make (1024)
			ifn := fn
			if dd /= Void then
				if dd.input_filename /= Void then
					ifn := dd.input_filename
				end
				private_input_buffer.append_string (dd.content)
			end
			if ifn /= Void and then not ifn.is_empty then
				make_input_file (fn)
			end
		ensure
			input_open: input_file /= Void implies input_file.is_open_read
		end

	--|--------------------------------------------------------------

	make_input_file (fn: STRING)
		require
			name_exists: fn /= Void and then not fn.is_empty
		do
			input_filename := fn
			create input_file.make (fn)
			if not input_file.exists then
				set_error (k_htsc_bad_request, "Missing input file")
				input_file := Void
			else
				input_file.open_read
				if not input_file.is_open_read then
					input_file := Void
					set_error (k_htsc_bad_request, "Cannot open input file")
				end
			end
		ensure
			input_open: input_file /= Void implies input_file.is_open_read
		end

	--|--------------------------------------------------------------

	make_with_buffer (buf, fn: STRING_8)
			-- Initialize `Current'.
		do
			private_input_buffer := buf
			if fn /= Void and then not fn.is_empty then
				make_input_file (fn)
			end
		ensure
			input_open: input_file /= Void implies input_file.is_open_read
		end

 --|========================================================================
feature -- I/O operations
 --|========================================================================

	read_stream (n: INTEGER_32)
		require
			small_enough: n <= buffer_capacity
			not_has_error: not has_error
		do
			if input_file /= Void then
				input_file.read_stream (n)
			else
				read_from_stdin (n)
			end
		end

	--|--------------------------------------------------------------

	copy_to_file (n: INTEGER_32; f: RAW_FILE)
		require
			exists: f /= Void
			writable: f.is_open_write
		do
			if input_file /= Void then
				input_file.start
				input_file.copy_to (f)
			else
				-- This copy is unavoidable, as it creates the
				-- actual new file
				copy_from_stdin (n, f)
			end
		end

	--|--------------------------------------------------------------

	read_from_stdin (n: INTEGER)
			-- Read up to n bytes from stdin and store in the input
			-- buffer
		do
			copy_from_stdin (n, Void)
		end

 --|--------------------------------------------------------------

	copy_from_stdin (n: INTEGER; f: FILE)
			-- Read up to n bytes from stdin
			-- Optionally write to given file 'f' (if non-void)
		require
			valid_count: n > 0
			file_open: f /= Void implies f.is_open_write or f.is_open_append
		local
			num, readsize, writecount: INTEGER
			done: BOOLEAN
		do
			readsize := n.min (1024)
			from
			until done or writecount >= n
			loop
				io.read_stream (readsize)
				num := io.last_string.count
				if num  = 0 then
					-- EOF
					done := True
				else
					private_input_buffer.append (io.last_string)
					if f /= Void then
						f.put_string (io.last_string)
					end
					writecount := writecount + num
				end
			end
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	input_filename: STRING_8
	input_file: RAW_FILE

	buffer_contents: STRING
		do
			Result := private_input_buffer
		end

	buffer_length: INTEGER
		do
			Result := private_input_buffer.count
		end

	buffer_capacity: INTEGER
		do
			Result := private_input_buffer.capacity
		end

	last_string: STRING_8
			-- Last string read from input
		do
			if input_file /= Void then
				Result := input_file.last_string
			else
				Result := buffer_contents
			end
		ensure then
			exists: Result /= Void
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	private_input_buffer: STRING

	--|--------------------------------------------------------------

	dispose
			-- Action to be executed just before garbage collection
			-- reclaims an object.
			-- Effect it in descendants to perform specific dispose
			-- actions. Those actions should only take care of freeing
			-- external resources; they should not perform remote calls
			-- on other objects since these may also be dead and reclaimed.
		do
			if input_file /= Void and then input_file.is_open_read then
				input_file.close
			end
			Precursor
		end

end -- class AEL_HTTP_AGENT_INPUT

