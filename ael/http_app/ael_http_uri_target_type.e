class AEL_HTTP_URI_TARGET_TYPE
-- Encapsulation of characteristics of URI target types

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (i: INTEGER; lb: STRING)
		do
			index := i
			label := lb
		end

--|========================================================================
feature -- Status
--|========================================================================

	index: INTEGER
	label: STRING

	--|--------------------------------------------------------------
invariant
	has_label: label /= Void and then not label.is_empty

end -- class AEL_HTTP_URI_TARGET_TYPE
