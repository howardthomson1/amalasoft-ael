class AEL_HTTP_URI_TARGET_TYPES

--|========================================================================
feature -- URI Target types
--|========================================================================

	K_tt_undefined: INTEGER = 0
	K_tt_top: INTEGER = -1

	-- Application context should define others in URI_TARGET_TYPES
	-- class that is defined for that application

	valid_target_types: AEL_DS_HASH_TABLE [AEL_HTTP_URI_TARGET_TYPE, INTEGER]
		once
			create Result.make (79)
			Result.extend (
				create {AEL_HTTP_URI_TARGET_TYPE}.make (K_tt_undefined, ""),
				K_tt_undefined)
			Result.extend (
				create {AEL_HTTP_URI_TARGET_TYPE}.make (K_tt_top, "/"),
				K_tt_top)
		end

	--|--------------------------------------------------------------

	oid_target_types: AEL_DS_HASH_TABLE [INTEGER, STRING]
			-- URI target types for specific first-field OIDs
		once
			create Result.make (29)
		end

	--|--------------------------------------------------------------

	oid_to_target_type (v: STRING): INTEGER
		require
			exists: v /= Void
		do
			if oid_target_types /= Void then
				Result := oid_target_types.item (v)
			end
		end

end -- class AEL_HTTP_URI_TARGET_TYPES

