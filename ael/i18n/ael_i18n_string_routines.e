--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| String manipulation and inspection routines
--|----------------------------------------------------------------------

class AEL_I18N_STRING_ROUTINES

inherit
	AEL_SPRT_STRING_ROUTINES

create
	default_create

--|========================================================================
feature -- Encoding
--|========================================================================

	string_url_decoded (v: STRING): STRING
		-- The URL-encoded string 'v' decoded
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			c: CHARACTER
			pr: INTEGER_REF
		do
			create Result.make (v.count + v.count // 10)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				inspect c
				when '+' then
					Result.extend (' ')
				when '%%' then
					-- An escaped character ?
					if i = lim then
						Result.extend (c)
					else
						create pr
						pr.set_item (i)
						Result.append (url_decoded_char (v, pr))
						i := pr.item
					end
				else
					Result.extend (c)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	url_decoded_char (buf: STRING; posr: INTEGER_REF): STRING
		-- Character(s) resulting from decoding the URL-encoded string
		require
			stream_exists: buf /= Void
			posr_exists: posr /= Void
			valid_start: posr.item <= buf.count
		local
			c: CHARACTER
			i, lim, nb: INTEGER
			not_a_digit: BOOLEAN
			ascii_pos, ival: INTEGER
			pos, hexlen: INTEGER
			u8_str: AEL_I18N_UTF8_CHAR
			hexstr: STRING
		do
			-- pos is index in stream of escape character ('%')
			pos := posr.item
			create Result.make (4)
			if buf.item (pos + 1) = 'u' then
				-- An escaped Unicode (ucs2) value, from ECMA scripts
				-- Has the form: %u<n> where <n> is the UCS value
				-- of the character (two byte integer, one to 4 chars
				-- after escape sequence).
				-- UTF-8 result can be 1 to 4 characters
				lim := buf.count
				from i := pos + 2
				until (i > lim) or not_a_digit
				loop
					c := buf.item (i)
					if c.is_hexa_digit then
						ival := ival * 16
						if c.is_digit then
							ival := ival + (c |-| '0')
						else
							ival := ival + (c.upper |-| 'A') + 10
						end
						i := i + 1
					else
						not_a_digit := True
					end
				end
				posr.set_item (i)
				-- ival is now UCS2 value; needs conversion to UTF8
				create u8_str.make_from_ucs_code (ival)
				Result.append (u8_str)
				nb := utf8_bytes_in_sequence (buf, pos)
			else
				-- ASCII char?
				-- Conversion to hex can have multiple leading zeroes
				hexlen := (lim.to_hex_string).count
				hexstr := buf.substring (pos + 1, pos + hexlen)
				ascii_pos := hex_substring_to_integer (buf, pos + 1, pos + hexlen)
				--ascii_pos := hex_to_ascii_pos (
				--buf.item (pos + 1), buf.item (pos + hexlen - 1))
				if ascii_pos >= 0x80 and ascii_pos <= 0xff then
					-- Might be improperly escaped
					create u8_str.make_from_ucs_code (ascii_pos)
					Result.append (u8_str)
					nb := utf8_bytes_in_sequence (buf, pos)
					if nb = 0 then
						-- Assume an escaped Latin-1 hi-bit character  Convert it
						-- from its code (UC index value) to its UTF-8 
						-- equivalent
					else
						posr.set_item (pos + nb - 1)
					end
				else
					posr.set_item (pos + hexlen)
					Result.extend (ascii_pos.to_character_8)
				end
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- FS-safe Encoding
--|========================================================================

	string_fs_safe_encoded (v: STRING): STRING
			-- The Filesystem-safe encoded equivalent of the given string
			-- Like URL-encoding but encodes '~' also
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			create Result.make (v.count + v.count // 10)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				inspect c.lower
				when
					'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
					'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
					'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'.', '-', '_'
				 then
					 Result.extend (c)
				when ' ' then
					Result.extend ('+')
				else
					Result.append (url_encoded_char (c))
				end
				i := i + 1
			end
		ensure
			exists: Result /= Void
			arg_not_changed: v ~ (old v)
		end

	string_fs_safe_decoded (v: STRING): STRING
			-- The Filesystem-safe encoded string decoded to its 
			-- original form
		require
			exists: v /= Void
		do
			Result := string_url_decoded (v)
		ensure
			exists: Result /= Void
			arg_not_changed: v ~ (old v)
		end

	--|--------------------------------------------------------------

	is_utf8_extension_byte (v: NATURAL_8): BOOLEAN
		do
			Result := (v & 0xc0) = 0x80
		end

	--|--------------------------------------------------------------

	is_utf8_extension_char (c: CHARACTER): BOOLEAN
		do
			Result := is_utf8_extension_byte(c.code.to_natural_8)
		end

	--|--------------------------------------------------------------

	utf8_bytes_in_sequence (s: STRING; spos: INTEGER): INTEGER
			--	If the given character is a legal first byte element in a
			-- utf8 byte sequence (aka character), then return the number
			-- of bytes in that sequence
			--	Result of zero means it's not a utf8 first byte
		require
			exists: s /= Void
			long_enough: s.count >= spos
		do
			Result := bytes_in_utf8_char (s.item (spos))
		end

	--|--------------------------------------------------------------

	string_length (v: STRING_8): INTEGER
			-- Number of characters (not bytes) in UTF-8 string 'v'
			-- Assumes valid UTF-8 string, and does not itself validate 
			-- extension bytes.
			-- A negative result denotes an invalid UTF-8 string
		local
			pos, nb, lim: INTEGER
			done: BOOLEAN
		do
			lim := v.count
			from pos := 1
			until pos > lim or done or Result < 0
			loop
				nb := bytes_in_utf8_char (v.item (pos))
				if nb = 0 then
					Result := -1
				else
					if lim < ((pos + nb) - 1) then
						Result := -1
					else
						Result := Result + 1
						pos := pos + nb
					end
				end
			end
		end

	--|--------------------------------------------------------------

	is_ascii_only (v: STRING_8): BOOLEAN
			-- Does string 'v' have ONLY single-byte (i.e. 7bit ASCII)
			-- characters?
		do
			Result := v.count = string_length (v)
		end

	--|--------------------------------------------------------------

	is_legal_utf8_character (v: STRING_8): BOOLEAN
			--	Is the given character (1 or more bytes) a LEGAL UTF-8
			--	character sequence?
			--
			------------------------------------------------------------
			-- UTF-8 Is a multi-byte encoding and looks like this:
			--
			-- bytes | bits | representation
			--     1 |    7 | 0vvv vvvv
			--     2 |   11 | 110v vvvv  10vv vvvv
			--     3 |   16 | 1110 vvvv  10vv vvvv  10vv vvvv
			--     4 |   21 | 1111 0vvv  10vv vvvv  10vv vvvv  10vv vvvv
			--
			-- According to ISO/IEC 10646-1: 1993/AMD.2: 1996 (E), the
			-- UTF-8 standard , any leading octet whose high order bit is
			-- set and which is not one of the sequences defined above is
			-- an illegal octet.
			--	"For all sequences of one octet the most significant bit
			--	 shall be a ZERO bit."
			-- This means that ALL single-byte characters in the Latin-n
			-- encodings that have values greater than 0x7f are illegal
			-- for UTF-8.
			-- Additionally, the octets 0xfe and 0xff are unused and are
			-- illegal in any position in the stream.
		require
			exists: v /= Void and then not v.is_empty
		local
			c: CHARACTER_8
			nb, i: INTEGER
		do
			if v /= Void and then not v.is_empty then
				c := v.item (1)
				nb := bytes_in_utf8_char (c)
				if nb /= 0 and v.count >= nb then
					Result := True
					from i := 2
					until i > nb or not Result
					loop
						if not char_is_utf_extension_byte (v.item (i)) then
							Result := False
						end
						i := i + 1
					end
				end
			end
		end

	--|------------------------------------------------------------------------

	is_utf_extension_byte (v: NATURAL_8): BOOLEAN
		do
			Result := (v & 0xc0) = 0x80
		end


	--|------------------------------------------------------------------------

	char_is_utf_extension_byte (c: CHARACTER_8): BOOLEAN
		do
			Result := is_utf_extension_byte(c.code.to_natural_8)
		end


	--|--------------------------------------------------------------

	is_utf_start_byte (v: CHARACTER_8): BOOLEAN
		do
			Result := bytes_in_utf8_char (v) /= 0
		end
	--|--------------------------------------------------------------

	is_utf8_char (s: STRING; nbp: INTEGER_REF): BOOLEAN
			-- Does the string begin with a legal UTF8 character?
			-- If so, then return the number of characters in the string
		require
			arg_exists: s /= Void
			ref_exists: nbp /= Void
		do
			Result := is_legal_utf8_character (s)
		end

	--|--------------------------------------------------------------

	validate_utf8_char (
		s: STRING; sp: INTEGER): TUPLE [BOOLEAN, INTEGER]
			-- Does the string 's', beginning at position 'sp', begin 
			-- with a legal UTF8 character?
			-- If so, then return the number of characters in the string
		require
			string_exists: s /= Void
		local
			lim: INTEGER
		do
			Result := [False, 0]
			if not s.is_empty then
				lim := bytes_in_utf8_char (s.item (sp))
				if lim > 0 then
					Result := [substr_is_utf8 (s, sp), lim]
				end
			end
		end

	--|--------------------------------------------------------------

	char_is_utf8 (s: STRING; len: INTEGER): BOOLEAN
		require
			arg_exists: s /= Void
		do
			if not s.is_empty then
				Result := is_legal_utf8_character (s)
			end
		end

	--|--------------------------------------------------------------

	substr_is_utf8 (s: STRING; spos: INTEGER): BOOLEAN
		require
			arg_exists: s /= Void
		do
			Result := is_legal_utf8_character (s.substring (spos, s.count.min(spos + 7)))
		end

	--|--------------------------------------------------------------

	string_is_legal_utf8 (str: STRING_8): BOOLEAN
			--	Is the given string a LEGAL UTF-8 character sequence
			-- for its full length?
		local
			i, lim, cc: INTEGER;
			c: CHARACTER_8
		do
			if str /= Void then
				Result := True
				lim := str.count
				from i := 1
				until i > lim or not Result
				loop
					c := str.item (i)
					cc := bytes_in_utf8_char (c)
					if cc = 0 then
						Result := False
					else
						if not is_complete_utf8_string (str, i) then
							Result := False
						end
						i := i + cc
					end
				end
			end
		end

	--|--------------------------------------------------------------

	is_complete_utf8_string (str: STRING_8; spos: INTEGER): BOOLEAN
			--	Is the given string, beginning at position 'spos;
			-- a COMPLETE UTF-8 character sequence?
			-- Remainder of string after a legal UTF8 sequence is
			-- not analyzed
		local
			i, lim, nb, cc: INTEGER
			c: CHARACTER_8
		do
			if str /= Void and then not str.is_empty then
				lim := str.count
				cc := number_of_bytes_in_char (str, spos)
				if cc > 0 and cc <= lim then
					Result := True
					from
						i := spos + 1
						nb := 1
					until i > lim or not Result or nb > cc
					loop
						c := str.item (i)
						if c = '%U' then
							-- UTF8 forbids embedded nulls
							Result := False
						elseif char_is_utf_extension_byte (c) then
							i := i + 1
							nb := nb + 1
						else
							Result := False
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	last_index_of_utf8_start (str: STRING_8; epos: INTEGER): INTEGER
			-- Position in 'str' at or before 'epos' at which a legal
			-- UTF8 start byte is found.  Zero if none found
		require
			exists: str /= Void
			valid_end: epos > 0 and epos <= str.count
		local
			i, lim, cc: INTEGER
		do
			lim := epos
			from i := lim
			until i = 0 or Result /= 0
			loop
				cc := number_of_bytes_in_char (str, i)
				if cc /= 0 then
					Result := i
				else
					i := i - 1
				end
			end
		end

	--|--------------------------------------------------------------

	number_of_bytes_in_char (v: STRING; spos: INTEGER): INTEGER
		require
			arg_exists: v /= Void
		do
			if not v.is_empty then
				Result := bytes_in_utf8_char (v.item (spos))
			end
		end

	--|--------------------------------------------------------------

	byte_is_utf8_start (v: CHARACTER; nbp: INTEGER_REF): BOOLEAN
		do
			Result := is_utf_start_byte (v)
		end

	--|--------------------------------------------------------------

	utf8_to_uc_code (v: STRING_8; spos: INTEGER): NATURAL_32
			--	UCS code for given UTF-8 character (byte sequence) 'v'
		require
			exists: v /= Void and then v.count >= spos
		local
			c1, c2, c3, c4: CHARACTER_8
			b1, b2, b3, b4: NATURAL_8
			has_error: BOOLEAN
			nb: INTEGER
		do
			c1 := v.item (spos)
			b1 := c1.code.to_natural_8
			if (b1 & 0x80) = 0 then
				nb := 1
			else
				if v.count < (spos + 1) then
					has_error := True
				end
			end
			if nb = 0 and not has_error then
				c2 := v.item (spos + 1)
				b2 := c2.code.to_natural_8
				if (b1 & 0xe0) = 0xc0 and then is_utf_extension_byte (b2) then
					nb := 2
				end
			end
			if nb = 0 and not has_error then
				if v.count < (spos + 2) then
					has_error := True
				else
					c3 := v.item (spos + 2)
					b3 := c3.code.to_natural_8
					if (b1 & 0xf0) = 0xe0 and then
						is_utf_extension_byte (b2) and
						is_utf_extension_byte (b3) then
						nb := 3
					end
				end
			end
			if nb = 0 and not has_error then
				if v.count < (spos + 3) then
					has_error := True
				else
					c4 := v.item (spos + 3)
					b4 := c4.code.to_natural_8
					if ((b1 & 0xf8) = 0xf0) and
						is_utf_extension_byte (b2) and
						is_utf_extension_byte (b3) and
						is_utf_extension_byte (b4) then
							nb := 4
					end
				end
			end
			inspect nb
			when 4 then
				Result := (b4 & 0x3f) |
				((b3 & 0x3f) |<< 6) |
				((b2 & 0x3f) |<< 12) |
				((b1 & 0x07) |<< 18)
			when 3 then
				Result := (b3 & 0x3f) | ((b2 & 0x3f) |<< 6) | ((b1 & 0x0f) |<< 12)
			when 2 then
				Result := (b2 & 0x3f) | ((b1 & 0x1f) |<< 6)
			when 1 then
				Result := b1
			else
			end
		end

	--|--------------------------------------------------------------

	uc_code_to_utf8 (v: INTEGER): STRING
			--	Convert the given UCS Code value into its corresponding
			--	UTF-8 character (byte sequence)
			--
			--	UTF-8 string is return via second arg, 's'
			--	Return value is length of sequence, 0 if error
			--
			------------------------------------------------------------
			-- UTF-8 Is a multi-byte encoding and looks like this:
			--
			-- bytes | bits | representation/value
			--     1 |    7 | 0vvv vvvv
			--                _vvv vvvv
			--     2 |   11 | 110v vvvv  10vv vvvv
			--                ___v vvvv  __vv vvvv
			--     3 |   16 | 1110 vvvv  10vv vvvv  10vv vvvv
			--                ____ vvvv  __vv vvvv  __vv vvvv
			--     4 |   21 | 1111 0vvv  10vv vvvv  10vv vvvv  10vv vvvv
			--                ____ _vvv  __vv vvvv  __vv vvvv  __vv vvvv
			--
			-- Example: Hebrew Alef (0xd7 0x90)
			--     2     11   1101 0111  1001 0000
			--                ___1 0111  __01 0000
			-- Compacts to:         101  1101 0000 -> 0x5d0 == Unicode value
		local
			ti: NATURAL_32
			s: STRING
		do
			create s.make (4)
			ti := v.as_natural_32
			if ti < 0x80 then
				s.extend (ti.as_natural_8.to_character_8)
			elseif ti < 0x800 then
				s.extend ((0xc0 | (ti |>> 6)).as_natural_8.to_character_8)
				s.extend ((0x80 | (ti & 0x3f)).as_natural_8.to_character_8)
			elseif ti < 0x10000 then
				s.extend ((0xe0 | (ti |>> 12)).as_natural_8.to_character_8)
				s.extend ((0x80 | ((ti |>> 6) & 0x3f)).as_natural_8.to_character_8)
				s.extend ((0x80 | (ti & 0x3f)).as_natural_8.to_character_8)
			elseif ti < 0x200000 then
				s.extend ((0xf0 | (ti |>> 18)).as_natural_8.to_character_8)
				s.extend ((0x80 | ((ti |>> 12) & 0x3f)).as_natural_8.to_character_8)
				s.extend ((0x80 | ((ti |>> 6) & 0x3f)).as_natural_8.to_character_8)
				s.extend ((0x80 | (ti & 0x3f)).as_natural_8.to_character_8)
			end
-- 			if ti < 0x80 then
-- 				s.put (v.to_natural_8, 1)
-- 			elseif ti < 0x800 then
-- 				s.put ((0xC0 | (ti |>> 6)).to_natural_8, 1)
-- 				s.put ((0x80 | (ti & 0x3F)).to_natural_8, 2)
-- 			elseif ti < 0x10000 then
-- 				s.put ((0xE0 | (ti |>> 12)).to_natural_8, 1)
-- 				s.put ((0x80 | ((ti |>> 6) & 0x3F)).to_natural_8, 2)
-- 				s.put ((0x80 | (ti & 0x3F)).to_natural_8, 4)
-- 			elseif ti < 0x200000 then
-- 				s.put ((0xF0 | (ti |>> 18)).to_natural_8, 1)
-- 				s.put ((0x80 | ((ti |>> 12) & 0x3F)).to_natural_8, 2)
-- 				s.put ((0x80 | ((ti |>> 6) & 0x3F)).to_natural_8, 3)
-- 				s.put ((0x80 | (ti & 0x3F)).to_natural_8, 4)
-- 			end
 			Result := s.string
		end

end -- class AEL_I18N_STRING_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 08-Feb-2010
--|     Created from code broken out of AEL_SPRT_STRING_ROUTINES
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries and the Amalasoft Printf cluster.
--|----------------------------------------------------------------------
