--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A STRING with UTF-8 characteristics
--|----------------------------------------------------------------------

class AEL_I18N_UTF8_CHAR

inherit
	STRING_8
		rename
			make as str_make,
			make_from_string as str_make_from_string
		redefine
			is_less
		end

create
	make, make_from_string, make_from_ucs_code, make_from_substring

create {STRING}
	str_make

 --|========================================================================
feature {NONE}	-- Creation
 --|========================================================================

	make
		do
			-- Theoretically, a UTF-8 character can occupy up to 7 bytes,
			-- but practically they are limited to 4 bytes
			str_make (4)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
		require
			arg_exists: v /= Void
		do
			make_from_substring (v, 1)
		end

	--|--------------------------------------------------------------

	make_from_substring (v: STRING; spos: INTEGER)
		require
			arg_exists: v /= Void
			start_is_valid: spos > 0 and spos <= v.count
		do
			make
			init_from_string (v, spos)
		end

	--|--------------------------------------------------------------

	make_from_ucs_code (v: INTEGER)
		do
			default_create
			init_from_ucs_code (v)
		end

 --|========================================================================
feature
 --|========================================================================

	init_from_string (v: STRING; spos: INTEGER)
		require
			arg_exists: v /= Void and then v.count >= spos
			arg_is_utf8 : substr_is_utf8 (v, spos)
		local
			c: CHARACTER
			nb, pos: INTEGER
		do
			wipe_out
			nb := i18n.utf8_bytes_in_sequence (v, spos)
			if nb = 0 then
				-- An invalid stream - NOT UTF8 or ASCII
			else
				from pos := spos
				until pos > nb
				loop
					c := v.item (pos)
					extend (c)
					pos := pos + 1
				end
				init_uc_code
			end
		end

	--|--------------------------------------------------------------

	init_from_ucs_code (v: INTEGER)
		-- Initialize Current from the given UCS2 code
		local
			s: STRING
		do
			private_uc_code := v.to_natural_32
			s := i18n.uc_code_to_utf8 (v)
			make_from_string (s)
		end

 --|========================================================================
feature
 --|========================================================================

	uc_code: INTEGER
		do
			if private_uc_code = 0 then
				init_uc_code
			end
			Result := private_uc_code.as_integer_32
		end

	--|--------------------------------------------------------------

	code_to_character (v: INTEGER): like Current
		local
			s: STRING
		do
			s := i18n.uc_code_to_utf8 (v)
			create Result.make_from_string (s)
		end

 --|------------------------------------------------------------------------

  is_utf8_char (s : STRING): BOOLEAN
	  -- Does the string begin with a legal UTF8 character?
		require
			arg_exists: s /= Void and then not s.is_empty
		do
			Result := i18n.utf8_bytes_in_sequence (s, 1) /= 0
		end

 --|------------------------------------------------------------------------

	char_is_utf8 (s: STRING; len: INTEGER): BOOLEAN
		require
			arg_exists: s /= Void and then not s.is_empty
		do
			Result := substr_is_utf8 (s, 1)
		end

 --|------------------------------------------------------------------------

	substr_is_utf8 (s: STRING; spos: INTEGER): BOOLEAN
		require
			arg_exists: s /= Void and s.count >= spos
		do
			Result := i18n.utf8_bytes_in_sequence (s, spos) /= 0
		end

	--|--------------------------------------------------------------

	verbose_out : STRING
		local
			i, lim: INTEGER
			apf: AEL_PRINTF
		do
			create Result.make( 24 )
			create apf
			lim := count
			from i := 1
			until i > lim
			loop
				Result.append (apf.aprintf( " 0x%%02x", << item(i).code >> ))
				i := i + 1
			end
			Result.extend ('%N')

			Result.extend (' ')
			Result.append (Current)
			Result.extend ('%N')

			Result.append (apf.aprintf( " UCS Code: %%08x%N", << uc_code >> ))
		end

	--|--------------------------------------------------------------

	is_less alias "<" (other : like Current): BOOLEAN
			--
			-- Is current character "less than" other ?
			--
			-- If compare_codes is true, then comparison is strictly by
			-- the UTF-8 collating sequence (UCS code),
			-- and so 'B' is less than 'a'
			-- Else, comparison assumes pure alphabetic order (as defined for 
			-- the apparent character repertoire). In ASCII, we treat upper
			-- case letters as less than lower case letters, and so
			-- 'B' would be after (greater than) 'a', but before (less than) 'b'
			--
			-- This depends on the collating sequence (Mapping Table) for
			-- the language, assuming of course a single character repertoire.
			-- If the string contains characters from multiple repertoires
			-- then a simple comparison of UCS codes is the best we can do.
		local
			s1, s2, up_this, up_that: STRING
		do
			s1 := Current
			s2 := other

			if s1 < s2 then
				Result := True
			end

			if not compare_codes then
				up_this := s1.as_upper
				up_that := s2.as_upper
				if up_this < up_that then
					Result := True
				end
			end
		end

	compare_codes: BOOLEAN
			-- If True, use UCS codes for comparison

 --|========================================================================
feature {NONE}
 --|========================================================================

	private_uc_code: NATURAL_32

	--|--------------------------------------------------------------

	i18n: AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

	init_uc_code
		do
			private_uc_code := i18n.utf8_to_uc_code (Current, 1)
		end

end -- class AEL_I18N_UTF8_CHAR

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 10-Feb-2010
--|     Renamed from AEL_SPRT_UTF8_CHAR and moved from support to 
--|     i18n cluster
--| 002 28-Jul-2009
--|     Added history.
--|     Compiled and tested using Eiffel 6.1 and 6.4
--| 001 17-Aug-2008
--|     Updated for Eiffel 6.1; eliminated C code
--| 000 17-Apr-2000
--|     Created original module.
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class with one of the creation routines
--|----------------------------------------------------------------------
