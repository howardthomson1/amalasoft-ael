----------------------------------------------------------------------------
-- Copyright (c) 1995-2010, All rights reserved by
--
-- Amalasoft
-- 273 Harwood Avenue
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
--------------------------------------------------------------------------
--    Author Identification
--
--  RFO     Roger F. Osmond, Amalasoft
--
--------------------------------------------------------------------------
--
--    Audit Trail
--
--  Rev  Who    Date     Comments
--
--  001  RFO   04-Jul-2010  Renamed AEL_I18N_UTF8_STRING.
--                          Replaced in-line derive_character_count 
--                          calls with invalidations (setting the flag
--                          content_is_stabile to false) and updating
--                          character_count logic accordingly
--  000  RFO   17-Apr-2000  Created original module.
--
--------------------------------------------------------------------------

class AEL_I18N_UTF8_STRING
  -- A STRING-like collection of UTF-8 Characters, Each of which can 
  -- be from 1 to 7 bytes long

inherit
	STRING_8
		rename
			copy as str_copy,
			append as str_append
		redefine
			is_less, extend, precede, prepend, remove,
			prune, prune_all, prune_all_leading, prune_all_trailing,
			wipe_out, keep_head, keep_tail, left_adjust, right_adjust,
			left_justify, right_justify, center_justify, share
		end

create
	make, make_from_string, make_shared

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_shared (v: STRING_8)
		do
			make (0)
			share (v)
		end

 --|========================================================================
feature -- STRING features requiring character count update
 --|========================================================================

	append (v: READABLE_STRING_8)
		do
			str_append (v)
			content_is_stabile := False
		end

	extend (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	precede (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	prepend (v: READABLE_STRING_8)
		do
			Precursor (v)
			content_is_stabile := False
		end

	remove (i: INTEGER)
		do
			Precursor (i)
			content_is_stabile := False
		end

	prune (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	prune_all (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	prune_all_leading (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	prune_all_trailing (c: CHARACTER)
		do
			Precursor (c)
			content_is_stabile := False
		end

	wipe_out
		do
			Precursor
			content_is_stabile := False
		end

	copy (other: STRING_8)
		local
			ts: like Current
		do
			ts ?= other
			if (ts /= Void) then
				str_copy (ts)
			else
				create ts.make_shared (other)
				copy (ts)
			end
			content_is_stabile := False
		end

	keep_head (n: INTEGER)
		do
			Precursor (n)
			content_is_stabile := False
		end

	keep_tail (n: INTEGER)
		do
			Precursor (n)
			content_is_stabile := False
		end

	left_adjust
		do
			Precursor
			content_is_stabile := False
		end

	right_adjust
		do
			Precursor
			content_is_stabile := False
		end

	left_justify
		do
			Precursor
			content_is_stabile := False
		end

	right_justify
		do
			Precursor
			content_is_stabile := False
		end

	center_justify
		do
			Precursor
			content_is_stabile := False
		end

	share (other: STRING_8)
		do
			Precursor (other)
			content_is_stabile := False
		end

	--|--------------------------------------------------------------

	character_count: INTEGER
		do
			if not content_is_stabile then
				derive_character_count
			end
			Result := private_character_count
		end

	--|--------------------------------------------------------------

	content_is_stabile: BOOLEAN
			-- Has content of Current NOT changed since last analysis?

	derive_character_count
			-- Analyze the UTF8 stream to derive the number of UTF8 
			-- characters in the stream, and therefore in Current
			-- N.B. NOT the same as byte count unless Current holds
			-- only ASCII characters!!
		local
			u8s: like to_utf8_stream
		do
			u8s := to_utf8_stream
			private_character_count := u8s.count
			content_is_stabile := True
		end

 --|========================================================================
feature
 --|========================================================================

	is_less alias "<"  (other: like Current): BOOLEAN
			--
			-- Is current string "less than" other ?
			--
			-- If compare_codes is true, then comparison is strictly by
			-- the UTF-8 collating sequence (UCS code), and so 'B' is less than 'a'
			-- Else, comparison assumes pure alphabetic order (as defined for 
			-- the apparent character repertoire). In ASCII, we treat upper
			-- case letters as less than lower case letters, and so
			-- 'B' would be after (greater than) 'a', but before (less than) 'b'
			--
			-- This depends on the collating sequence (Mapping Table) for
			-- the language, assuming of course a single character repertoire.
			-- If the string contains characters from multiple repertoires
			-- then a simple comparison of UCS codes is the best we can do.
		local
			this_is_not_less: BOOLEAN
			stream, other_stream: like to_utf8_stream
		do
			stream := to_utf8_stream
			other_stream := other.to_utf8_stream

			if (stream.count = other_stream.count) then
				if (stream.count = count) then
					-- Is single-byte encoding (ASCII)
					Result := Precursor (other)
				else
					from
						stream.start
						other_stream.start
					until
						stream.exhausted or Result or this_is_not_less
					loop
						stream.item.set_compare_codes
						other_stream.item.set_compare_codes

						if (stream.item < other_stream.item) then
							Result := True
						elseif (not stream.item.is_equal (other_stream.item)) then
							this_is_not_less := True
						else
							stream.forth
							other_stream.forth
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_utf8: BOOLEAN
		do
			Result := isr.string_is_legal_utf8 (Current)
		end

	--|--------------------------------------------------------------

	utf8_item (i: INTEGER): AEL_I18N_UTF8_CHAR
			-- The i-th UTF-8 Character in this STRING
		local
			ts: like to_utf8_stream
		do
			ts := to_utf8_stream
			if (ts.count >= i) then
				Result := ts.i_th (i)
			end
		end

	--|--------------------------------------------------------------

	to_utf8_stream: LINKED_LIST [AEL_I18N_UTF8_CHAR]
			-- Convert this character string into a stream of UTF8 characters.
			-- UTF8 Characters occupy from 1 to 7 bytes
		local
			pstr: STRING_8
			tstr: STRING_8
			utchar: AEL_I18N_UTF8_CHAR
			pos, nb: INTEGER
		do
			create Result.make
			create pstr.make_from_string (Current)
			from pos := 1
			until pos > count
			loop
				tstr := pstr.substring (pos, pstr.count)
				create utchar.make_from_string (tstr)
				nb := utchar.count
				if (nb > 0) then
					Result.extend (utchar)
					pos := pos + nb
				else
					pos := count + 1
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_last_full_character: INTEGER
		do
			if (is_complete_string) then
			end
			Result := private_index_of_last_full_character
		end

	--|--------------------------------------------------------------

	is_complete_string: BOOLEAN
		do
			private_index_of_last_full_character := 0
			if not is_empty then
				Result := isr.is_complete_utf8_string (Current, 1)
				if Result then
					private_index_of_last_full_character :=
						isr.last_index_of_utf8_start (Current, Current.count)
				end
			end
		end

	--|--------------------------------------------------------------

	compare_codes: BOOLEAN
			-- Should comparisons use collating sequence codes?

	set_compare_codes
		do
			compare_codes := True
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	private_index_of_last_full_character: INTEGER
	private_character_count: INTEGER

--|========================================================================
feature -- Common routines
--|========================================================================

	isr: 	AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_I18N_UTF8_STRING
