class UCS_CHAR_CONST

inherit
	ANY
		rename
			copy as any_copy,
			is_equal as any_is_equal
		end

 --|========================================================================
feature
 --|========================================================================

	ucs_data: UCS_CHARACTER_DATABASE
		once
			create Result.make
		end

  --|--------------------------------------------------------------

	preferred_ucsdb_file: STRING
		once
			create Result.make (128)
		end

	set_preferred_ucsdb_file (v: STRING)
		require
			name_exists: v /= Void
		do
			preferred_ucsdb_file.wipe_out
			preferred_ucsdb_file.append (v)
		end

 --|========================================================================
feature
 --|========================================================================

	K_min_uc_code: INTEGER = 0
	K_max_uc_code: INTEGER = 65_535

	Ks_ucs_db_path_env: STRING = "UCSDATA_PATH"

	Ks_ucs_db_path_dflt: STRING
		do
			--Result := "/nas/site/locale/UnicodeData.txt"
			Result := "/nas/site/locale/unidata2.txt"
		end

	--|--------------------------------------------------------------

	hex3400: INTEGER = 13312
	hex4Db5: INTEGER = 19893
	hex4E00: INTEGER = 19968
	hex9FA5: INTEGER = 40869
	hexAC00: INTEGER = 44032
	hexD7A3: INTEGER = 55203
	hexD800: INTEGER = 55296
	hexDB7F: INTEGER = 56191
	hexDBFF: INTEGER = 56319
	hexDFFF: INTEGER = 57343
	hexF8FF: INTEGER = 63743

	--|--------------------------------------------------------------

	range_label (tc: INTEGER): STRING
			-- The description field associated with the given UCS code range
			-- (if not otherwise available for unassigned values)
		do
			Result := "Unassigned"

			if tc >= 0x3400 then
				if tc <= 0x4Db5 then
					Result := "CJK Ideograph Extension A"
				elseif tc >= 0x4E00 then
					if tc <= 0x9FA5 then
						Result := "CJK Ideograph"
					elseif tc >= 0xAC00 then
						if tc <= 0xD7A3 then
							Result := "Hangul Syllable"
						elseif tc >= 0xD800 then
							if tc <= 0xDB7F then
								Result := "Non Private Use High Surrogate"
							elseif tc <= 0xDBFF then
								Result := "Private Use High Surrogate"
							elseif tc <= 0xDFFF then
								Result := "Low Surrogate"
							elseif tc <= 0xF8FF then
								Result := "Private Use"
							end
						end
					end
				end
			end
		end

end -- class UCS_CHAR_CONST
