class UCS_CHAR_SPEC
  -- Unicode Character Set - Character Specification
  -- Derived from the Unicode Character Database

inherit
	STRING_8
		rename
			make as str_make,
			make_from_string as str_make_from_string,
			out as str_out,
			is_equal as string_is_equal,
			code as code_of
		select
		str_out, copy, string_is_equal
		end
	UCS_CHAR_CONST
		rename
			out as ucc_out
		end

create
	make, make_from_string, make_from_uc_code

create {UCS_CHAR_SPEC}
	str_make

 --|========================================================================
feature {NONE}
 --|========================================================================

	make_from_string (v: STRING)
		require
			arg_exists: v /= Void
		do
			make
			from_string (v)
		end

	--|--------------------------------------------------------------

	make_from_uc_code (v: INTEGER)
		local
			ti: like Current
		do
			make
			ti := ucs_data.item (v)
			if ti.code < 65535 then
				from_string (ti)
			end
		end

	--|--------------------------------------------------------------

	make
		do
			str_make (128)
		end

	--|--------------------------------------------------------------

	from_string (v: STRING)
			-- UC character spec is a semicolon-separated list of attributes,
			-- occupying a single line of text, as follows:
			--    <code>;
			--    <description>;
			--    <general category>;
			--    <number of canonical combining classes>;
			--    <bidirectional category>;
			--    <character decomposition>;
			--    <?>;
			--    <?>;
			--    <?>;
			--    <mirrorable flag>;
			--    <unicode 1.0 name>;
			--    <ISO 10646 Comment>;
			--    <Title-case>;
			--    <?>;
			--    <other case code>;
			--
			-- The semicolon following the last field appears to be optional
			--
			-- Example 1:
			-- 0041;LATIN CAPITAL LETTER A;Lu;0;L;;;;;N;;;;0061;
			--  ^           ^              ^^ ^ ^     ^     ^
			-- code     description        || | |     |     lowercase code
			--                             || | |      \ NOT mirrorable
			--                             || |  \ Bidirectional Category (letter)
			--                             ||  \ Zero canonical combining classes
			--                             ||
			--                General Category: Letter, Upper Case
			--
			-- Example 2:
			-- 005B;LEFT SQUARE BRACKET;Ps;0;ON;;;;;Y;OPENING SQUARE BRACKET;;;;
			--  ^           ^           ^^ ^  ^     ^            ^
			-- code     description     || |  |     |     Unicode 1.0 name
			--                          || |  |      \ mirrorable
			--                          || |   \ Bidirectional Category (on)
			--                          ||  \ Zero canonical combining classes
			--                          ||
			--              General Category: Punctuation, Open (start)
			--
		do
			wipe_out
			append (v)
			prune_all_leading ('%N')
			create fields.make
			fields.fill (split (';'))
		end

 --|========================================================================
feature
 --|========================================================================

	code: INTEGER
		do
			Result := field_to_hex_integer (1)
		end

	--|--------------------------------------------------------------

	description: STRING
		do
			if fields /= Void and then fields.count >= 2 then
				Result := fields.i_th (2)
			else
				Result := range_label (code)
			end
		end

	--|--------------------------------------------------------------

	general_category: STRING
		do
			if fields /= Void and then fields.count >= 3 then
				Result := fields.i_th (3)
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	canonical_combining_class_count: INTEGER
		do
			if fields /= Void and then fields.count >= 4 then
				Result := field_to_integer (4)
			end
		end

	--|--------------------------------------------------------------

	bidirectional_category: STRING
		do
			if fields /= Void and then fields.count >= 5 then
				Result := fields.i_th (5)
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	character_decomposition: STRING
		do
			if fields /= Void and then fields.count >= 6 then
				Result := fields.i_th (6)
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	is_mirrorable: BOOLEAN
		local
			ts: STRING
		do
			if fields /= Void and then fields.count >= 10 then
				ts := fields.i_th (10)
				if (not ts.is_empty) and then (ts.item(1) = 'Y' or ts.item(1) = 'y')
				 then
					 Result := True
				end
			end
		end

	--|--------------------------------------------------------------

	unicode_1_name: STRING
		do
			if fields /= Void and then fields.count >= 11 then
				Result := fields.i_th (11)
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	iso_10646_comment: STRING
		do
			if fields /= Void and then fields.count >= 12 then
				Result := fields.i_th (12)
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	title_case_code: INTEGER
		do
			if fields /= Void and then fields.count >= 13 then
				Result := field_to_integer (13)
			end
		end

	--|--------------------------------------------------------------

	other_case_code: INTEGER
		do
			if fields /= Void and then fields.count >= 15 then
				Result := field_to_integer (15)
			end
		end

	--|--------------------------------------------------------------

	out: STRING
			--  UCS: 0000053E (ARMENIAN CAPITAL LETTER CA)
			--  General Category: Letter, Uppercase
			--  Canonical Combining Classes: 0
			--  Bidirectional Category: L
			--  Mirror-able: N
			--  Lowercase = 056E
			--  Title-case = nil
		local
			tc: INTEGER
		do
			create Result.make (256)
			tc := code
			--Result.append (asr.aprintf (" UCS: %%08x (%%s)%N",<<tc, description>>))
			Result.append (apf.aprintf (" Description: %%s%N", <<description>>))
			if (tc = 0) or ucs_data.is_in_reserved_range(tc) then
				Result.append (
					apf.aprintf (" General Category: %%s%N",
					<<ucs_data.range_label(tc)>>))
			else
				if fields.count > 1 then
					Result.append (
						apf.aprintf (" General Category: %%s%N",
						<<general_category_to_string(general_category)>>)
									  )
					Result.append (
						apf.aprintf (" Canonical Combining Classes: %%d%N",
						<<canonical_combining_class_count>>))
					Result.append (
						apf.aprintf (" Bidirectional Category: %%s%N",
						<<bidirectional_category>>))
					Result.append (
						apf.aprintf (" Mirror-able: %%s%N", <<is_mirrorable.out>>))
					if other_case_code /= 0 then
						Result.append (
							apf.aprintf (" Other case = %%04x%N",<<other_case_code>>))
					end
					if title_case_code /= 0 then
						Result.append (
							apf.aprintf (" Title case = %%04x%N", <<title_case_code>>))
					end
				end
			end
		end

	--|--------------------------------------------------------------

	general_category_to_string (s: STRING): STRING
		require
			string_exists: s /= Void
			string_has_values: not s.is_empty
		local
			c1, c2: CHARACTER
			major, minor: STRING
		do
			minor := ""
			c1 := s.item(1)
			if s.count >= 2 then
				c2 := s.item(2)
			end

			inspect (c1)
			when 'C' then
				major := "Character"
				inspect (c2)
				when 'c' then
					minor := "Control"
				when 'f' then
					minor := "Format"
				when 'o' then
					minor := "Other"
				when 's' then
					minor := "Surrogate"
				else
				end
			when 'L' then
				major := "Letter"
				inspect (c2)
				when 'l' then
					minor := "Lowercase"
				when 'm' then
					minor := "Modifier"
				when 'o' then
					minor := "Other"
				when 't' then
					minor := "Titlecase"
				when 'u' then
					minor := "Uppercase"
				else
				end
			when 'M' then
				major := "Mark"
				inspect (c2)
				when 'c' then
					minor := "Spacing Combining"
				when 'e' then
					minor := "Enclosing"
				when 'n' then
					minor := "Non-Spacing"
				else
				end
			when 'N' then
				major := "Number"
				inspect (c2)
				when 'd' then
					minor := "Decimal"
				when 'l' then
					minor := "Letter"
				when 'o' then
					minor := "Other"
				else
				end
			when 'P' then
				major := "Punctuation"
				inspect (c2)
				when 'c' then
					minor := "Connector"
				when 'd' then
					minor := "Dash"
				when 'e' then
					minor := "Close"
				when 'f' then
					minor := "Final Quote"
				when 'i' then
					minor := "Initial Quote"
				when 'o' then
					minor := "Other"
				when 's' then
					minor := "Open"
				else
				end
			when 'S' then
				major := "Symbol"
				inspect (c2)
				when 'c' then
					minor := "Currency"
				when 'k' then
					minor := "Modifier"
				when 'm' then
					minor := "Math"
				when 'o' then
					minor := "Other"
				else
				end
			when 'Z' then
				major := "Separator"
				inspect (c2)
				when 'l' then
					minor := "Line"
				when 'p' then
					minor := "Paragraph"
				when 's' then
					minor := "Space"
				else
				end
			else
			end
			Result := apf.aprintf ("%%s, %%s", <<major, minor>>)
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	field_to_integer (v: INTEGER): INTEGER
		require
			arg_in_range: v > 0 and then v <= 15
		local
			ts: STRING
		do
			if fields /= Void then
				ts := fields.i_th (v)
				if asr.is_decimal_integer (ts) then
					Result := asr.decimal_string_to_int (ts)
				end
			end
		end

	--|--------------------------------------------------------------

	field_to_hex_integer (v: INTEGER): INTEGER
		require
			arg_in_range: v > 0 and then v <= 15
		local
			ts: STRING
		do
			if fields /= Void then
				ts := fields.i_th (v)
				if asr.string_is_hex_integer (ts) then
					Result := asr.hex_string_to_integer (ts)
				end
			end
		end

	--|--------------------------------------------------------------

	fields: LINKED_LIST [STRING]

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class UCS_CHAR_SPEC
