----------------------------------------------------------------------------
-- Copyright (c) 1995-2000, All rights reserved by
--
-- Amalasoft
-- 273 Harwood Avenue
-- Littleton, MA 01460 USA
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
--**************************************************************************
--
--            Part of the:  Amalasoft Productivity Toolkit (APT)
--
--**************************************************************************
--
-- Author Identification
--
--	RFO	Roger F. Osmond, Amalasoft
--
----------------------------------------------------------------------------
-- Audit Trail
--
-- 000 RFO 20-Apr-00	Created original module.
----------------------------------------------------------------------------

note
  description: "{
  Encapsulation of the Unicode Character Database.
  Latest db is read from file into a STRING form, and expanded to
  individual character specifications on demand.
  Database file is indentified by the UCSDATA_PATH environment variable.
  This class should exist as a once-d entity
  }";
  status: "Copyright 1995-2000, Amalasoft";
  date: "$Date: 2000/04/21 $";
  revision: "$Revision: 000$";
  author: "Roger F. Osmond, Amalasoft"

class UCS_CHARACTER_DATABASE

inherit
	ARRAY [UCS_CHAR_SPEC]
		rename
			make as a_make,
			item as array_item
		select
		copy, is_equal
		end
	EXECUTION_ENVIRONMENT
		rename
			get as getenv,
			put as setenv
		undefine
			copy, is_equal
		end
	UCS_CHAR_CONST

create
	make

 --|========================================================================
feature {NONE}
 --|========================================================================

	make
		do
			a_make (K_min_uc_code, K_max_uc_code)
			init_db
		end

 --|========================================================================
feature {ANY}
 --|========================================================================

	item (v: INTEGER): like array_item
			-- The individual Character Specification corresponding
			-- to the given UCS code value
		require
			data_exists: data_exists
			index_in_range: is_valid_index(v)
		do
			Result := array_item (v)
			if Result = Void then
				----------------------------------------
				-- Create the entry from the db_stream
				-- if the entry is not yet present

				Result := entry_by_uc_code (v)
			end
		ensure
			result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_in_reserved_range (tc: INTEGER): BOOLEAN
			-- Some characters are not assigned yet, and have only ranges
			-- assigned.  If so, then there is not additional information
			-- available other than the range
			-- The description field associated with the given UCS code
			-- (if not otherwise available for unassigned values)
		do
			if tc >= hex3400 then
				if tc <= hex4Db5 then
					Result := True
				elseif tc >= hex4E00 then
					if tc <= hex9FA5 then
						Result := True
					elseif tc >= hexAC00 then
						if tc <= hexD7A3 then
							Result := True
						elseif tc >= hexD800 then
							if tc <= hexDB7F then
								Result := True
							elseif tc <= hexDBFF then
								Result := True
							elseif tc <= hexDFFF then
								Result := True
							elseif tc <= hexF8FF then
								Result := True
							end
						end
					end
				end
			end
		end

 --|========================================================================
feature {ANY}	-- Assertion Support
 --|========================================================================

	is_valid_index (v: INTEGER): BOOLEAN
		do
			Result := (v >= K_min_uc_code) and (v <= K_max_uc_code)
		end

	--|--------------------------------------------------------------

	data_exists: BOOLEAN
		do
			Result := db_stream /= Void and then not db_stream.is_empty
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

	init_db
		local
			path: FILE_NAME
			penv: STRING
			dbfile: PLAIN_TEXT_FILE
		do
			if not preferred_ucsdb_file.is_empty then
				create path.make_from_string (preferred_ucsdb_file)
			else
				create path.make
				penv := getenv (Ks_ucs_db_path_env)
				if penv /= Void and then not penv.is_empty then
					path.extend (penv)
				else
					path.extend (Ks_ucs_db_path_dflt)
				end
			end

			create dbfile.make (path)
			if dbfile.exists then
				dbfile.open_read
				if dbfile.is_open_read then
					if dbfile.readable then
						dbfile.read_stream (dbfile.count)
						create db_stream.make (0)
						db_stream.share (dbfile.last_string)
					end
					dbfile.close
				end
			end
		end

	--|--------------------------------------------------------------

	db_stream: STRING
			-- The STRING representation of the complete
			-- UnicodeData character database

	--|--------------------------------------------------------------

	populate_all
			-- Fill in all character positions from the db_stream
		require
			stream_exists: data_exists
		local
			tmp_c: like item
			tc: CELL [INTEGER]
			lim: INTEGER
			ts: STRING
		do
			create tc.put (1)
			lim := db_stream.count
			from ts := asr.next_line (db_stream, tc)
			until tc.item = 0 or tc.item > lim
			loop
				create tmp_c.make_from_string (ts)
				put (tmp_c, tmp_c.code)
				ts := asr.next_line (db_stream, tc)
			end
		end

	--|--------------------------------------------------------------

	entry_by_uc_code (v: INTEGER): like item
		require
			index_in_range: is_valid_index(v)
			stream_exists: data_exists
		local
			tstr: STRING
			pos1, pos2: INTEGER
		do
			-------------------------------------------------------
			-- The database stream exists as a sequence of lines,
			-- each of which begins with a 4 byte field containing
			-- a hexadecimal representation of the UCS code, and
			-- followed by a semicolon.
			-- Find the given UCS code in one of these fields

			if v > 0 then
				tstr := apf.aprintf ("%N%%04x", << v >>)
				tstr.to_upper
				pos1 := db_stream.substring_index (tstr, 1)
				if pos1 > 0 then
					pos1 := pos1 + 1
				end
			else
				pos1 := 1
			end

			----------------------------------------
			-- Now extract the full line from the
			-- db_stream, beginning with the UCS
			-- code field

			if pos1 > 0 then
				pos2 := db_stream.index_of ('%N', pos1)
				if pos2 > 0 then
					pos2 := pos2 - 1
				else
					pos2 := db_stream.count
				end
				tstr := db_stream.substring (pos1, pos2)
			end

			----------------------------------------
			-- Now generate a new character spec
			-- from the extracted line and add it
			-- to the database at the proper index

			if tstr /= Void then
				create Result.make_from_string (tstr)
			else
				Result := dummy_spec
			end
			put (Result, v)
		end

	--|--------------------------------------------------------------

	dummy_spec: UCS_CHAR_SPEC
		once
			create Result.make_from_string ("FFFF;Dummy Entry;So;0;;;;;;N;;;;;;")
		end

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class UCS_CHARACTER_DATABASE
