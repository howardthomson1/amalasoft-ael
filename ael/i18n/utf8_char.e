----------------------------------------------------------------------------
-- Copyright (c) 1995-2000, All rights reserved by
--
-- Amalasoft
-- 273 Harwood Avenue
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
--------------------------------------------------------------------------
--    Author Identification
--
--  RFO     Roger F. Osmond, Amalasoft
--
--------------------------------------------------------------------------
--
--    Audit Trail
--
--  Rev  Who    Date     Comments
--
--  000  RFO   17-Apr-00  Created original module.
--
--------------------------------------------------------------------------

class UTF8_CHAR

inherit
	STRING_8
		rename
			make as str_make,
			make_from_string as str_make_from_string,
			out as str_out
--			is_equal as string_is_equal
--			compare_ascii as compare_codes,
--			set_compare_ascii as set_compare_codes
		redefine
			infix "<", to_upper, to_lower
		select
		str_out, copy, is_equal
		end
	UCS_CHAR_CONST
		rename
			out as ucc_out
		end

create
	make, make_from_string, make_from_ucs_code, make_from_ucs_spec,
	make_from_substring

create {UTF8_CHAR}
	str_make

 --|========================================================================
feature {NONE}	-- Creation
 --|========================================================================

	make
		do
			str_make (K_max_bytes)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
		require
			arg_exists: v /= Void
		do
			make
			from_string (v, 1)
		end

	--|--------------------------------------------------------------

	make_from_substring (v: STRING; spos: INTEGER)
		require
			arg_exists: v /= Void
			start_is_valid: spos > 0 and spos <= v.count
		do
			make
			from_string (v, spos)
		end

	--|--------------------------------------------------------------

	make_from_ucs_code (v: INTEGER)
		do
			make
			from_ucs_code (v)
		end

	--|--------------------------------------------------------------

	make_from_ucs_spec (v: STRING)
		require
			arg_exists: v /= Void
		do
			make
			from_ucs_spec (v)
		end

 --|========================================================================
feature
 --|========================================================================

	from_string (v: STRING; spos: INTEGER)
		require
			arg_exists: v /= Void
			arg_is_utf8: isr.substr_is_utf8 (v, spos)
		local
			c: CHARACTER
			nb, pos: INTEGER
		do
			wipe_out
			nb := isr.number_of_bytes_in_char (v, spos)
			if nb > 0 then
				from pos := spos
				until pos > nb
				loop
					c := v.item (pos)
					extend (c)
					pos := pos + 1
				end
				if private_uc_code = 0 then
					private_uc_code := isr.utf8_to_uc_code (Current, 1)
				end
			end
		end

	--|--------------------------------------------------------------

	from_ucs_code (v: INTEGER)
		local
			ts: STRING_8
		do
			private_uc_code := v.to_natural_32
			ts := isr.uc_code_to_utf8 (private_uc_code.as_integer_32)
			from_string (ts, 1)
		end

	--|--------------------------------------------------------------

	from_ucs_spec (v: STRING)
		local
			ts: UCS_CHAR_SPEC
		do
			create ts.make_from_string (v)
			from_ucs_code (ts.code)
		end

	--|--------------------------------------------------------------

	uc_spec: UCS_CHAR_SPEC
		local
			tc: INTEGER
		do
			tc := uc_code.to_integer_32
			if ucs_data.data_exists and then (tc >= 0) then
				Result := ucs_data.item (tc)
			end
		end

 --|========================================================================
feature
 --|========================================================================

	is_utf8: BOOLEAN = True

	--|--------------------------------------------------------------

	uc_code: NATURAL_32
		local
			ts: STRING_8
		do
			if private_uc_code = 0 then
				ts := Current
				private_uc_code := isr.utf8_to_uc_code (ts, 1)
			end
			Result := private_uc_code
		end

	--|--------------------------------------------------------------

	code_to_character (v: INTEGER): like Current
		local
			ts: STRING_8
		do
			ts := isr.uc_code_to_utf8 (v)
			create Result.make_from_string (ts)
		end

	compare_codes: BOOLEAN

	set_compare_codes
		do
			compare_codes := True
		end

	--|--------------------------------------------------------------

	language: STRING
		local
			tc: INTEGER
			ts: UCS_CHAR_SPEC
		do
			Result := "Unknown"
			if ucs_data.data_exists and not is_empty then
				tc := uc_code.to_integer_32
				ts := ucs_data.item (tc)
				Result := ts.description
				if Result.is_equal ("Unassigned") then
					Result := range_label (tc)
				end
			end
		end

	--|--------------------------------------------------------------

	out: STRING
		local
			nb, i: INTEGER
			ts: like uc_spec
		do
			create Result.make (256)
			nb := count
			Result.append ("UTF-8 Character: ")
			from i := 1
			until i > nb
			loop
				Result.append (apf.aprintf (" 0x%%02x", << item(i).code >>))
				i := i + 1
			end
			Result.append ("%N")

			Result.append (" " + Current + "%N")

			Result.append (apf.aprintf (" UCS Code: %%08x%N", << uc_code >>))

			if not ucs_data.data_exists then
				Result.append ("Unicode database is unavailable%N")
			else
				ts := uc_spec
				if ts /= Void then
					Result.append (uc_spec.out)
				else
					Result.append ("Illegal UTF-8 Character%N")
				end
			end
		end

	--|--------------------------------------------------------------

	infix "<"  (other: like Current): BOOLEAN
			--
			-- Is current character "less than" other ?
			--
			-- If compare_codes is true, then comparison is strictly by
			-- the UTF-8 collating sequence (UCS code), and so 'B' is less than 'a'
			-- Else, comparison assumes pure alphabetic order (as defined for 
			-- the apparent character repertoire). In ASCII, we treat upper
			-- case letters as less than lower case letters, and so
			-- 'B' would be after (greater than) 'a', but before (less than) 'b'
			--
			-- This depends on the collating sequence (Mapping Table) for
			-- the language, assuming of course a single character repertoire.
			-- If the string contains characters from multiple repertoires
			-- then a simple comparison of UCS codes is the best we can do.
		local
			up_this: STRING
			up_that: STRING
			this_is_less: BOOLEAN
		do
			create up_this.make (count)
			up_this.append (Current)

			create up_that.make (other.count)
			up_that.append (other)

			if up_this < up_that then
				this_is_less := true
			end

			if compare_codes then
				Result := this_is_less
			else
				up_this.to_upper
				up_that.to_upper

				if up_this < up_that then
					Result := True
				elseif up_this.is_equal (up_that) then
					if this_is_less then
						Result := True
					end
				end
			end
		end

	--|--------------------------------------------------------------

	upper: like Current
		local
			uc: INTEGER
			ts: UCS_CHAR_SPEC
			gc: STRING
		do
			if ucs_data.data_exists then
				ts := ucs_data.item (uc_code.to_integer_32)
				gc := ts.general_category
				if gc /= Void and then gc.is_equal("Ll") then
					-- Is a lowercase letter
					uc := ts.other_case_code
					create Result.make_from_ucs_code (uc)
				end
			end
			if Result = Void then
				Result := Current
			end
		ensure
			result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	lower: like Current
		local
			uc: INTEGER
			ts: UCS_CHAR_SPEC
			gc: STRING
		do
			if ucs_data.data_exists then
				ts := ucs_data.item (uc_code.to_integer_32)
				gc := ts.general_category
				if gc /= Void and then gc.is_equal("Lu") then
					-- Is an uppercase letter
					uc := ts.other_case_code
					create Result.make_from_ucs_code (uc)
				end
			end
			if Result = Void then
				Result := Current
			end
		ensure
			result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	to_upper
		do
			wipe_out
			append (upper)
		end

	--|--------------------------------------------------------------

	to_lower
		do
			wipe_out
			append (lower)
		end

--|========================================================================
feature -- Common routines
--|========================================================================

	isr: 	AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	private_uc_code: NATURAL_32

	--|--------------------------------------------------------------

	K_max_bytes: INTEGER = 4
			-- Theoretically, a UTF-8 character can occupy up to 7 bytes,
			-- but practically they are limited to 4 bytes

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class UTF8_CHAR
