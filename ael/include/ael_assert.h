/*--------------------------------------------------------------------------
-- Copyright (c) 1995, All rights reserved by
--
-- Amalasoft
-- 126 New Estate Road
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
----------------------------------------------------------------------------
--
--	ael_assert.h - Master include for C assertion handling facility
--
----------------------------------------------------------------------------
-- Author Identification
--
--	RFO	Roger F. Osmond, Amalasoft
--
----------------------------------------------------------------------------
-- Audit Trail
--
-- 000 RFO 02-Apr-95	Original module adapted from older versions
--------------------------------------------------------------------------*/

#ifndef _AEL_ASSERT_H
#define _AEL_ASSERT_H

static char *sccsid_ael_assert_h =
"@(#) Copyright (c) 1995 Amalasoft: ael_assert.h, Version 000";

/*---------------------------------------------
-- These values are used to index an array of
-- labels in ael_assert.c
*/

# define Ki_as_require		0
# define Ki_as_ensure		1
# define Ki_as_check		2
# define Ki_as_invariant	3

# if defined(_AS_ASSERT_ALL)
#  define AS_ASSERT
# else
#  undef AS_ASSERT
# endif


/*--------------------------------------------------------------------------
--
--	Assertion (violation) Checks and Notification Calls
--
--	
*/
# ifdef AS_ASSERT

#  define Require(lbl,pred)	{if(!(pred)){_as_assert(Ki_as_require,\
							(lbl),\
							__FILE__,\
							__LINE__,\
							NULL);}}
#  define Ensure(lbl,pred)	{if(!(pred)){_as_assert(Ki_as_ensure,\
							(lbl),\
							__FILE__,\
							__LINE__,\
							NULL);}}
#  define Check(lbl,pred)	{if(!(pred)){_as_assert(Ki_as_check,\
							(lbl),\
							__FILE__,\
							__LINE__,\
							NULL);}}

# else

#  define Require(lbl,pred)	
#  define Ensure(lbl,pred)	
#  define Check(lbl,pred)	

# endif

/*----------------------------------------
-- Assertion keyword aliases
*/

# define require(lbl,pred)	Require(lbl,pred)
# define REQUIRE(lbl,pred)	Require(lbl,pred)

# define ensure(lbl,pred)	Ensure(lbl,pred)
# define ENSURE(lbl,pred)	Ensure(lbl,pred)

# define check(lbl,pred)	Check(lbl,pred)
# define CHECK(lbl,pred)	Check(lbl,pred)

/*--------------------------------------------------------------------------
--	External Function Declarations
*/

# ifdef _NO_PROTO

extern void _as_assert();
extern void _as_assert_set_exit();

# else

extern void _as_assert(int asrt_type, char *lbl,
		       char *filename, int lineno, Fptr_void rescue);
extern void _as_assert_set_exit(void);

# endif	/* _NO_PROTO */

#endif	/* _AEL_ASSERT_H */
