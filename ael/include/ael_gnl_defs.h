/*--------------------------------------------------------------------------
-- Copyright (c) 1995, All rights reserved by
--
-- Amalasoft
-- 126 New Estate Road
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
----------------------------------------------------------------------------
--
--	ael_gnl_defs.h - Common and general constants
--
----------------------------------------------------------------------------
-- Author Identification
--
--	RFO	Roger F. Osmond, Amalasoft
--
----------------------------------------------------------------------------
-- Audit Trail
--
-- 000 RFO 02-Apr-95	Original module (from many earlier versions)
--------------------------------------------------------------------------*/

#ifndef _AEL_GNL_DEFS_H
#define _AEl_GNL_DEFS_H

static char *sccsid_ael_gnl_defs_h =
"@(#) Copyright (c) 1995 Amalasoft: ael_gnl_defs.h, Version 000";

/*------------------------------------------------------------------------*/

#define K_true		1
#define K_false		0

#define K_void		0

#define K_ok		0
#define K_error		-1
#define K_maybe		2

#define K_octal		8
#define K_decimal	10
#define K_hex		16

#define K_fd_stdin	0
#define K_fd_stdout	1
#define K_fd_stderr	2

/*--------------------------------------------------------------------------*/

#define SET_STATUS(v)	{if(status){*(status)=(v);}}

#endif	/* _AEL_GNL_DEFS_H */
