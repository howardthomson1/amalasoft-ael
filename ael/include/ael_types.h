/*--------------------------------------------------------------------------
-- Copyright (c) 1995, All rights reserved by
--
-- Amalasoft
-- 126 New Estate Road
-- Littleton, MA 01460 USA 
--
-- This software is furnished under a license and may be used and copied
-- only  in  accordance  with  the  terms  of such  license and with the
-- inclusion of the above copyright notice. This software or  any  other
-- copies thereof may not be provided or otherwise made available to any
-- other person. No title to and ownership of  the  software  is  hereby
-- transferred.
--
-- The information in this software is subject to change without notice
-- and should not be construed as a commitment by Amalasoft.
--
-- Amalasoft assumes no responsibility for the use or reliability of this
-- software.
--
----------------------------------------------------------------------------
--
--	ael_types.h - Common type definitions
--
----------------------------------------------------------------------------
-- Author Identification
--
--	RFO	Roger F. Osmond, Amalasoft
--
----------------------------------------------------------------------------
-- Audit Trail
--
-- 001 RFO 01-Nov-13	Renamed, resurrected
-- 000 RFO 02-Apr-95	Original module (adapted from earlier ones)
--------------------------------------------------------------------------*/

#ifndef _AEL_TYPES_H
# define _AEL_TYPES_H

static char *sccsid_ael_types_h =
"@(#) Copyright (c) 1995 Amalasoft: ael_types.h, Version 000";

/*------------------------------------------------------------------------*/

#ifdef _NO_PROTO

typedef void (* Fptr_void)();
typedef void (* Fptr_void1)();
typedef void (* Fptr_void2)();
typedef void (* Fptr_void3)();
typedef int (* Fptr_int)();
typedef int (* Fptr_int1)();
typedef int (* Fptr_int2)();
typedef int (* Fptr_int3)();

#else

typedef void (* Fptr_void)(void);
typedef void (* Fptr_void1)(int a1);
typedef void (* Fptr_void2)(int a1, int a2);
typedef void (* Fptr_void3)(int a1, int a2, int a3);
typedef int (* Fptr_int)(void);
typedef int (* Fptr_int1)(int a1);
typedef int (* Fptr_int2)(int a1, int a2);
typedef int (* Fptr_int3)(int a1, int a2, int a3);

#endif

#ifdef WINDOWS
# include "winsock.h"
# include <sys/timeb.h>
#else
# ifdef WIN32
#  include "winsock.h"
#  include <sys/timeb.h>
# else
#  include <sys/time.h>
#  include <sys/timeb.h>
# endif
#endif

#endif /* _AEL_TYPES_H */
