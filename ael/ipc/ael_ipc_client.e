class AEL_IPC_CLIENT
-- Sender side of IPC mechanism

inherit
	SOCKET_RESOURCES

create
	make, make_for_message

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (v: like socket_path)
		do
			socket_path := v
			create soc1.make_client (socket_path)
		end

	make_for_message (p: like socket_path; msg: like last_message)
		do
			make (p)
			send_message (msg)
		end

--|========================================================================
feature -- Status
--|========================================================================

	socket_path: STRING
	message_proc: PROCEDURE [like Current, like last_message]
	last_message: AEL_IPC_MESSAGE

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	soc1: UNIX_STREAM_SOCKET

--|========================================================================
feature -- Status setting
--|========================================================================

	set_message_proc (v: like message_proc)
		do
			message_proc := v
		end

	--|--------------------------------------------------------------

	send_message (msg: like last_message)
			-- Send a message, listen for response
		require
			msg_exists: msg /= Void
		local
--RFO			resp: like last_message
		do
			last_message := msg
			soc1.connect
			msg.general_store (soc1)
--RFO			resp ?= msg.retrieved (soc1)
--RFO			if resp /= Void and then message_proc /= Void then
--RFO				message_proc.call ([Current, resp])
--RFO			end
			soc1.cleanup
		rescue
			soc1.cleanup
		end

end -- class AEL_IPC_CLIENT
