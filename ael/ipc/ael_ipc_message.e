class AEL_IPC_MESSAGE
-- Structured interprocess communication message"

inherit
	STORABLE

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (cid: INTEGER)
		require
			valid_caller: cid > 0
		do
			caller_id := cid
			sequence_number := seqno_ref.item + 1
			seqno_ref.set_item (sequence_number)
			create data.make (0)
		end

--|========================================================================
feature -- Status
--|========================================================================

	sequence_number: INTEGER_64
			-- Sequence number of message emanating from this process space

	caller_id: INTEGER
			-- PID of calling agent

	data_length: INTEGER
			-- Number of bytes of data
		do
			Result := data.count
		end

	data: STRING
			-- Actual body of message

--|========================================================================
feature -- Status setting
--|========================================================================

	set_data (v: STRING)
			-- Set the payload of the message to the given string
		require
			exists: v /= Void
		do
			data.wipe_out
			data.append (v)
		ensure
			is_set: data.is_equal (v)
			unassigned: data /= v
			data_not_moved: data = old data
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	seqno_ref: INTEGER_64_REF
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant
	valid_sequence_number: sequence_number /= 0
	has_message_body: data /= Void

end -- class AEL_IPC_MESSAGE
