class AEL_IPC_SERVER
-- Receiver side of IPC mechanism

inherit
	SOCKET_RESOURCES

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (v: like socket_path)
		do
			socket_path := v
--RFO TODO - make a portable version that doesnt use UNIX_STREAM_SOCKET!
--RFO			create soc1.make_server (socket_path)
			if not soc1.descriptor_available then
				-- ERROR
			else
				socket_open_successful := True
			end
--RFO			soc1.set_non_blocking
		end

--|========================================================================
feature -- Status
--|========================================================================

	socket_path: STRING
	message_proc: PROCEDURE [like Current, AEL_IPC_MESSAGE]

	socket_open_successful: BOOLEAN
			-- Was the associated socket opened successfully?

	exit_pending: BOOLEAN
			-- Has an exit been requested?

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	soc1, soc2: NETWORK_STREAM_SOCKET

	--|--------------------------------------------------------------

	read_from_socket
			-- Read pending message, if any
		local
			msg: AEL_IPC_MESSAGE
			tst: STORABLE
		do
			soc1.accept
			soc2 ?= soc1.accepted
			create tst
			msg ?= tst.retrieved (soc2)
			-- now process the message
			if message_proc /= Void then
				message_proc.call ([Current,msg])
			end
			soc2.close
		rescue
			if soc2 /= Void then
				soc2.close
			end
			retry
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_message_proc (v: like message_proc)
		do
			message_proc := v
		end

	--|--------------------------------------------------------------

	listen
		local
			count: INTEGER
		do
			count := 0
			-- Listen on at most 5 connections?????????????
			from soc1.listen (5)
			until count = 3 or exit_pending
			loop
				read_from_socket
				count := count + 1
			end
		end

	--|--------------------------------------------------------------

	set_exit_pending
		do
			exit_pending := True
		end

end -- class AEL_IPC_SERVER
