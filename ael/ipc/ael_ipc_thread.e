class AEL_IPC_THREAD
-- Thread for IPC send/receive

inherit
	THREAD

create
	make_with_values

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_values (pid: INTEGER; dp: STRING)
			-- Create current with process id 'pid' and using
			-- path 'dp' as directory for sockets
		require
			valid_pid: pid /= 0
			valid_dirpath: dp /= Void and then not dp.is_empty
		do
			make
			socket_path := dp.twin
			socket_path.append ("/ipc_" + pid.out)
			create last_message.make (pid)
			create busy_ref
			create exit_requested_ref
			create ipc_server.make (socket_path)
		end

--|========================================================================
feature -- Status
--|========================================================================

	ipc_server: AEL_IPC_SERVER
	socket_path: STRING

	--|--------------------------------------------------------------

	last_message: AEL_IPC_MESSAGE

	has_message: BOOLEAN
		do
			Result := last_message /= Void and then last_message.data_length /= 0
		end

	--|--------------------------------------------------------------

	is_busy: BOOLEAN
		do
			Result := busy_ref.item
		end

	busy_ref: BOOLEAN_REF

	exit_requested: BOOLEAN
		do
			Result := exit_requested_ref.item
		end

	exit_requested_ref: BOOLEAN_REF

	--|--------------------------------------------------------------

	message_locker: ANY

	message_is_locked: BOOLEAN
		-- Is message currently locked?
		do
			Result := message_locker = Void
		end

	have_message_lock (cl: ANY): BOOLEAN
			-- Does the caller have the message lock currently?
		do
			Result := cl = message_locker
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	lock_message (cl: ANY)
		require
			locker_exists: cl /= Void
		do
			if not message_is_locked then
				message_locker := cl
			end
		ensure
			locked: message_is_locked
		end

	--|--------------------------------------------------------------

	unlock_message (cl: ANY)
		require
			locker_exists: cl /= Void
			message_locked: message_is_locked
			locked_by_caller: message_locker = cl
		do
			message_locker := Void
		ensure
			unlocked: not message_is_locked
		end

	--|--------------------------------------------------------------

	clear_message (msg: like last_message; cl: ANY)
		require
			message_exists: msg /= Void
			message_is_last: msg = last_message
			message_locked: message_is_locked
			locked_by_caller: message_locker = cl
		do
			last_message := Void
		ensure
			cleared: not has_message
		end

	--|--------------------------------------------------------------

	request_exit
			-- Set exit_requested
			do
				exit_requested_ref.set_item (True)
				ipc_server.set_exit_pending
			end

--|========================================================================
feature -- Execution
--|========================================================================

	execute
		do
			if not ipc_server.socket_open_successful then
				exit
			else
				ipc_server.set_message_proc (agent on_message_receive)
				ipc_server.listen
				exit
			end
		end

--|========================================================================
feature {NONE} -- Message processing
--|========================================================================

	on_message_receive (sv: like ipc_server; msg: AEL_IPC_MESSAGE)
		require
			server_exists: sv /= Void
			message_exists: msg /= Void
		do
			busy_ref.set_item (True)
			last_message := msg
			busy_ref.set_item (False)
		end

end -- class AEL_IPC_THREAD
