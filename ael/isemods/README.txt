--------------------------------------------------
Mods to stock Eiffel 7.2
-------------------------
string_to_real_convertor.e
from $EIFFEL/library/base/elks/kernel/string
--|========================================================================
feature -- Modifications by RFO
--|========================================================================

	parse_substring_with_type (s: READABLE_STRING_GENERAL; nt, sp, ep: INTEGER)
			-- Parse string 's', within range sp, ep, as real number of 
			-- type 'nt'
		require
			exists: s /= Void
			valid_start: sp > 0 and sp <= s.count
			valid_end: ep >= sp and ep <= s.count
			valid_type: numinfo.real_double_type_valid (nt)
		local
			i, lim: INTEGER
		do
			reset (nt)
			lim := ep
			from i := sp
			until i > lim or last_state = 9
			loop
				parse_character (s.code (i).to_character_8)
				i := i + 1
			end
			last_substring_parse_position := i - 1
		end

	--|--------------------------------------------------------------

	last_substring_parse_position: INTEGER
			-- Index within the last substring given for parsing of the 
			-- end position of the parse

	--|--------------------------------------------------------------

	numinfo: NUMERIC_INFORMATION
		once
			create Result
		end

	--|--------------------------------------------------------------
