class AEL_JSON_ARRAY
-- An array encoded from/into JSON format

inherit
	AEL_JSON_OBJECT
		redefine
			make_elements,
			initialize_serial_params
		end
	--init_from_substream

create
	make, make_anonymous
--, make_from_stream, make_from_substream

--|========================================================================
feature -- Initialization
--|========================================================================

	make_elements
		do
			Precursor
--			set_element_structure
		end

	--|--------------------------------------------------------------

	initialize_serial_params
		do
			Precursor
			serial_params.enable_elements_as_array
		end

	--|--------------------------------------------------------------

	BAD_init_from_substream (v: STRING; spos, epos: INTEGER)
-- N.B. This should NEVER be called
-- JSON object now uses JSON parser, bypassing this
--
			-- A JSON array is bounded by '[' ']' pairs, possibly 
			-- preceded by a quoted name and colon.  JSON arrays
			-- can be nested.
			-- JSON arrays can contain raw integers, floats, string,
			-- or JSON objects, but must be homogeneous
			-- Numerics e.g. [ 1, 2, 3.14159 ] (floats)
			-- Numerics e.g. [ 1, 2, 3, 4 ] integers
			-- Strings e.g. [ "foo", "bar", foobar" ]
			-- Objects e.g. [ {"name": "foo"}, {"tag": "value"}, {"num": 
			-- 1}]
		require else
			never: False
--RFO 		local
--RFO 			i, sp, ep, lim, p: INTEGER
--RFO 			rx, rxs: detachable like rexp_json_stream
--RFO 			js: AEL_JSON_OBJECT
--RFO 			done: BOOLEAN
		do
			--RFO last_scan_position := 0
			--RFO lim := epos
			--RFO sp := index_of_next (v, spos)
			--RFO if sp = 0 then
			--RFO 	set_scan_error ("Missing opening non-whitespace", spos)
			--RFO else
			--RFO 	p := v.index_of ('[', sp)
			--RFO 	ep := asr.index_of_matching_close (v, p, lim, '\')
			--RFO 	rx := rexp_json_array_last
			--RFO 	rx.search_forward (v, spos, ep)
			--RFO 	if not rx.found then
			--RFO 		set_scan_error ("Missing open/close brackets", sp)
			--RFO 	end
			--RFO end
			--RFO if not has_error and rx /= Void then
			--RFO 	lim := rx.end_position
			--RFO 	-- Skip over opening bracket
			--RFO 	sp := rx.start_position + 1
			--RFO 	rxs := rexp_json_anon_obj

			--RFO 	-- TODO this looks ONLY for objects, not literals.  
			--RFO 	-- Need to add support for string and numeric items
			--RFO 	-- If first non-blank char WITHIN bounds is an open 
			--RFO 	-- '{' then all items should be objects.  If the first 
			--RFO 	-- non-blank char in bounds is a quote, then all 
			--RFO 	-- items should be strings.  If the first item in 
			--RFO 	-- bounds is numeric, then all items must be 
			--RFO 	-- numerics.  Reducing numerics to integers can be 
			--RFO 	-- a decision made after collecting all items (if 
			--RFO 	-- all are integer-like, then the type can be set 
			--RFO 	-- to integer for all).
			--RFO 	-- Items CANNOT be simple identifiers (as in a 
			--RFO 	-- javascript array, e.g. [ foo, somevar ]).  As 
			--RFO 	-- such, unquoted strings are not legal.

			--RFO 	init_objects_from_substream (v, sp, ep, lim)

			--RFO --RFO 	from i := sp
			--RFO --RFO 	until i >= lim or has_error or done
			--RFO --RFO 	loop
			--RFO --RFO 		-- Loop through strange range instantiated items and
			--RFO --RFO 		-- adding them to Current
			--RFO --RFO 		i := index_of_next (v, i)
			--RFO --RFO 		if i = 0 then
			--RFO --RFO 			-- Off end
			--RFO --RFO 			i := lim
			--RFO --RFO 		elseif i = lim then
 			--RFO --RFO 			done := True
			--RFO --RFO 		else
			--RFO --RFO 			sp := v.index_of ('{', i)
			--RFO --RFO 			if sp = 0 then
			--RFO --RFO 				-- No more items
			--RFO --RFO 				i := ep
			--RFO --RFO 				done := True
			--RFO --RFO 			else
			--RFO --RFO 				ep := asr.index_of_matching_close (v, sp, lim, '\')
			--RFO --RFO 				if ep /= 0 then
			--RFO --RFO 					-- Create an anonymous object
			--RFO --RFO 					create js.make_from_substream ("", v, sp, ep)
			--RFO --RFO 					if js.has_error then
			--RFO --RFO 						copy_errors (js)
			--RFO --RFO 					else
			--RFO --RFO 						add_element_by_name (js, element_name (js))
			--RFO --RFO 						-- Last scan position is one before
			--RFO --RFO 						-- closing char
			--RFO --RFO 					end
			--RFO --RFO 					i := js.last_scan_position + 1
			--RFO --RFO 				end
			--RFO --RFO 			end
			--RFO --RFO 		end
			--RFO --RFO 		if not done then
			--RFO --RFO 			i := i + 1
			--RFO --RFO 		end
			--RFO --RFO 	end
			--RFO --RFO 	if not has_error then
			--RFO --RFO 		last_scan_position := i
			--RFO --RFO 	end
 			--RFO --RFO 	extract_attributes
			--RFO end
		end

	--|--------------------------------------------------------------

	init_objects_from_substream (v: STRING; spos, epos, lim: INTEGER)
-- N.B. This should NEVER be called
-- JSON object now uses JSON parser, bypassing this
			-- Array has been found within stream and has been 
			-- determined to contain JSON objects
			-- Extract each into Current
		require
			never: False
		local
			i, sp, ep, osp, oep, itype: INTEGER
			js: AEL_JSON_OBJECT
			is_q1, done: BOOLEAN
		do
			sp := spos
			ep := epos
			oep := ep
			from i := sp
			until i >= lim or has_error or done
			loop
				-- Loop through range instantiating items and
				-- adding them to Current
				-- Items are comma-separated, but commas can also be 
				-- embedded in quotes.  Because items must be 
				-- homogeneous, the first step is to find the first item 
				-- (if any) and determine its major type (num, str, obj)
				-- Then it's a matter of finding the rest of them, by type
				i := index_of_next (v, i)
				if i = 0 then
					-- Off end
					i := lim
				elseif i = lim then
					done := True
				else
					inspect v.item (i)
					when '{' then
						-- Array of objects
						itype := K_itype_object
					when '%'' then
						is_q1 := True
						itype := K_itype_string
					when '%"' then
						itype := K_itype_string
					else
						if v.item (i).is_digit then
							itype := K_itype_numeric
							-- Scan all to determine if int or float
						else
						end
					end
					inspect itype
					when K_itype_string then
						if is_q1 then
							osp := v.index_of ('%'', i)
						else
							osp := v.index_of ('%"', i)
						end
					when K_itype_numeric then
						osp := asr.index_of_next_digit (v, i)
					else
						osp := v.index_of ('{', i)
					end
					if osp = 0 then
						-- No more items
						i := ep
						done := True
					else
						oep := asr.index_of_matching_close (v, osp, lim, '\')
						if oep /= 0 then
							-- Create an anonymous object
							create js.make_from_substream ("", v, osp, oep)
							if js.has_error then
								copy_errors (js)
							else
								add_element_by_name (js, element_name (js))
								-- Last scan position is one before
								-- closing char
							end
							i := js.last_scan_position + 1
						end
					end
				end
				if not done then
					i := i + 1
				end
			end
			if not has_error then
				item_type := itype
				last_scan_position := i
			end
			extract_attributes
		end

--|========================================================================
feature -- Status
--|========================================================================

	element_name (v: like element_by_name): STRING
			-- Name/id by which element 'v' is known
		do
			Result := ""
			if attached {AEL_JSON_OBJECT} v as jo then
				Result := jo.name
			end
		end

	--|--------------------------------------------------------------

	elements_as_array_enabled: BOOLEAN = true

	count: INTEGER
			-- Number of items in Current
		do
			Result := serial_elements.count
		end

	is_empty: BOOLEAN
			-- Does Current have no elements?
		do
			Result := count = 0
		end

	--|--------------------------------------------------------------

	item_type: INTEGER
			-- Type of items in Current

	is_object_array: BOOLEAN
			-- Are items in Current objects?
		do
			inspect item_type
			when K_itype_string then
			when K_itype_integer, K_itype_float, K_itype_numeric then
			else
				-- Default is object
				Result := True
			end
		end

	is_string_array: BOOLEAN
			-- Are items in Current simple strings?
		do
			Result := item_type = K_itype_string
		end

	is_numeric_array: BOOLEAN
			-- Are items in Current numeric (integer or float, or combo)?
		do
			inspect item_type
			when K_itype_integer, K_itype_float, K_itype_numeric then
				Result := True
			else
			end
		end

	is_integer_array: BOOLEAN
			-- Are items in Current integers?
		do
			Result := item_type = K_itype_integer
		end

	is_float_array: BOOLEAN
			-- Are items in Current floats?
		do
			Result := item_type = K_itype_float
		end

	is_valid_array_item_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid array item type?
		do
			inspect v
			when K_itype_integer, K_itype_float, K_itype_numeric then
				Result := True
			when K_itype_string, K_itype_object then
				Result := True
			else
			end
		end

--|========================================================================
feature -- Status Setting
--|========================================================================

	set_item_type (v: INTEGER)
			-- Set item_type of Current to 'v'
		require
			valid: is_valid_array_item_type (v)
		do
			item_type := v
		ensure
			is_set: item_type = v
		end

--|========================================================================
feature -- Access
--|========================================================================

	items_typed: LIST [ANY]
			-- Contents of Current, presented as a list of items,
			-- where items have the item type (vs always being 
			-- JSON objects).  If item type is JSON object, then result 
			-- is same as items_as_arrayed_list
		local
			has_js_error: BOOLEAN
			ts: STRING
			jo: AEL_JSON_OBJECT
			oc: CURSOR
			slist: LINKED_LIST [STRING]
		do
			if item_type = K_itype_object or item_type = K_itype_undefined then
				Result := items_as_arrayed_list
			else
				if not attached private_json_elements as pel then
					Result := items_as_arrayed_list
				else
					create slist.make
					-- 'pel' is a linked list of JSON objects
					oc := pel.cursor
					from pel.start
					until pel.after or has_js_error
					loop
						jo := pel.item
						if not attached jo.attribute_values as avt then
							-- ERROR
							set_error_message ("{
Parse error in AEL_JSON_ARRAY.items_type
  attribute_values is Void
}")
						elseif avt.is_empty then
							-- ERROR
							set_error_message ("{
Parse error in AEL_JSON_ARRAY.items_type
  attribute_values is empty - expected single, simple type
}")
						else
							avt.start
							ts := avt.item_for_iteration
							slist.extend (ts)
						end
						pel.forth
					end
					if pel.valid_cursor (oc) then
						pel.go_to (oc)
					end
					inspect item_type
					when K_itype_string then
						-- Typed item is STRING, and is the value of the sole 
						-- attribute of the corresponding object
						-- Result is all set at this point
						Result := slist
					when K_itype_integer then
						-- Typed item is INTEGER, and is the value of the sole 
						-- attribute, as a string, of the corresponding 
						-- object
						-- Result list needs to have its items converted
						create {LINKED_LIST [INTEGER]}Result.make
						from slist.start
						until slist.after or has_js_error
						loop
							if not slist.item.is_integer then
								-- ERROR
								has_js_error := True
								set_error_message ("{
Error in AEL_JSON_ARRAY.items_type
  Expected integer value
  }")
							else
								Result.extend (slist.item.to_integer)
							end
							slist.forth
						end
					when K_itype_float, K_itype_numeric then
						-- Typed item is numeric, and is the value of the sole 
						-- attribute, as a string, of the corresponding object
						-- Result list needs to have its items converted
						create {LINKED_LIST [DOUBLE]}Result.make
						from slist.start
						until slist.after or has_js_error
						loop
							if not slist.item.is_double then
								-- ERROR
								has_js_error := True
								set_error_message ("{
Error in AEL_JSON_ARRAY.items_type
  Expected numeric point value
  }")
							else
								Result.extend (slist.item.to_double)
							end
							slist.forth
						end
					else
						Result := items_as_arrayed_list
					end
				end
			end
		end

	items_as_arrayed_list: ARRAYED_LIST [AEL_JSON_OBJECT]
			-- Contents of Current, presented as an arrayed list
		do
			create Result.make (count)
			if not is_empty then
				Result.fill (serial_elements)
			end
		end

	items_as_array: ARRAY [AEL_JSON_OBJECT]
			-- Contents of Current, presented as an array
		do
			Result := items_as_arrayed_list.to_array
		end

	items_sorted: SORTED_TWO_WAY_LIST [AEL_JSON_OBJECT]
			-- Contents of Current, presented as a sorted 2-way list
		do
			create Result.make
			if not is_empty then
				Result.fill (serial_elements)
			end
		end

	--|--------------------------------------------------------------

	i_th (v: INTEGER): AEL_JSON_OBJECT
			-- Item at 1-based index 'v'
		require
			valid_index: v > 0 and v <= count
		local
			oc: CURSOR
			se: like serial_elements
		do
			se := serial_elements
			oc := se.cursor
			se.go_i_th (v)
			Result := se.item
			if se.valid_cursor (oc) then
				se.go_to (oc)
			end
		ensure
			exists: attached Result
		end

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: AEL_JSON_OBJECT)
		require
			exists: v /= Void
		do
			serial_elements.extend (v)
		end

	add_string_item (v: STRING)
			-- Add a string item to Current by encapsulating 'v'
			-- into a JSON object and adding that
			-- The string item value becomes both the key and the item 
			-- in the object's attributes
		require
			valid_type: is_string_array or (item_type = K_itype_undefined)
		local
			jso: AEL_JSON_OBJECT
		do
			set_item_type (K_itype_string)
			create jso.make_anonymous
			jso.add_attribute (v, v)
			extend (jso)
		end

	add_numeric_item (v: STRING)
			-- Add a numeric item to Current by encapsulating 'v'
			-- into a JSON object and adding that
			-- N.B. at present, numerics are treated as string items by 
			-- parser
			-- The string item value becomes both the key and the item 
			-- in the object's attributes
		require
			valid_type: is_numeric_array or (item_type = K_itype_undefined)
		local
			jso: AEL_JSON_OBJECT
		do
			set_item_type (K_itype_numeric)
			create jso.make_anonymous
			jso.add_attribute (v, v)
			extend (jso)
		end

--|========================================================================
feature -- Constants
--|========================================================================

	K_itype_undefined: INTEGER = 0
	K_itype_object: INTEGER = 1
	K_itype_string: INTEGER = 2
	K_itype_numeric: INTEGER = 3
	K_itype_integer: INTEGER = 4
	K_itype_float: INTEGER = 5

	--|--------------------------------------------------------------
invariant
--RFO 	no_private_elements: private_json_elements = Void

end -- class AEL_JSON_ARRAY
