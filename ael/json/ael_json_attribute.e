class AEL_JSON_ATTRIBUTE
-- An attribute in JSON syntax (a tag-value)

inherit
	AEL_JSON_COMPONENT
		undefine
			copy, is_equal, default_create
		end
	AEL_SPRT_ATTRIBUTE

create
	make_with_tag_and_value, make_from_stream

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_stream (v: STRING; spos: INTEGER; ec: CELL [INTEGER])
		require
			exists: v /= Void
			valid_start: spos <= v.count
		do
			default_create
			init_from_stream (v, spos, ec)
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_stream (v: STRING; sp: INTEGER; ec: CELL [INTEGER])
			-- Initialize Current from stream 'v' starting at 'sp'
			-- Stream has the form:
			-- \"<identifier>\"':'<ws>*<value>
			--
			-- <identifier>
			--   is a case-insensitive identifier denoting
			--   the attribute's name (tag)
			-- <ws>
			--   is arbitrary whitespace, including newlines
			-- <value>
			--   is either a quoted string, an integer or a real value
			--
			-- A backslash ('\') is used as the escape character
			--
			-- If initialized successfully, and 'ec' is non-Void, then
			-- set the item in 'ec' to the last character position in 
			-- 'v' consumed by this routine (set to zero if unsuccessful)
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			start_is_attribute: v.item (sp) = '%"' or v.item (sp) = '%''
		local
			spos, epos, t0, t1, i, lim: INTEGER
			qc: CHARACTER
			rx: AEL_SPRT_REGEXP
		do
			tag.wipe_out
			value.wipe_out
			if ec /= Void then
				lim := ec.item
				ec.put (0)
			else
				lim := v.count
			end
			rx := jconst.rexp_json_tag
			rx.search_forward (v, sp, lim)
			if not rx.found then
				set_error_message ("Malformed JSON tag")
			else
				t0 := rx.start_position
				t1 := rx.end_position
				-- Strip off quotes and colon
				set_tag (v.substring (t0 + 1, t1 - 2))
				-- Now extract the value
				spos := asr.index_of_next_non_whitespace (v, t1 + 1, True)
				if spos = 0 then
					set_error_message ("JSON attribute is truncated")
				else
					qc := v.item (spos)
					inspect qc
					when '%"', '%'' then
						--  A quoted value
						epos := asr.index_of_matching_close (v, spos, lim, '\')
						if epos = 0 then
							set_error_message ("Attribute tag is missing closing quote")
						elseif epos = spos + 1 then
							-- Empty string; do nothing
						else
							asr.append_substring (value, v, spos + 1, epos - 1)
						end
					else
						-- An unquoted value, terminated by ',', '}' or 
						-- whitespace
						epos := 0
						from i := spos + 1
						until i > lim
						loop
							inspect v.item (i)
							when ',', '}', '%N', '%T', '%R', ' ' then
								epos := i - 1
							else
								i := i + 1
							end
						end
						if epos = 0 then
							epos := lim
							asr.append_substring (value, v, spos, epos)
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	obs_init_from_stream (v: STRING; sp: INTEGER; ec: CELL [INTEGER])
			-- Initialize Current from stream 'v' starting at 'sp'
			-- Stream has the form:
			-- \"<identifier>\"':'<ws>*<value>
			--
			-- <identifier>
			--   is a case-insensitive identifier denoting
			--   the attribute's name (tag)
			-- <ws>
			--   is arbitrary whitespace, including newlines
			-- <value>
			--   is either a quoted string, an integer or a real value
			--
			-- A backslash ('\') is used as the escape character
			--
			-- If initialized successfully, and 'ec' is non-Void, then
			-- set the item in 'ec' to the last character position in 
			-- 'v' consumed by this routine (set to zero if unsuccessful)
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			start_is_attribute: v.item (sp) = '%"' or v.item (sp) = '%''
		local
			cpos, spos, epos, lim: INTEGER
			qc: CHARACTER
		do
			tag.wipe_out
			value.wipe_out
			if ec /= Void then
				lim := ec.item
				ec.put (0)
			else
				lim := v.count
			end
			qc := v.item (sp)
			spos := sp + 1
			epos := asr.substring_index_of_unescaped_char (v, qc, '\', spos, lim)
			if epos = 0 then
				set_error_message ("Missing closing quote for attribute tag")
			else
				set_tag (v.substring (spos, epos - 1))
				spos := epos + 1
			end
			if not has_error then
				cpos := v.index_of (':', spos)
				if cpos = 0 then
					set_error_message ("Missing colon separator for attribute")
				end
			end
			-- Now extract the value
			if not has_error then
				spos := asr.index_of_next_non_whitespace (v, cpos + 1, True)
				if spos = 0 then
					set_error_message ("Premature end to attribute")
				else
					if v.item (spos).is_digit then
						-- Numeric value
						epos := asr.index_of_next_whitespace (v, spos, True)
						if epos = 0 then
							epos := lim
						else
							epos := epos - 1
						end
						value.append (v.substring (spos, epos))
						if not value.is_double then
							set_error_message ("Malformed numeric value")
						end
					elseif v.item (spos) = qc then
						-- Quoted string value
						-- Skip over opening quote
						spos := spos + 1
						epos := asr.substring_index_of_unescaped_char (
							v, qc, '\', spos, lim)
						if epos = 0 then
							set_error_message ("Missing closing quote for attribute value")
						else
							set_value (v.substring (spos, epos - 1))
							if ec /= Void then
								ec.put (epos)
							end
						end
					else
						set_error_message ("Malformed attribute value")
					end
				end
			end
		end

--|========================================================================
feature {NONE} -- Services
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	jconst: AEL_JSON_CONSTANTS
		once
			create Result
		end

end -- class AEL_JSON_ATTRIBUTE
