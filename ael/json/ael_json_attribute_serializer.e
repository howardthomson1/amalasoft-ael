class AEL_JSON_ATTRIBUTE_SERIALIZER

inherit
	AEL_JSON_CONSTANTS
		undefine
			default_create, copy
		end
	AEL_SPRT_ATTRIBUTE_SERIALIZER
		redefine
			is_valid_input_stream, attribute_by_name,
			serial_out_prolog, serial_out_epilog,
			elements_out_prolog, elements_out_epilog,
			attributes_serial_out, shared_flags, shared_patterns
		end

create
	default_create

--|========================================================================
feature -- Access
--|========================================================================

 	attribute_by_name (al: like attrs; v: STRING): detachable AEL_JSON_ATTRIBUTE
			-- Attribute name and value associated with label 'v', if any
		do
			if attached {like attribute_by_name} Precursor (al, v) as ta then
				Result := ta
			end
		end

--|========================================================================
feature -- External representation
--|========================================================================

	serial_out_prolog (cl: like client): STRING
			-- String that precedes attributes in serial output
		local
			sp: AEL_SPRT_SERIAL_PARAMS
			depth_debug: BOOLEAN -- set to true to test depth setting
		do
			sp := cl.serial_params
			if cl.tag_label.is_empty or sp.tag_label_suppressed then
				if depth_debug then
				Result := asr.aprintf (
					"%%s{ %"current_serial_depth%": %"%%d%",%N",
					<< left_pad, current_serial_depth >>)
				else
					Result := asr.aprintf (
						"%%s{%N", << left_pad >>)
				end
			else
				if element_tags_are_quoted then
					if depth_debug then
						Result := asr.aprintf (
							"%%s%"%%s%": { %"current_serial_depth%": %"%%d%",%N",
							<< left_pad, cl.tag_label, current_serial_depth >>)
					else
						Result := asr.aprintf (
							"%%s%"%%s%": {%N",
							<< left_pad, cl.tag_label >>)
					end
				else
					if depth_debug then
						Result := asr.aprintf (
							"%%s%%s: { %"current_serial_depth%": %"%%d%",%N",
							<< left_pad, cl.tag_label, current_serial_depth >>)
					else
						Result := asr.aprintf (
							"%%s%%s: {%N",
							<< left_pad, cl.tag_label >>)
					end
				end
			end
		end

	serial_out_epilog (cl: like client): STRING
			-- String that follows attributes and elements in serial output
		do
			Result := "%N" + left_pad + "}"
		end

	--|--------------------------------------------------------------

	elements_out_prolog (sp: AEL_SPRT_SERIAL_PARAMS): STRING
			-- String that precedes elements in serial output
		local
			oc: CHARACTER
			fmt: STRING
		do
			if sp.elements_as_array_enabled then
				--if cl.elements_tag_label.is_empty then
				-- Use array notation
				oc := '['
			else
				oc := '{'
			end
			if element_tags_are_quoted then
				fmt := "%%s%"%%s%": %%c%N"
			else
				fmt := "%%s%%s: %%c%N"
			end
			Result := asr.aprintf (fmt, <<left_pad, sp.elements_tag_label, oc>>)
		end

	elements_out_epilog (sp: AEL_SPRT_SERIAL_PARAMS): STRING
			-- String that follows elements in serial output
		do
			if sp.elements_as_array_enabled then
				--if cl.elements_tag_label.is_empty then
				-- Use array notation
				Result := "%N" + left_pad + "]"
			else
				Result := "%N" + left_pad + "}"
			end
		end

	--|--------------------------------------------------------------

	attributes_serial_out (cl: like client; al: like attrs): STRING
			-- Serialized representation of attributes
		do
			increase_indent
			Result := Precursor (cl, al)
			decrease_indent
		end

	attribute_out (sp: AEL_SPRT_SERIAL_PARAMS; t, v: STRING): STRING
			-- Attribute, represented by tag 't' and value 'v' in
			-- string serialized form
		do
			if attribute_tags_are_quoted then
				Result := asr.aprintf ("%"%%s%": %"%%s%"", << t, v >>)
			else
				Result := asr.aprintf ("%%s: %"%%s%"", << t, v >>)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_input_stream (v: STRING): BOOLEAN
			-- Is the string 'v' a valid stream from which to initialize 
			-- attributes associated with Current?
		do
			Result := Precursor (v)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	shared_flags: HASH_TABLE [BOOLEAN, STRING]
			-- Flags shared by all instances of this class or its 
			-- descendents
		once
			create Result.make (11)
			Result.force (True, "element_tags_are_quoted")
			Result.force (True, "attribute_tags_are_quoted")
			--RFO let descendents decide
			--Result.force (True, "include_element_id")
		end

	--|--------------------------------------------------------------

	shared_patterns: HASH_TABLE [STRING, STRING]
			-- Patterns (strings) shared by all instances of this
			-- class or its descendents
		once
			create Result.make (11)
			Result.force ("", "left_pad")
			Result.force ("0", "element_id")
			Result.force (",%N", "element_separator")
		end

end -- class AEL_JSON_ATTRIBUTE_SERIALIZER
