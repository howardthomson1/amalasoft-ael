class AEL_JSON_CONSTANTS

--|========================================================================
feature -- Validation
--|========================================================================

	string_is_json (v: STRING): BOOLEAN
			-- Is the string 'v' a JSON stream?
		local
			rexp: AEL_SPRT_REGEXP
		do
			if v /= Void and then not v.is_empty then
				rexp := rexp_json_stream
				rexp.search_forward (v, 1, v.count)
				Result := rexp.found
			end
		end

--|========================================================================
feature -- Patterns
--|========================================================================

	Ks_pat_json_tag: STRING = "{
\"?(\w|-|/|\.)+\"?:
}"

	rexp_json_stream: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON stream
		do
			create Result.make ("\{.+\}$")
			Result.set_dotall (True)
		end

	rexp_json_anon_obj: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON stream
		do
			create Result.make ("\{.+\}")
			Result.set_dotall (True)
		end

	rexp_json_obj: AEL_SPRT_REGEXP
			-- A regular expression denoting an embedded JSON object
			-- occurring before another item item in another JSON object
		do
			create Result.make (Ks_pat_json_tag + "\s*\{.*\}\s*\,")
			Result.set_dotall (True)
		end

	rexp_json_obj_last: AEL_SPRT_REGEXP
			-- A regular expression denoting an embedded JSON object 
			-- occurring as the last item in another JSON object
		do
			create Result.make (Ks_pat_json_tag + "\s*\{.*\}")
			Result.set_dotall (True)
		end

	rexp_json_array: AEL_SPRT_REGEXP
			-- A regular expression denoting an embedded JSON array
			-- occurring before another item in another JSON object
		do
			create Result.make (Ks_pat_json_tag + "\s*\[.*\]\s*\,")
			Result.set_dotall (True)
		end

	rexp_json_array_last: AEL_SPRT_REGEXP
			-- A regular expression denoting an embedded JSON array 
			-- occurring as the last item in another JSON object
		do
			create Result.make (Ks_pat_json_tag + "\s*\[.*\]")
			Result.set_dotall (True)
		end

	rexp_json_tag: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON tag
		do
			create Result.make (Ks_pat_json_tag)
		end

	rexp_json_attr: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON attribute
		do
			create Result.make (Ks_pat_json_tag + "\s*\%".*\%"")
		end

	rexp_json_i_attr: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON integer attribute
		do
			create Result.make (Ks_pat_json_tag + "\s*\d+")
		end

	rexp_json_r_attr: AEL_SPRT_REGEXP
			-- A regular expression denoting a JSON real attribute
		do
			create Result.make (Ks_pat_json_tag + "\s*\d*(\.?\d+)")
		end

end -- class AEL_JSON_CONSTANTS
