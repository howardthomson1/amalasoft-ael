class AEL_JSON_DIGEST
-- A JSON-encoded digest of objects

inherit
	AEL_JSON_CONSTANTS
		undefine
			copy, is_equal
		end
	TWO_WAY_LIST [AEL_JSON_OBJECT]
		redefine
			make
		end

create
	make, make_from_stream, make_from_substream
create {AEL_JSON_DIGEST}
	make_sublist

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			Precursor {TWO_WAY_LIST}
		end

	make_from_stream (nm, v: STRING)
		require
			stream_exists: v /= Void
		do
			make_from_substream (nm, v, 1, v.count)
		end

	--|--------------------------------------------------------------

	make_from_substream (nm, v: STRING; sp, ep: INTEGER)
		require
			stream_exists: v /= Void
			valid_start: sp >= 0 and sp <= ep
			valid_end: ep >= sp and ep <= v.count
		do
--RFO TODO, why were these 2 lones commented out?
-- 			make (nm)
-- 			init_from_substream (v, sp, ep)
		end

--|========================================================================
feature -- Status
--|========================================================================

	object_by_name (v: STRING): detachable AEL_JSON_OBJECT
			-- Element, if a JSON object, with name 'v', if any
			-- Not all objects are named
		do
			if attached {like object_by_name} element_by_name (v) as obn then
				Result := obn
			end
		end

	array_by_name (v: STRING): detachable AEL_JSON_ARRAY
			-- Element, if a JSON array, with name 'v', if any
			-- Not all arrays are named
		do
			if attached {like array_by_name} element_by_name (v) as obn then
				Result := obn
			end
		end

	--|--------------------------------------------------------------

	element_by_name (v: STRING): detachable AEL_JSON_OBJECT
			-- Element with name 'v', if any
			-- Not all elements are named
		local
			oc: like cursor
		do
			oc := cursor
			from start
			until exhausted or attached Result
			loop
				if item.name ~ v then
					Result := item
				else
					forth
				end
			end
			go_to (oc)
		end

	--|--------------------------------------------------------------
invariant

end -- class AEL_JSON_DIGEST
