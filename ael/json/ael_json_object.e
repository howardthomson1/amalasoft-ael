class AEL_JSON_OBJECT
-- An object encoded from/into JSON format

inherit
	AEL_JSONABLE
		rename
			make as js_anon_make,
			make_from_stream as js_anon_make_from_stream,
			make_from_substream as js_anon_make_from_substream,
			elements as objects
		undefine
			is_equal
		redefine
			serial_elements, set_attributes_from_current_values,
			json_out_supported, initialize_serial_params,
			make_elements, has_elements, init_from_substream,
			cached_serial_elements
		end
	AEL_SPRT_IDENTIFIABLE_ATTRIBUTABLE
		rename
			make as js_anon_make,
			make_from_stream as js_anon_make_from_stream,
			make_from_substream as js_anon_make_from_substream,
			identifier as name,
			set_identifier as set_name,
			serial_out_supported as json_out_supported,
			elements as objects
		undefine
			is_equal,
			core_make, element_by_name, set_attributes_from_current_values,
			serial_elements, json_out_supported, make_elements, has_elements,
			initialize_serial_params, cached_serial_elements
		end
	COMPARABLE

create
	make, make_anonymous, make_from_stream, make_from_substream

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (nm: STRING)
			-- Create Current with name 'nm'
		do
			if is_valid_identifier (nm) then
				set_name (nm)
			else
				name := ""
			end
			js_anon_make
		end

	make_anonymous
			-- Create Current with no name
		do
			create name.make (0)
			js_anon_make
		end

	--|--------------------------------------------------------------

	make_from_stream (nm, v: STRING)
		require
			stream_exists: v /= Void
		do
			make_from_substream (nm, v, 1, v.count)
		end

	--|--------------------------------------------------------------

	make_from_substream (nm, v: STRING; sp, ep: INTEGER)
		require
			stream_exists: v /= Void
			valid_start: sp >= 0 and sp <= ep
			valid_end: ep >= sp and ep <= v.count
		do
			make (nm)
			init_from_substream (v, sp, ep)
		end

	--|--------------------------------------------------------------

	make_elements
		do
			Precursor {AEL_JSONABLE}
			--private_json_elements := Void
			--create serial_elements.make
			set_element_structure
		end

--|========================================================================
feature -- Initialize
--|========================================================================

	init_from_substream (v: STRING; spos, epos: INTEGER)
		do
			--| Simple switch for comparing/debugging new vs old parser
			if True then
				new_init_from_substream (v, spos, epos)
			else
				Precursor (v, spos, epos)
			end
		end

	new_init_from_substream (v: STRING; spos, epos: INTEGER)
			-- A JSON stream is bounded by '{' '}' pairs, possibly 
			-- preceded by a quoted name and colon.  JSON streams 
			-- can be nested (corresponding to object nesting)
		local
			parser: AEL_JSON_PARSER
		do

			last_scan_position := 0
			create parser.make_for_object (Current, v, spos, epos)
			if custom_label_special_characters /= Void then
				parser.add_special_label_characters(
					custom_label_special_characters, True)
			end
			--| Set trace or debug flags before calling parse
			--parser.enable_db_print
			--parser.enable_db_print_ch
			--parser.enable_trace
			parser.parse
			if parser.has_error then
				copy_errors (parser)
			else
				last_scan_position := parser.current_position
			end
		end

	--|--------------------------------------------------------------

	initialize_serial_params
		do
			Precursor {AEL_JSONABLE}
			serial_params.set_has_elements (True)
		end

--|========================================================================
feature -- Status
--|========================================================================

	cached_serial_elements: like serial_elements
			-- Serializable subordinate elements, if any,
			-- cached as a result of calling 'update_elements',
			-- if 'elements_are_from_cache' is True
			--
			-- In descendent, typical implementation of this feature 
			-- would be as attribute
		do
			create {LINKED_LIST [AEL_JSON_OBJECT]} Result.make
		end

	serial_elements: LINKED_LIST [AEL_JSON_OBJECT]
			-- Subordinate objects
		do
			check attached private_json_elements as lpe then
				Result := lpe
			end
		end

	name: STRING
			-- Name by which current is known
			--
			-- N.B. while this is typically the same as the 'name' 
			-- attribute, it might not be, and indeed this value and an 
			-- attribute called 'name' could be set independently.
			-- When created from a stream, the parser looks for an empty 
			-- name feature and if it finds a 'name' attribute, it 
			-- assigns its value to this feature.  If, however, this 
			-- feature is non-empty, then the parser will not clobber 
			-- its value, even if there exists a 'name' attribute in the 
			-- JSON stream.

	json_out_supported: BOOLEAN = True

	has_elements: BOOLEAN
			-- Does Current have the potential for having subordinate elements?
		do
			Result := serial_elements /= Void
		end

	has_any_objects: BOOLEAN
			-- Does Current have any actual objects?
		local
			ol: like serial_elements
		do
			ol := serial_elements
			Result := ol /= Void and then not ol.is_empty
		end

	--|--------------------------------------------------------------

	object_by_name (v: STRING): detachable AEL_JSON_OBJECT
			-- Element, if a JSON object, with name 'v', if any
		do
			if attached {like object_by_name} element_by_name (v) as obn then
				Result := obn
			end
		end

	has_object_by_name (v: STRING): BOOLEAN
			-- Does Current have an object (superficially) by name 'v'?
		do
			-- There can be multiple anonymous objects ????
			if not v.is_empty then
				Result := object_by_name (v) /= Void
			end
		end

	has_object (v: AEL_JSON_OBJECT): BOOLEAN
			-- Does Current have the object 'v'
		do
			Result := serial_elements.has (v)
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_name (v: STRING)
		do
			name := v.twin
		end

	extract_attributes
			-- Attributes are already recorded in 'attribute_values' and can 
			-- be accessed by name via 'value_for_attribute'.
			-- Redefine if in-class attributes are necessary.
			-- Example:
			--   name := value_for_attribute ("name")
		do
		end

	--|--------------------------------------------------------------

	set_attributes_from_current_values
			-- Add to attributes for each relevant feature of Current
			-- Example:
			--  add_attribute ("name", name)
			--
			-- Intent is to ensure that 'attribute_values' is in synch with 
			-- the values of any in-class attributes or other 
			-- serializable values
		do
			if attribute_values = Void then
				create attribute_values.make (11)
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_object (v: AEL_JSON_OBJECT)
			-- Add a named object to Current
		require
			exists: v /= Void
			is_named: attached v.name as vnm and then not vnm.is_empty
			is_new: not has_object_by_name (v.name)
		do
			objects.extend (v)
		ensure
			added: has_object_by_name (v.name)
		end

	add_unnamed_object (v: AEL_JSON_OBJECT)
			-- Add an unnamed object to Current
		require
			exists: v /= Void
			unnamed: (not attached v.name as vnm) or else vnm.is_empty
			is_new: not has_object (v)
		do
			objects.extend (v)
		ensure
			added: has_object (v)
		end

--|========================================================================
feature -- Configuration
--|========================================================================

	required_attribute_labels: ARRAY [STRING]
			-- Labels denoting required attributes
			-- 'attribute_values' should have entries for each label if 
			-- Current is valid
		do
			Result := {ARRAY [STRING]} <<>>
		ensure
			exists: Result /= Void
		end

	optional_attribute_labels: ARRAY [STRING]
			-- Labels denoting attributes not required but permitted
			-- 'attribute_values' can have entries for any of these labels,
			-- but must not have any entries for labels not in here
			-- or in 'required_attribute_labels' to be valid
		do
			Result := {ARRAY [STRING]} <<>>
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Validation
--|========================================================================

	has_all_required_attributes: BOOLEAN
			-- Does Current have all required attributes defined?
		local
			i, lim: INTEGER
			ra: like required_attribute_labels
			lb: STRING
		do
			if attached attribute_values as atl then
				Result := True
				ra := required_attribute_labels
				lim := ra.upper
				from i := ra.lower
				until i > lim or not Result
				loop
					lb := ra.item (i)
					if not attribute_values.has (lb) then
						Result := False
					end
					i := i + 1
				end
			end
		end

	has_rogue_attributes: BOOLEAN
			-- Does Current have any attributes it doesn't expect?
		local
			i, lim: INTEGER
			ra: like required_attribute_labels
			oa: like optional_attribute_labels
			lb: STRING
			num_found: INTEGER
		do
			if attached attribute_values as atl then
				ra := required_attribute_labels
				lim := ra.upper
				from i := ra.lower
				until i > lim
				loop
					lb := ra.item (i)
					if atl.has (lb) then
						num_found := num_found + 1
					end
					i := i + 1
				end
				oa := optional_attribute_labels
				lim := oa.upper
				from i := oa.lower
				until i > lim
				loop
					lb := oa.item (i)
					if atl.has (lb) then
						num_found := num_found + 1
					end
					i := i + 1
				end
				Result := atl.count > num_found
			end
		end

	add_special_label_characters (ca: detachable ARRAY [CHARACTER])
			-- Create from 'ca' a collection of non-letter, non-number 
			-- characters that will be valid in identifiers characters.
			--
			-- Default characters are also included if 'df' is True
		do
			if ca /= Void then
				custom_label_special_characters := ca
			end
		end

	custom_label_special_characters: detachable ARRAY [CHARACTER]
			-- Non-alphanumeric characters permitted in identifiers
			-- Empty by default, but can be filled by calling 
			-- 'add_special_label_characters'
		note
			option: stable
			attribute
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
			--
			-- COMPARISON IS BY NAME ONLY, for sorting purposes
			--
			-- Note well that is_equal is also redefined this way!!!!
		do
			Result := name < other.name
		ensure then
			asymmetric: Result implies not (other < Current)
		end

	--|--------------------------------------------------------------
invariant
--	no_private_elements: private_json_elements = Void

end -- class AEL_JSON_OBJECT
