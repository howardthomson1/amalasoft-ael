--|----------------------------------------------------------------------
--| Copyright (c) 1995-2011, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A simple JSON (RFC4627) text analyzer
--|
--|----------------------------------------------------------------------

class AEL_JSON_PARSER

inherit
	AEL_SPRT_MULTI_FALLIBLE
		redefine
			default_create
		end

create
	default_create, make_with_input, make_for_object, make_and_parse

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	make_and_parse (obj: like digest; buf: STRING; sp, ep: INTEGER)
			-- Create Current with input stream from buffer 'buf' between
			-- start position 'sp' and end position 'ep'
			--
			-- If 'obj' is non-Void, then initialize object 'obj' from
			-- buffer contents, else leave create digest/object in
			-- 'digest'.
			--
			-- Parsing is initiated by this routine
		require
			has_input: not buf.is_empty
			valid_start: sp >= 1
			valid_end: ep <= buf.count and ep >= sp
		do
			if attached obj then
				digest := obj
			end
			make_with_input (buf, sp, ep)
			parse
		end

	--|--------------------------------------------------------------

	make_with_input (v: STRING; sp, ep: INTEGER)
			-- Create Current with input stream from buffer 'buf' between
			-- start position 'sp' and end position 'ep'
			--
			-- 'parse' must be called subsequently
		require
			exists: v /= Void and then not v.is_empty
			valid_start: sp >= 1
			valid_end: ep <= v.count and ep >= sp
		do
			default_create
			input_buffer := v
			maximum_character_position := ep
			current_position := sp - 1
		end

	--|--------------------------------------------------------------

	make_for_object (obj: AEL_JSON_OBJECT; buf: STRING; sp, ep: INTEGER)
			-- Create Current with input stream from buffer 'buf' between
			-- start position 'sp' and end position 'ep'
			-- Resulting digest will initialize object 'obj
			--
			-- 'parse' must be called subsequently
		require
			object_exists: obj /= Void
			buffer_exists: buf /= Void and then not buf.is_empty
			valid_start: sp >= 1
			valid_end: ep <= buf.count and ep >= sp
		do
			digest := obj
			make_with_input (buf, sp, ep)
		end

	--|--------------------------------------------------------------

	default_create
		do
			Precursor
			create trace_indent.make (32)
		end

--|========================================================================
feature -- Parse invocation
--|========================================================================

	set_input (v: STRING; sp, ep: INTEGER)
			-- Set input stream to buffer 'v' between
			-- start position 'sp' and end position 'ep'
		require
			exists: v /= Void and then not v.is_empty
			valid_start: sp >= 1
			valid_end: ep <= v.count and ep >= sp
		do
			input_buffer := v
			maximum_character_position := ep
			current_position := sp - 1
		end

	--|--------------------------------------------------------------

	parse
			-- Parse input stream leaving instantiated digest or object 
			-- in 'digest'
		require
			has_input: has_input
		local
			c: CHARACTER
			tag: detachable STRING
		do
			-- First need to know if we have a digest/anonymous object 
			-- form (starting with '{'), or a named object form (starting 
			-- with a label)
 			get_next_non_whitespace
 			c := current_character
			if  c = '{' then
				-- A digest or anonymous object; valid start
			else
				if is_quote (c) then
					-- Quoted name tag; possible valid start
					tag := quoted_identifier_from_stream
				elseif c.is_alpha then
					-- Unquoted name tag; possible valid start
					tag := unquoted_identifier_from_stream
				else
					set_parse_error ("Invalid object identifier")
				end
				if not has_error then
					if digest /= Void and tag /= Void then
						if digest.name /= Void and then digest.name /~ tag then
							set_parse_error ("Object name and label mismatch")
						else
							-- TODO
							-- Set digest name to label
						end
					end
				end
				if not has_error then
					if not read_ahead then
						get_next_non_whitespace
					else
						read_ahead := False
					end
					c := current_character
					if c /= ':' then
						set_parse_error ("Unexpected character after identifier")
					end
				end
				if not has_error then
					-- Found the tag and ':', now need the next token 
					-- to be the opening bracket
					get_next_non_whitespace
					c := current_character
					if c /= '{' then
						set_parse_error ("Unexpected start-of-object character")
					end
				end
			end
			if not has_error then
				if digest = Void then
					if tag /= Void then
						create digest.make (tag)
					else
						create digest.make_anonymous
					end
				end
			end
			if not has_error and digest /= Void then
				parse_object (digest)
			end
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	parse_object (obj: AEL_JSON_OBJECT)
			-- Parse the json input stream in the object state
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: current_character = '{'
			object_exists: obj /= Void
		local
			c: CHARACTER
			done: BOOLEAN
			tobj: AEL_JSON_OBJECT
			jc: like named_element_from_stream
		do
			trace_in ("parse_object")
			from
			until has_error or (not has_input) or done
			loop
				if not read_ahead then
					get_next_non_whitespace
				else
					-- Use the already-read char
					read_ahead := False
				end
				if has_input then
					c := current_character
					if not has_error and not c.is_space then
						inspect c
						when '{' then
							-- Subordinate anonymous object start
							create tobj.make_anonymous
							obj.add_unnamed_object (tobj)
							parse_object (tobj)
						when '}' then
							done := True
						when '[' then
							-- Anonymous array start
						when ']' then
							set_parse_error ("Unexpected character")
						when ',' then
							-- Attribute separator; fall through
						else
							if is_quote (c) or c.is_alpha then
								-- Start of an identifier
								jc := named_element_from_stream
								if not has_error then
									if attached {AEL_JSON_OBJECT} jc as jo then
										obj.add_object (jo)
									elseif attached {AEL_JSON_ATTRIBUTE} jc as ja then
										if (obj.name.is_empty and then
											ja.tag ~ "name" and not ja.value.is_empty) then
												-- RFO TODO should we set the name?  
												-- Probably yes.
												obj.set_name (ja.value)
										end
										obj.add_attribute (ja.tag, ja.value)
									else
										set_parse_error ("Unexpected named element type")
									end
								end
							else
								set_parse_error ("Unexpected character")
							end
						end
					end
				end
			end
			trace_out ("parse_object")
		end

	--|--------------------------------------------------------------

	parse_array (array: AEL_JSON_ARRAY)
			-- Parse the json input stream in the array state, building
			-- contents of 'array'
			-- An array can contain objects or simple values like 
			-- strings or numerics
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: current_character = '['
			array_exists: array /= Void
		local
			c: CHARACTER
			ts: STRING
			done: BOOLEAN
			tobj: AEL_JSON_OBJECT
			tarray: AEL_JSON_ARRAY
			item_type: INTEGER
		do
			trace_in ("parse_array")
			from
			until has_error or not has_input or done
			loop
				if not read_ahead then
					get_next_non_whitespace
				else
					-- Use the already-read char
					read_ahead := False
				end
				if not has_input then
					set_parse_error ("Unexpected end of input in array parse")
				else
					c := current_character
					inspect c
					when '{' then
						-- start of anonymous object item
						if item_type /= 0 and item_type /= K_jst_object then
							set_parse_error ("Array has mixed item types")
						else
							item_type := K_jst_object
							create tobj.make_anonymous
							array.extend (tobj)
							parse_object (tobj)
						end
					when '}' then
						set_parse_error ("Unexpected character")
					when '[' then
						-- Start of anonymous array item
						if item_type /= 0 and item_type /= K_jst_array then
							set_parse_error ("Array has mixed item types")
						else
							item_type := K_jst_array
							create tarray.make_anonymous
							array.extend (tarray)
							parse_array (tarray)
						end
					when ']' then
						-- End of current array
						done := True
					when ',' then
						-- Item separator, skip
					when '%"', '%'' then
						-- quoted String, NOT object tag
						if item_type /= 0 and item_type /= K_jst_string then
							set_parse_error ("Array has mixed item types")
						else
							item_type := K_jst_string
							ts := quoted_value_from_stream
							array.add_string_item (ts)
						end
					else
						if c.is_space then
							-- Skip
						elseif c.is_hexa_digit then
							if item_type /= 0 and item_type /= K_jst_numeric then
								set_parse_error ("Array has mixed item types")
							else
								item_type := K_jst_numeric
								ts := unquoted_value_from_stream
								array.add_numeric_item (ts)
							end
						else
							set_parse_error ("Unexpected character")
						end
					end
				end
			end
			trace_out ("parse_array")
		end

	--|--------------------------------------------------------------

	parse_attribute  (tag: STRING)
			-- Parse the json input stream in the attribute state
		require
			no_errors: not has_error
			has_input: has_input
		local
			c: CHARACTER
			ts: STRING
			attr: AEL_JSON_ATTRIBUTE
		do
			trace_in ("parse_attribute")
			c := current_character
			if is_quote (c) then
				ts := quoted_value_from_stream
				create attr.make_with_tag_and_value (tag, ts)
			else
				-- An unquoted value
				ts := unquoted_value_from_stream
				create attr.make_with_tag_and_value (tag, ts)
			end
			trace_out ("parse_attribute")
		end

	--|--------------------------------------------------------------

	named_element_from_stream: detachable AEL_JSON_COMPONENT
			-- Extract from the input stream the named element (object, 
			-- array or attribute) -- starting at the current position
			-- Name (tag) might be quoted or not
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: is_quote (current_character) or current_character.is_alpha
		local
			c: CHARACTER
			tag, ts: STRING
			obj: AEL_JSON_OBJECT
			array: AEL_JSON_ARRAY
			attr: AEL_JSON_ATTRIBUTE
		do
			trace_in ("name_element")
			c := current_character
			if is_quote (c) then
				tag := quoted_identifier_from_stream
			else
				tag := unquoted_identifier_from_stream
			end
			if not has_error then
				-- tag needs a following ':'
				if not read_ahead then
					get_next_non_whitespace
				else
					read_ahead := False
				end
				c := current_character
				if c /= ':' then
					set_parse_error ("Unexpected character after identifier")
				end
			end
			if not has_error then
				-- Found the tag and ':', now need the value
				-- Can be an object, an array, a string, boolean 
				-- or a numeric
				get_next_non_whitespace
				c := current_character
				if c = '{' then
					-- Named object
					create obj.make (tag)
					parse_object (obj)
					Result := obj
				elseif c = '[' then
					-- Named array
					create array.make (tag)
					parse_array (array)
					Result := array
				elseif is_quote (c) then
					-- A quoted value
					ts := quoted_value_from_stream
					create attr.make_with_tag_and_value (tag, ts)
					Result := attr
				else
					-- An unquoted value
					ts := unquoted_value_from_stream
					create attr.make_with_tag_and_value (tag, ts)
					Result := attr
				end
			end
		ensure
			exists: not has_error implies Result /= Void
		end

	--|--------------------------------------------------------------

	quoted_identifier_from_stream: STRING
			-- Extract from the input stream the quoted identifier
			-- starting at the current position, excluding the enclosing 
			-- quotes
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: is_quote (current_character)
		local
			c, qc: CHARACTER
			cp: INTEGER
		do
			trace_in ("quoted_id")
			qc := current_character
			create Result.make (32)
			from cp := 1
			until has_error or not has_input or qc = '%U'
			loop
				get_next_character
				c := current_character
				if is_valid_label_character (c, cp) then
					Result.extend (c)
				elseif qc = c then
					-- End quote
					qc := '%U'
				else
					set_parse_error ("Invalid identifier character")
				end
				cp := cp + 1
			end
			trace_out ("quoted_id")
		ensure
			exists: not has_error implies
				Result /= Void and then not Result.is_empty
			unquoted: not Result.is_empty implies
				not is_quote (Result[1]) and
				not is_quote (Result[Result.count])
		end

	--|--------------------------------------------------------------

	unquoted_identifier_from_stream: STRING
			-- Extract from the input stream the unquoted identifier
			-- starting at the current position
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: is_valid_label_character (current_character, 1)
			no_read_ahead: not read_ahead
		local
			c: CHARACTER
			cp: INTEGER
		do
			trace_in ("unquoted_id")
			create Result.make (32)
			c := current_character
			Result.extend (c)
			from cp := 1
			until has_error or not has_input or read_ahead
			loop
				get_next_character
				c := current_character
				if is_valid_label_character (c, cp) then
					Result.extend (c)
					cp := cp + 1
				else
					read_ahead := True
				end
			end
			trace_out ("unquoted_id")
		ensure
			exists: not has_error implies
				Result /= Void and then not Result.is_empty
			unquoted: not Result.is_empty implies
				not is_quote (Result[1]) and
				not is_quote (Result[Result.count])
		end

	--|--------------------------------------------------------------

	quoted_value_from_stream: STRING
			-- Extract from the input stream the quoted value
			-- starting at the current position, excluding the enclosing 
			-- quotes
			-- Quoted values, unlike identifiers, can have embedded 
			-- spaces and other characters
			-- Quote characters can be embedded in a quoted value if 
			-- escaped by a preceding backslash ('\') character.
			-- Special escape sequences for tabs and such need to be 
			-- replaced per spec.
			--
			-- TODO - support unicode escape sequences and percent-style
			-- escapes
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: is_quote (current_character)
		local
			c, qc, pc: CHARACTER
		do
			trace_in ("quoted_value")
			qc := current_character
			pc := qc
			create Result.make (32)
			from
			until has_error or not has_input or qc = '%U'
			loop
				get_next_character
				if not has_input then
					set_parse_error ("Missing closing quote")
				else
					c := current_character
					inspect c
					when '%'', '%"' then
						if pc = '\' then
							-- Escaped embedded quote; replace escape char 
							-- only if it's the same as the outer quote
							if c = pc then
								Result.put (c, Result.count)
							else
								Result.extend (c)
							end
						elseif c = qc then
							-- Unescaped outer quote, end of value
							-- Do not include in Result
							qc := '%U'
						else
							-- An unescaped instance of the other quote
							-- Treat like any other character
							Result.extend (c)
						end
					when '\' then
						-- Escape char; is it escaped?	
						Result.extend (c)
						if pc = '\' then
							-- Escaped '\', so NOT an escape char
							-- Forget current char here, so 'pc' does not
							-- appear as an escape char
							c := '%U'
						end
					when 'v' then
						if pc = '\' then
							-- A backspace character
							Result.put ('%B', Result.count)
						else
							Result.extend (c)
						end
					when 'f' then
						if pc = '\' then
							-- A formfeed character
							Result.put ('%F', Result.count)
						else
							Result.extend (c)
						end
					when 'n' then
						if pc = '\' then
							-- A newline character
							Result.put ('%N', Result.count)
						else
							Result.extend (c)
						end
					when 'r' then
						if pc = '\' then
							-- A return character
							Result.put ('%R', Result.count)
						else
							Result.extend (c)
						end
					when 't' then
						if pc = '\' then
							-- A tab character
							Result.put ('%T', Result.count)
						else
							Result.extend (c)
						end
					when 'u' then
						if pc = '\' then
							-- A unicode character \uNNNN
							-- RFO TODO
							--Result.put ('%T', Result.count)
							Result.extend (c)
						else
							Result.extend (c)
						end
					else
						Result.extend (c)
					end
					pc := c
				end
			end
			trace_out ("quoted_value")
		ensure
			exists: not has_error implies Result /= Void
		end

	--|--------------------------------------------------------------

	unquoted_value_from_stream: STRING
			-- Extract from the input stream the unquoted value
			-- starting at the current position
			-- Unquoted values can be numerics or predefined literals
			-- including true and false
			-- Unquoted values cannot have included whitespace or quotes
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: not current_character.is_space and
				not is_quote (current_character)
		local
			c: CHARACTER
		do
			trace_in ("unquoted_value")
			create Result.make (32)
			c := current_character
			Result.extend (c)
			inspect c
			when 't', 'T', 'f', 'F' then
				-- Might be true or false literal
				-- get boolean literal
				Result := boolean_value_from_stream
			else
				if c.is_hexa_digit then
					--Result := hex_value_from_stream
					Result := word_from_stream (<<'.'>>)
				else
					set_parse_error ("Unexpected value character")
				end
			end
			trace_out ("unquoted_value")
		ensure
			exists: not has_error implies Result /= Void
		end

	--|--------------------------------------------------------------

	boolean_value_from_stream: STRING
			-- Extract from the input stream the boolean literal value
			-- starting at the current position
			-- Boolean values can be 'true' or 'false', but this routine
			-- is case-insenstive
		require
			no_errors: not has_error
			has_input: has_input
			valid_start: current_character.as_lower = 't' or
				current_character.as_lower = 'f'
		local
			c: CHARACTER
		do
			trace_in ("boolean_value")
			create Result.make (32)
			c := current_character.as_lower
			Result.extend (c)
			if c = 't' then
				get_next_character
				c := current_character.as_lower
				Result.extend (c)
				if c /= 'r' then
					set_parse_error ("Character is not part of Boolean literal")
				else
					get_next_character
					c := current_character.as_lower
					Result.extend (c)
					if c /= 'u' then
						set_parse_error ("Character is not part of Boolean literal")
					else
						get_next_character
						c := current_character.as_lower
						Result.extend (c)
						if c /= 'e' then
							set_parse_error (
								"Character is not part of Boolean literal")
						end
					end
				end
			else
				-- Must be looking for 'false'
				get_next_character
				c := current_character.as_lower
				Result.extend (c)
				if c /= 'a' then
					set_parse_error ("Character is not part of Boolean literal")
				else
					get_next_character
					c := current_character.as_lower
					Result.extend (c)
					if c /= 'l' then
						set_parse_error ("Character is not part of Boolean literal")
					else
						get_next_character
						c := current_character.as_lower
						Result.extend (c)
						if c /= 's' then
							set_parse_error (
								"Character is not part of Boolean literal")
						else
							get_next_character
							c := current_character.as_lower
							Result.extend (c)
							if c /= 'e' then
								set_parse_error (
									"Character is not part of Boolean literal")
							end
						end
					end
				end
			end
			trace_out ("boolean_value")
		ensure
			exists: not has_error implies
				Result /= Void and then Result.is_boolean
		end

--|========================================================================
feature -- Status
--|========================================================================

	input_buffer: detachable STRING
			-- Buffer loaded with content of input stream
		note
			option: stable
			attribute
		end

	has_input: BOOLEAN
			-- Is there any input to read?
		do
			if input_buffer /= Void and then not input_buffer.is_empty then
				Result := current_position <= maximum_character_position
			end
		end

	maximum_character_position: INTEGER
			-- Last valid position in input_buffer

	--|--------------------------------------------------------------

	current_position: INTEGER
			-- Current character position in the input stream

	current_character: CHARACTER
			-- Character at current input position
		require
			has_input: input_buffer /= Void and then not input_buffer.is_empty
		do
			if attached input_buffer as ib then
				Result := ib.item (current_position)
			end
		end

	read_ahead: BOOLEAN
			-- Has a character been read from input that we'd 
			-- really rather put back? We had to read it to
			-- resolve a token

	digest: detachable AEL_JSON_OBJECT
			-- Object or digest initialized as result of parsing the 
			-- input stream
		note
			option: stable
			attribute
		end

--|========================================================================
feature -- Support
--|========================================================================

	get_next_character
			-- Read from input stream the next character, if any
			-- Leave result in current_character, or set eof
		require
			has_input: has_input
		do
			current_position := current_position + 1
			if not has_input then
				db_print ("END%N")
			else
				db_print_ch (current_character)
			end
		end

	--|--------------------------------------------------------------

	word_from_stream (pa: ARRAY [CHARACTER]): STRING
			-- Read from input stream the next contiguous word, if any
			-- Word ends with any whitespace (blank, nl, tab, vt, 
			-- formfeed, or eof) or punctuation other than the values 
			-- passed as 'pa'
			-- Word must start at current position
		require
			has_input: has_input
			pa_exists: pa /= Void
			not_space: not current_character.is_space
			word_start: current_character.is_alpha_numeric
		local
			c: CHARACTER
			ts: STRING
		do
			Result := ""
			create ts.make (32)
			c := current_character
			ts.extend (c)
			from
			until has_error or not has_input or not Result.is_empty
			loop
				get_next_character
				c := current_character
				if not c.is_alpha_numeric and not pa.has (c) then
					Result := ts
				else
					ts.extend (c)
				end
			end
			if Result = Void then
				-- Must reached eof (or error) before finding a 
				-- non-conforming character
				if not has_error then
					Result := ts
				end
			else
				read_ahead := True
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	get_next_non_whitespace
			-- Read from input stream until the next non-whitespace 
			-- character is found, if any
			-- Leave result in current_character, or set eof
		require
			has_input: has_input
		do
			from get_next_character
			until
				has_error or not has_input or
				not current_character.is_space
			loop
				get_next_character
			end
		ensure
			not_space: not has_error implies not current_character.is_space
		end

	--|--------------------------------------------------------------

	is_quote (v: CHARACTER): BOOLEAN
			-- is 'v' a quote character?
		do
			inspect v
			when '%'', '%"' then
				Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	is_valid_label_character (c: CHARACTER; cp: INTEGER): BOOLEAN
			-- Is character 'c' at position cp valid for an identifier?
		do
			Result := c.is_alpha
			if not Result then
				Result := cp > 1 and c.is_alpha_numeric
				if not Result then
					-- Must be a special character
					Result := valid_label_special_characters.has (c)
				end
			end
		end

--|========================================================================
feature -- Debug status setting
--|========================================================================

	enable_db_print
			-- Enable general debug print statements
		do
			db_print_enabled := True
		end

	disable_db_print
			-- Disable general debug print statements
		do
			db_print_enabled := False
		end

	--|--------------------------------------------------------------

	enable_db_print_ch
			-- Enable printing information about each character as it is 
			-- read
		do
			db_print_ch_enabled := True
		end

	disable_db_print_ch
			-- Disable printing information about each character as it is 
			-- read
		do
			db_print_ch_enabled := False
		end

	--|--------------------------------------------------------------

	enable_trace
			-- Enable routine enter/exit tracing
		do
			trace_enabled := True
		end

	disable_trace
			-- Disable routine enter/exit tracing
		do
			trace_enabled := False
		end

--|========================================================================
feature -- Error reporting
--|========================================================================

	set_parse_error (msg: STRING)
			-- Record (not report) error 'msg' with contextual information
		require
			exists: msg /= Void
		do
			if has_input then
				set_error_message (
					apf.aprintf (
						"Parse error at position %%d, char=[%%c], %%s",
						<<current_position, current_character, msg>>))
			else
				set_error_message ("Parse error at end input, " + msg)
			end
		end

--|========================================================================
feature -- Debug and support
--|========================================================================

	db_print_enabled: BOOLEAN
			-- Is general debug printing enabled?

	db_print_ch_enabled: BOOLEAN
			-- Is printing of input per-character information enabled?

	trace_enabled: BOOLEAN
			-- Is routine enter/exit tracing enabled?

	--|--------------------------------------------------------------

	trace_indent: STRING
			-- Left margin indentation string for use in trace output

	trace_in (v: STRING)
			-- Print a routine trace enter message for routine name 'v'
		do
			if trace_enabled then
				increase_trace_indent
				print (trace_indent + "> " + v + "%N")
			end
		end

	trace_out (v: STRING)
			-- Print a routine trace exit message for routine name 'v'
		do
			if trace_enabled then
				print (trace_indent + "< " + v + "%N")
				if not trace_indent.is_empty then
					decrease_trace_indent
				end
			end
		end

	--|--------------------------------------------------------------

	increase_trace_indent
			-- Increase by one level the left margin indent of trace output
		require
			enabled: trace_enabled
		do
			if trace_enabled then
				trace_indent.append (" ")
			end
		end

	decrease_trace_indent
			-- Decrease by one level the left margin indent of trace output
		require
			enabled: trace_enabled
			has_indent: not trace_indent.is_empty
		do
			trace_indent.keep_head (trace_indent.count - 1)
		end

	--|--------------------------------------------------------------

	db_print (v: STRING)
			-- Print message 'v' to stdout only of db_print_enabled
		do
			if db_print_enabled then
				print (v)
			end
		end

	db_print_ch (c: CHARACTER)
			-- Print information of character 'c', only of 
			-- db_print_ch_enabled
		local
			ts: STRING
		do
			if db_print_ch_enabled then
				ts := db_char_out (c)
				if not ts.is_empty then
					print (ts)
				end
			end
		end

	db_char_out (c: CHARACTER): STRING
			-- Information about input character 'c'
		do
			Result := ""
			inspect c
			when '{' then
				Result := "<o_brace>"
			when '}' then
				Result := "<c_brace>"
			when '[' then
				Result := "<o_bracket>"
			when ']' then
				Result := "<c_bracket>"
			when ',' then
				Result := "<comma>"
			when '%"' then
				Result := "<quote_2>"
			when '%'' then
				Result := "<quote_1>"
			when ' ' then
				Result := c.out
			when '%T' then
				Result := c.out
			when '%N' then
				Result := "%N"
			when '%R' then
			else
			end
		end

	--|--------------------------------------------------------------

	print_digest
			-- Print to stdout the serialized form of 'digest'
			-- If 'digest' was provided client, then client likely has 
			-- special output formatting options
		require
			exists: digest /= Void
		do
			if attached digest as td then
				print (td.serial_out)
			end
		end

--|========================================================================
feature -- Customization
--|========================================================================

	add_special_label_characters (ca: ARRAY [CHARACTER]; df: BOOLEAN)
			-- Create from 'ca' a collection of non-letter, non-number 
			-- characters that will be valid in identifiers characters.
			--
			-- Default characters are also included if 'df' is True
		local
			i, lim: INTEGER
			tcc: like custom_label_special_characters
		do
			create tcc.make (5)
			custom_label_special_characters := tcc
			tcc.merge (dflt_label_special_characters)
			lim := ca.upper
			from i := ca.lower
			until i > lim
			loop
				tcc.force (ca[i], ca[i])
				i := i + 1
			end
		end

	reset_special_label_characters
			-- Reset to default values the set of special characters 
			-- allowed in identifiers (aka labels)
		do
			custom_label_special_characters := Void
		end

--|========================================================================
feature -- Constants
--|========================================================================

	-- JSON type constants (for array item consistency checking)
	K_jst_string: INTEGER = 1
	K_jst_numeric: INTEGER = 2
	K_jst_boolean: INTEGER = 3
	K_jst_object: INTEGER = 4
	K_jst_array: INTEGER = 5

	--|--------------------------------------------------------------

	valid_label_special_characters: like dflt_label_special_characters
			-- Non-alphanumeric characters permitted in identifiers
		do
			if attached custom_label_special_characters as lcc then
				Result := lcc
			else
				Result := dflt_label_special_characters
			end
		end

	dflt_label_special_characters: HASH_TABLE [CHARACTER, CHARACTER]
			-- Non-alphanumeric characters permitted in identifiers
		once
			create Result.make (5)
			Result.force ('_', '_')
			Result.force ('$', '$')
		end

	custom_label_special_characters: detachable like dflt_label_special_characters
			-- Non-alphanumeric characters permitted in identifiers
			-- Empty by default, but can be filled by calling 
			-- 'add_special_label_characters'
			--
			-- NOTE WELL: these values exist only within Current!!

	--|--------------------------------------------------------------
invariant

end -- class AEL_JSON_PARSER

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 06-Sep-2011
--|     Revample identifier validation, now supporting customization 
--|     of legal characters (for special contexts beyond JS objects)
--| 001 02-Feb-2011
--|     Created original module, borrowing from earlier JSON-oriented 
--|     classes
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of this class using one of the creation 
--| routines listed.  Unless creating with make_and_parse, once the
--| instance is initialized, call the 'parse' routine to execute.
--|
--|----------------------------------------------------------------------
