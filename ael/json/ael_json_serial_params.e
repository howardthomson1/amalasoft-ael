class AEL_JSON_SERIAL_PARAMS

inherit
	AEL_SPRT_SERIAL_PARAMS
		redefine
			initialize_patterns, initialize_flags
		end

create
	default_create

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_patterns
			-- Initialize common and special patterns
		do
			Precursor
			set_pattern ("attribute_separator", ",%N")
			set_pattern ("attribute_element_separator", ",%N")
			set_pattern ("elements_tag_label", "items")
			set_pattern ("serial_out_prolog", "items")
		end

	initialize_flags
			-- Initialize common and special flags
		do
			Precursor
			set_flag ("include_elements_tag", True)
		end

end -- class AEL_JSON_SERIAL_PARAMS
