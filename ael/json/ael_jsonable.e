deferred class AEL_JSONABLE

inherit
	AEL_JSON_COMPONENT
	AEL_SPRT_ATTRIBUTABLE
		rename
			serial_out_supported as json_out_supported
		redefine
			serial_elements, make_elements, initialize_serial_params
		end

--|========================================================================
feature {NONE} -- Private initialization
--|========================================================================

	make_serial_params
		do
			create private_json_params
		end

	make_serializer
		do
			create private_json_serializer
		end

	make_elements
		do
			create private_json_elements.make
			Precursor
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_serial_params
		do
			Precursor
			serial_params.set_attribute_separator (",%N")
			serial_params.set_attribute_element_separator (",%N")
			serial_params.set_pattern ("elements_tag_label", "objects")
		end

	init_from_substream (v: STRING; spos, epos: INTEGER)
			-- A JSON stream is bounded by '{' '}' pairs, possibly 
			-- preceded by a quoted name and colon.  JSON streams 
			-- can be nested (corresponding to object nesting)
		local
			i, sp, ep, lim, p: INTEGER
			rx,rxs,rxo,rxl,rxa,rxt,rxi,rxr,rxar,rxal: detachable like rexp_json_stream
			c: CHARACTER
			ts: STRING
			js: detachable AEL_JSON_OBJECT
			ja: detachable AEL_JSON_ARRAY
			done: BOOLEAN
		do
			last_scan_position := 0
			lim := epos
			sp := index_of_next (v, spos)
			if sp = 0 then
				set_scan_error ("Missing opening non-whitespace", spos)
			else
				p := v.index_of ('{', sp)
				ep := asr.index_of_matching_close (v, p, lim, '\')
				if ep = 0 then
					set_scan_error ("Missing closing '}'",p)
				else
					rxs := rexp_json_stream
					rxs.search_forward (v, sp, ep)
					if not rxs.found then
						set_scan_error ("Missing open/close brackets", sp)
					end
				end
			end
			if not has_error and rxs /= Void then
				lim := rxs.end_position
				-- Skip over opening bracket
				sp := rxs.start_position + 1

				from i := sp
				until i >= lim or has_error or done
				loop
					i := index_of_next (v, i)
					if i = 0 then
						-- Off end
						i := lim
					elseif i = lim then
						-- Done
					else
						c := v.item (i)
						inspect c
						when '%"' then
							-- A tag?
							rxt := rexp_json_tag
							rxt.search_forward (v, i, lim)
							if not rxt.found then
								set_scan_error ("Malformed tag", i)
							else
								-- Extract tag
								sp := rxt.start_position
								ep := rxt.end_position
								ts := v.substring (sp + 1, ep - 2)
								-- Might be object or attr
								p := index_of_next (v, rxt.end_position + 1)
								c := v.item (p)
								-- Check for object first, then attr to avoid 
								-- false positive matches on named objects as 
								-- attrs
								if c = '{' then
									-- An object
									rxo := rexp_json_obj
									rxo.search_forward (v, sp, lim)
									if rxo.found then
										ep := asr.index_of_matching_close (
											v, p, lim, '\')
										create js.make_from_substream (
											ts, v, p, ep)
									else
										rxl := rexp_json_obj_last
										rxl.search_forward (v, sp, lim)
										if rxl.found then
											ep := asr.index_of_matching_close (
												v, p, lim, '\')
											create js.make_from_substream (
												ts, v, p, ep)
										else
											-- Looked like an embedded obj, but 
											-- doesn't match either in-line or 
											-- end patterns
											js := Void
											set_scan_error (
												"Unexpected object-like construct", sp)
										end
									end
									if js /= Void then
										if js.has_error then
											set_error_message (js.last_error_message)
										else
											add_element_by_name (js, js.name)
											-- Last scan position is one before
											-- closing char
											i := js.last_scan_position + 1
										end
									end
								elseif c = '%"' then
									-- A string attr
									-- Need to limit range for pattern
									ep := index_of_char (v, '%"', p + 1, lim)
									if ep = 0 then
										set_scan_error ("Missing closing quote",p+1)
									else
										rxa := rexp_json_attr
										rxa.search_forward (v, sp, ep)
										if not rxa.found then
											set_scan_error ("Invalid string attribute", sp)
										else
											extract_attribute (
												v, rxa.start_position, rxa.end_position)
											i := rxa.end_position + 1
										end
									end
								elseif c.is_digit then
									-- A numeric attr?
									rxr := rexp_json_r_attr
									rxr.search_forward (v, sp, lim)
									if not rxr.found then
										-- Not a float, could be an integer
										rxi := rexp_json_i_attr
										rxi.search_forward (v, sp, lim)
										if not rxi.found then
											set_scan_error ("Invalid numeric attribute", p)
										else
											rx := rxi
										end
									else
										rx := rxr
									end
									if not has_error and rx /= Void then
										extract_attribute (
											v, rx.start_position, rx.end_position)
										i := rx.end_position + 1
									end
								elseif c = '[' then
									-- Start of array
									rxar := rexp_json_array
									rxar.search_forward (v, sp, lim)
									if rxar.found then
										ep := asr.index_of_matching_close (
											v, p, lim, '\')
										-- begin parsing array
										-- 'ts' is name of array, so we can't
										-- simply use that as the name of an
										-- object.  An array has a label (or 
										-- not), but is not an object per se
--RFO this call is not longer viable
-- JSON objects are created from parser, but the JSONABLEs are not
-- these creation routines were removed from JSON_ARRAY
--										create ja.make_from_substream (
-- 											ts, v, sp, ep)
									else
										rxal := rexp_json_array_last
										rxal.search_forward (v, sp, lim)
										if rxal.found then
											ep := asr.index_of_matching_close (
												v, p, lim, '\')
--RFO this call is not longer viable
-- JSON objects are created from parser, but the JSONABLEs are not
-- these creation routines were removed from JSON_ARRAY
--											create ja.make_from_substream (
--												ts, v, sp, ep)
										else
											-- Looked like an embedded array, but 
											-- doesn't match either in-line or 
											-- end patterns
											ja := Void
											set_scan_error (
												"Unexpected array-like construct", sp)
										end
									end
									if ja /= Void then
										if ja.has_error then
											set_error_message (ja.last_error_message)
										else
											add_element_by_name (ja, ja.name)
											-- Last scan position is one before ']'
 											i := ja.last_scan_position + 1
										end
									end
								elseif c = ']' then
									-- End of array
								elseif c = '}' then
									set_scan_error ("Unfinished tag", p)
								else
									set_scan_error ("Syntax error", p)
								end
							end
						when ',' then
							-- A comma separator; skip and keep looking
							i := i + 1
						when '{' then
							-- something else
							-- An object opening tag
							-- Could be an anonymous embedded object
							-- Don't do it Yet
							set_scan_error ("Unsupported construct", i)
						when '[' then
							-- An array opening tag
							-- Could be an anonymous embedded array
							-- Don't do it Yet
							set_scan_error ("Unsupported array construct", i)
						when '}' then
							-- A closing tag; we're done
							i := i + 1
							done := True
						else
							set_scan_error ("Syntax error", i)
						end
					end
				end
				if not has_error then
					last_scan_position := i
				end
 				extract_attributes
			end
		end

	--|--------------------------------------------------------------

	extract_attribute (v: STRING; sp, ep: INTEGER)
			-- Extract from stream 'v', between positions sp and ep,
			-- an attribute and value.
			-- Form is:
			-- '"'<attr_label>'": "'<attr_value>'"'
			-- Example:
			-- "name": "bozo"
			-- ^sp          ^ep
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep >= sp and ep <= v.count
		local
			ta: AEL_JSON_ATTRIBUTE
			ec: CELL [INTEGER]
		do
			create ec.put (ep)
			create ta.make_from_stream (v, sp, ec)
--RFO 			attribute_values.extend (ta.value, ta.tag)
			if not attached attribute_values then
				create attribute_values.make (7)
			end
			if attached attribute_values as al then
				al.force (ta.value, ta.tag)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	suppress_attribute_tag_quotes (tf: BOOLEAN)
			-- Suppress (or not) quotes around attribute tags
		do
			serial_params.set_attribute_tags_quoted (not tf)
		end

--|========================================================================
feature -- Status
--|========================================================================

	serializer: AEL_SPRT_ATTRIBUTE_SERIALIZER
			-- Serializing service for Current
		do
			check attached private_json_serializer as pjs then
				Result := pjs
			end
		end

	serial_params: AEL_SPRT_SERIAL_PARAMS
			-- Parameters directing serialization
		do
			check attached private_json_params as psp then
				Result := psp
			end
		end

	--|--------------------------------------------------------------

	serial_elements: CONTAINER [AEL_SPRT_ATTRIBUTABLE]
			-- Serializable subordinate elements, if any
		do
			check attached private_json_elements as pje then
				Result := pje
			end
		end

	--|--------------------------------------------------------------

	last_scan_position: INTEGER
			-- Index in input stream at which the last character 
			-- beloning to Current was found

	default_element: AEL_SPRT_ATTRIBUTABLE
		do
			create {AEL_JSON_OBJECT} Result.make_anonymous
		end

--|========================================================================
feature -- External representation
--|========================================================================

	to_json: STRING
			-- JSON object representing Current
		require
			supports_json_out: json_out_supported
		do
--			Result := serializer.serial_out ("", Current)
			Result := serializer.serial_out (Current, serial_params)
		end

	--|--------------------------------------------------------------

	to_json_filtered (ll: like tag_filter): STRING
			-- JSON element representing Current, but with only
			-- those attributes mathing the labels in 'll'
		require
			supports_json_out: json_out_supported
			labels_exist: ll /= Void
		do
			tag_filter := ll
			Result := to_json
			tag_filter := Void
		end

--|========================================================================
feature {NONE} -- Initialization support
--|========================================================================

	set_scan_error (msg: STRING; pos: INTEGER)
		do
			set_error_message (msg + " at position " + pos.out)
		end

	--|--------------------------------------------------------------

	index_of_next (v: STRING; sp: INTEGER): INTEGER
			-- Index, in 'v', of next non-whitespace character
			-- Newlines are considered whitespace
		require
			exists: v /= Void
			valid_start: sp >= 0 and sp <= v.count
		do
			Result := asr.index_of_next_non_whitespace (v, sp, True)
		end

	--|--------------------------------------------------------------

	index_of_char (v: STRING; c: CHARACTER; sp, ep: INTEGER): INTEGER
			-- Index, in 'v' between 'sp' and 'ep',
			-- of unescaped character 'c'
			-- Escape character is '\'
		require
			exists: v /= Void
			valid_start: sp >= 0 and sp <= ep
			valid_end: ep >= sp and ep <= v.count
		do
			Result := asr.substring_index_of_unescaped_char (v, c, '\', sp, ep)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_json_serializer: detachable AEL_JSON_ATTRIBUTE_SERIALIZER
			-- Serializing service for Current
		note
			option: stable
			attribute
		end

	private_json_params: detachable AEL_JSON_SERIAL_PARAMS
			-- Serialization parameters for Current
		note
			option: stable
			attribute
		end

	private_json_elements: detachable LINKED_LIST [AEL_JSON_OBJECT]
			-- Serializable subordinate elements, if any
		note
			option: stable
			attribute
		end

end -- class AEL_JSONABLE
