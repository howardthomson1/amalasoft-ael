class JSON

inherit
	JSON_CONSTANTS

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			last_error_index := -1
			last_decode := ""
		end

--|========================================================================
feature -- Decoding
--|========================================================================

	decode: ANY
		local
			index: INTEGER
			success: BOOLEAN
			ic: CELL [INTEGER]
			bc: CELL [BOOLEAN]
		do
			last_decode := json
			if json /= Void then
				ch_array := json.to_array
				index := 0
				success := True
				create ic.put (index)
				create bc.put (success)
				Result := parse_value (ch_array, ic, bc)
				if bc.item then
					last_error_index := -1
				else
					last_error_index := ic.item
				end
			else
				-- Void result
			end
		end

	--|--------------------------------------------------------------

	encoded: STRING
		do
			create Result.make (0)
			create builder.make (K_builder_capacity)
			if serialize_value (json, builder) then
				Result := builder.out
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	last_error_index: INTEGER
	last_decode: STRING

end -- class JSON
