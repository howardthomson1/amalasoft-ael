class JSON_CONSTANTS

--|========================================================================
feature -- Constants
--|========================================================================

	K_tok_none: INTEGER = 0
	K_tok_curly_open: INTEGER = 1
	K_tok_curly_close: INTEGER = 2
	K_tok_squared_open: INTEGER = 3
	K_tok_squared_close: INTEGER = 4
	K_tok_colon: INTEGER = 5
	K_tok_comma: INTEGER = 6
	K_tok_string: INTEGER = 7
	K_tok_number: INTEGER = 8
	K_tok_true: INTEGER = 9
	K_tok_false: INTEGER = 10
	K_tok_null: INTEGER = 11

	K_builder_capacity: INTEGER = 2000

end -- class JSON_CONSTANTS
