class JSON_STRING

create
	make_from_stream

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_stream (v: like stream)
		do
			stream := v
			parse_stream (v, 1)
		end

--|========================================================================
feature -- Status
--|========================================================================

	stream: STRING

--|========================================================================
feature -- Add Comment
--|========================================================================

	tbl_from_stream (v: STRING; sp: CELL[INTEGER]): HASH_TABLE [STRING, STRING]
		local
			i, lim: INTEGER
			c: CHARACTER
			token: INTEGER
		do
			create Result.make (17)
			lim := v.count
			from i := sp.item
			until i > count
			loop
				inspect v.item (i)
				when '"' then
				when '%'' then
				when ' ', '%T', '%R', '%N' then
				when '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then
				when '-', '+' then
				when '{' then
				when '}' then
				when '[' then
				when ']' then
				when '(' then
				when ')' then
				else
				end
				i := i + 1
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_tok_none: INTEGER = 0
	K_tok_curly_open: INTEGER = 1
	K_tok_curly_close: INTEGER = 2
	K_tok_squared_open: INTEGER = 3
	K_tok_squared_close: INTEGER = 4
	K_tok_colon: INTEGER = 5
	K_tok_comma: INTEGER = 6
	K_tok_string: INTEGER = 7
	K_tok_number: INTEGER = 8
	K_tok_true: INTEGER = 9
	K_tok_false: INTEGER = 10
	K_tok_null: INTEGER = 11

	K_builder_capacity: INTEGER = 2000

end -- class JSON_STRING
