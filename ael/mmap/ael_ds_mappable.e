note
	description: "{
Abstract notion of being memory-mappable
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_MAPPABLE

inherit
	AEL_DS_MAPPABLE_IMP

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original from AEL_DS_MAPPED_COMPONENT
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPABLE
