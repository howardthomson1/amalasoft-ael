note
	description: "{
Abstract implementation of memory-map-ability
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_MAPPABLE_I

inherit
	AEL_DS_MAPPED_STRUCTURE_TYPES
	AEL_SPRT_MULTI_FALLIBLE

--|========================================================================
feature -- Access
--|========================================================================

	address: POINTER
			-- Starting (physical) BYTE address of Current
		deferred
		end

--|========================================================================
feature -- Support
--|========================================================================

	put_byte_at_offset (v: NATURAL_8; o: INTEGER_32)
			-- Insert at address offset 'o' the 1-byte value 'v'
		do
			mr.c_put_byte (v, address + o)
		end

	put_char_at_offset (v: CHARACTER_8; o: INTEGER_32)
			-- Insert at address offset 'o' the 1-byte character value 'v'
		do
			mr.c_put_char (v, address + o)
		end

	put_ushort_at_offset (v: NATURAL_16; o: INTEGER_32)
			-- Insert at address offset 'o' the 2-byte value 'v'
		deferred
		end

	put_long_at_offset (v: INTEGER_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte signed value 'v'
		deferred
		end

	put_ulong_at_offset (v: NATURAL_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte value 'v'
		deferred
		end

	put_ulong_long_at_offset (v: NATURAL_64; o: INTEGER_32)
			-- Insert at address offset 'o' the 8-byte value 'v'
		deferred
		end

	put_string_at_offset (v: STRING_8; o: INTEGER_32)
			-- Insert at address offset 'o' the string 'v'
		do
			mr.put_string (v, address + o)
		end

	clear_string_at_offset (o, len: INTEGER_32)
			-- Insert 'len' nulls at address offset 'o'
		do
			mr.clear_string (address + o, len)
		end

	--|--------------------------------------------------------------

	byte_at_offset (o: INTEGER_32): NATURAL_8
			-- Byte at address offset 'o'
		do
			Result := mr.c_byte_at (address + o)
		end

	char_at_offset (o: INTEGER_32): CHARACTER_8
			-- Character at address offset 'o'
		do
			Result := mr.c_char_at (address + o)
		end

	ushort_at_offset (o: INTEGER_32): NATURAL_16
			-- 2-byte value at address offset 'o'
		deferred
		end

	long_at_offset (o: INTEGER_32): INTEGER_32
			-- 4-byte signed value at address offset 'o'
		deferred
		end

	ulong_at_offset (o: INTEGER_32): NATURAL_32
			-- 4-byte value at address offset 'o'
		deferred
		end

	ulong_long_at_offset (o: INTEGER_32): NATURAL_64
			-- 8-byte value at address offset 'o'
		deferred
		end

	--|--------------------------------------------------------------

	string_n_at_offset (o, len: INTEGER_32): STRING_8
			-- String of 'len' chars at address offset 'o'
		do
			Result := mr.string_n_at (address + o, len)
		end

	string_at_offset (o: INTEGER_32): STRING_8
			-- Null-terminated string at address offset 'o'
		require
			pos_nonnegative: o >= 0
		do
			Result := mr.string_at (address + o)
		end

	--|--------------------------------------------------------------

	ushort_array_at_offset (pos, a_count: INTEGER_32): ARRAY [NATURAL_16]
			-- Array of `count' 2-byte items from BYTE position `pos'.
		require
			pos_nonnegative: pos >= 0
			count_positive: a_count > 0
		local
			i, lim: INTEGER_32
			l_area: SPECIAL [NATURAL_16]
		do
			lim := a_count * 2
			from
				create l_area.make_empty (a_count)
			until
				i >= lim
			loop
				l_area.extend (ushort_at_offset (pos + i))
				i := i + 2
			end
			create Result.make_from_special (l_area)
		ensure
			read_array_not_void: Result /= Void
			read_array_valid_count: Result.count = a_count
		end

	--|--------------------------------------------------------------

	ushort_pairs_at_offset (p, num: INTEGER_32): ARRAY [detachable SPECIAL [NATURAL_16]]
			-- Read 'num' PAIRS of 2-bytes items ([ushort,ushort])
			-- from position 'p'.
		require
			pos_nonnegative: p >= 0
			count_positive: num > 0
		local
			i, lim, bi, blim: INTEGER_32
			l_area: SPECIAL [NATURAL_16]
		do
			create Result.make_filled (Void, 0, num - 1)
			blim := num
			lim := num * 4
			from
				i := 0
				bi := 0
				create l_area.make_empty (num)
			until
				i >= lim or bi >= blim
			loop
				create l_area.make_empty (2)
				Result.put (l_area, bi)
				l_area.extend (ushort_at_offset (p + i))
				i := i + 2
				l_area.extend (ushort_at_offset (p + i))
				i := i + 2
				bi := bi + 1
			end
		ensure
			read_array_not_void: Result /= Void
			read_array_valid_count: Result.count = num
		end

	--|--------------------------------------------------------------

	ushort_pair_at_offset (p: INTEGER_32): SPECIAL [NATURAL_16]
			-- PAIR of 2-bytes items ([ushort,ushort]) at position 'p'
		require
			pos_nonnegative: p >= 0
		do
			create Result.make_empty (2)
			Result.extend (ushort_at_offset (p))
			Result.extend (ushort_at_offset (p + 2))
		ensure
			exists: Result /= Void
			valid_count: Result.count = 2
		end

--|========================================================================
feature -- Octet conversion (platform dependent)
--|========================================================================

	byte_to_octets (v: NATURAL_8): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.byte_to_octets (v)
		end

	--|--------------------------------------------------------------

	short_to_octets (v: INTEGER_16): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	ushort_to_octets (v: NATURAL_16): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	--|--------------------------------------------------------------

	long_to_octets (v: INTEGER_32): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	ulong_to_octets (v: NATURAL_32): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	--|--------------------------------------------------------------

	long_long_to_octets (v: INTEGER_64): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	ulong_long_to_octets (v: NATURAL_64): STRING_8
			-- Octet stream equivalent of 'v'
		deferred
		end

	octets_to_n64 (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		deferred
		end

	octets_to_i64 (v: STRING): INTEGER_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		deferred
		end

--|========================================================================
feature -- Shared services
--|========================================================================

	mr: AEL_SPRT_MEMORY_ROUTINES
		once
			create Result
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original from ael_ds_mapped_component_i (7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--| This class is to be inherited by a corresponding _IMP class.
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPABLE_I
