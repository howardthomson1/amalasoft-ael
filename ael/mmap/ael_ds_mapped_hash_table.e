note
	description: "{
A mapped structure with hash table behavior that is designed from its 
persistent form first, as a contiguous block (in RAM or in file).
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_MAPPED_HASH_TABLE [G, K->STRING_8]

inherit
	AEL_DS_MAPPED_COMPONENT

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (bs, bc: INTEGER)
			-- Create Current with blocksize 'bs' and with 'bc' total
			-- content blocks (exclusive of header blocks)
		require
			valid_blocksize: is_valid_block_size (bs)
			valid_count: is_valid_block_count (bc)
		do
			--defined_structure_type := const.blocksize_to_structure_type (bs)
			initialize_dimensions (bs, bc)
			make_area
			initialize_area
		end

--RFO 	make_from_file (fn: STRING)
--RFO 			-- Create Current from file named 'fn'
--RFO 		do
--RFO 			init_from_file (fn)
--RFO 		end

--RFO 	make_memory_mapped (fn: STRING)
--RFO 			-- Create current using memory mapped file named 'fn'
--RFO 		do
--RFO 		end

	--|--------------------------------------------------------------

	initialize_dimensions (bs, bc: INTEGER)
			-- Initialize sizes and offsets per block size 'bs' and
			-- initial block count 'bc'
		require
			valid_blocksize: is_valid_block_size (bs)
			valid_count: is_valid_block_count (bc)
		do
			defined_blocksize := bs.as_natural_16
			initial_block_count := const.next_prime (bc).as_natural_16

			imap0_capacity := initial_block_count
			imap0_size := const.roundup_512 (
				const.K_size_imap_entry * initial_block_count)
			imap0_length := (imap0_size // defined_blocksize).as_natural_16
			imap0_byte_offset := 1 * defined_blocksize

			freelist_s_size := const.roundup_512 (
				const.K_size_freelist_s_entry * initial_block_count)
			freelist_s_length := (freelist_s_size // defined_blocksize).as_natural_16

			--| Need only 1/4 of block count items for block range free lists
			--| but allocate 1/2 (aids alignment, growth)
			freelist_r1_size := const.roundup_512 (
				const.K_size_freelist_r_entry * initial_block_count // 2)
			freelist_r1_length :=
				(freelist_r1_size//defined_blocksize).as_natural_16

			--| Need only 1/16 of block count items for large block range
			--| free lists but allocate 1/2 anyway (aids alignment, growth)
			freelist_r2_size := const.roundup_512 (
				const.K_size_freelist_r_entry * initial_block_count // 2)
			freelist_r2_length :=
				(freelist_r2_size//defined_blocksize).as_natural_16

			core_header_eor_offset := defined_blocksize - 2
			sb_eor_offset := defined_blocksize - 2

			freelist_s_0_block_offset := imap0_block_offset + imap0_length
			freelist_s_0_byte_offset := imap0_byte_offset + imap0_size
			freelist_r1_0_block_offset :=
				freelist_s_0_block_offset + freelist_s_length
			freelist_r1_0_byte_offset := freelist_s_0_byte_offset + freelist_s_size
			freelist_r2_0_block_offset :=
				freelist_r1_0_block_offset + freelist_r1_length
			freelist_r2_0_byte_offset :=
				freelist_r1_0_byte_offset + freelist_r1_size

			--| Total header size includes core header, index map and
			--| free lists
			header_size := core_header_size + imap0_size +
				freelist_s_size + freelist_r1_size + freelist_r2_size

			defined_content_start_address := header_size.as_natural_32
			defined_content_start_index :=
				(header_size // defined_blocksize).as_natural_16

			start_block_content_capacity := defined_blocksize -
				const.K_size_sb_header - const.K_size_eor_marker
		ensure
			valid_blocksize: is_valid_block_size (defined_blocksize)
			valid_count: is_valid_block_count (initial_block_count)
			valid_header_size: header_size >= defined_blocksize
			valid_content_start:
				defined_content_start_index >= (header_size // defined_blocksize)
		end

	--|--------------------------------------------------------------

	make_area
		require
			valid_blocksize: is_valid_block_size (defined_blocksize)
			valid_count: is_valid_block_count (initial_block_count)
			valid_header_size: header_size >= defined_blocksize
		local
			ibs, ibc, hs: INTEGER
		do
			ibs := defined_blocksize.as_integer_32
			ibc := initial_block_count.as_integer_32
			hs := header_size
			create area.make ((ibs * ibc) + hs)
			area_size := area.count
			initialize_addresses
		end

	--|--------------------------------------------------------------

	initialize_addresses
			-- Initialize address attributes
		require
			area_exists: area /= Void
		do
			imap0_address := address + imap0_byte_offset
			initialize_freelist_addresses
		end

	--|--------------------------------------------------------------

	initialize_freelist_addresses
			-- Initialize freelist address attributes
		require
			area_exists: area /= Void
		do
			freelist_s_0_address := address + freelist_s_0_byte_offset
			freelist_r1_0_address := address + freelist_r1_0_byte_offset
			freelist_r2_0_address := address + freelist_r2_0_byte_offset
		end

	--|--------------------------------------------------------------

	initialize_area
			-- Initial area with known values
		local
			bs: like defined_blocksize
			bc: like initial_block_count
		do
			bs := defined_blocksize
			bc := initial_block_count
			create core_header.make_for_address (address, bs, bc)
			core_header.set_content_start_index (defined_content_start_index)

			--| Create and initialize the first index map
			create imap0.make_for_address (imap0_address, bs, bc)

			--| Add imap0 block index to imap table in core header
			put_ushort_at_offset (imap0_block_offset, const.K_off_ch_imtbl)

			--| Create initial free lists
			create freelist_s_0.make_for_address (freelist_s_0_address, bs, bc)
			create freelist_r1_0.make_for_address (
				address + freelist_r1_0_byte_offset, bs, bc // 2)
			create freelist_r2_0.make_for_address (
				address + freelist_r2_0_byte_offset, bs, bc // 2)

			--| Add initial free lists block offsets for freelist tables
			core_header.put_freelist_s_table_item (freelist_s_0_block_offset, 0)
			core_header.put_freelist_r1_table_item (freelist_r1_0_block_offset, 0)
			core_header.put_freelist_r2_table_item (freelist_r2_0_block_offset, 0)

			--| Initialize table counts 'imc, 'sdc', 'r1dc' and 'r2dc'
		end

--|========================================================================
feature -- Access
--|========================================================================

	address: POINTER
			-- Physical address of area in memory
		do
			Result := area.item
		end

	area_size: INTEGER_32
			-- Number of bytes allocated to area
--BALAJI added the below 'is_from'
	--is_from: BOOLEAN
			-- Was Current created _from_ existing memory?
	core_header: 	AEL_DS_MAPPED_HASH_TABLE_CORE_HEADER
			-- Core header associated with Current

	imap0: AEL_DS_INDEX_MAP
			-- Initial index map

	imap0_address: POINTER
			-- Physical memory address of imap0

	freelist_s_0_address: POINTER
			-- Physical memory address of initial single block freelist

	freelist_r1_0_address: POINTER
			-- Physical memory address of initial small range freelist

	freelist_r2_0_address: POINTER
			-- Physical memory address of initial large range freelist

	freelist_s_0: AEL_DS_BLOCK_FREE_LIST
			-- Free list for single blocks

	freelist_r1_0: AEL_DS_BLOCK_RANGE_FREE_LIST_SMALL
			-- Free list for small block ranges (multiple adjacent blocks)

	freelist_r2_0: AEL_DS_BLOCK_RANGE_FREE_LIST_LARGE
			-- Free list for large block ranges (multiple adjacent blocks)

	--|--------------------------------------------------------------

	found_item_location: detachable TWO_WAY_LIST [like Ktype_index]
			-- Hierarchical location of most recent internal search
			-- Hierarchy includes addrss of start block as the last item
			-- in the list.
			-- Also included is the hashed index (used in imap0), and
			-- all extension block indices
			-- In effect, the trail of the search
			--
			-- Content of location:
			-- 1: index in imap
			-- 2: extension count
			-- ?: {extension block id, extension block index}*
			-- N: start block id

	found_item_index: like Ktype_index
			-- Position in Index Map to which key for most recent internal
			-- search resolved (if item found)
			-- If last search was unsuccessful, Result is zero
		do
			if attached found_item_location as l_fil then
				Result := l_fil.first
			end
		end

	found_item_block_id: like Ktype_index
			-- BLOCK address result of most recent internal search
		do
			if attached found_item_location as l_fil then
				Result := l_fil.last
			end
		end

	found_item_address: INTEGER_32
			-- BYTE OFFSET result of most recent internal search
		do
			if attached found_item_location as l_fil then
				Result := block_to_byte (l_fil.last)
			end
		end

	found_item_ext_count: INTEGER
			-- Number of extension block indices in collision chaing of
			-- most recent (successful) internal search
		do
			if attached found_item_location as l_fil then
				Result := l_fil.i_th (2).to_integer_32
			end
		end

	--|--------------------------------------------------------------

	found_item: detachable G
			-- Most recently found item from 'read_content', if any
			-- NOT set by 'internal_search'
		do
			if attached {G} found_item_string as l_fi then
				Result := l_fi
			end
		end

	found_item_string: detachable STRING_8
			-- Most recently found item from 'read_content', if any, in
			-- raw string form.
			-- NOT set by 'internal_search'

	--|--------------------------------------------------------------

	was_removed: BOOLEAN
			-- Was result of last removal operation successful?

	--|--------------------------------------------------------------

	item alias "[]", at alias "@" (k: K): detachable G assign force
			-- Item in Current stored by key 'k', if any
		note
			option: stable
		local
			addr: like found_item_address
		do
			internal_search (k)
			addr := found_item_address
			if addr /= 0 then
				read_content (addr)
				Result := found_item
			end
		end

	--|--------------------------------------------------------------

	has (k: K): BOOLEAN
			-- Does Current contain an item stored by key 'k'?
		do
			internal_search (k)
			Result := found_item_address /= 0
		end

--|========================================================================
feature -- Element change
--|========================================================================

	force (v: G; k: K)
			-- Add 'v' to Current, replacing any existing item by the
			-- same key 'k'
		local
			addr: like found_item_address
			ba: like next_available_blocks
			bc: INTEGER
			--cl, ocl: NATURAL_32
			idx, a: like Ktype_index
			oldi: like item_type
		do
			bc := content_blocks_required (v)
			--| If empty, no need to search, but try to allocate from
			--| deletions first
			if count = 0 then
				ba := next_available_blocks (bc)
				if ba = Void or else ba.count /= bc then
					-- No More Blocks; Resize or go home
				else
					--| add block to imap
					idx := key_to_index (k)
					a := ba [1]
					put_imap_item (a, idx)
					--p := imap0_byte_offset + (idx * const.K_size_imap_entry)
					--put_short (ba[1], p + const.K_size_block_index)
					--TODO
					-- IF NOT FROM FREE LIST
					core_header.set_highest_block_index (a)
					write_content (v, k, ba)
				end
				-- update item count unless errored out
				increment_item_count
			else
				internal_search (k)
				addr := found_item_address
				if addr = 0 then
					--| No blocks hash to hashed index
					ba := next_available_blocks (bc)
					if ba = Void or else ba.count /= bc then
						-- No More Blocks; Resize or go home
					else
						write_content (v, k, ba)
					end
					-- update item count unless errored out
					increment_item_count
				else
					--| An item exists by key 'k'
					--| Replace it
					--| Check content length of existing and new item to
					--| ensure that new content will fit
					--| Adjust block count, free extras, etc on size change
					create oldi.make_from_address (
						address + addr,
						defined_blocksize, initial_block_count)
					if content_lengths_match (v, oldi) then
						oldi.replace_content (item_to_octets (v))
					else
						-- TODO
					end
				end
			end
			increment_revision
		end

	--|--------------------------------------------------------------

	content_blocks_required (v: G): INTEGER
			-- Number of blocks required to hold new content
		deferred
		end

	content_lengths_match (newi: G; oldi: like item_type): BOOLEAN
			-- Does the length of new content 'newi' equal to the length
			-- of the old content in 'oldi'?
		deferred
		end

	item_to_octets (v: G): STRING_8
			-- String (stream) representation of 'v'
		deferred
		end

--|========================================================================
feature -- Element Removal
--|========================================================================

	remove (k: K)
			-- Remove item associated key 'k'
		local
			addr: like found_item_address
			--ba: like next_available_blocks
			--bc, idx, p: INTEGER
			sb: like sb_type
		do
			was_removed := False
			if count /= 0 then
				internal_search (k)
				--| found_item_address is BYTE address of most recently
				--| found item (result of internal search)
				if not attached found_item_location as loc then
					--| RFO Is it OK to fail a remove attempt?  Sure.
					--| It can be expensive to require 'has' on each call
				else
					--| An item exists by key 'k'
					--| Remove it (be aware of multi block items)
					addr := found_item_address
					if found_item_ext_count = 0 then
						--| If there are no extensions then simply remove
						--| the item block index from the imap
						put_imap_item (0, found_item_index)
						--| Add to single block freelist
						--|TODO, support multiple possible freelists
						--freelist_s_0.extend (found_item_index)
						create sb.make_from_address (
							address + addr, defined_blocksize, 1)
						sb.set_deleted
					else
						--| Walk the extensions to the end, and remove the
						--| last (resolving) block index from the extension
						--| block, updating its internal structure as needed
					end
					--| Once index to item is removed, decrement item count
					--| because even if failure occurs at this point, the
					--| structure is at least self consistent.
					decrement_item_count
					--| Now that index to item is gone and item count has
					--| been adjusted, it is safe to nuke the blocks
					--| themselves.

					--| Clear content?
					--| Pretty expensive; perhaps do a mark-and-sweep
					--| on the freelist, or wait for next allocation to
					--| nuke the content???????

					--| Let the world know the removal worked
					was_removed := True
				end
			end
		ensure
			removed: old has (k) implies was_removed
			gone: was_removed implies not has (k)
			counted: was_removed implies count = old count - 1
		end

	wipe_out
			-- Remove all items from Current but leaving structure intact
		do
			--| Need to find all blocks that have been used and re-mark
			--| them as unused
			--| Need to wipe out deletion lists
			--| Need to clear all indices in Imaps
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	internal_search (k: K)
			-- Search for an item by the key 'k'
			-- If found, preserve the item's full location information
			-- in 'found_item_location'
			-- If not found, set 'found_item_location' to Void
		local
			addr: like found_item_address
			idx, bid: like Ktype_index
			kl: like Ktype_byte
			ks: STRING_8
			fil: like found_item_location
		do
			create fil.make
			found_item_location := Void
			--found_item_address := 0
			found_item_string := Void
			if count /= 0 then
				idx := key_to_index (k)
				--| Record hashed index
				fil.extend (idx)
				--| Get block address field from master index map(s)
				bid := mapped_index_to_block_id (idx)
				if bid = 0 then
					--| No blocks hash to hashed index (or to key)
				else
					--| Record number of extensions
					fil.extend (0)
					--| Record block ID gathered from imap
					fil.extend (bid)
					--| A block already exists by hashed index,
					--| now find one that matches 'k'
					addr := block_to_byte (bid)
					kl := byte_at_offset (addr + const.K_off_sb_key_length)
					if kl = k.count then
						ks := string_n_at_offset (addr + const.K_off_sb_key, kl)
						if ks ~ k.string then
							-- Is a match
							--found_item_address := addr
							found_item_location := fil
						else
							-- Keep looking
							-- remember to add each point in the traversal
							-- to found_item_location
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	next_available_blocks (bc: INTEGER): like Ktype_index_array
			-- Next 'n' available block addresses,
			-- either from a free list or from heap
			-- Array is 1-based !!!!!!!!!
		local
			--ca: like Ktype_index
		do
			Result := const.empty_index_array_1
			--| Try for contiguous blocks first

			if deleted_block_count < bc then
				if count = 0 then
					Result := << defined_content_start_index >>
				else
					-- Allocate from next available content block
				end
			else
				-- Allocate from free lists
				Result := next_available_deleted_blocks (bc)
			end
		end

	--|--------------------------------------------------------------

	next_available_deleted_blocks (bc: INTEGER): like Ktype_index_array
			-- Next 'n' available block addresses from free lists
		require
			has_enough: bc <= deleted_block_count
		do
			Result := const.empty_index_array_1
			if bc >= const.K_size_min_freelist_r1_range then
				-- Look for a suitable range
				if bc >= const.K_size_min_freelist_r2_range then
				else
				end
			end
			if Result = const.empty_index_array_1 then
				-- Range was not allocated
				-- Allocate individual blocks
			end
		end

	--|--------------------------------------------------------------

	read_content (pos: INTEGER_32)
			-- Read content from item at (BYTE) OFFSET 'pos'
			-- Leave result in 'found_item'
		require
			valid_position: pos > 0 and pos < area_size
		deferred
		end

	--|--------------------------------------------------------------

	write_content (v: G; k: K; ba: like next_available_blocks)
			-- Write content 'v' and key 'k' to allocated blocks 'ba'
		require
			content_exists: v /= Void
			valid_key: is_valid_key (k)
			blocks_ok: ba /= Void and then not ba.is_empty
		deferred
		end

--|========================================================================
feature -- Status
--|========================================================================

	defined_blocksize: like Ktype_index
			-- Block size at which Current is built

	initial_block_count: like Ktype_index
			-- Number of blocks (exclusive of header blocks)
			-- in Current at time of creation

	header_size: INTEGER
			-- Number of BYTES in header

	core_header_size: INTEGER
			-- Number of BYTES in core header
		deferred
		end

	size_in_bytes: INTEGER
			-- Number of bytes, total, allocated for Current
		do
			Result := area.count
		end

--|========================================================================
feature -- External representation
--|========================================================================

	core_header_out: STRING
			-- Hex dump representation of Core Header contents, by field
		do
			Result := core_header.out
		end

	--|--------------------------------------------------------------

	core_header_formatted_out: STRING
			-- String representation of Core Header contents, by field
		do
			Result := core_header.formatted_out
		end

	--|--------------------------------------------------------------

	header_out: STRING
			-- String (hex dump) representation of Header contents
		local
			xd: AEL_PF_HEXDUMP
		do
			create xd.make_with_addr (address, header_size)
			Result := xd.to_string (0, 0, "", 0, 0)
		end

	--|--------------------------------------------------------------

	range_out (sp, ep: INTEGER): STRING
			-- Hex dump of byte range from 'sp' through 'ep'
		require
			valid_start: sp >= 0 and sp < area_size
			valid_end: ep >= sp and ep < area_size
		local
			xd: AEL_PF_HEXDUMP
		do
			create xd.make_with_addr (address + sp, ep - sp + 1)
			Result := xd.to_string (0, 0, "", 0, 0)
		end

--|========================================================================
feature -- Core Header Field Values
--|========================================================================

	structure_type: like Ktype_byte
			-- Encoded structure type of Current
			-- 'st'
			-- From area
		do
			Result := core_header.structure_type
		end

	blocksize: like Ktype_index
			-- Block size
			-- from structure type field in area
		do
			inspect structure_type &  0x07
			when 0x00 then
				Result := 512
			when 0x01 then
				Result := 1024
			when 0x02 then
				Result := 2048
			when 0x03 then
				Result := 4096
			when 0x04 then
				Result := 8192
			when 0x05 then
				Result := 16384
			when 0x07 then
				Result := 65535 -- denotes 'unlimted' special case
			else
				-- zero denotes invalid field
			end
		end

	access_type: like Ktype_byte
			-- Access type
			-- from structure type field in area
		do
			Result := (structure_type &  0x78) |>> 3
		end

	--|--------------------------------------------------------------

	original_block_count: like Ktype_index
			-- Original block count
			-- 'obc'
			-- From area
		do
			Result := core_header.original_block_count
		end

	current_block_count: like Ktype_index
			-- Current block count
			-- 'cbc'
			-- From area
		do
			Result := core_header.current_block_count
		end

	count: like Ktype_index
			-- Current item count
			-- 'ic'
			-- From area
		do
			Result := core_header.item_count
		end

	deleted_block_count: like Ktype_index
			-- Total number of blocks deleted (in all tables)
			-- 'dbc'
			-- From area
		do
			Result := core_header.deleted_block_count
		end

	highest_block_index: like Ktype_index
			-- Block index of highest assigned non-header block
			-- 'hbi'
			-- From area
		do
			Result := core_header.highest_block_index
		end

	content_start_index: like Ktype_index
			-- Block index of first non-header block (header length)
			-- 'csi'
			-- From area
		do
			Result := core_header.content_start_index
		end

	revision: NATURAL_64
			-- Revision number of Current (updated on each change)
			-- 'rev'
			-- From area
		do
			Result := core_header.revision
		end

	index_map_count: like Ktype_byte
			-- Number of index maps in use
			-- 'imc'
			-- From area
		do
			Result := core_header.index_map_count
		end

	single_freelist_count: like Ktype_byte
			-- Number of single-block freelists in use
			-- 'sdc'
			-- From area
		do
			Result := core_header.single_freelist_count
		end

	small_range_freelist_count: like Ktype_byte
			-- Number of small range freelists in use
			-- 'r1dc'
			-- From area
		do
			Result := core_header.small_range_freelist_count
		end

	large_range_freelist_count: like Ktype_byte
			-- Number of Large range freelists in use
			-- 'r2dc'
			-- From area
		do
			Result := core_header.large_range_freelist_count
		end

	index_map_table: like Ktype_index_array
			-- Table of index map block addresses
			-- 'imtbl'
			-- From area
		do
			Result := core_header.index_map_table
		end

	single_freelist_table: like Ktype_index_array
			-- Table of single-block freelist block addresses
			-- 'f1tbl'
			-- From area
		do
			Result := core_header.single_freelist_table
		end

	small_range_freelist_table: like Ktype_index_array
			-- Table of small block range freelist addresses
			-- 'frtbl1'
			-- From area
		do
			Result := core_header.small_range_freelist_table
		end

	large_range_freelist_table: like Ktype_index_array
			-- Table of large block range freelist addresses
			-- 'frtbl2'
			-- From area
		do
			Result := core_header.large_range_freelist_table
		end

	eoh_marker: like Ktype_index
			-- End-of-head marker
			-- 'eoh'
			-- From area
		do
			Result := core_header.eoh_marker
		end

	--|--------------------------------------------------------------

	freelist_s_0_items: like Ktype_index_array
			-- Initial table of single block deletions (freelist_s_0)
			-- From area
		do
			Result := ushort_array_at_offset (
				freelist_s_0_byte_offset,
				const.K_freelist_s_table_capacity)
		end

	freelist_r1_0_items: like Ktype_index_array
			-- Initial table of small block range deletions (freelist_r1_0)
			-- From area
		do
			Result := ushort_array_at_offset (
				freelist_r1_0_byte_offset,
				const.K_freelist_r1_table_capacity)
		end

	freelist_r2_0_items: like Ktype_index_array
			-- Initial table of large block range deletions (freelist_r2_0)
			-- From area
		do
			Result := ushort_array_at_offset (
				freelist_r2_0_byte_offset,
				const.K_freelist_r2_table_capacity)
		end

--|========================================================================
feature -- Field value setting
--|========================================================================

	set_item_count (v: INTEGER)
			-- Set the item count field to 'v'
		do
			core_header.set_item_count (v)
		end

	increment_item_count
		do
			core_header.increment_item_count
		end

	decrement_item_count
		do
			core_header.decrement_item_count
		end

	increment_revision
		do
			core_header.increment_revision
		end

	--|--------------------------------------------------------------

	put_imap_item (v, pos: like Ktype_index)
			-- Insert to 0-based index map index 'pos' value 'v'
		require
			valid_index: is_valid_imap_index (pos)
		do
			--|RFO TODO need to calculate WHICH index map has 'pos'
			--|Do the simple case for now, with a single map
			imap0.put (v, pos)
		ensure
			inserted: mapped_index_to_block_id (pos) = v
		end

--|========================================================================
feature -- Content Address Management
--|========================================================================

	mapped_index_to_addr (v: INTEGER): INTEGER_32
			-- BYTE Address of item mapping to index 'v'
		do
			Result := mapped_index_to_block_id (v).to_integer_32 *
				defined_blocksize
		end

	--|--------------------------------------------------------------

	mapped_index_to_block_id (v: INTEGER): like Ktype_index
			-- BLOCK Address of item mapping to index 'v'
		local
			bc: like Ktype_index
			ime: AEL_DS_INDEX_MAP_ENTRY
		do
			--bc := defined_imap0_length // const.K_size_imap_entry
			bc := initial_block_count
			if v > bc then
				-- Need an extension map
			else
				create ime.make_with_address (
					imap0_address + (v * const.K_size_imap_entry))
				Result := ime.item
			end
		end

--|========================================================================
feature -- Defined and pre-calculated offsets
--|========================================================================

	core_header_eor_offset: like Ktype_index
			-- Byte offset to EOH field in core header

	--|--------------------------------------------------------------

	imap0_byte_offset: INTEGER_32
			--| BYTE address of imap0
			--| Immediately follows core header

	imap0_block_offset: NATURAL_16
			--| BLOCK index of imap0
			--| Immediately follows core header

	--|--------------------------------------------------------------

	freelist_s_0_byte_offset: INTEGER_32
			-- BYTE offset of initial single-block freelist

	freelist_s_0_block_offset: like Ktype_index
			-- BLOCK offset of initial single-block freelist

	freelist_r1_0_byte_offset: INTEGER_32
			-- BYTE offset of initial small block range freelist

	freelist_r1_0_block_offset: like Ktype_index
			-- BLOCK offset of initial small block range freelist

	freelist_r2_0_byte_offset: INTEGER_32
			-- BYTE offset of initial large block range freelist

	freelist_r2_0_block_offset: like Ktype_index
			-- BLOCK offset of initial large block range freelist

	--|--------------------------------------------------------------

	sb_eor_offset: INTEGER_32
			-- Byte offset, in content start block, of 'eor' field

--|========================================================================
feature -- Defined and pre-calculated sizes, lengths and capacities
--|========================================================================

	imap0_length: like Ktype_index
			-- Size of imap0 in BLOCKS

	imap0_size: INTEGER_32
			-- Size of imap0 in BYTES

	imap0_capacity: like Ktype_index
			-- Number of slots in imap0

	--|--------------------------------------------------------------

	freelist_s_length: like Ktype_index
			-- Defined length of small deletions list, in BLOCKS

	freelist_s_size: INTEGER_32
			-- Defined length of small deletions list, in BYTES

	freelist_r1_length: like Ktype_index
			-- Defined length of small block range list, in BLOCKS

	freelist_r1_size: INTEGER_32
			-- Defined length of small block range list, in BYTEs

	freelist_r2_length: like Ktype_index
			-- Defined length of small block range list, in BLOCKS

	freelist_r2_size: INTEGER_32
			-- Defined length of small block range list, in BYTEs

	--|--------------------------------------------------------------

	start_block_content_capacity: INTEGER
			-- Size of content field in start block

--|========================================================================
feature -- Block and Structure Types
--|========================================================================

	--defined_structure_type: like Ktype_byte
			-- Structure type of Current

	defined_content_start_index: like Ktype_index
			-- BLOCK index of first non-header block (header length)
			-- 'csi'
			-- As defined

	defined_content_start_address: like Ktype_addr
			-- BYTE address of first non-header block

--|========================================================================
feature -- Persistence
--|========================================================================

	save_to_file (fn: STRING)
			-- Save Current to file named 'fn'
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
		end

	init_from_file (fn: STRING)
			-- Create Current from file named 'fn'
		require
			valid_file_name: fn /= Void and then not fn.is_empty
		do
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	key_to_index (k: K): like Ktype_index
			-- Index to which key 'k' maps, for use in the index map
			-- tables
		local
			increment, ipos: INTEGER
			h: INTEGER
		do
			h := k.hash_code
			increment := (h \\ (initial_block_count - 1)) + 1
			ipos := (h \\ initial_block_count) - increment
			Result := ((ipos + increment) \\ initial_block_count).as_natural_16
		end

	--|--------------------------------------------------------------

	block_to_byte (v: like Ktype_index): INTEGER_32
			-- BYTE OFFSET equivalent of BLOCK index 'v'
		do
			Result := v.as_integer_32 * defined_blocksize.as_integer_32
		end

	--|--------------------------------------------------------------

	block_to_ptr (v: like Ktype_index): POINTER
			-- POINTER equivalent of BLOCK index 'v'
		do
			Result := address +
				(v.as_integer_32 * defined_blocksize.as_integer_32)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_block_size (v: INTEGER): BOOLEAN
			-- Is 'v' a valid block size?
		do
			Result := const.is_valid_block_size (v)
		end

	is_valid_block_count (v: INTEGER): BOOLEAN
			-- Is 'v' a valid block count?
		do
			Result := const.is_valid_block_count (v)
		end

	is_valid_key (k: K): BOOLEAN
			-- Is 'k' a valid key?
		do
			Result := k /= Void and then
				(not k.is_empty and k.count <= const.K_size_sb_max_key_length)
		end

	is_valid_imap_index (v: like Ktype_index): BOOLEAN
			-- Is 'v' a valid index (slot number) in the index map?
		do
			Result := v < imap0_capacity
		end

	dimensions_initialized: BOOLEAN
			-- Are sizes and offsets initialized
		do
			Result :=
				defined_blocksize /= 0 and
				imap0_length /= 0 and
				freelist_s_length /= 0 and
				freelist_r1_length /= 0 and
				freelist_r2_length /= 0 and
				core_header_eor_offset /= 0 and
				imap0_byte_offset /= 0 and
				freelist_s_0_byte_offset /= 0 and
				freelist_r1_0_byte_offset /= 0 and
				freelist_r2_0_byte_offset /= 0 and
				header_size /= 0 and
				defined_content_start_index /= 0
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	area: MANAGED_POINTER
			-- Area in memory in which structure of Current resides

--|========================================================================
feature {NONE} -- Shared resources
--|========================================================================

	const: AEL_DS_MAPPED_HASH_TABLE_CONSTANTS
		once
			create Result
		end

--RFO 	apf: AEL_PRINTF
--RFO 		once
--RFO 			create Result
--RFO 		end

	item_type: AEL_DS_MAPPED_HASH_TABLE_ITEM
		require False
		do
			check False then end
		ensure false
		end

	sb_type: AEL_DS_MAPPED_HASH_TABLE_START_BLOCK
		require False
		do
			check False then end
		ensure false
		end

	--|--------------------------------------------------------------
invariant
	initialized: dimensions_initialized

--|----------------------------------------------------------------------
--| History
--|
--| 001  01-Dec-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| Structure/layout
--|
--|  Core Header 1 block
--|    st - 1 byte structure type
--|    stx - 1 byte structure type extension
--|    obc - 2 byte original block count
--|          Number of blocks allocated on creation
--|    cbc - current block count
--|          Number of blocks currently allocated
--|    ic - 2 byte item count (number of content items in Current)
--|    dbc - 2 byte deletion block count
--|    hbi - 2 byte highest block index
--|          Block address of highest allocated block
--|    csi - 2 byte content start index
--|          Block address where non-header blocks begin
--|    rev - 2 byte revision number, updated on each change
--|    imc - 1 byte index map count
--|          Number of items in Index Map Table (i.e. number of index maps)
--|    sdc - 1 byte single block deletion list count
--|          Number of items in Single Block Deletions List Table
--|         (i.e. number of single block freelists)
--|    r1dc - 1 byte small block range deletion list count
--|          Number of items in Small Block Range Deletions List Table
--|         (i.e. number of small block range freelists)
--|    r2dc - 1 byte large block range deletion list count
--|          Number of items in Large Block Range Deletions List Table
--|         (i.e. number of large block range freelists)
--|    imtbl - 128 byte Index Map Table
--|          Table of 64 2-byte block address of index maps
--|    f1tbl - 120 byte Single Block Freelist Table
--|          Table of 60 2-byte block address of single block freelists
--|    frtbl1 - 120 byte Block Range Freelist Table
--|          Table of 60 2-byte block address of small block range freelists
--|    frtbl2 - 120 byte Block Range Freelist Table
--|          Table of 60 2-byte block address of large block range freelists
--|    pad - blocksize-510 byte pad
--|    eor - 2 byte end-of-record marker
--|  Header Tables
--|    imap0 - at offset=blocksize (aka core header size)
--|          Initial content index map
--|          Contains 'initial_capacity' 2-byte items
--|          size=(initial_capacity * 2) + 6 bytes
--|    freelist_s_0 - at offset=imap0 offset + imap0 size
--|          Initial single-block freelist
--|          Contains 'initial_capacity' 2-byte items
--|          size=(initial_capacity * 2) + 6 bytes
--|    freelist_r1_0 - at offset=freelist_s_0 offset + freelist_s size
--|          Initial small block range freelist
--|          Contains 'initial_capacity'//2 4-byte items
--|          size=(initial_capacity * 2) + 6 bytes
--|    freelist_r2_0 - at offset=freelist_r1_0 offset + freelist_r1 size
--|          Initial large block range freelist
--|          Contains 'initial_capacity'//2 4-byte items
--|          size=(initial_capacity * 2) + 6 bytes
--|  Content (and extension) blocks
--|    content start blocks
--|    content extension blocks
--|    index map extension blocks
--|    extension freelists
--|
--| How-to
--|
--| Instantiate by calling 'make' with a (fixed) block size and an
--| initial block count.
--|
--| Use access, status and status setting routines to read and write
--| fields and values.
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPED_HASH_TABLE
