note
	description: "{
Core header of an AEL_DS_MAPPED_HASH_TABLE
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_MAPPED_HASH_TABLE_CORE_HEADER

inherit
	AEL_DS_MAPPED_BLOCK
		rename
			block_count as core_header_length
		redefine
			make, initialize_dimensions, initialize_fields_for, out
		end

create
	make_for_address, make_from_address

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (addr: like address; bs: like blocksize; bc: INTEGER)
		do
			defined_type_marker := const.blocksize_to_structure_type (bs)
			Precursor (addr, bs, bc)
		ensure then
			structure_type_defined: defined_structure_type /= 0
		end

	--|--------------------------------------------------------------

	initialize_dimensions (bs: like blocksize; bc: INTEGER)
			-- Initialize sizes and offsets per block size 'bs' and
			-- initial block count 'bc'
		do
			Precursor (bs, bc)
			defined_initial_block_count := const.next_prime (bc).as_natural_16
			eor_offset := blocksize - 2
		ensure then
			valid_count: is_valid_block_count (defined_initial_block_count)
		end

	--|--------------------------------------------------------------

	initialize_fields_for
			-- Initialize fields to known values (_for_ address)
		do
			Precursor
			initialize_block_count
		end

	initialize_block_count
		-- Initialize block count field
		do
			set_initial_block_count (defined_initial_block_count.as_natural_16)
		ensure
			initialized: original_block_count = defined_initial_block_count
		end

--|========================================================================
feature -- Field values
--|========================================================================

	structure_type: NATURAL_8
			-- Structure type of Current, from address
		do
			Result := byte_at_offset (const.K_off_ch_structure_type)
		end

	original_block_count: like Ktype_index
			-- Number of blocks (exclusive of header blocks)
			-- in Current at time of creation
		do
			Result := ushort_at_offset (const.K_off_ch_original_block_count)
		end

	current_block_count: like Ktype_index
			-- Number of blocks (exclusive of header blocks)
			-- in Current at present
		do
			Result := ushort_at_offset (const.K_off_ch_current_block_count)
		end

	item_count: like Ktype_index
			-- Current item count
			-- 'ic'
			-- From address
		do
			Result := ushort_at_offset (const.K_off_ch_item_count)
		end

	deleted_block_count: like Ktype_index
			-- Total number of blocks deleted (in all tables)
			-- 'dbc'
			-- From address
		do
			Result := ushort_at_offset (const.K_off_ch_deleted_block_count)
		end

	highest_block_index: like Ktype_index
			-- Block index of highest assigned non-header block
			-- 'hbi'
			-- From address
		do
			Result := ushort_at_offset (const.K_off_ch_highest_block_index)
		end

	content_start_index: like Ktype_index
			-- Block index of first non-header block (header length)
			-- 'csi'
			-- From address
		do
			Result := ushort_at_offset (const.K_off_ch_content_start_index)
		end

	revision: NATURAL_64
			-- Revision number of Current (updated on each change)
			-- 'rev'
			-- from address
		do
			Result := ushort_at_offset (const.K_off_ch_revision_field).as_natural_64
		end

	index_map_count: like Ktype_byte
			-- Number of index maps in use
			-- 'imc'
			-- from address
		do
			Result := byte_at_offset (const.K_off_ch_imap_table_count)
		end

	single_freelist_count: like Ktype_byte
			-- Number of single block free lists in use
			-- 'sdc'
			-- from address
		do
			Result := byte_at_offset (const.K_off_ch_freelist_s_count)
		end

	small_range_freelist_count: like Ktype_byte
			-- Number of small block range free lists in use
			-- 'r1dc'
			-- from address
		do
			Result := byte_at_offset (const.K_off_ch_freelist_r1_count)
		end

	large_range_freelist_count: like Ktype_byte
			-- Number of large block range free lists in use
			-- 'r2dc'
			-- from address
		do
			Result := byte_at_offset (const.K_off_ch_freelist_r2_count)
		end

	index_map_table: like Ktype_index_array
			-- Table of index map block addresses
			-- 'imtbl'
			-- from address
		do
			Result := ushort_array_at_offset (
				const.K_off_ch_imtbl, const.K_imtbl_capacity)
		end

	single_freelist_table: like Ktype_index_array
			-- Table of single-block freelist addresses
			-- 'f1tbl'
			-- from address
		do
			Result := ushort_array_at_offset (
				const.K_off_ch_freelist_s_table, const.K_freelist_s_table_capacity)
		end

	single_freelist_table_item (v: INTEGER): like Ktype_index
			-- Item, in single-block freelist table, at 0-based index 'v'
			-- from address
		do
			Result := ushort_at_offset (const.K_off_ch_freelist_s_table + v)
		end

	small_range_freelist_table: like Ktype_index_array
			-- Table of small block range freelist addresses
			-- 'frtbl_1'
			-- from address
		do
			Result := ushort_array_at_offset (
				const.K_off_ch_freelist_r1_table,
				const.K_freelist_r1_table_capacity)
		end

	large_range_freelist_table: like Ktype_index_array
			-- Table of large block range freelist addresses
			-- 'frtbl_2'
			-- from address
		do
			Result := ushort_array_at_offset (
				const.K_off_ch_freelist_r2_table,
				const.K_freelist_r2_table_capacity)
		end

	eoh_marker: like Ktype_index
			-- End-of-head marker
			-- 'eoh'
			-- from address
		do
			Result := ushort_at_offset (eor_offset)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_initial_block_count (v: like original_block_count)
			-- Set original block count field (ibc) to 'v'
			-- Also set current block count field (cbc)
		do
			put_ushort_at_offset (v, const.K_off_ch_original_block_count)
			put_ushort_at_offset (v, const.K_off_ch_current_block_count)
			defined_initial_block_count := v
		ensure
			is_set: original_block_count = v
			current_is_set: current_block_count = v
		end

	--|--------------------------------------------------------------

	set_item_count (v: INTEGER)
			-- Set the item count field to 'v'
		do
			put_ushort_at_offset (v.as_natural_16, const.K_off_ch_item_count)
		end

	increment_item_count
		do
			put_ushort_at_offset (
				ushort_at_offset (const.K_off_ch_item_count) + 1,
				const.K_off_ch_item_count)
		end

	decrement_item_count
		do
			put_ushort_at_offset (
				ushort_at_offset (const.K_off_ch_item_count) - 1,
				const.K_off_ch_item_count)
		end

	--|--------------------------------------------------------------

	set_content_start_index (v: like content_start_index)
			-- Set the content start index (csi) field to 'v'
		do
			put_ushort_at_offset (
				v.as_natural_16, const.K_off_ch_content_start_index)
		end

	--|--------------------------------------------------------------

	set_highest_block_index (v: like highest_block_index)
			-- Set the highest block index (hbi) field to 'v'
		do
			put_ushort_at_offset (
				v.as_natural_16, const.K_off_ch_highest_block_index)
		end

	--|--------------------------------------------------------------

	increment_revision
		do
			put_ushort_at_offset (
				ushort_at_offset (const.K_off_ch_revision_field) + 1,
				const.K_off_ch_revision_field)
		end

	put_freelist_s_table_item (v: NATURAL_16; pos: INTEGER)
			-- Insert the value 'v' at 0-based position 'pos' in the
			-- single block deletions list table
			-- Item is block address of a deletions list (aka free list)
		do
			put_ushort_at_offset (v, const.K_off_ch_freelist_s_table + pos)
		end

	put_freelist_r1_table_item (v: NATURAL_16; pos: INTEGER)
			-- Insert the value 'v' at 0-based position 'pos' in the
			-- small block range freelist table
			-- Item is block address of a freelist
		do
			put_ushort_at_offset (v, const.K_off_ch_freelist_r1_table + pos)
		end

	put_freelist_r2_table_item (v: NATURAL_16; pos: INTEGER)
			-- Insert the value 'v' at 0-based position 'pos' in the
			-- large block range freelist table
			-- Item is block address of a freelist
		do
			put_ushort_at_offset (v, const.K_off_ch_freelist_r2_table + pos)
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING
			-- Hex dump representation of contents of Current, by field
		local
			xd: AEL_PF_HEXDUMP
		do
			create xd.make_with_addr (address, blocksize)
			Result := xd.to_string (0, 0, "", 0, 0)
		end

	--|--------------------------------------------------------------

	formatted_out: STRING
			-- String representation of contents of Current, by field
		do
			create Result.make (blocksize * 8)
			Result.append (apf.aprintf (
				"st=%%02x:obc=%%05d:cbc=%%05d:ic=%%05d:dbc=%%05d:hbi=%%05d:csi=%%05d:rev=%%05d:imc=%%03d:sdc=%%03d:r1dc=%%03d:r2dc=%%03d%N",
				<< structure_type, original_block_count, current_block_count,
				item_count, deleted_block_count, highest_block_index,
				content_start_index, revision, index_map_count,
				single_freelist_count,
				small_range_freelist_count, large_range_freelist_count
				>>))
		end

--|========================================================================
feature -- Constants and predefined values
--|========================================================================

	defined_structure_type: NATURAL_8
			-- Structure type of Current, as defined at creation time
		do
			Result := defined_type_marker
		end

	defined_initial_block_count: INTEGER_32
			-- Number of blocks (exclusive of header blocks)
			-- in Current at time of creation

	defined_type_marker: NATURAL_8
			-- Defined block type marker for Current

	defined_eor_marker: NATURAL_16
			-- Defined EOR marker for Current
		once
			Result := const.K_eor_marker_core
		end

--|========================================================================
feature -- Shared resources
--|========================================================================

--RFO 	apf: AEL_PRINTF
--RFO 		once
--RFO 			create Result
--RFO 		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Jan-2013
--|     Compiled and tested on Eiffel 7.1
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate using one of the creation routines.
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPED_HASH_TABLE_CORE_HEADER
