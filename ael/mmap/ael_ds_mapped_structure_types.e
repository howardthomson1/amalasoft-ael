note
	description: "{
Type definitions for mapped structures
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_DS_MAPPED_STRUCTURE_TYPES

--|========================================================================
feature -- Type anchors
--|========================================================================

	Ktype_index: NATURAL_16
			-- Type anchor for block indices
			-- Block indices are in numbers of blocks

	K_size_index_type: INTEGER_32 = 2
			-- Number of bytes in index_type

	Ktype_addr: NATURAL_32
			-- Type anchor for byte addresses

	K_size_addr_type: INTEGER_32 = 4
			-- Number of bytes in addr_type

	Ktype_index_array: ARRAY [like Ktype_index]
			-- Type anchor for arrays of block indices
		require
			False
		do
			check False then end
		ensure
			False
		end

	Ktype_byte: NATURAL_8
			-- Type anchor for bytes

	to_index_type (v: INTEGER): like Ktype_index
		do
			Result := v.as_natural_16
		end

	--|--------------------------------------------------------------

	empty_index_array_1: like Ktype_index_array
			-- Empty 1-based array of like Ktype_index
		do
			create Result.make_empty
		end

	empty_index_array_0: like Ktype_index_array
			-- Empty 1-based array of like Ktype_index
		do
			create Result.make_empty
			Result.rebase (0)
		end

	--|--------------------------------------------------------------

	access_type_label (v: INTEGER): STRING
			-- Access type label for type value 'v'
		do
			if v >= 0 and v <= 7 then
				Result := access_type_labels [v]
			else
				Result := "UNDEFINED"
			end
		end

	access_type_labels: ARRAY [STRING]
		once
			Result := <<
				"Indexed",
				"Hashed",
				"RESERVED",
				"RESERVED",
				"Tree",
				"1-Way Linked",
				"2-Way Linked",
				"RESERVED"
				>>
				Result.rebase (0)
		end

	--|--------------------------------------------------------------

	i32_to_pointer (v: INTEGER_32): POINTER
			-- Convert integer 32 'v' to equivalent POINTER value
		do
			Result := default_pointer + v
		end

	n32_to_pointer (v: NATURAL_32): POINTER
			-- Convert natural 32 'v' to equivalent POINTER value
		do
			Result := default_pointer + v.as_integer_32
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Jan-2013
--|     Compiled and tested on Eiffel 7.1
--|----------------------------------------------------------------------
--| How-to
--|
--| Inherit by class that uses its features or instantiate by default
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPED_STRUCTURE_TYPES
