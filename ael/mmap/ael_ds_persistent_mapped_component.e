note
	description: "{
Abstract notion of a persistent (memory) mapped component
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_PERSISTENT_MAPPED_COMPONENT

inherit
	AEL_DS_MAPPED_COMPONENT

--|----------------------------------------------------------------------
--| History
--|
--| 001 18-Oct-2013
--|     Spun off from ael_ds_mapped_component.e
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--|----------------------------------------------------------------------

end -- class AEL_DS_PERSISTENT_MAPPED_COMPONENT
