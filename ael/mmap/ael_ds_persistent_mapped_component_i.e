note
	description: "{
Abstract implementation of (memory) mapped file components
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_PERSISTENT_MAPPED_COMPONENT_I

inherit
	AEL_DS_MAPPED_COMPONENT_I

--|========================================================================
feature -- Persistence support
--|========================================================================

	is_saved: BOOLEAN
		do
			if attached pers_pathname as lpp then
				Result := afr.file_by_name_exists (lpp.out)
			end
		end

	bak_file_enabled: BOOLEAN
			-- On save, is creation of a .bak file enable?

	--|--------------------------------------------------------------

	save_as (fn: STRING)
			-- Save entire contents of current as path name 'fn'
			-- Saved file becomes the file associated with Current
			-- and 'fn' becomes Current's path
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			set_pers_pathname (fn)			
			save
		ensure
			path_retained: (not has_error) implies
				attached pers_pathname as lpp and then lpp.out ~ fn
			saved: (not has_error) implies afr.file_by_name_exists (fn)
		end

	--|--------------------------------------------------------------

	set_pers_pathname (fn: STRING_8)
		require
			valid: fn /= Void and then not fn.is_empty
		do
			create pers_pathname.make_from_string (fn)
			pers_dirname := pers_pathname.parent
			if attached pers_pathname.entry as lpe then
				pers_filename := lpe
			else
				record_error (
					"Persistent pathname [%%s] does not have an entry",
					pers_pathname.out)
			end
		end

	--|--------------------------------------------------------------

	save
			-- Save to a NEW file the entire structure
			-- If memory mapping is desired, do not use this function
		require
			has_path: pers_pathname /= Void
		local
			tmp_file, orig_file: RAW_FILE
			bak_file: detachable RAW_FILE
			tmp_name: STRING
			bak_name: detachable STRING
		do
			create_save_dir
			if pers_pathname /= Void then
				tmp_name := pers_pathname.out + ".tmp"
				create orig_file.make_with_path (pers_pathname)
				create tmp_file.make_with_name (tmp_name)
				if bak_file_enabled then
					bak_name := pers_pathname.out + ".bak"
					create bak_file.make_with_name (bak_name)
				end
				tmp_file.open_write
				if not tmp_file.is_open_write then
					set_error_message ("Could not open " + tmp_name)
				else
					write_to_file (tmp_file)
					tmp_file.close
					if attached bak_file and then attached bak_name as l_bn then
						if bak_file.exists then
							bak_file.delete
						end
						if orig_file.exists then
							orig_file.rename_file (l_bn)
						end
					end
					tmp_file.rename_path (pers_pathname)
				end
			end
		ensure
			saved: is_saved
		end

	--|--------------------------------------------------------------

	retrieve
			-- Populate Current from persistent storage (file)
			-- For partial population, use memory map routines instead
		require
			has_path: pers_pathname /= Void
		local
			tf: RAW_FILE
			td: DIRECTORY
			retrying: BOOLEAN
		do
			--| open file, read blocks of bytes, write into address, etc
			if not retrying
				and then pers_dirname /= Void and pers_filename /= Void
			 then
				create td.make_with_path (pers_dirname)
				if not td.exists then
					-- Big trouble, directory is not there, so how can 
					-- file possibly be?
					record_error (
						"In retrieve, directory [%%s] does not exist.",
						pers_dirname.out)
				else
					create tf.make_with_path (pers_pathname)
					if not tf.exists then
						record_error (
							"In retrieve, stored image [%%s] does not exist.",
							pers_pathname.out)
					else
						tf.open_read
						if not tf.is_open_read then
							record_error ("In retrieve, unable to open %%s",
							pers_pathname.out)
						else
							reload_from_file (tf)
							tf.close
						end
					end
				end
			end
		rescue
			if not retrying then
				-- Retrieval error
				retrying := True
				retry
			end
		end

--|========================================================================
feature -- Paths
--|========================================================================

	pers_pathname: detachable PATH
			-- Full pathname of file into which to store Current
		note
			option: stable
			attribute
		end

	pers_dirname: detachable PATH
			-- Name of directory in which resides the file into which
			-- to store Current
		note
			option: stable
			attribute
		end

	pers_filename: detachable PATH
			-- Basename of file into which to store Current
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	is_valid_pathname (v: STRING): BOOLEAN
			-- Is 'v' a valid persistent path name?
		require
			has_dirname: pers_dirname /= Void
			has_filename: pers_filename /= Void
		do
			if v /= Void and then not v.is_empty
				and then attached pers_dirname as lpd
					and then attached pers_filename as lpf
			 then
				 Result := v.starts_with (lpd.out) and v.ends_with (lpf.out)
			end
		end

--|========================================================================
feature {NONE} -- Persistence Implementation
--|========================================================================

	create_save_dir
			-- Create, if it does not already exists, the directory into 
			-- which to save Current
		require
			has_dirname: pers_dirname /= Void
		local
			td: DIRECTORY
		do
			if attached pers_dirname as lpd then
				create td.make_with_path (lpd)
				if not td.exists then
					td.recursive_create_dir
				end
			end
		end

	--|--------------------------------------------------------------

	write_to_file (tf: RAW_FILE)
			-- Write all of Current's headers and content to file 'tf'
		require
			exists: tf /= Void
			file_exists: tf.exists
			is_open: tf.is_open_write
		do
			--| use put_managed_pointer with area, start pos and length
		ensure
			file_exists: tf.exists
			is_open: tf.is_open_write
		end

	--|--------------------------------------------------------------

	reload_from_file (tf: RAW_FILE)
			-- Reload all of Current's headers and content from file 'tf'
		require
			exists: tf /= Void
			file_exists: tf.exists
			is_open: tf.is_open_read
		do
		ensure
			file_exists: tf.exists
			is_open: tf.is_open_read
		end

--|========================================================================
feature -- Shared services
--|========================================================================

	afr: AEL_SPRT_FILE_ROUTINES
		once
			create Result
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 18-Oct-2013
--|     Spun off from ael_ds_mapped_component_i.e
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--| This class is to be inherited by a corresponding _IMP class.
--|----------------------------------------------------------------------

end -- class AEL_DS_PERSISTENT_MAPPED_COMPONENT_I
