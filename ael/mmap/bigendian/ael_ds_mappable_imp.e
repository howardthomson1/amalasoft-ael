note
	description: "{
Big-endian, platform-aware implementation of
abstract notion of being memory-mappable.
Maps are stored in network (big-endian) order for optimal portability.
If maps are intended for single-platform use, native might be a better option.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_MAPPABLE_IMP

inherit
	AEL_DS_MAPPABLE_I

--|========================================================================
feature -- Support
--|========================================================================

	put_ushort_at_offset (v: NATURAL_16; o: INTEGER_32)
			-- Insert at address offset 'o' the 2-byte value 'v'
		do
			mr.put_ushort_be (v, address + o)
		end

	put_long_at_offset (v: INTEGER_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte signed value 'v'
		do
			mr.put_long_be (v, address + o)
		end

	put_ulong_at_offset (v: NATURAL_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte value 'v'
		do
			mr.put_ulong_be (v, address + o)
		end

	put_ulong_long_at_offset (v: NATURAL_64; o: INTEGER_32)
			-- Insert at address offset 'o' the 8-byte value 'v'
		do
			mr.put_ulong_long_be (v, address + o)
		end

	ushort_at_offset (o: INTEGER_32): NATURAL_16
			-- 2-byte value at address offset 'o'
		do
			Result := mr.ushort_at_be (address + o)
		end

	long_at_offset (o: INTEGER_32): INTEGER_32
			-- 4-byte signed value at address offset 'o'
		do
			Result := mr.long_at_be (address + o)
		end

	ulong_at_offset (o: INTEGER_32): NATURAL_32
			-- 4-byte value at address offset 'o'
		do
			Result := mr.ulong_at_be (address + o)
		end

	ulong_long_at_offset (o: INTEGER_32): NATURAL_64
			-- 8-byte value at address offset 'o'
		do
			Result := mr.ulong_long_at_be (address + o)
		end

--|========================================================================
feature -- Octet conversion (platform dependent)
--|========================================================================

	short_to_octets (v: INTEGER_16): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.short_to_octets_be (v)
		end

	ushort_to_octets (v: NATURAL_16): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ushort_to_octets_be (v)
		end

	--|--------------------------------------------------------------

	long_to_octets (v: INTEGER_32): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.long_to_octets_be (v)
		end

	ulong_to_octets (v: NATURAL_32): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ulong_to_octets_be (v)
		end

	--|--------------------------------------------------------------

	long_long_to_octets (v: INTEGER_64): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.long_long_to_octets_be (v)
		end

	ulong_long_to_octets (v: NATURAL_64): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ulong_long_to_octets_be (v)
		end

	octets_to_n64 (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		do
			Result := mr.octets_to_n64_be (v)
		end

	octets_to_i64 (v: STRING): INTEGER_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		do
			Result := mr.octets_to_i64_be (v)
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Oct-2013
--|     Original from ael_ds_mapped_component_imp (7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--| This class is to be inherited by AEL_DS_MAPPED_COMPONENT.
--| There are possibly several variants of this class
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPABLE_IMP
