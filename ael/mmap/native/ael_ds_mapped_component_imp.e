note
	description: "{
Little-endian (e,g, Intel) platform-specific implementation of
Abstract notion of a (memory) mapped component.
Maps are stored in native order for optimal efficiency.
If maps are intended for streaming or network use, big-endian might be
a better option.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_MAPPED_COMPONENT_IMP

inherit
	AEL_DS_MAPPED_COMPONENT_I

--|========================================================================
feature -- Support
--|========================================================================

	put_ushort_at_offset (v: NATURAL_16; o: INTEGER_32)
			-- Insert at address offset 'o' the 2-byte value 'v'
		do
			mr.c_put_ushort (v, address + o)
		end

	put_long_at_offset (v: INTEGER_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte signed value 'v'
		do
			mr.c_put_long (v, address + o)
		end

	put_ulong_at_offset (v: NATURAL_32; o: INTEGER_32)
			-- Insert at address offset 'o' the 4-byte value 'v'
		do
			mr.c_put_ulong (v, address + o)
		end

	put_ulong_long_at_offset (v: NATURAL_64; o: INTEGER_32)
			-- Insert at address offset 'o' the 8-byte value 'v'
		do
			mr.c_put_ulong_long (v, address + o)
		end

	ushort_at_offset (o: INTEGER_32): NATURAL_16
			-- 2-byte value at address offset 'o'
		do
			Result := mr.c_ushort_at (address + o)
		end

	long_at_offset (o: INTEGER_32): INTEGER_32
			-- 4-byte signed value at address offset 'o'
		do
			Result := mr.c_long_at (address + o)
		end

	ulong_at_offset (o: INTEGER_32): NATURAL_32
			-- 4-byte value at address offset 'o'
		do
			Result := mr.c_ulong_at (address + o)
		end

	ulong_long_at_offset (o: INTEGER_32): NATURAL_64
			-- 8-byte value at address offset 'o'
		do
			Result := mr.c_ulong_long_at (address + o)
		end

--|========================================================================
feature -- Octet conversion (platform dependent)
--|========================================================================

	short_to_octets (v: INTEGER_16): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.short_to_octets_ne (v)
		end

	ushort_to_octets (v: NATURAL_16): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ushort_to_octets_ne (v)
		end

	--|--------------------------------------------------------------

	long_to_octets (v: INTEGER_32): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.long_to_octets_ne (v)
		end

	ulong_to_octets (v: NATURAL_32): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ulong_to_octets_ne (v)
		end

	--|--------------------------------------------------------------

	long_long_to_octets (v: INTEGER_64): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.long_long_to_octets_ne (v)
		end

	ulong_long_to_octets (v: NATURAL_64): STRING_8
			-- Octet stream equivalent of 'v'
		do
			Result := mr.ulong_long_to_octets_ne (v)
		end

	octets_to_n64 (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		do
			Result := mr.octets_to_n64_ne (v)
		end

	octets_to_i64 (v: STRING): INTEGER_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		do
			Result := mr.octets_to_i64_ne (v)
		end

--|----------------------------------------------------------------------
--| History
--|
--| 003 05-Apr-2013
--|     Split original class into _I, _IMP and child classes
--| 002 13-Mar-2013
--|     Added initial persistence support
--|     Compiled and tested on Eiffel 7.2
--| 001 14-Jan-2013
--|     Compiled and tested on Eiffel 7.1
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--| This class is to be inherited by AEL_DS_MAPPED_COMPONENT.
--| There are possibly several variants of this class
--|----------------------------------------------------------------------

end -- class AEL_DS_MAPPED_COMPONENT_IMP
