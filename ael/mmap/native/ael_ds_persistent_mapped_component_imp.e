note
	description: "{
Little-endian (e,g, Intel) platform-specific implementation of
Abstract notion of a persistent (memory) mapped component.
Maps are stored in native order for optimal efficiency.
If maps are intended for streaming or network use, big-endian might be
a better option.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

deferred class AEL_DS_PERSISTENT_MAPPED_COMPONENT_IMP

inherit
	AEL_DS_MAPPED_COMPONENT_IMP
	AEL_DS_PERSISTENT_MAPPED_COMPONENT_I

--|----------------------------------------------------------------------
--| History
--|
--| 001 18-Oct-2013
--|     Spun off from ael_ds_mapped_component_imp.e
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is deferred and not directly instantiable.
--| This class is to be inherited by AEL_DS_MAPPED_COMPONENT.
--| There are possibly several variants of this class
--|----------------------------------------------------------------------

end -- class AEL_DS_PERSISTENT_MAPPED_COMPONENT_IMP
