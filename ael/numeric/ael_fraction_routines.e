--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Routines for converting floating point values to fractions
--|----------------------------------------------------------------------

class AEL_FRACTION_ROUTINES

create
	default_create

--|========================================================================
feature -- Conversion
--|========================================================================

	float_to_fraction (v: DOUBLE): AEL_FRACTION
			-- The fractional equivalent of the given value
			--
			-- A zero denominator denotes a "no match" situation, where it was
			-- impossible to get a close-enough match using the default
			-- parameters.  If this occurs, consider using the lower-level
			-- function fuzzy_float_to_fractional_parts and non-default parameters
		local
			f3: like fuzzy_float_to_fractional_parts
		do
			f3 :=  fuzzy_float_to_fractional_parts (v, 0, 0, default_denominators)
			create Result.make_with_components (f3.integer_item (1),
			f3.integer_item (2),
			f3.integer_item (3))
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	fuzzy_float_to_fractional_parts (d: DOUBLE;
	                                 flim, fzp: INTEGER;
												den_list: ARRAY [INTEGER]
										):TUPLE [INTEGER, INTEGER, INTEGER, INTEGER]
			-- FF2F - Convert the given floating point number into component
			-- fractional parts.
			-- Result is a TUPLE of the whole part, the fractional numerator,
			-- the fractional denominator and the precision at which the
			-- conversion occured (number of decimal places)
			--
			-- A zero denominator denotes a "no match" situation
			--
			-- Testing and conversion takes place in precision-major order.
			-- That is, all of the candidate denominators are tested for a given
			-- precision, then the precision is lowered.  This means it is
			-- possible to get a match of a higher-value denominator when the
			-- expected match might have been to a lower value (with less
			-- precision).
			-- A non-zero "flim" argument limits the precision to the given
			-- number of decimal places (acceptable values from 1 through 4).
			-- A flim of 4 begins with a 4-decimal place test and could result
			-- in a higher denominator than a test at fewer decimal places.
			-- The default precision begins with 3 decimal places.
			--
			-- Fuzziness is regulated by the "fzp" argument.  By default,
			-- matches occur when the values are within 2 percent at the
			-- specified precision.  This value is over-ridden by a non-zero
			-- "fzp" argument.  Tha value is expressed in terms of percent
			-- deviation (for the given precision).
			--
			-- The "denoms" argument is a sorted (lowest to highest) sequence of
			-- denominators to use for the testing.  If this is Void or empty,
			-- then a default sequence is used
		require
			valid_precision: is_valid_precision(flim)
			valid_fuzziness: is_valid_fuzziness(fzp)
			valid_denominators: (den_list /= Void and then not den_list.is_empty)
				implies denominators_are_valid(den_list)
		local
			wp, rb_frac, den, i, lim, plim: INTEGER
			tmod, prec, pfactor, b_denom, fuzzp, pct_dev: INTEGER
			fp, fxd: DOUBLE
			denoms: ARRAY [INTEGER]
		do
			create Result

			if den_list = Void or else den_list.is_empty then
				denoms := default_denominators
			else
				denoms := den_list
			end

			-- Set percentage of allowable deviation
			if fzp > 0 then
				fuzzp := fzp
			else
				fuzzp := K_default_fractional_fuzziness
			end

			-- Whole part is easy enough

			wp := d.truncated_to_integer
			fp := d - wp
			Result.put_integer (wp, 1)

			-- These are, if not common, at least reasonable

			lim := denoms.count

			-- If non-default precision is desired, set it here

			if flim > 0 then
				plim := flim
			else
				plim := K_default_fractional_precision
			end

			-- For each level of precision, from highest to lowest,
			-- conduct tests for each candidate denominator until a match is
			-- found

			from prec := plim
			until (prec < 1) or (den /= 0)
			loop
				-- Decimal shift the fractional parts to match the precision

				pfactor := (10^prec).rounded
				rb_frac := (fp*pfactor).rounded
				from i := 1
				until i > lim or den /= 0
				loop
					-- For each candidate denominator, calculate the modulo
					-- remainder of the biased fractional part by the
					-- denominator's modulo factor.  For example, the denominator
					-- '2' has a modula factor of 1/2 the biasing factor (e.g. 100//2).
					--
					-- If the remainder is zero or close to zero (per fuzz
					-- factor), then it's a match.
					-- To get the numerator, divide the biased fractional part by the
					-- biased denominator.
					-- b_denom is the biased denominator
					-- rb_frac is the rounded biased fractional part
					-- tmod is the modulo remainder
					-- pct_dev is the percent deviation from an even modulus

					-- e.g. 100 // 2 -> 50 (1/2 of 100)
					-- e.g. 100 // 6 -> 16 (1/6 of 100)
					-- NOTE - there is an issue when the denominator approaches
					-- the value of the scaling factor
					-- e.g. 100 // 64 -> 1 (1/64 of 100)

					fxd := pfactor / denoms.item (i)
					b_denom := fxd.rounded
					-- e.g. 145 \\ 33 -> 4

					tmod := rb_frac \\ b_denom
					-- e.g. (4 * 100) -> 4
					-- e.g. 145 \\ 1 -> 0
					pct_dev := (tmod * 100) // rb_frac
					if (tmod = 0) or (pct_dev <= fuzzp) then
						-- An even fraction (at least at 'pf' decimal places),
						-- or a value within the acceptable range of deviation
						den := denoms.item (i)
						Result.put_integer ((rb_frac / b_denom).rounded, 2)
						Result.put_integer (denoms.item (i), 3)
						Result.put_integer (prec, 4)
					else
						i := i + 1
					end
				end
				prec := prec - 1
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Validation
--|========================================================================

	denominators_are_valid (dl: ARRAY [INTEGER]): BOOLEAN
			-- Are all denominators in the given list valid?
		require
			exists: dl /= Void
		local
			i, lim: INTEGER
		do
			if not dl.is_empty then
				Result := True
				lim := dl.upper
				from i := dl.lower
				until (not Result) or (i > lim)
				loop
					Result := is_valid_denominator (dl.item (i))
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_denominator (v: INTEGER): BOOLEAN
			-- Is the given value a valid denomiator?
		do
			Result := v > 0
		end

	is_valid_numerator (v: INTEGER): BOOLEAN
			-- Is the given value a valid numerator?
		do
			Result := v >= 0
		end

	--|--------------------------------------------------------------

	is_valid_precision (v: INTEGER): BOOLEAN
			-- Is the given value a valid level of precision?
		do
			Result := v >= 0 and v <= 4
		end

	is_valid_fuzziness (v: INTEGER): BOOLEAN
			-- Is the given value a valid percentag of fuzziness?
		do
			Result := v >= 0 and v < 100
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	default_denominators: ARRAY [INTEGER]
			-- Default set of denominators for comparison and result
		do
			Result := simple_denominators
		ensure
			all_valid: denominators_are_valid (Result)
		end

	simple_denominators: ARRAY [INTEGER]
			-- Default set of denominators for comparison and result
		once
			Result := << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 16, 32, 64, 128 >>
		ensure
			all_valid: denominators_are_valid (Result)
		end

	power_of_2_denominators: ARRAY [INTEGER]
			-- Set of denominators containing only powers of 2
		once
			Result := << 1, 2, 4, 8, 16, 32, 64, 128 >>
		ensure
			all_valid: denominators_are_valid (Result)
		end

	K_default_fractional_precision: INTEGER = 3
			-- Number of decimal places to which to carry comparisons

	K_default_fractional_fuzziness: INTEGER = 2
			-- Percentage error acceptable for comparisons at each precision

end -- class AEL_FRACTION_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|
--| This class is typically inherited by another class needing it
--| capabilities.
--| This class can be instantiated by itself if desired.
--|----------------------------------------------------------------------
