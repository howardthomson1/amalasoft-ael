--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Routines for specialized numeric processing
--|----------------------------------------------------------------------

class AEL_NUMERIC_ROUTINES

inherit
	AEL_NUMERIC_INTEGER_ROUTINES

create
	default_create

--|========================================================================
feature -- Conversion
--|========================================================================

	real_rounded_to_next_even_mod (v: REAL_64; m: INTEGER; zero_ok: BOOLEAN): INTEGER
			-- The given number 'v', rounded toward higher magnitude,
			-- to an even multiple of 'm'
			-- Positive numbers are rounded up
			-- Negative numbers are rounded down
			-- If zero is acceptable (the 'zero_ok' flag = True) then
			-- a value of zero will yield a result of zero.  If not,
			-- then a value of zero will yield a result of 'm'
		require
			valid_mod: m > 1
		local
			tv: INTEGER
		do
			inspect v.sign
			when 1 then
				tv := v.ceiling
			when -1 then
				tv := v.floor
			else
			end
			Result := rounded_to_next_even_mod (tv, m, zero_ok)
		end

	--|--------------------------------------------------------------

	rounded_64 (v: DOUBLE): INTEGER_64
			-- Double precision value 'v' rounded to 64 bit inteeger
		do
			Result := v.rounded_real_64.truncated_to_integer_64
		end

	--|--------------------------------------------------------------

	real_remainder (v1, v2: REAL): REAL
			-- Floating point remainder of v1 / v2
			-- Like v1 \\ v2 for reals
		local
			tf1, tf2, vmax, maxv, mx: REAL
			ti1, ti2: INTEGER
		do
			-- Bump size of reals to avoid losing too much precision
			vmax := v1.max (v2)
			maxv := v1.max_value
			if vmax < (maxv / 100_000) then
				mx := 100_000
			elseif vmax < (maxv / 10_000) then
				mx := 10_000
			elseif vmax < (maxv / 1_000) then
				mx := 1_000
			elseif vmax < (maxv / 100) then
				mx := 100
			elseif vmax < (maxv / 10) then
				mx := 10
			else
				mx := 1
			end
			tf1 := v1 * mx
			tf2 := v2 * mx
			ti1 := tf1.rounded.abs
			ti2 := tf2.rounded.abs
			Result := (ti1 \\ ti2) / mx
		end

	--|--------------------------------------------------------------

	double_to_parts (v: DOUBLE): ARRAY [INTEGER_64]
			-- Decomposition of the double precision value 'v'
			-- into a pair of 64 bit INTEGERs representing the
			-- whole and factional Decimal parts, where the fractional
			-- part is biased by 10^7 to become a whole number.
			-- A third value in the Result array is the exponent
			-- bias (the power of 10 by which the 2nd value is
			-- less than 7), for the convenience of the caller.
			-- This (not coincidentially) is the number of leading
			-- zeroes in the fractional part
			--
			-- Double precision uses 24 bits for fractional exponent
			-- so that is the extent of precision available.
			--
			-- Fractional part result smaller than 10^7 imply
			-- smaller orders of magnitude in the original
			-- fractional part
			-- Example:  555.1234567890 yields [555, 1234568 0]
			-- Example:  555.123 yields [555, 1230000, 0]
			-- Example:  555.00000123 yields [555, 12, 5]
			-- Example:  -555.00123 yields [-555, 12300, 2]
		local
			is_neg: BOOLEAN
			wp, fp: INTEGER_64
			pv, tv: DOUBLE
		do
			is_neg := v < v.zero
			if is_neg then
				pv := -v
			else
				pv := v
			end
			wp := rounded_64 (pv)
			-- Shift value to normalize fractional part to whole
			-- DP uses 24 bits for fractional exponent
			tv := ((pv * (10^7)) - (wp * (10^7)))
			fp := rounded_64 (tv)
			if is_neg then
				wp := - wp
			end
			Result := << wp, fp, 7 - rounded_64 (log10 (tv)) - 1 >>
		ensure
			exists: Result /= Void and then Result.count = 3
		end

--|========================================================================
feature -- Logarithms
--|========================================================================

	logn (v: REAL_64): REAL_64
			-- Natural logarithm of `v'
		do
			Result := dbl_math.log (v)
		end

	log2 (v: REAL_64): REAL_64
			-- Base 2 logarithm of `v'
		do
			Result := dbl_math.log (v) / dbl_math.log ({REAL_64} 2.0)
		end

	log8 (v: REAL_64): REAL_64
			-- Base 8 logarithm of `v'
		do
			Result := dbl_math.log (v) / logn_8
		end

	log10 (v: REAL_64): REAL_64
			-- Base 10 logarithm of `v'
		do
			Result := dbl_math.log10 (v)
		end

	log16 (v: REAL_64): REAL_64
			-- Base 16 logarithm of `v'
		do
			Result := dbl_math.log (v) / logn_16
		end

	logx (v: REAL_64; b: INTEGER): REAL_64
			-- Base 'b' logarithm of `v'
		do
			inspect b
			when 8 then
				Result := log8 (v)
			when 10 then
				Result := log10 (v)
			when 16 then
				Result := log16 (v)
			else
				Result := logb (v, b)
			end
		end

	logb (v: REAL_64; b: INTEGER): REAL_64
			-- Base 'b' logarithm of `v'
		do
			Result := dbl_math.log (v) / dbl_math.log (b.to_double)
		end

	--|--------------------------------------------------------------

	logn_8: REAL_64
			-- Natural log of 8
		once
			Result := dbl_math.log ({REAL_64}8.0)
		end

	logn_16: REAL_64
			-- Natural log of 16
		once
			Result := dbl_math.log ({REAL_64}16.0)
		end

--|========================================================================
feature {NONE} -- Shared
--|========================================================================

	dbl_math: DOUBLE_MATH
		once
			create Result
		end

end -- class AEL_NUMERIC_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 24-Feb-2013
--|     Added log functions
--|     Compiled and tested with void-safe EiffelStudio 7.1
--| 003 28-Jul-2009
--|     Added rounded_64 (for double-to-int64 rounding) and 
--|     double_to_parts (for decomposition of dp values into whole 
--|     and fractional parts
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|
--| This class is typically inherited by another class needing it
--| capabilities.
--| This class can be instantiated by itself if desired.
--|----------------------------------------------------------------------
