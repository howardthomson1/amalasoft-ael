note
	description: "{
A complex formatting parameter, including a container format and
component format parts
A complex format token has a more complex structure:
  "%C(" <container_fmt> [ <sub_fmt> ] ')'
    <container_fmt> ::= List, Matrix or Table std syntax
    <sub_fmt> ::= '[' <fmt> ']' <fmt_args>
    <fmt> ::= per-item format string, std syntax
    <fmt_args> ::= '$'<tags>   (position-relative, unseparated)
    <tags> ::= <tag> [<tags>]
    <tag> ::= V | I | K | R | C
  Where:
    V=item value (all containers)
    I=item position (List and Table)
    K=item key (Hash Table only)
    R=item row (Matrix only)
    C=item column (Matrix only)

  Examples:
    %C(#L[%-4d: %+16s]$IV)
    %C(+M[(%04d,%04d) %+16s]$CRV)
    %C(,H[%a (by %s, at %d)]$VKI)
    %C(L)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_COMPLEX_FORMAT_PARAM

inherit
	AEL_PF_FORMATTING_CONSTANTS
		undefine
			default_create
		end
	AEL_PF_FORMAT_ERROR_SUPPORT
		redefine
			default_create
		end
	AEL_PF_TYPE_ROUTINES
		undefine
			default_create
		end

create
	make_from_string

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_string (v: STRING; spos: INTEGER)
			-- Create and Initialize Current from string 'v', beginning 
			-- with start position 'spos'
		require
			exists: v /= Void
			valid_position: (spos > 0) and (spos < v.count)
			long_enough: (v.count - spos) >= 3
			valid_start: v.item (spos) = 'C'
			has_open: v.item (spos+1) = '('
		do
			original_string := v
			original_position := spos
			default_create
		end

	--|--------------------------------------------------------------

	default_create
		do
			create tokens.make
			item_format := ""
			args := ""
		end

--|========================================================================
feature -- Status
--|========================================================================

	original_string: STRING
			-- Reference to the whole original format string given

	original_position: INTEGER
			-- Original starting position in the format string

	has_format_error: BOOLEAN
			-- Has a format error been encountered?

	has_arg_type_error: BOOLEAN
			-- Has an argument type mismatch been encountered?

	container_param: detachable AEL_PF_FORMAT_PARAM
			-- Format parameter for container part

	container_type: INTEGER
			-- Type of container, based on type tag

	is_typed: BOOLEAN
			-- Has the container type been defined?
		do
			Result := container_type /= 0
		end

	item_format: STRING
			-- Format string for per-item output

	args: STRING
			-- Subordinate element arguments, each represented by a 
			-- single character

	tokens: LINKED_LIST [AEL_PF_FORMAT_TOKEN]
			-- Tokens extracted from component expression

	param_length: INTEGER
			-- Length of entire parameter string, from 'C' through 
			-- closing paren

--|========================================================================
feature -- Status setting
--|========================================================================

	analyze
			-- Parse original string, from original position through end 
			-- of complex format expression, capturing elements and 
			-- checking for syntax and other errors
		local
			i, lim, sp, ep, p: INTEGER
			ts, cfmt, istr: STRING
			has_subs: BOOLEAN
			fc: CELL [INTEGER]
			ct: CHARACTER
			tlist: LINKED_LIST [AEL_PF_FORMAT_TOKEN]
		do
			ts := original_string
			lim := ts.count
			sp := original_position + 1 -- index of open paren
			-- 'sp' was at the 'C' tag, now at next char='('
			-- Preconditions on create require at least "C(" at start,
			-- and ensure at least 4 chars "C(L)" total
			-- Find end of complex expression (closing paren)
			-- N.B. this syntax precludes having a deliberately dangling 
			-- closing paren in the format string.
			ep := index_of_closing_paren (ts, sp)
			param_length := ep
			if ep = 0 then
				-- Format error
				add_format_error (
					ts, sp, "Missing closing paren")
			else
				-- Find range of container format
				-- "%C("<cfmt>[<ifmt>[<iargs>]]")"
				-- <cfmt> ::= ["$"][<sep>]<ctype>
				-- <ctype> ::= 'L' | 'H' | 'M'
				-- <ifmt> ::= "["<format_string>"]"
				-- <iargs> ::= "$" {VIKRC)*
				-- E.g. "%%C(#,L[%%03d: %%d]$IV)"
				-- From i = 1st char after '(' to first component opening 
				-- '[' or closing ')', whichever comes first
				lim := ep
				i := sp + 1
				p := ts.index_of ('[', i)
				cfmt := "%%"
				if p /= 0 and p < ep then
					-- Has component parts, at least per-item format
					has_subs := True
					cfmt.append (ts.substring (i, p - 1))
					sp := p
				else
					-- Degenerate case, just the container format
					cfmt.append (ts.substring (i, ep - 1))
				end
				-- Check the container tag - only 3 options, and must be
				-- the last character in pre-components substring (just 
				-- before the first open bracket)
				ct := cfmt.item (cfmt.count)
				inspect ct
				when 'L' then
					container_type := K_printf_fmt_type_list
				when 'S' then
					container_type := K_printf_fmt_type_sequence
				when 'H' then
					container_type := K_printf_fmt_type_table
				when 'M' then
					container_type := K_printf_fmt_type_matrix
				else
					-- Format Error, invalid container type tag
					add_format_error (
						ts, sp, "Invalid container type tag [" + ct.out + "]")
					has_format_error := True
				end
				-- Create an as-if container param
				create container_param.make_from_string (cfmt, 1)
				if (not has_format_error) and has_subs then
					-- Extract subordinate elements
					-- 'sp' points to the opening '[' of the item format
					-- 'ep' points to the closing paren
					-- Item format should be the substring between the 
					-- opening '[' and the last closing ']' before 'ep'
					p := ts.last_index_of (']', ep)
					if p = 0 then
						-- Format Error
						add_format_error (
							ts, sp, "Missing closing bracket")
						has_format_error := True
					else
						item_format.append (ts.substring (sp + 1, p - 1))
						-- Now check for item arguments
						-- Arguments, if any, begin with '$' immediately 
						-- after the closing bracket if the item format
						-- No $, no args - but maybe a syntax error
						if p = ep - 1 then
							-- No args, no problem
						elseif ts.item (p + 1) /= '$' then
							-- Format Error
							add_format_error (
								ts, p, "Missing '$' before complex arguments")
							has_format_error := True
						else
							-- Extract args, less '$'
							istr := ts.substring (p + 2, ep - 1)
							-- Now check for validity
							create fc.put (0)
							validate_arg_string (istr, fc)
							if fc.item /= 0 then
								-- Format Error
								add_format_error (
									ts, p + fc.item + 1,
									"Invalid argument tag [" +
										istr.item (fc.item).out + "]")
								has_format_error := True
							else
								args.append (istr)
							end
						end
						if not has_format_error then
							-- Create the item format param
							create fc.put (0)
							tlist := Printf_fmt_funcs.tokens_extracted (
								item_format, fc)
							tokens.fill (tlist)
						end
					end
				end
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	validate_arg_string (v: STRING; epc: CELL [INTEGER])
			-- Validate 'v' as an argument string
			-- Does NOT include leading '$', only item type tags
			-- Not valid if any characters are not valid
			-- If invalid character is found, put its index into 'epc'
			-- If 'epc' has no non-zero item, then is valid
		local
			i, lim: INTEGER
		do
			if attached v as tv then
				-- OK (but kinda dumb) to be empty
				lim := tv.count
				from i := 1
				until i > lim or epc.item > 0
				loop
					if not Ks_item_type_tags.has (v.item (i).as_upper) then
						epc.put (i)
					end
					i := i + 1
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	index_of_closing_paren (buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing paren MATCHING the 
			-- opening paren at 'sp'
			-- Zero if not found
		require
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = '('
		do
			Result := index_of_closing ('(', buf, sp)
		end

	index_of_closing_bracket (buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing bracket MATCHING the 
			-- opening bracket at 'sp'
			-- Zero if not found
		require
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = '['
		do
			Result := index_of_closing ('[', buf, sp)
		end

	index_of_closing (oc: CHARACTER; buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing character MATCHING the 
			-- opening character 'oc' at position 'sp'
			-- Zero if not found
			--
			-- Note Well: DOES NOT allow for quoted or escaped characters!!
		require
			valid_open: oc = '(' or oc = '{' or oc = '[' or oc = '<'
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = oc
		local
			i, lim, pc: INTEGER
			cc, c: CHARACTER
		do
			inspect oc
			when '(' then
				cc := ')'
			when '{' then
				cc := '}'
			when '[' then
				cc := ']'
			when '<' then
				cc := '>'
			else
				-- Caught by precondition
			end
			lim := buf.count
			from i := sp
			until Result /= 0 or i > lim
			loop
				c := buf.item (i)
				if c = oc then
					pc := pc + 1
				elseif c = cc then
					pc := pc - 1
					if pc = 0 then
						Result := i
					end
				else
				end
				i := i + 1
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_item_type_tags: STRING = "VIKRC"
			-- Set of tag characters allowed in item arg lists

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Aug-2018
--|     Created original module (Eiffel 18.7)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_PF_COMPLEX_FORMAT_PARAM
