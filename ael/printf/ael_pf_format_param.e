note
	description: "{
An individual formatting parameter, encapsulating format string
parsing and interpretation, argument coordination and output
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_FORMAT_PARAM

inherit
	AEL_PF_FORMATTING_CONSTANTS
	AEL_PF_FORMAT_ERROR_SUPPORT
	AEL_PF_TYPE_ROUTINES

create
	make_from_string

 --|========================================================================
feature {NONE} -- Creation and Initialization
 --|========================================================================

	make_from_string (v: STRING; spos: INTEGER)
			-- Initialize this object from the given arguments
		require
			exists: v /= Void
			valid_position: (spos > 0) and (spos < v.count)
			long_enough: (v.count - spos) >= 1
			is_format: v.item(spos) = '%%'
		do
			core_make
			original_string := v
			original_position := spos
			extract_token
			-- init attachments
			argument := ""
		end

	--|--------------------------------------------------------------

	core_make
		do
			create token.make (4)
		end

 --|========================================================================
feature {NONE} -- Parsing and initialization
 --|========================================================================

	extract_token
			-- Extract the actual format parameter from the 
			-- original format string, beginning at the given position
			--
			-- If a simple format token, then structure is:
			-- '%' (after escaping)
			-- '#' (decoration) OR '~' (agent)
			-- '-', '+', or '=' (alignment)
			-- [N[.M]] (field width)
			-- <type_specifier>
			--
			-- A complex format token has a more complex structure
			-- "%C("<container_fmt>[<item_fmt>[<item_args>]]")"
			-- <container_fmt> ::= ["$"][<sep>]<ctype>
			-- <ctype> ::= 'L' | 'H' | 'M'
			-- <item_fmt> ::= "["<std_format_string>"]"
			-- <item_args> ::= "$" {VIKRC)*
			--  Where:
			--    V=item value (all containers)
			--    I=item position (List and Table)
			--    K=item key (Hash Table only)
			--    R=item row (Matrix only)
			--    C=item column (Matrix only)
			--
			--  Examples:
			--    %C(#L[%-4d: %+16s]$IV)
			--    %C(+M[(%04d,%04d) %+16s]$CRV)
			--    %C(,H[%a (by %s, at %d)]$VKI)
			--    %C(L)
		local
			i, lim: INTEGER
			iref: INTEGER_REF
			ts: like original_string
			cplx_fp: AEL_PF_COMPLEX_FORMAT_PARAM
		do
			ts := original_string
			lim := ts.count

			i := original_position
			--| The character at spos is, per contract,
			--| the leading percent sign
			token.extend (ts.item (i))
			i := i + 1
			inspect ts.item (i)
			when '#', '~' then --| Decoration and Agent flags
				token.extend (ts.item (i))
				i := i + 1
			when 'C' then
				-- A complex format expression.  Break into container and 
				-- subcomponent formats
				-- Next character MUST be an open paren, and must have 
				-- enough characters to complete at least the minimum 
				-- complex token, else it can't be a complex format param
				-- "C(L)"
				if ts.item (i + 1) /= '(' or (i+3) > lim then
					-- It's not valid, fall through
				else
					format_type := K_printf_fmt_type_complex
					create cplx_fp.make_from_string (ts, i)
					cplx_fp.analyze
					if cplx_fp.has_format_error then
--TODO is this reported outward?
						has_format_error := True
					else
						complex_param := cplx_fp
						-- If successful, complex token has the container 
						-- format, and has capture related bits and pieces.  
						-- As such, there is no need to extract this token, 
						-- simply transfer the findings (keeping them for 
						-- later) and skip the index past the complex token
						token.wipe_out
						token := original_string.substring (
							original_position,
							original_position + cplx_fp.param_length-1)
					end
				end
			else
			end
			if not has_error then
				if not is_complex then
					-- Capture alignment specifier, if any
					inspect ts.item (i)
					when '-', '=', '+' then
						token.extend (ts.item (i))
						set_alignment (ts.item (i))
						i := i + 1
					else
					end
					if ts.item (i).is_digit then --| Field width
						create iref
						iref.set_item (i)
						extract_field_width (iref)
						i := iref.item
					elseif ts.item (i) = '.' then
						-- Might be a floating point format missing an overall 
						-- field width, or might be a list or decimal separator
						if i < lim
							and then ts.item (i+1) = 'd'
								or ts.item (i+1) = 'L'
						 then
							 -- Leave stream intact, without advance.
							 --
							 -- extract_type_specifier will capture the separator
							 -- and advance as needed
						else
							field_width := 1
							create iref
							iref.set_item (i)
							extract_field_width (iref)
							i := iref.item
						end
					end
					--| Now get the type specifier
					if i > original_string.count then
						-- ERROR, ran off the end with a bad format string
						add_format_error (
							original_string, i-1, "Malformed format parameter")
						has_format_error := True
					else
						extract_type_specifier (i)
						if is_typed and not has_format_error then
							if is_float or is_percent then
								if not has_fractional_field_width then
									fractional_field_width :=
										K_printf_dflt_fp_fractional_width
								end
								if not has_field_width then
									field_width := K_printf_dflt_fp_whole_width
								end
							end
						end
					end
				end
			end
		ensure
			string_unchanged: original_string.is_equal(old original_string)
		end

	--|--------------------------------------------------------------

	set_alignment (v: CHARACTER)
			-- Set the alignment option according to the tag 'v'
		do
			inspect v
			when '-' then
				is_left_aligned := True
			when '=' then
				is_center_aligned := True
			when '+' then
				is_right_aligned := True
			end
		end

	--|--------------------------------------------------------------

	extract_field_width (iref: INTEGER_REF)
			-- From the given string, beginning at the position
			-- indicated by iref, extract the overall and,
			-- for floating point values, fractional field widths
			--
			-- Note Well: this field has a different interpretation for 
			-- List and Matrix formats (which don't use field widths).
			-- For those formats, the first value (ordinarily the field 
			-- width) is intepreted as the 1st index in a range, and the 
			-- second value (ordinarily the fractional field width) is 
			-- interpreted as the last index in a range.
		require
			position_exists: iref /= Void
			position_large_enough: iref.item > 0
			position_small_enough: iref.item <= original_string.count
			char1_is_digit_or_dp: original_string.item (iref.item).is_digit or
				(original_string.item (iref.item) = '.' and field_width /= 0)
		local
			i, lim: INTEGER
		do
			lim := original_string.count
			i := iref.item
			if original_string.item(i) = '0' then
				private_pad_character := '0'
			end
			from
			until (i > lim) or not original_string.item (i).is_digit
			loop
				token.extend (original_string.item (i))
				field_width :=
					(field_width * 10) + original_string.item (i).out.to_integer
				i := i + 1
			end
			if original_string.item (i) = '.' then --| Floating point or Pct
				token.extend (original_string.item (i))
				from i := i + 1
				until i > lim or not original_string.item (i).is_digit
				loop
					has_fractional_field_width := True
					token.extend (original_string.item (i))
					fractional_field_width := (fractional_field_width * 10)
					+ original_string.item (i).out.to_integer
					i := i + 1
				end
			end
			if has_fractional_field_width and field_width = 0 then
				field_width := fractional_field_width + 1
			end
			has_field_width := field_width /= 0
			iref.set_item (i)
		ensure
			field_flag_set:	has_field_width = True
			no_flag_no_frac: (not has_fractional_field_width)
				implies fractional_field_width = 0
		end

	--|--------------------------------------------------------------

	extract_type_specifier (pos: INTEGER)
			-- Capture the type specifier at position 'pos'
			--
			-- An exception to simple tag matching occurs with Complex 
			-- formats.  In that case, the complex aspect has already 
			-- been capture before now, and to check/set it here could 
			-- corrupt the state of the analysis
		require
			valid_position: pos > 0 and pos <= original_string.count
		local
			pc: CHARACTER
		do
			type_specifier := original_string.item (pos)
			token.extend (type_specifier)
			if pos < original_string.count then
				pc := original_string.item (pos + 1)
			end

			inspect type_specifier
			when 'a' then
				format_type := K_printf_fmt_type_any
			when 'A' then
				format_type := K_printf_fmt_type_agent
			when 'b' then
				format_type := K_printf_fmt_type_binary
			when 'B' then
				format_type := K_printf_fmt_type_boolean
			when 'c' then
				format_type := K_printf_fmt_type_character
			when 'd' then
				format_type := K_printf_fmt_type_decimal
			when 'f' then
				format_type := K_printf_fmt_type_float
			when 'H' then
				format_type := K_printf_fmt_type_table
			when 'L' then
				format_type := K_printf_fmt_type_list
			when 'M' then
				format_type := K_printf_fmt_type_matrix
			when 'o' then
				format_type := K_printf_fmt_type_octal
			when 'P' then
				format_type := K_printf_fmt_type_percent
			when 's' then
				format_type := K_printf_fmt_type_string
			when 'S' then
				format_type := K_printf_fmt_type_sequence
			when 'u' then
				format_type := K_printf_fmt_type_unsigned
			when 'x' then
				format_type := K_printf_fmt_type_hex
			when 'X' then
				format_type := K_printf_fmt_type_hex_cap
			else
				-- Not a type specifier, but might be a separator
				if pc = 'L' then
					-- List format can accept a single optional separator
					-- character using the form:  %%<sep>L
					private_list_separator := type_specifier
					type_specifier := pc
					token.extend (type_specifier)
					format_type := K_printf_fmt_type_list
				elseif pc = 'S' then
					-- Sequence format can accept a single optional separator
					-- character using the form:  %%<sep>S
					private_list_separator := type_specifier
					type_specifier := pc
					token.extend (type_specifier)
					format_type := K_printf_fmt_type_sequence
				elseif pc = 'M' then
					-- Matrix format can accept a single optional separator
					-- character using the form:  %%[#]<sep>M
					-- 'pos' should have landed on the separator (and have 
					-- already caught an recorded the decorator, if 
					-- present)
					--
					-- TODO add range support to matrix, list and table,
					-- as well as their complex counterparts
					-- Use field width to set index first and last
					private_matrix_separator := type_specifier
					type_specifier := pc
					token.extend (type_specifier)
					format_type := K_printf_fmt_type_matrix
				elseif pc = 'H' then
					-- Table format can accept a single optional separator
					-- character using the form:  %%[#]<sep>H
					-- 'pos' should have landed on the separator (and have 
					-- already caught an recorded the decorator, if present)
					private_list_separator := type_specifier
					type_specifier := pc
					token.extend (type_specifier)
					format_type := K_printf_fmt_type_table
				elseif pc = 'd' then
					-- Decimal format can accept a single optional separator 
					-- character using the form:  %%<sep>d
					private_thousands_separator := type_specifier
					type_specifier := original_string.item (pos+1)
					token.extend (type_specifier)
					format_type := K_printf_fmt_type_decimal
				else
					-- Bad type value, invalid format
--RFO					has_format_error := True
--RFO					add_unknown_type_error (original_string, pos, type_specifier)
				end
			end
		end

 --|========================================================================
feature {AEL_PF_FORMATTING_ROUTINES} -- Argument setting
 --|========================================================================

	set_argument (v: detachable ANY)
			-- Associate the given argument with this format parameter
		do
			argument := v
			argument_defined := True
			if attached {NATURAL_64_REF} v or
				attached {NATURAL_32_REF} v or
				attached {NATURAL_16_REF} v or
				attached {NATURAL_8_REF} v
			 then
				 -- Arg is unsigned, regardless of format; good to know
				 arg_is_unsigned := True
			end
		ensure
			was_set_attached: attached v implies argument = v
			was_set_void: (not attached v) implies argument = Void
			flag_set: argument_defined
		rescue
			-- This should never happen as the body has only 2 
			-- assignments, but it has been observerd (and reported), so 
			-- until the issue is identified and fixed, there is an 
			-- optional call to a client to post/log/print the message
			if v = Void then
				log_to_client ("In failed printf set argument, v is Void")
			else
				log_to_client ("In failed printf set argument, v="+v.out)
			end
			-- Don't retry
		end

 --|========================================================================
feature {AEL_PF_FORMATTING_ROUTINES} -- Argument formatting
 --|========================================================================

	complex_param: detachable AEL_PF_COMPLEX_FORMAT_PARAM
			-- The format parameter for a Complex expression, if any

	--|--------------------------------------------------------------

	arg_out: STRING
			-- Formatted representation of the associated argument
		local
			tstr: STRING
			ta: like argument
		do
			create Result.make (8)
			tstr := ""
			if argument_defined then
				if argument = Void then
					tstr := "Void"
					add_arg_type_error (original_string, original_position,
					expected_class_name, "Void")
				elseif is_string then
					tstr := string_arg_out
				elseif is_boolean then
					tstr := bool_arg_out
				elseif is_float then
					tstr := float_arg_out
				elseif is_character then
					tstr := char_arg_out
				elseif is_integer then
					tstr := int_arg_out
				elseif is_list then
					-- Check to see if, despite %L format specifier, arg is 
					-- actually a table
					if attached {TUPLE} argument as tpa and then
						attached {like Kta_table}tpa.item (1)
					 then
						tstr := table_arg_out
					else
						tstr := list_arg_out
					end
				elseif is_sequence then
					-- Check to see if, despite %L format specifier, arg is 
					-- actually a table
					if attached {TUPLE} argument as tpa and then
						attached {like Kta_table}tpa.item (1)
					 then
						tstr := table_arg_out
					else
						tstr := list_arg_out
					end
				elseif is_matrix then
					tstr := matrix_arg_out
				elseif is_table then
					tstr := table_arg_out
				elseif is_any then
					tstr := any_arg_out
				elseif is_agent then
					tstr := agent_arg_out
				elseif is_complex then
					if has_printf_error then
						tstr := "Internal error in complex format"
					else
						tstr := complex_arg_out
					end
				elseif is_percent then
					tstr := percent_arg_out
				else
					check 
						untyped: not is_typed
					end
					ta := argument
					if ta /= Void then
						tstr := ta.out
					else
						tstr := "Void"
					end
				end
			end
			if has_arg_type_error then
				add_arg_type_error (original_string, original_position,
				expected_class_name, argument_class_name)
			end
			--| After formatting by type and decoration,
			--| align/justify the string within the field
			Result.append (aligned (tstr))
		ensure
			exists:	Result /= Void
		end

 --|========================================================================
feature {AEL_PF_FORMATTING_ROUTINES} -- Value scanning
 --|========================================================================

	value_in (buf: STRING; spos: INTEGER; ec: CELL [INTEGER]): detachable ANY
			-- Value, if any, from 'buf' at position 'spos' 
			-- corresponding to Current
		require
			exists: buf /= Void
			valid_start: spos > 0 and spos <= buf.count
			end_cell_exists: ec /= Void
		do
			if is_string then
				Result := pfsr.string_at (buf, spos, ec, field_width)
			elseif is_boolean then
				Result := pfsr.boolean_at (buf, spos, ec, field_width, is_decorated)
			elseif is_float then
				Result := pfsr.double_at (
					buf, spos, ec, field_width, fractional_field_width, is_decorated)
			elseif is_character then
				Result := buf.item (spos)
				ec.put (spos + 1)
			elseif is_decimal then
				Result := pfsr.decimal_at (
					buf, spos, ec, field_width, is_decorated, thousands_separator)
			elseif is_hex then
				Result := pfsr.hexadecimal_at (
					buf, spos, ec, field_width, is_decorated)
			elseif is_octal then
				Result := pfsr.octal_at (buf, spos, ec, field_width, is_decorated)
			elseif is_binary then
				Result := pfsr.binary_at (buf, spos, ec, field_width, is_decorated)
			elseif is_unsigned then
				Result := pfsr.natural_at (
					buf, spos, ec, field_width, is_decorated, thousands_separator)
			elseif is_list then
				Result := pfsr.list_at (buf, spos, ec, field_width, list_separator)
			elseif is_agent then
				-- RFO TODO.  Make this less horrific
				check
					agent_scan_unsupported: False
				end
			elseif is_percent then
				-- RFO TODO.  This is simply float.  Need to be 
				-- implemented properly for percent, checking for 
				-- trailing percent symbol and dividing value by 100
				Result := pfsr.double_at (
					buf, spos, ec, field_width, fractional_field_width, is_decorated)
			else
				check 
					untyped: not is_typed
				end
			end
		end

 --|========================================================================
feature -- Context queries
 --|========================================================================

	original_length: INTEGER
			-- Length of original token
		do
			Result := token.count
		ensure
			non_negative: Result >= 0
		end

	--|--------------------------------------------------------------

	original_string: STRING
			-- Reference to the whole original format string given

	original_position: INTEGER
			-- Original starting position in the format string

	token: STRING
			-- Original format token, captured from input
	
	has_format_error: BOOLEAN
			-- Has a format error been encountered?

	has_arg_type_error: BOOLEAN
			-- Has an argument type mismatch been encountered?

	has_error: BOOLEAN
		do
			Result := not last_printf_errors.is_empty
		end

 --|========================================================================
feature {AEL_PF_SCAN_ROUTINES,AEL_PF_TYPE_ROUTINES} -- Private context queries
 --|========================================================================

	type_specifier: CHARACTER
			-- Character in format string denoting type

	argument: detachable ANY
			-- Argument associated with this format

	argument_defined: BOOLEAN
			-- Has the argument value been defined yet?

	arg_is_unsigned: BOOLEAN
			-- Is the actual argument value an unsigned type?

	--|--------------------------------------------------------------

	is_decorated: BOOLEAN
			-- Was a decoration specifier found?
		do
			Result := (token.count > 1) and then (token.item (2) = '#')
		end

	--|--------------------------------------------------------------

	is_aligned: BOOLEAN
			-- Does the format call for special aligment?
			-- By default, all formats are left-aligned
		do
			Result := is_right_aligned or is_center_aligned
		end

	--|--------------------------------------------------------------

	is_left_aligned: BOOLEAN
			-- Should formatted arg begin at left edge of field?

	is_right_aligned: BOOLEAN
			-- Should formatted arg end at right edge of field?

	is_center_aligned: BOOLEAN
			-- Should formatted arg be centered in field?

	--|--------------------------------------------------------------

	is_zero_padded (fw, aw: INTEGER): BOOLEAN
			-- Does the format result in leading zero padding?
		do
			Result := (is_right_aligned or is_center_aligned or not is_aligned)
				and (fw > aw) and (pad_character = '0')
		end

	--|--------------------------------------------------------------

	pad_character: CHARACTER
			-- Character used to align and justify in field
		do
--			if (is_float or (is_integer and not is_binary)) then
			if is_float or is_percent or is_integer then
				Result := private_pad_character
				if is_binary and Result = '0' then
					-- If zero-padded binary,
					-- negative values should actually be 1-padded
					if not (is_unsigned or arg_is_unsigned) then
						Result := '1'
					end
				end
			end
			if Result = '%U' then
				Result := ' '
			end
		end

	--|--------------------------------------------------------------

	list_separator: STRING
			-- STRING used to separate list elements
		do
			if private_list_separator /= '%U' then
				Result := private_list_separator.out
			else
				if is_decorated then
					Result := ""
				else
					Result := default_printf_list_delimiter
				end
			end
		end

	--|--------------------------------------------------------------

	matrix_separator: STRING
			-- STRING used to separate per-column items in a row-major 
			-- matrix, and per-row items in a column-major matrix
			-- 
		do
			if private_matrix_separator /= '%U' then
				Result := private_matrix_separator.out
			else
				if is_decorated then
					Result := " "
				else
					Result := default_printf_matrix_delimiter
				end
			end
		end

	--|--------------------------------------------------------------

	thousands_separator: STRING
			-- STRING used to separate thousands in decimal integers
		do
			if private_thousands_separator /= '%U' then
				Result := private_thousands_separator.out
			elseif is_decorated then
				Result := default_printf_thousands_delimiter
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	has_field_width: BOOLEAN
	has_fractional_field_width: BOOLEAN

	field_width: INTEGER
			-- Overall field width for non-floating point values
			-- For floating point values, the whole part field width

	fractional_field_width: INTEGER
			-- For floating point values, the width of the
			-- fractional part (right of decimal point)

	--|--------------------------------------------------------------

	decoration: STRING
			-- String to apply as context-specific decoration
		do
			Result := ""
			if is_decorated then
				if is_hex then
					Result := "0x"
				elseif is_octal then
					Result := "0"
				end
			end
		ensure
			exists: Result /= Void
		end

 --|========================================================================
feature {AEL_PF_TYPE_ROUTINES} -- Type queries
 --|========================================================================

	is_character: BOOLEAN
			-- Does format expect a character argument?
		do
			Result := is_char
		end

	--|--------------------------------------------------------------

	is_integer: BOOLEAN
			-- Does format expect an integer argument?
		do
			Result := is_decimal or is_hex or is_octal or is_binary or is_unsigned
		end

	--|--------------------------------------------------------------

	is_any: BOOLEAN
			-- Does format expect an argument of type?
		do
			Result := format_type = K_printf_fmt_type_any
		end

	is_boolean: BOOLEAN
			-- Does format expects a boolean argument?
		do
			Result := format_type = K_printf_fmt_type_boolean
		end

	is_binary: BOOLEAN
			-- Does format expect an integer argument?
			-- Shows in binary format
		do
			Result := format_type = K_printf_fmt_type_binary
		end

	is_char: BOOLEAN
			-- Does format expect a character argument?
		do
			Result := format_type = K_printf_fmt_type_character
		end

	is_decimal: BOOLEAN
			-- Does format expect an integer argument?
			-- Shows in decimal format
		do
			Result := format_type = K_printf_fmt_type_decimal
		end

	is_float: BOOLEAN
			-- Does format expect a floating point argument?
			-- (REAL OR DOUBLE)
		do
			Result := format_type = K_printf_fmt_type_float
		end

	is_octal: BOOLEAN
			-- Does format expect an integer argument?
			-- Shows in octal format
		do
			Result := format_type = K_printf_fmt_type_octal
		end

	is_string: BOOLEAN
			-- Does format expect a string argument?
		do
			Result := format_type = K_printf_fmt_type_string
		end

	is_unsigned: BOOLEAN
			-- Does format expect an integer argument?
			-- Shows in unsigned decimal format
		do
			Result := format_type = K_printf_fmt_type_unsigned
		end

	is_hex: BOOLEAN
			-- Does format expect an integer argument?
			-- Shows in hexadecimal format
		do
			Result := format_type = K_printf_fmt_type_hex or
				format_type = K_printf_fmt_type_hex_cap
		end

	is_list: BOOLEAN
			-- Does format expect a list or container argument?
			-- Shows in delimited format
		do
			Result := format_type = K_printf_fmt_type_list
		end

	is_matrix: BOOLEAN
			-- Does format expect a matrix argument?
			-- Shows in delimited format
		do
			Result := format_type = K_printf_fmt_type_matrix
		end

	is_sequence: BOOLEAN
			-- Does format expect a list or container argument?
			-- Shows in delimited format
		do
			Result := format_type = K_printf_fmt_type_sequence
		end

	is_table: BOOLEAN
			-- Does format expect a hash table argument?
			-- Shows in delimited format
		do
			Result := format_type = K_printf_fmt_type_table
		end

	is_agent: BOOLEAN
			-- Does format expect a TUPLE [ANY, FUNCTION] argument?
		do
			Result := format_type = K_printf_fmt_type_agent
		end

	is_complex: BOOLEAN
			-- Is format complex, expecting a container?
		do
			Result := format_type = K_printf_fmt_type_complex
		end

	is_percent: BOOLEAN
			-- Does format expect a float arg (to be interpreted as percent)?
		do
			Result := format_type = K_printf_fmt_type_percent
		end

--|========================================================================
feature {AEL_PF_TYPE_ROUTINES} -- Visible type queries
--|========================================================================

	is_typed: BOOLEAN
			-- Was a valid type specifier found?
		do
			Result := format_type > 0
		end

	--|--------------------------------------------------------------

	format_type: INTEGER
			-- Defined format type (if any, else 0)

	--|--------------------------------------------------------------

	minus_is_withheld: BOOLEAN
			-- Is the minus sign being withheld until
			-- after alignment for a negative decimal
			-- integer argument value?

 --|========================================================================
feature {NONE} -- Output Implementation
 --|========================================================================

	any_arg_out: STRING_8
			-- Formatted ANY argument
		do
			if attached argument as ta then
				Result := ta.out
			else
				Result := "Void"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	complex_arg_out: STRING_8
			-- Formatted expression argument (just a placeholder for now)
		require
			is_complex: attached complex_param
		do
			if attached argument as ta and
				attached complex_param as cxp and then
				attached cxp.container_param as cp
			 then
--TODO, implement the formats given
				inspect  cxp.container_type
				when K_printf_fmt_type_list, K_printf_fmt_type_sequence then
					Result := list_arg_out
					if attached {TUPLE} ta as tpa and then
						attached {like Kta_table}tpa.item (1)
					 then
						Result := table_arg_out
					else
						Result := list_arg_out
					end
				when K_printf_fmt_type_table then
						Result := table_arg_out
				when K_printf_fmt_type_matrix then
						Result := matrix_arg_out
				else
					Result := ta.out
				end
			else
				Result := "Void"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	string_arg_out: STRING_8
			-- Formatted string argument
		local
			ta: detachable ANY
		do
			if attached any_to_string_8 (argument) as tsa then
				Result := tsa.as_string_8
			else
				--| Argument doesn't match
				has_arg_type_error := True
				ta := argument
				if ta /= Void then
					Result := ta.out
				else
					Result := "Void"
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	bool_arg_out: STRING
			-- Formatted boolean argument
		local
			tba: detachable BOOLEAN_REF
			ta: detachable ANY
		do
			tba := any_to_boolean_ref (argument)
			if tba /= Void then
				if tba.item then
					Result := printf_bool_out_true
				else
					Result := printf_bool_out_false
				end
			else
				--| Argument doesn't match
				has_arg_type_error := True
				ta := argument
				if ta /= Void then
					Result := ta.out
				else
					Result := "Void"
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	float_arg_out: STRING
			-- Formatted floating point argument
		do
			Result := real_arg_out (False)
		end

	--|--------------------------------------------------------------

--RFO 	obs_real_arg_out (pf: BOOLEAN): STRING
--RFO -- replaced with simplified routine
--RFO 			-- Formatted floating point argument
--RFO 			-- If 'pf' format as a percentage by multiplying the value by 
--RFO 			-- 100 and appending a percent symbol after the number.
--RFO 			--
--RFO 			-- While it is tempting to use the FORMAT_DOUBLE base class, 
--RFO 			-- the restrictions imposed by that class, along with some 
--RFO 			-- odd results, make it an unsuitable option.  It was tried.
--RFO 		local
--RFO 			tda: detachable REAL_64_REF
--RFO 			whole_out, fract_out, ds_out: STRING
--RFO 			i, ffw: INTEGER
--RFO 			whole_part: INTEGER_64
--RFO  			parts: LIST [STRING]
--RFO 			tsa, rs: STRING
--RFO 			is_negative, needs_no_rp: BOOLEAN
--RFO 		do
--RFO 			-- Set default value
--RFO 			Result := "Void"
--RFO 			ffw := fractional_field_width
--RFO 			whole_out := ""
--RFO 			fract_out := ""
--RFO 			if attached argument as ta then
--RFO 				--
--RFO 				-- Check for type of actual arg (vs fmt)
--RFO 				--
--RFO 				tda := any_to_real_64_ref (ta)
--RFO 				if attached tda then
--RFO 					-- Is a Double (REAL_64)
--RFO 					is_negative := tda < tda.zero
--RFO 				else
--RFO 					-- Not a double.  Is it real?
--RFO 					if attached any_to_real_32_ref (ta) as tra then
--RFO 						-- Is a REAL_32
--RFO 						is_negative := tra < tra.zero
--RFO 						tda := tra.to_double
--RFO 					elseif attached any_to_integer_64_ref (ta) as tia then
--RFO 						-- Not real either; how about integer?
--RFO 						-- Conversion function handles all integer sizes
--RFO 						-- Yup, it's an integer of some kind
--RFO 						is_negative := tia < tia.zero
--RFO 						tda := tia.to_double
--RFO 						--parts := printf_fmt_funcs.double_parts (tda)
--RFO 					elseif attached any_to_natural_64_ref (ta) as tian then
--RFO 						-- Last chance; Is it a Natural?
--RFO 						-- Conversion function handles all natural sizes
--RFO 						tda := tian.to_real_64
--RFO 					else
--RFO 						has_arg_type_error := True
--RFO 					end
--RFO 				end
--RFO 			end
--RFO 			if attached tda and not has_arg_type_error then
--RFO 				-- No error so far, and 'tda' has been set for all 
--RFO 				-- numeric type args

--RFO 				if pf then
--RFO 					-- Percent format; multiply value by 100
--RFO 					tda := tda * 100.0
--RFO 				end
--RFO 				whole_part := tda.truncated_to_integer_64
--RFO 				whole_out := whole_part_out (whole_part.out)

--RFO 				-- Begin by looking at what the built-in formatter thinks
--RFO 				Result := tda.out
--RFO 				if printf_fmt_funcs.is_exponential_notation (Result) then
--RFO 					Result := printf_fmt_funcs.scientific_to_decimal (Result)
--RFO 					parts := Result.split (decimal_separator)
--RFO 				else
--RFO --					parts := Result.split ('.')
--RFO 					parts := Result.split (decimal_separator)
--RFO 				end
--RFO 				-- Check for special case of 0 first
--RFO 				if parts.count = 1 then
--RFO 					-- No radix point, so only whole part
--RFO 					--
--RFO 					-- What happened?  If the value is 0 (or 0.0),
--RFO 					-- then the built-in output will be a simple "0".
--RFO 					-- That's not likely what a user wants with printf,
--RFO 					-- UNLESS the overall field width is 1 or 0, AND the 
--RFO 					-- fractional field width is 0, then maybe it's 
--RFO 					-- OK, but we need to see what users really want, 
--RFO 					-- or maybe make the behavior configurable
--RFO 					-- For now, we'll output "0" when fract width is 0
--RFO 					-- and "0."<zeroes to fill> when fract width is >0
--RFO 					if ffw = 0 then
--RFO 						fract_out := ""
--RFO 						needs_no_rp := True
--RFO 					else
--RFO 						-- make a fractional field with enough '0's
--RFO 						create fract_out.make_filled ('0', ffw)
--RFO 					end
--RFO 				else
--RFO 					-- Has 2 parts, either side of radix point
--RFO 					-- Built-in function thinks it's a float of some 
--RFO 					-- kind; do the work
--RFO 					whole_out := whole_part_out (parts.first)
--RFO 					-- Check fractional part for relationship to
--RFO 					-- requested field width
--RFO 					fract_out := parts.last
--RFO 					if fract_out.count = ffw then
--RFO 						-- Done
--RFO 					elseif fract_out.count < ffw then
--RFO 						-- Pad right with zeroes, or go fp crazy
--RFO 						from i := fract_out.count
--RFO 						until i >= ffw
--RFO 						loop
--RFO 							fract_out.extend ('0')
--RFO 							i := i + 1
--RFO 						end
--RFO 					else
--RFO 						-- Fract part is wider than request,
--RFO 						-- Truncate, with rounding
--RFO 						--RFO TODO need rounding to include whole part
--RFO 						tsa := fract_out.twin
--RFO 						-- Current logic runs afoul with carry past 
--RFO 						-- fractional part, so need a total rework (or fix)
--RFO 						-- For now, just truncate what the core library 
--RFO 						-- gave us
--RFO 						--TODO
--RFO 						--rs := printf_fmt_funcs.round_to_position (tsa, ffw)
--RFO 						fract_out := tsa.substring (1, ffw)
--RFO 						tsa := rs
--RFO 					end
--RFO 				end -- else, parts.count = 2
--RFO 				if is_negative and
--RFO 					is_zero_padded (field_width, whole_out.count)
--RFO 				 then
--RFO 					 minus_is_withheld := True
--RFO 				end
--RFO 				if needs_no_rp then
--RFO 					ds_out := ""
--RFO 				else
--RFO 					ds_out := decimal_separator.out
--RFO 				end
--RFO 				Result := whole_out + ds_out + fract_out
--RFO 				if pf then
--RFO 					Result.extend ('%%')
--RFO 				end
--RFO 			end
--RFO 		ensure
--RFO 			exists: Result /= Void
--RFO 		end

	--|--------------------------------------------------------------

	real_arg_out (pf: BOOLEAN): STRING
			-- Formatted floating point argument
			-- If 'pf' format as a percentage by multiplying the value by 
			-- 100 and appending a percent symbol after the number.
			--
			-- While it is tempting to use the FORMAT_DOUBLE base class, 
			-- the restrictions imposed by that class, along with some 
			-- odd results, make it an unsuitable option.  It was tried.
		local
			tda: detachable REAL_64_REF
			tia: INTEGER_64_REF
			wp, fpi: INTEGER_64
			fp, f10: REAL_64
			fpl: LIST [STRING]
			whole_out, fract_out, ds_out, rp: STRING
			ffw, dc: INTEGER
 			parts: LIST [STRING]
			rs: STRING
			is_negative, needs_no_rp: BOOLEAN
		do
			-- Set default value
			Result := "Void"
			ffw := fractional_field_width
			whole_out := ""
			fract_out := ""
			if attached argument as ta then
				--
				-- Check for type of actual arg (vs fmt)
				--
				tda := any_to_real_64_ref (ta)
				if attached tda then
					-- Is a Double (REAL_64)
					is_negative := tda < tda.zero
				else
					-- Not a double.  Is it real?
					if attached any_to_real_32_ref (ta) as tra then
						-- Is a REAL_32
						is_negative := tra < tra.zero
						tda := tra.to_double
					elseif attached any_to_integer_64_ref (ta) as tiaa then
						-- Not real either; how about integer?
						-- Conversion function handles all integer sizes
						-- Yup, it's an integer of some kind
						tia := tiaa
						is_negative := tia < tia.zero
						tda := tia.to_double
						--parts := printf_fmt_funcs.double_parts (tda)
					elseif attached any_to_natural_64_ref (ta) as tian then
						-- Last chance; Is it a Natural?
						-- Conversion function handles all natural sizes
						tda := tian.to_real_64
					else
						has_arg_type_error := True
					end
				end
			end
			if attached tia and not has_arg_type_error then
				-- gen whole int part, then enough zeroes to fill fract 
				-- part spec
				whole_out := tia.out
				create fract_out.make_filled ('0', ffw)
			elseif attached tda and not has_arg_type_error then
				-- No error so far, and 'tda' has been set for all 
				-- numeric type args

				if pf then
					-- Percent format; multiply value by 100
					tda := tda * 100.0
				end
				--TODO break out whole and fractional parts BEFORE formatting
				if tda < 0.0 then
					wp := (0.0 - tda).truncated_to_integer_64
				else
					wp := tda.truncated_to_integer_64
				end
				-- shift fractional part by sufficient magnitude to 
				-- enable capturing fractional field width
				f10 := 10.0
				f10 := f10.power (ffw.to_double)
				fp := (tda - wp.to_double) * f10
				fp := fp.rounded_real_64
				fpi := fp.truncated_to_integer_64
				whole_out := wp.out
				fract_out := fpi.out
				if fract_out.count < ffw then
					-- zero pad fractional part at left
					dc := ffw - fract_out.count
					create rp.make_filled ('0', dc)
					fract_out := rp + fract_out
				end
				-- NOTE WELL: The significand of a double is 53 bits, 
				-- leaving 11 bits for the exponent
				-- An equivalent 53-bit integer would have about 16
				-- decimal digits.  As such, the string representation
				-- of a double must have no more than 17 digits total
				-- e.g. 1234567890.1234567
				-- The fractional part is zero-padded after the 17th digit
				dc := whole_out.count + fract_out.count
				dc := 17 - dc
				if dc < 0 then
					-- Too many digits overall, truncate fractional part 
					-- by replacing digits, from right, with zeroes
					fract_out := fract_out.head (ffw + dc)
					create rp.make_filled ('0', -dc)
					fract_out.append (rp)
				end
			end
--				fpl := printf_fmt_funcs.fp_out_to_parts (tda.out)
--				rs := fpl.first + decimal_separator.out + fpl.last
--				--rs := printf_fmt_funcs.rounded_to_precision (tda.out, ffw)
--				rs := printf_fmt_funcs.rounded_to_precision (rs, ffw)
--				parts := rs.split (decimal_separator)
--				whole_out := whole_part_out (parts.first)
--				fract_out := parts.last
			if not has_arg_type_error then
				if is_negative and
					is_zero_padded (field_width, whole_out.count)
				 then
					 minus_is_withheld := True
				end
				if needs_no_rp then
					ds_out := ""
				else
					ds_out := decimal_separator.out
				end
				Result := whole_out + ds_out + fract_out
				if pf then
					Result.extend ('%%')
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	whole_part_out (wp: STRING): STRING
			-- Formatted (filled, adjusted, etc.) whole part of a 
			-- floating point value, where 'wp' is the whole part
			-- derived from the built-in output format
		do
			-- Whole part padding is handled in the back-end routines
			-- All that needs handling here is thousands decoration
			if is_decorated and wp.count > 3 then
				-- Decoration of the whole part adds commas at the 
				-- thousands boundaries
				Result := thousands_separated (wp)
			else
				Result := wp
			end
		end

	--|--------------------------------------------------------------

	obs_fractional_part_out (fp: STRING): STRING
-- Unnecessary. Use 'real_arg_out' uses 'rounded_to_precision'
			-- Fractional part of a floating point value, where 
			-- fractional part string 'fp' have been derived from 
			-- default output format
			-- Because fractional field with is a component of an item 
			-- output, padding is performed here, rather than leaving it 
			-- to the back-end routines
			--
			--RFO TODO, move logic from real_arg_out to here
			-- This routine is presently not in use
		local
			fpw: INTEGER
			fps, rp, lzs: STRING
			nc: INTEGER
			v1: INTEGER_64
			td: REAL_64
			p10: INTEGER
		do
			fpw := fractional_field_width
			rp := ""
			if fp.count > fpw then
				-- Be very careful with leading zeroes.  The fractional 
				-- part has been separated from the whole, and so a 
				-- fractional part less than (originally) 0.1 will have 
				-- one or more leading zeroes.  Conversion of the now 
				-- integer-like value to real would ignore the leading 
				-- zeroes and change the value, dividing it by 'n' 
				-- factors of ten, where 'n' is the number of leading 0s.
				-- Work on the field width with the fractional part as a 
				-- string first, determining the number of leading zeroes,
				-- then add them after numeric rounding.
				--
				-- Truncate, with rounding
				-- e.g.  xx.59999 with 2 decimal pts should be xx.60
				-- Divide (real) by powers of ten to gen a new real value 
				-- whose whole part is the not-yet-rounded former 
				-- fractional part, then round that value to the 
				-- fractional part with the proper number of digits.
				nc := printf_fmt_funcs.num_leading_chars ('0', fp)
				if nc > 0 then
					create lzs.make_filled ('0', nc)
				else
					lzs := ""
				end
				td := fp.to_real_64
				from p10 := fp.count - fpw
				until p10 <= 0
				loop
					td := td / 10
					p10 := p10 - 1
				end
				v1 := td.rounded_real_64.truncated_to_integer_64
				fps := lzs + v1.out
			elseif fp.count < fpw then
				-- Right pad with padding character
				create rp.make_filled ('0', fpw - fp.count)
				create fps.make (fp.count + rp.count)
				fps.append (fp)
			else
				fps := fp
			end
			Result := fps + rp
		end

	--|--------------------------------------------------------------

	int_thousands_separated (v: INTEGER): STRING
			-- String representation of integer value 'v',
			-- in which a thousnds_separator is inserted
			-- at each thousands positions
		local
			vout: STRING
		do
			vout := v.out
			if vout.count <= 3 then
				Result := vout
			else
				Result := thousands_separated (vout)
			end
		end

	thousands_separated (v: STRING): STRING
			-- String representation of integer value, decorated with 
			-- commas at the thousands positions
		local
			i, lim: INTEGER
			ts: STRING
		do
			lim := v.count
			if lim <= 3 then
				Result := v
			else
				create ts.make (lim + (lim // 3))
				from i := lim
				until i <= 0
				loop
					ts.extend (v.item (i))
					i := i - 1
					if i > 0 then
						ts.extend (v.item (i))
						i := i - 1
						if i > 0 then
							ts.extend (v.item (i))
							i := i - 1
						end
					end
					if i > 0 then
						ts.append (thousands_separator)
					end
				end
				ts.mirror
				Result := ts
			end
		end

	--|--------------------------------------------------------------

	char_arg_out: STRING
			-- Formatted character argument
		local
			tc8: detachable CHARACTER_8_REF
			tc32: detachable CHARACTER_32_REF
		do
			Result := ""
			tc8 := any_to_character_8_ref (argument)
			tc32 := any_to_character_32_ref (argument)
			if tc8 /= Void then
				Result := tc8.out
			elseif tc32 /= Void then
				--RFO need MUCH better Ucode support than this!!!!!!!!!
				if tc32.is_character_8 then
					Result := tc32.to_character_8.out
				else
					Result := tc32.out
				end
			else
				--| Argument doesn't match
				has_arg_type_error := True
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	int_arg_out: STRING
			-- Formatted integer argument
		local
			tia8: detachable INTEGER_8_REF
			tia16: detachable INTEGER_16_REF
			tia32: detachable INTEGER_32_REF
			tia64: detachable INTEGER_64_REF
			tian8: detachable NATURAL_8_REF
			tian16: detachable NATURAL_16_REF
			tian32: detachable NATURAL_32_REF
			tian64: detachable NATURAL_64_REF
			tip: detachable POINTER_REF
			ti: INTEGER_64
			tn: NATURAL_64
			nb: INTEGER
			ts: STRING
		do
			create Result.make (8)
			tia8 := any_to_integer_8_ref (argument)
			tia16 := any_to_integer_16_ref (argument)
			tia32 := any_to_integer_32_ref (argument)
			tia64 := any_to_integer_64_ref (argument)
			tip := any_to_pointer_ref (argument)
			if tia8 /= Void then
				nb := 1
				if is_unsigned or arg_is_unsigned then
					create tian32
					tian32.set_item (tia8.as_natural_32)
					create tian64
					tian64.set_item (tian32.as_natural_64)
				else
					create tia64
					tia64.set_item (tia8.as_integer_64)
				end
			elseif tia16 /= Void then
				nb := 2
				if is_unsigned or arg_is_unsigned then
					create tian32
					tian32.set_item (tia16.as_natural_32)
					create tian64
					tian64.set_item (tian32.as_natural_64)
				else
					create tia64
					tia64.set_item (tia16.as_integer_64)
				end
			elseif tia32 /= Void then
				nb := 4
				if is_unsigned or arg_is_unsigned then
					create tian32
					tian32.set_item (tia32.as_natural_32)
					create tian64
					tian64.set_item (tian32.as_natural_64)
				else
					create tia64
					tia64.set_item (tia32.as_integer_64)
				end
			elseif tia64 /= Void then
				nb := 8
				if is_unsigned or arg_is_unsigned then
					create tian64
					tian64.set_item (tia64.as_natural_64)
				end
			elseif tip /= Void then
				nb := 4
				if is_unsigned or arg_is_unsigned then
					create tian32
					tian32.set_item (tip.to_integer_32.as_natural_32)
					create tian64
					tian64.set_item (tian32.as_natural_64)
				else
					create tia64
					tia64.set_item (tip.to_integer_32.as_integer_64)
				end
			else
				tian8 := any_to_natural_8_ref (argument)
				tian16 := any_to_natural_16_ref (argument)
				tian32 := any_to_natural_32_ref (argument)
				tian64 := any_to_natural_64_ref (argument)
				if tian8 /= Void then
					nb := 1
					create tian64
					tian64.set_item (tian8.as_natural_64)
				elseif tian16 /= Void then
					nb := 2
					create tian64
					tian64.set_item (tian16.as_natural_64)
				elseif tian32 /= Void then
					nb := 4
					create tian64
					tian64.set_item (tian32.as_natural_64)
				elseif tian64 /= Void then
					nb := 8
				else
					has_arg_type_error := True
				end
				if tian64 = Void then
					has_arg_type_error := True
				else
					if format_type = K_printf_fmt_type_decimal then
						if is_decorated  then
							-- Arg is unsigned but request wants int 
							-- decoration
							if tian64 <= {INTEGER_32}.Max_value.to_natural_64 then
								-- OK to treat as signed value
								create tia64
								tia64.set_item (tian64.as_integer_64)
							else
								format_type := K_printf_fmt_type_unsigned
							end
						else
							format_type := K_printf_fmt_type_unsigned
						end
					end
				end
			end
			if not has_arg_type_error then
				check
					unsigned_natural: is_unsigned implies tian64 /= Void
				end
				if tia64 /= Void then
					ti := tia64
					tn := ti.as_natural_64
				end
				if tian64 /= Void then
					ti := tian64.as_integer_64
					tn := tian64
				end
				if is_hex then
					--Result := printf_fmt_funcs.integer_to_hex (ti, nb)
					Result := printf_fmt_funcs.natural_to_hex (
						tn, nb, format_type = K_printf_fmt_type_hex_cap)
				elseif is_octal then
					Result := printf_fmt_funcs.integer_to_octal (ti, nb)
				elseif is_binary then
					Result := printf_fmt_funcs.integer_to_binary (ti)
					if is_decorated then
						Result.extend ('b')
					end
					if field_width /= 0 and field_width < Result.count then
						prune_leading_zeroes (Result, field_width)
					end
				elseif is_unsigned then
					Result := printf_fmt_funcs.natural_out (tn, nb)
				else -- Decimal
					ts := decimal_out (ti)
					if ((ti < ti.zero) and
						is_zero_padded (field_width, ts.count)) then
							minus_is_withheld := True
							Result := (-ti).out
					else
						Result := ts
					end
				end
			end
		ensure
			exists:	Result /= Void
		end

	--|--------------------------------------------------------------

	decimal_out (v: INTEGER_64): STRING
			-- String form of decimal value 'v', possible decorated
		local
			ts: STRING
			i, j, lim: INTEGER
		do
			ts := v.out
			lim := ts.count
			if (lim <= 3) or
				((not is_decorated) and thousands_separator.is_empty)
			 then
				 Result := ts
			else
				create Result.make (lim + lim // 3)
				from i := lim
				until i <= 0
				loop
					from j := 1
					until j > 3 or i <= 0
					loop
						Result.extend (ts.item (i))
						i := i - 1
						j := j + 1
					end
					if i > 0 then
						Result.append (thousands_separator)
					end
				end
				Result.mirror
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	list_arg_out: STRING
			-- Formatted container argument (%L or %S)
		local
			tca: detachable CONTAINER [detachable ANY]
			tla: like Kta_linear
			ta: detachable ANY
			sep, ctok: STRING
			--ifmt, cplx_args: STRING
			tt: detachable TUPLE
			tfunc: detachable FUNCTION [STRING]
			has_range, in_range: BOOLEAN
			sp, ep, lp: INTEGER
		do
			-- Arg must be a container unless the agent flag was provided, in 
			-- which case the arg must be a TUPLE, the first item of 
			-- which is a list
			Result := ""

			if attached complex_param as cxp and then
				attached cxp.container_param as cp
			 then
				sep := cp.list_separator
				if is_sequence then
					sep := sep + " "
				end
				--ifmt := cxp.item_format
				--cplx_args := cxp.args
				ctok := cp.token
			else
				sep := list_separator
				if is_sequence then
					sep := sep + " "
				end
				--ifmt := ""
				--cplx_args := ""
				ctok := token
				sp := field_width
				ep := fractional_field_width
				has_range := sp /= 0 and ep >= sp
			end
			-- If format was specified as %#L, then the '#' character is 
			-- not treated as a decoration trigger (not applicable to 
			-- lists), but as request newline-separated list items.  It 
			-- is not possible to specify a newline as a separator using 
			-- the normal separator mechanism, as the newline is 
			-- interpreted as being part of the string.
			-- A sequence of "%%NL" is interpeted as an escaped quote, 
			-- followed by "NL"
			if ctok.starts_with ("%%#") and not is_sequence then
				sep := sep + "%N"
			end
			tt := any_to_tuple (argument)
			if tt /= Void then
				--| Arg must be a TUPLE [LIST, FUNCTION]
				if tt.count /= 2 then
					add_arg_type_error (
						original_string, original_position,
						expected_class_name, argument_class_name)
				else
					--tca := any_to_container (tt.reference_item (1))
					if attached {CONTAINER[detachable ANY]} tt.reference_item (1)
					as ta1 then
						tca := ta1
					end
					if 
						attached {FUNCTION [STRING]}
						tt.reference_item (2) as ltf
					 then
						 tfunc := ltf
					end
				end
			else
				--tca := any_to_container (argument)
				if attached {CONTAINER[detachable ANY]} argument as ta2 then
					tca := ta2
				end
			end
			if tca = Void then
				--| Argument doesn't match
				has_arg_type_error := True
				ta := argument
				if ta /= Void then
					Result := ta.out
				else
					Result := "Void"
				end
			else
				tla := tca.linear_representation
				from tla.start
				until tla.exhausted
				loop
					lp := tla.index
					in_range := (not has_range) or (lp >= sp and lp <= ep)
					if in_range then
						-- if not ranged, or within range, include item
						-- else, skip
						ta := tla.item
						if tfunc /= Void then
							Result.append (tfunc.item ([ta, tla.index]))
						elseif attached ta as ata then
							if attached complex_param as cxp then
								Result.append (complex_list_item_out (tla, ata, lp))
							else
								Result.append (ata.out)
							end
						else
							Result.append ("Void")
						end
					end
					tla.forth
					if tfunc = Void then
						if (not has_range) and (not tla.exhausted) then
							Result.append (sep)
						else
							lp := tla.index
							if has_range and lp > sp and lp <= ep then
								Result.append (sep)
							end
						end
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	table_arg_out: STRING
			-- Formatted hash table argument (%L)
		local
			tca: like Kta_table
			ta: detachable ANY
			sep, ifmt, cplx_args, ctok: STRING
			tt: detachable TUPLE
			tfunc: detachable FUNCTION [STRING]
			htc: AEL_PF_TABLE_CLIENT
			tk: HASHABLE
			tp: INTEGER
		do
			-- Arg must be a container unless the agent flag was provided, in 
			-- which case the arg must be a TUPLE, the first item of 
			-- which is a hash table
			Result := ""

			if attached complex_param as cxp and then
				attached cxp.container_param as cp
			 then
				sep := cp.list_separator
				ifmt := cxp.item_format
				cplx_args := cxp.args
				ctok := cp.token
			else
				sep := list_separator
				ifmt := ""
				cplx_args := ""
				ctok := token
			end
			-- If format was specified as %#H, then the '#' character is 
			-- not treated as a decoration trigger, but as requesting
			-- newline-separated items.
			if ctok.starts_with ("%%#") then
				sep := sep + "%N"
			end
			tt := any_to_tuple (argument)
			if tt /= Void then
				--| Arg must be a TUPLE [LIST, FUNCTION]
				if tt.count /= 2 then
					add_arg_type_error (
						original_string, original_position,
						expected_class_name, argument_class_name)
				else
					if attached {like Kta_table}tt.reference_item (1) as ta1 then
						tca := ta1
					end
					if attached {FUNCTION [STRING]} tt.reference_item (2) as ltf
					 then
						 tfunc := ltf
					end
				end
			else
				--tca := any_to_container (argument)
				if attached {like Kta_table} argument as ta2 then
					tca := ta2
				end
			end
			if tca = Void then
				--| Argument doesn't match
				has_arg_type_error := True
				ta := argument
				if ta /= Void then
					Result := ta.out
				else
					Result := "Void"
				end
			else
				-- 'htc' is needed to be able to tell if hash table is at 
				-- the last item, to signal agent.
				create htc.make_with_table (tca)
				from tca.start
				until tca.after
				loop
					ta := tca.item_for_iteration
					tk := tca.key_for_iteration
					tp := htc.table_position
					if tfunc /= Void then
						Result.append (tfunc.item ([ta, tp]))
					elseif attached ta as ata then
						if attached complex_param as cxp then
							Result.append (complex_table_item_out (tca, tk, tp))
						else
							Result.append (ata.out)
						end
					else
						Result.append ("Void")
					end
					tca.forth
					if tfunc = Void then
						if not tca.after then
							Result.append (sep)
						end
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	matrix_arg_out: STRING
			-- Formatted matrix (ARRAY2) argument (%M)
		local
			tca: like Kta_matrix
			ta: detachable ANY
			sep, ifmt, cplx_args, ctok: STRING
			tt: detachable TUPLE
			tfunc: detachable FUNCTION [STRING]
			r, c, rlim, clim: INTEGER
			is_col_major: BOOLEAN
			has_range, in_range: BOOLEAN
			sp, ep: INTEGER
		do
			-- Arg must be an ARRAY2 unless the agent flag was provided, in 
			-- which case the arg must be a TUPLE, the first item of 
			-- which is an ARRAY2
			Result := ""
			-- The '#' character is treated as a decoration trigger,
			-- adding the row and column as "("<row>","<col>")" after 
			-- each item value.  Decoration does NOT apply to agent form.
			--
			-- If row-major (default), then output is row-at-a-time and 
			-- a newline is inserted between rows, and the list separator
			-- is inserted between the columns.
			-- If column-major, then output is column-at-a-time and a
			-- newline is inserted between columns, with the list separator
			-- inserted between rows
			if attached complex_param as cxp and then
				attached cxp.container_param as cp
			 then
				sep := cp.matrix_separator
				is_col_major := cp.is_left_aligned
				ifmt := cxp.item_format
				cplx_args := cxp.args
				ctok := cp.token
			else
				sep := matrix_separator
				is_col_major := is_left_aligned
				ifmt := ""
				cplx_args := ""
				ctok := token
				sp := field_width
				ep := fractional_field_width
				has_range := sp /= 0 and ep >= sp
			end
			-- If format was specified as %#M, then the '#' character is 
			-- not treated as a decoration trigger, but as requesting
			-- newline-separated items.
			if ctok.starts_with ("%%#") then
				sep := sep + "%N"
			end
			tt := any_to_tuple (argument)
			if tt /= Void then
				--| Arg must be a TUPLE [ARRAY2, FUNCTION]
				if tt.count /= 2 then
					add_arg_type_error (
						original_string, original_position,
						expected_class_name, argument_class_name)
				else
					if 
						attached {like Kta_matrix}
						tt.reference_item (1) as ta1
					 then
						tca := ta1
					end
					if attached {FUNCTION [STRING]} tt.reference_item (2) as ltf
					 then
						 tfunc := ltf
					end
				end
			elseif attached {like Kta_matrix} argument as ta2 then
				tca := ta2
			end
			if tca = Void then
				--| Argument doesn't match
				has_arg_type_error := True
				ta := argument
				if ta /= Void then
					Result := ta.out
				else
					Result := "Void"
				end
			else
				if not is_col_major then
					-- Row-major
					rlim := tca.height
					clim := tca.width
					from r := 1
					until r > rlim
					loop
						in_range := (not has_range) or (r >= sp and r <= ep)
						from c := 1
						until c > clim
						loop
							if in_range then
								ta := tca.item (r, c)
								if tfunc /= Void then
									Result.append (tfunc.item ([ta, r, c]))
								elseif attached ta as ata then
									if attached complex_param as cxp then
										Result.append (complex_matrix_item_out (tca, r, c))
									else
										Result.append (ata.out)
									end
								else
									Result.append ("Void")
								end
							end
							c := c + 1
							if tfunc = Void then
								--TODO, what if no more rows????
								if in_range and c <= clim then
									Result.append (sep)
								end
							end
						end -- from c := 1
						r := r + 1
						if tfunc = Void then
							if (not has_range) and r <= rlim then
								Result.append ("%N")
							else
								if has_range and r > sp and r <= ep then
									Result.append ("%N")
								end
							end
						end
					end -- from r := 1
				else
					-- Column-major
					rlim := tca.height
					clim := tca.width
					from c := 1
					until c > clim
					loop
						in_range := (not has_range) or (c >= sp and c <= ep)
						from r := 1
						until r > rlim
						loop
							if in_range then
								ta := tca.item (r, c)
								if tfunc /= Void then
									Result.append (tfunc.item ([ta, r, c]))
								elseif attached ta as ata then
									if attached complex_param as cxp then
										Result.append (complex_matrix_item_out (tca, r, c))
									else
										Result.append (ata.out)
									end
								else
									Result.append ("Void")
								end
							end
							r := r + 1
							if tfunc = Void then
								if in_range and r <= rlim then
									Result.append (sep)
								end
							end
						end
						c := c + 1
						if tfunc = Void and c <= clim then
							if (not has_range) and c <= clim then
								Result.append ("%N")
							else
								if has_range and c > sp and c <= ep then
									Result.append ("%N")
								end
							end
						end
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	complex_list_item_out (vl: like Kta_linear; ti: ANY; p: INTEGER): STRING
			-- Formatted item, in list 'vl', at index 'p'
			-- according to values from 'complex_param'
		require
			is_complex: attached complex_param
		local
			iargs: STRING
			args: LINKED_LIST [detachable ANY]
			i, lim: INTEGER
		do
			create args.make
			if not attached complex_param as cxp then
				Result := "<< Missing Complex Param >>"
			else
				iargs := cxp.args
				lim := iargs.count
				from i := 1
				until i > lim
				loop
					inspect iargs.item (i)
					when 'V' then
						args.extend (ti)
					when 'I' then
						args.extend (p)
					else
						args.extend ("<< Invalid Item Param >>")
					end
					i := i + 1
				end
				Result := apf.aprintf (cxp.item_format, args)
			end
		end

	--|--------------------------------------------------------------

	complex_matrix_item_out (mx: like Kta_matrix; r, c: INTEGER): STRING
			-- Formatted item, in matrix 'mx', at row 'r', col 'c',
			-- according to values from 'complex_param'
		require
			is_complex: attached complex_param
		local
			iargs: STRING
			args: LINKED_LIST [detachable ANY]
			i, lim: INTEGER
		do
			create args.make
			if not attached complex_param as cxp then
				Result := "<< Missing Complex Param >>"
			else
				iargs := cxp.args
				lim := iargs.count
				from i := 1
				until i > lim
				loop
					inspect iargs.item (i)
					when 'V' then
						args.extend (mx.item (r, c))
					when 'I', 'R' then
						args.extend (r)
					when 'C' then
						args.extend (c)
					else
						args.extend ("<< Invalid Item Param >>")
					end
					i := i + 1
				end
				Result := apf.aprintf (cxp.item_format, args)
			end
		end

	--|--------------------------------------------------------------

	complex_table_item_out (ht: like Kta_table; k: HASHABLE; p: INTEGER): STRING
			-- Formatted item, in table 'ht', by key 'k', according to 
			-- values from 'complex_param'
			-- 'p' is iteration position
		require
			is_complex: attached complex_param
		local
			iargs: STRING
			args: LINKED_LIST [detachable ANY]
			i, lim: INTEGER
		do
			create args.make
			if not attached complex_param as cxp then
				Result := "<< Missing Complex Param >>"
			else
				iargs := cxp.args
				lim := iargs.count
				from i := 1
				until i > lim
				loop
					inspect iargs.item (i)
					when 'V' then
						args.extend (ht.item (k))
					when 'I' then
						args.extend (p)
					when 'K' then
						-- The more exotic HASHABLEs may not be suitable 
						-- candidates for args, at least I don't think so
						-- Type-matching will happen later, I suppose, so 
						-- maybe this restriction is unnecessary, or 
						-- premature
						--if attached {ROUTINE} k then
							-- No good
						--elseif attached {TUPLE} k then
							-- No good
						--else
							args.extend (k)
						--end
					else
						args.extend ("<< Invalid Item Param >>")
					end
					i := i + 1
				end
				Result := apf.aprintf (cxp.item_format, args)
			end
		end

	--|--------------------------------------------------------------

	agent_arg_out: STRING
			-- Agent-formatted argument
		do
			Result := ""
			if
				attached
				{TUPLE [ANY, FUNCTION [detachable ANY, STRING]]}
				argument as targ
			 then
				 -- Second argument in TUPLE should be a function that 
				 -- returns a string
				 if
					 attached {FUNCTION [detachable ANY, STRING]}
					 targ.item (2) as tfunc
				  then
					  -- Call function with first item in TUPLE as 
					  -- argument
					  if attached {ANY} targ.item (1) as tai then
						  -- The attachment test above accomodates a 
						  -- peculiarity with TUPLEs in SCOOP mode, and has 
						  -- no effect on non-SCOOP code
						  Result := tfunc.item ([tai])
					  else
						  Result := "Void"
					  end
				 else
					 Result := "Void"
				 end
			 else
				 Result := "Void"
			 end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	percent_arg_out: STRING
			-- Floating point argument, formatted as a percentage
			-- Value provided is multiplied by 100 and a percent sign appended
		do
			Result := real_arg_out (True)
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- Output Support
--|========================================================================

	aligned (tstr: STRING): STRING
			-- Copy of given string aligned into field width
			-- per current parameters
		require
			str_exists: tstr /= Void
		local
			pad_len, minus_pad_len: INTEGER
			pre_str, post_str: STRING
		do
			create Result.make (field_width.max (tstr.count))
			pre_str := ""
			post_str := ""
			if tstr.count < field_width then
				if minus_is_withheld then
					minus_pad_len := 1
				end
				pad_len := (field_width - tstr.count 
				- minus_pad_len - decoration.count).max (0)
				if is_center_aligned then
					create pre_str.make_filled (pad_character, pad_len // 2)
					create post_str.make_filled (' ', pad_len - pre_str.count)
				else
					if is_left_aligned then
						create post_str.make_filled (' ', pad_len)
					else
						create pre_str.make_filled (pad_character, pad_len)
					end
				end
			end
			if minus_is_withheld then
				Result.extend ('-')
			end
			if is_zero_padded (field_width, tstr.count) then
				Result.append (decoration)
			end
			Result.append (pre_str)
			if not is_zero_padded (field_width, tstr.count) then
				Result.append (decoration)
			end
			Result.append (tstr)
			Result.append (post_str)
		ensure
			exists: Result /= Void
			big_enough: Result.count >= field_width
		end

	--|--------------------------------------------------------------

	prune_leading_zeroes (ts: STRING; fw: INTEGER)
			-- Prune leading zeroes in 'ts', to no less than length fw
		local
			i, lim, nz: INTEGER
		do
			lim := ts.count - fw
			from i := 1
			until i > lim
			loop
				if ts.item (i) = '0' then
					nz := nz + 1
					i := i + 1
				else
					i := lim + 1
				end
			end
			ts.keep_tail (ts.count - nz)
		end

	--|--------------------------------------------------------------

	private_pad_character: CHARACTER
			-- Padding character to use instead of default

	private_list_separator: CHARACTER
			-- Separator character to use instead of default

	private_matrix_separator: CHARACTER
			-- Separator character to use instead of default

	private_thousands_separator: CHARACTER
			-- Thousands separator character to use instead of default

	pfsr: AEL_PF_SCAN_ROUTINES
		once
			create Result
		end

	apf: AEL_PRINTF
		once
			create Result
		end

 --|========================================================================
feature {AEL_PF_FORMATTING_ROUTINES} -- Type representation
 --|========================================================================

	argument_class_name: STRING
			-- The name of the class to which argument belongs
		local
			ta: detachable like argument
		do
			if argument_defined then
				ta := argument
				if ta /= Void then
					Result := (create {INTERNAL}).class_name (ta)
				else
					Result := "** Argument is Void **"
				end
			else
				Result := "** Not Yet Defined **"
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	expected_class_name: STRING
			-- The name of the class expected by the type parameter
		local
			tc: CHARACTER
			ts: STRING
			ti: INTEGER
		do
			if is_character then
--				Result := "CHARACTER"
				Result := (create {INTERNAL}).class_name (tc)
			elseif is_boolean then
				Result := "BOOLEAN"
			elseif is_integer then
--				Result := "INTEGER"
				Result := (create {INTERNAL}).class_name (ti)
			elseif is_float or is_percent then
				Result := "DOUBLE/REAL"
			elseif is_string then
--				Result := "STRING"
				ts := ""
				Result := (create {INTERNAL}).class_name (ts)
			else
				Result := "** Undefined **"
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	index_of_closing_paren (buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing paren MATCHING the 
			-- opening paren at 'sp'
			-- Zero if not found
		require
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = '('
		do
			Result := index_of_closing ('(', buf, sp)
		end

	index_of_closing_bracket (buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing bracket MATCHING the 
			-- opening bracket at 'sp'
			-- Zero if not found
		require
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = '['
		do
			Result := index_of_closing ('[', buf, sp)
		end

	index_of_closing (oc: CHARACTER; buf: STRING; sp: INTEGER): INTEGER
			-- 1-based index, in 'buf', of closing character MATCHING the 
			-- opening character 'oc' at position 'sp'
			-- Zero if not found
			--
			-- Note Well: DOES NOT allow for quoted or escaped characters!!
		require
			valid_open: oc = '(' or oc = '{' or oc = '[' or oc = '<'
			valid_start: sp > 0 and sp < buf.count
			begins_with_open: buf.item (sp) = oc
		local
			i, lim, pc: INTEGER
			cc, c: CHARACTER
		do
			inspect oc
			when '(' then
				cc := ')'
			when '{' then
				cc := '}'
			when '[' then
				cc := ']'
			when '<' then
				cc := '>'
			else
				-- Caught by precondition
			end
			lim := buf.count
			from i := sp
			until Result /= 0 or i > lim
			loop
				c := buf.item (i)
				if c = oc then
					pc := pc + 1
				elseif c = cc then
					pc := pc - 1
					if pc = 0 then
						Result := i
					end
				else
				end
				i := i + 1
			end
		end

--|========================================================================
feature {NONE} -- Type anchors
--|========================================================================

	Kta_linear: LINEAR [detachable ANY]
			-- Type anchor ONLY
			-- DO NOT CALL - EVER
		require
			never_call_ever: False
		do
			check False then end
		ensure
			never_call: False
		end

	Kta_table: HASH_TABLE [detachable ANY, HASHABLE]
			-- Type anchor ONLY
			-- DO NOT CALL - EVER
		require
			never_call_ever: False
		do
			check False then end
		ensure
			never_call: False
		end

	Kta_matrix: ARRAY2 [detachable ANY]
			-- Type anchor ONLY
			-- DO NOT CALL - EVER
		require
			never_call_ever: False
		do
			check False then end
		ensure
			never_call: False
		end

	--|--------------------------------------------------------------
invariant
	token_exists: token /= Void
	format_string_exists: original_string /= Void
	position_is_valid: original_position > 0 and
	original_position <= original_string.count

end -- class AEL_PF_FORMAT_PARAM

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 015 02-Apr-2019
--|     Rework real_arg_out, to used new 'rounded_to_precision' function
--|     Compiled and tested with Eiffel 18.1
--| 014 28-Aug-2018
--|     Updated comments and descriptions.
--|     Added support for new formats: Matrix, (Hash) Table, ANY, and 
--|     Complex.  Added more error checks and more errors inserted 
--|     into output.  Added range support for container formats.
--|     Added support for alternative Boolean representations.
--|     Compiled and tested with Eiffel 18.1
--| 013 10-Aug-2018
--|     Added assignment test to accommodate TUPLE/SCOOP behavior
--| 012 01-Jul-2018
--|     Added thousands separators for decorated floats
--| 011 17-Mar-2018
--|     Added support for newline-separated lists using "%#L"
--|     Compiled with 18.01, void-safe without issue
--| 010 19-Jul-2013
--|     Tweaked type-matching for strings to support immutables
--| 009 02-Mar-2013
--|     Fixed formatting issue with decorated naturals
--| 008 18-May-2011
--|     Fixed ham-fisted bug with hex formats of NATURALs in int_arg_out.
--| 007 03-Apr-2011
--|     Added support for non-default separators when grouping 
--|     decimals using %<c>d notation.
--|     Enhanced binary support to fix field width issue.
--|     Tweaked extract_token to permit '.' as a delimiter for List 
--|     and Decimal formats.
--|     Broke out decimal_out to enable custom thousands delimeter 
--|     when not decorated.
--| 006 17-Feb-2010
--|     Compiled and tested using Eiffel 6.5 w/ standard syntx
--|     Updated extract_token and extract_field_width to support float
--|     formats missing overall width values (e.g. %%.2f)
--| 005 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_PRINTF and need not be addressed directly
--|----------------------------------------------------------------------
