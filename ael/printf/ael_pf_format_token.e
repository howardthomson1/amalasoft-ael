note
	description: "{
A token within a format string
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_FORMAT_TOKEN

create
	make, make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (p: INTEGER)
			-- Create Current with position 'p'
		do
			create string_value.make (16)
			position := p
			default_create
		end

	make_from_string (v: STRING; p: INTEGER; tf: BOOLEAN)
			-- Create Current from string 'v' at position 'p'
		do
			string_value := v
			position := p
			default_create
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	string_value: STRING
			-- Whole original token string

	position: INTEGER
			-- Position in stream where token starts

	format: detachable AEL_PF_FORMAT_PARAM
			-- Format derived from token, if any

	is_format: BOOLEAN
			-- Is the token a format specifier?
		do
			Result := format /= Void
		end

	is_empty: BOOLEAN
			-- Is the token string empty?
		do
			Result := string_value.is_empty
		end

	has_format_error: BOOLEAN
			-- Does Current have a format error?
		local
			tf: like format
		do
			tf := format
			Result := tf /= Void and then tf.has_format_error
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_format (v: like format)
			-- Set the format of Current to 'v'
		do
			format := v
		end

	set_string_value (v: STRING)
			-- Set Current's string value to 'v'
		do
			string_value := v
		end

	set_position (v: INTEGER)
			-- Set position to 'v'
		do
			position := v
		end

	extend (v: CHARACTER)
			-- Add 'v' to Current's string value
		do
			string_value.extend (v)
		end

end -- class AEL_PF_FORMAT_TOKEN

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 006 28-Aug-2018
--|     Updated comments and description
--|     Compiled and tested w/ Eiffel 18.7
--| 005 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_PRINTF and need not be addressed directly
--|----------------------------------------------------------------------
