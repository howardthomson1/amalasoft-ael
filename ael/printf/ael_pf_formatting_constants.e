note
	description: "{
Constants used by the other members of the printf cluster.
Also offers is_valid_first_fmt_char() to its descendents
Holds the shared error list (last_printf_errors) and provides
a shared instanace of AEL_PF_FORMATTING_ROUTINES for descendents.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_FORMATTING_CONSTANTS

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	Printf_hex_digits: STRING = "0123456789abcdef"
	Printf_hex_cap_digits: STRING = "0123456789ABCDEF"
	Printf_octal_digits: STRING = "01234567"

	K_printf_dflt_fp_fractional_width: INTEGER = 6
	K_printf_dflt_fp_whole_width: INTEGER = 1

	Ks_printf_missing_arg_stub: STRING
		once

			Result := "Void"
		end

	Ks_printf_mismatch_type_stub: STRING
		once
			Result := "** Arg Type Mismatch **"
		end

	Ks_printf_type_specifiers: STRING = "dsfxuocbLBAPMaHC"
			-- String containing all legal format characters,
			-- in order corresponding to integer type constants

 --|========================================================================
feature {NONE} -- Format type codes
 --|========================================================================

	-- N.B. these correspond to char positions in Ks_printf_type_specifiers
	-- Keep them in synch

	K_printf_fmt_type_decimal: INTEGER = 1 -- 'd'
	K_printf_fmt_type_string: INTEGER = 2 -- 's'
	K_printf_fmt_type_float: INTEGER = 3 -- 'f'
	K_printf_fmt_type_hex: INTEGER = 4 -- 'x'
	K_printf_fmt_type_unsigned: INTEGER = 5 -- 'u'
	K_printf_fmt_type_octal: INTEGER = 6 -- 'o'
	K_printf_fmt_type_character: INTEGER = 7 -- 'c'
	K_printf_fmt_type_binary: INTEGER = 8 -- 'b'
	K_printf_fmt_type_list: INTEGER = 9 -- 'L'
	K_printf_fmt_type_boolean: INTEGER = 10 -- 'B'
	K_printf_fmt_type_agent: INTEGER = 11 -- 'A'
	K_printf_fmt_type_percent: INTEGER = 12 -- 'P'
	K_printf_fmt_type_matrix: INTEGER = 13 -- 'M' (m is 13th letter)
	K_printf_fmt_type_any: INTEGER = 14 -- 'a'
	K_printf_fmt_type_table: INTEGER = 15 -- 'H'
	K_printf_fmt_type_complex: INTEGER = 16 -- 'C'
	K_printf_fmt_type_hex_cap: INTEGER = 17 -- 'X'
	K_printf_fmt_type_sequence: INTEGER = 18 -- 'S'

 --|========================================================================
feature {NONE} -- Error codes
 --|========================================================================

	K_printf_fmt_err_arg_count: INTEGER = 1
	K_printf_fmt_err_arg_type: INTEGER = 2

	K_printf_fmt_err_no_type: INTEGER = 11
	K_printf_fmt_err_unknown_type: INTEGER = 12

	K_printf_fmt_err_pad_char: INTEGER = 21

	K_printf_fmt_err_field_width: INTEGER = 31

	K_printf_fmt_err_internal: INTEGER = 99

 --|========================================================================
feature {AEL_PRINTF} -- Shared routines
 --|========================================================================

	printf_fmt_funcs: AEL_PF_FORMATTING_ROUTINES
		once
			create Result
		end

	last_printf_errors: LINKED_LIST [ AEL_PF_FORMAT_ERROR ]
			-- List of errors from most recent operation
		once
			create Result.make
		end

	reset_printf_errors
			-- Clear any presently recorded errors
		do
			last_printf_errors.wipe_out
		end

	--|--------------------------------------------------------------

	log_to_client (v: STRING)
			-- If client log agent exists, call it with 'v'
		local
			p: like client_log_proc
		do
			p := client_log_proc
			if p /= Void then
				p.call ([v])
			end
			io.error.put_string (v)
		end

	client_log_proc: detachable PROCEDURE [STRING]
		do
			Result := client_log_proc_cell.item
		end

	client_log_proc_cell: CELL [detachable PROCEDURE [STRING]]
		once
			create Result.put (Void)
		end

	set_client_log_proc (v: like client_log_proc)
			-- Set the procedure to call to log a message for the client
			-- For debugging support only
		do
			client_log_proc_cell.put (v)
		end

	--|--------------------------------------------------------------

	printf_client_error_agent:
		detachable PROCEDURE [AEL_PF_FORMAT_ERROR]
		do
			Result := printf_client_error_agent_cell.item
		end

	printf_client_error_agent_cell:
		CELL [detachable PROCEDURE [AEL_PF_FORMAT_ERROR]]
		once
			create Result.put (Void)
		end

	--|--------------------------------------------------------------

	printf_bool_out_is_lower: BOOLEAN
		do
			Result := printf_bool_out_cell.item
		end

	printf_bool_out_cell: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	printf_bool_out_true: STRING
			-- Representation of BOOLEAN True
		do
			if not printf_bool_out_alt_true.is_empty then
				Result := printf_bool_out_alt_true
			elseif printf_bool_out_is_lower then
				Result := Ks_printf_bool_lower_true
			else
				Result := Ks_printf_bool_dflt_True
			end
		end

	printf_bool_out_false: STRING
			-- Representation of BOOLEAN False
		do
			if not printf_bool_out_alt_false.is_empty then
				Result := printf_bool_out_alt_false
			elseif printf_bool_out_is_lower then
				Result := Ks_printf_bool_lower_false
			else
				Result := Ks_printf_bool_dflt_false
			end
		end

	--|--------------------------------------------------------------

	printf_bool_out_alt_true: STRING
			-- Alternate representation of BOOLEAN True
			-- Defined via 'set_printf_bool_out_alt_true'
		once
			create Result.make (0)
		end

	printf_bool_out_alt_false: STRING
			-- Alternate representation of BOOLEAN False
			-- Defined via 'set_printf_bool_out_alt_false'
		once
			create Result.make (0)
		end

	--|--------------------------------------------------------------

	set_printf_bool_out_alt_true (v: STRING)
			-- Set, to 'v', the representation of BOOLEAN True
		require
			valid: attached v as tv and then not tv.is_empty
		do
			printf_bool_out_alt_true.wipe_out
			printf_bool_out_alt_true.append (v)
		end

	set_printf_bool_out_alt_false (v: STRING)
			-- Set, to 'v', the representation of BOOLEAN False
		require
			valid: attached v as tv and then not tv.is_empty
		do
			printf_bool_out_alt_false.wipe_out
			printf_bool_out_alt_false.append (v)
		end

	--|--------------------------------------------------------------

	reset_printf_bool_out
			-- Reset the format of Boolean args to defaults
		do
			set_printf_bool_out_lower (False)
			reset_printf_bool_true
			reset_printf_bool_false
		end

	reset_printf_bool_true
			-- Reset, to default, the representation of BOOLEAN True
		do
			printf_bool_out_alt_true.wipe_out
		end

	reset_printf_bool_false
			-- Reset, to default, the representation of BOOLEAN False
		do
			printf_bool_out_alt_false.wipe_out
		end

	Ks_printf_bool_dflt_true: STRING = "True"
	Ks_printf_bool_dflt_false: STRING = "False"

	Ks_printf_bool_lower_true: STRING = "true"
	Ks_printf_bool_lower_false: STRING = "false"

	--|--------------------------------------------------------------

	has_non_digits_10 (v: STRING): BOOLEAN
			-- Does 'v' contain characters other than decimal digits?
		do
			Result := num_non_digits_10 (v) /= 0
		end

	has_non_digits_16 (v: STRING): BOOLEAN
			-- Does 'v' contain characters other than hex digits?
		do
			Result := num_non_digits_16 (v) /= 0
		end

	num_non_digits_10 (v: STRING): INTEGER
			-- Number of characters in 'v' other than decimal digits
		local
			i, lim: INTEGER
		do
			lim := v.count
			if lim /= 0 then
				from i := 1
				until i > lim
				loop
					if not v.item (i).is_digit then
						Result := Result + 1
					end
					i := i + 1
				end
			end
		end

	num_non_digits_16 (v: STRING): INTEGER
			-- Number of characters in 'v' other than hex digits
		local
			i, lim: INTEGER
		do
			lim := v.count
			if lim /= 0 then
				from i := 1
				until i > lim
				loop
					if not v.item (i).is_hexa_digit then
						Result := Result + 1
					end
					i := i + 1
				end
			end
		end

 --|========================================================================
feature {NONE} -- Validation
 --|========================================================================

	is_valid_printf_first_fmt_char (c: CHARACTER): BOOLEAN
		do
			inspect c
			when '#', '-', '+', '=', '~' then
				--| Decoration and alignment flags
				Result := True
				-- '#' is the decoration flag
				-- '-', '+' and '=' are alignment flags
				-- '~' is the agent flag
			else
				if is_valid_printf_type_specifier (c) then
					Result := True
				else
					--| Field width
					Result := c.is_digit
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_printf_type_specifier (c: CHARACTER): BOOLEAN
		do
			Result := Ks_printf_type_specifiers.has (c)
		end

	is_valid_fp_string (v: STRING): BOOLEAN
		do
			if not v.is_integer then
				Result := v.is_double
			end
		end

--|========================================================================
feature -- Default format options
--|========================================================================

	default_printf_fill_char: CHARACTER
			-- Usually blank unless numeric and zero,
			-- can be changed to any valid character
		do
			Result := dflt_printf_fillchar_ref.item
		end

	--|--------------------------------------------------------------

	default_printf_list_delimiter: STRING
			-- Default delimiter to use when representing List formats
		do
			Result := dflt_printf_list_delimiter
		end

	default_printf_matrix_delimiter: STRING
			-- Default delimiter to use when representing Matrix formats
		do
			Result := dflt_printf_matrix_delimiter
		end

	--|--------------------------------------------------------------

	decimal_separator: CHARACTER
			-- Separator to use to representing the decimal point
		do
			Result := dflt_decimal_separator_ref.item
		end

	--|--------------------------------------------------------------

	default_printf_thousands_delimiter: STRING
			-- Default delimiter to use between groups of 3 digits
			-- when representing decimal formats
		do
			Result := dflt_printf_thousands_delimiter
		end

--|========================================================================
feature -- Default format options setting
--|========================================================================

	set_default_printf_fill_char (v: CHARACTER)
			-- Change the fill character from blank to 
			-- the given new value for ALL subsequent 
			-- printf calls in this thread space
		do
			dflt_printf_fillchar_ref.put (v)
		end

	reset_default_printf_fill_char
			-- Reset the default fill character to blank
		do
			dflt_printf_fillchar_ref.put (' ')
		end

	--|--------------------------------------------------------------

	set_decimal_separator (v: CHARACTER)
			-- Change the character used to denote the decimal point
			-- to 'v' for ALL subsequent printf calls in this thread space
		do
			dflt_decimal_separator_ref.put (v)
		end

	reset_decimal_separator
			-- Reset the character used to denote the decimal point
		do
			dflt_decimal_separator_ref.put ('.')
		end

	--|--------------------------------------------------------------

	set_default_printf_list_delimiter (v: STRING)
			-- Change the default list delimiter string from a single
			-- blank character to the given string for ALL subsequent 
			-- printf calls in this thread space
		do
			dflt_printf_list_delimiter.wipe_out
			dflt_printf_list_delimiter.append (v)
		end

	reset_default_printf_list_delimiter
			-- Reset the default list delimiter string
		do
			dflt_printf_list_delimiter.wipe_out
			dflt_printf_list_delimiter.extend (' ')
		end

	--|--------------------------------------------------------------

	set_default_printf_matrix_delimiter (v: STRING)
			-- Change the default matrix delimiter string from a single
			-- blank character to the given string for ALL subsequent 
			-- printf calls in this thread space
		do
			dflt_printf_matrix_delimiter.wipe_out
			dflt_printf_matrix_delimiter.append (v)
		end

	reset_default_printf_matrix_delimiter
			-- Reset the default matrix delimiter string
		do
			dflt_printf_matrix_delimiter.wipe_out
			dflt_printf_matrix_delimiter.extend (' ')
		end

	--|--------------------------------------------------------------

	set_default_printf_thousands_delimiter (v: STRING)
			-- Change the default thousands delimiter string from an
			-- empty string to the given string for ALL subsequent 
			-- printf calls (in this thread space)
		do
			dflt_printf_thousands_delimiter.wipe_out
			dflt_printf_thousands_delimiter.append (v)
		end

	reset_default_printf_thousands_delimiter
			-- Reset the default thousands delimiter string
		do
			dflt_printf_thousands_delimiter.wipe_out
			dflt_printf_thousands_delimiter.extend (',')
		end

	set_printf_client_error_agent (v: like printf_client_error_agent)
			-- Set the procedure to call upon encountering a format error
		do
			printf_client_error_agent_cell.put (v)
		ensure
			is_set: printf_client_error_agent = v
		end

	set_printf_bool_out_lower (tf: BOOLEAN)
			-- If 'tf', then set the format of Boolean args to be
			-- lower case true/false.
			-- If not 'tf', then reset format to be Capitalized True/False
		do
			printf_bool_out_cell.put (tf)
		end

 --|========================================================================
feature {NONE} -- Alternate fill support
 --|========================================================================

	dflt_printf_fillchar_ref: CELL [CHARACTER]
			-- Usually blank unless numeric and zero,
			-- can be changed to any valid character
		once
			create Result.put (' ')
		end

	dflt_printf_list_delimiter: STRING
			-- Delimiter to use when representing List formats
		once
			create Result.make (1)
			Result.extend (' ')
		end

	dflt_printf_matrix_delimiter: STRING
			-- Delimiter to use when representing Matrix formats
		once
			create Result.make (1)
			Result.extend (' ')
		end

	dflt_printf_thousands_delimiter: STRING
			-- Thousands delimiter to use when representing decimal formats
		once
			create Result.make (1)
			Result.extend (',')
		end

	dflt_decimal_separator_ref: CELL [CHARACTER]
			-- Default separator to use to representing the decimal point
		once
			create Result.put ('.')
		end

 --|========================================================================
feature -- Help
 --|========================================================================

	printf_help_message: STRING
		do
			create Result.make (3072)
			Result.append ("{
 The various string formatting routines provide a means by which to
 format strings for output or other purposes in a manner reminiscent of
 the traditional printf functions in C and similar languages.

  Format string construction (in order):

    %
    [<decoration_flag>]
    [<agent_flag>]
    [<alignment_flag>]
    [<fill_specifier>]
    [<field_width>]
    <field_type>

  Where:

    <decoration_flag> ::=  '#'
      Decoration consumes part of the field width
      Decoration is applied as follows:
        "0x" preceding hexadecimal values
        "0" preceding octal values
        "b" following binary values
        Decimal values show delimiters at thousands (commas by default)
		    Thousands delimeters can also apply to unsigned (natural) 
          arguments using the decimal format, but only if the unsigned
          value does not exceed the max value of a signed 64 bit integer.

    <agent_flag> ::=  '~'
      Valid for List formats only.  Cannot be combined with decoration flag

    <alignment_flag> ::=  '-' |   '+'  |  '='
                         (left   right  centered)

    <fill_specifier> ::=  <character>
      Fills remainder of field width with given character
      (default is blank)

    <field_width> ::=  <simple_width> | <complex_width>

    <field_type> ::=  <character>

      Field type can be at least on of the following:
        'A' denotes an Agent expression.  Argument must be a function
            that accepts an argument of type ANY and returns a STRING.
        'a' denotes an 'any' expression.  Argument can be of any type,
            and output, for that argument will be <arg>.out.
            No decoration or aligment options apply.
        'b' denotes a BINARY integer expression
            This shows as ones and zeroes
        'B' denotes a BOOLEAN expression
            This shows as "True" or "False"
            Use the global option to set the output to be lower case.
        'c' denotes a single CHARACTER
        'C' denotes a Complex format, for a container and its items
        'd' denotes a DECIMAL integer expression
            Type specifier can be preceded by a delimiter character
            with which to separate groups of 3 adjacent digits (thousands).
            Alignment characters cannot be uses a delimiters.
        'f' denotes a REAL or DOUBLE expression
            Field width for floating point values
            are given in the form:
             <overall_width>"."<right_width>
             Where overall_width is the minimum
             width of the entire representation,
             and <right_width> is the width for
             the fractional part (a.k.a. precision)
             A precision of 0 results in a whole number
             representation, without decimal point
             (effectively rounded to integer)
        'L' denotes a list (or any CONTAINER)
            Type specifier can be preceded by a delimiter character
            with which to separate list items (default is blank).
            Alignment characters cannot be used a delimiters.
            In place of a delimiter or default decorator ('#'),
            the agent marker ('~') can be used (i.e "%%~L").
            In that case, the argument must be a TUPLE [CONTAINER,FUNCTION],
            with the function accepting an argument of type ANY, checking
            typed attachment in the body of the function.
            Example:
            printf ("Array Contents: %%~L", << [my_array, agent item_out] >>)
            Note well that the agent is called for -each- item in the 
            collection, and not for the collection as a whole.
        'M' denotes a matix (an ARRAY2)
            Type specifier can be preceded by a delimiter character
            with which to separate list items (default is blank).
            Alignment characters cannot be used a delimiters.
            In place of a delimiter or default decorator ('#'),
            the agent marker ('~') can be used (i.e "%%~M").
            In that case, the argument must be a TUPLE [ARRAY2,FUNCTION],
            with the function accepting an argument list of:
            (ANY, INTEGER, INTEGER), where the first ANY argument is the
            item in the matrix, and the INTEGER arguments are the row and
            column of the item.  The agent function checks the typed
            attachment of the ANY arg in the body of the function.
            Example:
            printf ("Matrix Contents: %%~M", << [my_matrix, agent item_out] >>)
            Note well that the agent is called for -each- item in the 
            matrix, and not for the collection as a whole.
        'o' denotes an OCTAL integer expression
        'P' denotes a PERCENT expression (float value multiplied by 100
            and followed by a percent symbol in the output
        's' denotes character STRING
        'S' denotes a SEQUENCE (a list or any CONTAINER)
            Similar to 'L' (List), but list separator is limited to
            a single character, without decoration.
            Type specifier can be preceded by a delimiter character
            with which to separate list items IN ADDITION TO a BLANK.
            For example, a comma (',') delimiter would have items
            separated by ", ".
            When not delimiter is specified, a blanks is used alone.
            Alignment characters cannot be used a delimiters.
            Unlike the List format, there is no support for decoration,
            nor is there agent support at this time.
        'u' denotes an UNSIGNED DECIMAL integer expression
        'x' denotes a HEXADECIMAL integer expression
        
 In use, a class calls one of the printf routines with at least
 a format string and an argument list.

 The argument list contains the arguments that align (positionally) with
 the format specifiers in the format string.
 The argument list, when indeed it holds multiple arguments, can be either a
 TUPLE or a proper descendent of FINITE.

 Clients wishing access to the printf functions should inherit or create
 and instance of the AEL_PRINTF class.
	}")
   end

end -- class AEL_PF_FORMATTING_CONSTANTS

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 010 02-Aug-2018
--|     Added support for Pointer, Agent, ANY, Matrix, Table and 
--|     Complex formats.  Added reset_printf_errors.
--|     Added support for alternative BOOLEAN representations.
--|     Added default_printf_matrix_delimiter; set and reset.
--|     Update help message text.
--|     Compiled and tested with Eiffel 18.7
--| 009 02-Mar-2013
--|     Updated help message to clarify thousands separator behavior 
--|     for natural arguments.
--| 008 23-Feb-2013
--|     Added support for percent, agent and list+agent formats.
--|     Agent format support required addition of the agent flag, '~' 
--|     in place of the decoration flag ('#') (i.e. a single format 
--|     token can have either a decoration flag or an agent flag but 
--|     not both).  Alignment flags are unaffected.
--|     Tweaked arg parsing to interpret any single list arg as an arg
--|     list.  The alternative (the previous interpretation) would be 
--|     to allow single-arg containers but fail to recognize 
--|     containers for list format when they are alone in a an actual 
--|     arg list like an array.
--| 007 03-Apr-2011
--|     Added default_printf_thousands_delimiter and related routines 
--|     to support non-default separators when grouping decimals.
--| 006 25-Aug-2010
--|     Change export of funcs and errors to AEL_PRINTF (see ael_printf.e)
--| 005 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_PRINTF and need not be addressed directly
--|----------------------------------------------------------------------
