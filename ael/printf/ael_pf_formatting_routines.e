note
	description: "{
Core formatting routines for ael_printf cluster
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_FORMATTING_ROUTINES

inherit
	AEL_PF_FORMATTING_CONSTANTS
	AEL_PF_FORMAT_ERROR_SUPPORT
	AEL_PF_TYPE_ROUTINES
	AEL_PF_SCAN_ROUTINES

 --|========================================================================
feature -- Core formatter
 --|========================================================================

	pf_formatted (fmt: STRING; args: detachable ANY): STRING
			-- A string formatted according to the format 'fmt'
			-- and arguments 'args' given, in the manner of printf
			--
			-- 'args' can be:
			--   A TUPLE containing arguments
			--   A data structure conforming to FINITE (e.g. ARRAY or LIST)
			--   An individual object reference relating to a single
			--   format item
			--   Or, if no arguments are needed, simply Void
			--
			--   N.B.
			--   There is potential for confusion/ambiguity.  Per policy, 
			--   when the outermost type of the arg list is an ARRAY, 
			--   it is interpreted as an argument list and NOT as a 
			--   single argument that happens to be an ARRAY.
			--
			-- A complete description is embedded within the help_message
			-- function elsewhere in this class.
		require
			format_string_exists: fmt /= Void
		local
			idx, fp_count: INTEGER
			fc: CELL [INTEGER]
			fp: detachable AEL_PF_FORMAT_PARAM
			tok: AEL_PF_FORMAT_TOKEN
			tokens: LINKED_LIST [AEL_PF_FORMAT_TOKEN]
			arg_count: INTEGER
			arglist: detachable FINITE [detachable ANY]
			argtuple: detachable TUPLE
			arg: detachable ANY
			ts: STRING
		do
			create tokens.make
			Result := ""
			if args /= Void then
				-- If args is an array, then interpret it as a list of 
				-- args and not as a single arg that happens to be a list
				if attached {ARRAY [detachable ANY]} args as argarray then
					arglist := argarray
					arg_count := argarray.count
				else
					if attached {FINITE [detachable ANY]} args as ta then
						arglist := ta
						if attached {READABLE_STRING_GENERAL} args as tsg then
							-- Strings are indexable, but we want the
							-- whole string, not each character
							arglist := << tsg >>
						end
						arg_count := arglist.count
					else
						if attached {TUPLE} args as ta then
							argtuple := ta
							arg_count := argtuple.count
						else
							arg_count := 1
							arglist := << args >>
						end
					end
				end
				-- TODO, is this a bit heavy-handed?
				last_printf_errors.wipe_out
				create fc.put (0)
				tokens := tokens_extracted (fmt, fc)
				if has_printf_error then
					Result := fmt + "  " + last_printf_error_out
				end
				fp_count := fc.item
			end -- if args /= Void
			if not has_printf_error then
				if fp_count = 0 then
					-- No format, then args are irrelevant
					if args /= Void then
						add_arg_count_error (fmt, fmt.count, fp_count, arg_count)
					end
					Result := fmt
				else
					Result := ""
					if fp_count > 1 then
						-- Must have a container or tuple argument list
						if arglist = Void and argtuple = Void then
							add_arg_count_error (fmt, fmt.count, fp_count, arg_count)
							Result := fmt
						end
					end
				end
			end
			--| RFO Is this too harsh?
			if (not has_printf_error) and fp_count > 0 and Result.is_empty then
				create Result.make (fmt.count)
				-- Walk the tokens and match up with arguments
				from
					tokens.start
					idx := 0
				until tokens.exhausted
				loop
					tok := tokens.item
					fp := tok.format
					if fp = Void then
						Result.append (tok.string_value)
					else
						if fp.has_format_error then
							-- A bad format specifier, or just text that
							-- looks like one, copy to output unchanged and
							-- assume there is not corresponding argument
							Result.append (fp.token)
						else
							--| Bump the argument index
							idx := idx + 1
							--| Check the argument index to make sure we didn't 
							--| run out of arguments
							if idx > arg_count then
								-- Ran out of arguments
								add_arg_count_error (fmt, tok.position, idx, arg_count)
								Result.append (Ks_printf_missing_arg_stub)
							else
								--| Get the positional argument for the format
								arg := Void
								if argtuple /= Void then
									-- The 'item' call triggers an error for 
									-- SCOOP compilation, because the 
									-- compiler sees it as separate.
									-- The problem seems to relate to TUPLEs 
									-- without typed items.
									-- The typed attachment test works around 
									-- this issue
									if attached {ANY} (argtuple.item (idx)) as tta then
										arg := tta
									end
								else
									-- To gain access to indexability of arglist
									if attached
									{INDEXABLE [detachable ANY,INTEGER]} arglist as tis
									 then
										if attached {READABLE_STRING_GENERAL} tis as tsg
										 then
											-- Strings are indexable, but we want the
											-- whole string, not the idxth character
											arg := tsg
										else
											-- If arg is a container, then it 
											-- must be interpreted as an arg 
											-- list and not as a single 
											-- container arg
											arg := tis.item (idx)
										end
									else -- not indexable
										Result.append (Ks_printf_missing_arg_stub)
										add_internal_error (
											fmt, tok.position,
											"Possible corrupted args list")
									end
								end
								fp.set_argument (arg)
								if arg_matches_type (arg, fp) then
									ts := fp.arg_out
									if fp.has_arg_type_error then
										-- Error not caught by arg_matches_type
										add_arg_type_error (
											fmt, tok.position,
											fp.expected_class_name,
											fp.argument_class_name)
										Result.append (Ks_printf_mismatch_type_stub)
									end
									Result.append (ts)
								else
									-- Not a match, consume the arg but
									-- show an error
									add_arg_type_error (
										fmt, tok.position,
										fp.expected_class_name,
										fp.argument_class_name)
									Result.append (Ks_printf_mismatch_type_stub)
								end
							end
						end
					end
					tokens.forth
				end
				--RFO should this be done before walking the arg list?
				if fp_count /= arg_count then
					-- Have too many args (too few already caught)
					add_arg_count_error (fmt, fmt.count, fp_count, arg_count)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	pf_extracted (buf, fmt: STRING): ARRAY [detachable ANY]
			-- Values extracted from 'buf', corresponding to the format
			-- specified in 'fmt'
			-- For each format specifier in 'fmt', Result will contain 
			-- an object corresponding to specifier, in relative 
			-- position.
			-- Tokens in 'fmt' that are not format specifiers do not 
			-- appear in Result.
			--
			-- A complete description is embedded within the help_message
			-- function elsewhere in this class.
		require
			buffer_exists: buf /= Void
			valid_format_string: fmt /= Void and then not fmt.is_empty
		local
			ti, lim, idx, fp_count: INTEGER
			fc: CELL [INTEGER]
			tok: AEL_PF_FORMAT_TOKEN
			tokens: LINKED_LIST [AEL_PF_FORMAT_TOKEN]
			values: LINKED_LIST [detachable ANY]
			tval: detachable ANY
			--ok_to_advance: BOOLEAN
		do
			-- Capture tokens from 'fmt'
			create Result.make_empty
			create fc.put (0)
			tokens := tokens_extracted (fmt, fc)
			fp_count := fc.item
			if fp_count > 0 and Result.is_empty then
				create values.make
				-- Walk the tokens and match up with values from 'buf'
				lim := buf.count
				from
					tokens.start
					idx := 1
				until tokens.exhausted or idx > lim
				loop
					--ok_to_advance := True
					tok := tokens.item
					if not attached tok.format as fp then
						-- Literal, advance through it
						if substr_is_whitespace (tok.string_value, 1, -1, True) then
							-- Advance to next non-ws
							ti := index_of_next_non_whitespace (buf, idx, lim, True)
							if ti = 0 then
								idx := lim + 1
							else
								idx := ti
							end
						else
							ti := buf.substring_index (tok.string_value, idx)
							if ti = 0 then
								-- ERROR, keep looking with next token
							else
								-- advance to char after token
								idx := ti + tok.string_value.count
							end
						end
					else
						if fp.has_format_error then
							-- A bad format specifier, or just text that
							-- looks like one; advance through it
							idx := idx + tok.string_value.count - 1
						else
							--| Extract the value for the format
							-- get *, then add to Result
							create fc.put (idx)
							tval := fp.value_in (buf, idx, fc)
							values.extend (tval)
							if tval = Void or fc.item = 0 then
								-- Not a type match
								-- Could call it quits and fill Result with 
								-- Voids, or can try to realign with next 
								-- token
								-- Show an error in any case

								-- ERROR
							else
								idx := fc.item
							end
						end
						idx := idx + 1
					end
					tokens.forth
				end
				if not values.is_empty then
					create Result.make_filled (Void, 1, values.count)
					from
						values.start
						idx := 1
					until values.exhausted
					loop
						Result.put (values.item, idx)
						idx := idx + 1
						values.forth
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	tokens_extracted (
		fmt: STRING; fc: CELL [INTEGER]): LINKED_LIST [AEL_PF_FORMAT_TOKEN]
			-- List of tokens, including but not limited to format 
			-- specifiers, extracted from 'fmt'
			-- Into 'fc', write the number of format specfiers found
		require
			valid: fmt /= Void and then not fmt.is_empty
			counter_exists: fc /= Void
		local
			pos, lim, fp_count, tok_width: INTEGER
			fp: detachable AEL_PF_FORMAT_PARAM
			c: CHARACTER
			tok: AEL_PF_FORMAT_TOKEN
--			ts: STRING
		do
			create Result.make
			create tok.make (1)
			Result.extend (tok)
			lim := fmt.count
			from pos := 1
			until pos > lim or has_printf_error
			loop
				c := fmt.item (pos)
				tok_width := 1
				if c /= '%%' then
					--| Not a format param
					--| Add char directly to Result
					tok.extend (c)
				elseif fmt.count <= pos then
					--| Not big enough to be a format param
					--| Add char directly to Result
					--| fmt.count is last fmt char, pos is current index
					--| if count = pos, then pct is last char and cannot
					--| be a format, but if count = pos + 1, it can be,
					--| but not much more
					tok.extend (c)
				else
					--| We have what we think is a format parameter
					--| Create a new format parameter
					create fp.make_from_string (fmt, pos)
					if fp.has_error then
						--print ("%N***** FP [" + fmt + "] HAS ERROR [" +
						--ts + "]*****%N")
						tok.set_string_value (last_printf_error_out)
						Result.extend (tok)
					else
						if (not tok.is_empty) or tok.is_format then
							-- In use
							create tok.make (pos)
							Result.extend (tok)
						end
						--| Reposition past the format parameter
						tok_width := fp.original_length
						if fp.is_typed then
							if not fp.has_format_error then
								fp_count := fp_count + 1
								tok.set_format (fp)
							else
								-- Need to handle an actual error,
								-- already recorded in param
							end
						else
							-- Not a format token, just a string
							tok.set_string_value (fp.token)
						end
					
						create tok.make (pos+tok_width)
						Result.extend (tok)
					end
				end
				pos := pos + tok_width
			end -- end token extraction loop
			fc.put (fp_count)
		end

	--|========================================================================
feature -- Conversion functions
	--|========================================================================

	natural_out (v: NATURAL_64; nb: INTEGER): STRING
			-- Decimal string representation of given unsigned integer,
			-- with no padding, and respectful of integer size (nb)
		do
			if v = 0 then
				Result := "0"
			else
				inspect nb
				when 1 then
					Result := v.to_natural_8.out
				when 2 then
					Result := v.to_natural_16.out
				when 4 then
					Result := v.to_natural_32.out
				else
					Result := v.out
				end
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

--RFO 	integer_to_hex (v: INTEGER_64; nb: INTEGER): STRING
			-- Hexadecimal string representation of given integer,
			-- with no padding.
			--
			-- Hex format uses 1 position (digit) for each nibble (4 bits)
			-- The idea is to capture the least significant nibble from
			-- the original value, convert that into a hex character,
			-- then shift the original value by	4 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer.
--RFO 		do
--RFO 			Result := natural_to_hex (v.to_natural_64, nb)
--RFO 		ensure
--RFO 			exists: Result /= Void and then not Result.is_empty
--RFO 		end

	--|--------------------------------------------------------------

	natural_to_hex (v: NATURAL_64; nb: INTEGER; cf: BOOLEAN): STRING
			-- Hexadecimal string representation of given unsigned integer,
			-- with no padding.
			--
			-- Hex format uses 1 position (digit) for each nibble (4 bits)
			-- The idea is to capture the least significant nibble from
			-- the original value, convert that into a hex character,
			-- then shift the original value by	4 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer.
			-- If 'cf', use capital letters for a-f
		local
			tmps, xds: STRING
			tv, tm: NATURAL_64
		do
			if v = 0 then
				Result := "0"
			else
				if cf then
					xds := Printf_hex_cap_digits
				else
					xds := Printf_hex_digits
				end
				tv := v
				--| Build the result string backwards, then flip it
				create tmps.make (8)
				from
				until tv = 0
				loop
					tm := tv \\ 16
					tmps.extend (xds.item (tm.as_integer_32 + 1))
					tv := tv // 16
				end
				Result := tmps.mirrored
				Result.keep_tail (nb*2)
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	integer_to_octal (v: INTEGER_64; nb: INTEGER): STRING
			-- Octal string representation of given integer,
			-- with no padding
			--
			-- Octal format uses 1 position (digit) for each 3 bits
			-- The idea is to capture the least significant 3 bits from
			-- the original value, convert that into an octal character,
			-- then shift the original value by	3 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer
		local
			tmps: STRING
			tv, tm: NATURAL_64
		do
			if v = 0 then
				Result := "0"
			else
				inspect nb
				when 1 then
					Result := natural_8_to_octal (v.to_natural_8)
				when 2 then
					Result := natural_16_to_octal (v.to_natural_16)
				when 4 then
					Result := natural_32_to_octal (v.to_natural_32)
				else
					tv := v.to_natural_64
					--| Build the result string backwards, then flip it
					create tmps.make (10)
					from
					until tv = 0
					loop
						tm := tv \\ 8
						tmps.extend (Printf_octal_digits.item (tm.as_integer_32 + 1))
						tv := tv // 8
					end
					Result := tmps.mirrored
				end
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	natural_8_to_octal (v: NATURAL_8): STRING
			-- Octal string representation of given integer,
			-- with no padding
			--
			-- Octal format uses 1 position (digit) for each 3 bits
			-- The idea is to capture the least significant 3 bits from
			-- the original value, convert that into an octal character,
			-- then shift the original value by	3 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer
		local
			tmps: STRING
			tv, tm: NATURAL_8
		do
			if v = 0 then
				Result := "0"
			else
				tv := v
				--| Build the result string backwards, then flip it
				create tmps.make (10)
				from
				until tv = 0
				loop
					tm := tv \\ 8
					tmps.extend (Printf_octal_digits.item (tm.as_integer_32 + 1))
					tv := tv // 8
				end
				Result := tmps.mirrored
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	natural_16_to_octal (v: NATURAL_16): STRING
			-- Octal string representation of given integer,
			-- with no padding
			--
			-- Octal format uses 1 position (digit) for each 3 bits
			-- The idea is to capture the least significant 3 bits from
			-- the original value, convert that into an octal character,
			-- then shift the original value by	3 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer
		local
			tmps: STRING
			tv, tm: NATURAL_16
		do
			if v = 0 then
				Result := "0"
			else
				tv := v
				--| Build the result string backwards, then flip it
				create tmps.make (10)
				from
				until tv = 0
				loop
					tm := tv \\ 8
					tmps.extend (Printf_octal_digits.item (tm.as_integer_32 + 1))
					tv := tv // 8
				end
				Result := tmps.mirrored
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	natural_32_to_octal (v: NATURAL_32): STRING
			-- Octal string representation of given integer,
			-- with no padding
			--
			-- Octal format uses 1 position (digit) for each 3 bits
			-- The idea is to capture the least significant 3 bits from
			-- the original value, convert that into an octal character,
			-- then shift the original value by	3 bits, repeating the
			-- process until all bits are exhausted
			-- From a print standpoint, it's exhausted when the value
			-- remaining is zero, regardless of the number of bits in a
			-- theoretical integer
		local
			tmps: STRING
			tv, tm: NATURAL_32
		do
			if v = 0 then
				Result := "0"
			else
				tv := v
				--| Build the result string backwards, then flip it
				create tmps.make (10)
				from
				until tv = 0
				loop
					tm := tv \\ 8
					tmps.extend (Printf_octal_digits.item (tm.as_integer_32 + 1))
					tv := tv // 8
				end
				Result := tmps.mirrored
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	integer_to_binary (v: INTEGER_64): STRING
			-- Binary string representation of the given integer
			-- Integer argument can be of any size
			-- Representation presents smallest possible complete
			-- set of bit values (e.g. a value that fits into an
			-- INTEGER_16, but not into an INTEGER_8 shows as 16 bits)
		local
			i, first_bit: INTEGER
			vn: NATURAL_64
		do
			first_bit := 63
			vn := v.as_natural_64
			if v < 0 then
				-- Negative value
				if v >= {INTEGER_8}.min_value then
					first_bit := 7
				elseif v >= {INTEGER_16}.min_value then
					first_bit := 15
				elseif v >= {INTEGER_32}.min_value then
					first_bit := 31
				end
			else
				if v <= {NATURAL_8}.max_value then
					first_bit := 7
				elseif v <= {NATURAL_16}.max_value then
					first_bit := 15
				elseif v <= {NATURAL_32}.max_value then
					first_bit := 31
				end
			end
			create Result.make (first_bit)
			from i := first_bit
			until i < 0
			loop
				if vn.bit_test (i) then
					Result.extend ('1')
				else
					Result.extend ('0')
				end
				i := i - 1
			end
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	double_parts_out (wp, fp: STRING; fpw: INTEGER): STRING
			-- Combined formatted whole and fractional parts
			-- with fract part padded or truncated to fractional
			-- field width
			-- No whole part padding
		local
			fps, rp, lzs: STRING
			nc: INTEGER
			v1: INTEGER_64
			td: REAL_64
			p10: INTEGER
		do
			rp := ""
			if fp.count > fpw then
				-- Be very careful with leading zeroes.  The fractional 
				-- part has been separated from the whole, and so a 
				-- fractional part less than (originally) 0.1 will have 
				-- one or more leading zeroes.  Conversion of the now 
				-- integer-like value to real would ignore the leading 
				-- zeroes and change the value, dividing it by 'n' 
				-- factors of ten, where 'n' is the number of leading 0s.
				-- Work on the field width with the fractional part as a 
				-- string first, determining the number of leading zeroes,
				-- then add them after numeric rounding.
				--
				-- Truncate, with rounding
				-- e.g.  xx.59999 with 2 decimal pts should be xx.60
				-- Divide (real) by powers of ten to gen a new real value 
				-- whose whole part is the not-yet-rounded former 
				-- fractional part, then round that value to the 
				-- fractional part with the proper number of digits.
				nc := num_leading_chars ('0', fp)
				if nc > 0 then
					create lzs.make_filled ('0', nc)
				else
					lzs := ""
				end
				td := fp.to_real_64
				from p10 := fp.count - fpw
				until p10 <= 0
				loop
					td := td / 10
					p10 := p10 - 1
				end
				v1 := td.rounded_real_64.truncated_to_integer_64
				fps := lzs + v1.out
			elseif fp.count < fpw then
				-- Right pad with padding character
				create rp.make_filled ('0', fpw - fp.count)
				create fps.make (fp.count + rp.count)
				fps.append (fp)
			else
				fps := fp
			end
			Result := wp + decimal_separator.out + fps + rp
		end

	--|--------------------------------------------------------------

	double_parts (v: DOUBLE): LIST [STRING]
			-- DOUBLE value decomposed into whole and fractional parts
			-- as strings
		local
			ts: STRING
		do
			if v < v.zero then
				ts := (-v).out
			else
				ts := v.out
			end
			Result := fp_out_to_parts (ts)
		ensure
			exists: Result /= Void
			valid: Result.count = 2 and then
				Result.first /= Void and Result.last /= Void
		end

	--|--------------------------------------------------------------

	float_parts (v: REAL_32): LIST [STRING]
			-- FLOAT (REAL_32) value decomposed into whole and fractional 
			-- parts as strings
		local
			ts: STRING
		do
			if v < v.zero then
				ts := (-v).out
			else
				ts := v.out
			end
			Result := fp_out_to_parts (ts)
		ensure
			exists: Result /= Void
			valid: Result.count = 2 and then
				Result.first /= Void and Result.last /= Void
		end

	--|--------------------------------------------------------------

	fp_out_to_parts (v: STRING): LIST [STRING]
			-- Floating point value string representation decomposed
			-- into whole and fractional part strings
			-- 'pc' is the radix point character used in 'v'
		local
			ts: STRING
			pc: like decimal_separator
		do
			create {ARRAYED_LIST [STRING]}Result.make (2)
			if v.is_integer_64 then
				Result.wipe_out
				Result.extend (v.to_integer_64.out)
			elseif is_valid_fp_string (v) then
				ts := v
				if is_exponential_notation (v) then
					ts := scientific_to_decimal (v)
					pc := decimal_separator
				elseif v.has (decimal_separator) then
					--				pc := '.'
					pc := decimal_separator
				end
				-- At this point (so to speak), we have a string 
				-- representation of the fp value, according to the 
				-- internal formatting mechanism.
				-- The next step is to apply the desired formatting to the 
				-- value, but rather than work from the double value itself,
				-- we work with the already normalized string

				Result := ts.split (pc)
			end
			if Result.is_empty then
				Result.extend ("0")
				Result.extend ("0")
			elseif Result.count = 1 then
				-- Double had no fractional part
				Result.extend ("0")
			end
		ensure
			exists: Result /= Void
			valid: Result.count = 2 and then
				Result.first /= Void and Result.last /= Void
		end

	--|--------------------------------------------------------------

	is_exponential_notation (v: STRING): BOOLEAN
			-- Is the given string in exponential/scientific notation?
			-- Example:  "1.234c012"
		require
			exists: v /= Void
		do
			if not v.is_empty then
				Result := v.item (1).is_digit and
					(v.occurrences ('.') = 1) and
					(v.as_lower.occurrences ('e') = 1)
			end
		end

	--|--------------------------------------------------------------

	scientific_to_decimal (v: STRING): STRING
			-- Convert the given string, in scientific (exponential)
			-- notation to simple decimal notation
			-- Example:  1.0000000000000001e-15 -> 0.000000000000001
			-- Example:  1.00006e03 --> 1000.06
			-- Example:  1.00006e-03 --> 0.00100006
			-- Example:  123.00456e012 --> 123004560.0
			-- Example:  1.2e-15 --> 0.0000000000000012
		require
			exists: v /= Void and then not v.is_empty
			is_scientific: is_exponential_notation (v)
		local
			is_neg_exp: BOOLEAN
			vs, wps, fps: STRING
			dp, ei, exp: INTEGER
		do
			create Result.make (0)
			vs := v.as_lower
			ei := vs.index_of ('e', 1)
			exp := vs.substring (ei + 1, vs.count).to_integer_32
			-- 'exp' is the exponent value, and may be negative
			-- 'vs' is now the value (non-exponent) field, and may or 
			-- may not have a non-zero whole part
			vs := vs.substring (1, ei - 1)

			if exp < 0 then
				is_neg_exp := True
				exp := -exp
			end

			-- Can be victimized (again) by FP precision issues here
			-- Typically, for REAL_64 0s, we see a low order value added,
			-- and for REAL_32 0s, we see a low order value subtracted
			dp := vs.index_of ('.', 1)
			if dp > 0 then
				fps := vs.substring (dp + 1, vs.count)
				wps := vs.substring (1, dp - 1)
			else
				fps := "0"
				wps := vs
			end

			create Result.make (exp+wps.count+fps.count+2)
			if is_neg_exp then
				-- Value part must be shifted right (zero left padded)
				-- to align decimal point properly
				-- Depending on size of wps relative to exp, wps might
				-- straddle the decimal point (i.e. is the negative
				-- exponent 'negative enough' to capture all digits?)
				if exp >= wps.count then
					-- Shift all digits to right of dp
					Result.extend ('0')
					Result.extend (decimal_separator)
					Result.append (create {STRING}.make_filled ('0',exp-1))
					Result.append (wps)
				else
					-- exp < wps.count; Split wps across dp
					Result.append (wps.substring (1, (wps.count - exp)))
					Result.extend (decimal_separator)
					Result.append (wps.substring ((wps.count - exp) + 1, wps.count))
				end
				Result.append (fps)
			else
				-- Value part must be shifted left (zero right padded)
				-- to align decimal point properly
				-- If exp is positive, then wps will always appear
				-- in result unchanged
				Result.append (wps)
				-- Depending on size of fps relative to exp, fps might
				-- straddle the decimal point
				if exp >= fps.count then
					Result.append (fps)
					Result.append (create {STRING}.make_filled ('0',exp-fps.count))
					Result.extend (decimal_separator)
					Result.extend ('0')
				else
					-- exp < fps.count; Split fps across dp
					Result.append (fps.substring (1, (fps.count - exp) - 2))
					Result.extend (decimal_separator)
					Result.append (fps.substring ((fps.count - exp) - 1, fps.count))
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	roundable_to_position (v: STRING; tp: INTEGER): BOOLEAN
			-- Is string, representing fractional part of float, 
			-- roundable, to position 'tp', within its bounds?
			--
			-- Rounding the digit, at position 'tp' in string 'v'
			-- e.g. v =  999000000000005, tp = 2
			--            ^
			-- would result in a carry of the first digit, and so is not 
			-- roundable within its bounds
		require
			valid_position: tp <= v.count
		local
			lim: INTEGER
			ti, ni: INTEGER_64
		do
			lim := v.count
			if tp = lim then
				Result := True -- but would be a no-op
			else
				ti := char_to_int64 (v.item (tp))
				-- tp is not equal to lim, so we can look ahead by 1
				ni := char_to_int64 (v.item (tp + 1))
				--if ni > 5 or (ni = 5 and ti \\ 2 /= 0) then
				if not is_roundable (ti, ni) then
					Result := True -- but won't need it
				else
					if ti < 9 then
						-- Will not wrap
						Result := True
					elseif ti = 9 then
						-- Will carry into next digit, no good
					end
				end
			end
		end

	obs_round_to_position (v: STRING; tp: INTEGER)
--Replaced with function 'rounded_to_precision', working with full 
--string, not just fractional part
			-- Round the digit, at position 'tp' in string 'v'
			-- e.g. v =  999000000000005, tp = 2
			--            ^
			--      v = 100
			--            ^
			--  N.B. shift all trailing digits to right (if any), 
			--  because new string has spilled into area BEFORE start.
			--
			-- Cannot do it because rounding would make value at tp 0, 
			-- and would force carry to tp 1, and tp 0 (before the start 
			-- of string).  Clearly, this logic is wrong
			-- round 'v' to what?  the value would have to spill/carry 
			-- into the whole part (assuming this is the fractional 
			-- part), or would 
		require
			valid_position: tp <= v.count
		local
			i, lim: INTEGER
			ti, ni, ri: INTEGER_64
			is_even, carry, rf, done: BOOLEAN
		do
			lim := v.count
			if tp = lim then
				-- Do Nothing
				done := True
			else
				-- If the target digit is less than 9, then rounding can 
				-- be limited to the target position and the digits to 
				-- its right.
				-- If the target digit is 9, then rounding would need to 
				-- propagate to the digits to its left, INCLUDING the 
				-- ones past the decimal point!!!
				ti := char_to_int64 (v.item (tp))
				-- tp is not equal to lim, so we can look ahead by 1
				ni := char_to_int64 (v.item (tp + 1))
				--if ni > 5 or (ni = 5 and ti \\ 2 /= 0) then
				if is_roundable (ti, ni) then
					-- Need to round up target, regardless of what might 
					-- lay to the right of 'ni'
					if ti < 9 then
						-- Will not wrap
						-- Round up the target digit
						ti := ti + 1
						v.put (int64_to_char (ti), tp)
						done := True
					elseif ti = 9 then
						-- Will carry into next digit
					else
						-- Target digit might wrap, need to do iterative 
						-- round-up
					end
				elseif ni = 5 then
					-- Next digit is '5' and target is even, so would not 
					-- round up if that's all there was, ...
					-- BUT the 5 could round to six, based on its 
					-- right-hand neighbor, so look deeper or give up and 
					-- jump into a full iterative rounding loop
				else
					-- ni < 5
					-- No need to round up target
					done := True
				end
			end
			if not done then
				if ti < 9 then
					-- This means that target will not carry on round-up
					-- So it's necessary to look farther to the
					-- right to see if there would be a rounding 
					-- propagation issue, with the the right-hand neighbor
					-- being bumped over to 6, requiring the target to be 
					-- rounded up.
					--
					-- All that is needed, as far as looking to the right
					-- goes, is to determine if that next digit will bump
					-- and therefore require the target to round up.
					--
					-- ti is already set to target, and < 9
					-- ni is already set to target's RH neighbor, and >= 5
					from i := tp + 1
					until i > lim or rf
					loop
						if (i+2) <= lim then
							ri := char_to_int64 (v.item (i+2))
							rf := needs_rounding (ni, ri)
						else
							-- Near the end of the string
						end
						ni := ri
						i := i + 1
					end
					if rf then
						ti := ti + 1
						v.put (int64_to_char (ti), tp)
					end
				else
					-- ti = 9
					-- Incrementing the target digit would cause it to
					-- wrap, and so the whole field should be checked,
					-- right-to-left, even past the decimal point.
					--
					-- From end of original string, until processing the 
					-- target position AND no carries are pending, round 
					-- digits at each position based on values of that digit 
					-- and the one to its right
					carry := False
					ni := char_to_int64 (v.item (lim))
					from i := lim - 1
					until i <= 1 or done
					loop
						ti := char_to_int64 (v.item (i))
						is_even := ti \\ 2 = 0
						--if ni > 5 or (ni = 5 and not is_even) or carry then
						if is_roundable (ni, ti) then
							-- Round up the current digit
							ti := ti + 1
							if ti = 10 then
								carry := True
								v.put ('0', i)
							else
								carry := False
								v.put (int64_to_char (ti), i)
							end
						else
							-- Don't round and don't carry
							carry := False
							if i <= tp then
								done := True
							end
						end
						ni := ti
						i := i - 1
					end
					-- Pick up the last character
					if carry and not done then
						ti := char_to_int64 (v.item (1)) + 1
						v.put (int64_to_char (ti), 1)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	digits_to_ints (v: STRING): ARRAYED_LIST [INTEGER]
			-- Digits in string 'v', each converted to integers, and 
			-- inserted into list of values
		require
			is_decimal: v.is_integer_64
		local
			i, lim, tv: INTEGER
			tc, c0: CHARACTER
		do
			lim := v.count
			create Result.make (lim)
			c0 := '0'
			from i := 1
			until i > lim
			loop
				tc := v.item (i)
				tv := (tc.natural_32_code - c0.natural_32_code).as_integer_32
				Result.extend (tv)
				i := i + 1
			end
		end

	digits_out (da: LIST [INTEGER]): STRING
			-- Values in 'da' as comma-separated list
		do
			create Result.make ((da.count * 2)  - 1)
			from da.start
			until da.after
			loop
				Result.append (da.item.out)
				da.forth
				if not da.after then
					Result.extend (',')
				end
			end
		end

	--|--------------------------------------------------------------

	rounded_to_precision (v: STRING; np: INTEGER): STRING
			-- Float value represented by 'v' rounded to position 'np'
			-- e.g. v =  99.9000000000005, np = 2
			--               ^
			--      v = 100.00
			-- N.B. 'v' might not have a decimal point
		require
			--valid_precision: np <= v.count
			valid_precision: np >= 0
		local
			i, lim: INTEGER
			prts: LIST [STRING]
			wv: INTEGER_64
			fp, wp: STRING
			dout1, dout2: STRING
			tv, pv, wlen, flen: INTEGER
			digits: ARRAYED_LIST [INTEGER]
			carry, dbf, errf: BOOLEAN
		do
			dbf := False -- enable/disable debug logic
			Result := ""
			wp := ""
			fp := ""
			create digits.make (0)
			if not (v.is_integer_64 or is_valid_fp_string (v)) then
				-- Input ERROR
				errf := True
				Result := "Invalid floating point argument: " + v
			elseif has_non_digits_10 (v) then
				if v.has (decimal_separator) then
					prts := v.split (decimal_separator)
					wp := prts.first
					fp := prts.last
					digits := digits_to_ints (fp)
				else
					-- ERROR
					Result := "Invalid or corrupted numeric value: " + v
					errf := True
				end
			else
				-- Is whole-only; no rounding needed, just padding
				-- See lim check below
				wp := v.twin
				fp := ""
				create digits.make (0)
			end
			if not errf then
				wlen := wp.count
				flen := fp.count
				wv := wp.to_integer_64
				lim := fp.count
				if lim <= np then
					-- Might need padding, otherwise done
					from i := fp.count + 1
					until i > np
					loop
						digits.extend (0)
						i := i + 1
					end
				else
					-- 99.9, np=2 -> "99.90"
					-- 99.9 -> 99, 9 -> [9]
					-- flen < np -> no rounding, zero-pad to np
					-- 9, 0
					-- No loop
					-- [9, 0] -> 99, [9, 0] -> 99.90
					--
					-- 99.995, np=2 -> "100.00"
					-- 99.995 -> 99, 995 -> [9, 9, 5]
					-- i=3  9, 9, 5 -> tv=5, pv=9 roundable -> [9, 10, 0]
					-- i=2  9, 10, 0 -> tv=10, pv=9 roundable -> [10, 0, 0]
					-- i=1  10, 0, 0 -> not roundable, loop terminates
					-- 10,* -> whole := whole + 1
					-- [10, 0, 0] -> 99 + 1, [0, 0] -> 100.00
					--
					-- 99.955, np=2 -> "99.96"
					-- 99.955 -> 99, 955 -> [9, 5, 5]
					-- i=3  9, 5, 5 -> tv=5, pv=5 roundable -> 9, 6, 0
					-- i=2  9, 6, 0 -> tv=6, tv/=10, no rounding, loop terminates
					-- 9,* -> whole unchanged
					-- 9, 6, 0 -> 99, [9, 6] -> 99.96
					--
					-- 99.955, np=0 -> "100."
					-- 99.955 -> 99, 955 -> [9, 5, 5]
					-- i=3  9, 5, 5 -> tv=5, pv=5 roundable -> 9, 6, 0
					-- i=2  9, 6, 0 -> tv=6, pv=9 roundable -> 10, 0, 0
					-- i=1 10, 0, 0 -> loop terminates
					-- 10,* -> whole := whole + 1
					-- 10, 0, 0 -> 99 + 1, [] -> 100.
					--
					-- 100, np=2 -> "100.00" (no decimal pt in input)
					-- 100 -> 100, "" -> []
					-- flen < np -> no rounding, zero-pad to np
					-- 0, 0
					-- No loop
					-- [0, 0] -> 100, [0, 0] -> 100.00
					from i := lim
					until i < np or i = 0
					loop
						-- for debug only
						if dbf then
							dout1 := digits_out (digits)
						end
						tv := digits.i_th (i)
						if i = np and tv /= 10 then
							-- Done; no need to round beyond here, and tv is 
							-- not a result of rounding
						else
							if i = 1 then
								pv := 0
							else
								pv := digits.i_th (i - 1)
							end
							if is_roundable (tv, pv) then
								-- bump pv and set tv to 0
								tv := 0
								digits.put_i_th (tv, i)
								if i = 1 then
									carry := True
								else
									pv := pv + 1
									digits.put_i_th (pv, i - 1)
								end
							else
								-- No rounding needed; leave as is
								if i = np + 1 then
									-- Nothing left to carry/round, tap the 
									-- index to force exit
									i := i - 1
								end
							end
						end
						-- for debug only
						if dbf then
							dout2 := digits_out (digits)
						end
						i := i - 1
					end
					if np > 0 and np <= lim and then digits.i_th (np) = 10 then
						--???
						-- Stopped rounding at np, but need to propagate 
						-- farther West.  Do we stop at np - 1 then?
					end
				end -- else, not flen < np
				if digits.first = 10 or carry then
					-- fract part was rounded out of existence,
					-- bump the whole part
					wv := wv + 1
					digits.put_i_th (0, 1)
				end
				-- for debug only
				if dbf then
					dout2 := digits_out (digits)
				end
				-- rebuild fractional part from digits
				Result := wv.out + decimal_separator.out
				from i := 1
				until i > np
				loop
					tv := digits.i_th (i)
					Result.append (tv.out)
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	is_roundable (v, vp: INTEGER_64): BOOLEAN
			-- Is 'v' large enough that it could cause rounding of its 
			-- next higher-order neighbor digit (vp) ?
		do
			Result := v > 5 or (v = 5 and (vp \\ 2 /= 0))
		end

	--|--------------------------------------------------------------

	needs_rounding (ni, ri: INTEGER_64): BOOLEAN
			-- Does digit 'ni' need to be rounded up, based on the value 
			-- of its right-hand neighbor 'ri'?
		do
			if ri > 5 then
				Result := True
			elseif ri = 5 then
				-- Round up if digit is odd, else don't
				Result := ni \\ 2 /= 0
			end
		end

	--|--------------------------------------------------------------

	int64_to_char (v: INTEGER_64): CHARACTER
			-- Character (from 0-9) representing value 'v'
		require
			valid_range: v >= 0 and v <= 9
		do
			Result := v.out.item (1)
		end

	--|--------------------------------------------------------------

	char_to_int32 (v: CHARACTER): INTEGER_32
		local
			c0: CHARACTER
		do
			c0 := '0'
			Result := (v.natural_32_code - c0.natural_32_code).as_integer_32
		end

	char_to_int64 (v: CHARACTER): INTEGER_64
		local
			c0: CHARACTER
		do
			c0 := '0'
			Result := (v.natural_32_code - c0.natural_32_code).as_integer_64
		end

	--|--------------------------------------------------------------

	num_leading_chars (c: CHARACTER; v: STRING): INTEGER
			-- The number of leading characters 'c' in string 'v', if any
		require
			exists: v /= Void
		local
			i, lim, nc: INTEGER
		do
			Result := -1
			lim := v.count
			from i := 1
			until i > lim or Result >= 0
			loop
				if v.item (i) /= c then
					Result := nc
				else
					nc := nc + 1
					i := i + 1
				end
			end
			Result := nc -- case of all chars in 'v' being 'c'
		end

end -- class AEL_PF_FORMATTING_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 016 02-Apr-2019
--|     Replaced problematic 'round_to_position' with simpler (and 
--|     more reliable) 'rounded_to_precision' for floating point 
--|     rounding
--|     Updated header text
--|     Compiled and tested with Eiffel 18.1
--| 015 28-Aug-2018
--|     Added support for new formats: Matrix, (Hash) Table, Any, 
--|     Complex
--|     Compiled and tested with Eiffel 18.1
--| 014 10-Aug-2018
--|     Added assignment test in pf_formatted type detection to 
--|     address and issue with TUPLEs lacking type args, in SCOOP envs
--|     Made significant changes to floating point formatting to 
--|     address issues with fractional parts (not fixed in 013)
--| 013 19-Mar-2018
--|     Fixed major issue in double_parts_out where leading zeroes in 
--|     the fractional part were being lost during rounding/truncation
--| 012 08-Oct-2015
--|     Fixed issue in double_parts_out where truncating fractional 
--|     part was just plain wrong due to improper rounding logic.
--| 011 19-Jul-2013
--|     Tweaked type-matching for strings to support immutables
--| 010 23-Feb-2013
--|     Added support for percent, agent and list+agent formats.
--|     Agent format support required addition of the agent flag, '~' 
--|     in place of the decoration flag ('#') (i.e. a single format 
--|     token can have either a decoration flag or an agent flag but 
--|     not both).  Alignment flags are unaffected.
--|     Tweaked arg parsing to interpret any single list arg as an arg
--|     list.  The alternative (the previous interpretation) would be 
--|     to allow single-arg containers but fail to recognize 
--|     containers for list format when they are alone in a an actual 
--|     arg list like an array.
--| 009 18-May-2011
--|     Replaced integer_to_hex function with natural_to_hex.
--| 008 03-Apr-2011
--|     Updated integer_to_binary to use NATURALs instead of INTEGERs 
--|     to avoid premature size promotion.
--| 007 06-Oct-2009
--|     Fixed issue with floats rendered without fractional parts
--| 006 17-Aug-2009
--|     Fixed problem with incorrect rendering of floats less than 1
--|     Improved rounding behavior to use round-half-to-even method
--| 005 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_PRINTF and need not be addressed directly
--|----------------------------------------------------------------------
