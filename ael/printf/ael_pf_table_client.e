note
	description: "{
Client of hash table via which otherwise hidden status is
accessible
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_PF_TABLE_CLIENT

inherit
	HASH_TABLE [INTEGER, STRING]
		redefine
			make
		end

create
	make_with_table

create {HASH_TABLE}
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_table (v: HASH_TABLE [detachable ANY, HASHABLE])
		do
			make (0)
			table := v
		end

	make (sz: INTEGER)
			-- Create Current with initial size 'sz
		local
			tt: HASH_TABLE [detachable ANY, STRING]
		do
			create tt.make (0)
			table := tt
			Precursor (sz)
		end

--|========================================================================
feature
--|========================================================================

	table: HASH_TABLE [detachable ANY, HASHABLE]

	table_position: INTEGER
			-- Position (zero-based) of iterator for 'table'
			-- Relevant for iteration over table (vs its linear rep)
		do
			Result := table.iteration_position
		end

	is_last: BOOLEAN
			-- Is 'table' in its last iteration position?
		do
			Result := table.iteration_position = table.iteration_upper
		end

end -- class AEL_PF_TABLE_CLIENT

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 28-Aug-2018
--|     Original module
--|     Compiled and tested w/ Eiffel 18.7
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_PRINTF and need not be addressed directly
--|----------------------------------------------------------------------
