class PRINTF_TEST

inherit
	PRINTF_TESTS

create
	make

 --|========================================================================
feature -- Initialization
 --|========================================================================

	make
		do
			create tests_passed.make
			create tests_failed.make
			show_expected_on_pass := True

			exec_arg_count_test_1
			exec_percent_char_test_1
			exec_percent_char_test_2
			exec_percent_char_test_3

			exec_any_test_1
			exec_any_test_2
			exec_any_test_3

			exec_string_test_1
			exec_string_test_2
			exec_string_test_3
			exec_string_test_4
			exec_string_test_5

			exec_boolean_test_1
			exec_boolean_test_2
			exec_boolean_test_3

			exec_integer_test_1
			exec_integer_test_2
			exec_integer_test_3

			exec_float_test_1
			exec_float_test_2
			exec_float_test_3
			exec_float_test_4
			exec_float_test_5
			exec_float_test_6
			print ("** Setting decimal separator to ','%N")
			printf_fmt_funcs.set_decimal_separator (',')
			exec_float_test_1
			exec_float_test_2
			exec_float_test_3
			exec_float_test_4
			exec_float_test_6
			print ("** Resetting decimal separator to '.'%N")
			printf_fmt_funcs.reset_decimal_separator

			exec_binary_test_1
			exec_binary_test_2

			exec_octal_test_1
			exec_octal_test_2
			exec_octal_test_3
			exec_octal_test_4
			exec_octal_test_5
			exec_octal_test_6
			exec_octal_test_7

			exec_hex_test_1
			exec_hex_test_2
			exec_hex_test_3
			exec_hex_test_4
			exec_hex_test_5
			exec_hex_test_6
			exec_hex_test_7

			exec_char_test_1
			exec_char_test_2
			exec_char_test_3
			exec_char_test_4

			exec_unsigned_test_1
			exec_unsigned_test_2
			exec_unsigned_test_3
			exec_unsigned_test_4
			exec_unsigned_test_5

			exec_list_test_1
			exec_list_test_2
			exec_list_test_3
			exec_list_test_4
			exec_list_test_5
			exec_list_test_6
			exec_list_test_7

			exec_list_range_test_1

			exec_list_agent_test_1
			exec_list_agent_test_2
			exec_list_agent_test_3

			exec_matrix_test_1
			exec_matrix_test_2
			exec_matrix_test_3
			exec_matrix_test_4

			exec_matrix_range_test_1

			exec_hash_table_test_1
			exec_hash_table_agent_test_1
			exec_hash_table_agent_test_2
			exec_hash_table_agent_test_3
			exec_hash_table_agent_test_4

			exec_complex_test_1
			exec_complex_test_2
			exec_complex_test_3

			exec_complex_test_1e
			exec_complex_test_2e
			exec_complex_test_3e
			exec_complex_test_4e
			exec_complex_test_5e

			exec_agent_test_1
			exec_agent_test_2

			exec_solo_test_1
			exec_solo_test_2

			exec_verbatim_test_1

			exec_dumptest_1

			exec_scan_test_1

			report_results
		end

--|========================================================================
feature -- Execution
--|========================================================================

	exec_arg_count_test_1
		local
			ta: ARRAY [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			test_name := "Arg count test 1"
			desc := "Single integer arg in fmt; 0 and more args"
			fmt := "*%%d*"
			v1 := 99
			ok_out := "*99*"
			ta := << v1 >>
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)

			error_expected := True
			t_out := aprintf (fmt, <<v1, v2, v3>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, << >>)
			record_result (fmt, t_out, "*Void*", test_name, desc, 3)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			error_expected := False
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_percent_char_test_1
		local
			ta: ARRAY [INTEGER_32]
			tlist: LINKED_LIST [INTEGER_32]
			v1: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			test_name := "Percent char test 1"
			desc := "Literal percent, single int fmt and argument"
			fmt := "%%d%%"
			v1 := 99
			ok_out := "99%%"
			ta := << v1 >>
			create tlist.make
			tlist.extend (v1)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tlist)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)

			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void%%", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_percent_char_test_2
		local
			ta: ARRAY [INTEGER_32]
			tlist: LINKED_LIST [INTEGER_32]
			v1: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			test_name := "Percent char test 2"
			desc := "Literal percent as next-to-last char"
			fmt := "%%d%% "
			v1 := 99
			ok_out := "99%% "
			ta := << v1 >>
			create tlist.make
			tlist.extend (v1)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tlist)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			-- TODO is it better to assume that a fmt param with no 
			-- args, or Void args, is not a format param, or a error?
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)

			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void%% ", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_percent_char_test_3
		local
			test_name, fmt, t_out: STRING
			desc: STRING
		do
			test_name := "Percent char test 3"
			desc := "Percent followed by non-formatting characters"
			fmt := "%%a%%A%%C%%D%%e%%E%%F%%G%%g%%h%%H%%I%%i%%J%%j%%k%%K%%l%%m%%M"
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 1)
			fmt := "%%n%%O%%P%%p%%q%%Q%%R%%r%%S%%t%%U%%v%%V%%w%%W%%X%%y%%Y%%z%%Z"
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 2)
			fmt := "%%0%%1%%2%%3%%4%%5%%6%%7%%8%%9%%0"
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 3)
			fmt := "%%!%%@%%#%%$%%^%%&%%*%%(%%)%%-%%_%%+%%=%%{%%}%%[%%]%%|%%\"
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 4)
			fmt := "%%:%%;%%<%%>%%,%%.%%?%%/%%~%%`"
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_any_test_1
		local
			ta: ARRAY [detachable STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts, t_out: STRING
			desc: STRING
		do
			test_name := "ANY test 1"
			desc := "Single STRING argument, '%%a' format"
			fmt := "%%a"
			ok_out := "foo"
			ts := "foo"
			create tl.make
			ta := {ARRAY [STRING]} << ts >>
			tl.extend (ts)
if false then
			t_out := aprintf (fmt, ts)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<ts>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [ts])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
end
			-- TODO what does 'any' mean when there is an array of 
			-- items?  In this test, the array is an arg list and the 
			-- arg is 'ts', so should NOT cause an error
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			-- TODO is it better to assume that a fmt param with no 
			-- args, or Void args, is not a format param, or a error?
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)

			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_any_test_2
		local
			ta: ARRAY [INTEGER]
			tl: LINKED_LIST [INTEGER]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
			v: INTEGER
		do
			test_name := "ANY test 2"
			desc := "Single INTEGER argument, '%%a' format"
			fmt := "%%a"
			ok_out := "123"
			v := 123
			create tl.make
			ta := {ARRAY [INTEGER]} << v >>
			tl.extend (v)
			t_out := aprintf (fmt, v)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_any_test_3
		local
			ta: ARRAY [DOUBLE]
			tl: LINKED_LIST [DOUBLE]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
			v: DOUBLE
		do
			test_name := "ANY test 2"
			desc := "Single DOUBLE argument, '%%a' format"
			fmt := "%%a"
			ok_out := "123.456"
			v := 123.456
			create tl.make
			ta := {ARRAY [DOUBLE]} << v >>
			tl.extend (v)
			t_out := aprintf (fmt, v)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_string_test_2
		local
			ta: ARRAY [STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "String test 2"
			fmt := "*%%-10s*"
			ok_out := "*foo       *"
			ts := "foo"
			--create ta.make (0, 1)
			create tl.make
			--ta.put (ts, 1)
			ta := << ts >>
			tl.extend (ts)
			t_out := aprintf (fmt, ts)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<ts>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [ts])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "*Void      *", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_string_test_3
		local
			ta: ARRAY [STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "String test 3"
			fmt := "*%%10s*"
			ok_out := "*       foo*"
			ts := "foo"
			--create ta.make (0, 1)
			create tl.make
			--ta.put (ts, 1)
			ta := << ts >>
			tl.extend (ts)
			t_out := aprintf (fmt, ts)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<ts>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [ts])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "*      Void*", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_string_test_4
		local
			ta: ARRAY [STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "String test 4"
			fmt := "*%%=11s*"
			ok_out := "*    foo    *"
			ts := "foo"
			--create ta.make (0, 1)
			create tl.make
			--ta.put (ts, 1)
			ta := << ts >>
			tl.extend (ts)
			t_out := aprintf (fmt, ts)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<ts>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [ts])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "*   Void    *", test_name, desc, 7)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_string_test_5
		local
			ta: ARRAY [STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts1, ts2, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "String test 5"
			fmt := "*%%=5s*%%5s"
			ok_out := "* foo *" + Ks_alpha_upper
			ts1 := "foo"
			ts2 := Ks_alpha_upper
			ta := << ts1, ts2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<ts1, ts2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [ts1, ts2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
			error_expected := True
			t_out := aprintf (fmt, <<ts1, Void>>)
			record_result (fmt, t_out, "* foo * Void", test_name, desc, 6)
			t_out := aprintf (fmt, <<Void, ts2>>)
			record_result (fmt, t_out, "*Void *" + ts2, test_name, desc, 7)
			t_out := aprintf (fmt, <<Void, Void>>)
			record_result (fmt, t_out, "*Void * Void", test_name, desc, 8)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_integer_test_1
		local
			ta: ARRAY [INTEGER]
			tl: LINKED_LIST [INTEGER]
			v1: INTEGER
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Integer test 1"
			fmt := "*%%d*"
			v1 := 1492
			ok_out := "*" + v1.out +"*"
			ta := << v1 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_integer_test_2
		local
			ta: ARRAY [INTEGER]
			tl: LINKED_LIST [INTEGER]
			v1, v2: INTEGER
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Integer test 2"
			fmt := "*%%8d%%8d*"
			v1 := 1492
			v2 := 123456789
			ok_out := "*    " + v1.out + v2.out + "*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_integer_test_3
		local
			ta: ARRAY [INTEGER]
			tl: LINKED_LIST [INTEGER]
			v1, v2: INTEGER
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Decimal integer with decoration"
			test_name := "Integer test 3"
			fmt := "*%%#d*%%8d*"
			v1 := 123456789
			v2 := 123456789
			ok_out := "*123,456,789*" + v2.out + "*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_1
		local
			ta: ARRAY [REAL]
			tl: LINKED_LIST [REAL]
			v1, v2: REAL
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Values = 12345.6789, 0.123456789"
			test_name := "Float test 1"
			fmt := "*%%3.2f*%%3.2f"
			v1 := {REAL}12345.6789
			v2 := {REAL}0.123456789
			ok_out := "*12345" + pf_fmt_constants.decimal_separator.out + "68*0" + 
				pf_fmt_constants.decimal_separator.out + "12"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_2
		local
			ta: ARRAY [REAL_64]
			tl: LINKED_LIST [REAL_64]
			v1, v2: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Values = 12345.6789, 0.123456789"
			test_name := "Float test 2"
			fmt := "*%%12.4f*%%12.4f"
			v1 := 12345.6789
			v2 := 0.123456789
			ok_out := "*  12345" + pf_fmt_constants.decimal_separator.out + "6789*      0" +
				pf_fmt_constants.decimal_separator.out + "1235"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_3
		local
			ta: ARRAY [REAL_64]
			tl: LINKED_LIST [REAL_64]
			v1, v2: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Values = 1234567890.1234567890, 0.001234"
			test_name := "Float test 3"
			fmt := "*%%22.10f*%%10.6f"
			v1 := 1234567890.1234567890
			v2 := 0.001234
			-- NOTE WELL: fractional part of a DOUBLE can have no
			-- more than 24 bits of precision;  this translates to
			-- 7 decimal places.  All digits after the 7th will be '0'
			ok_out := "* 1234567890" + pf_fmt_constants.decimal_separator.out + 
				"1234567000*  0" + pf_fmt_constants.decimal_separator.out + "001234"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
--  Float test 3.1 -
--    Format   [*%22.10f*%10.6f]
--    Expected [* 1234567890.1234567890*  0.001234]
--    Actual   [* 1234567890.1234567000*  0.001234]
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_4
		local
			ta: ARRAY [REAL_64]
			tl: LINKED_LIST [REAL_64]
			v1, v2: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Values = 1_000_000_000_000_000.10, 1 / 1_000_000_000_000_000"
			test_name := "Float test 4"
			fmt := "*%%21.2f*%%20.17f"
			v1 := 1_000_000_000_000_000.10
			v2 := 1 / 1_000_000_000_000_000
			ok_out := "*  1000000000000000" +
				pf_fmt_constants.decimal_separator.out + 
				"10* 0" + pf_fmt_constants.decimal_separator.out +
				"00000000000000100"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_5
		local
			ta: ARRAY [REAL_64]
			tl: LINKED_LIST [REAL_64]
			v1, v2: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Whole numbers in float format"
			test_name := "Float test 5"
			fmt := "*%%9.2f*%%9.2f*"
			v1 := 150
			v2 := 15000
			ok_out := "*   150.00* 15000.00*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_float_test_6
		local
			ta: ARRAY [REAL_64]
			tl: LINKED_LIST [REAL_64]
			v1, v2: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Partial float format"
			test_name := "Float test 6"
			fmt := "*%%.2f*%%9.2f*"
			v1 := 199.95
			v2 := 1.23456789
			ok_out := "*199"+ pf_fmt_constants.decimal_separator.out + "95*     1" +
				pf_fmt_constants.decimal_separator.out + "23*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_binary_test_1
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Decorated, zero-filled"
			test_name := "Binary test 1"
			fmt := "*%%#08b*%%#08b"
			v1 := 19
			v2 := 123456789
			ok_out := "*0010011b*111"+"01011011"+"11001101"+"00010101b"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_binary_test_2
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Undecorated, zero-filled"
			test_name := "Binary test 2"
			fmt := "*%%08b*%%08b"
			v1 := 19
			v2 := 123456789
			ok_out := "*00010011*111"+"01011011"+"11001101"+"00010101"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_1
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Shorter and longer than field width, no decoration"
			test_name := "Octal test 1"
			fmt := "*%%8o*%%8o"
			v1 := 19
			v2 := 123456789
			ok_out := "*      23*726746425"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_2
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Octal test 2"
			fmt := "*%%#8o*%%#8o"
			v1 := 19
			v2 := 123456789
			ok_out := "*     023*0726746425"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_3
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "64 bit integer, zero-fill"
			test_name := "Octal test 3"
			fmt := "*%%#08o*%%#08o"
			v1 := 19
			v2 := 123456789
			ok_out := "*00000023*0726746425"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_4
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "64 bit integer, wide zero-fill"
			test_name := "Octal test 4"
			fmt := "*%%#024o*%%#024o"
			v1 := -19 -- 1777777777777777777755
			v2 := 12345678901 -- 133767016065
			ok_out := "*001777777777777777777755*000000000000133767016065"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_5
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "32 bit integer, default decoration"
			test_name := "Octal test 5"
			fmt := "*%%#16o*%%#16o"
			v1 := -19 -- 37777777755
			v2 := 123456789 -- 726746425
			ok_out := "*    037777777755*      0726746425"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_6
		local
			ta: ARRAY [INTEGER_8]
			tl: LINKED_LIST [INTEGER_8]
			v1, v2: INTEGER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "8 bit integer, default decoration"
			test_name := "Octal test 6"
			fmt := "*%%#6o*%%#6o"
			v1 := -19 -- 355
			v2 := 123 -- 173
			ok_out := "*  0355*  0173"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_octal_test_7
		local
			ta: ARRAY [INTEGER_16]
			tl: LINKED_LIST [INTEGER_16]
			v1, v2: INTEGER_16
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "16 bit integer, default decoration"
			test_name := "Octal test 7"
			fmt := "*%%#10o*%%#10o"
			v1 := -19 -- 177755
			v2 := 12345 -- 30071
			ok_out := "*   0177755*    030071"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_boolean_test_1
		local
			ta: ARRAY [BOOLEAN]
			tl: LINKED_LIST [BOOLEAN]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Boolean values, default representation"
			test_name := "Boolean test 1"
			fmt := "*%%B*%%B"
			ok_out := "*True*False"
			ta := << True, False >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<True, False>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [True, False])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_boolean_test_2
		local
			ta: ARRAY [BOOLEAN]
			tl: LINKED_LIST [BOOLEAN]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Boolean values, lower-case"
			test_name := "Boolean test 2"
			fmt := "*%%B*%%B"
			ok_out := "*true*false"
			ta := << True, False >>
			create tl.make
			tl.fill (ta)
			set_printf_bool_out_lower
			t_out := aprintf (fmt, <<True, False>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [True, False])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
			reset_printf_bool_out
		end

	--|--------------------------------------------------------------

	exec_boolean_test_3
		local
			ta: ARRAY [BOOLEAN]
			tl: LINKED_LIST [BOOLEAN]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Boolean values, Yes/No representation"
			test_name := "Boolean test 3"
			fmt := "*%%B*%%B"
			ok_out := "*Yes*No"
			ta := << True, False >>
			create tl.make
			tl.fill (ta)
			set_printf_bool_out ("Yes", "No")
			t_out := aprintf (fmt, <<True, False>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [True, False])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)

			reset_printf_bool_out

			-- Test the reset function
			ok_out := "*True*False"
			t_out := aprintf (fmt, <<True, False>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_hex_test_1
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 1"
			fmt := "*%%x*"
			v1 := 1492
			ok_out := "*5d4*"
			ta := << v1 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

--|--------------------------------------------------------------

	exec_hex_test_2
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 2"
			fmt := "*%%8x%%8x*"
			v1 := 1492 -- 5d4
			v2 := 123456789 -- 75bcd15
			ok_out := "*     5d4 75bcd15*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hex_test_3
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 3"
			fmt := "*%%#10x%%#10x*"
			v1 := 1492 -- 5d4
			v2 := 123456789 -- 75bcd15
			ok_out := "*     0x5d4 0x75bcd15*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hex_test_4
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 4"
			fmt := "*%%#20x%%#20x*"
			v1 := -1492 -- fffffffffffffa2c
			v2 := 123456789 -- 75bcd15
			ok_out := "*          0xfffffa2c           0x75bcd15*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hex_test_5
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 5"
			fmt := "*%%#20x%%#20x*"
			v1 := -1492 -- fffffffffffffa2c
			v2 := 12_345_678_901 -- 2dfdc1c35
			ok_out := "*  0xfffffffffffffa2c         0x2dfdc1c35*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hex_test_6
		local
			ta: ARRAY [INTEGER_8]
			tl: LINKED_LIST [INTEGER_8]
			v1, v2: INTEGER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 6"
			fmt := "*%%#10x%%#10x*"
			v1 := -109 -- 93
			v2 := 123 -- 7b
			ok_out := "*      0x93      0x7b*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hex_test_7
		local
			ta: ARRAY [INTEGER_16]
			tl: LINKED_LIST [INTEGER_16]
			v1, v2: INTEGER_16
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := ""
			test_name := "Hex test 7"
			fmt := "*%%#12x%%#12x*"
			v1 := -1090 -- fbbe
			v2 := 12345 -- 3039
			ok_out := "*      0xfbbe      0x3039*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_char_test_1
		local
			ta: ARRAY [CHARACTER_8]
			tl: LINKED_LIST [CHARACTER_8]
			v1, v2, v3: CHARACTER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Multiple chars in line"
			test_name := "Char test 1"
			fmt := "*%%c%%c%%c*"
			v1 := 'a'
			v2 := 'b'
			v3 := 'c'
			ok_out := "*abc*"
			ta := << v1, v2, v3 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2, v3>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2, v3])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_char_test_2
		local
			ta: ARRAY [CHARACTER_8]
			tl: LINKED_LIST [CHARACTER_8]
			v1, v2, v3: CHARACTER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Non-printable characters"
			test_name := "Char test 2"
			fmt := "*%%c%%c%%c*"
			v1 := ''
			v2 := ''
			v3 := ''
			ok_out := "**"
			ta := << v1, v2, v3 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2, v3>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2, v3])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_char_test_3
		local
			ta: ARRAY [CHARACTER_8]
			tl: LINKED_LIST [CHARACTER_8]
			v1, v2, v3: CHARACTER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Backspace and underline"
			test_name := "Char test 3"
			fmt := "*%%c%%c%%c*"
			v1 := 'a'
			v2 := ''
			v3 := '_'
			ok_out := "*a_*"
			ta := << v1, v2, v3 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2, v3>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2, v3])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_char_test_4
		local
			ta: ARRAY [CHARACTER_32]
			tl: LINKED_LIST [CHARACTER_32]
			v1, v2, v3: CHARACTER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Character 32 basic"
			test_name := "Char test 4"
			fmt := "*%%c%%c%%c*"
			v1 := 'a'
			v2 := 'b'
			v3 := 'c'
			ok_out := "*abc*"
			ta := << v1, v2, v3 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<v1, v2, v3>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [v1, v2, v3])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_unsigned_test_1
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Single positive INT 32 arg"
			test_name := "Unsigned test 1"
			fmt := "*%%u*"
			v1 := 1492
			ok_out := "*1492*"
			ta := << v1 >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<v1>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_unsigned_test_2
		local
			ta: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "2 INT 32 args; One positive, one negative"
			test_name := "Unsigned test 2"
			fmt := "*%%u*%%u*"
			v1 := 1492
			v2 := -1492 -- 4294965804
			ok_out := "*1492*4294965804*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			error_expected := True
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, "*1492*Void*", test_name, desc, 1)
			error_expected := False

			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_unsigned_test_3
		local
			ta: ARRAY [INTEGER_64]
			tl: LINKED_LIST [INTEGER_64]
			v1, v2: INTEGER_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "2 INT 64 args; One small, one large"
			test_name := "Unsigned test 3"
			fmt := "*%%u*%%u*"
			v1 := 123
			v2 := 4_294_965_804
			ok_out := "*123*4294965804*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			error_expected := True
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, "*123*Void*", test_name, desc, 1)
			error_expected := False

			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_unsigned_test_4
		local
			ta: ARRAY [INTEGER_8]
			tl: LINKED_LIST [INTEGER_8]
			v1, v2: INTEGER_8
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "2 INT 8 args; One positive, one negative"
			test_name := "Unsigned test 4"
			fmt := "*%%u*%%u*"
			v1 := 123
			v2 := -123
			ok_out := "*123*133*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			error_expected := True
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, "*123*Void*", test_name, desc, 1)
			error_expected := False

			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_unsigned_test_5
		local
			ta: ARRAY [INTEGER_16]
			tl: LINKED_LIST [INTEGER_16]
			v1, v2: INTEGER_16
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "2 INT 16 args; One positive, one negative"
			test_name := "Unsigned test 5"
			fmt := "*%%u*%%u*"
			v1 := 123
			v2 := -123
			ok_out := "*123*65413*"
			ta := << v1, v2 >>
			create tl.make
			tl.fill (ta)
			error_expected := True
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, "*123*Void*", test_name, desc, 1)
			error_expected := False

			t_out := aprintf (fmt, <<v1, v2>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_complex_test_1
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex for list of integers, comma sep, w/ index"
			test_name := "Complex test 1"
			fmt := "%%C(#,L[%%03d: %%d]$IV)"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
001: 123,
002: 456,
003: 789
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)
			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_complex_test_2
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v21, v22, v23: INTEGER_32
			--tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex for 2D array of integers, comma sep, w/ r,c"
			test_name := "Complex test 2"
			fmt := "%%C(,M[%%d (%%d,%%d)]$VRC)"
			v11 := 11
			v12 := 12
			v13 := 13
			v21 := 21
			v22 := 22
			v23 := 23
			ok_out := "{
11 (1,1),12 (1,2),13 (1,3)
21 (2,1),22 (2,2),23 (2,3)
}"
			create vm.make_filled (0, 2, 3)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			va := << v11, v12, v13, v21, v22, v23 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_complex_test_3
		local
			vht: HASH_TABLE [INTEGER, STRING]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [HASH_TABLE [INTEGER, STRING]]
			tl: LINKED_LIST [HASH_TABLE [INTEGER, STRING]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex w/ hash table of integers by string, w/ key"
			test_name := "Complex test 3"
			fmt := "%%C(,H[ %"%%s%": %%04d]$KV)"

			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := " %"v1_key%": 0123, %"v2_key%": 0456, %"v3_key%": 0789"
			va := << v1, v2, v3 >>
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")
			ta := << vht >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vht >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vht])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_complex_test_1e
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex test, for container type tag error"
			test_name := "Complex test 1e"
			fmt := "%%C(#,x[%%03d: %%d]$IV)"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
%C(#,x[%03d: %d]$IV)  Invalid container type tag [x]
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)

			error_expected := True

			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_complex_test_2e
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex test, for argument tag error"
			test_name := "Complex test 2e"
			fmt := "%%C(#,L[%%03d: %%d]$IX)"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
%C(#,L[%03d: %d]$IX)  Invalid argument tag [X]
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)
			error_expected := True
			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_complex_test_3e
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex test, for argument list error"
			test_name := "Complex test 3e"
			fmt := "%%C(#,L[%%03d: %%d$IV)"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
%C(#,L[%03d: %d$IV)  Missing closing bracket
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)
			error_expected := True
			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_complex_test_4e
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex test, for closing paren error"
			test_name := "Complex test 4e"
			fmt := "%%C(#,L[%%03d: %%d]$IV"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
%C(#,L[%03d: %d]$IV  Missing closing paren
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)
			error_expected := True
			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_complex_test_5e
		local
			va: ARRAY [INTEGER_32]
			tl: LINKED_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Complex test, for missing open paren error"
			test_name := "Complex test 5e"
			fmt := "%%C#,L[%%03d: %%d]$IV)"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
%C#,L[** Arg Type Mismatch **: Void]$IV)
}"
			va := << v1, v2, v3 >>
			create tl.make
			tl.fill (va)
			error_expected := True
			t_out := aprintf (fmt, << tl >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [tl])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_matrix_test_1
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v21, v22, v23: INTEGER_32
			--tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Matrix using 2D array of integers, default sep"
			test_name := "Matrix test 1"
			fmt := "%%M"
			v11 := 11
			v12 := 12
			v13 := 13
			v21 := 21
			v22 := 22
			v23 := 23
			ok_out := "{
11 12 13
21 22 23
}"
			create vm.make_filled (0, 2, 3)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			va := << v11, v12, v13, v21, v22, v23 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_matrix_test_2
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v21, v22, v23: INTEGER_32
			--tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Matrix using 2D array of integers, comma sep"
			test_name := "Matrix test 2"
			fmt := "%%,M"
			v11 := 11
			v12 := 12
			v13 := 13
			v21 := 21
			v22 := 22
			v23 := 23
			ok_out := "{
11,12,13
21,22,23
}"
			create vm.make_filled (0, 2, 3)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			va := << v11, v12, v13, v21, v22, v23 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_matrix_test_3
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v21, v22, v23: INTEGER_32
			--tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Matrix using 2D array of integers, decorated (sp+nl-sep)"
			test_name := "Matrix test 3"
			fmt := "%%#M"
			v11 := 11
			v12 := 12
			v13 := 13
			v21 := 21
			v22 := 22
			v23 := 23
			ok_out := "{
11 
12 
13
21 
22 
23
}"
			create vm.make_filled (0, 2, 3)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			va := << v11, v12, v13, v21, v22, v23 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_matrix_test_4
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v21, v22, v23: INTEGER_32
			--tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Matrix using 2D array of integers, sp+nl sep, col-major"
			test_name := "Matrix test 4"
			fmt := "%%#-M"
			v11 := 11
			v12 := 12
			v13 := 13
			v21 := 21
			v22 := 22
			v23 := 23
			--  11  12  13
			--  21  22  23
			ok_out := "{
11 
21
12 
22
13 
23
}"
			create vm.make_filled (0, 2, 3)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			va := << v11, v12, v13, v21, v22, v23 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_matrix_range_test_1
		local
			vm: ARRAY2 [INTEGER_32]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v11, v12, v13, v14,
			v21, v22, v23, v24,
			v31, v32, v33, v34,
			v41, v42, v43, v44: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Matrix range, 2D array of integers, by rows"
			test_name := "Matrix range test 1"
			fmt := "%%2.3M"
			v11 := 11
			v12 := 12
			v13 := 13
			v14 := 14
			v21 := 21
			v22 := 22
			v23 := 23
			v24 := 24
			v31 := 31
			v32 := 32
			v33 := 33
			v34 := 34
			v41 := 41
			v42 := 42
			v43 := 43
			v44 := 44
			ok_out := "{
21 22 23 24
31 32 33 34
}"
			create vm.make_filled (0, 4, 4)
			vm.put (v11, 1, 1)
			vm.put (v12, 1, 2)
			vm.put (v13, 1, 3)
			vm.put (v14, 1, 4)
			vm.put (v21, 2, 1)
			vm.put (v22, 2, 2)
			vm.put (v23, 2, 3)
			vm.put (v24, 2, 4)
			vm.put (v31, 3, 1)
			vm.put (v32, 3, 2)
			vm.put (v33, 3, 3)
			vm.put (v34, 3, 4)
			vm.put (v41, 4, 1)
			vm.put (v42, 4, 2)
			vm.put (v43, 4, 3)
			vm.put (v44, 4, 4)
			va := <<
				v11, v12, v13, v14,
				v21, v22, v23, v24,
				v31, v32, v33, v34,
				v41, v42, v43, v44 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vm >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vm])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_hash_table_test_1
		local
			vht: HASH_TABLE [INTEGER, STRING]
			va: ARRAY [INTEGER_32]
			ta: ARRAY [HASH_TABLE [INTEGER, STRING]]
			tl: LINKED_LIST [HASH_TABLE [INTEGER, STRING]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Table using hash table of integers, default sep"
			test_name := "Hash Table test 1"
			fmt := "%%H"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "123 456 789"
			va := << v1, v2, v3 >>
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")
			ta := << vht >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << vht >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [vht])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 5)
		end

	--|--------------------------------------------------------------

	exec_hash_table_agent_test_1
		local
			vht: HASH_TABLE [INTEGER_32, STRING]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Hash Table with per-item List (not table) agent"
			test_name := "Hash table agent test 1"
			fmt := "*[%%~L]*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*[123456789]*"
			reset_default_printf_list_delimiter
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")

			t_out := aprintf (fmt, <<[vht, agent echo_table_item]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_hash_table_agent_test_2
		local
			vht: HASH_TABLE [REAL_32, STRING]
			v1, v2, v3: REAL_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Hash Table with per-item List agent, real item and table"
			test_name := "Table agent test 2"
			fmt := "*[%%~L]*"
			v1 := 12.3
			v2 := 45.6
			v3 := 789.1
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")
			ok_out := "*[12.30 (v1_key), 45.60 (v2_key), 789.10 (v3_key)]*"
			reset_default_printf_list_delimiter
			t_out := aprintf (
				fmt, <<[vht, agent echo_table_item_table (?, vht, ?)]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_hash_table_agent_test_3
		local
			vht: HASH_TABLE [INTEGER_32, STRING]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Hash Table with per-item Table agent"
			test_name := "Hash table agent test 3"
			fmt := "*[%%~H]*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*[123456789]*"
			reset_default_printf_list_delimiter
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")

			t_out := aprintf (fmt, <<[vht, agent echo_table_item]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_hash_table_agent_test_4
		local
			vht: HASH_TABLE [REAL_32, STRING]
			v1, v2, v3: REAL_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Hash Table with per-item Table agent, real item and table"
			test_name := "Table agent test 4"
			fmt := "*[%%~H]*"
			v1 := 12.3
			v2 := 45.6
			v3 := 789.1
			create vht.make (3)
			vht.extend (v1, "v1_key")
			vht.extend (v2, "v2_key")
			vht.extend (v3, "v3_key")
			ok_out := "*[12.30 (v1_key), 45.60 (v2_key), 789.10 (v3_key)]*"
			reset_default_printf_list_delimiter
			t_out := aprintf (
				fmt, <<[vht, agent echo_table_item_table (?, vht, ?)]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_list_test_1
		local
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			tp: TUPLE [INTEGER_32, INTEGER_32, INTEGER_32]
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List using array of integers, default sep"
			test_name := "List test 1"
			fmt := "%%L"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "123 456 789"
			va := << v1, v2, v3 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << va >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [va])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			tp := [v1, v2, v3]
			-- List does not accept a TUPLE, must be CONTAINER
			--RFO TODO, mismatch is handle more benignly, output 
			--tuple.out (its tagged out form)
			error_expected := True
			t_out := aprintf (fmt, <<tp>>)
			ok_out := "** Arg Type Mismatch **" + tp.out
			record_result (fmt, t_out, ok_out, test_name, desc, 7)
			error_expected := False

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 8)
		end

	--|--------------------------------------------------------------

	exec_list_test_2
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List using 2-way list of integers, default sep"
			test_name := "List test 2"
			fmt := "*%%L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*123 456 789*"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << VL >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_list_test_3
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List, w/ comma and NL separator using 2-way list of integers"
			test_name := "List test 3"
			fmt := "*%%#,L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
*123,
456,
789*
}"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<VL>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_list_test_4
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List, comma sep, w/o decoratio; 2-way list of integers"
			test_name := "List test 4"
			fmt := "*%%,L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*123,456,789*"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<VL>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_list_test_5
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List, #, no sep; 2-way list of integers"
			test_name := "List test 5"
			fmt := "*%%#L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "{
*123
456
789*
}"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<VL>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_list_test_6
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List, no sep, no #; 2-way list of integers"
			test_name := "List test 6"
			fmt := "*%%L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*123 456 789*"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, <<VL>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)
		end

	--|--------------------------------------------------------------

	exec_list_test_7
		local
			VL: TWO_WAY_LIST [INTEGER_32]
			ta: ARRAY [TWO_WAY_LIST [INTEGER_32]]
			tl: LINKED_LIST [TWO_WAY_LIST [INTEGER_32]]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List, custom dflt sep; 2-way list of integers"
			test_name := "List test 7"
			fmt := "*%%L*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*123->456->789*"
			create VL.make
			VL.fill (<< v1, v2, v3 >>)
			ta := << VL >>
			create tl.make
			tl.fill (ta)
			set_default_printf_list_delimiter ("->")
			t_out := aprintf (fmt, <<VL>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, << <<v1, v2, v3>> >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [VL])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)

			reset_default_printf_list_delimiter
		end

	--|--------------------------------------------------------------

	exec_list_range_test_1
		local
			va: ARRAY [INTEGER_32]
			ta: ARRAY [ARRAY [INTEGER_32]]
			tl: LINKED_LIST [ARRAY [INTEGER_32]]
			v1, v2, v3, v4, v5, v6: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Range of indices in List, default sep"
			test_name := "List range test 1"
			fmt := "%%2.4L"
			v1 := 123
			v2 := 456
			v3 := 789
			v4 := 1123
			v5 := 1456
			v6 := 1789
			ok_out := "456 789 1123"
			va := << v1, v2, v3, v4, v5, v6 >>
			ta := << va >>
			create tl.make
			tl.fill (ta)
			t_out := aprintf (fmt, << va >>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, [va])
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
		end

	--|--------------------------------------------------------------

	exec_list_agent_test_1
		local
			vlist: TWO_WAY_LIST [INTEGER_32]
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List with per-item agent"
			test_name := "List agent test 1"
			fmt := "*[%%~L]*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*[123456789]*"
			reset_default_printf_list_delimiter
			create vlist.make
			vlist.fill (<< v1, v2, v3 >>)
			t_out := aprintf (fmt, <<[vlist, agent echo_list_item]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_list_agent_test_2
		local
			vlist: TWO_WAY_LIST [REAL_64]
			v1, v2, v3: REAL_64
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List with per-item agent, real"
			test_name := "List agent test 2"
			fmt := "*[%%~L]*"
			v1 := 12.3
			v2 := 45.6
			v3 := 789.1
			ok_out := "*[12.3045.60789.10]*"
			reset_default_printf_list_delimiter
			create vlist.make
			vlist.fill (<< v1, v2, v3 >>)
			t_out := aprintf (fmt, <<[vlist, agent echo_list_item_real]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_list_agent_test_3
		local
			vlist: TWO_WAY_LIST [REAL_32]
			v1, v2, v3: REAL_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "List with per-item agent, real item and list"
			test_name := "List agent test 3"
			fmt := "*[%%~L]*"
			v1 := 12.3
			v2 := 45.6
			v3 := 789.1
			ok_out := "*[12.30, 45.60, 789.10]*"
			reset_default_printf_list_delimiter
			create vlist.make
			vlist.fill (<< v1, v2, v3 >>)
			t_out := aprintf (
				fmt, <<[vlist, agent echo_list_item_list (?, vlist, ?)]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_agent_test_1
		local
			v1, v2, v3: STRING
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Agent with TUPLE [STRING, FUNCTION] arg"
			test_name := "Agent test 1"
			fmt := "*%%~A*"
			v1 := "123"
			v2 := "456"
			v3 := "789"
			ok_out := "*-->123<--*"
			reset_default_printf_list_delimiter
			t_out := aprintf (fmt, <<[v1, agent echo_any]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_agent_test_2
		local
			v1, v2, v3: INTEGER_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Agent with integer arg"
			test_name := "Agent test 2"
			fmt := "*%%~A*"
			v1 := 123
			v2 := 456
			v3 := 789
			ok_out := "*-->123<--*"
			reset_default_printf_list_delimiter
			t_out := aprintf (fmt, <<[v1, agent echo_any]>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
		end

	--|--------------------------------------------------------------

	exec_solo_test_1
		local
			vlist: TWO_WAY_LIST [STRING]
			v1: STRING
			v2: INTEGER_32
			v3: REAL_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Single arg, no list"
			test_name := "Solo test 1"
			fmt := "*%%s*"
			v1 := "foo"
			v2 := 123
			v3 := {REAL_32}5.7
			ok_out := "*foo*"
			create vlist.make
			vlist.extend (v1)
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			error_expected := True
			t_out := aprintf (fmt, v2)
			record_result (fmt, t_out, "*** Arg Type Mismatch ***", test_name, desc, 2)
			error_expected := False

			t_out := aprintf (fmt, vlist)
			record_result (fmt, t_out, ok_out, test_name, desc, 3)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 4)
		end

	--|--------------------------------------------------------------

	exec_solo_test_2
		local
			v1: INTEGER_32
			v2: STRING
			v3: REAL_32
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Single int arg, no list"
			test_name := "Solo test 2"
			fmt := "*%%d*"
			v1 := 123
			v2 := "foo"
			v3 := {REAL_32}5.7
			ok_out := "*123*"
			t_out := aprintf (fmt, v1)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)

			error_expected := True
			t_out := aprintf (fmt, v2)
			record_result (fmt, t_out, "*** Arg Type Mismatch ***", test_name, desc, 2)
			t_out := aprintf (fmt, v3)
			record_result (fmt, t_out, "*** Arg Type Mismatch ***", test_name, desc, 3)
			error_expected := False

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 4)
		end

	--|--------------------------------------------------------------

	exec_verbatim_test_1
		local
			vlist: TWO_WAY_LIST [ANY]
			va: ARRAY [ANY]
			v1: INTEGER_32
			v2: STRING
			test_name, fmt, ok_out, t_out, void_out: STRING
			desc: STRING
		do
			desc := "Verbatim format, 1 int, 1 string arg, literal pct"
			test_name := "Verbatim test 1"
			fmt := "{
This is a verbatim string test
where the format string is a
  "Verbatim"
string.  The format also includes
an integer arg (%d),
a string arg (%s),
and a literal percent sign (%)
}"
			v1 := 123
			v2 := "foo"
			va := << v1, v2 >>
			create vlist.make
			vlist.fill (va)
			ok_out := "{
This is a verbatim string test
where the format string is a
  "Verbatim"
string.  The format also includes
an integer arg (123),
a string arg (foo),
and a literal percent sign (%)
}"
			void_out := "{
This is a verbatim string test
where the format string is a
  "Verbatim"
string.  The format also includes
an integer arg (Void),
a string arg (Void),
and a literal percent sign (%)
}"
			t_out := aprintf (fmt, va)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, vlist)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [v1, v2])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 4)

			error_expected := True
			t_out := aprintf (fmt, [])
			record_result (fmt, t_out, void_out, test_name, desc, 5)
			error_expected := False
		end

	--|--------------------------------------------------------------

	exec_dumptest_1
		local
			--vlist: TWO_WAY_LIST [ANY]
			--va: ARRAY [ANY]
			--v1: INTEGER_32
			--v2: STRING
			test_name, fmt, ok_out, t_out: STRING
			desc: STRING
		do
			desc := "Hex dump, default format, string"
			test_name := "Hex dump test 1"
			fmt := "{
This is a hex dump string test
where the string includes "Hex dump 0xmumble".  The format is default (16, w/ ascii, decimal addrs)
}"
			ok_out := "{

00000000    5468 6973  2069 7320  6120 6865  7820 6475   |This is a hex du|
00000016    6D70 2073  7472 696E  6720 7465  7374 0A77   |mp string test w|
00000032    6865 7265  2074 6865  2073 7472  696E 6720   |here the string |
00000048    696E 636C  7564 6573  2022 4865  7820 6475   |includes "Hex du|
00000064    6D70 2030  786D 756D  626C 6522  2E20 2054   |mp 0xmumble".  T|
00000080    6865 2066  6F72 6D61  7420 6973  2064 6566   |he format is def|
00000096    6175 6C74  2028 3136  2C20 772F  2061 7363   |ault (16, w/ asc|
00000112    6969 2C20  6465 6369  6D61 6C20  6164 6472   |ii, decimal addr|
00000128    7329                                         |s)              |

}"
			t_out := axdump (fmt, "", 0, 0, 0, 0)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)

			desc := "Hex dump, wide format, w/ ascii, decimal addrs, string"
			test_name := "Hex dump test 2"
			fmt := "{
This is a hex dump string test
where the string includes "Hex dump 0xmumble".  The format is wide (64, w/ ascii, decimal addrs)
}"
			ok_out := "{

00000000  5468 6973 2069 7320  6120 6865 7820 6475  6D70 2073 7472 696E  6720 7465 7374 0A77  6865 7265 2074 6865  2073 7472 696E 6720  696E 636C 7564 6573  2022 4865 7820 6475 |This is a hex dump string test where the string includes "Hex du|
00000064  6D70 2030 786D 756D  626C 6522 2E20 2054  6865 2066 6F72 6D61  7420 6973 2077 6964  6520 2836 342C 2077  2F20 6173 6369 692C  2064 6563 696D 616C  2061 6464 7273 29   |mp 0xmumble".  The format is wide (64, w/ ascii, decimal addrs) |

}"
			t_out := axdump (fmt, "w", 0, 0, 0, 0)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
		end

	--|--------------------------------------------------------------

	exec_scan_test_1
		local
--RFO 			v1, v3: INTEGER_32
--RFO 			v2, buf: STRING
--RFO 			test_name, fmt, ok_out, t_out: STRING
--RFO 			desc: STRING
--RFO 			ra: ARRAY [detachable ANY]
		do
--RFO 			desc := "Scanf test"
--RFO 			test_name := "Scan test 1"
--RFO 			--fmt := "* %%*s %%d %%s %%d"
--RFO 			fmt := " %%s %%d %%s %%d"
--RFO 			buf := "This is a 1 in a 1000000 shot"
--RFO 			v1 := 123
--RFO 			v2 := "in a"
--RFO 			v3 := 1000000
--RFO 			--ok_out := "*123*"
--RFO 			ra := sscanf (buf, fmt)
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	echo_any (v: detachable ANY): STRING
		do
			if not attached v as av then
				Result := "Void"
			else
				Result := "-->" + av.out + "<--"
			end
		end

	echo_table_item (v: detachable ANY): STRING
		do
			create Result.make_empty
			if not attached v as av then
				Result.append ("Void")
			else
				Result.append (av.out)
			end
		end

	echo_table_item_table (v: detachable ANY; vl: HASH_TABLE [ANY, STRING]; p: INTEGER): STRING
		local
			ts: STRING
		do
			create Result.make_empty
			if not attached {REAL_32_REF} v as av then
				Result.append ("Void")
			elseif attached {HASH_TABLE[REAL_32,STRING]} vl as avl then
				if attached {STRING} avl.key_for_iteration as sk then
					ts := sk
				else
					ts := "non-string"
				end
				Result.append (aprintf ("%%2.2f (%%s)", << av, ts >>))
				if p < avl.iteration_upper then
					Result.append (", ")
				end
			end
		end

	echo_list_item (v: detachable ANY): STRING
		do
			create Result.make_empty
			if not attached v as av then
				Result.append ("Void")
			else
				Result.append (av.out)
			end
		end

	echo_list_item_real (v: detachable ANY): STRING
		do
			create Result.make_empty
			if not attached {REAL_64_REF} v as av then
				Result.append ("Void")
			else
				Result.append (aprintf ("%%2.2f", << av >>))
			end
		end

	echo_list_item_list (v: detachable ANY; vl: LIST [REAL_32]; p: INTEGER): STRING
		do
			create Result.make_empty
			if not attached {REAL_32_REF} v as av then
				Result.append ("Void")
			elseif attached vl as avl then
				Result.append (aprintf ("%%2.2f", << av >>))
--				if not avl.after then
				if p < avl.count then
					Result.append (", ")
				end
			end
		end

--|========================================================================
feature {NONE} -- Recording and reporting
--|========================================================================

	record_result (fmt, t_o, ok_o, t_name, desc: STRING; id: INTEGER)
			-- Record result of test ID='id' and name='t_name',
			-- comparing the test output 't_o' with the expected 
			-- output 'ok_o'
			-- Reset any recorded errors after recording result, to set 
			-- up for next test
		local
			ts, es, xs: STRING
			xf, ef: BOOLEAN
		do
--print (last_printf_errors.count.out + " errors recorded%N")
--print (printf_errors_out)
			xf := error_expected
			if attached last_printf_error as lpe then
				ef := True
				es := lpe.description
			else
				es := "None Reported"
			end
			if xf then
				xs := "Error expected"
			else
				xs := "None expected"
			end
			ts := aprintf ("%%s.%%d - %%s", <<t_name, id, desc>>)
			if t_o.is_equal (ok_o) and (ef = xf) then
				if show_expected_on_pass then
					ts := aprintf ("{
%s
  Format   [%s]
  Expected [%s]
  Error    [%s][%s]
}",
						<<ts, fmt, ok_o, xs, es>>)
				end
				tests_passed.extend (ts)
			else
				ts := aprintf ("{
%s
  Format   [%s]
  Expected [%s]
  Actual   [%s]
  Error    [%s][%s]
}",
					<<ts, fmt, ok_o, t_o, xs, es>>)
				tests_failed.extend (ts)
			end
			reset_printf_errors
		end

	--|--------------------------------------------------------------

	report_results
			-- Report collected results to console
			-- using non-printf routines
		do
			print ("Tests Failed:%N")
			print_list (tests_failed)
			print ("Tests Passed:%N")
			print_list (tests_passed)
			print ("Totals:%N")
			print ("  " + tests_passed.count.out + " Passed%N")
			print ("  " + tests_failed.count.out + " Failed%N")
		end

	--|--------------------------------------------------------------

	print_list (tl: like tests_passed)
			-- Write, to console, the contents of list 'tl'
			-- using non-printf routines
		do
			if tl.is_empty then
				print ("  NONE%N")
			else
				from tl.start
				until tl.exhausted
				loop
					print (tl.item )
					print ("%N")
					tl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	tests_passed: LINKED_LIST [STRING]
			-- Collection of tests that have passed

	tests_failed: LINKED_LIST [STRING]
			-- Collection of tests that have failed

	show_expected_on_pass: BOOLEAN
			-- should expected values be shown in report of passed 
			-- tests?

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	Ks_alpha_upper: STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			-- The very basic Latin alphabet, in upper case
			-- Used as a source of characters for tests

end -- class PRINTF_TEST
