deferred class PRINTF_TESTS

inherit
	PRINTF_TESTS_I

--|========================================================================
feature -- Execution
--|========================================================================

	exec_string_test_1
		local
			ta: ARRAY [detachable STRING]
			tl: LINKED_LIST [STRING]
			test_name, fmt, ok_out, ts, t_out: STRING
			desc: STRING
		do
			test_name := "String test 1"
			desc := "Single argument"
			fmt := "%%s"
			ok_out := "foo"
			ts := "foo"
			create tl.make
			ta := {ARRAY [STRING]} << ts >>
			tl.extend (ts)
			t_out := aprintf (fmt, ts)
			record_result (fmt, t_out, ok_out, test_name, desc, 1)
			t_out := aprintf (fmt, <<ts>>)
			record_result (fmt, t_out, ok_out, test_name, desc, 2)
			t_out := aprintf (fmt, [ts])
			record_result (fmt, t_out, ok_out, test_name, desc, 3)
			t_out := aprintf (fmt, ta)
			record_result (fmt, t_out, ok_out, test_name, desc, 4)
			t_out := aprintf (fmt, tl)
			record_result (fmt, t_out, ok_out, test_name, desc, 5)

			t_out := aprintf (fmt, Void)
			record_result (fmt, t_out, fmt, test_name, desc, 6)

			error_expected := True
			t_out := aprintf (fmt, <<Void>>)
			record_result (fmt, t_out, "Void", test_name, desc, 7)
			error_expected := False
		end

end -- class PRINTF_TESTS
