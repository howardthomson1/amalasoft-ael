deferred class PRINTF_TESTS_I
-- Template for tests in printf test suite
-- Supports different flavors of test for void-safety, for example

inherit
	AEL_PRINTF

 --|========================================================================
feature -- Tests
 --|========================================================================

	exec_arg_count_test_1
		deferred
		end

	exec_percent_char_test_1
		deferred
		end

	exec_percent_char_test_2
		deferred
		end

	exec_percent_char_test_3
		deferred
		end

	exec_string_test_1
		deferred
		end

	exec_string_test_2
		deferred
		end

	exec_string_test_3
		deferred
		end

	exec_string_test_4
		deferred
		end

	exec_string_test_5
		deferred
		end

	exec_integer_test_1
		deferred
		end

	exec_integer_test_2
		deferred
		end

	exec_integer_test_3
		deferred
		end

	--|--------------------------------------------------------------

	exec_float_test_1
		deferred
		end

	exec_float_test_2
		deferred
		end

	exec_float_test_3
		deferred
		end

	exec_float_test_4
		deferred
		end

	exec_float_test_5
		deferred
		end

	exec_binary_test_1
		deferred
		end

	exec_binary_test_2
		deferred
		end

	exec_octal_test_1
		deferred
		end

	exec_octal_test_2
		deferred
		end

	exec_octal_test_3
		deferred
		end

	exec_octal_test_4
		deferred
		end

	exec_octal_test_5
		deferred
		end

	exec_octal_test_6
		deferred
		end

	exec_octal_test_7
		deferred
		end

	exec_boolean_test_1
		deferred
		end

	exec_hex_test_1
		deferred
		end

	exec_hex_test_2
		deferred
		end

	exec_hex_test_3
		deferred
		end

	exec_hex_test_4
		deferred
		end

	exec_hex_test_5
		deferred
		end

	exec_hex_test_6
		deferred
		end

	exec_hex_test_7
		deferred
		end

	exec_char_test_1
		deferred
		end

	exec_char_test_2
		deferred
		end

	exec_char_test_3
		deferred
		end

	exec_char_test_4
		deferred
		end

	exec_unsigned_test_1
		deferred
		end

	exec_unsigned_test_2
		deferred
		end

	exec_unsigned_test_3
		deferred
		end

	exec_unsigned_test_4
		deferred
		end

	exec_unsigned_test_5
		deferred
		end

	exec_list_test_1
		deferred
		end

	exec_list_test_2
		deferred
		end

	exec_list_test_3
		deferred
		end

	exec_list_test_4
		deferred
		end

	exec_list_test_5
		deferred
		end

	exec_list_test_6
		deferred
		end

	exec_solo_test_1
		deferred
		end

	exec_solo_test_2
		deferred
		end

	exec_verbatim_test_1
		deferred
		end

--|========================================================================
feature {NONE} -- Recording and reporting
--|========================================================================

	record_result (fmt, t_o, ok_o, t_name, desc: STRING; id: INTEGER)
		deferred
		end

	--|--------------------------------------------------------------

	report_results
		deferred
		end

	--|--------------------------------------------------------------

	print_list (tl: like tests_passed)
		deferred
		end

	--|--------------------------------------------------------------

	tests_passed: LINKED_LIST [STRING]
		deferred
		end

	tests_failed: LINKED_LIST [STRING]
		deferred
		end

	show_expected_on_pass: BOOLEAN
		deferred
		end

	error_expected: BOOLEAN
			-- Does mose recent test expect to trig

end -- class PRINTF_TESTS_I
