deferred class AEL_QUERIABLE
-- Ability to support queries of self

inherit
	AEL_SPRT_ASSOCIATIVE

--|========================================================================
feature -- Query support
--|========================================================================

	matches_param (qp: AEL_QUERY_PARAM): BOOLEAN
			-- Does Current match query tag 'qt' and value 'qv'
			-- with sense 'qs' ?
			-- See detailed syntax in help_text
		require
			valid: qp /= Void and then qp.is_valid
			valid_label: is_valid_value_label (qp.tag)
		do
			Result := qp.value_matches (string_value_by_label (qp.tag))
		end

end -- class AEL_QUERIABLE
