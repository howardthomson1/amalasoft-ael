deferred class AEL_QUERIABLE_TABLE [G->AEL_QUERIABLE]
-- Abstract table with ability to support queries of contained items

inherit
	AEL_SPRT_MULTI_FALLIBlE
	AEL_QUERY_SUPPORT

--|========================================================================
feature -- Status
--|========================================================================

	queries: detachable AEL_QUERY
			-- List of query parameters constituting query

	query_range: detachable INTEGER_INTERVAL
			-- Range of matches to include in result of query

	item_query_tags: detachable LINKED_LIST [STRING]
			-- Individual tags extracted from query string
			-- (ie those not part of a tv pair)
			-- Query tags denote the attributes to include in the 
			-- output of each element matching a query

	has_query: BOOLEAN
			-- Does Current have any defined queries presently?
		do
			Result := attached queries as lq and then not lq.is_empty
		end

	has_query_range: BOOLEAN
			-- Does Current have a defined query range?
		do
			Result := query_range /= Void
		end

	has_item_query_tags: BOOLEAN
			-- Does Current have any tags relating to item attrs?
		do
			Result := attached item_query_tags as lqt
				and then not lqt.is_empty
		end

	--|--------------------------------------------------------------

	item_matches_query (v: G): BOOLEAN
			-- Does item 'v' match defined queries?
		do
			Result := True
			if attached queries as ql and then not ql.is_empty then
				-- Compare item's labeled feature values w/ those in the
				-- query string.  Item must match all query args
				Result := item_matches_queries (v, ql)
			end
		end

	--|--------------------------------------------------------------

	item_matches_queries (v: G; ql: like queries): BOOLEAN
			-- Does item 'v' match list of queries 'ql'?
		require
			exists: v /= Void
			queries_exist: ql /= Void
		local
			ti: AEL_QUERY_PARAM
			oc: CURSOR
		do
			if not attached ql then
				-- ERROR see precondition
			else
				Result := True
				oc := ql.cursor
				from ql.start
				until ql.exhausted or not Result
				-- continue on error, but report it
				loop
					ti := ql.item
					if not v.is_valid_value_label (ti.tag) then
						set_error_message ("Invalid value label in query")
					else
						Result := v.matches_param (ti)
					end
					ql.forth
				end
				ql.go_to (oc)
			end
		end

--|========================================================================
feature -- Traversal
--|========================================================================

	start
		deferred
		end

	forth
		deferred
		end

	after: BOOLEAN
		deferred
		end

	count: INTEGER
		deferred
		end

--|========================================================================
feature -- Access
--|========================================================================

	item_for_iteration: detachable G
		deferred
		end

	new_list_for_matches: LINKED_LIST [G]
			-- A newly created list into which matching items can be placed
		do
			create Result.make
		end

	new_sorted_list_for_matches (
		sp: STRING; rf, af: BOOLEAN): AEL_DS_LIST_AGENT_SORTED [G]
			-- A newly created list into which matching items can be 
			-- placed
			-- 'sp' is the attribute whose value to use for sorting
			-- 'rf' is the reverse-sort flag (reverse if True)
		do
			if attached query_range as lqr then
				create {AEL_DS_BOUNDED_LIST_AGENT_SORTED [G]}Result.make_bounded (
					agent item_compare (?,?,sp,rf,af), lqr.upper)
			else
				create Result.make (agent item_compare (?,?,sp,rf,af))
			end
		end

	--|--------------------------------------------------------------

	items_matching_query: like new_list_for_matches
			-- Items in Current that match active query
		local
			ql: like queries
			do_range: BOOLEAN
			sparam: detachable STRING
			is_reverse, is_alpha: BOOLEAN
		do
			if has_query then
				create ql.make
				ql.compare_objects
				if attached queries as lq then
					ql.fill (lq)
				end
				-- Extract from the queries the sort param, if any
				from ql.start
				until ql.exhausted or sparam /= Void
				loop
					if ql.item.is_sort then
						sparam := ql.item.value
						is_reverse := ql.item.is_reverse_sort
						is_alpha := ql.item.is_alpha_sort
						ql.remove
						ql.finish
					else
						ql.forth
					end
				end
				ql.start
			end

			if sparam /= Void then
				Result := new_sorted_list_for_matches (sparam, is_reverse, is_alpha)
			else
				Result := new_list_for_matches
			end
			if attached query_range as lqr
				and then (lqr.lower > 1 or lqr.upper <= count)
			 then
				 do_range := True
			end
			if not has_query then
				-- Can occur if sort was only param
				if do_range then
					fill_items_matching_nonvoid_range (Result)
				else
					-- Without any queries, all items match
					if attached {CONTAINER [G]} Current as tl then
						Result.fill (tl)
					else
					end
				end
			else
				-- Has either query only or query and range
				if do_range then
					-- Both
					fill_items_matching_nonvoid_query_and_range (Result, ql)
				else
					-- Query only
					fill_items_matching_nonvoid_query (Result, ql)
				end
			end
		end

--|========================================================================
feature -- Label-based comparison
--|========================================================================

	item_compare (v1, v2: G; lbl: STRING; rf, af: BOOLEAN): INTEGER
			-- If item 'v1' less than 'v2' then Result is negative
			-- If item 'v1' greater than 'v2' then Result is positive
			-- If item 'v1' and 'v2' are equal, then Result is 0
			--
			-- If 'rf', then reverse order ('v2' < 'v1)
			-- If 'af', then use alpha sort instead of default
		require
			items_exist: v1 /= Void and v2 /= Void
			valid_label: v1.is_valid_value_label (lbl)
		do
			if v1.is_less_by_value (v2, lbl, af) then
				Result := -1
			elseif v2.is_less_by_value (v1, lbl, af) then
				Result := 1
			end
			if rf then
				Result := 0 - Result
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	fill_items_matching_nonvoid_query (rl: LIST [G]; ql: like queries)
			-- Fill list 'rl' with items in Current that match active query
		require
			list_exists: rl /= Void
			queries_exist: ql /= Void
			has_query_only: has_query and not has_query_range
		do
			-- Compare item's labeled feature values w/ those in the
			-- query string.  Item must match all query args
			from start
			until after
			loop
				if attached item_for_iteration as ti then
					ti.update_values
					if item_matches_queries (ti, ql) then
						rl.extend (ti)
					end
				end
				forth
			end
		end

	--|--------------------------------------------------------------

	fill_items_matching_nonvoid_query_and_range (rl: LIST [G]; ql: like queries)
			-- Fill list 'rl' with items in Current that match active query
			-- In addition to value label matches, query also support
			-- range request (as in first 'n' items, 'nth' thru 'mth')
			-- Interpretation of range by itself is simple enough, with
			-- a start and end position, but interpretation of range when
			-- combined with other query params is another story.
			-- Does it mean "from the range n to m, the items that match 
			-- the other params", or does it mean "from all the items 
			-- that match the other params, the items n to m"?
			-- The purpose of the range query is to limit the amount of
			-- information per-request, with the abilty to ask for more
			-- the next time.  Assuming the information does not change
			-- between requests, it is reasonable to filter the number of
			-- items returned only after the other matches have 
			-- completed, except for resource consumption considerations
			-- on the server side.  To be more resource sensitive, it is 
			-- then necessary to check the range _during_ assembly of 
			-- the other matches.
		require
			list_exists: rl /= Void
			queries_exist: ql /= Void
			has_both: has_query and has_query_range
		local
			num_needed, num, first_idx, last_idx: INTEGER
		do
			-- Compare item's labeled feature values w/ those in the
			-- query string.  Item must match all query args
			if not attached query_range as lqr then
				-- ERROR see precondition
			else
				first_idx := lqr.lower
				last_idx := lqr.upper
				num_needed := lqr.count
				from start
				until after or rl.count >= num_needed
				loop
					if attached item_for_iteration as ti then
						num := num + 1
						ti.update_values
						if item_matches_queries (ti, ql) and then
							num >= first_idx and rl.count <= last_idx
						 then
							 rl.extend (ti)
						end
					end
					forth
				end
			end
		end

	--|--------------------------------------------------------------

	fill_items_matching_nonvoid_range (rl: LIST [G])
			-- Fill list 'rl' with items in Current that match active query range
			-- Interpretation of range by itself is simple enough, with
			-- a start and end position within all items in Current
		require
			list_exists: rl /= Void
			has_only_range: not has_query and has_query_range
		local
			num_needed, num, first_idx, last_idx: INTEGER
		do
			-- Compare item's labeled feature values w/ those in the
			-- query string.  Item must match all query args
			if not attached query_range as lqr then
				-- ERROR see precondition
			else
				first_idx := lqr.lower
				last_idx := lqr.upper
				num_needed := lqr.count
				from start
				until after or rl.count >= num_needed
				loop
					if attached item_for_iteration as ti then
						num := num + 1
						if num >= first_idx and rl.count <= last_idx then
							rl.extend (ti)
						end
					end
					forth
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_queries (v: like queries)
			-- Set the list of queries for Current to 'v'
		do
			queries := v
		end

	set_query_range (v: like query_range)
			-- Set the range of outputs to 'v'
		do
			query_range := v
		end

	set_item_query_tags (v: like item_query_tags)
			-- Set the tags of item attrs to include in outputs to 'v'
		do
			item_query_tags := v
		end

end -- class AEL_QUERIABLE_TABLE
