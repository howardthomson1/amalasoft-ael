class AEL_QUERY
-- A set of query parameters, the combination of which constitutes a 
-- query

inherit
	AEL_QUERY_SUPPORT
		undefine
			copy, is_equal, out
		end
	LINKED_LIST [AEL_QUERY_PARAM]
		redefine
			out
		end

create
	make, make_from_string, make_from_query_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_string (v: STRING)
			-- Create Current from string 'v'
			-- String has the form:
			--   <query_param>[&<query_param>]*
			-- Where:
			--   <query_param> ::= <tag>"="[<qs>][<value>]
		require
			valid: v /= Void and then v.has ('=')
		do
			make
			init_from_string (v)
		end

	make_from_query_string (v: AEL_QUERY_STRING)
			-- Create Current from query string 'v'
			-- String has the form:
			--   <query_param>[&<query_param>]*
			-- Where:
			--   <query_param> ::= <tag>"="[<qs>][<value>]
		require
			valid: v /= Void
		do
			make
			init_from_query_string (v)
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_string (v: STRING)
			-- Initialize Current from string 'v'
			-- String has the form:
			--   <query_param>[&<query_param>]*
			-- Where:
			--   <query_param> ::= <tag>"="[<qs>][<value>]
		require
			valid: v /= Void and then v.has ('=')
		local
			rawp: LIST [STRING]
			ti: STRING
			qp: like item
			has_error: BOOLEAN
		do
			wipe_out
			rawp := v.split ('&')
			rawp.start
			if rawp.item.is_empty then
				-- Skip over case of leading separator
				rawp.forth
			end
			from
			until rawp.exhausted or has_error
			loop
				ti := rawp.item
				if ti.has ('=') then
					create qp.make_from_string (ti)
					if qp.is_valid then
						extend (qp)
					else
						-- ERROR
						has_error := True
					end
				else
					-- ERROR
					has_error := True
				end
				rawp.forth
			end
		end

	--|--------------------------------------------------------------

	init_from_query_string (v: AEL_QUERY_STRING)
			-- Initialize Current from query string 'v'
			-- String has the form:
			--   <query_param>[&<query_param>]*
			-- Where:
			--   <query_param> ::= <tag>"="[<qs>][<value>]
		require
			valid: v /= Void
		local
			param_pairs: LIST [AEL_SPRT_TV_PAIR]
			tv: AEL_SPRT_TV_PAIR
			qp: like item
			has_error: BOOLEAN
		do
			wipe_out
			param_pairs := v.param_pairs.contents_sorted
			from param_pairs.start
			until param_pairs.exhausted or has_error
			loop
				tv := param_pairs.item
				create qp.make_from_string (tv.out)
				if qp.is_valid then
					extend (qp)
				else
					-- ERROR
					has_error := True
				end
				param_pairs.forth
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	out: STRING
			-- String representation of Current (as it would appear in a 
			-- URI, less the leading '/'
		do
			create Result.make (128)
			do_all (agent append_item_out (?, False, Result))
		end

	out_quoted: STRING
			-- String representation of Current (as it would appear in a 
			-- URI, less the leading '?', and with each value quoted
		do
			create Result.make (128)
			do_all (agent append_item_out (?, True, Result))
		end

	--|--------------------------------------------------------------

	has_parameter_by_name (v: STRING): BOOLEAN
			-- Does Current have a parameter with name 'v'?
		require
			exists: v /= Void and then not v.is_empty
		local
			oc: CURSOR
		do
			oc := cursor
			from start
			until exhausted or Result
			loop
				if item.tag ~ v then
					Result := True
				else
					forth
				end
			end
			go_to (oc)
		end

--|========================================================================
feature -- Access
--|========================================================================

	parameter_by_name (v: STRING): detachable like item
			-- Query parameter with name 'v', if any
		require
			exists: v /= Void and then not v.is_empty
		local
			oc: CURSOR
		do
			oc := cursor
			from start
			until exhausted or Result /= Void
			loop
				if item.tag ~ v then
					Result := item
				else
					forth
				end
			end
			go_to (oc)
		end

	--|--------------------------------------------------------------

	value_for_parameter (v: STRING): STRING
			-- Value for query parameter with name 'v', Empty if no match
		require
			exists: v /= Void and then not v.is_empty
		local
			ti: detachable like item
		do
			Result := ""
			ti := parameter_by_name (v)
			if ti /= Void then
				Result := ti.value
			else
				Result := ""
			end
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove_parameter_by_name (v: STRING)
		require
			exists: v /= Void
			belongs: has_parameter_by_name (v)
		local
			oc: CURSOR
			removed: BOOLEAN
		do
			oc := cursor
			from start
			until exhausted or removed
			loop
				if item.tag ~ v then
					remove
					removed := True
				else
					forth
				end
			end
			if valid_cursor (oc) then
				go_to (oc)
			end
		ensure
			gond: not has_parameter_by_name (v)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	append_item_out (v: like item; qf: BOOLEAN; buf: STRING)
			-- Append into 'buf the string representation of item 'v'
		require
			exists: v /= Void
			belongs: has (v)
			buffer_exists: buf /= Void
		local
			fmt: STRING
			op: STRING
		do
			if qf then
				fmt := "%%s%%s%"%%s%""
			else
				fmt := "%%s%%s%%s"
			end
			op := operator_by_sense (v.criterion)
			inspect v.criterion
			when K_qs_any, K_qs_none then
				-- Any and None have no 'value' per se
				buf.append (apf.aprintf (fmt, <<v.tag, op, "">>))
			else
				buf.append (apf.aprintf (fmt, <<v.tag, op, v.value>>))
			end
			if v /= last then
				buf.extend ('&')
			end
		end

	--|--------------------------------------------------------------

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class AEL_QUERY
