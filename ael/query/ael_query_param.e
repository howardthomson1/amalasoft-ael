class AEL_QUERY_PARAM
-- An individual query parameter in the form of a tag, a query value,
-- and a criterion
-- The tag represents a value retrievable from an AEL_QUERIABLE item
-- The query value is compared with the one from the queriable item
-- The criterion is applied to the associated value (for matching the 
-- query value with the retrieved value)

inherit
	AEL_QUERY_SUPPORT
		undefine
			default_create
		end
	AEL_SPRT_FALLIBLE
		redefine
			default_create
		end

create
	make, make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create
		do
			create tag.make (0)
			create value.make (0)
		end

	make (t: STRING; v: STRING; c: INTEGER)
			-- Create Current with tag 't', query value 'v' and 
			-- criterion 'c'
		require
			valid_tag: t /= Void
			value_exists: v /= Void
			valid_criterion: is_valid_query_sense (c)
		do
			default_create
			tag := t
			value := v
			set_criterion (c)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create Current from string 'v'
			-- String has the form:
			-- <tag>"="[<qs>][<value>]
		require
			valid: v /= Void and then v.has ('=')
		local
			pos, lim, tsen: INTEGER
			ts, tts, tval, ttag, emsg: STRING
			qc: CELL [INTEGER]
		do
			default_create
			tsen := K_qs_eq
			lim := v.count
			if lim > 0 then
				pos := v.index_of ('=',1)
				--RFO decode should be done long before here
				--ttag := i18.string_url_decoded (v.substring (1, pos - 
				--1))
				ttag := v.substring (1, pos - 1)
				if pos = lim then
					tval := ""
				else
					-- Capture the value, including any qualifiers
					ts := v.substring (pos + 1, lim)
					if is_qualified (ts) then
						create qc.put (K_qs_eq)
						create tts.make (ts.count)
						create emsg.make (32)
						analyze_qualified_value (ts, tts, qc, emsg)
						tsen := qc.item
						ts := tts
					elseif is_simple_wild (ts) then
						if ts.item (1) = '!' then
							tsen := K_qs_none
						else
							tsen := K_qs_any
						end
					elseif ttag ~ Ks_sort_tag or ttag ~ Ks_sort_alpha_tag then
						-- Special sort param, value represents field by 
						-- which to sort
						if ts.is_empty then
							-- ERROR
						else
							if ttag ~ Ks_sort_alpha_tag then
								tsen := K_qs_sort_alpha
							else
								tsen := K_qs_sort
							end
							if ts.item (1) = '-' then
								tsen := 0 - tsen
								ts.keep_tail (ts.count - 1)
							end
						end
					end
					--RFO decode should have been done long before here
					--tval := i18.quotes_stripped (i18.string_url_decoded 
					--(ts))
					tval := asr.quotes_stripped (ts)
				end
				make (ttag, tval, tsen)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	tag: STRING
			-- Tag denoting the value to compare

	value: STRING
			-- String representation of value to be compared

	criterion: INTEGER
			-- Comparision criterion

	is_not: BOOLEAN
			-- Is the sense of Current a form of negation?
		do
			Result := is_negation_sense (criterion)
		end

	is_sort: BOOLEAN
			-- Is this a built-in 'sort' query param?
		do
			inspect criterion
			when
				K_qs_sort, K_qs_sort_reverse,
				K_qs_sort_alpha, K_qs_sort_alpha_reverse
			 then
				 Result := True
			else
			end
		end

	is_alpha_sort: BOOLEAN
			-- Is this a built-in 'sort' query param, and is it alpha 
			-- (vs ASCII)?
		do
			inspect criterion
			when K_qs_sort_alpha, K_qs_sort_alpha_reverse then
				Result := True
			else
			end
		end

	is_reverse_sort: BOOLEAN
			-- Is this a built-in 'sort' query param, and is it reverse 
			-- order?
		do
			inspect criterion
			when K_qs_sort_reverse, K_qs_sort_alpha_reverse then
				Result := True
			else
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_criterion (v: INTEGER)
		require
			valid_criterion: is_valid_query_sense (v)
		do
			criterion := v
			configure_comparator
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid: BOOLEAN
			-- Is Current a valid query parameter?
		do
			Result := (tag /= Void and then not tag.is_empty) and
				(value /= Void) and
				is_valid_query_sense (criterion)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	value_matches (v: STRING): BOOLEAN
			-- Does value 'v' match Current?
		require
			exists: v /= Void
		do
			Result := True
			if attached comparitor as lc then
				Result := lc.item ([v])
				if is_not then
					Result := not Result
				end
			end
		end

	--|--------------------------------------------------------------

	comparitor: detachable PREDICATE [STRING]
			-- Comparison function to use for 'value_matches'


--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	configure_comparator
			-- Based on the current 'criterion', set up a priori the 
			-- comparison function to use for 'value_matches'
		local
			rexp: AEL_SPRT_REGEXP
--RFO 			wc: AEL_SPRT_WILDCARD
		do
			comparitor := Void
			inspect criterion
			when K_qs_undefined, K_qs_eq, K_qs_ne then
				comparitor := agent value_is_equal
			when K_qs_has, K_qs_not_has then
				comparitor := agent value_is_contained
			when K_qs_ge, K_qs_lt then
				comparitor := agent value_is_greater_or_equal
			when K_qs_le, K_qs_gt then
				comparitor := agent value_is_less_or_equal
			when K_qs_rexp, K_qs_rexpi, K_qs_not_rexp, K_qs_not_rexpi then
				if criterion = K_qs_rexpi or criterion = K_qs_not_rexpi then
					create rexp.make_insensitive (value)
				else
					create rexp.make (value)
				end
				if not rexp.compiled then
					set_error_message ("Invalid reqular expression in query")
				else
					comparitor := agent value_matches_expression (?, rexp)
				end
			when K_qs_wild, K_qs_wildi, K_qs_not_wild, K_qs_not_wildi then
				--RFO if criterion = K_qs_wildi or criterion = K_qs_not_wildi then
				--RFO 	create wc.make_insensitive (value)
				--RFO else
				--RFO 	create wc.make (value)
				--RFO end
				--RFO if not wc.compiled then
				--RFO 	set_error_message ("Invalid wildcard expression in query")
				--RFO else
				--RFO 	comparitor := agent value_matches_wildcard (?, wc)
				--RFO end
			when K_qs_any then
				comparitor := agent value_matches_any
			when K_qs_none then
				comparitor := agent value_matches_none
			else
				set_error_message ("Unknown qualifying query")
			end
		end

	--|--------------------------------------------------------------

	value_is_equal (v: STRING): BOOLEAN
			-- Is given value 'v' equal to query value 'value'?
		do
			Result := (v.is_empty and value.is_empty) or v ~ value
		end

	value_is_contained (v: STRING): BOOLEAN
			-- Is query value contained within 'v'?
			-- (i.e does 'v' have a substring 'value'?)
			-- If value is empty, then match is true only
			-- for queries with an empty pattern
		do
			Result := (v.is_empty and value.is_empty) or else
				v.substring_index (value, 1) /= 0
		end

	value_is_greater_or_equal (v: STRING): BOOLEAN
			-- Is 'v' greater than or equal to query value?
			-- The negated form is "less than"
		do
			if value.is_double and v.is_double then
				-- Use numeric comparison instead of string
				Result := v.to_double >= value.to_double
			else
				Result := v >= value
			end
		end

	value_is_less_or_equal (v: STRING): BOOLEAN
			-- Is 'v' less than or equal to query value?
			-- The negated form is "greater than"
		do
			if value.is_double and v.is_double then
				-- Use numeric comparison instead of string
				Result := v.to_double <= value.to_double
			else
				Result := v <= value
			end
		end

	value_matches_expression (v: STRING; rx: AEL_SPRT_REGEXP): BOOLEAN
			-- Does 'v' match the regular expression 'rx'?
		require
			valid_expression: rx /= Void and then rx.compiled
		do
			if v.is_empty then
				-- Can match only if rexp calls for 0 or more of any
				Result := value.is_empty or value ~ ".*"
			else
				Result := rx.matches (v)
			end
		end

	--RFO value_matches_wildcard (v: STRING; wc: AEL_SPRT_WILDCARD): BOOLEAN
	--RFO 		-- Does 'v' match the wildcard 'wc'?
	--RFO 	require
	--RFO 		valid_expression: wc /= Void and then wc.compiled
	--RFO 	do
	--RFO 		if v.is_empty then
	--RFO 			-- Can match only if wc calls for any
	--RFO 			Result := value.is_empty or value ~ "*"
	--RFO 		else
	--RFO 			Result := wc.matches (v)
	--RFO 		end
	--RFO 	end

	value_matches_any (v: STRING): BOOLEAN
			-- Does 'v' match anything?  Yes, always
		do
			Result := True
		end

	value_matches_none (v: STRING): BOOLEAN
			-- Does 'v' match nothing?  No, always
		do
			Result := False
		end

end -- class AEL_QUERY_PARAM
