note
	description: "{
Encapsulation of a CGI/FASTCFGI QUERY_STRING, without interpretation
or encode/decode "
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2011, Amalasoft";
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create an instance of this class by calling the make_from_string routine
with the full text of the query string as argument.  Creating the object
triggers decomposition which then leaves the query arguments in param_pairs
and simple tags.
To find a specific value by its tag, call value_for_tag.
If requested tag is solo, then result will be an empty string.
}"

class AEL_QUERY_STRING

inherit
	AEL_QUERY_SUPPORT
		redefine
			out
		end

create
	make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_string (v: STRING)
			-- Create Current from string 'v'
		require
			exists: v /= Void
		do
			string := v.twin
			create param_pairs.make
			create solo_tags.make
			solo_tags.compare_objects
			decompose
		end

	make_from_encoded_string (v: STRING)
			-- Create Current from url-encoded string 'v'
		require
			exists: v /= Void
		do
			make_from_string (i18.string_url_decoded (v))
		end

--|========================================================================
feature {NONE} -- Analysis
--|========================================================================

	decompose
		require
			has_content: string /= Void and then not string.is_empty
		local
			tv: AEL_SPRT_TV_PAIR
			raw_pairs, tl: LINKED_LIST [STRING]
			pos: INTEGER
			ti, tt, ts: STRING
		do
			raw_pairs := asr.string_to_list (string, '&', '\')
			from raw_pairs.start
			until raw_pairs.exhausted
			loop
				create tv.make
				ti := raw_pairs.item
				if not tv.is_tv_pair (ti) then
					-- a solo tag
					if ti.has (',') then
						-- A comma-separated list of tags in a single query 
						-- param
						tl := asr.string_to_list (ti, ',', '\')
						from tl.start
						until tl.exhausted
						loop
							if not tl.item.is_empty then
								solo_tags.extend (tl.item)
							end
							tl.forth
						end
					else
						solo_tags.extend (ti)
					end
				else
					pos := ti.index_of ('=',1)
					tt := ti.substring (1, pos - 1)
					tv.set_tag (tt)
					if pos < ti.count then
						ts := ti.substring (pos + 1, ti.count)
						tv.set_value (asr.quotes_stripped (ts))
						param_pairs.extend (tv)
					end
				end
				raw_pairs.forth
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	string: STRING
			-- Original string
			-- If current string representation is desired, use 'out'

	param_pairs: AEL_SPRT_TV_PAIR_LIST
			-- List of tag-value pair parameters

	solo_tags: LINKED_LIST [STRING]

	is_empty: BOOLEAN
			-- Does current _not_ have any items? (solo or tv-pair)
		do
			Result := not has_solo_tags and not has_param_pairs
		end

	has_solo_tags: BOOLEAN
			-- Does Current have any solo tags?
		do
			Result := not solo_tags.is_empty
		end

	has_param_pairs: BOOLEAN
			-- Does Current have any param pairs?
		do
			Result := not param_pairs.is_empty
		end

 --|------------------------------------------------------------------------

	out: STRING
			-- String representation of Current
		local
			tl: LIST [AEL_SPRT_TV_PAIR]
		do
			create Result.make (string.count)
			-- Show pairs first
			if not param_pairs.is_empty then
				tl := param_pairs.contents_sorted
				from tl.start
				until tl.exhausted
				loop
					Result.append (tl.item.out)
					tl.forth
					if not tl.exhausted then
						Result.extend ('&')
					end
				end
				if not solo_tags.is_empty then
					Result.extend ('&')
				end
			end
			Result.append (alr.list_to_string (solo_tags, "&"))
		end

	html_out: STRING
		do
			Result := param_pairs.decorated_out ("", "<br>%R%N")
			if not solo_tags.is_empty then
				Result.append (alr.list_to_string (solo_tags, "<br>%R%N"))
			end
		end

 --|------------------------------------------------------------------------

	value_for_tag (v: STRING): STRING
			-- Value for given argument, if any
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := param_pairs.value_for_tag (v)
		ensure
			exists: Result /= Void
		end

	has_pair_by_tag (v: STRING): BOOLEAN
			-- Does Current have a param pair with tag 'v'?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := param_pairs.has_tag (v)
		end

	has_solo_tag (v: STRING): BOOLEAN
			-- Does Current have solo tag 'v'?
		do
			-- solo_tags was set to object comparison on creation
			Result := solo_tags.has (v)
		end

--|========================================================================
feature -- Element change
--|========================================================================

	add_pair (tv: AEL_SPRT_TV_PAIR)
		require
			exists: tv /= Void
			valid: tv.is_valid_tag (tv.tag)
		do
			param_pairs.extend (tv)
		end

	add_tv_pair (t, v: STRING)
		require
			exists: t /= Void and v /= Void
			valid: not t.is_empty
		do
			param_pairs.add_item (t, v)
		end

	add_solo_tag (v: STRING)
		require
			exists: v /= Void and not v.is_empty
		do
			solo_tags.extend (v)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	remove_pair (tv: AEL_SPRT_TV_PAIR)
		require
			exists: tv /= Void
			belongs: has_pair_by_tag (tv.tag)
		do
			param_pairs.remove_item_by_tag (tv.tag)
		end

	remove_solo_tag (v: STRING)
			-- Remove from solo_tags the item 'v'
		require
			exists: v /= Void
			belongs: has_solo_tag (v)
		local
			oc: CURSOR
		do
			oc := solo_tags.cursor
			solo_tags.start
			solo_tags.search (v)
			solo_tags.remove
			if solo_tags.valid_cursor (oc) then
				solo_tags.go_to (oc)
			end
		end

--|========================================================================
feature {NONE} -- Common services
--|========================================================================

--RFO 	asr: AEL_SPRT_STRING_ROUTINES
--RFO 		once
--RFO 			create Result
--RFO 		end

	alr: AEL_SPRT_LIST_ROUTINES
		once
			create Result
		end

end -- class AEL_QUERY_STRING
