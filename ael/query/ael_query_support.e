class AEL_QUERY_SUPPORT
-- Constants and routines to support classes in AEL query cluster

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_query_sense (v: INTEGER): BOOLEAN
		do
			Result := query_sense_labels.has (v)
		end

	--|--------------------------------------------------------------

	is_qualified (v: STRING): BOOLEAN
			-- Is 'v' a qualified query expression?
		do
			if v /= Void and then not v.is_empty then
				if v.item (1) = '^' and then v.count >= 2 then
					Result := query_qualifiers.has (v.item (2))
				end
			end
		end

	is_simple_wild (v: STRING): BOOLEAN
			-- Is 'v' a _simple_ wildcard expression?
			-- Supported expressions are "*" and "!*"
		do
			Result := v ~ "*" or v ~ "!*"
		end

	is_negation_sense (v: INTEGER): BOOLEAN
		require
			valid_sense: is_valid_query_sense (v)
		do
			Result := v < 0
		end

	--|--------------------------------------------------------------

	is_query_regexp_operator (v: STRING): BOOLEAN
		do
			if v /= Void and then not v.is_empty then
				Result := query_regexp_operator_ids.has (v)
			end
		end

	--|--------------------------------------------------------------

	operator_string (v: STRING; spos: INTEGER): STRING
			-- Full sequence of characters in the operator part of the 
			-- query string 'v', where the operator starts as 'spos'.
			-- v[spos] must be the leading '=' character
		require
			valid: v /= Void
			valid_start: spos > 0 and spos <= v.count
			starts_with_op: v.item (spos) = '='
		local
			lim, sp: INTEGER
		do
			create Result.make (8)
			Result.extend ('=')
			lim := v.count
			sp := spos + 1
			if lim >= sp then
				if v.item (sp) = '*' then
					-- 'all' wildcard
					Result.extend (v.item (sp))
				elseif v.item (sp) = '^' then -- check for caret
					Result.extend (v.item (sp))
					sp := sp + 1
					if v.item (sp) = Kc_qq_not then -- check for negation
						sp := sp + 1
						Result.extend (v.item (sp))
					end
					if lim >= sp then
						-- Check for criterion characters
						if query_qualifiers.has (v.item (sp)) then
							Result.extend (v.item (sp))
							-- Is qualified
							inspect v.item (sp)
							when Kc_qq_sub then
								-- substring: "=^=" or "=^!="
							when Kc_qq_lt then
								-- less than: "=^<.." or "=^!<.."
								if v.count > sp and then v.item (sp + 1) = '=' then
									sp := sp + 1
									Result.extend (v.item (sp))
								end
							when Kc_qq_gt then
								-- greater than: "=^>.." or "=^!>.."
								if v.count > sp and then v.item (sp + 1) = '=' then
									sp := sp + 1
									Result.extend (v.item (sp))
								end
							when Kc_qq_rx then
								-- regexp: "=^~=" or "=^!~="
								if v.count > sp and then v.item (sp + 1) = '=' then
									sp := sp + 1
									Result.extend (v.item (sp))
								end
							when Kc_qq_wild then
								-- wildcard: "=^*" or "=^!*"
							else
							end
						end
					end
				end
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- Support implementation
--|========================================================================

	analyze_qualified_value (qv, v: STRING; qc: CELL [INTEGER]; emsg: STRING)
			-- Analyze qualified value 'qv', placing into 'qc' the query
			-- sense and placing into 'v' the value itself (less
			-- qualifiers)
			-- 'qv' begins with the '^' qualifier character.  The 
			-- leading '=' have been bypassed.
		require
			original_exists: qv /= Void and then not qv.is_empty
			is_qualified: is_qualified (qv)
			target_exists: v /= Void
			sense_exists: qc /= Void
		local
			is_neg, rxi: BOOLEAN
			sp, first_pos: INTEGER
			sense: INTEGER
		do
			v.wipe_out
			-- Position 1 should be the '^' operator
			-- Position 2 is the first criterion character
			first_pos := 2
			if qv.item (first_pos) = Kc_qq_not then
				is_neg := True
				sense := K_qs_ne
				sp := 1 + first_pos
			else
				sp := first_pos
				sense := K_qs_eq
			end
			if qv.count >= sp then
				-- Check the first criterion character
				inspect qv.item (sp)
				when Kc_qq_sub then
					if is_neg then
						sense := K_qs_not_has
					else
						sense := K_qs_has
					end
				when Kc_qq_lt then
					if is_neg then
						if qv.count >= (sp + 1) and then qv.item (sp + 1) = '=' then
							sense := K_qs_gt
							sp := sp + 1
						else
							sense := K_qs_ge
						end
					else
						if qv.count >= (sp + 1) and then qv.item (sp + 1) = '=' then
							sense := K_qs_le
							sp := sp + 1
						else
							sense := K_qs_lt
						end
					end
				when Kc_qq_gt then
					if is_neg then
						if qv.count >= (sp + 1) and then qv.item (sp + 1) = '=' then
							sense := K_qs_lt
							sp := sp + 1
						else
							sense := K_qs_le
						end
					else
						if qv.count >= (sp + 1) and then qv.item (sp + 1) = '=' then
							sense := K_qs_ge
							sp := sp + 1
						else
							sense := K_qs_gt
						end
					end
				when Kc_qq_rx then
					if qv.count >= (sp + 1) then
						if qv.item (sp + 1) = '='then
							-- Form is "=~=" or "=!~="
							-- Denotes case-insensitive
							sp := sp + 1
							rxi := True
						end
					end
					if is_neg then
						if rxi then
							sense := K_qs_not_rexpi
						else
							sense := K_qs_not_rexp
						end
					else
						if rxi then
							sense := K_qs_rexpi
						else
							sense := K_qs_rexp
						end
					end
				when Kc_qq_wild then
					if sp = qv.count then
						-- Cases =^* and =^!* with no trailing value
						if is_neg then
							sense := K_qs_none
						else
							sense := K_qs_any
						end
					else
						if is_neg then
							if qv.count >= (sp + 1) and then qv.item (sp + 1)= '=' then
								sense := K_qs_not_wildi
								sp := sp + 1
							else
								sense := K_qs_not_wild
							end
						else
							if qv.count >= (sp + 1) and then qv.item (sp + 1)= '=' then
								sense := K_qs_wildi
								sp := sp + 1
							else
								sense := K_qs_wild
							end
						end
					end
				else
					if sp = 1 then
						-- ERROR
						if emsg /= Void then
							emsg.append ("Invalid qualifying character '" +
								qv.item (sp).out + "'")
						end
					else
						sp := sp - 1
					end
				end
			end
--RFO 			print ("query is qualified, qv=" + qv + ", qual=" +
--RFO 				query_sense_labels.item (sense) + "%N")
			if emsg = Void or else emsg.is_empty then
				qc.put (sense)
				v.append (qv.substring (sp+1, qv.count))
			end
		end

	--|--------------------------------------------------------------

	operator_by_sense (v: INTEGER): STRING
		require
			valid_sense: is_valid_query_sense (v)
		do
			Result := query_sense_operator (v)
		end

--|========================================================================
feature -- Support
--|========================================================================

	help_text: STRING
		once
			Result := "{
Queries are tag-value pairs, with the form:
   <tag>"="[<qual>]<value>

Matching is, by default, based on equivalence (as in
and exact match of content, not necessarily reference)

Tags are always non-empty, but values can be empty

Values can be preceded by qualifying strings to
denote negation and conditions other than simple 
equivalence.  There is always a leading '=' sign,
followed by the qualifier marker '^', and then the
actual qualifier,

Qualified and unqualified combinations include:
  "="     - denotes exact, case-sensitive match
  "=^!"   - denotes simple negation of condition
            tag=^!value is true for all items whose value 
            associated with 'tag' is not exactly 'value'
  "=^*"   - denotes wildcard condition
            tag=^* is true for all items
            tag=^*value is true for all items that match
            the wilcard expression 'value'
  "=^!*"  - denotes negation of wildcard condition
            tag=^!* is false for all items
            tag=^!*value is true for all items that do NOT
            match the wilcard expression 'value'
  "=^="   - denotes substring equivalence
            tag=^=value is true for all items whose value 
            associated with 'tag' contains 'value'
  "=^!="  - denotes negation of substring equivalence
            tag=^!=value is true for all items whose value 
            associated with 'tag' does not contain 'value'
  "=^~"   - denotes regular expression matching
            tag=^~value is true for all items whose value 
            associated with 'tag' matches the regular 
            expression 'value'
  "=^~="  - denotes case-insensitive regular expression matching
            tag=^~=value is true for all items whose value 
            associated with 'tag' matches the regular 
            expression 'value'
  "=^!~"  - denotes negation of regular expression matching
            tag=^!~value is true for all items whose value 
            associated with 'tag' does not match the regular 
            expression 'value'
  "=^>"   - denotes comparative superiority
            tag=^>value is true for all items whose value 
            associated with 'tag' is greater than 'value'
  "=^>="  - denotes comparative superiority or equivalence
            tag=^>=value is true for all items whose value 
            associated with 'tag' is equal to or greater 
            than 'value'
  "=^!>"  - denotes negation of comparative superiority
            tag=^!>value is true for all items whose value 
            associated with 'tag' is not greater than 'value'
  "=^!>=" - denotes negation of comparative superiority or
            equivalence.
            tag=^!>=value is true for all items whose value 
            associated with 'tag' is not equal to or greater
            than 'value'
  "=^<"   - denotes comparative inferiority
            tag=^<value is true for all items whose value 
            associated with 'tag' is less than 'value'
  "=^<="  - denotes comparative inferiority or equivalence
            tag=^<=value is true for all items whose value 
            associated with 'tag' is equal to or less than
            'value'
  "=^!<"  - denotes negation of comparative inferiority
            tag=^!<value is true for all items whose value 
            associated with 'tag' is not less than 'value'
  "=^!<=" - denotes negation of comparative inferiority or
            equivalence.
            tag=^!<=value is true for all items whose value 
            associated with 'tag' is not equal to or less
            than 'value'

From the original query string (e.g. tag=^<value), the 
leading equal sign is stripped when converted to a 
tv-pair.
The trailing qualifiers remain as part of the value field 
within the tv-pair.

Note well that in some environments, certain characters might be
escaped or transformed.
}"
		end

  --|--------------------------------------------------------------

  help_text_escaped: STRING
			-- Help text with open/close arrows escaped
		once
			Result := help_text.twin
			Result.replace_substring_all ("<", "&lt;")
			Result.replace_substring_all (">", "&gt;")
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_sort_tag: STRING = "_sort"
	Ks_sort_alpha_tag: STRING = "_SORT"

	K_qs_undefined: INTEGER = 0

	K_qs_eq: INTEGER = 1
			-- Default sense
	K_qs_has: INTEGER = 2
	K_qs_ge: INTEGER = 3
	K_qs_le: INTEGER = 4
	K_qs_rexp: INTEGER = 5
	K_qs_rexpi: INTEGER = 6
	K_qs_wild: INTEGER = 7
	K_qs_wildi: INTEGER = 8
	K_qs_any: INTEGER = 9

	K_qs_ne: INTEGER = -1
	K_qs_not_has: INTEGER = -2
	K_qs_lt: INTEGER = -3
	K_qs_gt: INTEGER = -4
	K_qs_not_rexp: INTEGER = -5
	K_qs_not_rexpi: INTEGER = -6
	K_qs_not_wild: INTEGER = -7
	K_qs_not_wildi: INTEGER = -8
	K_qs_none: INTEGER = -9

	K_qs_sort: INTEGER = 99
	K_qs_sort_reverse: INTEGER = -99
	K_qs_sort_alpha: INTEGER = 98
	K_qs_sort_alpha_reverse: INTEGER = -98


	Ks_qs_undefined: STRING = "=?"
	Ks_qs_eq: STRING = "="
	Ks_qs_ne: STRING = "=^!"
	Ks_qs_has: STRING = "=^="
	Ks_qs_not_has: STRING = "=^!="
	Ks_qs_ge: STRING = "=^>="
	Ks_qs_gt: STRING = "=^>"
	Ks_qs_le: STRING = "=^<="
	Ks_qs_lt: STRING = "=^<"
	Ks_qs_rexp: STRING = "=^~"
	Ks_qs_rexpi: STRING = "=^~="
	Ks_qs_not_rexp: STRING = "=^!~"
	Ks_qs_not_rexpi: STRING = "=^!~="
	Ks_qs_wild: STRING = "=^*"
	Ks_qs_wildi: STRING = "=^*="
	Ks_qs_not_wild: STRING = "=^!*"
	Ks_qs_not_wildi: STRING = "=^!*="
	Ks_qs_any: STRING = "=*"
	Ks_qs_none: STRING = "=!"
	Ks_qs_sort: STRING = "="
	Ks_qs_sort_reverse: STRING = "=-"
	Ks_qs_sort_alpha: STRING = "="
	Ks_qs_sort_alpha_reverse: STRING = "=-"

	--|--------------------------------------------------------------

	query_sense_label (v: INTEGER): STRING
		require
			valid: is_valid_query_sense (v)
		do
			if attached query_sense_labels.item (v) as lo then
				Result := lo
			else
				Result := Ks_qs_undefined
			end
		end

	query_sense_labels: HASH_TABLE [STRING, INTEGER]
		once
			create Result.make (19)
			Result.extend ("Undefined", K_qs_undefined)
			Result.extend ("Equal", K_qs_eq)
			Result.extend ("Not equal", K_qs_ne)
			Result.extend ("Has", K_qs_has)
			Result.extend ("Not has", K_qs_not_has)
			Result.extend ("Greater or Equal", K_qs_ge)
			Result.extend ("Greater", K_qs_gt)
			Result.extend ("Less or Equal", K_qs_le)
			Result.extend ("Less", K_qs_lt)
			Result.extend ("Regexp", K_qs_rexp)
			Result.extend ("Regexp Ins", K_qs_rexpi)
			Result.extend ("Not Regexp", K_qs_not_rexp)
			Result.extend ("Not Regexp Ins", K_qs_not_rexpi)
			Result.extend ("Wild", K_qs_wild)
			Result.extend ("Wild Ins", K_qs_wildi)
			Result.extend ("Not Wild", K_qs_not_wild)
			Result.extend ("Not Wild Ins", K_qs_not_wildi)
			Result.extend ("Any", K_qs_any)
			Result.extend ("None", K_qs_none)
			Result.extend (Ks_sort_tag, K_qs_sort)
			Result.extend (Ks_sort_tag, K_qs_sort_reverse)
			Result.extend (Ks_sort_alpha_tag, K_qs_sort_alpha)
			Result.extend (Ks_sort_alpha_tag, K_qs_sort_alpha_reverse)
		end

	--|--------------------------------------------------------------

	obs_query_sense_operators: HASH_TABLE [STRING, INTEGER]
		once
			create Result.make (19)
			Result.extend ("=?", K_qs_undefined)
			Result.extend ("=", K_qs_eq)
			Result.extend ("=^!", K_qs_ne)
			Result.extend ("=^=", K_qs_has)
			Result.extend ("=^!=", K_qs_not_has)
			Result.extend ("=^>=", K_qs_ge)
			Result.extend ("=^>", K_qs_gt)
			Result.extend ("=^<=", K_qs_le)
			Result.extend ("=^<", K_qs_lt)
			Result.extend ("=^~", K_qs_rexp)
			Result.extend ("=^~=", K_qs_rexpi)
			Result.extend ("=^!~", K_qs_not_rexp)
			Result.extend ("=^!~=", K_qs_not_rexpi)
			Result.extend ("=^*", K_qs_wild)
			Result.extend ("=^*=", K_qs_wildi)
			Result.extend ("=^!*", K_qs_not_wild)
			Result.extend ("=^!*=", K_qs_not_wildi)
			Result.extend ("=*", K_qs_any)
			Result.extend ("=!", K_qs_none)
			Result.extend ("=", K_qs_sort)
			Result.extend ("=-", K_qs_sort_reverse)
			Result.extend ("=", K_qs_sort_alpha)
			Result.extend ("=-", K_qs_sort_alpha_reverse)
		end

	query_sense_operator (v: INTEGER): STRING
		require
			valid: is_valid_query_sense (v)
		do
			if attached query_sense_operators.item (v) as lo then
				Result := lo
			else
				Result := Ks_qs_undefined
			end
		end

	query_sense_operators: HASH_TABLE [STRING, INTEGER]
		once
			create Result.make (19)
			Result.extend (Ks_qs_undefined, K_qs_undefined)
			Result.extend (Ks_qs_eq, K_qs_eq)
			Result.extend (Ks_qs_ne, K_qs_ne)
			Result.extend (Ks_qs_has, K_qs_has)
			Result.extend (Ks_qs_not_has, K_qs_not_has)
			Result.extend (Ks_qs_ge, K_qs_ge)
			Result.extend (Ks_qs_gt, K_qs_gt)
			Result.extend (Ks_qs_le, K_qs_le)
			Result.extend (Ks_qs_lt, K_qs_lt)
			Result.extend (Ks_qs_rexp, K_qs_rexp)
			Result.extend (Ks_qs_rexpi, K_qs_rexpi)
			Result.extend (Ks_qs_not_rexp, K_qs_not_rexp)
			Result.extend (Ks_qs_not_rexpi, K_qs_not_rexpi)
			Result.extend (Ks_qs_wild, K_qs_wild)
			Result.extend (Ks_qs_wildi, K_qs_wildi)
			Result.extend (Ks_qs_not_wild, K_qs_not_wild)
			Result.extend (Ks_qs_not_wildi, K_qs_not_wildi)
			Result.extend (Ks_qs_any, K_qs_any)
			Result.extend (Ks_qs_none, K_qs_none)
			Result.extend (Ks_qs_sort, K_qs_sort)
			Result.extend (Ks_qs_sort_reverse, K_qs_sort_reverse)
			Result.extend (Ks_qs_sort_alpha, K_qs_sort_alpha)
			Result.extend (Ks_qs_sort_alpha_reverse, K_qs_sort_alpha_reverse)
		end

	query_sense_operator_id: HASH_TABLE [INTEGER, STRING]
		once
			create Result.make (19)
			Result.extend (K_qs_undefined, Ks_qs_undefined)
			Result.extend (K_qs_eq, Ks_qs_eq)
			Result.extend (K_qs_ne, Ks_qs_ne)
			Result.extend (K_qs_has, Ks_qs_has)
			Result.extend (K_qs_not_has, Ks_qs_not_has)
			Result.extend (K_qs_ge, Ks_qs_ge)
			Result.extend (K_qs_gt, Ks_qs_gt)
			Result.extend (K_qs_le, Ks_qs_le)
			Result.extend (K_qs_lt, Ks_qs_lt)
			Result.extend (K_qs_rexp, Ks_qs_rexp)
			Result.extend (K_qs_rexpi, Ks_qs_rexpi)
			Result.extend (K_qs_not_rexp, Ks_qs_not_rexp)
			Result.extend (K_qs_not_rexpi, Ks_qs_not_rexpi)
			Result.extend (K_qs_wild, Ks_qs_wild)
			Result.extend (K_qs_wildi, Ks_qs_wildi)
			Result.extend (K_qs_not_wild, Ks_qs_not_wild)
			Result.extend (K_qs_not_wildi, Ks_qs_not_wildi)
			Result.extend (K_qs_any, Ks_qs_any)
			Result.extend (K_qs_none, Ks_qs_none)
			Result.extend (K_qs_sort, Ks_qs_sort)
			Result.extend (K_qs_sort_reverse, Ks_qs_sort_reverse)
			Result.extend (K_qs_sort_alpha, Ks_qs_sort_alpha)
			Result.extend (K_qs_sort_alpha_reverse, Ks_qs_sort_alpha_reverse)
		end

	query_regexp_operators: HASH_TABLE [STRING, INTEGER]
		once
			create Result.make (7)
			Result.extend (Ks_qs_rexp, K_qs_rexp)
			Result.extend (Ks_qs_rexpi, K_qs_rexpi)
			Result.extend (Ks_qs_not_rexp, K_qs_not_rexp)
			Result.extend (Ks_qs_not_rexpi, K_qs_not_rexpi)
		end

	query_regexp_operator_ids: HASH_TABLE [INTEGER, STRING]
		once
			create Result.make (7)
			Result.extend (K_qs_rexp, Ks_qs_rexp)
			Result.extend (K_qs_rexpi, Ks_qs_rexpi)
			Result.extend (K_qs_not_rexp, Ks_qs_not_rexp)
			Result.extend (K_qs_not_rexpi, Ks_qs_not_rexpi)
		end

	--|--------------------------------------------------------------

	query_qualifiers: STRING
		once
			Result := "!*=<>~"
		end

	Kc_qq_not: CHARACTER = '!'
	Kc_qq_wild: CHARACTER = '*'
	Kc_qq_sub: CHARACTER = '='
	Kc_qq_rx: CHARACTER = '~'
	Kc_qq_gt: CHARACTER = '>'
	Kc_qq_lt: CHARACTER = '<'

--|========================================================================
feature -- Support
--|========================================================================

	i18: AEL_I18N_STRING_ROUTINES
		once
			create Result
		end

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_QUERY_SUPPORT
