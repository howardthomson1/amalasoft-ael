note
	description: "{
An abstractly structured document section
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_STRUCTURED_DOC_SECTION

inherit
	AEL_DS_INDIRECT_2WAY_LIST [AEL_STRUCTURED_DOC_SECTION]
		rename
			make as ds_make
		redefine
			default_create, extend
		end
	AEL_USML_TYPES
		undefine
			default_create, copy, is_equal
		end

create
	make_with_level, make_as_paragraph

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_level (v: INTEGER)
			-- Create Current with level 'v'
		require
			valid: v > 0
		do
			level := v
			default_create
			set_default_style
		end

	make_as_paragraph
			-- Create Current as a paragraph
		do
			default_create
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		local
			tl: like list
		do
--			create items.make
			create text.make (0)
			create title.make (0)
			create tl.make
			ds_make (tl)
			Precursor
		end

	--|--------------------------------------------------------------

	set_default_style
		local
			ff: STRING
			ps: INTEGER
			fbf, fif, fuf: BOOLEAN
		do
			if is_title_worthy then
				ff := dflt_title_font_family
				ps := dflt_title_point_size
				fbf := dflt_title_is_bold
				fif := dflt_title_is_italic
				fuf := dflt_title_is_underlined
				set_title_font (ff, ps, fbf, fif, fuf)
			else
				ff := dflt_text_font_family
				ps := dflt_text_point_size
				fbf := dflt_text_is_bold
				fif := dflt_text_is_italic
				fuf := dflt_text_is_underlined
				set_text_font (ff, ps, fbf, fif, fuf)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	level: INTEGER
			-- Level in document hierarchy
			-- Level 1 is top-level headers ("H1")
			-- Level 0 is non-header text ("P")
			-- Document itself is also level 0, but is known to be the 
			-- document

	parent: detachable like Current

	title: STRING
			-- Text of section title/heading

	text: STRING
			-- Text belonging (directly) to Current

	text_length: INTEGER
			-- Number of characters in 'text'
		do
			Result := text.count
		end

	--|--------------------------------------------------------------

	has_items: BOOLEAN
			-- Does Current have any subordinate elements?
		do
			Result := not items.is_empty
		end

	--|--------------------------------------------------------------

	is_document: BOOLEAN
			-- Is Current a document?
		do
		end

	is_heading: BOOLEAN
			-- Is Current a heading
		do
			Result := level > 0
		end

	is_title_worthy: BOOLEAN
			-- Is Current worthy of a title?
			-- a heading or a document
		do
			Result := is_heading or is_document
		end

	--|--------------------------------------------------------------

	text_font_family: detachable STRING
	text_point_size : INTEGER
	text_is_bold: BOOLEAN
	text_is_italic: BOOLEAN
	text_is_underlined: BOOLEAN
	text_color: detachable EV_COLOR

	title_font_family: detachable STRING
	title_point_size : INTEGER
	title_is_bold: BOOLEAN
	title_is_italic: BOOLEAN
	title_is_underlined: BOOLEAN
	title_color: detachable EV_COLOR

	start_index: INTEGER
			-- Position, in a text field, at which Current begins

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text (v: STRING)
		do
			text.wipe_out
			text.append (v)
		end

	append_text (v: STRING)
		do
			text.append (v)
		end

	append_line (v: STRING)
			-- Append text 'v' to text, adding a newline at end
		do
			text.append (v)
			text.append ("%N")
		end

	set_title (v: STRING)
		require
			is_hdr: is_title_worthy
		do
			title.wipe_out
			title.append (v)
		end

	--|--------------------------------------------------------------

	set_level (v: INTEGER)
			-- Set level in doc hierarchy for Current
		do
			level := v
		end

	set_parent (v: like parent)
		do
			parent := v
		end

	set_start_index (v: INTEGER)
		require
			valid: v >= 0
		do
			start_index := v
		end

	--|--------------------------------------------------------------

	set_title_font (ff: STRING; ps: INTEGER; b, i, u: BOOLEAN)
			-- Set the preferred font by which to render 'title'
			-- 'ff' is font family ("sans", "times", or "fixed")
			-- 'ps' is point size (positive integer)
			-- 'b' is bold flag (bold if True)
			-- 'i' is italics flag (italics if True)
			-- 'u' is underline flag (underlined if True)
		require
			valid_family: is_valid_font_family (ff)
			is_hdr: is_title_worthy
		do
			title_font_family := ff
			title_point_size := ps
			title_is_bold := b
			title_is_italic := i
			title_is_underlined := u
		end

	set_title_color (v: EV_COLOR)
			-- Set the preferred color of Current's title
		do
			title_color := v
		end

	--|--------------------------------------------------------------

	set_text_font (ff: STRING; ps: INTEGER; b, i, u: BOOLEAN)
			-- Set the preferred font by which to render 'text'
			-- 'ff' is font family ("sans", "times", or "fixed")
			-- 'ps' is point size (positive integer)
			-- 'b' is bold flag (bold if True)
			-- 'i' is italics flag (italics if True)
			-- 'u' is underline flag (underlined if True)
		require
			valid_family: is_valid_font_family (ff)
		do
			text_font_family := ff
			text_point_size := ps
			text_is_bold := b
			text_is_italic := i
			text_is_underlined := u
		end

	set_text_color (v: EV_COLOR)
			-- Set the preferred color of Current's text
		do
			text_color := v
		end

--|========================================================================
feature -- Access
--|========================================================================

	items: TWO_WAY_LIST [AEL_STRUCTURED_DOC_SECTION]
		do
			Result := list
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: like item)
			-- Add item 'v' to end of list
		do
			Precursor (v)
			v.set_parent (Current)
		ensure then
			parented: v.parent = Current
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	dflt_title_font_family: STRING
		once
			Result := Ks_ff_sans
		end

	dflt_title_point_size: INTEGER
		do
			if is_document then
				Result := 18
			else
				inspect level
				when 1 then
					Result := 16
				when 2 then
					Result := 14
				when 3 then
					Result := 12
				when 4 then
					Result := 12
				else
					Result := 11
				end
			end
		end

	dflt_title_is_bold: BOOLEAN
		do
			if is_document then
				Result := True
			else
				inspect level
				when 1 then
					Result := True
				when 2 then
					Result := True
				when 3 then
					Result := True
				when 4 then
					Result := True
				else
				end
			end
		end

	dflt_title_is_italic: BOOLEAN
		do
			inspect level
			when 4 then
				Result := True
			when 5 then
				Result := True
			else
				Result := False
			end
		end

	dflt_title_is_underlined: BOOLEAN
		do
		end

	--|--------------------------------------------------------------

	dflt_text_font_family: STRING = "times"

	dflt_text_point_size: INTEGER
		do
			Result := 140
		end

	dflt_text_is_bold: BOOLEAN
		do
		end

	dflt_text_is_italic: BOOLEAN
		do
		end

	dflt_text_is_underlined: BOOLEAN
		do
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_STRUCTURED_DOC_SECTION
