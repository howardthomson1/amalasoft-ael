note
	description: "{
An abstractly structured document
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_STRUCTURED_DOCUMENT

inherit
	AEL_STRUCTURED_DOC_SECTION
		redefine
			is_document, default_create
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
			-- Create Current
		do
			default_create
		end

	default_create
			-- Create Current in its default initial state
		do
 			create title.make (0)
			create tags.make
			Precursor
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_stream (v: STRING)
			-- Initialize Current from stream 'v'
			-- Stream must be in USML (Ultra-simple markup language) 
			-- format
			-- Format includes tags that the define the style of the 
			-- text that follows (until the next tag or end of stream)
			-- Tags exists on lines all by themselves.  In this way, 
			-- they act more like section separators than tags in more 
			-- traditional markups like HTML (in which tags have open 
			-- and closing counterparts, or at least most of them do)
			-- Example
			--   <usml h1>
			--   Introduction (this is text of the heading)
			--   <usml p>
			--   This is the text of the introduction (paragraphs)
		require
			is_usml: v.starts_with (Ks_usml_tag_prefix)
		local
			sp, lim, lvl: INTEGER
			pcell: CELL [INTEGER]
			bf, itf, uf, dbf: BOOLEAN
			ps, cv: INTEGER
			ff, rc: STRING
			line: STRING
			tag, ptag: AEL_USML_TAG
			ts1, ts2, ts3, ts4, ts5, ts6, ts7, ts8: AEL_STRUCTURED_DOC_SECTION
			ts99: AEL_STRUCTURED_DOC_SECTION
			tsx, tsp: AEL_STRUCTURED_DOC_SECTION
		do
			lim := v.count

			create pcell.put (1)
			from sp := 1
			until sp > lim or sp = 0 or has_error
			loop
				pcell.put (sp)
				line := next_line_iq (v, pcell)
				if line.starts_with ("<usml c") then
					dbf := dbf
				end
				if is_valid_usml_tag (line) then
					create tag.make_with_text (line)
					if attached tag.error_message as msg then
						set_error_message (msg)
					end
					tags.extend (tag)
					if tag.is_title then
						tsp := Void
						create tsx.make_with_level (tag.level)
						extend (tsx)
						set_title (tag.tag_text)
					elseif tag.is_header then
						-- Header text follows header and should be 
						-- assigned to sec.text
						tsp := Void
						lvl := tag.level
						if lvl = 1 then
							create ts1.make_with_level (lvl)
							extend (ts1)
							tsx := ts1
						else
							create tsx.make_with_level (lvl)
							add_section (
								tsx, ts1, ts2, ts2, ts4, ts5, ts6, ts7, ts8)
							inspect lvl
							when 2 then
								ts2 := tsx
							when 3 then
								ts3 := tsx
							when 4 then
								ts4 := tsx
							when 5 then
								ts5 := tsx
							when 6 then
								ts6 := tsx
							when 7 then
								ts7 := tsx
							when 8 then
								ts8 := tsx
							else
								-- not possible
							end
						end
						tsx.set_values_from_tag (tag)
					elseif tag.is_macro then
						-- Tag defines a macro for subsequent use in tags
						--ptag := tag
						-- Add macro
						if attached tag.macro_name as mnm then
							if macros.has (mnm) then
								macros.replace (tag, mnm)
							else
								macros.extend (tag, mnm)
							end
						else
							-- ERROR
						end
					else
						-- Not a header, can be paragraph, comment tag
						-- or a format definition tag
						if tag.is_paragraph then
							create tsp.make_as_paragraph
							ptag := tag
							tsp.set_values_from_tag (tag)
							if attached tsx then
								tsx.extend (tsp)
							else
								extend (tsp)
							end
						elseif tag.is_format then
							-- Tag defines format for a class of section, 
							-- from this point forward, unless redefined in
							-- that section's tag
							ptag := tag
							set_default_format_from_tag (ptag)
						else
							-- a comment, do not change anything
						end
					end
				else
					-- text until next tag; add to current section
					if attached tsp then
						tsp.append_line (line)
					elseif attached tsx then
						tsx.set_text (line)
						if tsx.level = K_usml_tag_title then
							set_title (tsx.text)
						end
					else
						-- add to top-level as new section?
						set_error_message ("Orphaned section")
					end
				end
				sp := pcell.item
			end
--print (headings_out)
--print (tags_out)
--print (formats_out)
		end

	--|--------------------------------------------------------------

	add_section (
		sec: AEL_STRUCTURED_DOC_SECTION;
		ts1, ts2, ts3, ts4: detachable AEL_STRUCTURED_DOC_SECTION;
		ts5, ts6, ts7, ts8: detachable AEL_STRUCTURED_DOC_SECTION)
			-- Add section 'sec' at the best level available
		local
			lvl: INTEGER
		do
			lvl := sec.level
			inspect lvl
			when 99 then
				-- document title
				--set_title_from_section (sec)
				extend (sec)
			when 1 then
				extend (sec)
			when 2 then
				if attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 3 then
				if attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 4 then
				if attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 5 then
				if attached ts4 then
					ts4.extend (sec)
				elseif attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 6 then
				if attached ts5 then
					ts5.extend (sec)
				elseif attached ts4 then
					ts4.extend (sec)
				elseif attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 7 then
				if attached ts6 then
					ts6.extend (sec)
				elseif attached ts5 then
					ts5.extend (sec)
				elseif attached ts4 then
					ts4.extend (sec)
				elseif attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 8 then
				if attached ts7 then
					ts7.extend (sec)
				elseif attached ts6 then
					ts6.extend (sec)
				elseif attached ts5 then
					ts5.extend (sec)
				elseif attached ts4 then
					ts4.extend (sec)
				elseif attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			when 9 then
				if attached ts8 then
					ts8.extend (sec)
				elseif attached ts7 then
					ts7.extend (sec)
				elseif attached ts6 then
					ts6.extend (sec)
				elseif attached ts5 then
					ts5.extend (sec)
				elseif attached ts4 then
					ts4.extend (sec)
				elseif attached ts3 then
					ts3.extend (sec)
				elseif attached ts2 then
					ts2.extend (sec)
				elseif attached ts1 then
					ts1.extend (sec)
				else
					extend (sec)
				end
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_document: BOOLEAN = True
			-- Is Current a document? Of course it is

	tags: TWO_WAY_LIST [AEL_USML_TAG]

	title: STRING

--|========================================================================
feature -- Status setting
--|========================================================================

	set_title (v: STRING)
		do
 			title.wipe_out
 			title.append (v)
		end

	set_default_format_from_tag (tag: AEL_USML_TAG)
			-- Set the default format for element level 'lvl' where 
			-- format includes values from 'tag'
		local
			bf, itf, uf, stf: BOOLEAN
			ps, cv, vs: INTEGER
			ff, rc: STRING
			fmt: AEL_SDOC_FORMAT
			lvl: INTEGER
		do
			lvl := tag.format_level
			fmt := default_format_by_level (lvl)
			if attached tag.text_font_family as tff then
				ff := tff
			else
				ff := dflt_font_family (lvl)
			end
			ps := tag.text_point_size
			cv := tag.text_case_value
			vs := tag.text_vertical_offset
			bf := tag.text_is_bold
			itf := tag.text_is_italic
			uf := tag.text_is_underlined
			stf := tag.text_is_strike_through
			if attached tag.text_rgb_color as c then
				rc := c
			else
				rc := ""
			end
			fmt.set_char_values (ff, rc, ps, cv, vs, bf, itf, uf, stf)
		end

	set_default_font_family (v: STRING; lvl: INTEGER)
			-- Set the default font family for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_font_family (v)
		end

	set_default_point_size (v, lvl: INTEGER)
			-- Set the default point size for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_point_size (v)
		end

	set_default_bold (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default bold for element level 'lvl' to 'tf'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_bold (tf)
		end

	set_default_italic (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default italic for element level 'lvl' to 'tf'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_italic (tf)
		end

	set_default_underlined (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default underlined for element level 'lvl' to 'tf'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_underlined (tf)
		end

	set_default_case_value (v, lvl: INTEGER)
			-- Set the default case value for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_case_value (v)
		end

	set_default_color (v: STRING; lvl: INTEGER)
			-- Set the default color for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_text_color (v)
		end

	--|--------------------------------------------------------------

	set_default_left_margin (v, lvl: INTEGER)
			-- Set the default left margin for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_left_margin (v)
		end

	set_default_right_margin (v, lvl: INTEGER)
			-- Set the default right margin for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_right_margin (v)
		end

	set_default_top_margin (v, lvl: INTEGER)
			-- Set the default top margin for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_top_margin (v)
		end

	set_default_bottom_margin (v, lvl: INTEGER)
			-- Set the default bottom margin for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_bottom_margin (v)
		end

	set_default_alignment_value (v, lvl: INTEGER)
			-- Set the default alignment value for element level 'lvl'
		local
			fmt: AEL_SDOC_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_alignment_value (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

	most_recent_section (lvl: INTEGER): detachable AEL_STRUCTURED_DOC_SECTION
			-- Most recent section at level 'lvl', since most recent 
			-- section lvl-1, if any
		local
			tsec: AEL_STRUCTURED_DOC_SECTION
			oc: CURSOR
			tlevel: INTEGER
			done: BOOLEAN
		do
			if not is_empty then
				tsec := last
				tlevel := tsec.level
				if tlevel = lvl then
					Result := tsec
				elseif tlevel > lvl then
					oc := cursor
					finish
					from back
					until before or done or attached Result
					loop
						tsec := last
						tlevel := tsec.level
						if tlevel = lvl then
							Result := tsec
						elseif tlevel > lvl then
							back
						else
							done := True
						end
					end
					if valid_cursor (oc) then
						go_to (oc)
					end
				else
				end
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	tags_out: STRING
		local
			tag: AEL_USML_TAG
		do
			create Result.make (2048)
			from tags.start
			until tags.after
			loop
				tag := tags.item
				Result.append (tag.tag_text + "%N")
				tags.forth
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	next_line_iq (str: STRING; lp: CELL [INTEGER]): STRING
			-- Substring from the string 'str' that includes
			-- the characters up to but not including the next
			-- newline character (quoted or NOT).
			-- The line position is stored in 'lp' such that it denotes
			-- the read start position on entry and one character past
			-- the end position on exit.
			-- If there are no more characters between the starting
			-- line position and the end of the string, then Result
			-- is empty and the line position is set to zero.
			-- If the end of the string is found before a newline, then
			-- the characters from the starting point to the end of
			-- the string are returned and the line position set to
			-- one greater than the length of the string.
		require
			stream_exists: str /= Void
			position_exists: lp /= Void
			valid_start: lp.item > 0 and lp.item <= str.count
		local
			spos, epos: INTEGER
		do
			spos := lp.item
			epos := str.index_of ('%N', spos)
			if epos = 0 then
				-- Not found
				epos := str.count + 1
			end
			if epos = spos then
				-- An empty line (but a line just the same)
				Result := ""
				lp.put (epos + 1)
			else
				Result := str.substring (spos, epos - 1)
				if Result.is_empty then
					lp.put (0)
				else
					lp.put (epos + 1)
				end
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_STRUCTURED_DOCUMENT
