class AEL_STATE

inherit
	ANY
		redefine
			is_equal, out
		end

create
	make, make_default

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make (v: INTEGER; lbl: STRING)
			-- Create Current with value 'v' and label 'lbl'
		do
			value := v
			label := lbl
		end

	make_default
		do
			is_dummy := True
			make (0, "NO SUCH STATE")
		end

--|========================================================================
feature -- Status
--|========================================================================

	value: INTEGER
			-- State number

	label: STRING
			-- State name

	--|--------------------------------------------------------------

	step: detachable ANY
			-- Optional context-dependent associated step

	is_dummy: BOOLEAN

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of Current
		do
			create Result.make (32)
			Result.append ("State (" + value.out + ") name=")
			Result.append_string (label)
		end

	--|--------------------------------------------------------------

	is_equal (other: like Current): BOOLEAN
			-- Is the label (state name) the same as other?
		do
			Result := label.is_equal (other.label)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_step (v: ANY)
			-- Set optional context-dependent associated step to 'v'
		do
			step := v
		end

	--|--------------------------------------------------------------
invariant
	label_exists: label /= Void and then not label.is_empty

end -- class AEL_STATE
