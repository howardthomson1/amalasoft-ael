class AEL_STATE_CONST

inherit
	ANY
		rename
			copy as asc_copy,
			is_equal as asc_is_equal
		end

 --|========================================================================
feature {NONE}
 --|========================================================================

 -- These features must be fixed somehow, RFO

	exit_from_application	: INTEGER = -2
	return_to_previous	: INTEGER = -1
	retrace_to_prior	: INTEGER = -4
	all_states		: INTEGER = 0

	--|--------------------------------------------------------------

	initial: INTEGER
end -- class AEL_STATE_CONST
