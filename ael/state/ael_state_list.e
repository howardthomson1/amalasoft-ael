class AEL_STATE_LIST

inherit
	LINKED_LIST [AEL_STATE]
		rename
			make as ll_make,
			extend as ll_extend
		redefine
			ll_make, default_create
		end

create
	make, make_from_sequences

create {LINKED_LIST}
	ll_make

 --|========================================================================
feature
 --|========================================================================

	make
		do
			default_create
			ll_make
			compare_objects
		end

	--|--------------------------------------------------------------

	make_from_sequences (s: ARRAY [LINKED_LIST [AEL_STATE]])
			-- Construct the state list from a collection of
			-- state sequences
			-- The states in the sequences need not be unique, but
			-- we ensure that they are not duplicated in the reusulting
			-- state list.
		require
			sequences_exist: s /= Void
		local
			l: LINKED_LIST [AEL_STATE]
			st: AEL_STATE
			i, lim: INTEGER
		do
			make
			lim := s.count
			from i := 1
			until i > lim
			loop
				l := s.item (i)
				if l /= Void and then not l.is_empty then
					from l.start
					until l.exhausted
					loop
						st := l.item
						if st /= Void and then not has_state (st) then
							extend (st)
						end
						l.forth
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	default_create
		do
			create state_names.make (17)
		end

	ll_make
		do
			default_create
			Precursor
		end

	--|--------------------------------------------------------------

	has_state (st: like item): BOOLEAN
		require
			state_exists: st /= Void
		do
			start
			search (st)
			Result := not exhausted
			start
		end

	--|--------------------------------------------------------------

	extend (v: like item)
		do
			if not has (v) then
				ll_extend (v)
--RFO 				state_names.extend (v.label, v.value)
				state_names.force (v.label, v.value)
			end
		end

--|========================================================================
feature {AEL_STATE_CONST} -- Implementation
--|========================================================================

	state_names: HASH_TABLE [STRING, INTEGER]

end -- class AEL_STATE_LIST
