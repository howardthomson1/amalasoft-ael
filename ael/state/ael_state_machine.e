class AEL_STATE_MACHINE

inherit
	ARRAY [HASH_TABLE [INTEGER, STRING]]
		rename
			make as array_make,
			put as array_put,
			is_equal as array_is_equal,
			copy as array_copy
		redefine
			out
		select
		array_copy, array_is_equal
		end
	EXCEPTIONS
		rename
			is_equal as exc_is_equal,
			copy as exc_copy
		undefine
			out
		end
	AEL_STATE_CONST
		undefine
			out
		end
	AEL_SPRT_MULTI_FALLIBLE
		undefine
			out
		end

create
	make, make_no_retrace

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make
			-- Create current with retrace enabled
		local
			ti: like default_item
		do
			create ti.make (0)
			default_item := ti

			build_states_list
			states_count := states_list.count
			create state_history.make
			create transitions.make (states_count)
			create transitions_by_label.make (states_count)
			initial := state_by_name ("initial")
			make_filled (ti, 1, states_count)
			--Precursor (dv, mini, maxi)
			initialize

		end

	--|--------------------------------------------------------------

	make_no_retrace
			-- Create current with retrace disabled
		do
			make
			retrace_disabled := True
		end

	--|--------------------------------------------------------------

	build_states_list
			-- Create states_list
		do
			create states_list.make
			-- Descendent must redefine and initialize
		ensure
			exists: states_list /= Void
		end

 --|========================================================================
feature {NONE} -- Intialization
 --|========================================================================

	default_item: like item
			-- Non-void item used to fill transitions list

	--|--------------------------------------------------------------

	initialize
		require
			states_list_exists: states_list /= Void
			valid_state_count: states_count > 0
			not_done: not transitions_are_initialized
		local
			i: INTEGER
			new_entry: like item
			tl: like transitions_for_label
		do
			current_transitions := Void
			last_transit_label := Void
			private_current_state := 0
			prior_state := 0
			-- Cannot clear all on an array thanks to new void-safe rules
			-- array was created filled with default_item
			--clear_all

			from	
				i := 1
			until
				i > states_count
			loop
				create tl.make
				transitions.extend (tl)
				create new_entry.make (5)
				array_put (new_entry, i)
				i := i + 1
			end
			initialize_transitions
		end

	--|--------------------------------------------------------------

	initialize_transitions
		require
			states_list_exists: states_list /= Void
			not_empty: not states_list.is_empty
			uninitialized: not transitions_are_initialized
		do
			transitions_are_initialized := True
		ensure
			initialized: transitions_are_initialized
		end

	--|--------------------------------------------------------------

	set_initial_state (initial_state: INTEGER)
			-- Set state to `initial_state'. 
		do
			push_state (initial_state)
			prior_state := initial_state
		end

	--|--------------------------------------------------------------

	put_unique (ost, nst: INTEGER; label: STRING)
			-- Specify transition `label' going from `ost to `nst'
			-- If there are no transitions from 'ost' via 'label', already
		do
			if not state_has_transition_for_label (ost, label) then
				put (ost, nst, label)
			end
		end

	--|--------------------------------------------------------------

	put (ost, nst: INTEGER; lbl: STRING)
			-- Specify transition `label' going from `ost to `nst'.
		require
			label_exists: lbl /= Void
			transition_is_unique: not state_has_transition_for_label(ost,lbl)
			has_state: attached item (ost)
		local
			tt: AEL_STATE_TRANSITION
			--tl: like transitions_for_label
		do
			check attached item (ost) as ti then
				ti.put (nst, lbl)
				create tt.make (state_name(ost), state_name(nst), lbl)
				add_transition (ost, nst, tt)
			end
		end

	--|--------------------------------------------------------------

	add_transition (ost, nst: INTEGER; tt: AEL_STATE_TRANSITION)
		require
			transition_exists: tt /= Void
		local
			tl: like transitions_for_label
		do
			tt.set_integer_states (ost, nst)
			transitions.i_th (ost).extend (tt)

			tl := transitions_by_label.item (tt.label)
			if tl = Void then
				create tl.make
				transitions_by_label.put (tl, tt.label)
			end
			tl.extend (tt)
		end

--|========================================================================
feature -- Status
--|========================================================================

	state_name (v: INTEGER): STRING
		local
--RFO 			ast: AEL_STATE
--RFO 			oc: CURSOR
			ts: detachable STRING
		do
			if v > 0 then
				if not attached states_list as tsl then
					check
						void_safe_error: False
					end
				else
					ts := tsl.state_names.item (v)
--RFO 				sl := tsl
--RFO 				oc := tsl.cursor
--RFO 				from tsl.start
--RFO 				until tsl.exhausted or (Result /= Void)
--RFO 				loop
--RFO 					ast := tsl.item
--RFO 					if ast.value = v then
--RFO 						Result := ast.label
--RFO 					end
--RFO 					tsl.forth
--RFO 				end
--RFO 				tsl.go_to (oc)
				end
			else
				if v = exit_from_application then
					ts := "exit_from_application"
				elseif v = return_to_previous then
					ts := "return_to_previous"
				elseif v = retrace_to_prior then
					ts := "retrace_to_prior"
				elseif v = all_states then
					ts := "all_states"
				end
			end
			if attached ts as lts then
				Result := lts
			else
				Result := v.out
			end
		ensure
			Result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	states_list: AEL_STATE_LIST

	--|--------------------------------------------------------------

	state_by_name (v: STRING): INTEGER
		require
			name_exists: v /= Void
		local
			oc: CURSOR
		do
			if v.is_equal("exit_from_application") then
				Result := exit_from_application
			elseif v.is_equal("return_to_previous") then
				Result := return_to_previous
			elseif v.is_equal("retrace_to_prior") then
				Result := retrace_to_prior
			elseif v.is_equal("all_states") then
				Result := all_states
			else
				if not attached states_list as tsl then
					check
						void_safe_error: False
					end
				else
					oc := tsl.cursor
					from tsl.start
					until tsl.exhausted or (Result /= 0)
					loop
						if tsl.item.label.is_equal (v) then
							Result := tsl.item.value
						end
						tsl.forth
					end
					tsl.go_to (oc)
				end
			end
		end

	--|--------------------------------------------------------------

	current_transitions: detachable like item
			-- Current transition table

 --|========================================================================
feature {NONE} -- Implementation status
 --|========================================================================

	private_current_state: INTEGER
	prior_state: INTEGER

	states_count: INTEGER

	--|--------------------------------------------------------------

	transitions: ARRAYED_LIST [LINKED_LIST [AEL_STATE_TRANSITION]]
		note
			option: stable
			attribute
		end

	transitions_by_label: HASH_TABLE [LINKED_LIST [AEL_STATE_TRANSITION], STRING]
			-- The collection of state transitions for which
			-- the given string is the transition label.
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	has_transition (fs, ts, lb: STRING): BOOLEAN
		require
			from_state_exists: fs /= Void
			to_state_exists: ts /= Void
			label_exists: lb /= Void
		local
			ta: like transitions
			tl: like transitions_for_label
			t: AEL_STATE_TRANSITION
			oc: CURSOR
		do
			create t.make (fs, ts, lb)
			ta := transitions
			oc := ta.cursor
			from ta.start
			until ta.exhausted or Result
			loop
				tl := ta.item
				if tl /= Void then
					Result := tl.has (t)
				end
				ta.forth
			end
			ta.go_to (oc)
		end

	--|--------------------------------------------------------------

	state_has_transition_for_label (fs: INTEGER; lb: STRING): BOOLEAN
		require
			label_exists: lb /= Void
		do
			Result := attached transition_for_label (fs, lb)
		end

	--|--------------------------------------------------------------

	transition_for_label (ost: INTEGER; lb: STRING): detachable AEL_STATE_TRANSITION
			-- If the given state already has a transition for the given
			-- label, then return the state to which it would transit,
			-- else return Void
		require
			label_exists: lb /= Void
		local
			oc: CURSOR
		do
			if ost > 0 then
				if attached transitions.i_th (ost) as tl then
					oc := tl.cursor
					from tl.start
					until tl.exhausted or Result /= Void
					loop
						if tl.item.label.is_equal (lb) then
							Result := tl.item
						else
							tl.forth
						end
					end
					tl.go_to (oc)
				end
			end
		end

	--|--------------------------------------------------------------

	transitions_for_label (v: STRING): detachable LINKED_LIST [AEL_STATE_TRANSITION]
			-- The collection of transitions for which the given string
			-- is the transition label.
			-- If no transitions exists for the given label, then Void
		do
			if v /= Void and then not v.is_empty then
				Result := transitions_by_label.item (v)
			end
		end

	--|--------------------------------------------------------------

	prior_states_for_label (v: STRING): like states_for_label
			-- The collection of States for which the given label
			-- effects the transition.
			-- If no transition exists for the given label, then Empty
		do
			Result := states_for_label (v, True)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	next_states_for_label (v: STRING): like states_for_label
			-- The collection of States FROM which the given label
			-- effects the transition.
			-- If no transition exists for the given label, then Empty
		do
			Result := states_for_label (v, False)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	states_for_label (v: STRING; is_prev: BOOLEAN): LINKED_LIST [ INTEGER ]
			-- The collection of States (previous if is_prev, else next)
			-- for which the given label effects the transition.
			-- If no transitions exists for the given label, then Empty
		local
			tl: like transitions_for_label
			oc: CURSOR
		do
			create Result.make
			if v /= Void and then not v.is_empty then
				tl := transitions_for_label (v)
				if tl /= Void and then not tl.is_empty then
					oc := tl.cursor
					from tl.start
					until tl.exhausted
					loop
						if is_prev then
							Result.extend (tl.item.prev_state)
						else
							Result.extend (tl.item.next_state)
						end
						tl.forth
					end
				end
			end
		ensure
			exists: Result /= Void
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	retrace_disabled: BOOLEAN

	is_verbose: BOOLEAN

	report_warnings_as_errors: BOOLEAN

	transitions_are_initialized: BOOLEAN
			-- Have transitions been initialized?

	--|--------------------------------------------------------------

	state: INTEGER
			-- Current state
		do
			if retrace_disabled then
				Result := private_current_state
			else
				Result := top_state
			end
		end

	current_state_name: STRING
		do
			Result := state_name (state)
		end

	--|--------------------------------------------------------------

	previous_state: INTEGER
		local
			ns: INTEGER
		do
			if retrace_disabled then
				Result := prior_state
			else
				ns := state_history.count
				if ns > 1 then
					Result := state_history.i_th (ns - 1)
				else
					Result := top_state
				end
			end
		end

	--|--------------------------------------------------------------

	last_transit_label: detachable STRING

	state_history: TWO_WAY_LIST [ INTEGER ]

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_verbose
		do
			is_verbose := True
		end

	disable_verbose
		do
			is_verbose := False
		end

	push_state (st: INTEGER)
		do
			if (retrace_disabled) then
				prior_state := private_current_state
				private_current_state := st
			else
				state_history.extend (st)
			end
			if item (state) /= default_item then
				current_transitions := item (state)
			else
				current_transitions := Void
			end
		end

	--|--------------------------------------------------------------

	pop_state
		require
			history_not_empty: retrace_disabled or state_history.count > 1
		local
			temp: INTEGER
		do
			if retrace_disabled then
				temp := prior_state
				prior_state := private_current_state
				private_current_state := temp
			else
				state_history.finish
				state_history.remove
			end
			if item (state) /= default_item then
				current_transitions := item (state)
			else
				current_transitions := Void
			end
		end

	--|--------------------------------------------------------------

	top_state: INTEGER
		require
			history_not_empty: retrace_disabled or not state_history.is_empty
		do
			if retrace_disabled then
				Result := private_current_state
			else
				Result := state_history.last
			end
		end

	--|--------------------------------------------------------------

	purge_states
		do
			from
			until state_history.count = 1
			loop
				pop_state
			end
		end

	--|--------------------------------------------------------------

	transit (label: STRING)
			-- Effect a transition from current state to next state via 
			-- 'label'
		require
			has_transitions: current_transitions /= Void
		local
			temp: INTEGER
			msg: STRING
			is_return: BOOLEAN
			ts: like transition_for_label
		do
			if not attached current_transitions as lct then
				-- ERROR see precondition
			else
				last_transit_label := label
				if not lct.has (label) then
					msg := apf.aprintf (
						"No transit from %%s via %%s%N",
						<<state_name (state), label>>)
					print_verbose (msg)
					if label /~ "initial" or report_warnings_as_errors then
						set_error_message (msg)
					end
				else
					temp := lct.item (label)
					print_verbose (
						apf.aprintf (
							"Transit from %%s to %%s, via %%s%N",
							<<state_name (state), state_name (temp), label>>))
					if temp = Return_to_previous then
						is_return := True
						pop_state
					elseif temp = Retrace_to_prior then
						is_return := True
						pop_state
					elseif temp = Exit_from_application then
						die (0)
					elseif temp = 0 then
						-- How did we get here? this is for the debugger to stop
						temp := 0
					else
						push_state (temp)
					end
				end
				if (state_change_notify_proc /= Void and then
					(previous_state /= temp) or is_return)
				 then
					 ts := transition_for_label (previous_state, label)
					if ts /= Void then
						ts.mark_transition
					end
					if state_change_notify_proc /= Void then
						state_change_notify_proc.call (
							[previous_state, state, label])
					end
				end
			end
		end

 --|========================================================================
feature -- Agent setting
 --|========================================================================

	set_state_change_notify_proc (v :like state_change_notify_proc)
		do
			if v /= Void then
				state_change_notify_proc := v
			end
		end

	state_change_notify_proc: detachable PROCEDURE [INTEGER, INTEGER, STRING]
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	set_verbose_output_proc (v: like verbose_output_proc)
		do
			if v /= Void then
				verbose_output_proc := v
			end
		end

	verbose_output_proc: detachable PROCEDURE [STRING]
		note
			option: stable
			attribute
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING
			-- String representation of Current
		do
			create Result.make (2048)
			Result.append (states_out)
			Result.append (transitions_out)
		end

	--|--------------------------------------------------------------

	states_out: STRING
			-- String representation of states list
		local
			oc: CURSOR
			sl: like states_list
		do
			sl := states_list
			oc := sl.cursor
			create Result.make (1024)
			from sl.start
			until sl.exhausted
			loop
				Result.append (sl.item.out)
				Result.extend ('%N')
				sl.forth
			end
			sl.go_to (oc)
		end

	--|--------------------------------------------------------------

	dump_transitions: STRING
			-- String representation of transitions table
		obsolete "Use 'transitions_out' instead"
		do
			Result := transitions_out
		end

	transitions_out: STRING
			-- String representation of transitions table
		local
			trl: like transitions_for_label
			tr: like transition_for_label
			lim: INTEGER
			fmt_str: STRING
			tlc, tc: CURSOR
		do
			create Result.make (1024)

			fmt_str := "%%5s    %%-14s -- %%=14s -->     %%s%N"
			Result.append (
				apf.aprintf (fmt_str, << "", "From:", "via", "To:" >>))
			Result.append (
				apf.aprintf ("------%%-14s----%%=14s---------%%22s%N",
				<< "----------------------",
				"--------------",
				"----------------------"
				>>))

			lim := transitions.count
			tlc := transitions.cursor
			from transitions.start
			until transitions.exhausted
			loop
				trl := transitions.item
				if trl /= Void then
					tc := trl.cursor
					from trl.start
					until trl.exhausted
					loop
						tr := trl.item
						if tr /= Void then
							Result.append (
								apf.aprintf (fmt_str,
								<< tr.transit_count.out,
								tr.from_state,
								tr.label,
								tr.to_state
								>>))
						end
						trl.forth
					end
					trl.go_to (tc)
				end
				transitions.forth
			end
			transitions.go_to (tlc)
		end

	--|--------------------------------------------------------------

	print_verbose (v: STRING)
		do
			if is_verbose then
				if verbose_output_proc /= Void then
					verbose_output_proc.call ([v])
				else
					print (v)
				end
			end
		end

end -- class AEL_STATE_MACHINE
