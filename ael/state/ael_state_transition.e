class AEL_STATE_TRANSITION

inherit
	AEL_STATE_CONST

create
	make

 --|========================================================================
feature {NONE}
 --|========================================================================

	make (fs, ts, tl: STRING)
		require
			from_state_exists: fs /= Void
			to_state_exists: ts /= Void
			label_exists: tl /= Void
		do
			create from_state.make (fs.count)
			from_state.append (fs)
			create to_state.make (ts.count)
			to_state.append (ts)
			create label.make (tl.count)
			label.append (tl)
		end

 --|========================================================================
feature
 --|========================================================================

	is_equal (other: like Current): BOOLEAN
			-- The transition is equal if all attributes are equal
		do
			Result :=
				from_state.is_equal (other.from_state) and
				to_state.is_equal (other.to_state) and
				label.is_equal (other.label)
		end

	--|--------------------------------------------------------------

	set_integer_states (p, n: INTEGER)
		require
			prev_valid: is_valid_integer_state (p)
			next_valid: is_valid_integer_state (n)
		do
			prev_state := p
			next_state := n
		end

	--|--------------------------------------------------------------

	from_state: STRING
	to_state: STRING
	label: STRING

	prev_state: INTEGER
	next_state: INTEGER

	transit_count: INTEGER
			-- Number of times, in this program execution
			-- that this transition has taken place

 --|========================================================================
feature {AEL_STATE_MACHINE}
 --|========================================================================

	mark_transition
			-- Increment the number of times, in this program execution
			-- that this transition has taken place
		do
			transit_count := transit_count + 1
		end

--|========================================================================
feature -- Add Comment
--|========================================================================

	is_valid_integer_state (v: INTEGER): BOOLEAN
			-- Is 'v' a valid integer state value?
		do
			Result := v >= 0
			if not Result then
				inspect v
				when
					exit_from_application, return_to_previous, retrace_to_prior
				 then
					 Result := True
				else
				end
			end
		end

--|--------------------------------------------------------------
invariant
--RFO 	transits: not from_state.is_equal(to_state)
	label_exists: label /= Void and then not label.is_empty

end -- class AEL_STATE_TRANSITION
