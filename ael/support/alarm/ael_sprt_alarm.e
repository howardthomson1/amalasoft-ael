class AEL_SPRT_ALARM

inherit
	AEL_SPRT_TIMER_CONTROL

create
	make_with_interval

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_interval (v: INTEGER)
			-- Create an alarm with the given number of milliseconds.
			-- Interval should be an even multiple of K_timer_interval
			-- milliseconds
		do
			default_create
			create actions
			interval := timer.milliseconds_to_ticks (v)
			timer.register_alarm (Current)
		end

--|========================================================================
feature {AEL_SPRT_TIMER} -- Notification
--|========================================================================

	tick
			-- Called by central timer at each clock tick
		do
			if not is_disabled then
				tick_count := tick_count + 1
				if tick_count >= interval then
					respond_to_alarm
					tick_count := 0
				end
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	interval: INTEGER
			-- Number of ticks before action should be taken

	is_disabled: BOOLEAN
			-- Is Current temporary disabled?

 --|------------------------------------------------------------------------

	actions: ACTION_SEQUENCE [ TUPLE ]
			-- Actions to perform on each wake-up call

--|========================================================================
feature -- Status setting
--|========================================================================

	disable
			-- Disable this alarm; if disabled, not actions are performed
		do
			tick_count := 0
			is_disabled := True
		end

 --|------------------------------------------------------------------------

	pause
			-- Temporarily disable this alarm, retaining its current tick count
		do
			is_disabled := True
		end

 --|------------------------------------------------------------------------

	enable, resume
			-- Restart this alarm
		do
			is_disabled := False
		end

 --|------------------------------------------------------------------------

	unregister
			-- Tell the central timer to remove this alarm from its 
			-- collection permanently
		do
			timer.unregister_alarm (Current)
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	tick_count: INTEGER
			-- Number of times in this cycle that this alarm has been ticked

--|------------------------------------------------------------------------

	respond_to_alarm
			-- On number of ticks reaching the count representing the defined
			-- interval, execute all defined actions associated with this alarm
		do
			from actions.start
			until actions.exhausted
			loop
				actions.item.call ([])
				actions.forth
			end
		end

end -- class AEL_SPRT_ALARM
