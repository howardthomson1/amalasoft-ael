class AEL_SPRT_TIMER
  -- A central timer intended to be used for multiple clients
  -- A single instance of this class should be onced in a class
  -- inherited by all possible client classes.
  -- Each client that needs this service must create an alarm
  -- object (AEL_SPRT_ALARM) with its own interval.  Clients can have
  -- as many alarm objects as they wish.

inherit
	THREAD_CONTROL
	MEMORY
		redefine
			dispose
		end

create
	make_with_interval

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_interval (v: INTEGER)
			-- Create the timer with the given minimum interval (aka granularity)
		do
			interval := v
			create termination_flag.put (False)
			create timer_thread.make (termination_flag, agent do_tick)
		end

--|========================================================================
feature -- Control
--|========================================================================

	termination_flag: CELL [BOOLEAN]

	start
		require
			not_started: not is_started
		do
			timer_thread.set_interval (interval)
			timer_thread.launch
			is_started := True
		ensure
			started: is_started
		end

 --|------------------------------------------------------------------------

	stop
		do
			is_stopped := True
			termination_flag.put (True)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_started: BOOLEAN
			-- Has the timer started yet?

	is_stopped: BOOLEAN
			-- Has a stop request been made?

 --|------------------------------------------------------------------------

--	timeout: EV_TIMEOUT
	timer_thread: AEL_SPRT_TIMER_THREAD

	interval: INTEGER
			-- Milliseconds between ticks

	ticks: INTEGER_64
			-- Total number of ticks

 --|------------------------------------------------------------------------

	milliseconds_to_ticks (v: INTEGER): INTEGER
			-- Number of milliseconds per timer interval
		do
			Result := v // interval
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	do_tick
		do
			--io.put_string("*")
			ticks := ticks + 1
			if not alarms.is_empty then
				update_alarms
			end
		end

	--|--------------------------------------------------------------

	dispose
			-- Kill off child thread when Current is garbage collected
		do
			stop
			join_all
		end

--|========================================================================
feature {AEL_SPRT_ALARM} -- Alarm registration
--|========================================================================

	register_alarm (v: AEL_SPRT_ALARM)
		require
			alarm_exists: v /= Void
			is_new: not alarm_is_registered(v)
		do
      alarms.extend (v)
		ensure
			registered: alarm_is_registered(v)
		end

 --|------------------------------------------------------------------------

	unregister_alarm (v: AEL_SPRT_ALARM)
		require
			alarm_exists: v /= Void
			is_registered: alarm_is_registered(v)
		local
			oc: CURSOR
		do
			oc := alarms.cursor
			alarms.start
			alarms.search (v)
			alarms.remove
			alarms.go_to (oc)
		ensure
			unregistered: not alarm_is_registered(v)
		end

 --|------------------------------------------------------------------------

	alarm_is_registered (v: AEL_SPRT_ALARM): BOOLEAN
		do
			Result := alarms.has (v)
		end

--|========================================================================
feature {NONE} -- Alarm implementation
--|========================================================================

	updating: BOOLEAN

 --|------------------------------------------------------------------------

	alarms: like all_mt_alarms
		do
			Result := all_mt_alarms
		end

 --|------------------------------------------------------------------------

	all_mt_alarms: LINKED_LIST [AEL_SPRT_ALARM]
		once ("PROCESS")
			create Result.make
		end

 --|------------------------------------------------------------------------

	update_alarms
		require
			not_updating: not updating
		local
			oc: CURSOR
		do
			updating := True
			oc := alarms.cursor
			from alarms.start
			until alarms.exhausted
			loop
				alarms.item.tick
				alarms.forth
			end
			alarms.go_to (oc)
			updating := False
		end

end -- class AEL_SPRT_TIMER
