class AEL_SPRT_TIMER_CONTROL
-- Control of common central timer

--|========================================================================
feature {NONE} -- Control
--|========================================================================

	timer_start
		do
			if not timer.is_started then
				timer.start
			end
		end

	timer_stop
		do
			timer.stop
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	K_timer_interval: INTEGER = 100
			-- Number of milliseconds per tick in timer

	timer: AEL_SPRT_TIMER
			-- Common central timer
		note
      once_status: global
		once
			create Result.make_with_interval (K_timer_interval)
		end

end -- class AEL_SPRT_TIMER_CONTROL
