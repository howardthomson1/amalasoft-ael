class AEL_SPRT_TIMER_THREAD

inherit
	THREAD
		rename
			make as thread_make
		undefine
			sleep
		end
	EXECUTION_ENVIRONMENT
		rename
			launch as launch_process
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (tc: like termination_flag; ota: like on_tick_action)
		do
			on_tick_action := ota
			termination_flag := tc
			thread_make
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_interval (v: INTEGER)
			-- Set the tick interval to the 'v' milliseonds
		do
			nanoseconds_per_tick := v.to_integer_64 * 1_000_000
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	execute
		do
			from
			until termination_flag.item
			loop
				sleep (nanoseconds_per_tick)
				if on_tick_action /= Void then
					on_tick_action.call ([])
				end
				--io.put_string ("*")
			end
			-- RFO TODO, make msg conditional, maybe a debug
			io.put_string ("%NTerminating timer thread%N")
			exit
		end

	--|--------------------------------------------------------------

	nanoseconds_per_tick: INTEGER_64

--|========================================================================
feature -- Status
--|========================================================================

	interval: INTEGER
			-- Milliseconds between ticks
		do
			Result := (nanoseconds_per_tick // 1_000_000).to_integer
		end

	termination_flag: CELL [BOOLEAN]

	on_tick_action: PROCEDURE

end -- class AEL_SPRT_TIMER_THREAD
