deferred class AEL_SPRT_CLI_CMD
-- Command-line-interface (CLI) command
-- APPLICATION_ENV provided by specific application

inherit
	EXECUTION_ENVIRONMENT
	AEL_SPRT_FALLIBLE
	AEL_SPRT_PLATFORM
   EXCEPTIONS

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (argv: ARRAY [STRING])
			-- Create an instance of this command, for execution
			-- using the given arguments 'argv'
		require
			argv_exist: argv /= Void
		local
			ts: STRING
		do
			if argv.is_empty then
				command := K_cmd_show_usage
			else
				ts := argv.item (1)
				if ts.is_equal ("-h") or ts.is_equal ("--help") then
					command := K_cmd_show_help
				else
					original_user_id := user_id
					parse_arguments (argv)
				end
			end
			if has_error then
				print_error
				if exit_on_error then
					die (1)
				end
			end
		end

 --|------------------------------------------------------------------------

	parse_arguments (argv: ARRAY [STRING])
		require
			argv_exists: argv /= Void
		deferred
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	disable_exit_on_error
		do
			exit_on_error_disabled := True
		end

--|========================================================================
feature -- Status
--|========================================================================

	cmd_name: detachable STRING
			-- Command name (basename)
		note
			option: stable
		deferred
		end

	command: INTEGER
			-- ID of specific command variant to execute

	original_user_id: INTEGER
			-- User ID when the command began

 --|--------------------------------------------------------------

	exit_on_error: BOOLEAN
			-- Should the process exit on an error in the command?
		do
			Result := not exit_on_error_disabled
		end

	exit_on_error_disabled: BOOLEAN
			-- Is exiting on an error in the command disabled?

	--|--------------------------------------------------------------

	is_prepared: BOOLEAN
			-- Has this command been prepared for execution?

	is_executable: BOOLEAN
			-- Is this command currently executable?
		do
			Result := is_prepared and not has_error
		end

 --|--------------------------------------------------------------

	successful: BOOLEAN
			-- Was the most recent execution attempt successful?

	execution_complete: BOOLEAN
			-- Has this command completed execution?

 --|--------------------------------------------------------------

	usage_message: STRING
		deferred
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

 --|--------------------------------------------------------------

	help_message: STRING
		do
			Result := usage_message.twin
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

--|========================================================================
feature -- Execution
--|========================================================================

	verify_is_root
			-- Check that the user ID is root, else error
		do
			if user_id /= 0 then
				set_cmd_error ("You must be root to run this command.", False)
			end
		end

	--|--------------------------------------------------------------

	be_root
			-- Set the effective UID to root for the duration of this
			-- command or until the effective UID is changed again
		do
			if user_id /= 0 then
				set_user_id (0)
				if not setuid_successful then
					set_cmd_error ("Unable to setuid to root.", False)
				end
			end
		end

	--|--------------------------------------------------------------

	be_original
			-- Reset the effective UID to the original one
			-- Assumes current UID is root
		do
			if original_user_id /= 0 then
				set_user_id (original_user_id)
				if not setuid_successful then
					set_cmd_error ("Unable to setuid to original UID.", False)
				end
			end
		end

	--|--------------------------------------------------------------

	prepare
			-- Set up for execution
		require
			no_errors: not has_error
		do
			is_prepared := True
		ensure
			prepared: is_prepared
		end

 --|--------------------------------------------------------------

	frozen execute
		require
			executable: is_executable
		do
			successful := False
			execution_complete := False
			pre_exec
			core_exec
			post_exec
			cleanup
			execution_complete := True
		end

--|========================================================================
feature -- Error handling
--|========================================================================

	is_usage_error: BOOLEAN
			-- Was the most recent error (if any) a usage errror?
			-- If not, then must be an execution error

 --|--------------------------------------------------------------

	set_cmd_error (v: STRING; uf: BOOLEAN)
		require
			exists: v /= Void and then not v.is_empty
		do
			set_error_message (v)
			is_usage_error := uf
		ensure
			is_set: has_error
		end

 --|--------------------------------------------------------------

	set_execution_error (v: STRING)
		require
			exists: v /= Void and then not v.is_empty
		do
			set_error_message (v)
			is_usage_error := False
		ensure
			is_set: has_error
		end

 --|--------------------------------------------------------------

	print_error
		require
			has_error: has_error
		do
			print (error_message)
			io.new_line
			if is_usage_error then
				show_usage
			end
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	show_usage
			-- Echo a usage message to stdout
		do
			print (usage_message)
			io.new_line
		end

	show_help
			-- Echo a more detailed help message to stdout
		do
			print (help_message)
			io.new_line
		end

 --|--------------------------------------------------------------

	pre_exec
			-- Actual execution logic
		do
		end

	core_exec
			-- Actual execution logic
		do
			inspect command
			when K_cmd_show_usage then
				show_usage
				successful := not has_error
				execution_complete := True
			when K_cmd_show_help then
				show_help
				successful := not has_error
				execution_complete := True
			else
			end
		end

	post_exec
			-- Actual execution logic
		do
		end

 --|--------------------------------------------------------------

	cleanup
			-- Post-execution cleanup
		do
		end

--|========================================================================
feature -- External Command Implementation
--|========================================================================

	last_xcmd_exit_code: INTEGER
			-- Exit code of most recently executed external command
			-- If cmd did not launch, then result is 9 (for 'no')
			-- If cmd does not exist, then result is 99 (for 'no no')
		do
			Result := K_ecode_no_launch
			if last_xcmd /= Void then
				Result := K_ecode_no_cmd
				if last_xcmd.launched then
					Result := last_xcmd.exit_code
				end
			end
		end

	last_xcmd: detachable PROCESS
			-- Most recently executed external command, if any
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	run_external_command (
		cl: STRING; d: detachable STRING; cb: PROCEDURE [STRING])
			-- Execute an external command line and capture its exit status
			-- and output (if any)
		require
			command_exists: cl /= Void and then not cl.is_empty
			-- working directory 'd' is optional
			-- Agent (callback) 'cb' is optional
		do
			run_external_command_with_timeout (cl, d, cb, 60_000)
		end

	--|--------------------------------------------------------------

	run_external_command_with_timeout (
		cl: STRING; d: detachable STRING; cb: PROCEDURE [STRING]
		tmo: INTEGER)
			-- Execute an external command line and capture its exit status
			-- and output (if any)
		require
			command_exists: cl /= Void and then not cl.is_empty
			valid_timeout: tmo >= 0 -- 0 denotes use default
			-- working directory 'd' is optional
			-- Agent (callback) 'cb' is optional
		local
			pf: PROCESS_FACTORY
			tx: like last_xcmd
			tov: INTEGER
		do
			if tmo = 0 then
				tov := 60_000
			else
				tov := tmo
			end
			create pf
			tx := pf.process_launcher_with_command_line (cl, d)
			last_xcmd := tx
			if cb /= Void then
				tx.redirect_output_to_agent (cb)
			end
			tx.set_on_fail_launch_handler (agent on_failed_launch)
			tx.launch
			--tx.wait_for_exit
			if tx.launched then
				tx.wait_for_exit_with_timeout (tov)
			end
		end

--|========================================================================
feature {NONE} -- Agents and actions
--|========================================================================

	on_failed_launch
			-- Handle launch failure
		do
			set_error_message ("Failed to launch external command")
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_cmd_show_usage: INTEGER = 1000
	K_cmd_show_help: INTEGER = 1001
	K_cmd_shell: INTEGER = 1002

	K_ecode_no_cmd: INTEGER = 999
			-- If cmd does not exist (for 'no no no')

	K_ecode_no_launch: INTEGER = 99
			-- If cmd did not launch (for 'no no')

end -- class AEL_SPRT_CLI_CMD
