class AEL_SPRT_SHELL_CMD
-- Command for executin and capturing the output of an arbitrary 
-- shell command

inherit
	AEL_SPRT_CLI_CMD
		rename
			make as cli_make,
			arguments as env_arguments
		redefine
			is_executable, core_exec
		end

create
	make, make_from_string

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (nm: STRING; args: ARRAY [STRING])
		do
			cmd_name := nm
			cli_make (args)
		end

	make_from_string (nm: STRING)
		local
			args: ARRAY [STRING]
		do
			cmd_name := nm
			args := << "" >>
			make (nm, args)
		end

	--|--------------------------------------------------------------

	parse_arguments (argv: ARRAY [STRING])
			-- Anything goes
		local
			i, lim: INTEGER
			cli: STRING
		do
			arguments := argv
			if attached cmd_name as cnm then
				cli := cnm.twin
				cmd_line := cli
				lim := argv.upper
				from i := argv.lower
				until i > lim
				loop
					cli.extend (' ')
					cli.append (argv.item (i))
					i := i + 1
				end
			end
			command := K_cmd_shell
		end

--|========================================================================
feature -- Status
--|========================================================================

	cmd_name: detachable STRING
			-- Command name
		note
			option: stable
			attribute
		end

	cmd_line: detachable STRING
			-- Full command line, with args
		note
			option: stable
			attribute
		end

	cmd_output: detachable STRING
		note
			option: stable
			attribute
		end

	arguments: detachable ARRAY [STRING]
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	usage_message: STRING
		do
			Result := " "
		end

	--|--------------------------------------------------------------

	is_executable: BOOLEAN
			-- Is this command currently executable?
		do
			Result := cmd_line /= Void and then not cmd_line.is_empty
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	core_exec
		do
			Precursor
			if not execution_complete then
				create cmd_output.make (1024)
				if attached cmd_line as cli then
					run_external_command (cli, Void, agent on_cmd_output)
					if last_xcmd_exit_code /= 0 then
						set_execution_error ("Shell command failed")
					else
					end
				else
					set_execution_error ("Internal error: cmd_line is Void")
				end
			end
			successful := not has_error
		end

	--|--------------------------------------------------------------

	on_cmd_output (v: STRING)
		do
			if cmd_output /= Void then
				cmd_output.append (v)
			end
		end

end -- class AEL_SPRT_SHELL_CMD
