note
	description: "{
Routines supporting rfc4648 Base32 encoding/decoding
Depends on other AEL_SPRT_* classes
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_BASE32

--|========================================================================
feature -- rfc4648 Base32 encoding
--|========================================================================

	stream_to_base32 (v: STRING_8; bf: BOOLEAN): STRING
			-- rfc4648 base32 encoding of octet stream 'v'
			--
			-- Convert the given octet stream 'v' into its base 32 notation
			-- Octet need to be grouped into sets of 5 to make a 40 bit 
			-- stream segments.
			-- The 40 bit segments are then rearranged into 5-bit quintets
			-- and each quintet is then encoded according to the base32
			-- alphabet.
			--
			-- If 'bf' then pad result to even 8 octet stream and pad 
			-- each segment to even 5-bit byte
		require
			exists: v /= Void and then not v.is_empty
		local
			i, j, lim: INTEGER
			ts: STRING
		do
			lim := v.count
			create Result.make (lim * 2)
			if lim <= 5 then
				-- a single segment
				Result.append (stream_to_base32_segment (v, bf))
			else
				from i := 1
				until i > lim
				loop
					create ts.make (5)
					from j := 0
					until j >= 5 or (i + j) > lim
					loop
						ts.extend (v.item (i+j))
						j := j + 1
					end
					i := i + 5
					Result.append (stream_to_base32_segment (ts, bf))
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	stream_to_base32_segment (v: STRING_8; pf: BOOLEAN): STRING
			-- rfc4648 base32 encoding of 5 byte (40 bit) or shorter
			-- octet stream 'v'
			--
			-- 40 bit segments are arranged into 5-bit quintets and each
			-- quintet is then encoded according to the base32 alphabet.
			--
			-- When streams are not even multiples of 5 octets, padding
			-- occurs as follows:
			--  1  octet  (8 bits):
			--    xxxxxxxx
			--      becomes
			--    xxxxx xxx00 (10 bits)
			--    result is optionally padded to become ??====== (8 chars)
			--  2 octets (16 bits):
			--    xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx x0000 (20 bits)
			--    result is optionally padded to become ????==== (8 chars)
			--  3 octets (24 bits):
			--    xxxxxxxx xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx xxxxx xxxx0 (25 bits)
			--    result is optionally padded to become ?????=== (8 chars)
			--  4 octets (32 bits):
			--    xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx000 (35 bits)
			--    result is optionally padded to become ???????= (8 
			--    chars)
			--
			-- By working with octets, logic avoids endian issues
		require
			exists: v /= Void
			is_segment: v.count <= 5
		local
			i, lim: INTEGER
			bits, ts: STRING
			tn: NATURAL_8
		do
			lim := v.count
			create bits.make (lim * 5)
			create Result.make (lim * 2)
			-- Build the bit stream
			from i := 1
			until i > lim
			loop
				ts := asr.octet_to_binary ((v.item (i).natural_32_code).as_natural_8)
				bits.append (ts)
				i := i + 1
			end
			inspect lim
			when 1 then
				--    xxxxxxxx
				--      becomes
				--    xxxxx xxx00 (10 bits)
				-- 'bits' should have length of 8; Pad to 10 bits
				bits.append ("00")
			when 2 then
				--  2 octets (16 bits):
				--    xxxxxxxx xxxxxxxx
				--      becomes
				--    xxxxx xxxxx xxxxx x0000 (20 bits)
				-- 'bits' should have length of 16; Pad to 20 bits
				bits.append ("0000")
			when 3 then
				--  3 octets (24 bits):
				--    xxxxxxxx xxxxxxxx xxxxxxxx
				--      becomes
				--    xxxxx xxxxx xxxxx xxxxx xxxx0 (25 bits)
				-- 'bits' should have length of 24; Pad to 25 bits
				bits.append ("0")
			when 4 then
			--  4 octets (32 bits):
			--    xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx000 (35 bits)
				-- 'bits' should have length of 32; Pad to 35 bits
				bits.append ("000")
			when 5 then
				-- 'bits' should be complete at 40 and needs no padding
			end
			-- Convert 'bits' to integers representing 5-bit bytes
			lim := bits.count
			from i := 1
			until i > lim
			loop
				ts := bits.substring (i, i + 4)
				tn := asr.binary_string_to_natural_8 (ts) + 1
				Result.extend (base32_alphabet.item (tn))
				i := i + 5
			end
		end

	--|--------------------------------------------------------------

	base32_symbol_to_integer (v: CHARACTER): INTEGER
			-- Integer value of given base32 symbol (aka digit)
		require
			is_base32_symbol: is_base32_symbol (v)
		do
			Result := base32_alphabet.index_of (v, 1) - 1
		end

	--|--------------------------------------------------------------

	base32_to_stream (v: STRING): STRING
			-- rfc4648 base32 stream 'v' decoded
			--
			-- Convert the given base32 string 'v' into its original 
			-- stream
			-- If 'v' is padded, strip any padding in result
			--
			-- Each character in a base32 string encodes (up to) 5 bits of
			-- the original stream.  As such, each 64 bits (8 chars) of 
			-- the base32 string represents 40 bits (5 bytes) of the 
			-- original stream
			--
			-- Decoding occurs in segments of 8 chars, with end-of-stream
			-- special handling for truncated segments (if unpadded)
			--
			-- When streams are not even multiples of 5 octets, padding
			-- occurs as follows:
			--  1  octet  (8 bits):
			--    xxxxxxxx
			--      becomes
			--    xxxxx xxx00 (10 bits)
			--    result is optionally padded to become ??====== (8 chars)
			--  2 octets (16 bits):
			--    xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx x0000 (20 bits)
			--    result is optionally padded to become ????==== (8 chars)
			--  3 octets (24 bits):
			--    xxxxxxxx xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx xxxxx xxxx0 (25 bits)
			--    result is optionally padded to become ?????=== (8 chars)
			--  4 octets (32 bits):
			--    xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
			--      becomes
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx
			--    xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx xx000 (35 bits)
			--    result is optionally padded to become ???????= (8 
			--    chars)
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			ts: STRING
		do
			if v.is_empty then
				Result := ""
			elseif v.count < 8 then
				Result := base32_segment_to_stream (v)
			else
				create Result.make (v.count)
				lim := v.count
				from i := 1
				until i > lim
				loop
					ts := v.substring (i, lim.min (i+7))
					Result.append (base32_segment_to_stream (ts))
					i := i + 8
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	base32_segment_to_stream (v: STRING): STRING
			-- rfc4648 base32 stream 'v' decoded
			--
			-- Convert the given 8-char or shorter base32 string segment 
			-- 'v' into its original stream
			-- If 'v' is padded, strip any padding in result
			--
			-- Each character in a base32 string encodes (up to) 5 bits of
			-- the original stream.  As such, each 64 bits (8 chars) of 
			-- the base32 string represents 40 bits (5 bytes) of the 
			-- original stream
		require
			exists: v /= Void
			small_enough: v.count <= 8
		local
			i, lim: INTEGER
			ti: INTEGER
			ts, seg_str: STRING
			tn: NATURAL_8
			c: CHARACTER
		do
			if v.is_empty then
				Result := ""
			else
				create Result.make (5)
				create seg_str.make (8)
				lim := v.count
				from i := 1
				until i > lim or else v.item (i) = '='
				loop
					c := v.item (i)
					-- 'ti' is 5-bit value
					ti := base32_symbol_to_integer (c)
					-- 'ts' is the binary format string representation 
					-- of the character in 'v' at current position
					ts := asr.octet_to_binary (ti.as_natural_8)
					-- 'ts' has a full 8 bits but only 5 bits of 
					-- information, and needs to be cropped
					ts.keep_tail (5)
					-- Continue to build the bit representation for 
					-- this segment by appending the last character's
					-- binary representation
					seg_str.append (ts)
					i := i + 1
				end
				-- Capture 8-bit substreams from binary format segment
				lim := seg_str.count
				from i := 1
				until i > lim
				loop
					-- If encoding was bit-padded, then there will be
					-- a trailing segment with fewer than 8 bits.  That 
					-- segment should be ignored.
					ts := seg_str.substring (i, lim.min (i + 7))
					if ts.count = 8 then
						tn := asr.binary_string_to_natural_8 (ts)
						Result.extend (tn.to_character_8)
					end
					i := i + 8
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_base32_symbol (v: CHARACTER): BOOLEAN
		do
			Result := base32_alphabet.has (v)
		end

	--|--------------------------------------------------------------

	is_valid_base32_string (v: STRING): BOOLEAN
		local
			i, lim: INTEGER
		do
			if v /= Void and then not v.is_empty then
				Result := True
				lim := v.count
				-- Padding to 8-char boundaries can result in 3, 4, or 6
				-- padding characters at end of string
				if v.ends_with ("===") then
					lim := lim - 3
					if v.ends_with ("====") then
						lim := lim - 1
						if v.ends_with ("======") then
							lim := lim - 2
						end
					end
				end
				from i := 1
				until i > lim or not Result
				loop
					if not is_base32_symbol (v.item (i)) then
						Result := False
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	integer_to_base32 (v: INTEGER): STRING
			-- Convert the given integer into its base 32 notation
		do
			Result := natural32_to_base32 (v.as_natural_32)
		end

	natural32_to_base32 (v: NATURAL_32): STRING
			-- Convert the given unsigned integer into its base 32 notation
		local
			ti: NATURAL_32
			tmps: STRING
			tv, tm: NATURAL_32
		do
			create Result.make (6)
			ti := v
			if ti = 0 then
				Result.extend ('0')
			else
				-- Build the result string backwards, then flip it

				create tmps.make (32)
				from tv := ti
				until tv = 0
				loop
					tm := tv \\ 32
					tmps.extend (base32_alphabet.item ((tm + 1).to_integer_32))
					tv := tv // 32
				end
				Result.append (tmps.mirrored)
			end
		end

	natural64_to_base32 (v: NATURAL_64): STRING
			-- Convert the given unsigned value into its base 32 notation
		local
			tmps: STRING
			tv, tm: NATURAL_64
		do
			create Result.make (6)
			if v = 0 then
				Result.extend ('0')
			else
				-- Build the result string backwards, then flip it

				create tmps.make (32)
				from tv := v
				until tv = 0
				loop
					tm := tv \\ 32
					tmps.extend (base32_alphabet.item ((tm + 1).to_integer_32))
					tv := tv // 32
				end
				Result.append (tmps.mirrored)
			end
		end

	--|--------------------------------------------------------------

	base32_to_integer (v: STRING): INTEGER
			-- Integer value represented by given base32 string
		require
			is_base32_string: is_valid_base32_string (v)
		local
			i, lim: INTEGER
		do
			lim := v.count
			from i := 1
			until i > lim
			loop
				Result := (Result *32) + base32_symbol_to_integer (v.item (i))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	base32_to_natural32 (v: STRING): NATURAL_32
			-- N32 value represented by given base32 string
		require
			is_base32_string: is_valid_base32_string (v)
		local
			i, lim: INTEGER
		do
			lim := v.count
			from i := 1
			until i > lim
			loop
				Result := (Result *32) +
					(base32_symbol_to_integer (v.item (i))).as_natural_32
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	base32_to_natural64 (v: STRING): NATURAL_64
			-- N64 value represented by given base32 string
		require
			is_base32_string: is_valid_base32_string (v)
		local
			i, lim: INTEGER
		do
			lim := v.count
			from i := 1
			until i > lim
			loop
				Result := (Result *32) +
					(base32_symbol_to_integer (v.item (i))).as_natural_64
				i := i + 1
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	-- Per rfc4648
	base32_alphabet: STRING
			-- Characters used in base32 encoding
		once
			Result := Ks_base32_alphabet_rfc4648
		end

	Ks_base32_alphabet_rfc4648: STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

	Kc_base32_pad: CHARACTER = '='

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 002 03-May-2015
--|     Added natural 64 conversions; broke alphabet out to support redefine
--| 001 04-July-2012
--|     Created original module from existing AEL_SPRT_STRING_ROUTINES
--|----------------------------------------------------------------------
--| How-to
--|
--|  Inherit of instantiate using default_create
--|----------------------------------------------------------------------

end -- class AEL_SPRT_BASE32
