--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Routines to support encoding to / decoding from Base64
--|----------------------------------------------------------------------

class AEL_SPRT_BASE64

create
	default_create

 --|========================================================================
feature -- Conversion
 --|========================================================================

	encoded_string (v: STRING; cll: CELL [INTEGER]): STRING
			-- The result of encoding, into base64, the given string
			--
			-- Base64 takes three bytes and encodes them as four bytes,
			-- padding the result to an even four bytes (with '=' chars)
		require
			arg_exists: v /= Void
			continuation_exists: cll /= Void
		local
			i, lim: INTEGER
			byte1, byte2, byte3: INTEGER -- int32 to prevent sign extension
			byte_count, line_len: INTEGER
			ts: STRING
		do
			line_len := cll.item

			lim := v.count
			create Result.make (lim + (lim // 2))

			from  i := 1
			invariant lim = v.count
			until i > lim
			loop
				byte_count := 1

				-- Collect enough bytes (up to three) to encode

				byte1 := v.item (i).code

				if i < v.count then
					i := i + 1
					byte2 := v.item (i).code
					byte_count := byte_count + 1

					if i < v.count then
						i := i + 1
						byte3 := v.item (i).code
						byte_count := byte_count + 1
					end
				end

				ts := bytes_to_string (byte_count, byte1, byte2, byte3)
				if (line_len + ts.count) > K_max_line_len then
					Result.extend ('%N')
					line_len := 0
				end
				Result.append (ts)
				line_len := line_len + ts.count

				i := i + 1
			variant (lim - i) + 1
			end
			cll.put (line_len)
		ensure
			result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	decoded_string (v: STRING): STRING
			-- The result of decoding, from base64, the given string
			-- A legal base64 string has an even multiple of 4 bytes
			-- after allowing for the newline characters at the end
			-- of each line
		require
			valid_string: v /= Void
			legal_length: v.is_empty or v.count >= 4
		local
			byte_count: INTEGER
			pos, lim: INTEGER
			byte1, byte2, byte3, byte4, tmp1, tmp2: INTEGER
			done: BOOLEAN
		do
			lim := v.count
			create Result.make (lim)

			from pos := 0
			invariant lim = v.count
			until (pos >= lim) or done
			loop
				byte_count := 0

				pos := next_decodable_position (v, pos)
				if pos > 0 then
					byte1 := decoded_byte_at_position (v, pos)
					byte_count := byte_count + 1
				end

				pos := next_decodable_position (v, pos)
				if pos > 0 then
					byte2 := decoded_byte_at_position (v, pos)
					byte_count := byte_count + 1
				end

				pos := next_decodable_position (v, pos)
				if pos > 0 then
					if v.item (pos) /= Kc_pad then
						byte3 := decoded_byte_at_position (v, pos)
						byte_count := byte_count + 1
					end
				end

				pos := next_decodable_position (v, pos)
				if pos > 0 then
					if v.item(pos) /= Kc_pad then
						byte4 := decoded_byte_at_position (v, pos)
						byte_count := byte_count + 1
					end
				end

				done := byte_count < 4

				if byte_count > 1 then
					tmp1 := byte1.bit_shift_left (2) & 0xff
					tmp2 := byte2.bit_shift_right (4) & 0x03
					Result.extend ((tmp1 | tmp2).to_character_8)
					if byte_count > 2 then
						tmp1 := byte2.bit_shift_left (4) & 0xff
						tmp2 := byte3.bit_shift_right (2) & 0x0f
						Result.extend ((tmp1 | tmp2).to_character_8)
						if byte_count > 3 then
							Result.extend(
								((byte4 | byte3.bit_shift_left(6))& 0xff).to_character_8)
						end
					end
				end
			--variant (lim - pos) + 1
			end
		ensure
			result_exists: Result /= Void
		end

	--|--------------------------------------------------------------

	process_stream (
		fn, outbuf: detachable STRING; bufsz: INTEGER; is_decode: BOOLEAN)
			-- From the file with the given pathname (fn), process the contents
			-- either encode or decode, depending on the is_decode flag 
			-- value.
			-- If fn is Void or empty, then read input from stdin.
			-- If outbuf is non-Void, append processed stream to outbuf,
			-- else, write processed stream to stdout
			-- Use bufsz to read blocks from the given file (must be even mod 12)
		require
			valid_buffer_size: (bufsz > 0) and (bufsz \\ 12) = 0
		local
			tf: detachable RAW_FILE
			infile: FILE
			buf: STRING
			len_cell: CELL [INTEGER]
			done: BOOLEAN
		do
			if fn = Void or else fn.is_empty then
				infile := io.input
			else
				create tf.make_with_name (fn)
				tf.open_read
				infile := tf
			end

			create len_cell.put (0)

			from infile.read_stream (bufsz)
			until done
			loop
				if is_decode then
					buf := decoded_string (infile.last_string)
				else
					buf := encoded_string (infile.last_string, len_cell)
				end

				if outbuf /= Void then
					outbuf.append (buf)
				else
					print (buf)
				end

				if infile.end_of_file then
					done := True
				else
					infile.read_stream (bufsz)
					if infile.last_string = Void or infile.last_string.is_empty then
						done := True
					end
				end
			end
			if tf /= Void and then tf.is_open_read then
				tf.close
			end
		end

	--|--------------------------------------------------------------

	encode_file (infile, outfile: FILE; rcnt: CELL [INTEGER])
			-- From the given input file, encode the contents into
			-- the given output file.
			-- Returnt the number of input bytes processed into rcnt,
			-- -1 if an error opening files, 0 if no processing occurred
		require
			has_input_file: infile /= Void
			input_file_exists: infile.exists
			input_file_closed: infile.is_closed
			has_output_file: outfile /= Void
			result_count_exists: rcnt /= Void
		local
			buf, infile_basename: STRING
			bufsz: INTEGER
			len_cell: CELL [INTEGER]
			done: BOOLEAN
			nbytes: INTEGER
		do
			rcnt.put (0)
			bufsz := K_dflt_buf_size
			create len_cell.put (0)

			infile_basename := asr.basename (infile.path.name.as_string_8)
			if infile.count = 0 then
				done := True
			else
				infile.open_read
				if not infile.is_open_read then
					rcnt.put (-1)
				else
					outfile.open_write
					if not outfile.is_open_write then
						rcnt.put (-1)
					end
				end
			end
			if rcnt.item < 0 then
				done := True
			end
			if not done then
				infile.read_stream (bufsz)
				outfile.put_string (
					apf.aprintf (
						"begin-base64 %%o %%s%N",
						<< infile.protection, infile_basename >>))
				from
				until done
				loop
					nbytes := nbytes + infile.last_string.count
					buf := encoded_string (infile.last_string, len_cell)
					outfile.put_string (buf)

					if infile.end_of_file then
						done := True
					else
						infile.read_stream (bufsz)
						if infile.last_string = Void or infile.last_string.is_empty then
							done := True
						end
					end
				end
			end
			if infile /= Void and then not infile.is_closed then
				infile.close
			end
			if outfile /= Void and then not outfile.is_closed then
				outfile.put_string ("%N====%N")
				outfile.close
				rcnt.put (nbytes)
			end
		end

	--|--------------------------------------------------------------

	decode_file (infile, outfile: RAW_FILE; rcnt: CELL [INTEGER])
			-- From the given input file, decode the contents into
			-- the given output file.
			-- Return the number of input bytes processed into rcnt,
			-- -1 if an error opening files, 0 if no processing occurred
		require
			has_input_file: infile /= Void
			input_file_exists: infile.exists
			input_file_closed: infile.is_closed
			has_output_file: outfile /= Void
			result_count_exists: rcnt /= Void
		local
			buf, infile_basename, line: STRING
			done: BOOLEAN
			nbytes: INTEGER
		do
			rcnt.put (0)

			if infile.count = 0 then
				done := True
			else
				infile.open_read
				if not infile.is_open_read then
					rcnt.put (-1)
				else
					infile_basename := asr.basename (infile.path.name.as_string_8)
					outfile.open_write
					if not outfile.is_open_write then
						rcnt.put (-1)
					end
				end
			end
			if rcnt.item < 0 then
				done := True
			end
			if not done then
				infile.read_line
				line := infile.last_string
				if line.substring_index("begin-base64",1) /= 1 then
					-- ERROR
					done := True
				else
					from
						infile.read_line
						line := infile.last_string
					until done or (line = Void) or else line.is_empty
					loop
						nbytes := nbytes + line.count
						if (line.count /= K_max_line_len) and then
							line.is_equal ("====")
						 then
							 done := True
						else
							buf := decoded_string (line)
							outfile.put_string (buf)

							if infile.end_of_file then
								done := True
							else
								infile.read_line
								if infile.last_string = Void or else
									infile.last_string.is_empty then
										done := True
								end
							end
						end
					end
				end
			end
			if infile /= Void and then not infile.is_closed then
				infile.close
			end
			if outfile /= Void and then not outfile.is_closed then
				outfile.close
				rcnt.put (nbytes)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_decodable_string (v: STRING): BOOLEAN
			-- Is the given string decodable?
		local
			pos, lim: INTEGER
		do
			if v /= Void and then v.is_empty or ((v.count \\ 4) = 0) then
				Result := True
				if not v.is_empty then
					lim := v.count - 1
					from pos := 1
					invariant lim = (v.count - 1)
					until pos > lim or not Result
					loop
						if not Ks_character_map.has (v.item (pos+1)) then
							Result := False
						else
							pos := pos + 1
						end
					variant (lim - pos) + 2
					end
				end
			end
		end

 --|========================================================================
feature {NONE} -- Support routines
 --|========================================================================

	bytes_to_string (bcnt: INTEGER; byte1, byte2, byte3: INTEGER): STRING
		require
			valid_count: bcnt >= 1 and bcnt <= 3
			valid_bytes: byte1 >= 0 and byte2 >= 0 and byte3 >= 0
		local
			ti: INTEGER
			tmp1, tmp2: NATURAL_8
			ch: CHARACTER
		do
			create Result.make (4)

			tmp1 := byte1.bit_shift_right (2).to_natural_8
			ch := encoded_char_at_position (tmp1)
			Result.extend (encoded_char_at_position (tmp1))

			tmp1 := (byte1 & 0x03).bit_shift_left (4).to_natural_8
			if bcnt > 1 then
				tmp2 := byte2.bit_shift_right (4).to_natural_8
				Result.extend (encoded_char_at_position (tmp1 | tmp2))
				tmp1 := (byte2 & 0x0f).bit_shift_left (2).to_natural_8
				if bcnt > 2 then
					tmp2 := (byte3 & 0xc0).bit_shift_right (6).to_natural_8
					Result.extend (encoded_char_at_position (tmp1 | tmp2))
					ti := byte3 & 0x3f
					Result.extend (encoded_char_at_position (ti.to_natural_8))
				else
					-- Has only two bytes
					Result.extend (encoded_char_at_position (tmp1))
					Result.extend (Kc_pad)
				end
			else -- has only one byte
				Result.extend (encoded_char_at_position (tmp1))
				Result.extend (Kc_pad)
				Result.extend (Kc_pad)
			end
		ensure
			result_exists: Result /= Void
			valid_length: Result.count = 4
		end

	--|--------------------------------------------------------------

	encoded_char_at_position (b: NATURAL_8): CHARACTER
			-- the encoded character value, from the character map,
			-- of the given byte value
		local
			i: INTEGER
		do
			i := b + 1
			Result := Ks_character_map.item (i)
		end

	--|--------------------------------------------------------------

	decoded_byte_at_position (v: STRING; pos: INTEGER): INTEGER
			-- the decoded byte at the given position of the given string
		require
			string_exists: v /= Void
			valid_position: pos <= v.count
		local
			i: INTEGER
			ch: CHARACTER
		do
			ch := v.item (pos)
			i := Ks_character_map.index_of (ch, 1) - 1
			Result := i
		end

	--|------------------------------------------------------------------------

	next_decodable_position (v: STRING; from_pos: INTEGER): INTEGER
			-- Next (1-based) index
			-- Zero result denotes no decodable positions left
		require
			string_exists: v /= Void
			valid_position: from_pos <= v.count
		local
			pos, lim: INTEGER
			found: BOOLEAN
		do
			lim := v.count - 1
			from pos := from_pos
			invariant lim = (v.count - 1)
			until pos > lim or found
			loop
				if Ks_character_map.has (v.item (pos+1)) then
					Result := pos + 1
					found := True
				end
				pos := pos + 1
			variant (lim - pos) + 2
			end
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	Kc_pad: CHARACTER = '='

	K_max_line_len: INTEGER = 60
	K_dflt_buf_size: INTEGER = 1020
			-- Must be a multiple of 12

	--|--------------------------------------------------------------

	Ks_character_map: STRING =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="

	--|--------------------------------------------------------------

	apf: AEL_PRINTF
		once
			create Result
		end

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_SPRT_BASE64

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 28-Jul-2009
--|     Added history.
--|     Changed name from AEL_STR_BASE64
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|     Converted use of INTEGER_REF to CELL [INTEGER].
--|     Updated to used NATURAL_8s instead of INTEGERs.
--|     Replaced symbolic integer constants with numerics.
--|     Replaced basename with call to library function.
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries and the Amalasoft Printf cluster.
--|----------------------------------------------------------------------
