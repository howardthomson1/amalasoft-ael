--|----------------------------------------------------------------------
--| Copyright (c) 1995-2010, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class providing enhanced (cmd line) argument processing
--|----------------------------------------------------------------------

class AEL_SPRT_ARGUMENTS

inherit
	ARGUMENTS
	AEL_SPRT_CL_CONSTANTS

--|========================================================================
feature -- Status
--|========================================================================

	unexpected_arguments: detachable LINKED_LIST [STRING]
			-- Command line arguments that were found but unexpected
		note
			option: stable
			attribute
		end

	arguments_analyzed: detachable LINKED_LIST [INTEGER]
			-- Indices of command line arguments that were found
		note
			option: stable
			attribute
		end

	missing_required_arguments: detachable LINKED_LIST [AEL_SPRT_CL_ARGUMENT]
			-- Require cmd line arguments that were not found
		note
			option: stable
			attribute
		end

	missing_optional_arguments: detachable LINKED_LIST [AEL_SPRT_CL_ARGUMENT]
			-- Optional cmd line arguments that were not found
		note
			option: stable
			attribute
		end

	--|--------------------------------------------------------------

	required_arguments: ARRAY [AEL_SPRT_CL_ARGUMENT]
			-- Arguments that are required
			-- Must be non-void to use 'analyze_command_line'
		do
			Result := {ARRAY [AEL_SPRT_CL_ARGUMENT]}<<>>
		end

	optional_arguments: detachable ARRAY [AEL_SPRT_CL_ARGUMENT]
			-- Arguments that are not required, but are permitted
		do
			Result := {ARRAY [AEL_SPRT_CL_ARGUMENT]}<<>>
		end

	new_default_argument: AEL_SPRT_CL_ARGUMENT
			-- Newly created default argument
		do
			create Result.make (K_argtype_word, '-', "nosuchargument", Void)
		end

	unexpected_args_ok: BOOLEAN
		do
			Result := unexpected_args_flag.item
		end

	unexpected_args_flag: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	set_unexpected_args_ok (tf: BOOLEAN)
			-- Set the unexpected args flag to 'tf'
		do
			unexpected_args_flag.put (tf)
		end

--|========================================================================
feature -- Error reporting
--|========================================================================

	argument_errors: TWO_WAY_LIST [AEL_SPRT_ERROR_INSTANCE]
			-- List of recorded errors
		do
			Result := amf.errors
		end

	has_argument_errors: BOOLEAN
			-- Was there an error found during argument processing?
		do
			Result := amf.has_error
		end

	set_arg_error_message (v: STRING)
		do 
			amf.set_error_message (v)
		end

	set_arg_error_message_formatted (fmt: STRING; args: detachable ANY)
			-- Set the error state with the given error message format 
			-- values (using printf conventions)
			-- Set the error code to '1'
		do 
			amf.set_error_message_formatted (fmt, args)
		end

	set_arg_error (c: INTEGER; msg: STRING)
			-- Set the error state with the given error code 'c'
			-- and message 'msg'
		do
			amf.set_error (c, msg)
		end

	clear_arg_errors
			-- Clear error codes and messages, if any
		do
			amf.clear_errors
		end

	last_arg_error: detachable AEL_SPRT_ERROR_INSTANCE
			-- Most recent error, if any (Void if none recorded)
		do
			Result := amf.last_error
		end

	last_arg_error_message: detachable STRING
			-- Most recent error message, if any (Void if none recorded)
		do
			Result := amf.last_error_message
		end

--|========================================================================
feature -- Analysis
--|========================================================================

	analyze_command_line
			-- Extract and analyze, from the command line, any given
			-- arguments
		require
			args_defined: required_arguments /= Void
		local
			i, lim: INTEGER
			a: detachable AEL_SPRT_CL_ARGUMENT
			ra: like required_arguments
			oa: detachable like optional_arguments
			ts: detachable STRING
			argsa: LINKED_LIST [INTEGER]
			argsu: LINKED_LIST [STRING]
		do
			create argsa.make
			arguments_analyzed := argsa
			create missing_required_arguments.make
			create missing_optional_arguments.make
			ra := required_arguments
			lim := ra.upper
			from i := ra.lower
			until i > lim
			loop
				a := ra.item (i)
				analyze_cl_argument (a, True)
				i := i + 1
			end
			oa := optional_arguments
			if oa /= Void then
				lim := oa.upper
				from i := oa.lower
				until i > lim
				loop
					a := oa.item (i)
					analyze_cl_argument (a, False)
					i := i + 1
				end
			end
			create argsu.make
			unexpected_arguments := argsu
			lim := argument_count
			from i := 1
			until i > lim
			loop
				if not argsa.has (i) then
					if unexpected_args_ok then
						argsu.extend (argument (i))
					else
						argsu.extend (
							apf.aprintf ("  Argument [%%s] at position %%d",
							<< argument (i), i >>))
					end
				end
				i := i + 1
			end
			if not unexpected_args_ok and not argsu.is_empty then
				create ts.make (128)
				from argsu.start
				until argsu.exhausted
				loop
					ts.append (argsu.item)
					argsu.forth
					if not argsu.exhausted then
						ts.extend ('%N')
					end
				end
				set_arg_error_message ("Unexpected arguments%N" + ts)
			end
		end

	--|--------------------------------------------------------------

	analyze_cl_argument (a: AEL_SPRT_CL_ARGUMENT; rf: BOOLEAN)
			-- Analyze the argument 'a' and, if sp defined, then execute 
			-- the associated action
		local
			idx: INTEGER
			indices: ARRAY [INTEGER]
			ts: detachable STRING
			auld_sign: CHARACTER
		do
			indices := {ARRAY [INTEGER]}<< >>
			auld_sign := option_sign.item
			set_option_sign (a.option_sign)
			inspect a.argument_type
			when K_argtype_char then
				-- Argument of the form "<sign><opt>"
				-- where <opt> is a single character
				-- Example: "-x"
				idx := index_of_solo_character_option (a.option.item (1))
				if idx /= 0 then
					-- Is found;  Char option value is present/absent, 
					-- represented as "True"/"False", respectively
					a.set_value (True)
					indices := << idx >>
				end
			when K_argtype_word then
				-- Argument of the form "<sign><opt>"
				-- where <opt> is a single word
				-- Example: "-xyz"
				idx := index_of_word_option (a.option)
				if idx /= 0 then
					-- Is found;  Word option value is present/absent, 
					-- represented as "True"/"False", respectively
					a.set_value (True)
					indices := << idx >>
				end
			when K_argtype_word_after then
				-- Argument of the form "<sign><opt><ws><val>"
				-- Example: "-xyz abc"
				idx := index_of_word_option (a.option)
				if idx /= 0 then
					-- Is found;  Word-after option value is the second 
					-- word in the sequence, and is a STRING
					ts := separate_word_option_value (a.option)
					if ts = Void then
						-- ERROR
						set_arg_error_message ("Option " + a.option +
							" requires a following word")
					else
						a.set_value (ts)
						indices := << idx, idx + 1 >>
					end
				end
			when K_argtype_standalone_word then
				-- Argument of the form "<opt>", without sign, and not 
				-- tied to another option
				-- Example: "abc"
				-- NOTA BENE: standalone arg MUST be last on cmd line
				-- and must not begin with '-'
				--set_option_sign ('%U')
				-- Cannot use index_of_word_option from ARGUMENTS because 
				-- it requires a non-empty option value
				if argument_count > 0 then
					if argument_count > 0 then
						ts := argument (argument_count)
						if not ts.is_empty and then ts.item (1) /= '-' then
							a.set_value (argument (argument_count))
							idx := argument_count
							indices := << idx >>
						end
					end
				end
			when K_argtype_word_appended then
				-- Argument of the form "<sign><opt><val>"
				-- Example: <opt> = "id=", "-id=12345"
				idx := index_of_beginning_with_word_option (a.option)
				if idx /= 0 then
					-- Is found;  Word-appended option value is the part 
					-- of the found argument after the option string
					a.set_value (coalesced_word_option_value (a.option))
					indices := << idx >>
				end
			when K_argtype_word_after_appended then
				-- Argument of the form "<sign><opt> <val>"
				-- Example: <opt> = "f", "-foop foo.txt"
				idx := index_of_beginning_with_word_option (a.option)
				if idx /= 0 then
					-- Is found;  Word-appended option value is the part 
					-- of the found argument after the option string
					-- Word after is the next argument
					ts := argument (idx)
					if ts.item (1) = option_sign then
						ts.prune (option_sign)
					end
					ts := separate_word_option_value (ts)
					if ts = Void then
						-- ERROR
						set_arg_error_message ("Option " + a.option +
							" requires a following word")
					else
						a.set_value (<<coalesced_word_option_value (a.option), ts>>)
						indices := << idx, idx + 1 >>
					end
				end
				--| NO else case; is internal error
			end
			if idx = 0 then
				-- Argument was not found in cmd cmdline
				if rf then
					-- Was required
					if attached missing_required_arguments as argsmr then
						argsmr.extend (a)
					end
				else
					-- Was optional
					if attached missing_optional_arguments as argsmr then
						argsmr.extend (a)
					end
				end
			else
				if attached arguments_analyzed as argsa then
					argsa.fill (indices)
				end
				if attached a.action as aa then
					aa.call ([a])
				end
			end
			set_option_sign (auld_sign)
		end

--|========================================================================
feature -- Queries
--|========================================================================

	index_of_solo_character_option (o: CHARACTER): INTEGER
			-- Does command line specify character option `o' and, if so,
			-- at what position?
			-- If one of the space-separated arguments is of the form `Xo',
			-- where `X' is the current `option_sign', and 'o' is the
			-- single character option,
			-- then index of this argument in list of arguments; else 0.
			-- Example: "-h"
			-- N.B. This differs from 'index_of_character_option' in
			-- ARGUMENTS because that routine allows words and is 
			-- therefore too liberal with its matches.
		require
			o_non_null: o /= '%U'
		local
			i, lim: INTEGER
 		do
			lim := argument_count
			from i := 1
			until i > lim or Result /= 0
			loop
				if option_character_equal (argument_array.item (i), o) and
					argument_array.item (i).count = 2 then
						Result := i
				else
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_beginning_with_word_follows_option (opt: STRING): INTEGER
			-- Index argument beginning with word option `opt' and 
			-- followed by word at the next index in the command line,
			-- if any
			-- Example:  <opt> = "foo" would match "-foop foop.txt" 
			-- (note the extra 'p')
			-- Result is index of <opt>.  Result+1 is index of following 
			-- word
		require
			opt_non_void: opt /= Void
			opt_meaningful: not opt.is_empty
		local
			i: INTEGER
		do
			from
				i := 1
			until
				i > argument_count or else
				option_word_begins_with (argument_array.item (i), opt)
			loop
				i := i + 1
			end
			if i < argument_count then
				--| must be '<' to allow for following word
				Result := i
			end
		end

--|========================================================================
feature -- Error handling
--|========================================================================

	apf: AEL_PRINTF
		once
			create Result
		end

	amf: 	AEL_SPRT_MULTI_FALLIBLE
		once
			create Result
		end

	merge_arg_errors
			-- Merge child's errors with those in amf
		do
			copy_errors (amf)
		end

	copy_errors (fromerrs: like amf)
			-- Reimplement in child if inherited
		do
		end

	print_arg_errors (nf, cf, clr: BOOLEAN)
			-- Report all recorded errors by printing to console
			-- If 'nf', then show sequence numbers
			-- If 'cf' then show error codes
			-- If 'clr', then clear errors when finished reporting
		do
			amf.print_errors (nf, cf, clr)
		end

end -- class AEL_SPRT_ARGUMENTS

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 12-Mar-2013
--|     Removed conditional setting of option sign when null.
--| 002 12-Mar-2013
--|     Added support for word arg after a partial word arg
--|     Example: tar-like args cvf <fname> where 'c' is arg root and 
--|     'vf' is optional and variable
--| 001 15-Aug-2010
--|     Created original module (6.6)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class should be inherited by classes that wants to process 
--| args.
--| While it is possible to use this class simply as a way to get to 
--| the ARGUMENTS ancestor, exploiting this class as intended has
--| significant benefit.  To do so, a descendent class should redefine
--| 'required_arguments' and 'optional_arguments' as needed, defining
--| the arguments that are required and that are not required but 
--| permitted, respectively.
--|
--| The descendent must also provide action routines to respond to the
--| discovery of arguments in the command line.  In practice, it is 
--| useful to have one action routine per argument, but it is not a 
--| requirement.
--|
--| In the initialization sequence of the descendent, there then be a 
--| call to 'analyze_arguments', which in turn will analyze the 
--| command line arguments in accordance with the definitions just 
--| mentioned.
--|
--| Any required arguments that are missing from the command line are 
--| collected in the 'missing_required_arguments' list.  Any optional
--| arguments that are not found are collected in the 
--| 'missing_optional_arguments' list for completeness.
--| Any arguments that are found but not either required or optional 
--| are collected in the 'unexpected_arguments' list as STRINGs 
--| denoting the unexpectedd argument and its command line position.
--| It is the repsonsibility of the descendent to check for any 
--| recorded errors and convey them as appropriate.
--|----------------------------------------------------------------------
