--|----------------------------------------------------------------------
--| Copyright (c) 1995-2010, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Abstract notion of a command line argument, as used by the class
--| AEL_SPRT_ARGUMENTS.
--|----------------------------------------------------------------------

class AEL_SPRT_CL_ARGUMENT

inherit
	AEL_SPRT_FALLIBLE
	AEL_SPRT_CL_CONSTANTS

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (t: INTEGER; os: CHARACTER; opt: STRING; ap: like action)
			-- Create Current with argument_type 't', option_sign 'os',
			-- option 'opt' and action agent 'ap'
		require
			valid_argtype: is_valid_argument_type (t)
			option_exists: opt /= Void
		do
			argument_type := t
			option_sign := os
			option := opt
			action := ap
		ensure
			type_set: argument_type = t
			sign_set: option_sign = os
			option_assigned: option = opt
			action_assigned: action = ap
		end

--|========================================================================
feature -- Status
--|========================================================================

	argument_type: INTEGER
			-- The format type of the argument

	--|--------------------------------------------------------------

	option_sign: CHARACTER
			-- Leading option sign
			-- N.B. to use a double dash ("--"), for example, you would 
			-- specify '-' as the option sign, and add the other '-' to 
			-- the option itself

	option: STRING
			-- Option string, less sign

--	action: detachable PROCEDURE [TUPLE [like Current]]
	action: detachable PROCEDURE [like Current]
			-- Action to execute on successful analysis

	--|--------------------------------------------------------------

	value: detachable ANY
			-- Value extracted from argument, if any

	--|--------------------------------------------------------------

	integer_value: INTEGER
			-- Integer equivalent of value, if any, and if integer
			-- Result of zero can be error or valid, and is the
			-- responsibility of the action to determine validity
			-- in that case.
		do
			if attached {INTEGER} value as ti then
				Result := ti
			elseif attached {STRING} value as ts then
				if ts.is_integer then
					Result := ts.to_integer
				end
			end
		end

	string_value: STRING
			-- String equivalent of value (empty if value is Void)
		do
			Result := ""
			if attached {STRING} value as ti then
				Result := ti
			end
		end

	boolean_value: BOOLEAN
			-- Boolean equivalent of value, if any, and if BOOLEAN
		do
			if attached {BOOLEAN} value as ti then
				Result := ti
			elseif attached {STRING} value as ts then
				if ts.is_boolean then
					Result := ts.to_boolean
				end
			end
		end

	double_value: DOUBLE
			-- REAL_64 (aka double) equivalent of value, if any, and if REAL
		do
			if attached {REAL} value as ti then
				Result := ti
			elseif attached {STRING} value as ts then
				if ts.is_double then
					Result := ts.to_double
				end
			end
		end

	array_value: detachable ARRAY [STRING]
			-- 1 or more STRINGs in an array, if any
		local
			a: ARRAY [STRING]
		do
			if attached {ARRAY [detachable STRING]} value as ta then
				if ta.count = 2 then
					if attached {ARRAY [STRING]} ta as ti then
						Result := ti
					else
						-- Create an attached one
						create a.make_filled ("", 1, 2)
						if attached ta.item (1) as s1 then
							a.put (s1, 1)
							if attached ta.item (2) as s2 then
								a.put (s2, 2)
								Result := a
							end
						end
					end
				end
			end
		ensure
			two_items: Result /= Void implies Result.count = 2
		end

	--|--------------------------------------------------------------

	has_integer_value: BOOLEAN
			-- Is value non-Void and if so, does it represent an integer?
		do
			if attached {INTEGER} value as ti then
				Result := True
			elseif attached {STRING} value as ts then
				Result := ts.is_integer
			end
		end

	has_string_value: BOOLEAN
			-- Is value non-Void and if so, does it represent a string?
		do
			Result := attached {STRING} value
		end

	has_array_value: BOOLEAN
			-- Is value non-Void and if so, does it represent a string?
		do
			Result := attached {ARRAY [STRING]} value
		end

	has_boolean_value: BOOLEAN
			-- Is value non-Void and if so, does it represent a boolean?
			-- N.B. In many cases, the mere presence of an option denotes
			-- true case for the associated (but unspoken) value, and so
			-- there can, in such cases, be no value to inspect.
			-- If the argument type is solo character, then this is
			-- managed automatically.
		do
			if attached {BOOLEAN} value as ti then
				Result := True
			elseif attached {STRING} value as ts then
				Result := ts.is_boolean
			end
		end

	has_double_value: BOOLEAN
			-- Is value non-Void and if so, does it represent a double?
		do
			if attached {REAL} value as ti then
				Result := True
			elseif attached {STRING} value as ts then
				Result := ts.is_double
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_value (v: detachable like value)
			-- Set the value associated with Current to 'v'
		do
			value := v
		ensure
			is_set: value = v
		end

	--|--------------------------------------------------------------
invariant
	char_is_single: argument_type = K_argtype_char implies option.count = 1

end -- class AEL_SPRT_CL_ARGUMENT

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 12-Mar-2013
--|     Added support for word arg after a partial word arg
--|     Example: tar-like args cvf <fname> where 'c' is arg root and 
--|     'vf' is optional and variable
--| 001 15-Aug-2010
--|     Created original module (6.6)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is used by AEL_SPRT_ARGUMENTS and its descendents.
--| Instances of this class are added to the 'required_arguments' and
--| 'optional_arguments' lists in descendents of AEL_SPRT_ARGUMENTS 
--| and passed to argument handler agents during analysis.
--|----------------------------------------------------------------------
