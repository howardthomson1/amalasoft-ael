--|----------------------------------------------------------------------
--| Copyright (c) 1995-2010, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Constants used by AEL command line argument classes AEL_SPRT_ARGUMENTS
--| and AEL_SPRT_CL_ARGUMENT (and descendents)
--|----------------------------------------------------------------------

class AEL_SPRT_CL_CONSTANTS

--|========================================================================
feature {NONE} -- Argument types
--|========================================================================

	K_argtype_char: INTEGER = 1
			-- Argument of the form "<sign><opt>"
			-- where <opt> is a single character
			-- Example: "-x"

	K_argtype_word: INTEGER = 2
			-- Argument of the form "<sign><opt>"
			-- where <opt> is a single word
			-- Example: "-xyz"

	K_argtype_word_after: INTEGER = 3
			-- Argument of the form "<sign><opt><ws><val>"
			-- Example: "-xyz abc"

	K_argtype_standalone_word: INTEGER = 4
			-- Argument of the form "<opt>"
			-- Example: "abc"

	K_argtype_word_appended: INTEGER = 5
			-- Argument of the form "<sign><opt><val>"
			-- Example: <opt> = "id=", "-id=12345"

	K_argtype_word_after_appended: INTEGER = 6
			-- Argument of the form "<sign><opt>[mumble] <val>"
			-- Example: <opt> = "f", "-foo foo.txt"

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_argument_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid argument type?
		do
			inspect v
			when
				K_argtype_char, K_argtype_word, K_argtype_word_after,
				K_argtype_standalone_word, K_argtype_word_appended,
				K_argtype_word_after_appended
			 then
				 Result := True
			else
			end
		end

end -- AEL_SPRT_CL_CONSTANTS

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 12-Mar-2013
--|     Added support for word arg after a partial word arg
--|     Example: tar-like args cvf <fname> where 'c' is arg root and 
--|     'vf' is optional and variable
--| 001 15-Aug-2010
--|     Created original module (6.6)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by AEL_SPRT_ARGUMENTS and AEL_SPRT_CL_ARGUMENT.
--| Use the constants in this class to denote specific argument types.
--|----------------------------------------------------------------------
