class AEL_SPRT_DEBUG_ENV

inherit
	AEL_PRINTF

 --|========================================================================
feature -- Status
 --|========================================================================

	dbprint_proc: detachable PROCEDURE [STRING]
		do
			Result := dbprint_proc_cell.item
		end

	dbprintf_proc: detachable PROCEDURE [STRING, detachable ANY]
		do
			Result := dbprintf_proc_cell.item
		end

	--|--------------------------------------------------------------

	debug_enabled: BOOLEAN
		do
			Result := debug_level_cell.item > 0
		end

	dbprint_enabled: BOOLEAN
		do
			Result := dbprint_cell.item
		end

	trace_enabled: BOOLEAN
		do
			Result := trace_level_cell.item > 0
		end

	debug_level: INTEGER
		do
			Result := debug_level_cell.item
		end

	trace_level: INTEGER
		do
			Result := trace_level_cell.item
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_dbprint_proc (v: like dbprint_proc)
			-- Set dbprint_proc to 'v'
			-- dbprint will then call 'dbprint_proc' instead of 'print'
		do
			dbprint_proc_cell.put (v)
		end

	set_dbprintf_proc (v: like dbprintf_proc)
			-- Set dbprintf_proc to 'v'
			-- dbprint will then call 'dbprintf_proc' instead of 'printf'
		do
			dbprintf_proc_cell.put (v)
		end

	--|--------------------------------------------------------------

	set_debug_level (v: INTEGER)
		require
			valid_level: v >= 0
		do
			debug_level_cell.put (v)
		ensure
			is_set: debug_level = v
		end

	enable_debug
		do
			set_debug_level (1)
		end

	disable_debug
		do
			set_debug_level (0)
		end

	--|--------------------------------------------------------------

	enable_dbprint
		do
			dbprint_cell.put (True)
		end

	disable_dbprint
		do
			dbprint_cell.put (False)
		end

	--|--------------------------------------------------------------

	set_trace_level (v: INTEGER)
		require
			valid_level: v >= 0
		do
			trace_level_cell.put (v)
		ensure
			is_set: trace_level = v
		end

	enable_trace
		do
			set_trace_level (1)
		end

	disable_trace
		do
			set_trace_level (0)
		end

--|========================================================================
feature -- Debug operations
--|========================================================================

	dbprint (v: STRING)
		local
			pf: like dbprint_proc
		do
			if dbprint_enabled then
				pf := dbprint_proc
				if pf /= Void then
					pf.call ([v])
				else
					print (v)
				end
			end
		end

	dbprintf (fmt: STRING; args: detachable ANY)
		local
			pf: like dbprintf_proc
		do
			if dbprint_enabled then
				pf := dbprintf_proc
				if pf /= Void then
					pf.call ([fmt,args])
				else
					printf (fmt, args)
				end
			end
		end

--|========================================================================
feature {NONE} -- Shared implementation
--|========================================================================

	debug_level_cell: CELL [INTEGER]
		once
			create Result.put (0)
		end

	dbprint_cell: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	trace_level_cell: CELL [INTEGER]
		once
			create Result.put (0)
		end

	dbprint_proc_cell: CELL [detachable PROCEDURE [STRING]]
		once
			create Result.put (Void)
		end

	dbprintf_proc_cell: CELL [
		detachable PROCEDURE [STRING, detachable ANY]]
		once
			create Result.put (Void)
		end

end -- class AEL_SPRT_DEBUG_ENV
