class AEL_SPRT_ERROR_INSTANCE

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (c: INTEGER; m: STRING)
		require
			valid_msg: is_valid_msg (m)
		do
			code := c
			message := m
		end

--|========================================================================
feature -- Status
--|========================================================================

	code: INTEGER
			-- The error code

	message: STRING
			-- The error message

	calling_class: detachable STRING
			-- Optional name of the class from which Current was recorded

--|========================================================================
feature {AEL_SPRT_MULTI_FALLIBLE} -- Status Setting
--|========================================================================

	set_calling_class (v: STRING)
		do
			calling_class := v
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_msg (v: STRING): BOOLEAN
		do
			Result := v /= Void
		end

	--|--------------------------------------------------------------
invariant
	valid: is_valid_msg (message)

end -- class AEL_SPRT_ERROR_INSTANCE
