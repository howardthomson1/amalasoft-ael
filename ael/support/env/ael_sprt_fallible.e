--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class providing support for the notion of errors
--|----------------------------------------------------------------------

class AEL_SPRT_FALLIBLE

--|========================================================================
feature -- Status
--|========================================================================

	error_message: STRING
			-- Most recent error message
		do
			if attached private_error_message as pm then
				Result := pm
			else
				Result := ""
			end
		end

	error_code: INTEGER
			-- Most recent error code
		do
			Result := private_error_code
		end

	--|--------------------------------------------------------------

	has_error: BOOLEAN
			-- Has there been an error recorded?
		do
			Result := private_error_message /= Void
		ensure
			message_if_true: Result implies error_code /= 0
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_error (c: INTEGER; msg: STRING)
			-- Set the error state with the given error code 'c'
			-- and message 'msg'
		require
			msg_exists: msg /= Void and then not msg.is_empty
		do
			private_error_code := c
			private_error_message := msg
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	set_error_message (msg: STRING)
			-- Set the error state with the given error message
			-- Set the error code to '1'
		require
			msg_exists: msg /= Void and then not msg.is_empty
		do
			set_error (1, msg)
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	clear_error
			-- Clear error code and message, if any
		do
			private_error_code := 0
			private_error_message := Void
		ensure
			no_error: not has_error
		end

--|========================================================================
feature {NONE} -- Private attributes
--|========================================================================

	private_error_message: detachable STRING
			-- Most recent error message

	private_error_code: INTEGER
			-- Most recent error code

end -- class AEL_SPRT_FALLIBLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 28-Jul-2009
--|     Added history.
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class should be inherited by any class that wants to track errors
--|----------------------------------------------------------------------
