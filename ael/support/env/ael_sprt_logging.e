--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Basic logging support that includes a shareable logfile,
--| timestamped entries and rotating log files.
--| See instructions at end of class text.
--|----------------------------------------------------------------------

deferred class AEL_SPRT_LOGGING

inherit
	AEL_SPRT_SERVICES

--|========================================================================
feature -- Logging
--|========================================================================

	logit (lvl: INTEGER; msg: STRING_8)
			-- Write the given message to the log file if the level is
			-- equal or less than current logging level
		require
			message_exists: msg /= Void
			has_open_log: logging_enabled implies
				attached logfile as f and then not f.is_closed
			valid_level: lvl >= 0
		local
			ms: STRING
		do
			if logging_enabled then
				if msg.is_empty then
					ms := "** no message **"
				else
					ms := msg
				end
				if lvl >= log_level then
					logfile.put_string (new_log_entry (ms))
					logfile.flush
					logseqno_cell.put (logseqno + 1)
				end
				if logfile.count >= logfile_size_threshold then
					rotate_log_files
				end
			end
		ensure
			seqno_incremented: logging_enabled implies
				logseqno = (old logseqno + 1)
		end

	--|--------------------------------------------------------------

	logit_f (lvl: INTEGER; fmt: STRING_8; args: ANY)
			-- Write the given message, as defined by format string
			-- 'fmt' and arguments 'args', to the log file if the level
			-- is equal or less than current logging level
		require
			format_exists: fmt /= Void
			has_log: logfile /= Void
			log_open: not logfile.is_closed
			valid_level: lvl >= 0
		do
			logit (lvl, aprintf (fmt, args))
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	logfile: PLAIN_TEXT_FILE
		do
			Result := logfile_cell.item
		ensure
			exists: Result /= Void
			exists_on_disk: Result.exists
			is_open: Result.is_open_write
		end

	--|--------------------------------------------------------------

	logfile_name: STRING
			-- Full name of log file
		do
			Result := logfile.path.out
		end

	--|--------------------------------------------------------------

	logseqno: NATURAL_32
			-- Sequence number for log entries
		do
			Result := logseqno_cell.item
		end

	log_level: INTEGER
			-- Current log message threshold
			-- Level 0 denotes that only lowest-level log entries
			-- (i.e. "always") entries are logged.
			-- Higher levels denote a more verbose log state
			--
			-- Level < 0 denotes logging disabled
		do
			Result := log_level_cell.item
		end

	logging_enabled: BOOLEAN
			-- Is logging (presently) enabled?
		do
			Result := log_level >= 0
		end

	--|--------------------------------------------------------------

	application_name: STRING
			-- The name of the application incorporating this class
		deferred
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	--|--------------------------------------------------------------

	application_id: INTEGER
			-- ID that identifies the current application amongst its
			-- peers on a given system
		deferred
		end

	--|--------------------------------------------------------------

	logfile_size_threshold: INTEGER
			-- Size of log file above which a new log file is created
		do
			Result := logfile_size_threshold_cell.item
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_log_level (v: INTEGER)
			-- Set the current log message threshold to 'v'
			-- Level 0 will log "always" messages only
			-- Higher levels will log debug and trace messages with
			-- a defined level that is equal or less than the log level
			--
			-- Setting log level to a negative value disables all
			-- logging, even for 'always' messages
		do
			log_level_cell.put (v)
		end

	disable_logging
			-- Disable logging, at least temporarily, by setting 
			-- log_level to denote 'nothing'
		do
			set_log_level (-1)
		end

	--|--------------------------------------------------------------

	set_logfile_size_threshold (v: INTEGER)
			-- Set the size of log file above which a new log file
			-- is created
		do
			logfile_size_threshold_cell.put (v)
			logit (0, "Changing logfile size threshold to " + v.out)
		end

--|========================================================================
feature {NONE} -- Error recording
--|========================================================================

	logger_errors: AEL_SPRT_MULTI_FALLIBLE
			-- Errors recorded during logger startup
		once
			create Result
		end

--|========================================================================
feature -- Error recording
--|========================================================================

	record_logger_error (fmt: STRING; args: detachable ANY)
			-- Set the error state with the given error message format 
			-- values (using printf conventions)
			-- Set the error code to '1'
		require
			msg_exists: fmt /= Void and then not fmt.is_empty
		do
			logger_errors.record_error (fmt, args)
		ensure
			has_error: has_logger_error
		end

	has_logger_error: BOOLEAN
			-- Are there any recorded logger errors?
		do
			Result := logger_errors.has_error
		ensure
			valid_if_true: Result implies logger_errors.has_error
			valid_if_false: not Result implies not logger_errors.has_error
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	logfile_cell: CELL [PLAIN_TEXT_FILE]
		once
			create Result.put (new_logfile)
		end

	log_level_cell: CELL [INTEGER]
			-- Shared log level
			-- Level 0 denotes that only lowest-level log entries,
			-- i.e. "always" entries are logged.
			-- Higher levels denote a more verbose trace or debug state
			-- Setting log level to a negative value disables all
			-- logging, even for 'always' messages
		once
			create Result.put (0)
		end

	logseqno_cell: CELL [NATURAL_32]
			-- Sequence number for log entries
		once
			create Result.put (1)
		end

	logfile_size_threshold_cell: CELL [INTEGER]
			-- Size of log file above which a new log file is created
		once
			create Result.put (K_logfile_threshold)
		end

	--|--------------------------------------------------------------

	new_logfile: PLAIN_TEXT_FILE
			-- A newly created primary (i.e. not bak) log file
		local
--			fn: FILE_NAME
			fn: PATH
			td, pd: DIRECTORY
		do
			-- Record error if parent directory does no exist or its not writable
			create pd.make (Ks_log_parent_dir_name)
			if not pd.exists then
				record_logger_error ("Directory %%s does not exist",
						Ks_log_parent_dir_name)
				--| If unable to create a log file, then use Console as a 
				--| last resort
				Result := io.output
			elseif not pd.is_writable then
				record_logger_error ("Directory %%s does not have write permissions",
						Ks_log_parent_dir_name)
				--| If unable to create a log file, then use Console as a 
				--| last resort
				Result := io.output
			else
				create td.make (Ks_log_dir_name)
				if not td.exists then
					td.recursive_create_dir
				end
				fn := new_logfile_path
				--create Result.make_with_name (fn)
				create Result.make_with_path (fn)
				-- Continue writing to existing log on restart
				Result.open_append
			end
		ensure
			exists: Result /= Void
			open: not attached {CONSOLE} Result implies Result.is_open_append
			open: attached {CONSOLE} Result implies Result.is_open_write
			error_recorded: attached {CONSOLE} Result implies has_logger_error
		end

	--|--------------------------------------------------------------

	--RFO new_logfile_name: FILE_NAME
	--RFO 		-- Name of primary log file
	--RFO 	do
	--RFO 		create Result.make_from_string (Ks_log_dir_name)
	--RFO 		Result.extend (application_name + "_" + application_id.out + ".log")
	--RFO 	end

	new_logfile_path: PATH
			-- Pathname of primary log file
		do
			create Result.make_from_string (Ks_log_dir_name)
			Result := Result.extended (
				application_name + "_" + application_id.out + ".log")
		end

	--|--------------------------------------------------------------

	logfile_bak_name (v: INTEGER): STRING
			-- Name used for a log backup file for log version 'v'
		require
			valid_bak_id: v > 0 and v <= maximum_logfile_backups
		local
			tp: PATH
		do
			tp := new_logfile_path
			Result := tp.out
			Result.append ("." + v.out)
		end

	maximum_logfile_backups: INTEGER
			-- Highest number logfile backup
		do
			Result := 9
		end

	--|--------------------------------------------------------------

	rotate_log_files
		local
			tf: like logfile
			i: INTEGER
		do
			create tf.make_with_name (logfile_bak_name (maximum_logfile_backups))
			if tf.exists then
				-- It's too old, say b-bye to make room for the next oldest
				tf.delete
			end
			from i := maximum_logfile_backups - 1
			until i = 0
			loop
				create tf.make_with_name (logfile_bak_name (i))
				if tf.exists then
					-- Rename to one id higher
					tf.rename_file (logfile_bak_name (i + 1))
				end
				i := i - 1
			end
			tf := logfile
			tf.close
			tf.rename_file (logfile_bak_name (1))
			-- Make a new one
			logfile_cell.put (new_logfile)
		ensure
			rotated: -- not has_logger_error implies is_rotated
		end

	--|--------------------------------------------------------------

	timestamp: AEL_TIMESTAMP
			-- Timestamp representing present
		do
			create Result.make
			Result.generate_terse
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	new_log_entry (msg: STRING): STRING
			-- The given message formatted as a log file entry
		require
			message_exists: msg /= Void
		do
			Result := aprintf (Ks_logentry_fmt, << timestamp, logseqno, msg >>)
			if Result.item (Result.count) /= '%N' then
				-- Add newline if msg didn't have one already
				Result.extend ('%N')
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_log_dir_name: STRING
		deferred
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	Ks_log_parent_dir_name: STRING
		deferred
		ensure
			exists: Result /= Void and then not Result.is_empty
		end

	K_logfile_threshold: INTEGER
			-- Size of log file above which a new log file is created
		do
			Result := 65535
		end

	Ks_logentry_fmt: STRING
			-- Format of log entry (less trailing newline)
		do
			Result := "%%s %%010u %%s"
		end

end -- class AEL_SPRT_LOGGING

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 19-Jul-2018
--|     Added disable_logging and logging_enabled routines.  Added 
--|     check in logit for logging_enabled.  Updated preconditions
--|     for logit accordingly
--|     Compiled and tested using Eiffel 18.1, void-safe
--| 002 05-Oct-2010
--|     Added history.
--|     Compiled and tested using Eiffel 6.6
--|     Added logfile rotation, removed process ID replaced with app id
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited.  The descendent must supply the
--| application_id and application_name routines.
--| Clients or descendent can call logit or logit_f.  Each takes a log
--| level argument.  The log level can be used to filter content from
--| the entering the log, based on severity level, for example.
--|
--| Primary log is always *.log, and backups are always *.log.n where
--| n is the backup depth (largest n means oldest backup).  Log
--| entries are always written to Primary
--|
--| This class relies on classes from the Eiffel Base libraries and
--| the Amalasoft Printf  and Support clusters.
--|----------------------------------------------------------------------
