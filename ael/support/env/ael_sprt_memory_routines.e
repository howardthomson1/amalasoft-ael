note
	description: "{
Routines supporting direct memory operations
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_MEMORY_ROUTINES

--|========================================================================
feature -- Status
--|========================================================================

	is_little_endian: BOOLEAN
			-- Is current platform a little endian one?
		do
			Result := c_endian_test = 1
		end

--|========================================================================
feature -- Access (value retrieval, platform-dependent)
--|========================================================================

	byte_at (p: POINTER): NATURAL_8
			-- Value of byte (NATURAL_8) at address 'p'.
		require
			not_null: p /= default_pointer
		do
			Result := c_byte_at (p)
		end

	char_at (p: POINTER): CHARACTER_8
			-- Value of character at address 'p'.
		require
			not_null: p /= default_pointer
		do
			Result := c_char_at (p)
		end

	short_at (p: POINTER; sbf: BOOLEAN): INTEGER_16
			-- Value of 2 adjacent bytes, at address 'p', in 
			-- platform-dependent SHORT form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ushort (c_ushort_at (p)).as_integer_16
			else
				Result := c_short_at (p)
			end
		end

	ushort_at (p: POINTER; sbf: BOOLEAN): NATURAL_16
			-- Value of 2 adjacent bytes, at address 'p', in 
			-- platform-dependent USHORT form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ushort (c_ushort_at (p))
			else
				Result := c_ushort_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_at (p: POINTER; sbf: BOOLEAN): INTEGER_32
			-- Value of 4 adjacent bytes, at address 'p', in 
			-- platform-dependent LONG form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ulong (c_ulong_at (p)).as_integer_32
			else
				Result := c_long_at (p)
			end
		end

	ulong_at (p: POINTER; sbf: BOOLEAN): NATURAL_32
			-- Value of 4 adjacent bytes, at address 'p', in 
			-- platform-dependent ULONG form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ulong (c_ulong_at (p))
			else
				Result := c_ulong_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_long_at (p: POINTER; sbf: BOOLEAN): INTEGER_64
			-- Value of 8 adjacent bytes, at address 'p', in 
			-- platform-dependent LONG LONG (64 bit) form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ulong_long (
					c_ulong_long_at (p)).as_integer_64
			else
				Result := c_long_long_at (p)
			end
		end

	ulong_long_at (p: POINTER; sbf: BOOLEAN): NATURAL_64
			-- Value of 8 adjacent bytes, at address 'p', in 
			-- platform-dependent ULONG LONG (64 bit) form
			--
			-- If 'sbf', then swap bytes (little-big or vice versa)
			-- Client should be endian-aware of pointer source and 
			-- operating environment (i.e. platform)
		require
			not_null: p /= default_pointer
		do
			if sbf then
				Result := c_endian_swapped_ulong_long (c_ulong_long_at (p))
			else
				Result := c_ulong_long_at (p)
			end
		end

	--|--------------------------------------------------------------

	float_at (p: POINTER): REAL_32
			-- Value of 4 adjacent bytes, at address 'p', in 
			-- platform-dependent FLOAT form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			Result := c_float_at (p)
		end

	double_at (p: POINTER): REAL_64
			-- Value of 8 adjacent bytes, at address 'p', in 
			-- platform-dependent DOUBLE form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			Result := c_double_at (p)
		end

	--|--------------------------------------------------------------

	string_at (p: POINTER): STRING_8
			-- Value of adjacent bytes, NULL-terminated, starting at 
			-- address 'p' interpreted as a STRING.
		require
			not_null: p /= default_pointer
		do
			create Result.make_from_c (p)
		end

	string_n_at (p: POINTER; nb: INTEGER): STRING_8
			-- Value of 'nb' adjacent bytes at address 'p' interpreted 
			-- as a STRING.  NO NULL TERMINATION REQUIRED
		require
			not_null: p /= default_pointer
		do
			create Result.make (nb + 1)
			-- 1-based index
			Result.from_c_substring (p, 1, nb)
		end

	substring_at (p: POINTER; sp, ep: INTEGER): STRING_8
			-- Value of adjacent bytes from 'sp' though 'ep' relative to 
			-- address 'p' interpreted as a STRING.
			-- NO NULL TERMINATION REQUIRED
		require
			not_null: p /= default_pointer
			valid_start: sp > 0
			valid_end: ep > 0 and ep <= sp
		local
			nb: INTEGER
		do
			nb := ep - sp + 1
			create Result.make (nb + 1)
			Result.from_c_substring (p, sp - 1, ep - 1)
		end

	--|--------------------------------------------------------------

	byte_array_at (p: POINTER; ic: INTEGER): ARRAY [NATURAL_8]
			-- Value of 'ic' adjacent bytes starting at address 'p' in 
			-- the form of an array of bytes (NATURAL_8)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_8]
		do
			bc := 1
			lim := ic * bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_byte_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic
		end

	--|--------------------------------------------------------------

	short_array_at (p: POINTER; ic: INTEGER): ARRAY [INTEGER_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend shorts) starting at address 'p' in 
			-- the form of an array of shorts (INTEGER_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_short_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	ushort_array_at (p: POINTER; ic: INTEGER): ARRAY [NATURAL_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend unsigned shorts) starting at address
			-- 'p' in the form of an array of unsigned shorts (NATURAL_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_ushort_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	long_array_at (p: POINTER; ic: INTEGER): ARRAY [INTEGER_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependent longs) starting at address 'p' in 
			-- the form of an array of longs (INTEGER_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_long_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	ulong_array_at (p: POINTER; ic: INTEGER): ARRAY [NATURAL_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependend unsigned longs) starting at address
			-- 'p' in the form of an array of unsigned longs (NATURAL_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_ulong_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	long_long_array_at (p: POINTER; ic: INTEGER): ARRAY [INTEGER_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependent long longs) starting at address 'p' in 
			-- the form of an array of long longs (INTEGER_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_long_long_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

	--|--------------------------------------------------------------

	ulong_long_array_at (p: POINTER; ic: INTEGER): ARRAY [NATURAL_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependend unsigned long longs) starting at address
			-- 'p' in the form of an array of unsigned long longs (NATURAL_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (c_ulong_long_at (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

--|========================================================================
feature -- Access (value retrieval, Big-endian)
--|========================================================================

	short_at_be (p: POINTER): INTEGER_16
			-- Value of 2 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian SHORT form, with result in 
			-- platform-correct SHORT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_16
		do
			if is_little_endian then
				--| Requires swapping
				tu := c_ushort_at (p)
				Result := (c_endian_swapped_ushort (tu)).as_integer_16
			else
				--| No swap required
				Result := c_short_at (p)
			end
		end

	ushort_at_be (p: POINTER): NATURAL_16
			-- Value of 2 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian USHORT form, with result in 
			-- platform-correct USHORT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				--| Requires swapping
				Result := c_ushort_at (p)
				Result := (c_endian_swapped_ushort (Result))
			else
				--| No swap required
				Result := c_ushort_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_at_be (p: POINTER): INTEGER_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian LONG form, with result in 
			-- platform-correct LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_32
		do
			if is_little_endian then
				--| Requires swapping
				tu := c_ulong_at (p)
				Result := (c_endian_swapped_ulong (tu)).as_integer_32
			else
				--| No swap required
				Result := c_long_at (p)
			end
		end

	ulong_at_be (p: POINTER): NATURAL_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian ULONG form, with result in 
			-- platform-correct ULONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				--| Requires swapping
				Result := c_ulong_at (p)
				Result := (c_endian_swapped_ulong (Result))
			else
				--| No swap required
				Result := c_ulong_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_long_at_be (p: POINTER): INTEGER_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian LONG LONG form, with result in 
			-- platform-correct LONG LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_64
		do
			if is_little_endian then
				--| Requires swapping
				tu := c_ulong_long_at (p)
				Result := (c_endian_swapped_ulong_long (tu)).as_integer_64
			else
				--| No swap required
				Result := c_long_long_at (p)
			end
		end

	ulong_long_at_be (p: POINTER): NATURAL_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian ULONG LONG form, with result in 
			-- platform-correct ULONG LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				--| Requires swapping
				Result := c_ulong_long_at (p)
				Result := (c_endian_swapped_ulong_long (Result))
			else
				--| No swap required
				Result := c_ulong_long_at (p)
			end
		end

	--|--------------------------------------------------------------

	float_at_be (p: POINTER): REAL_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian FLOAT form, with result in 
			-- platform-correct FLOAT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			Result := c_float_at (p)
			if is_little_endian then
				--| Requires swapping
				Result := (c_endian_swapped_float (Result))
			else
				--| No swap required
			end
		end

	double_at_be (p: POINTER): REAL_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Big-Endian DOUBLE form, with result in 
			-- platform-correct DOUBLE form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			Result := c_double_at (p)
			if is_little_endian then
				--| Requires swapping
				Result := (c_endian_swapped_double (Result))
			else
				--| No swap required
			end
		end

	--|--------------------------------------------------------------

	short_array_at_be (p: POINTER; ic: INTEGER): ARRAY [INTEGER_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend shorts) starting at address 'p' in 
			-- the form of an array of shorts (INTEGER_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (short_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	ushort_array_at_be (p: POINTER; ic: INTEGER): ARRAY [NATURAL_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend unsigned shorts) starting at address
			-- 'p' in the form of an array of unsigned shorts (NATURAL_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ushort_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	long_array_at_be (p: POINTER; ic: INTEGER): ARRAY [INTEGER_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependent longs) starting at address 'p' in 
			-- the form of an array of longs (INTEGER_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (long_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	ulong_array_at_be (p: POINTER; ic: INTEGER): ARRAY [NATURAL_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependend unsigned longs) starting at address
			-- 'p' in the form of an array of unsigned longs (NATURAL_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ulong_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	long_long_array_at_be (p: POINTER; ic: INTEGER): ARRAY [INTEGER_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependent long longs) starting at address 'p' in 
			-- the form of an array of long longs (INTEGER_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (long_long_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

	--|--------------------------------------------------------------

	ulong_long_array_at_be (p: POINTER; ic: INTEGER): ARRAY [NATURAL_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependend unsigned long longs) starting at address
			-- 'p' in the form of an array of unsigned long longs (NATURAL_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ulong_long_at_be (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

--|========================================================================
feature -- Access (value retrieval, Little-endian)
--|========================================================================

	short_at_le (p: POINTER): INTEGER_16
			-- Value of 2 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian SHORT form, with result in 
			-- platform-correct SHORT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_16
		do
			if not is_little_endian then
				--| Requires swapping
				tu := c_ushort_at (p)
				Result := (c_endian_swapped_ushort (tu)).as_integer_16
			else
				--| No swap required
				Result := c_short_at (p)
			end
		end

	ushort_at_le (p: POINTER): NATURAL_16
			-- Value of 2 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian USHORT form, with result in 
			-- platform-correct USHORT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_16
		do
			if not is_little_endian then
				--| Requires swapping
				tu := c_ushort_at (p)
				Result := (c_endian_swapped_ushort (tu))
			else
				--| No swap required
				Result := c_ushort_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_at_le (p: POINTER): INTEGER_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian LONG form, with result in 
			-- platform-correct LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_32
		do
			if not is_little_endian then
				--| Requires swapping
				tu := c_ulong_at (p)
				Result := (c_endian_swapped_ulong (tu)).as_integer_32
			else
				--| No swap required
				Result := c_long_at (p)
			end
		end

	ulong_at_le (p: POINTER): NATURAL_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian ULONG form, with result in 
			-- platform-correct ULONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				--| Requires swapping
				Result := c_ulong_at (p)
				Result := (c_endian_swapped_ulong (Result))
			else
				--| No swap required
				Result := c_ulong_at (p)
			end
		end

	--|--------------------------------------------------------------

	long_long_at_le (p: POINTER): INTEGER_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian LONG LONG form, with result in 
			-- platform-correct LONG LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		local
			tu: NATURAL_64
		do
			if not is_little_endian then
				--| Requires swapping
				tu := c_ulong_long_at (p)
				Result := (c_endian_swapped_ulong_long (tu)).as_integer_64
			else
				--| No swap required
				Result := c_long_long_at (p)
			end
		end

	ulong_long_at_le (p: POINTER): NATURAL_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian ULONG LONG form, with result in 
			-- platform-correct ULONG LONG form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				--| Requires swapping
				Result := c_ulong_long_at (p)
				Result := (c_endian_swapped_ulong_long (Result))
			else
				--| No swap required
				Result := c_ulong_long_at (p)
			end
		end

	--|--------------------------------------------------------------

	float_at_le (p: POINTER): REAL_32
			-- Value of 4 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian FLOAT form, with result in 
			-- platform-correct FLOAT form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			Result := c_float_at (p)
			if not is_little_endian then
				--| Requires swapping
				Result := (c_endian_swapped_float (Result))
			else
				--| No swap required
			end
		end

	double_at_le (p: POINTER): REAL_64
			-- Value of 8 adjacent bytes, at address 'p', assumed to be 
			-- stored in Little-Endian DOUBLE form, with result in 
			-- platform-correct DOUBLE form (i.e. swap if needed)
		require
			not_null: p /= default_pointer
		do
			Result := c_double_at (p)
			if not is_little_endian then
				--| Requires swapping
				Result := (c_endian_swapped_double (Result))
			else
				--| No swap required
			end
		end

	--|--------------------------------------------------------------

	short_array_at_le (p: POINTER; ic: INTEGER): ARRAY [INTEGER_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend shorts) starting at address 'p' in 
			-- the form of an array of shorts (INTEGER_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (short_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	ushort_array_at_le (p: POINTER; ic: INTEGER): ARRAY [NATURAL_16]
			-- Value of 'ic' adjacent byte pairs (interpreted as 
			-- platform-dependend unsigned shorts) starting at address
			-- 'p' in the form of an array of unsigned shorts (NATURAL_16)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_16]
		do
			bc := 2
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ushort_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 2
		end

	--|--------------------------------------------------------------

	long_array_at_le (p: POINTER; ic: INTEGER): ARRAY [INTEGER_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependent longs) starting at address 'p' in 
			-- the form of an array of longs (INTEGER_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (long_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	ulong_array_at_le (p: POINTER; ic: INTEGER): ARRAY [NATURAL_32]
			-- Value of 'ic' adjacent byte quads (interpreted as 
			-- platform-dependend unsigned longs) starting at address
			-- 'p' in the form of an array of unsigned longs (NATURAL_32)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_32]
		do
			bc := 4
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ulong_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 4
		end

	--|--------------------------------------------------------------

	long_long_array_at_le (p: POINTER; ic: INTEGER): ARRAY [INTEGER_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependent long longs) starting at address 'p' in 
			-- the form of an array of long longs (INTEGER_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [INTEGER_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (long_long_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

	--|--------------------------------------------------------------

	ulong_long_array_at_le (p: POINTER; ic: INTEGER): ARRAY [NATURAL_64]
			-- Value of 'ic' adjacent byte octals (interpreted as 
			-- platform-dependend unsigned long longs) starting at address
			-- 'p' in the form of an array of unsigned long longs (NATURAL_64)
		require
			not_null: p /= default_pointer
			valid_count: ic /= 0
		local
			i, lim, bc: INTEGER_32
			l_area: SPECIAL [NATURAL_64]
		do
			bc := 8
			lim := ic + bc
			create l_area.make_empty (ic)
			from i := 0
			until i >= lim
			loop
				l_area.extend (ulong_long_at_le (p + i))
				i := i + bc
			end
			create Result.make_from_special (l_area)
		ensure
			exists: Result /= Void
			valid_count: Result.count = ic // 8
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	put_byte (v: NATURAL_8; p: POINTER)
			-- Insert 1-byte value 'v' to address 'p'
		require
			not_null: p /= default_pointer
		do
			c_put_byte (v, p)
		end

	put_char (v: CHARACTER_8; p: POINTER)
			-- Insert 1-byte character value 'v' to address 'p'
		require
			not_null: p /= default_pointer
		do
			c_put_char (v, p)
		end

	put_short_ne (v: INTEGER_16; p: POINTER)
			-- Insert 'v' as 2-byte SIGNED value to address 'p', in
			-- platform-dependent SHORT form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_short (v, p)
		end

	put_ushort_ne (v: NATURAL_16; p: POINTER)
			-- Insert 2-byte value 'v' to address 'p', in
			-- platform-dependent USHORT form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_ushort (v, p)
		end

	put_long_ne (v: INTEGER_32; p: POINTER)
			-- Insert 'v' as 4-byte signed value to address 'p', in
			-- platform-dependent LONG form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_long (v, p)
		end

	put_ulong_ne (v: NATURAL_32; p: POINTER)
			-- Insert 'v' as 4-byte unsigned value to address 'p', in
			-- platform-dependent ULONG form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_ulong (v, p)
		end

	--|--------------------------------------------------------------

	put_long_long_ne (v: INTEGER_64; p: POINTER)
			-- Insert 'v' as 8-byte signed value to address 'p', in
			-- platform-dependent LONG LONG form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_long_long (v, p)
		end

	put_ulong_long_ne (v: NATURAL_64; p: POINTER)
			-- Insert 'v' as 8-byte unsigned value to address 'p', in
			-- platform-dependent ULONG LONG form (not endian-aware)
		require
			not_null: p /= default_pointer
		do
			c_put_ulong_long (v, p)
		end

	--|--------------------------------------------------------------

	put_string (v: STRING_8; p: POINTER)
			-- Copy contents of 'v' into address 'p'
			-- adding a NULL terminator at end
		require
			exists: v /= Void
			not_null: p /= default_pointer
		local
			ta: ANY
		do
			ta := v.to_c
			memcopy ($ta, p, v.count + 1)
		end

	--|--------------------------------------------------------------

	put_substring (v: STRING_8; sp, ep: INTEGER; p: POINTER)
			-- Copy contents of 'v', from 'sp' to 'ep', into address 'p'
			-- adding a NULL terminator at end
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep > 0 and ep <= sp
			not_null: p /= default_pointer
		local
			ta: ANY
			ts: STRING_8
		do
			ts := v.substring (sp, ep)
			ta := ts.to_c
			memcopy ($ta, p, ts.count + 1)
		end

	--|--------------------------------------------------------------

	clear_string (p: POINTER; len: INTEGER_32)
			-- Fill address 'p' with Nulls, for 'len' bytes
		require
			not_null: p /= default_pointer
		do
			memset (p, 0, len)
		end

	--|--------------------------------------------------------------

	put_stream (v: STRING_8; p: POINTER)
			-- Copy contents of 'v' into address 'p'
			-- WITHOUT adding a NULL terminator at end
		require
			exists: v /= Void
			not_null: p /= default_pointer
		local
			ta: ANY
		do
			ta := v.area
			memcopy ($ta, p, v.count)
		end

	put_substream (v: STRING_8; sp, ep: INTEGER; p: POINTER)
			-- Copy contents of 'v', from 'sp' to 'ep', into address 'p'
			-- WITHOUT adding a NULL terminator at end
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep > 0 and ep <= sp
			not_null: p /= default_pointer
		local
			ta: ANY
		do
			ta := v.area
			memcopy (($ta)+(sp-1), p, ep - sp + 1)
		end

--|========================================================================
feature -- Octet conversion (Big-Endian)
--|========================================================================

	short_to_octets_be (v: INTEGER_16): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			if is_little_endian then
				c_put_short (c_endian_swapped_short (v), mp.item)
			else
				c_put_short (v, mp.item)
			end
			Result := address_to_octets (mp.item, 2)
			-- TODO Need a more efficient mechanism
		end

	ushort_to_octets_be (v: NATURAL_16): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			if is_little_endian then
				c_put_ushort (c_endian_swapped_ushort (v), mp.item)
			else
				c_put_ushort (v, mp.item)
			end
			Result := address_to_octets (mp.item, 2)
		end

	--|--------------------------------------------------------------

	long_to_octets_be (v: INTEGER_32): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			if is_little_endian then
				c_put_long (c_endian_swapped_long (v), mp.item)
			else
				c_put_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 4)
			-- TODO Need a more efficient mechanism
		end

	ulong_to_octets_be (v: NATURAL_32): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			if is_little_endian then
				c_put_ulong (c_endian_swapped_ulong (v), mp.item)
			else
				c_put_ulong (v, mp.item)
			end
			Result := address_to_octets (mp.item, 4)
		end

	--|--------------------------------------------------------------

	long_long_to_octets_be (v: INTEGER_64): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (8)
			if is_little_endian then
				c_put_long_long (c_endian_swapped_long_long (v), mp.item)
			else
				c_put_long_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 8)
			-- TODO Need a more efficient mechanism
		end

	ulong_long_to_octets_be (v: NATURAL_64): STRING_8
			-- Big-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
			--ca: ANY
		do
			create mp.make (8)
			if is_little_endian then
				c_put_ulong_long (c_endian_swapped_ulong_long (v), mp.item)
			else
				c_put_ulong_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 8)
			--ca := Result.area
			--memcopy (mp.item, $ca, 8)
		end

--|========================================================================
feature -- Octet streams to integral values - Big-Endian
--|========================================================================

	octets_to_n16_be (v: STRING): NATURAL_16
			-- Unsigned 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ushort_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_ushort (Result)
			end
		end

	octets_to_i16_be (v: STRING): INTEGER_16
			-- Signed 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_short_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_short (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_n32_be (v: STRING): NATURAL_32
			-- Unsigned 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_ulong (Result)
			end
		end

	octets_to_i32_be (v: STRING): INTEGER_32
			-- Signed 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_long (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_n64_be (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_long_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_ulong_long (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_i64_be (v: STRING): INTEGER_64
			-- Signed 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_long_at (cstr.item)
			if is_little_endian then
				Result := c_endian_swapped_long_long (Result)
			end
		end

--|========================================================================
feature -- Octet conversion (Little-Endian)
--|========================================================================

	short_to_octets_le (v: INTEGER_16): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			if not is_little_endian then
				c_put_short (c_endian_swapped_short (v), mp.item)
			else
				c_put_short (v, mp.item)
			end
			Result := address_to_octets (mp.item, 2)
			-- TODO Need a more efficient mechanism
		end

	ushort_to_octets_le (v: NATURAL_16): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			if not is_little_endian then
				c_put_ushort (c_endian_swapped_ushort (v), mp.item)
			else
				c_put_ushort (v, mp.item)
			end
			Result := address_to_octets (mp.item, 2)
		end

	--|--------------------------------------------------------------

	long_to_octets_le (v: INTEGER_32): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			if not is_little_endian then
				c_put_long (c_endian_swapped_long (v), mp.item)
			else
				c_put_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 4)
			-- TODO Need a more efficient mechanism
		end

	ulong_to_octets_le (v: NATURAL_32): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			if not is_little_endian then
				c_put_ulong (c_endian_swapped_ulong (v), mp.item)
			else
				c_put_ulong (v, mp.item)
			end
			Result := address_to_octets (mp.item, 4)
		end

	--|--------------------------------------------------------------

	long_long_to_octets_le (v: INTEGER_64): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (8)
			if not is_little_endian then
				c_put_long_long (c_endian_swapped_long_long (v), mp.item)
			else
				c_put_long_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 8)
			-- TODO Need a more efficient mechanism
		end

	ulong_long_to_octets_le (v: NATURAL_64): STRING_8
			-- Little-Endian octet stream equivalent of value 'v'
		local
			mp: MANAGED_POINTER
			--ca: ANY
		do
			create mp.make (8)
			if not is_little_endian then
				c_put_ulong_long (c_endian_swapped_ulong_long (v), mp.item)
			else
				c_put_ulong_long (v, mp.item)
			end
			Result := address_to_octets (mp.item, 8)
			--ca := Result.area
			--memcopy (mp.item, $ca, 8)
		end

--|========================================================================
feature -- Octet streams to integral values - Little-Endian
--|========================================================================

	octets_to_n16_le (v: STRING): NATURAL_16
			-- Unsigned 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ushort_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_ushort (Result)
			end
		end

	octets_to_i16_le (v: STRING): INTEGER_16
			-- Signed 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_short_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_short (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_n32_le (v: STRING): NATURAL_32
			-- Unsigned 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_ulong (Result)
			end
		end

	octets_to_i32_le (v: STRING): INTEGER_32
			-- Signed 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_long (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_n64_le (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_long_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_ulong_long (Result)
			end
		end

	--|--------------------------------------------------------------

	octets_to_i64_le (v: STRING): INTEGER_64
			-- Signed 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_long_at (cstr.item)
			if not is_little_endian then
				Result := c_endian_swapped_long_long (Result)
			end
		end

--|========================================================================
feature -- Octet conversion (platform dependent)
--|========================================================================

	byte_to_octets (v: NATURAL_8): STRING_8
			-- Octet stream equivalent of 'v'
		local
			mp: MANAGED_POINTER
		do
			create mp.make (1)
			mp.put_natural_8 (v, 0)
			Result := address_to_octets (mp.item, 1)
		end

	--|--------------------------------------------------------------

	short_to_octets_ne (v: INTEGER_16): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			mp.put_integer_16 (v, 0)
			Result := address_to_octets (mp.item, 2)
			-- TODO Need a more efficient mechanism
		end

	ushort_to_octets_ne (v: NATURAL_16): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
		do
			create mp.make (2)
			mp.put_natural_16 (v, 0)
			Result := address_to_octets (mp.item, 2)
		end

	--|--------------------------------------------------------------

	long_to_octets_ne (v: INTEGER_32): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			mp.put_integer_32 (v, 0)
			Result := address_to_octets (mp.item, 4)
			-- TODO Need a more efficient mechanism
		end

	ulong_to_octets_ne (v: NATURAL_32): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
		do
			create mp.make (4)
			mp.put_natural_32 (v, 0)
			Result := address_to_octets (mp.item, 4)
		end

	--|--------------------------------------------------------------

	long_long_to_octets_ne (v: INTEGER_64): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
		do
			create mp.make (8)
			mp.put_integer_64 (v, 0)
			Result := address_to_octets (mp.item, 8)
			-- TODO Need a more efficient mechanism
		end

	ulong_long_to_octets_ne (v: NATURAL_64): STRING_8
			-- Platform-dependent octet stream equivalent of 'v'
			-- (not endian-aware)
		local
			mp: MANAGED_POINTER
			--ca: ANY
		do
			create mp.make (8)
			mp.put_natural_64 (v, 0)
			Result := address_to_octets (mp.item, 8)
			--ca := Result.area
			--memcopy (mp.item, $ca, 8)
		end

	--|--------------------------------------------------------------

	address_to_octets (p: POINTER; nb: INTEGER): STRING_8
			-- Stream of 'nb' bytes at address 'p'
		local
			i: INTEGER
		do
			create Result.make (nb)
			from i := 0
			until i >= nb
			loop
				Result.extend (c_byte_at (p + i).to_character_8)
				i := i + 1
			end
		end

--|========================================================================
feature -- Octet streams to integral values
--|========================================================================

	octets_to_n8 (v: STRING): NATURAL_8
			-- Unsigned 8 bit integer equivalent of 1-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 1
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_byte_at (cstr.item)
		end

	--|--------------------------------------------------------------

	octets_to_n16_ne (v: STRING): NATURAL_16
			-- Unsigned 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ushort_at (cstr.item)
		end

	octets_to_i16_ne (v: STRING): INTEGER_16
			-- Signed 16 bit integer equivalent of 2-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 2
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_short_at (cstr.item)
		end

	--|--------------------------------------------------------------

	octets_to_n32_ne (v: STRING): NATURAL_32
			-- Unsigned 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_at (cstr.item)
		end

	octets_to_i32_ne (v: STRING): INTEGER_32
			-- Signed 32 bit integer equivalent of 4-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 4
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_at (cstr.item)
		end

	--|--------------------------------------------------------------

	octets_to_n64_ne (v: STRING): NATURAL_64
			-- Unsigned 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_ulong_long_at (cstr.item)
		end

	--|--------------------------------------------------------------

	octets_to_i64_ne (v: STRING): INTEGER_64
			-- Signed 64 bit integer equivalent of 8-octet string 'v'
		require
			exists: v /= Void
			valid_length: v.count = 8
		local
			cstr: C_STRING
		do
			create cstr.make (v)
			Result := c_long_long_at (cstr.item)
		end

--|========================================================================
feature -- Byte operations
--|========================================================================

	highest_bit_set (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the most significant bit
			-- that is set in the given byte
			-- If none is set, then -1
		do
			if v = 0 then
				Result := -1
			else
				Result := hi_bits_by_value.item (v)
			end
		end

	highest_bit_set_16 (v: NATURAL_16): INTEGER
			-- The number (from 0 to 15, of the most significant bit
			-- that is set in the given byte
			-- If none is set, then -1
		local
			i, lim: NATURAL_16
		do
			Result := -1
			if v /= 0 then
				lim := 15
				from i := lim
				until i < 0 or Result /= -1
				loop
					if v.bit_test (i) then
						Result := i
					else
						i := i - 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	highest_bit_unset (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the most significant bit
			-- that is UNSET in the given byte
			-- If none is UNSET (all are set), then -1
		do
			Result := highest_bit_set (byte_complement (v))
		end

	--|--------------------------------------------------------------

	lowest_bit_set (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7), of the least significant bit
			-- that is set in the given byte
			-- If none is set, then -1
			--
			-- All odd numbers have 1 as lowest bit set
		local
			i: INTEGER
			m: NATURAL_8
		do
			Result := -1
			if v /= 0 then
				from
					i := 0
					m := 1
				until i > 7 or Result >= 0
				loop
					if v & m /= 0 then
						Result := i
					else
						i := i + 1
						m := m |<< 1
					end
				end
			end
		end

	lowest_bit_set_16 (v: NATURAL_16): INTEGER
			-- The number (from 0 to 15, of the least significant bit
			-- that is set in the given byte
			-- If none is set, then -1
		local
			i, lim: NATURAL_16
		do
			Result := -1
			if v /= 0 then
				lim := 15
				from i := 0
				until i > lim or Result /= -1
				loop
					if v.bit_test (i) then
						Result := i
					else
						i := i + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	lowest_bit_unset (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the least significant bit
			-- that is UNSET in the given byte
			-- If none is UNSET (all are set), then -1
		local
			i: INTEGER
			m: NATURAL_8
		do
			if v = 0 then
				Result := 0
			else
				Result := -1
				if v /= 0xff then
					from
						i := 0
						m := 1
					until i > 7 or Result >= 0
					loop
						if v & m = 0 then
							Result := i
						else
							i := i + 1
							m := m |<< 1
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	byte_complement (v: NATURAL_8): NATURAL_8
			-- Complement of 'v'
		do
			Result := c_byte_complement (v)
		end

	--|--------------------------------------------------------------

	num_bits_set_8 (v: NATURAL_8): INTEGER
			-- Number of bits set in the given byte 'v'
		do
			Result := c_num_bits_set_8 (v)
		end

	--|--------------------------------------------------------------

	num_bits_set_16 (v: NATURAL_16): INTEGER
			-- Number of bits set in the given ushort 'v'
		do
			Result := c_num_bits_set_16 (v)
		end

	--|--------------------------------------------------------------

	num_bits_set_32 (v: NATURAL_32): INTEGER
			-- Number of bits set in the given unsigned long 'v'
		do
			Result := c_num_bits_set_32 (v)
		end

--|========================================================================
feature -- Value setting - Big-Endian
--|========================================================================

	put_short_be (v: INTEGER_16; p: POINTER)
			-- Insert 'v' as 2-byte SIGNED value to address 'p', in
			-- Big-Endian SHORT form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_short (
					c_endian_swapped_ushort (v.as_natural_16).as_integer_16, p)
			else
				--| already big-endian
				c_put_short (v, p)
			end
		end

	put_ushort_be (v: NATURAL_16; p: POINTER)
			-- Insert 2-byte value 'v' to address 'p', in
			-- Big-Endian USHORT form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_ushort (c_endian_swapped_ushort (v), p)
			else
				--| already big-endian
				c_put_ushort (v, p)
			end
		end

	--|--------------------------------------------------------------

	put_long_be (v: INTEGER_32; p: POINTER)
			-- Insert 'v' as 4-byte signed value to address 'p', in
			-- Big-endian LONG form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_long (
					c_endian_swapped_ulong (v.as_natural_32).as_integer_32, p)
			else
				--| already big-endian
				c_put_long (v, p)
			end
		end

	put_ulong_be (v: NATURAL_32; p: POINTER)
			-- Insert 'v' as 4-byte signed value to address 'p', in
			-- Big-endian LONG form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_ulong (c_endian_swapped_ulong (v), p)
			else
				--| already little-endian
				c_put_ulong (v, p)
			end
		end

	--|--------------------------------------------------------------

	put_long_long_be (v: INTEGER_64; p: POINTER)
			-- Insert 'v' as 8-byte signed value to address 'p', in
			-- Big-endian LONG LONG form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_long_long (
					c_endian_swapped_ulong_long (v.as_natural_32).as_integer_64, p)
			else
				--| already big-endian
				c_put_long_long (v, p)
			end
		end

	put_ulong_long_be (v: NATURAL_64; p: POINTER)
			-- Insert 'v' as 8-byte signed value to address 'p', in
			-- Big-endian LONG LONG form
		require
			not_null: p /= default_pointer
		do
			if is_little_endian then
				c_put_ulong_long (c_endian_swapped_ulong_long (v), p)
			else
				--| already little-endian
				c_put_ulong_long (v, p)
			end
		end

--|========================================================================
feature -- Value setting - Little-Endian
--|========================================================================

	put_short_le (v: INTEGER_16; p: POINTER)
			-- Insert 'v' as 2-byte SIGNED value to address 'p', in
			-- Little-Endian SHORT form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_short (
					c_endian_swapped_ushort (v.as_natural_16).as_integer_16, p)
			else
				--| already little-endian
				c_put_short (v, p)
			end
		end

	put_ushort_le (v: NATURAL_16; p: POINTER)
			-- Insert 2-byte value 'v' to address 'p', in
			-- Little-Endian USHORT form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_ushort (c_endian_swapped_ushort (v), p)
			else
				--| already little-endian
				c_put_ushort (v, p)
			end
		end

	--|--------------------------------------------------------------

	put_long_le (v: INTEGER_32; p: POINTER)
			-- Insert 'v' as 4-byte signed value to address 'p', in
			-- Little-endian LONG form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_long (
					c_endian_swapped_ulong (v.as_natural_32).as_integer_32, p)
			else
				--| already little-endian
				c_put_long (v, p)
			end
		end

	put_ulong_le (v: NATURAL_32; p: POINTER)
			-- Insert 'v' as 4-byte signed value to address 'p', in
			-- Little-endian LONG form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_ulong (c_endian_swapped_ulong (v), p)
			else
				--| already little-endian
				c_put_ulong (v, p)
			end
		end

	--|--------------------------------------------------------------

	put_long_long_le (v: INTEGER_64; p: POINTER)
			-- Insert 'v' as 8-byte signed value to address 'p', in
			-- Little-endian LONG LONG form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_long_long (
					c_endian_swapped_ulong_long (v.as_natural_64).as_integer_64, p)
			else
				--| already little-endian
				c_put_long_long (v, p)
			end
		end

	put_ulong_long_le (v: NATURAL_64; p: POINTER)
			-- Insert 'v' as 8-byte signed value to address 'p', in
			-- Little-endian U LONG LONG form
		require
			not_null: p /= default_pointer
		do
			if not is_little_endian then
				c_put_ulong_long (c_endian_swapped_ulong_long (v), p)
			else
				--| already little-endian
				c_put_ulong_long (v, p)
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	are_equal (p1, p2: POINTER; nb: INTEGER): BOOLEAN
			-- Do 'p1' and 'p2' have the same values for the next 'nb' 
			-- bytes (inclusive)?
		require
			first_valid: p1 /= default_pointer
			second_valid: p2 /= default_pointer
			valid_count: nb > 0
		do
			Result := c_memcmp (p1, p2, nb) = 0
		end

	pointers_compared (p1, p2: POINTER): INTEGER
			-- Comparison of pointers p1 and p2.
			-- If equal values, Result = 0
			-- If p1 > p2 then Result > 0
			-- If p1 < p2 then Result < 0
		do
			--| The base libraries do not offer a conversion from pointer 
			--| to int 64
			Result := c_pointers_compared (p1, p2)
		end

 --|========================================================================
feature -- Bulk memory operations
 --|========================================================================

	memset (p: POINTER; v: INTEGER; nb: INTEGER)
			-- Set 'nb' memory locations starting at address 'p' with 
			-- value 'v'
		require
			not_null: p /= default_pointer
		do
			c_memset (p, v, nb)
		end

	memclear (p: POINTER; nb: INTEGER)
			-- Set to zero, 'nb' memory locations starting at address 'p' 
		require
			not_null: p /= default_pointer
		do
			c_memset (p, 0, nb)
		end

	--|--------------------------------------------------------------

	memcopy (p1, p2: POINTER; nb: INTEGER)
			-- Copy 'nb' bytes from p1 to p2
			-- NOT OVERLAPPING (use memmove if overlapping)
		require
			first_valid: p1 /= default_pointer
			second_valid: p2 /= default_pointer
			valid_count: nb > 0
			no_overlap: ((p1 + nb).to_integer_32 < p2.to_integer_32) or
				((p2 + nb).to_integer_32 < p1.to_integer_32)
		do
			--| N.B. args are reversed!!!
			c_memcpy (p2, p1, nb)
		ensure
			same: are_equal (p1, p2, nb)
		end

	memove (p1, p2: POINTER; nb: INTEGER)
			-- Copy 'nb' bytes from p1 to p2
			-- Supports OVERLAPPING addresses
		require
			first_valid: p1 /= default_pointer
			second_valid: p2 /= default_pointer
			valid_count: nb > 0
		do
			c_memmove (p2, p1, nb)
		ensure
			same: are_equal (p1, p2, nb)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	hi_bits_by_value: ARRAY [INTEGER]
			-- The highest set 0-based bit positions for a given value
			-- If no bits are set, the result is -1
			--
			-- NOTE WELL that this is a 0-based array!!
		local
			i: INTEGER
		once
			create Result.make_filled (0, 0, 255)
			Result.put (-1, 0) -- 00000000
			Result.put (0, 1)  -- 00000001
			Result.put (1, 2)  -- 00000010
			Result.put (1, 3)  -- 00000011
			Result.put (2, 4)  -- 00000100
			Result.put (2, 5)  -- 00000101
			Result.put (2, 6)  -- 00000110
			Result.put (2, 7)  -- 00000111
			from i := 8
			until i > 15
			loop
				Result.put (3, i) -- 00001000 - 00001111
				i := i + 1
			end
			from i := 16
			until i > 31
			loop
				Result.put (4, i) -- 00010000 - 00011111
				i := i + 1
			end
			from i := 32
			until i > 63
			loop
				Result.put (5, i) -- 00100000 - 00111111
				i := i + 1
			end
			from i := 64
			until i > 127
			loop
				Result.put (6, i) -- 01000000 - 01111111
				i := i + 1
			end
			from i := 128
			until i > 255
			loop
				Result.put (7, i) -- 10000000 - 11111111
				i := i + 1
			end
		ensure
			exists: Result /= Void
			valid_range: Result.lower = 0 and Result.upper = 255
		end

	bit_masks_8: ARRAY [NATURAL_8]
			-- Single bit masks for each bit in an 8-bit natural,
			-- indexed from 0 through 7
			-- To support bitwise iteration from least to most 
			-- significant bits in a byte
		once
			Result := {ARRAY [NATURAL_8]}<< 1, 2, 4, 8, 16, 32, 64, 128 >>
			Result.rebase (0)
		end

--|========================================================================
feature -- Externals
--|========================================================================

	c_byte_at (p: POINTER): NATURAL_8
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((unsigned char *)$p));
			}"
		end

	c_char_at (p: POINTER): CHARACTER_8
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((char *)$p));
			}"
		end

	c_short_at (p: POINTER): INTEGER_16
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((short *)$p));
			}"
		end

	c_ushort_at (p: POINTER): NATURAL_16
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((unsigned short *)$p));
			}"
		end

	c_long_at (p: POINTER): INTEGER_32
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((long *)$p));
			}"
		end

	c_ulong_at (p: POINTER): NATURAL_32
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((unsigned long *)$p));
			}"
		end

	c_long_long_at (p: POINTER): INTEGER_64
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((long long *)$p));
			}"
		end

	c_ulong_long_at (p: POINTER): NATURAL_64
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((unsigned long long *)$p));
			}"
		end

	c_float_at (p: POINTER): REAL_32
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((float *)$p));
			}"
		end

	c_double_at (p: POINTER): REAL_64
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			return (*((double *)$p));
			}"
		end

	--|--------------------------------------------------------------

	c_strlen (p: POINTER): INTEGER
		external
			"C signature (char *): EIF_INTEGER use <string.h>"
		alias
			"strlen"
		end

	--|--------------------------------------------------------------

	c_pointer_to_n64 (p: POINTER): NATURAL_64
			-- Unsigned long long equivalent of pointer 'p'
		external
			"C inline"
		alias
			"{
			return ((long long) $p);
			}"
		end

	c_pointers_compared (p1, p2: POINTER): INTEGER
			-- Comparison of pointers p1 and p2.
			-- If equal values, Result = 0
			-- If p1 > p2 then Result > 0
			-- If p1 < p2 then Result < 0
		external
			"C inline"
		alias
			"{
			if ($p1 == $p2) {
				return (0);
			} else if ($p1 > $p2) {
				return (1);
			} else if ($p2 > $p1) {
				return (-1);
			}
			}"
		end

	--|--------------------------------------------------------------

	c_endian_test: INTEGER
			-- Test for endian-ness of current platform
			-- If little-endian, result is 1
			-- If big-endian, result is 256
		external
			"C inline"
		alias
			"{
			unsigned char a8 [2] = { 1, 0 };
			return *((short *) a8);
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_short (s: INTEGER_16): INTEGER_16
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2;
			b1 = $s & 255;
			b2 = ($s >> 8) & 255;
			return (b1 << 8) + b2;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_long (i: INTEGER_32): INTEGER_32
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2, b3, b4;
			b1 = $i & 255;
			b2 = ($i >> 8) & 255;
			b3 = ($i >> 16) & 255;
			b4 = ($i >> 24) & 255;
			return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_long_long (v: INTEGER_64): INTEGER_64
		external
			"C inline"
		alias
			"{
			union
			{
				unsigned long long ll;
				unsigned char b[8];
				} dat1, dat2;

				dat1.ll = $v;
				dat2.b[0] = dat1.b[7];
				dat2.b[1] = dat1.b[6];
				dat2.b[2] = dat1.b[5];
				dat2.b[3] = dat1.b[4];
				dat2.b[4] = dat1.b[3];
				dat2.b[5] = dat1.b[2];
				dat2.b[6] = dat1.b[1];
				dat2.b[7] = dat1.b[0];
				return dat2.ll;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_ushort (s: NATURAL_16): NATURAL_16
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2;
			b1 = $s & 255;
			b2 = ($s >> 8) & 255;
			return (b1 << 8) + b2;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_ulong (i: NATURAL_32): NATURAL_32
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2, b3, b4;
			b1 = $i & 255;
			b2 = ($i >> 8) & 255;
			b3 = ($i >> 16) & 255;
			b4 = ($i >> 24) & 255;
			return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_ulong_long (v: NATURAL_64): NATURAL_64
		external
			"C inline"
		alias
			"{
			union
			{
				unsigned long long ll;
				unsigned char b[8];
				} dat1, dat2;

				dat1.ll = $v;
				dat2.b[0] = dat1.b[7];
				dat2.b[1] = dat1.b[6];
				dat2.b[2] = dat1.b[5];
				dat2.b[3] = dat1.b[4];
				dat2.b[4] = dat1.b[3];
				dat2.b[5] = dat1.b[2];
				dat2.b[6] = dat1.b[1];
				dat2.b[7] = dat1.b[0];
				return dat2.ll;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_float (f: REAL_32): REAL_32
		external
			"C inline"
		alias
			"{
			union
			{
				float f;
				unsigned char b[4];
				} dat1, dat2;

				dat1.f = $f;
				dat2.b[0] = dat1.b[3];
				dat2.b[1] = dat1.b[2];
				dat2.b[2] = dat1.b[1];
				dat2.b[3] = dat1.b[0];
				return dat2.f;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_double (d: REAL_64): REAL_64
		external
			"C inline"
		alias
			"{
			union
			{
				double d;
				unsigned char b[8];
				} dat1, dat2;

				dat1.d = $d;
				dat2.b[0] = dat1.b[7];
				dat2.b[1] = dat1.b[6];
				dat2.b[2] = dat1.b[5];
				dat2.b[3] = dat1.b[4];
				dat2.b[4] = dat1.b[3];
				dat2.b[5] = dat1.b[2];
				dat2.b[6] = dat1.b[1];
				dat2.b[7] = dat1.b[0];
				return dat2.d;
			}"
		end

	--|--------------------------------------------------------------

	c_put_byte (v: NATURAL_8; p: POINTER)
			-- Write 1-byte value 'v' at address 'p'
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((unsigned char *)$p) = $v;
			}"
		end

	c_put_char (v: CHARACTER_8; p: POINTER)
			-- Write 1-byte signed character value 'v' at address 'p'
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((char *)$p) = $v;
			}"
		end

	c_put_short (v: INTEGER_16; p: POINTER)
			-- Write 2-byte signed value 'v' to address 'p' in 
			-- platform-dependent SHORT form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((short *)$p) = $v;
			}"
		end

	c_put_ushort (v: NATURAL_16; p: POINTER)
			-- Write 2-byte unsigned value 'v' to address 'p' in 
			-- platform-dependent USHORT form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((unsigned short *)$p) = $v;
			}"
		end

	c_put_long (v: INTEGER_32; p: POINTER)
			-- Write 4-byte signed value 'v' to address 'p' in 
			-- platform-dependent LONG form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((long *)$p) = $v;
			}"
		end

	c_put_ulong (v: NATURAL_32; p: POINTER)
			-- Write 4-byte unsigned value 'v' to address 'p' in 
			-- platform-dependent ULONG form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((unsigned long *)$p) = $v;
			}"
		end

	c_put_long_long (v: INTEGER_64; p: POINTER)
			-- Write 8-byte signed value 'v' to address 'p' in 
			-- platform-dependent LONG LONG form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((long long *)$p) = $v;
			}"
		end

	c_put_ulong_long (v: NATURAL_64; p: POINTER)
			-- Write 8-byte unsigned value 'v' to address 'p' in 
			-- platform-dependent ULONG LONG form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((unsigned long long *)$p) = $v;
			}"
		end

	c_put_float (v: REAL_32; p: POINTER)
			-- Write 4-byte float value 'v' to address 'p' in 
			-- platform-dependent FLOAT form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((float *)$p) = $v;
			}"
		end

	c_put_double (v: REAL_64; p: POINTER)
			-- Write 8-byte float value 'v' to address 'p' in 
			-- platform-dependent DOUBLE form (not endian-aware)
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			*((double *)$p) = $v;
			}"
		end

	c_put_string (v: POINTER; len: INTEGER_32; p: POINTER)
			-- Write string value 'v' to address 'p', null-terminated
		require
			valid_pointer: p /= default_pointer
		external
			"C inline"
		alias
			"{
			int i, lim;
			char *s = (char *)$v;
			char *cp = (char *)$p;
			lim = $len;
			for(i=0;i<lim;i++){
				cp[i]= s[i];
			}
			}"
		end

--|========================================================================
feature -- Bit/Byte Functions (borrowed from ael_numeric_integer_routines)
--|========================================================================

	c_byte_complement (v: NATURAL_8): NATURAL_8
			-- Complement of 'v'
		require
			in_range: v >= 0 and v <= 255
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
				return ~($v);
			}
			]"
		end

	--|--------------------------------------------------------------

	c_num_bits_set_8 (byte: NATURAL_8): INTEGER
			-- Number of bits set in the given byte
			-- Uses Kernighan's algorithm
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned char v; // count the number of bits set in v
				int Result=0; // c accumulates the total bits set in v
				v = $byte;
				for (; v; Result++) {
				  v &= v - 1; // clear the least significant bit set
				}
				return Result;
				}
			]"
		end

	c_num_bits_set_16 (num: NATURAL_16): INTEGER
			-- Number of bits set in the given 16 bit number
			-- Uses Kernighan's algorithm
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned short v; // count the number of bits set in v
				int Result=0; // c accumulates the total bits set in v
				v = $num;
				for (; v; Result++) {
				  v &= v - 1; // clear the least significant bit set
				}
				return Result;
				}
			]"
		end

	c_num_bits_set_32 (num: NATURAL_32): INTEGER
			-- Number of bits set in the given 32 bit number
			-- Uses Kernighan's algorithm
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned int v; // count the number of bits set in v
				int Result=0; // c accumulates the total bits set in v
				v = $num;
				for (; v; Result++) {
				  v &= v - 1; // clear the least significant bit set
				}
				return Result;
				}
			]"
		end

	c_lowest_bit_set_8 (byte: NATURAL_8): INTEGER
			-- Least significant 1 bit in 'byte'
			-- -1 if none is set
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned char v, m;
				int i=0;
				v = $byte;
				if (v==0) { return (-1); }
				for (m=1; i<8; i++) {
					if (v & m) { return (i); }
					i++;
					m << i;
				}
				return (-1);
				}
			]"
		end

--|========================================================================
feature -- Externals borrowed from POINTER_REF in the Base libraries
--|========================================================================

	c_memcpy (destination, source: POINTER; count: INTEGER)
			-- C memcpy
		external
			"C signature (void *, const void *, size_t) use <string.h>"
		alias
			"memcpy"
		end

	c_memmove (destination, source: POINTER; count: INTEGER)
			-- C memmove
		external
			"C signature (void *, const void *, size_t) use <string.h>"
		alias
			"memmove"
		end

	c_memset (source: POINTER; val: INTEGER; count: INTEGER)
			-- C memset
		external
			"C signature (void *, int, size_t) use <string.h>"
		alias
			"memset"
		end

	c_memcmp (source, other: POINTER; count: INTEGER): INTEGER
			-- C memcmp
		external
			"C signature (void *, void *, size_t): EIF_INTEGER use <string.h>"
		alias
			"memcmp"
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Jan-2013
--|     Compiled and tested on Eiffel 7.1
--| TODO rework with conditionals to deal with 32 vs 64 bit longs, and
--| with the lack of stdint.h support for Windows.
--|----------------------------------------------------------------------
--| How-to
--|
--| Inherit or Instantiate using default create
--|----------------------------------------------------------------------

end -- class AEL_SPRT_MEMORY_ROUTINES
