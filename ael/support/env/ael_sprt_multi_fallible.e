--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A class providing support for the notion of errors with support
--| for multiple errors.
--|----------------------------------------------------------------------

class AEL_SPRT_MULTI_FALLIBLE

--|========================================================================
feature -- Status
--|========================================================================

	has_error: BOOLEAN
			-- Has there been an error recorded?
		do
			Result :=  attached errors_list as t and then not t.is_empty
		ensure
			message_if_true: Result implies last_error /= Void
		end

	--|--------------------------------------------------------------

	number_of_errors: INTEGER
		do
			if attached errors_list as t then
				if not t.is_empty then
					Result := t.count
				end
			end
		end

	--|--------------------------------------------------------------

	first_error_message: STRING
			-- First recorded error message
		do
			if attached first_error as t then
				Result := t.message
			else
				Result := ""
			end
		end

	first_error_code: INTEGER
			-- First recorded error code
			-- Most recent error code
		do
			if attached first_error as t then
				Result := t.code
			end
		end

	first_error: detachable AEL_SPRT_ERROR_INSTANCE
			-- First recorded error, if any (Void if none recorded)
		do
			if attached errors as t and then not t.is_empty then
				Result := t.first
			end
		end

	--|--------------------------------------------------------------

	last_error_message: STRING
			-- Most recent error message
		do
			if attached last_error as t then
				Result := t.message
			else
				Result := ""
			end
		end

	last_error_code: INTEGER
			-- Most recent error code
		do
			if attached last_error as t then
				Result := t.code
			end
		end

	last_error: detachable AEL_SPRT_ERROR_INSTANCE
			-- Most recent error, if any (Void if none recorded)
		do
			if attached errors as t and then not t.is_empty then
				Result := t.last
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_error (c: INTEGER; msg: STRING)
			-- Set the error state with the given error code 'c'
			-- and message 'msg'
		require
			msg_exists: msg /= Void and then not msg.is_empty
		local
			el: like errors_list
		do
			if attached errors_list as lel then
				el := lel
			else
				create el.make
			end
			el.extend (create {like last_error}.make (c, msg))
			el.last.set_calling_class (generator)
			errors_list := el
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	set_error_message (msg: STRING)
			-- Set the error state with the given error message
			-- Set the error code to '1'
		require
			msg_exists: msg /= Void and then not msg.is_empty
		do
			set_error (1, msg)
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	set_error_message_formatted, record_error (fmt: STRING; args: detachable ANY)
			-- Set the error state with the given error message format 
			-- values (using printf conventions)
			-- Set the error code to '1'
		require
			msg_exists: fmt /= Void and then not fmt.is_empty
		do
			set_error (1, apf.aprintf (fmt, args))
		ensure
			has_error: has_error
		end

	--|--------------------------------------------------------------

	clear_errors
			-- Clear error codes and messages, if any
		do
			if attached errors_list as t then
				t.wipe_out
			end
		ensure
			no_error: not has_error
		end

	--|--------------------------------------------------------------

	copy_errors (other: AEL_SPRT_MULTI_FALLIBLE)
			-- Copy from other any recorded errors
		require
			exists: other /= Void
		local
			occ: detachable CURSOR
		do
			if other.has_error then
				if not attached errors_list then
					create errors_list.make
				end
				if attached errors_list as el then
					if attached other.errors as oel then
						occ := oel.cursor
						el.fill (oel)
						oel.go_to (occ)
					end
				end
			end
		ensure
			other_unchanged: other.is_equal (old other)
		end

--|========================================================================
feature {NONE} -- Error on exit support
--|========================================================================

	exit
			-- Exit the application immediately, without raising an
			-- exception, and presenting an exit status code denoting
			-- the last_error_code if has_error, else zero.
		do
			(create {EXCEPTIONS}).die (last_error_code)
		end

--|========================================================================
feature -- Status
--|========================================================================

	errors: TWO_WAY_LIST [AEL_SPRT_ERROR_INSTANCE]
			-- List of recorded errors
		do
			if attached errors_list as el then
				Result := el
			else
				create Result.make
			end
		end

	error_messages: TWO_WAY_LIST [STRING]
			-- Reported error messages
		do
			create Result.make
			if attached errors_list as el then
				el.do_all (agent add_error_message (?, Result))
			end
		ensure
			exists: Result /= Void
			complete: Result.count = errors.count
			sample_correct: not Result.is_empty implies
				Result.first = errors.first.message
		end

	error_codes: TWO_WAY_LIST [INTEGER]
			-- Reported error codes
		do
			create Result.make
			if attached errors_list as el then
				el.do_all (agent add_error_code (?, Result))
			end
		ensure
			exists: Result /= Void
			complete: Result.count = errors.count
			sample_correct: not Result.is_empty implies
				Result.first = errors.first.code
		end

--|========================================================================
feature {NONE} -- Agents
--|========================================================================

	add_error_message (v: AEL_SPRT_ERROR_INSTANCE; tl: like error_messages)
		do
			tl.extend (v.message)
		end

	add_error_code (v: AEL_SPRT_ERROR_INSTANCE; tl: like error_codes)
		do
			tl.extend (v.code)
		end

--|========================================================================
feature -- Reporting
--|========================================================================

	error_report (oa: ARRAY [BOOLEAN]): STRING
			-- Formatted error report, per options provided
			-- If 'oa'[1], then show sequence numbers
			-- If 'oa'[2] then show error codes
			-- If 'oa'[3], then clear errors when finished reporting
		require
			exists: oa /= Void
			one_based: oa.lower = 1
		local
			nf, cf, clr, icmf: BOOLEAN
			mc: CURSOR
			i, lim: INTEGER
			fmt: STRING
			eargs: ARRAY [ANY]
		do
			create Result.make (1024)
			lim := oa.count
			if lim >= 1 then
				-- If 'nf', then show sequence numbers
				nf := oa.item (1)
			end
			if lim >= 2 then
				-- If 'cf' then show error codes
				cf := oa.item (2)
			end
			if lim >= 3 then
				-- If 'clr', then clear errors when finished reporting
				clr := oa.item (3)
			end

			--RFO would have used an agent mechanism but that runs afoul 
			-- of Void safety with full class checking enabled and 
			-- calling this function from a rescue clause
			if nf and cf then
				fmt := "%%d. (%%d)  %%s%N"
				icmf := True
			elseif nf then
				fmt := "%%d. %%s%N"
			elseif cf then
				fmt := "(%%d)  %%s%N"
			else
				fmt := "%%s%N"
			end
			if attached errors_list as el then
				mc := el.cursor
				from
					el.start
					i := 1
				until el.exhausted
				loop
					if ok_to_show_caller then
						Result.append (apf.aprintf ("%%s: ", el.item.calling_class))
					end
					if icmf then
						eargs := << i, el.item.code, el.item.message >>
					elseif nf then
						eargs := << i, el.item.message >>
					elseif cf then
						eargs := << el.item.code, el.item.message >>
					else
						eargs := {ARRAY [ANY]}<< el.item.message >>
					end
					Result.append (apf.aprintf (fmt, eargs))
					--Result.append (opf.item ([i, el.item.code, el.item.message]))
					el.forth
					i := i + 1
				end
				el.go_to (mc)
			end
			if clr then
				clear_errors
			end
		end

	--|--------------------------------------------------------------

	print_errors (nf, cf, clr: BOOLEAN)
			-- Report all recorded errors by printing to console
			-- If 'nf', then show sequence numbers
			-- If 'cf' then show error codes
			-- If 'clr', then clear errors when finished reporting
		do
			print_errors_with_options (<<nf, cf, clr>>)
		end

	--|--------------------------------------------------------------

	print_errors_with_options (oa: ARRAY [BOOLEAN])
			-- Report all recorded errors by printing to console
			-- according to options in array 'oa'
			-- If 'oa'[1], then show sequence numbers
			-- If 'oa'[2] then show error codes
			-- If 'oa'[3], then clear errors when finished reporting
		require
			exists: oa /= Void
			one_based: oa.lower = 1
		do
			print (error_report (oa))
		end

--|========================================================================
feature {NONE} -- Shared flags
--|========================================================================

	ok_to_show_caller: BOOLEAN
		do
			Result := caller_flag_cell.item
		end

	caller_flag_cell: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	enable_show_caller
		do
			caller_flag_cell.put (True)
		end

	disable_show_caller
		do
			caller_flag_cell.put (False)
		end

--|========================================================================
feature {NONE} -- Output agents
--|========================================================================

--RFO 	out_error_code_only (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Include only the error code per line
--RFO 		do
--RFO 			Result := apf.aprintf ("%%d%N", <<c>>)
--RFO 		end

--RFO 	out_error_index_and_code (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Include the index, the error code per line
--RFO 		do
--RFO 			Result := apf.aprintf ("$$d. %%d%N", <<i, c>>)
--RFO 		end

--RFO 	out_error_msg_only (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Include only the error message per line
--RFO 		do
--RFO 			Result := apf.aprintf ("%%s%N", m)
--RFO 		end

--RFO 	out_error_index_and_msg (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Include index and error message per line
--RFO 		do
--RFO 			Result := apf.aprintf ("%%d. %%s%N", <<i, m>>)
--RFO 		end

--RFO 	out_error_code_and_msg (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Include only the error code and the error message per line
--RFO 		do
--RFO 			Result := apf.aprintf ("(%%d)  %%s%N", <<c, m>>)
--RFO 		end

--RFO 	out_full_error_with_index (i, c: INTEGER; m: STRING): STRING
--RFO 			-- Print to console the error entry with values:
--RFO 			-- 'i'=position, 'c'=error code, 'm'=error message
--RFO 			-- Includes index, error code and message per line
--RFO 		do
--RFO 			Result := apf.aprintf ("%%d. (%%d)  %%s%N", <<i, c, m>>)
--RFO 		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	errors_list: detachable TWO_WAY_LIST [AEL_SPRT_ERROR_INSTANCE]
			-- Actual, but not visible, error list
			-- Represented by 'error_list' to ensure void safety to 
			-- callers

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class AEL_SPRT_MULTI_FALLIBLE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 11-Feb-2016
--|     Rewoked report_errors to used flags and locals rather than 
--|     the previous implementation that used agents.  As this 
--|     function was being called from a rescue clause, it was 
--|     resulting in compile-time Void safety errors.  Removing the 
--|     agent assignments alleviated the problem.
--| 003 02-Sep-2010
--|     Change implementation to use a single list of error objects
--|     rather than the original 2 lists (codes and messages).
--|     Added global flag to include class name of caller on
--|     set_error.
--|     Reworked output options, moving to an array of options rather
--|     than an argument list of individual options.
--|     Added error_report function that provides the error history,
--|     with format options, without requiring a print; updated rest
--|     of reporting logic to use this core function.
--| 002 28-Jul-2009
--|     Added history.
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| This class should be inherited by any class that wants to track errors
--|----------------------------------------------------------------------
