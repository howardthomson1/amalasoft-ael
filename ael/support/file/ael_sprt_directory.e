note
	description: "{
An enhanced DIRECTORY
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2017 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_DIRECTORY

inherit
	DIRECTORY

create
	make, make_open_read

--|========================================================================
feature -- Traversal
--|========================================================================

	traverse (cb: PROCEDURE [STRING];
	          dots, rf: BOOLEAN; em: detachable STRING)
			-- Read this directory contents, calling the given agent at
			-- each entry.  If 'dots' then make agent calls for '.' and
			-- '..' entries, else do not.
			-- If 'rf' then read recursively, else do not.
			--
			-- If 'em' is non-void, then write any error messages to it
			--
			-- N.B. This routine leaves this directory state unchanged
			-- as it uses a surrogate for traversal
		local
			td, sd: like Current
			tname: DIRECTORY_NAME
			tf: RAW_FILE
		do
			create td.make (path.name)
			if not td.exists then
				if attached em as m then
					m.wipe_out
					m.append ("Directory [" + path.name.to_string_8 + "] does not exist.")


--					m.append (create {STRING}.make_from_string ("Directory [") + path.name + "] does not exist.")
--					m.append ("Directory [")
--					m.append (path.name.to_string_8)
--					m.append ("] does not exist.")

				end
			else
				td.open_read
				if td.is_closed then
					if attached em as m then
						m.wipe_out
						m.append ("Unable to open directory [" + path.name.to_string_8 + "].")
					end
				else
					from td.readentry
					until td.last_entry_8 = Void
					loop
						if attached td.last_entry_8 as lde then
							if (not is_dot (lde)) or dots then
								create tname.make_from_string (td.path.name.as_string_8)
								tname.extend (lde)
								if cb /= Void then
									cb.call ([tname])
								end
								if rf then
									create tf.make_with_name (tname)
									if (tf.is_directory) then
										create sd.make (tname)
										sd.read_recursively (cb, dots, em)
									end
								end
							end
						end
						td.readentry
					end
					td.close
				end
			end
		end

	--|--------------------------------------------------------------

	read_recursively (cb: PROCEDURE [STRING];
	                  dots: BOOLEAN; em: detachable STRING)
			-- Read this directory contents recursively, calling the
			-- given agent at each entry.  If 'dots' then make agent calls
			-- for '.' and '..' entries, else do not
			--
			-- If 'em' is non-void, then write any error messages to it
			--
			-- N.B. This routine leaves this directory state unchanged
			-- as it uses a surrogate for traversal
		do
			traverse (cb, dots, True, em)
		end

	--|--------------------------------------------------------------

	read_all (cb: PROCEDURE [STRING];
	          dots: BOOLEAN; em: detachable STRING)
			-- Read this directory contents superficially, calling the
			-- given agent at each entry.  If 'dots' then make agent calls
			-- for '.' and '..' entries, else do not
			--
			-- If 'em' is non-void, then write any error messages to it
			--
			-- N.B. This routine leaves this directory state unchanged
			-- as it uses a surrogate for traversal
		do
			traverse (cb, dots, False, em)
		end

--|========================================================================
feature -- Copy
--|========================================================================

	recursive_copy_directory (dn: STRING)
			-- Copy recursively Current to 'dn'.
			-- Do nothing if the directory could not be copied,
			-- if it did not exist, or if `new_name' already exists.
			--
			-- Do nothing if the directory can not be copied
			-- or if 'dn' already exists
		require
			valid_name: dn /= Void and then not dn.is_empty
		local
			retrying: BOOLEAN
			a_dir: AEL_SPRT_DIRECTORY
			new_root: AEL_SPRT_DIRECTORY
			subname: DIRECTORY_NAME
			new_subname: DIRECTORY_NAME
			afr: AEL_SPRT_FILE_ROUTINES
		do
			if (not retrying) and then exists then
				create afr
				create new_root.make (dn)
				if not new_root.exists and not afr.file_by_name_exists (dn) then
					new_root.recursive_create_dir
					open_read
					if not is_closed then
						from readentry
						until last_entry_8 = Void
						loop
							if attached last_entry_8 as tname
								and then not is_dot (tname)
							 then
								create subname.make_from_string (path.name.as_string_8)
								subname.extend (tname)
								create new_subname.make_from_string (dn)
								new_subname.extend (tname)
								if afr.fsnode_is_directory (subname) then
									create a_dir.make (subname)
									a_dir.recursive_copy_directory (new_subname)
								else
									afr.copy_file (subname, new_subname)
								end
							end
							readentry
						end
						close
					end
				end
			end
		rescue
			if not retrying then
				retrying := True
				retry
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	reset (dn: STRING)
			-- Change directory name to 'dn' and reset
			-- file descriptor and all information.
		require
			valid_file_name: dn /= Void
		do
			set_name (dn)
			if mode /= Close_directory then
				close
			end
--RFO 			last_entry := Void
			start
		ensure
			renamed: path.name ~ dn
			closed: is_closed
		end

--|========================================================================
feature -- Support
--|========================================================================

	is_dot (v: STRING): BOOLEAN
			-- Is the given string either "." or ".."?
		do
			if (v.count > 0 and v.count <= 2) and then v.item (1) = '.' then
				Result := v.count = 1 or v.item (2) = '.'
			end
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 19-Dec-2017
--|     Retrofitted with template hdr/trailer
--|     Changed (obsolete) calls to 'lastentry' to 'last_entry_8'.
--|     Removed Void-ing of (obsolete) 'lastentry' in 'reset', replaced
--|     with call to 'start'
--|     Compiled with Eiffel Studio 17.05, Void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_DIRECTORY
