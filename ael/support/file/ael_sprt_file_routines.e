--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| File-related support routines
--|----------------------------------------------------------------------

class AEL_SPRT_FILE_ROUTINES

--|========================================================================
feature -- Access queries
--|========================================================================

	file_by_name_exists (fn: STRING): BOOLEAN
			-- Does a file by name 'fn' exist locally on disk?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.exists
		end

	--|--------------------------------------------------------------

	file_path_exists (fn: STRING): BOOLEAN
			-- Does a file by name 'fn' exist locally on disk, without
			-- following symbolic links?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.path_exists
		end

	--|--------------------------------------------------------------

	file_is_readable_by_euid (fn: STRING): BOOLEAN
			-- Does the named file allow read access for the current 
			-- user, by effective uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_readable
		end

	--|--------------------------------------------------------------

	file_is_readable_by_ruid (fn: STRING): BOOLEAN
			-- Does the named file allow read access for the current 
			-- user, by real uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_access_readable
		end

	--|--------------------------------------------------------------

	file_is_writable_by_euid (fn: STRING): BOOLEAN
			-- Does the named file allow write access for the current 
			-- user, by effective uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_writable
		end

	--|--------------------------------------------------------------

	file_is_writable_by_ruid (fn: STRING): BOOLEAN
			-- Does the named file allow write access for the current 
			-- user, by real uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_access_writable
		end

	--|--------------------------------------------------------------

	file_is_executable_by_euid (fn: STRING): BOOLEAN
			-- Does the named file allow execute access for the current 
			-- user, by effective uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_executable
		end

	--|--------------------------------------------------------------

	file_is_executable_by_ruid (fn: STRING): BOOLEAN
			-- Does the named file allow execute access for the current 
			-- user, by real uid?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_access_executable
		end

--|========================================================================
feature -- File/node type queries
--|========================================================================

	fsnode_is_directory (fn: STRING): BOOLEAN
			-- Is the named filesytem node a directory?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_directory
		end

	--|--------------------------------------------------------------

	fsnode_is_device (fn: STRING): BOOLEAN
			-- Is the named filesytem node a device?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_device
		end

	--|--------------------------------------------------------------

	fsnode_is_symlink (fn: STRING): BOOLEAN
			-- Is the named filesytem node a symbolic link?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_symlink
		end

	--|--------------------------------------------------------------

	fsnode_is_regular_file (fn: STRING): BOOLEAN
			-- Is the named filesytem node a plain (regular) file?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_plain
		end

	--|--------------------------------------------------------------

	fsnode_is_fifo (fn: STRING): BOOLEAN
			-- Is the named filesytem node a FIFO?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_fifo
		end

	--|--------------------------------------------------------------

	fsnode_is_socket (fn: STRING): BOOLEAN
			-- Is the named filesytem node a socket?
		require
			valid_name: fn /= Void and then not fn.is_empty
		do
			scratch_file.reset (fn)
			Result := scratch_file.is_socket
		end

--|========================================================================
feature -- File Info type labels
--|========================================================================

	file_info_file_type_label (v: INTEGER): STRING
			-- Label corresponding to file type from FILE_INFO class
			-- File type in FILE_INFO is  4 bits, 12 lowest bits zeroed
		do
			-- TODO, decypher, experimentally, the meaning of the 
			-- encodings
			-- Plain text appears to be 32768 (1000 0000 0000 0000)
			-- By the comment in FILE_INFO, we should be looking at 
			-- 1000b and ignoring the lower 12 bits
			Result := "Unknown"
		end

--|========================================================================
feature -- Directory support
--|========================================================================

	is_directory_readable (dn: STRING): BOOLEAN
			-- Does directory 'dn' exist, and if so, is it
			-- accessible for reading?
		require
			valid_name: dn /= Void and then not dn.is_empty
		do
			scratch_dir.reset (dn)
			if scratch_dir.exists then
				Result := scratch_dir.is_readable
			end
		end

	--|--------------------------------------------------------------

	directory_exists (dn: STRING): BOOLEAN
			-- Does directory 'dn' physically exist on disk?
		require
			valid_name: dn /= Void and then not dn.is_empty
		local
			td: DIRECTORY
		do
			create td.make (dn)
			Result := td.exists
		end

	--|--------------------------------------------------------------

	is_directory_empty (dn: STRING): BOOLEAN
			-- Does directory 'dn' exist, and if so, does it
			-- contain no entries other than '.' and '..'?
		require
			valid_name: dn /= Void and then not dn.is_empty
		do
			scratch_dir.reset (dn)
			Result := scratch_dir.is_empty
		end

	--|--------------------------------------------------------------

	create_directory (dn: STRING)
			-- Create a new directory named `dn'.
			--
			-- Do nothing if the directory could not
			-- be created, if it already existed or if
			-- `dn' is a complext path and the parent does not exist.
		require
			valid_name: dn /= Void and then not dn.is_empty
		local
			td: DIRECTORY
		do
			create td.make (dn)
			if not td.exists then
				td.create_dir
			end
		end

	--|--------------------------------------------------------------

	recursive_create_directory (dn: STRING)
			-- Create on disk a new directory named `dn', creating
			-- intermediate directories in the path as needed.
			--
			-- Do nothing if the directory could not be created,
			-- if it already existed or intermediate directories do not 
			-- exist, cannot be created or are inaccessible.
		require
			valid_name: dn /= Void and then not dn.is_empty
		local
			td: DIRECTORY
		do
			create td.make_with_name (dn)
			if not td.exists then
				td.recursive_create_dir
			end
		end

	--|--------------------------------------------------------------

	delete_directory (dn: STRING)
			-- Delete from disk the directory named `dn'.
			-- Do nothing if the directory could not be deleted,
			-- if it did not exist or if it is not empty.
		require
			valid_name: dn /= Void and then not dn.is_empty
		do
			scratch_dir.reset (dn)
			if scratch_dir.exists then
				scratch_dir.delete
			end
		end

	--|--------------------------------------------------------------

	recursive_delete_directory (dn: STRING)
			-- Recursively delete from disk the directory named `dn'
			-- and its contents, including subdirectories and files
		require
			valid_name: dn /= Void and then not dn.is_empty
		do
			scratch_dir.reset (dn)
			if scratch_dir.exists then
				scratch_dir.recursive_delete
			end
		end

	--|--------------------------------------------------------------

	recursive_copy_directory (old_name, new_name: STRING)
			-- Copy recursively directory named `old_name' to `new_name'.
			-- Do nothing if the directory could not be copied,
			-- if it did not exist, or if `new_name' already exists.
		require
			valid_old_name: old_name /= Void and then not old_name.is_empty
			valid_old_name: new_name /= Void and then not new_name.is_empty
		do
			scratch_dir.reset (old_name)
			if scratch_dir.exists then
				scratch_dir.recursive_copy_directory (new_name)
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	paths_are_same_file (p1, p2: STRING): BOOLEAN
			-- Do the 2 file pathnames refer to the same phyiscal file?
		require
			valid_path1: p1 /= Void and then not p1.is_empty
			valid_path2: p2 /= Void and then not p2.is_empty
		do
			scratch_file.reset (p1)
			Result := scratch_file.same_file (p2)
		end

	--|--------------------------------------------------------------

	files_have_same_contents (p1, p2: STRING): BOOLEAN
			-- Do files p1 and p2 have identical contents?
		require
			names_exist: p1 /= Void and p2 /= Void
		local
			f1, f2: RAW_FILE
		do
			create f1.make_with_name (p1)
			create f2.make_with_name (p2)
			if f1.exists and f2.exists then
				f1.open_read
				f2.open_read
				if f1.is_open_read and f2.is_open_read
					and then f1.count = f2.count
				 then
					 Result := True
				end
			end
			if Result then
				from
				until (not Result) or f1.end_of_file or f2.end_of_file
				loop
					f1.read_stream (1024)
					f2.read_stream (1024)
					Result := f1.last_string ~ f2.last_string
				end
			end
			if f1.is_open_read then
				f1.close
			end
			if f2.is_open_read then
				f2.close
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	file_size (fn: STRING): INTEGER
			-- Size of file by name
		require
			valid_name: fn /= Void and then not fn.is_empty
			src_exists: file_by_name_exists (fn)
		do
			scratch_file.reset (fn)
			Result := scratch_file.count
		end

	--|--------------------------------------------------------------

	file_info (fn: STRING): FILE_INFO
			-- Information about the file.
		require
			valid_name: fn /= Void and then not fn.is_empty
			src_exists: file_by_name_exists (fn)
		do
			if scratch_file.path.name /~ fn then
				scratch_file.reset (fn)
			end
			Result := scratch_file.file_info
		end

	--|--------------------------------------------------------------

	file_mtime (fn: STRING): INTEGER
			-- Time file 'fn' was most recently modified, in seconds 
			-- since epoch, UTC
		require
			valid_name: fn /= Void and then not fn.is_empty
			src_exists: file_by_name_exists (fn)
		local
			ufi: like file_info
		do
			ufi := file_info (fn)
			Result := ufi.change_date
		end

--|========================================================================
feature -- Copying, moving, deleting
--|========================================================================

	copy_file (src, dst: STRING)
			-- Copy contents of file 'src' to file 'dst', overwriting 
			-- contents of 'dst' if it exists
		require
			names_exist: src /= Void and dst /= Void
			src_exists: file_by_name_exists (src)
		local
			sf, df: RAW_FILE
		do
			create sf.make_open_read (src)
			create df.make_open_write (dst)
			sf.copy_to (df)
			sf.close
			df.close
		end

	concatenate_files (fn1, fn2: STRING)
			-- Copy contents of file 'fn2' to the end of file 'fn1'.
			--
			-- Do nothing if file 'fn2' does not exist.
			-- Do nothing if file 'fn2' cannot be opened for append
			-- or if file 'fn2' cannot be opened for read.
			--
			-- Create file 'fn1' if it does not exist yet.
			-- If file 'fn2' is the same actual file as 'fn1',
			-- then append a copy of 'fn1' to itself.
		require
			valid_name1: fn1 /= Void and then not fn1.is_empty
			valid_name2: fn2 /= Void and then not fn2.is_empty
		local
			sf: RAW_FILE
		do
			scratch_file.reset (fn1)
			create sf.make_with_name (fn2)
			if sf.exists and then sf.is_access_readable then
				if scratch_file.is_access_writable then
					scratch_file.append (sf)
				end
			end
		end

	--|--------------------------------------------------------------

	rename_file (old_nm, new_nm: STRING)
			-- Rename file named 'old_nm' to 'new_nm'
		require
			names_exist: old_nm /= Void and new_nm /= Void
			original_exists: file_by_name_exists (old_nm)
		local
			sf: RAW_FILE
		do
			create sf.make_with_name (old_nm)
			sf.rename_file (new_nm)
		end

	--|--------------------------------------------------------------

	rename_directory (old_nm, new_nm: STRING)
			-- Rename directory named 'old_nm' to 'new_nm'
		require
			names_exist: old_nm /= Void and new_nm /= Void
			original_exists: directory_exists (old_nm)
		local
			td: DIRECTORY
		do
			create td.make (old_nm)
			td.change_name (new_nm)
		end

	--|--------------------------------------------------------------

	remove_file (fn: STRING)
			-- Remove the named file from disk
		require
			valid_name: fn /= Void and then fn.count > 0
			file_exists: file_by_name_exists (fn)
		do
			scratch_file.reset (fn)
			scratch_file.delete
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	file_to_lines (fn: STRING): LINKED_LIST [STRING]
			-- Convert the given text file into a sequence of lines
			-- Newline character is consumed for each line
		require
			valid_name: fn /= Void and then not fn.is_empty
			file_exists: file_by_name_exists (fn)
			is_plain: fsnode_is_regular_file (fn)
			is_readable: file_is_readable_by_euid (fn)
		local
			tf: PLAIN_TEXT_FILE
		do
			create Result.make
			create tf.make_open_read (fn)
			from
				tf.start
				tf.read_line
			until tf.end_of_file
			loop
				Result.extend (tf.last_string.twin)
				tf.read_line
			end
			tf.close
      end

--|========================================================================
feature -- File I/O
--|========================================================================

	print_file (fn: STRING; bs: INTEGER; emsg: detachable STRING)
			-- Write the contents of file 'fn' to standard output
			-- in blocks of 'bs' bytes at a time.
			-- If 'bs' is zero, try to write in optimal sized blocks
			--
			-- If there is an error during this operation, put the error 
			-- message into the (optional) 'emsg'
		require
			valid_name: fn /= Void and then not fn.is_empty
			file_exists: file_by_name_exists (fn)
			is_plain: fsnode_is_regular_file (fn)
			is_readable: file_is_readable_by_euid (fn)
			valid_count: bs >= 0
		local
			inf: RAW_FILE
			err: detachable STRING
		do
			create inf.make_with_name (fn)
			if not inf.exists then
				err := "File does not exist."
			else
				inf.open_read
				if not inf.is_open_read then
					err := "Could not open file."
				else
					put_file (inf, Void, bs, emsg)
					inf.close
				end
			end
			if attached err as ae and attached emsg as am then
				am.wipe_out
				am.append (ae)
			end
      end

	--|--------------------------------------------------------------

	file_contents (fn: STRING; emsg: STRING): STRING
			-- The contents of file 'fn' as a single string
			--
			-- If there is an error during this operation, put the error 
			-- message into the (optional) 'emsg'
		require
			valid_name: fn /= Void and then not fn.is_empty
			file_exists: file_by_name_exists (fn)
			is_plain: fsnode_is_regular_file (fn)
			is_readable: file_is_readable_by_euid (fn)
		do
			Result := partial_file_contents (fn, 1, 0, emsg)
      end

	--|--------------------------------------------------------------

	partial_file_contents (fn: STRING; sp, ep: INTEGER; emsg: STRING): STRING
			-- The contents of file 'fn', from file position 'sp' through 
			-- file position 'ep', as a string
			--
			-- 'sp' and 'ep' are 1-based
			-- If 'sp' is larger than the size of the file, it is an error
			-- If 'ep' is larger than the size of the file, then the end
			-- position will be the end of the file, and is not an error
			-- If 'ep' is zero, then end position will be then end of 
			-- the file, and is not an error
			--
			-- If there is an error during this operation, put the error 
			-- message into the (optional) 'emsg'
		require
			valid_name: fn /= Void and then not fn.is_empty
			file_exists: file_by_name_exists (fn)
			is_plain: fsnode_is_regular_file (fn)
			is_readable: file_is_readable_by_euid (fn)
			valid_start: sp > 0
			valid_end: ep = 0 or ep >= sp
		local
			inf: RAW_FILE
			err: detachable STRING
			rd_size: INTEGER
		do
			create Result.make (0)
			create inf.make_with_name (fn)
			if not inf.exists then
				err := "File does not exist."
			else
				if sp > inf.count then
					err := "Invalid start position"
				else
					inf.open_read
					if not inf.is_open_read then
						err := "Could not open file."
					end
				end
			end
			if err = Void then
				inf.start
				if sp > 1 then
					inf.move (sp - 1)
				end
				if ep = 0 then
					rd_size := inf.count - sp + 1
				else
					rd_size := ep.min (inf.count) - sp + 1
				end
				inf.read_stream (rd_size)
				Result := inf.last_string
				inf.close
			end
			if err /= Void and emsg /= Void then
				emsg.wipe_out
				emsg.append (err)
			end
      end

	--|--------------------------------------------------------------

	put_file (
		inf: FILE; outf: detachable FILE; bs: INTEGER; emsg: detachable STRING)
			-- Write the contents of file 'inf' to output file 'outf'
			-- in blocks of 'bs' bytes at a time.
			-- If 'bs' is zero, try to write in optimal sized blocks
			--
			-- If there is an error during this operation, put the error 
			-- message into the (optional) 'emsg'
		require
			input_exists: inf /= Void
			input_open: inf.is_open_read
			output_open: outf /= Void implies outf.is_open_write
			input_file_exists: inf.exists
			output_file_exists: outf /= Void implies outf.exists
			input_is_readable: inf.is_readable
			output_is_writable: outf /= Void implies outf.is_writable
			valid_count: bs >= 0
		local
			outfile: FILE
			err: detachable STRING
			rs: INTEGER
		do
			if bs = 0 then
				-- Determine optimal block size
				rs := inf.count.min (10_000)
			else
				rs := bs.min (inf.count)
			end
			if attached outf as f then
				outfile := f
			else
				outfile := io.output
			end
			from inf.start
			until inf.end_of_file or err /= Void
			loop
				inf.read_stream (rs)
				outfile.put_string (inf.last_string)
			end
			if err /= Void and emsg /= Void then
				emsg.wipe_out
				emsg.append (err)
			end
		ensure
			input_closure_unchanged: inf.is_closed = old inf.is_closed
			output_closure_unchanged: outf /= Void implies
				inf.is_closed = old inf.is_closed
      end

	--|--------------------------------------------------------------

	write_string_to_file (str, fn: STRING; af: BOOLEAN)
			-- Write string 'str' to file 'fn'
			-- If 'af', then append 'str' to end of existing file,
			-- else, overwrite any existing content with 'str'
		require
			string_exists: str /= Void
			filename_exists: fn /= Void
		do
			write_substring_to_file (str, fn, 1, str.count, af)
		end

	--|--------------------------------------------------------------

	write_substring_to_file (str, fn: STRING; sp, ep: INTEGER; af: BOOLEAN)
			-- Write substring 'str' to file 'fn'
			-- If 'af', then append 'str' to end of existing file,
			-- else, overwrite any existing content with 'str'
		require
			string_exists: str /= Void
			filename_exists: fn /= Void
		local
			tf: PLAIN_TEXT_FILE
			i, nb: INTEGER
			ss: SPECIAL [CHARACTER]
		do
			if ep /= 0 and ep >= sp then
				nb := ep - sp + 1
			end
			create tf.make_with_name (fn)
			if af and tf.exists then
				tf.open_append
			else
				tf.open_write
			end
			if not tf.is_closed then
				if nb /= 0 then
					ss := str.area
					from i := 0
					until i >= nb
					loop
						tf.put_character (ss.item (i + sp - 1))
						i := i + 1
					end
				end
				tf.close
			end
		end

--|========================================================================
feature -- Temp file support (N.B. requires process library!!)
--|========================================================================

	--RFO new_local_temp_filename (fn: STRING; i: INTEGER): FILE_NAME
	--RFO 		-- A new, unique, temporary file name
	--RFO 		-- Temp file name will share the same directory (and
	--RFO 		-- propably file system, unless a symlink) as the filename
	--RFO 		-- given
	--RFO 	require
	--RFO 		name_exists: fn /= Void and then not fn.is_empty
	--RFO 	local
	--RFO 		fname: STRING
	--RFO 		asr: AEL_SPRT_STRING_ROUTINES
	--RFO 	do
	--RFO 		create asr
	--RFO 		create Result.make_from_string (asr.dirname (fn))
	--RFO 		fname := asr.aprintf (
	--RFO 			"_tmp_%%s-%%d-%%d-%%d",
	--RFO 			<< asr.smart_basename (fn), pform.process_id, i, next_fn_index >>)
	--RFO 		Result.extend (fname)
	--RFO 	ensure
	--RFO 		exists: Result /= Void
	--RFO 	end

	new_local_temp_filename (fn: STRING; i: INTEGER): STRING
			-- A new, unique, temporary file name
			-- Temp file name will share the same directory (and
			-- propably file system, unless a symlink) as the filename
			-- given
		require
			name_exists: fn /= Void and then not fn.is_empty
		do
			Result := new_local_temp_path (fn, i).name.as_string_8
		ensure
			exists: Result /= Void
		end

	new_local_temp_path (fn: STRING; i: INTEGER): PATH
			-- A new, unique, temporary file path
			-- Temp file name will share the same directory (and
			-- propably file system, unless a symlink) as the filename
			-- given
		require
			name_exists: fn /= Void and then not fn.is_empty
		local
			fname: STRING
			asr: AEL_SPRT_STRING_ROUTINES
		do
			create asr
			create Result.make_from_string (asr.dirname (fn))
			fname := asr.aprintf (
				"_tmp_%%s-%%d-%%d-%%d",
				<< asr.smart_basename (fn), pform.process_id, i, next_fn_index >>)
			Result := Result.extended (fname)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	next_fn_index: INTEGER
		-- Autoincrementing index used to create unique filenames
		do
			Result := next_fn_index_ref.item
			next_fn_index_ref.put (Result + 1)
		ensure
			more: Result = old next_fn_index + 1
		end

	next_fn_index_ref: CELL [INTEGER]
			-- Shared incrementing indes for temp filename creation
		once
			create Result.put (0)
		end

	--|--------------------------------------------------------------

	pform: AEL_SPRT_PLATFORM
		once
			create Result
		end

	--|--------------------------------------------------------------

	scratch_file: RAW_FILE
			-- Scratch file object used for file operations rather than 
			-- creating a separate local for each operation.
			-- Routines should reset the (name of) this file before
			-- each operation using it.
		once
			create Result.make_with_name ("scratchfilename")
		ensure
			file_not_void: Result /= Void
			file_closed: Result.is_closed
		end

	scratch_dir: AEL_SPRT_DIRECTORY
			-- Scratch directort object used for directory operations
			-- rather than creating a separate local for each operation.
			-- Routines should reset the (name of) this directory before
			-- each operation using it.
		once
			create Result.make ("scratchdirname")
		ensure
			file_not_void: Result /= Void
			file_closed: Result.is_closed
		end

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 19-Jan-2013
--|     Recompiled and tested, void-safe, with Eiffel 7.1
--|     Change from using local tmp files to a shared scratch file
--| 002 16-Sep-2010
--|     Added file_size and file_info functions
--|     Added print_file, put_file, write_string_to_file,
--|     write_substring_to_file, file_contents, partial_file_contents
--|     Moved new_local_temp_filename here (though it requires the
--|     'process' library)
--| 001 28-Apr-2010
--|     Created original from MUCH older legacy apt class A_FUTIL
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries and other Amalasoft clusters.
--|----------------------------------------------------------------------

end -- class AEL_SPRT_FILE_ROUTINES
