note
	description: "{
File System Related Features.
Meant as void-safe substitute for Gobo's KL_FILE_SYSTEM classes,
with kudos and thanks to Gobo of course
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/01/14 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_FS_UTILS

inherit
	AEL_SPRT_FILE_ROUTINES
		redefine
			rename_file, copy_file, is_directory_readable, directory_exists,
			is_directory_empty, create_directory, recursive_create_directory,
			delete_directory, recursive_delete_directory,
			recursive_copy_directory
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_current_file_system: BOOLEAN
			-- Is file system the file system of the underlying platform?
		do
--RFO 			Result := implementation.is_current_file_system
		end

--|========================================================================
feature -- File handline
--|========================================================================

	is_file_readable (a_filename: STRING): BOOLEAN
			-- Can file named 'a_filename' be opened in read mode?
		require
			a_filename_not_void: a_filename /= Void
		do
			if not a_filename.is_empty then
				Result := file_is_readable_by_euid (a_filename)
			end
		end

	--|--------------------------------------------------------------

	file_exists (a_filename: STRING): BOOLEAN
			-- Does file named 'a_filename' physically exist on disk?
		require
			a_filename_not_void: a_filename /= Void
		do
			if not a_filename.is_empty then
				Result := file_by_name_exists (a_filename)
			end
		end

	--|--------------------------------------------------------------

	file_count (a_filename: STRING): INTEGER
			-- Number of bytes in file named 'a_filename';
			-- Return -1 if the number of bytes was not available,
			-- if the file did not exist for example.
		require
			a_filename_not_void: a_filename /= Void
		do
			Result := -1
			if (not a_filename.is_empty)
				and then file_by_name_exists (a_filename)
			 then
				 Result := file_size (a_filename)
			end
		ensure
			valid_values: Result = -1 or Result >= 0
		end

	--|--------------------------------------------------------------

	file_time_stamp (a_filename: STRING): INTEGER
			-- Time stamp (number of seconds since 1 January 1970
			-- at 00:00:00 UTC) of last modification to file 'a_filename';
			-- Return -1 if the time stamp was not available, if the
			-- file did not exist for example, or if the time stamp
			-- didn't fit into an INTEGER. (Use DT_DATE_TIME.make_from_epoch
			-- to convert this time stamp to a human readable format.)
		require
			a_filename_not_void: a_filename /= Void
		do
			Result := -1
			if (not a_filename.is_empty)
				and then file_by_name_exists (a_filename)
			 then
				 Result := file_mtime (a_filename)
			end
		ensure
			valid_values: Result = -1 or Result >= 0
		end

	--|--------------------------------------------------------------

	same_physical_file (a_filename1, a_filename2: STRING): BOOLEAN
			-- Are files named 'a_filename1' and 'a_filename2'
			-- the same physical file? Return False if one
			-- or both files don't exist. (Return True if
			-- it was impossible to determine whether the
			-- files were physically the same files.)
		require
			a_filename1_not_void: a_filename1 /= Void
			a_filename2_not_void: a_filename2 /= Void
		do
			if not a_filename1.is_empty and not a_filename2.is_empty then
				Result := paths_are_same_file (a_filename1, a_filename2)
			end
		end

	same_text_files (a_filename1, a_filename2: STRING): BOOLEAN
			-- Do files named 'a_filename1' and 'a_filename2'
			-- contain the same number of lines and are these
			-- lines equal? Return False if one or both files
			-- don't exist or cannot be open in read mode.
		require
			a_filename1_not_void: a_filename1 /= Void
			a_filename2_not_void: a_filename2 /= Void
		do
			Result := files_have_same_contents (a_filename1, a_filename2)
		end

	same_binary_files (a_filename1, a_filename2: STRING): BOOLEAN
			-- Do files named 'a_filename1' and 'a_filename2'
			-- contain the same number of characters and are these
			-- characters equal? Return False if one or both files
			-- don't exist or cannot be open in read mode.
		require
			a_filename1_not_void: a_filename1 /= Void
			a_filename2_not_void: a_filename2 /= Void
		do
			Result := files_have_same_contents (a_filename1, a_filename2)
		end

	rename_file (old_name, new_name: STRING)
			-- Rename file named 'old_name' as 'new_name'.
			-- Do nothing if the file could not be renamed, if
			-- it did not exist or if 'new_name' is physically
			-- the same file as current file. Overwrite 'new_name'
			-- if it already existed. ('old_name' and 'new_name'
		require else
			old_name_not_void: old_name /= Void
			new_name_not_void: new_name /= Void
		do
			if file_by_name_exists (old_name) then
				Precursor (old_name, new_name)
			end
		end

	--|--------------------------------------------------------------

	copy_file (old_name, new_name: STRING)
			-- Copy file named 'old_name' to 'new_name'.
			-- Do nothing if the file could not be copied, if it
			-- did not exist or if 'new_name' is physically
			-- the same file as 'old_name'. Overwrite 'new_name'
			-- if it already existed.
		require else
			old_name_not_void: old_name /= Void
			new_name_not_void: new_name /= Void
		do
			if file_by_name_exists (old_name) then
				Precursor (old_name, new_name)
			end
		end

	--|--------------------------------------------------------------

	concat_files (a_target_filename, a_source_filename: STRING)
			-- Copy content of file 'a_source_filename' to the end of file
			-- 'a_target_filename'. Do nothing if file 'a_source_filename'
			-- does not exist. Create file 'a_target_filename' if it does
			-- not exist yet. If file 'a_source_filename' is physically the
			-- same as file 'a_target_filename', then a copy of the file is
			-- appended to itself. Do nothing if file 'a_target_filename'
			-- could not be open in append mode or if file 'a_source_filename'
			-- could not be opened in read mode.
		require
			a_target_filename_not_void: a_target_filename /= Void
			a_source_filename_not_void: a_source_filename /= Void
		do
			if (not a_target_filename.is_empty) and
				(not a_source_filename.is_empty)
			 then
				 concatenate_files (a_target_filename, a_source_filename)
			end
		end

	--|--------------------------------------------------------------

	delete_file (a_filename: STRING)
			-- Delete file named 'a_filename'.
			-- Do nothing if the file could not be
			-- deleted or if it did not exist.
		require
			a_filename_not_void: a_filename /= Void
		do
			if (not a_filename.is_empty) and then file_by_name_exists (a_filename)
			 then
				 remove_file (a_filename)
			end
		end

 --|========================================================================
feature -- Directory handling
 --|========================================================================

	is_directory_readable (a_dirname: STRING): BOOLEAN
			-- Can directory named 'a_dirname' be opened in read mode?
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Result := Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	directory_exists (a_dirname: STRING): BOOLEAN
			-- Does directory named 'a_dirname' physically exist on disk?
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Result := Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	is_directory_empty (a_dirname: STRING): BOOLEAN
			-- Does directory named 'a_dirname' contain no entry apart
			-- from the parent and current directory entries?
			-- Return False if not able to open current directory.
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Result := Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	create_directory (a_dirname: STRING)
			-- Create a new directory named 'a_dirname'.
			-- Do nothing if the directory could not
			-- be created, if it already existed or if
			-- 'a_dirname' is a nested directory name
			-- and the parent directory does not exist.
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	recursive_create_directory (a_dirname: STRING)
			-- Create a new directory named 'a_dirname' on disk.
			-- Create its parent directories if they do not exist yet.
			-- Do nothing if the directory could not be created,
			-- if it already existed or 'name' is a nested directory
			-- name and its parent directory does not exist and
			-- could not be created.
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	delete_directory (a_dirname: STRING)
			-- Delete directory named 'a_dirname'.
			-- Do nothing if the directory could not be deleted,
			-- if it did not exist or if it is not empty.
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	recursive_delete_directory (a_dirname: STRING)
			-- Delete directory named 'a_dirname', its files
			-- and its subdirectories recursively. Do nothing if
			-- the directory could not be deleted, if it did not exist.
		require else
			name_exists: a_dirname /= Void
		do
			if not a_dirname.is_empty then
				Precursor (a_dirname)
			end
		end

	--|--------------------------------------------------------------

	recursive_copy_directory (old_name, new_name: STRING)
			-- Copy recursively directory named 'old_name' to 'new_name'.
			-- Do nothing if the directory could not be copied,
			-- if it did not exist, or if 'new_name' already existed.
		require else
			old_name_exists: old_name /= Void
			new_name_exists: new_name /= Void
		do
			if not old_name.is_empty and not new_name.is_empty then
				Precursor (old_name, new_name)
			end
		end

 --|========================================================================
feature -- Working directory
 --|========================================================================

	cwd, current_working_directory: STRING
			-- Name of current working directory;
			-- Return absolute pathname with the naming
			-- convention of the underlying file system
			-- (Return a new object at each call.)
		do
			Result := env.current_working_directory
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	cd, set_current_working_directory (a_dirname: STRING)
			-- Set current working directory to 'a_dirname'.
			-- Do nothing if the current working directory could not
			-- be changed or if directory 'a_dirname' did not exist.
		require
			valid_dirname: a_dirname /= Void
		local
			rescued: BOOLEAN
			a_string_dirname: STRING
		do
			if not rescued and then not a_dirname.is_empty then
				-- Prevent cat calls from args that might be string descendents
				a_string_dirname := a_dirname.string
				if not a_string_dirname.is_empty then
					env.change_working_directory (a_string_dirname)
				end
			end
		rescue
			if not rescued then
				rescued := True
				retry
			end
		end

--|========================================================================
feature -- Support
--|========================================================================

	new_input_file (a_name: STRING): PLAIN_TEXT_FILE
			-- New input text file in current file system
		require
			a_name_not_void: a_name /= Void
		do
			create Result.make (a_name)
		ensure
			input_file_not_void: Result /= Void
			name_set: Result.name = a_name
			is_closed: Result.is_closed
		end

	--|--------------------------------------------------------------

	new_output_file (a_name: STRING): PLAIN_TEXT_FILE
			-- New output text file in current file system
		require
			a_name_not_void: a_name /= Void
		do
			create Result.make (a_name)
		ensure
			output_file_not_void: Result /= Void
			name_set: Result.name = a_name
			is_closed: Result.is_closed
		end

	--|--------------------------------------------------------------

	eol: STRING
			-- Line separator in current file system
		do
--RFO 			Result := implementation.eol
Result := "%N"
		ensure
			eol_not_void: Result /= Void
			eol_not_empty: Result.count > 0
		end

	--|--------------------------------------------------------------

	env: EXECUTION_ENVIRONMENT
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--	implementation: AEL_SPRT_FS_UTILS_IMP
			-- Platform-specific implementation for Current

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Jan-2013
--|     Compiled and tested on Eiffel 7.1
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_FS_UTILS

