deferred class AEL_SPRT_PERSISTENT
-- Simple persistence support for generic objects using STORABLE
-- Inherit this from the class you want to persist

inherit
	AEL_SPRT_MULTI_FALLIBLE

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	init_from_other (v: like Current)
		require
			valid: other_is_compatible (v)
		do
			purge_attributes
			copy (v)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_saved: BOOLEAN
		do
			Result := path_exists_on_disk (pers_pathname)
		end

	bak_file_enabled: BOOLEAN
			-- On save, is creation of a .bak file enable?

--|========================================================================
feature -- Status setting
--|========================================================================

	purge_attributes
			-- Remove or reset all attributes
		deferred
		end

--|========================================================================
feature -- Validation
--|========================================================================

	other_is_compatible (v: like Current): BOOLEAN
			-- Is other sufficiently compatible with Current to be a 
			-- source for initialization (i.e. does other reprsent Current)?
		do
			--| Redefine in descendent to a bit smarter
			Result := v /= Void
		ensure
			exists_at_least: Result implies v /= Void
		end

	is_valid_pathname (v: STRING): BOOLEAN
			-- Is 'v' a valid persistent path name?
		require
			has_dirname: pers_dirname /= Void
			has_filename: pers_filename /= Void
		do
			if v /= Void and then not v.is_empty then
				Result := v.starts_with (pers_dirname) and
					v.ends_with (pers_filename)
			end
		end

--|========================================================================
feature -- I/O Operations
--|========================================================================

	save
		local
			--td: DIRECTORY
			tmp_file, orig_file: RAW_FILE
			bak_file: detachable RAW_FILE
			tmp_name: STRING
			bak_name: detachable STRING
		do
			create_save_dir
			tmp_name := pers_pathname + ".tmp"
			create orig_file.make_with_name (pers_pathname)
			create tmp_file.make_with_name (tmp_name)
			if bak_file_enabled then
				bak_name := pers_pathname + ".bak"
				create bak_file.make_with_name (bak_name)
			end
			tmp_file.open_write
			if not tmp_file.is_open_write then
				set_error_message ("Could not open " + tmp_name)
			else
				tmp_file.independent_store (Current)
				tmp_file.close
				if attached bak_file and then attached bak_name as l_bn then
					if bak_file.exists then
						bak_file.delete
					end
					if orig_file.exists then
						orig_file.rename_file (l_bn)
					end
				end
				tmp_file.rename_file (pers_pathname)
			end
		ensure
			saved: is_saved
		end

	--|--------------------------------------------------------------

	retrieve
		do
			--RFO, purge is called from init_from_other
			--purge
			--RFO, used to create a file if not existed
			-- this would be a side effect and is not longer
			-- done.
			if attached new_from_stored as tcs and not has_error then
				init_from_other (tcs)
			end
		end

	--|--------------------------------------------------------------

	new_from_stored: detachable like Current
			-- A new object of same type as Current, instantiated from 
			-- its stored image
		local
			tf: RAW_FILE
			td: DIRECTORY
			retrying: BOOLEAN
		do
			if not retrying then
				create td.make (pers_dirname)
				if not td.exists then
					-- Big trouble, directory is not there, so how can 
					-- file possibly be?
					set_error_message (
						"Directory " + pers_dirname + " does not exist")
				else
					create tf.make_with_name (pers_pathname)
					if not tf.exists then
						set_error_message (
							"Stored image " + pers_pathname + " does not exist.")
					else
						tf.open_read
						if not tf.is_open_read then
							set_error_message ("Could not open " + pers_pathname)
						else
							if attached {like new_from_stored} tf.retrieved as tc then
								Result := tc
							else
								set_error_message (
									pers_pathname + " is not a compatible format.")
							end
							tf.close
						end
					end
				end
			end
		rescue
			if not retrying then
				-- Retrieval error
				retrying := True
				retry
			end
		end

--|========================================================================
feature -- Paths
--|========================================================================

	pers_dirname: DIRECTORY_NAME
			-- Name of directory in which resides the file into which
			-- to store Current
		deferred
		ensure
			valid: Result /= Void and then not Result.is_empty
		end

	pers_pathname: FILE_NAME
			-- Full pathname of file into which to store Current
		do
			create Result.make_from_string (pers_dirname)
			Result.extend (pers_filename)
		ensure
			valid: is_valid_pathname (Result)
		end

	pers_filename: STRING
			-- Basename of file into which to store Current
		deferred
		ensure
			valid: Result /= Void and then not Result.is_empty
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	create_save_dir
			-- Create, if it does not already exists, the directory into 
			-- which to save Current
		local
			td: DIRECTORY
		do
			create td.make (pers_dirname)
			if not td.exists then
				td.recursive_create_dir
			end
		end

--|========================================================================
feature -- Assertion support
--|========================================================================

	path_exists_on_disk (v: STRING): BOOLEAN
		local
			rf: RAW_FILE
		do
			create rf.make_with_name (v)
			Result := rf.exists
		end

end -- class AEL_SPRT_PERSISTENT
