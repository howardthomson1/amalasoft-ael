--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| UNIX-style pathname (forward slash delimited)
--|----------------------------------------------------------------------

class AEL_SPRT_UX_PATHNAME

inherit
	STRING
		rename
			make as string_make,
			make_from_string as string_make_from_string,
			extend as string_extend
		export
			{AEL_SPRT_UX_PATHNAME} all
			{ANY} is_empty, empty, to_c, wipe_out, out, string, twin,
			prunable, hash_code
		end

create
	make, make_from_string, make_from_pathname

create {STRING}
	string_make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
			-- Create Current empty
		do
			string_make (0)
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create Current from the given string
		require
			exists: v /= Void
		do
			string_make (v.count)
			append (v)
		end

	--|--------------------------------------------------------------

	make_from_pathname (p: PATH_NAME)
			-- Create Current from given path name
			-- If the local directory separator is present, convert
			-- it to the UNIX-style separator
		require
			arg_exists: p /= Void
		local
			i, lim: INTEGER
			oe: OPERATING_ENVIRONMENT
			sep: CHARACTER
		do
			create oe
			sep := oe.directory_separator
			make_from_string (p)
			lim := count
			from i := 1
			until i > lim
			loop
				if item (i) = sep then
					put (directory_separator, i)
				end
				i := i + 1
			end
		end

 --|========================================================================
feature -- Element change
 --|========================================================================

	extend (dname: STRING)
			-- Append the subdir or file 'dname' as a path component
		require
			string_exists: dname /= Void
		do
			string_extend (directory_separator)
			append (dname)
		end

	--|--------------------------------------------------------------

	extend_from_array (directories: ARRAY [STRING])
			-- Append the subdirectories from `directories' to the path name.
		require
			array_exists: directories /= Void and then not (directories.is_empty)
		local
			i, nb: INTEGER
		do
			from
				i := directories.lower
				nb := directories.upper
			until
				i > nb
			loop
				extend (directories.item (i))
				i := i + 1
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	remove_leading_separators
			-- Remove from Current all leading directory separator 
			-- characters, if any exist
		do
			prune_all_leading (directory_separator)
		ensure
			pruned: (not is_empty) implies first_character /= directory_separator
		end

	remove_adjacent_separators
			-- Remove from Current all directory separators that occur 
			-- back-to-back (i.e. "//"), leaving only single separators
		local
			i, lim: INTEGER
			c, pc: CHARACTER
			ts: STRING
		do
			lim := count
			create ts.make (count)
			from i := 1
			until i > lim
			loop
				c := item (i)
				if c = directory_separator and pc = directory_separator then
					i := i + 1
				end
				pc := item (i)
				ts.extend (pc)
				i := i + 1
			end
			if ts.count /= count then
				wipe_out
				append (ts)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	first_character: like item
			-- First character in Current
			--'%U' if empty
		do
			if not is_empty then
				Result := item (1)
			end
		end

	--|--------------------------------------------------------------

	last_character: like item
			-- Last character in Current
			--'%U' if empty
		do
			if not is_empty then
				Result := item (count)
			end
		end

	has_segments: BOOLEAN
			-- Does Current have any segments (other than basename)?
		do
			Result := has (directory_separator)
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	to_components: LIST [STRING]
			-- Pathname components in list form
		do				
			Result := split (directory_separator)
			-- If there is a leading separator, split will create
			-- and extra, empty component; Remove it
			if Result.count > 1 and then Result.first.is_empty then
				Result.start
				Result.remove
			end
		end

	--|--------------------------------------------------------------

	less_top: like Current
			-- Copy of Current, but without first segment
		require
			not_empty: not is_empty
			has_segments: has_segments
		local
			ep: INTEGER
		do
			ep := index_of (directory_separator, 1)
			create Result.make_from_string (substring (ep+1, count))
		end

 --|========================================================================
feature -- Constants
 --|========================================================================

	directory_separator: CHARACTER = '/'

end -- class AEL_SPRT_UX_PATHNAME

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class using one of the listed creation 
--| routines.
--|----------------------------------------------------------------------
