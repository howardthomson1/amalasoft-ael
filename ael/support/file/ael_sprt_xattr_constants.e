class AEL_SPRT_XATTR_CONSTANTS

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	legal_xattr_namespaces: ARRAY [STRING]
		once
			Result := << "security", "system", "trusted", "user" >>
			Result.compare_objects
		end

	Kc_tv_separator: CHARACTER = ':'
			-- Character separationg the tag and value parts

	Kc_ns_separator: CHARACTER = '.'
			-- Character separationg the tag and namspace parts

end -- class AEL_SPRT_XATTR_CONSTANTS
