class AEL_SPRT_PLATFORM_IMP
-- Platform-specific support

--|========================================================================
feature -- Platform-specific routines
--|========================================================================

	process_id: INTEGER
			-- Current process's ID
		do
			Result := getpid
		end

	user_id: INTEGER
			-- Current user's ID
		do
			Result := getuid
		end

	user_name: STRING
			-- Current user's name (from OS)
		local
			env: EXECUTION_ENVIRONMENT
		do
			Result := "Unknown"
			create env
			if attached env.item ("USER") as ts and then not ts.is_empty then
				Result := ts
			end
		ensure
			exists: Result /= Void
		end

	user_id_by_name (v: STRING): INTEGER
			-- User ID corresponding to user name 'v'
			-- If there is no such user, then -1
		local
			nm: C_STRING
		do
			create nm.make (v)
			Result := pwuid_by_name (nm.item)
		end

	setuid_successful: BOOLEAN
			-- Was the last setuid attempt successful?
		do
			Result := setuid_flag_ref.item
		end

	setuid_flag_ref: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	--|--------------------------------------------------------------

	set_user_id (v: INTEGER)
		local
			ti: INTEGER
		do
			setuid_flag_ref.put (False)
			ti := setuid (v)
			if ti = 0 then
				setuid_flag_ref.put (True)
			end
		end

	msleep (v: INTEGER)
			-- Sleep for 'v' milliseconds
		do
			usleep (v // 1000)
		end

	--|--------------------------------------------------------------

	create_symlink (targ, lnk: STRING)
		local
			fp, np: ANY
		do
			fp := targ.to_c
			np := lnk.to_c
			symlink ($fp, $np)
		end

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	getpid: INTEGER
			-- Query the current process's PID
		external
			"C inline use %"unistd.h%""
		alias
			"return getpid();"
		end

	getuid: INTEGER
			-- Query the current user's ID
		external
			"C inline use %"unistd.h%""
		alias
			"return getuid();"
		end

	setuid (v: INTEGER): INTEGER
		-- Change the effective user id to 'v'
		external
			"C inline use %"unistd.h%""
		alias
			"return setuid ($v);"
		end

	pwuid_by_name (s: POINTER): INTEGER
			-- User ID corresponding to the username 's'
		external
			"C inline use <pwd.h>"
		alias
			"{
			struct passwd *pw;
			pw = getpwnam ($s);
			if (! pw) return -1;
			return pw->pw_uid;
			}"
		end

	--|--------------------------------------------------------------

	usleep (v: INTEGER)
			-- Sleep for 'v' units
		external
			"C inline use %"unistd.h%""
		alias
			"usleep($v);"
		end

	symlink (targ, lnk: POINTER)
			-- Create a symlink from near to far
		external
			"C inline use %"unistd.h%""
		alias
			"symlink($targ,$lnk);"
		end

end -- class AEL_SPRT_PLATFORM_IMP
