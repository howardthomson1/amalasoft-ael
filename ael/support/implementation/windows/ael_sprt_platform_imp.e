class AEL_SPRT_PLATFORM_IMP
-- Platform-specific support

--|========================================================================
feature -- Platform-specific routines
--|========================================================================

	process_id: INTEGER
			-- Current process's ID
		do
			Result := pinfo.process_id
		end

	user_id: INTEGER
			-- Current user's ID
		do
			-- Zero on Windows
		end

	user_name: STRING
			-- Current user's name (from OS)
		local
			st, len: INTEGER
			p: MANAGED_POINTER
			str: STRING_32
		do
			str := "Unknown"
			len := c_user_name_maxlen + 1
			create p.make (len)
			st := c_get_user_name_w (p.item, $len)
			if st /= 0 then
				create str.make_from_c (p.item)
				if len /= str.count then
					-- Might have not been a wide char, try ASCII
					len := c_user_name_maxlen + 1
					st := c_get_user_name_a (p.item, $len)
					if st /= 0 then
						create str.make_from_c (p.item)
					end
				end
			end
			Result := str
		ensure
			exists: Result /= Void
		end

	user_id_by_name (v: STRING): INTEGER
			-- User ID corresponding to user name 'v'
			-- If there is no such user, then -1
		do
			Result := -1
		end

	setuid_successful: BOOLEAN
			-- Was the last setuid attempt successful?
		do
			-- Always false on Windows
		end

	setuid_flag_ref: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	--|--------------------------------------------------------------

	set_user_id (v: INTEGER)
		do
			-- No such animal
		end

	msleep (v: INTEGER)
			-- Sleep for 'v' milliseconds
		local
			ex: EXECUTION_ENVIRONMENT
		do
			create ex
			ex.sleep (v * 1000_000)
		end

	usleep (v: INTEGER)
			-- Sleep for 'v' microseconds
		local
			ex: EXECUTION_ENVIRONMENT
		do
			create ex
			ex.sleep (v * 1000)
		end

	--|--------------------------------------------------------------

	create_symlink (targ, lnk: STRING)
		do
			-- No can do
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	pinfo: PROCESS_INFO_IMP
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	c_get_user_name_a (buf, lp: POINTER): INTEGER
			-- Get the current user name from the OS.
			-- Implementation is for ASCII user names only
		external
			"C inline use %"Lmcons.h%""
		alias
		"{
			return ((EIF_INTEGER) GetUserNameA ((LPTSTR)$buf, (LPDWORD)($lp)));
			}"
		end

	c_get_user_name_w (buf, lp: POINTER): INTEGER
			-- Get the current user name from the OS.
			-- Implementation is for Unicode (USC2) user names only
		external
			"C inline use %"Lmcons.h%""
		alias
		"{
			return ((EIF_INTEGER) GetUserNameW ((LPTSTR)$buf, (LPDWORD)($lp)));
			}"
		end

	c_user_name_maxlen: INTEGER
		external
			"C macro use <Lmcons.h>"
		alias
			"UNLEN"
		end

end -- class AEL_SPRT_PLATFORM_IMP
