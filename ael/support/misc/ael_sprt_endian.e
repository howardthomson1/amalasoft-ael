class AEL_SPRT_ENDIAN
-- Endian support functions

--|========================================================================
feature -- Status
--|========================================================================

	is_little_endian: BOOLEAN
			-- Is current platform a little endian one?
		do
			Result := c_endian_test = 1
		end

--|========================================================================
feature -- Externals
--|========================================================================

	c_endian_test: INTEGER
			-- Test for endian-ness of current platform
			-- If little-endian, result is 1
			-- If big-endian, result is 256
		external
			"C inline"
		alias
			"{
			unsigned char a8 [2] = { 1, 0 };
			return *((short *) a8);
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_short (s: NATURAL_16): NATURAL_16
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2;
			b1 = $s & 255;
			b2 = ($s >> 8) & 255;
			return (b1 << 8) + b2;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_long (i: NATURAL_32): NATURAL_32
		external
			"C inline"
		alias
			"{
			unsigned char b1, b2, b3, b4;
			b1 = $i & 255;
			b2 = ($i >> 8) & 255;
			b3 = ($i >> 16) & 255;
			b4 = ($i >> 24) & 255;
			return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
			}"
		end

	--|--------------------------------------------------------------

	c_endian_swapped_float (f: REAL_32): REAL_32
		external
			"C inline"
		alias
			"{
			union
			{
				float f;
				unsigned char b[4];
				} dat1, dat2;

				dat1.f = $f;
				dat2.b[0] = dat1.b[3];
				dat2.b[1] = dat1.b[2];
				dat2.b[2] = dat1.b[1];
				dat2.b[3] = dat1.b[0];
				return dat2.f;
			}"
		end

end -- class AEL_SPRT_ENDIAN
