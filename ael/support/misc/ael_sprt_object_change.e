note
	description: "{
Abstraction of a change to a known object.
Used in client notification system to provide more detail to change
notifications
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_OBJECT_CHANGE

inherit
	ANY
		redefine
			default_create
		end
	AEL_SPRT_OBJECT_CHANGE_TYPES
		undefine
			default_create
		end

create
	make_with_values

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_values (co: like changed_object; ct: INTEGER)
			-- Create Current for object 'co', with change type 'ct'
		do
			changed_object := co
			change_type := ct
			default_create
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	changed_object: ANY
			-- Object that is the subject of change

	change_type: INTEGER
			-- Nature of change

	before_data: detachable ANY
			-- Data, if any, describing the object state before the change

	after_data: detachable ANY
			-- Data, if any, describing the object state after
			-- (i.e. resulting from) the change

--|========================================================================
feature -- Status setting
--|========================================================================

	set_data (bd: like before_data; ad: like after_data)
			-- Set the before and after data to 'bd' and 'ad', respectively
		do
			before_data := bd
			after_data := ad
		end

	set_before_data (bd: like before_data)
			-- Set the before data to 'bd'
		do
			before_data := bd
		end

	set_after_data (ad: like after_data)
			-- Set the after data to 'ad'
		do
			after_data := ad
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Sep-2014
--|     Created original module (Eiffel 14.05)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_OBJECT_CHANGE
