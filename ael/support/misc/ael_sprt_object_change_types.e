note
	description: "{
Constants denoting types of object change
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_OBJECT_CHANGE_TYPES

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	-- In the abstract, changes can be tagged as "+", "-", or "!"
	-- meaning "added", "removed", or "modified"
	-- The change types defined here attempt to retain the simplicity
	-- (elegance?) of that abstract concept.

	K_change_type_extended: INTEGER = 1
			-- Object has been extended (item added)

	K_change_type_pruned: INTEGER = 2
			-- Object has been pruned (item removed)

	K_change_type_modified: INTEGER = 3
			-- Object itself has been modified, but NOT extended/pruned

	K_change_type_replaced: INTEGER = 4
			-- Object has replaced another (if any)

	K_change_type_item_modified: INTEGER = 5
			-- An item in Object been modified (assumes in-place)
			-- Applies to container-like objects

	K_change_type_item_replaced: INTEGER = 6
			-- An item in Object been replaced (assumes in-place) by another
			-- Applies to container-like objects

	K_change_type_item_swapped: INTEGER = 7
			-- An item in Object swapped places with another
			-- Applies to container-like objects

	K_change_type_item_moved: INTEGER = 8
			-- An item in Object has moved its position
			-- Applies to container-like objects

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 30-Sep-2014
--|     Created original module (Eiffel 14.05)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_OBJECT_CHANGE_TYPES
