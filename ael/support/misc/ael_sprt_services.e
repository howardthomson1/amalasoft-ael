--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| String manipulation and inspection routines
--|----------------------------------------------------------------------

class AEL_SPRT_SERVICES

inherit
	AEL_PRINTF

--|========================================================================
feature -- Shared services
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
			-- String-related routines
		once
			create Result
		end

	alr: AEL_SPRT_LIST_ROUTINES
			-- List-related routines
		once
			create Result
		end

	afr: AEL_SPRT_FILE_ROUTINES
			-- File-related routines
		once
			create Result
		end

end -- class AEL_SPRT_SERVICES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 24-Aug-2010
--|     Created original module (Eiffel 6.6)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries and other Amalasoft SPRT classes.
--|----------------------------------------------------------------------
