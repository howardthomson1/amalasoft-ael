note
	description: "{
Abstract notion of having a beginning and an end time
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2016 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_TIME_BOUNDED

inherit
	COMPARABLE

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================
	
	start_time: detachable AEL_UNIX_DATE_TIME
			-- Time at which Current began

	end_time: detachable AEL_UNIX_DATE_TIME
			--	 Time at which Current ended

	has_started: BOOLEAN
			-- Has Current begun?
		do
			Result := attached start_time as st and then st <= time_Now
		ensure
			true_in_past: Result implies
				attached start_time as st and then st <= time_now
			false_consistent: (not Result) implies
				(not attached start_time as st) or else time_now <= st
		end

	has_ended: BOOLEAN
			-- Has the end time been reached?
		do
			-- If no end time, then no, it has not ended, nor can it
			if attached end_time as et then
				Result := time_now >= et
			end
		ensure
			true_if_past: Result implies
				attached end_time as et and then et <= time_now
			false_consistent: (not Result) implies
				(not attached end_time as et) or else time_now <= et
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_start_time (v: like start_time)
			-- Set Current's start time to 'v'
		do
			start_time := v
		ensure
			is_set: start_time = v
		end

	begin
			-- Set Current's start time to present
		require
			not_started: not has_started
		do
			create start_time.make_now_utc
		ensure
			begun: has_started
		end

	--|--------------------------------------------------------------

	set_end_time (v: like end_time)
			-- Set the end_time value to 'v'
		require
			not_before_start: (attached v as tv and attached start_time as st)
				implies tv >= st
		do
			end_time := v
		ensure
			is_set: end_time = v
		end

	--|--------------------------------------------------------------

	terminate
			-- End Current's time immediately
		do
			end_time := time_now
		ensure
			ended: has_ended
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
			-- Less implies an earlier start time
			-- If start times are identical, then less implies earlier 
			-- end time
		local
			less_b, same_b: BOOLEAN
		do
			if attached start_time as st then
				if attached other.start_time as ost then
					less_b := st < ost
					same_b := st ~ ost
				else
					-- For consistency, a Void start is greater than a non-Void
					less_b := True
				end
			else
				if attached other.start_time as ost then
					-- other has non-void start, Current does not;
					-- Result will be False
				else
					-- Both have Void starts
					same_b := True
				end
			end
			if less_b then
				Result := True
			elseif same_b then
				-- Starts are same; Result depends solely on end times
				if attached end_time as et then
					if attached other.end_time as oet then
						Result := et < oet
					else
						Result := True -- non-Void < Void
					end
				else
					Result := attached other.end_time
				end
			end
		end

	--|--------------------------------------------------------------

	is_in_range (v: AEL_UNIX_DATE_TIME): BOOLEAN
			-- Does time 'v' fall between Current's start and end times?
		require
			exists: attached v
			has_start: attached start_time
			has_end: attached end_time
		do
			if attached start_time as st and attached end_time as et then
				Result := v >= st and v <= et
			end
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

	time_now: AEL_UNIX_DATE_TIME
		do
			create Result.make_now_utc
		end

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants and shared services
--|========================================================================

	aeldtr: AEL_DATE_TIME_ROUTINES
			-- Shared (shareable) routines supporting date/time functions
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Mar-2016
--|     Created original module (Eiffel 15.01, void-safe) from 
--|     existing minimal class
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_TIME_BOUNDED
