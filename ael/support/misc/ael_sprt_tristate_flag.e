--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Flag with 3 possible states:
--|    True unconditionally,
--|    False unconditionally and
--|    Deferred
--|----------------------------------------------------------------------

class AEL_SPRT_TRISTATE_FLAG

inherit
	ANY
		redefine
			default_create
		end

create
	default_create,
	make_true, make_false, make_deferred,
	make_with_state, make_from_label

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create
		do
		end

	make_true
			-- Create current in True state
		do
			default_create
			set_true
		ensure
			is_true: is_true
		end

	--|--------------------------------------------------------------

	make_false
			-- Create current in False state
		do
			default_create
			set_false
		ensure
			is_false: is_false
		end

	--|--------------------------------------------------------------

	make_deferred
			-- Create current in Deferred (default) state
		do
			default_create
		end

	--|--------------------------------------------------------------

	make_with_state (v: INTEGER)
			-- Create current in the given state
		require
			valid_label: is_valid_state (v)
		do
			default_create
			set_state (v)
		ensure
			has_state: state = v
		end

	--|--------------------------------------------------------------

	make_from_label (v: STRING)
		require
			valid_label: is_valid_state_label (v)
		do
			default_create
			set_state_from_label (v)
		ensure
			has_state: state_out.is_case_insensitive_equal (v)
		end

--|========================================================================
feature -- Status
--|========================================================================

	state: INTEGER
			-- Current flag state

	is_deferred: BOOLEAN
		-- Is the current state "deferred"?
		do
			Result := state = K_st_deferred
		end

	is_true: BOOLEAN
		-- Is the current state "unconditionally true"?
		do
			Result := state = K_st_true
		end

	is_false: BOOLEAN
		-- Is the current state "unconditionally false"?
		do
			Result := state = K_st_false
		end

	is_boolean (tf: BOOLEAN): BOOLEAN
		-- Is the current state the same as the Boolean value 'tf'?
		do
			Result := ((state = K_st_true) and tf) or
				((state = K_st_false) and not tf)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid: BOOLEAN
			-- Is Current valid? i.e. is it in a valid state?
		do
			Result := is_valid_state (state)
		end

	--|--------------------------------------------------------------

	is_invalid: BOOLEAN
			-- Is Current explicitly invalid?
			-- i.e. is it not in a valid state, and is its state defined
			-- as a known invalid state (versus some random value)
		do
			Result := state = K_st_invalid
		end

	--|--------------------------------------------------------------

	is_valid_state (v: INTEGER): BOOLEAN
			-- Is 'v' a valid state?
		do
			inspect v
			when K_st_deferred, K_st_true, K_st_false then
				Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	is_valid_state_label (v: STRING): BOOLEAN
			-- Is 'v' a valid state label?
		require
			exists: v /= Void
		local
			i: INTEGER
		do
			if not v.is_empty then
				i := index_of_state_label (v)
				Result := i >= K_st_minimum and i <= K_st_maximum
			end
		end

	has_state_label (v: STRING): BOOLEAN
			-- Does the state_labels array contain the equivalent of 'v'?
		require
			exists: v /= Void
		do
			Result := index_of_state_label (v) >= K_st_minimum
		end

	--|--------------------------------------------------------------

	index_of_state_label (v: STRING): INTEGER
			-- Index, within the state_labels array, of 'v', 0 if not found
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			ts: STRING
		do
			Result := -1
			ts := v.as_lower
			lim := state_labels.upper
			from i := state_labels.lower
			until i > lim or Result >= K_st_minimum

			loop
				if ts ~ state_labels.item (i).as_lower then
					Result := i
				else
					i := i + 1
				end
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	state_out: STRING
			-- String representation of Current
		do
			Result := state_labels.item (state)
		ensure
			exists: attached Result as tr and then not tr.is_empty
		end

	--|--------------------------------------------------------------

	label_to_state (v: STRING): INTEGER
			-- State corresponding to label 'v'
		require
			is_valid_state_label (v)
		local
			i: INTEGER
		do
			Result := K_st_invalid
			i := index_of_state_label (v)
			if i >= K_st_minimum and i <= K_st_maximum then
				Result := i
			end
		ensure
			valid_state: is_valid_state (Result)
		end

	state_labels_are_valid: BOOLEAN
			-- Are the state labels initialized and valid?
		do
			if attached state_labels as tsl and then tsl.count = 3 then
				Result := tsl.lower = 0
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_from_other (other: AEL_SPRT_TRISTATE_FLAG)
		require
			exists: other /= Void
		do
			state := other.state
		ensure
			is_set: state = other.state
		end

	set_true
			-- Set Current state to True
		do
			state := K_st_true
		ensure
			is_true: is_true
		end

	set_false
			-- Set Current state to False
		do
			state := K_st_false
		ensure
			is_false: is_false
		end

	set_deferred
			-- Set Current state to Deferred
		do
			state := K_st_deferred
		ensure
			is_deferred: is_deferred
		end

	set_boolean (tf: BOOLEAN)
			-- Set Current state to Boolean value 'tf'
		do
			if tf then
				state := K_st_true
			else
				state := K_st_false
			end
		end

	--|--------------------------------------------------------------

	set_state (v: INTEGER)
			-- Set Current state to 'v'
		require
			valid_state: is_valid_state (v)
		do
			state := v
		ensure
			set: state = v
			valid_state: is_valid_state (state)
		end

	--|--------------------------------------------------------------

	set_state_from_label (v: STRING)
			-- Set Current state from given label 'v'
		require
			valid_state: is_valid_state_label (v)
		do
			state := label_to_state (v)
		ensure
			set: state_out.is_case_insensitive_equal (v)
			valid_state: is_valid_state (state)
		end

	set_deferred_state_label (v: STRING)
		require
			exists: v /= Void and then not v.is_empty
		do
			private_deferred_state_label := v
			state_labels.put (v, K_st_deferred)
		ensure
			is_set: deferred_state_label ~ v
			is_recorded: state_labels.item (K_st_deferred) ~ v
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	state_labels: ARRAY [STRING]
			-- Labels corresponding to valid states
		once
			create Result.make_filled ("", K_st_minimum, K_st_maximum)
			Result.compare_objects
			Result.put (deferred_state_label, K_st_deferred)
			Result.put ((True).out, K_st_true)
			Result.put ((False).out, K_st_false)
		end

	--|--------------------------------------------------------------

	K_st_minimum: INTEGER = 0
	K_st_maximum: INTEGER = 2

	K_st_invalid: INTEGER = -1
			-- Invalid state

	K_st_deferred: INTEGER = 0
			-- Default state; neither true nor false

	K_st_true: INTEGER = 1
			-- True state

	K_st_false: INTEGER = 2
			-- False state

	--|--------------------------------------------------------------

	deferred_state_label: STRING
		do
			if attached private_deferred_state_label as pdl then
				Result := pdl
			else
				Result := dflt_deferred_state_label.twin
			end
		end

	private_deferred_state_label: detachable STRING

	dflt_deferred_state_label: STRING = "deferred"

	--|--------------------------------------------------------------
invariant
	known_state: is_valid or is_invalid
	valid_labels: state_labels_are_valid

end -- class AEL_SPRT_TRISTATE_FLAG

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 16-Sep-2010
--|     Replaced hard-wired "deferred" label with function, and broke 
--|     out 'default_create' to allow descendent redefinitions.
--|     Added explicit calls to default_create in make_true and make_false.
--|     Added set_boolean and is_boolean.
--|     Change true and false strings to use built-in BOOLEAN 'out's.
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using one of the creation 
--| routines, or using default create.
--| Set state using one of the set* routines
--| Query state using 'state' or one of the is_* query routines
--|----------------------------------------------------------------------
