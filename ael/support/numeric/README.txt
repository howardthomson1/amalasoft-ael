README.txt - Intro to the Amalasoft Numeric cluster

The Amalasoft Numeric cluster is a collection of Eiffel classes that
provide special numeric components and capabilities.
This cluster depends on the Eiffel base libraries (available in open source
or commercial form from Eiffel Software www.eiffel.com) and is, like other
Eiffel code, portable across platforms.  It also depends on the Amalasoft
Printf cluster.

There are 3 subdirectories in the cluster.  The 'legacy' subdirectory
contains a legacy version of AEL_SPRT_FRACTION.  The 'void-safe' subdirectory
contains a version of AEL_SPRT_FRACTION that is compatible with the new void
safe mechanisms and with the new standard syntax.  The 'test' contains
a test driver and 2 subdirectories with configuration files for legacy
and void-safe versions.  Define exclude rules for the subdirectories
that do not apply in your context.

     ------------------------------------------------------------

The cluster includes the following classes:

  AEL_SPRT_FRACTION
    A numeric value expressed as a whole and fractional parts

  AEL_SPRT_FRACTION_ROUTINES
    Routines for converting floating point values to fractions

  AEL_SPRT_USBLD_FRACTION
    A numeric value expressed as a whole and fractional parts
    specifically configured to be compatible with the building
    industry in the US

  AEL_SPRT_NUMERIC_ROUTINES
    Routines for specialized numeric processing, including rounding to
    and even multiple.  Inherits AEL_SPRT_NUMERIC_INTEGER_ROUTINES

  AEL_SPRT_NUMERIC_INTEGER_ROUTINES
    Routines for specialized numeric integer processing, including 
    rounding to even multiples, and byte-based queries for bit testing
    (highest/lowest bit set/unset and byte-wise complement)
