note
	description: "{
Simple encapsulation of a pair of x, y values
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2014 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_COORDINATE

create
	make,
	make_double,
	default_create

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (xp, yp: INTEGER)
			-- Create Current with row 'r' and column 'c'
		do
			x_double := xp
			y_double := yp
		end

	--|--------------------------------------------------------------

	make_double (xp, yp: DOUBLE)
		do
			x_double := xp
			y_double := yp
		end

--|========================================================================
feature -- Status
--|========================================================================

	x: INTEGER
		do
			if x_double > 0.0 then
				Result := (x_double + 0.5).truncated_to_integer
			else
				Result := (x_double - 0.5).truncated_to_integer
			end		end

	y: INTEGER
		do
			if y_double > 0.0 then
				Result := (y_double + 0.5).truncated_to_integer
			else
				Result := (y_double - 0.5).truncated_to_integer
			end
		end

	x_double: DOUBLE
	y_double: DOUBLE

--|========================================================================
feature -- Status setting
--|========================================================================

	set_x (v: INTEGER)
		do
			x_double := v
		end

	set_y (v: INTEGER)
		do
			y_double := v
		end

	set_x_double (v: DOUBLE)
		do
			x_double := v
		end

	set_y_double (v: DOUBLE)
		do
			y_double := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- External representation and serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation (private)
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Aug-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_COORDINATE
