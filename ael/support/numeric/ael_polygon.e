note
	description: "{
Values describing a polygon, by its points' positions
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2020 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_POLYGON

inherit
	ANY
		redefine
			default_create
		end

create
	make,
	default_create

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (pa: ARRAY [AEL_COORDINATE])
			-- Create Current with point positions 'pa'
			-- 'pa' must be non-empty; if create empty is desired,
			-- use 'default_create' instead
		require
			not_empty: not pa.is_empty
			one_based: ps.lower = 1
		do
			default_create
			points := pa
		end

	default_create
		do
			create points.make_empty
		end

--|========================================================================
feature -- Status
--|========================================================================

	points: ARRAY [AEL_COORDINATE]

	point (i: INTEGER): detachable [AEL_COORDINATE]
			-- i_th point, if any
		do
			if is_valid_index (i) then
				Result := points.item (i)
			end
		end

	count: INTEGER
		do
			Result := points.count
		end

	bounding_box: AEL_RECTANGLE
		do
			create Result.make (top, left, width, height)
		end

	--|--------------------------------------------------------------

	left: INTEGER
		local
			i, lim, tx: INTEGER
		do
			Result := i.max_value
			lim := count
			from i := 1
			until i > lim
			loop
				tx := points.item (i).x
				Result := Result.min (tx)
				i := i + 1
			end
			if Result = i.max_value then
				Result := 0
			end
		end

	right: INTEGER
		local
			i, lim, tx: INTEGER
		do
			Result := i.min_value
			lim := count
			from i := 1
			until i > lim
			loop
				tx := points.item (i).x
				Result := Result.max (tx)
				i := i + 1
			end
			if Result = i.min_value then
				Result := 0
			end
		end

	top: INTEGER
		local
			i, lim, tx: INTEGER
		do
			Result := i.max_value
			lim := count
			from i := 1
			until i > lim
			loop
				ty := points.item (i).y
				Result := Result.min (ty)
				i := i + 1
			end
			if Result = i.max_value then
				Result := 0
			end
		end

	bottom: INTEGER
		local
			i, lim, tx: INTEGER
		do
			Result := i.min_value
			lim := count
			from i := 1
			until i > lim
			loop
				ty := points.item (i).y
				Result := Result.max (ty)
				i := i + 1
			end
			if Result = i.min_value then
				Result := 0
			end
		end

	--|--------------------------------------------------------------

	width: INTEGER
		do
			Result := right - left
		end

	height: INTEGER
		do
			Result := bottom - top
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_point (v: AEL_COORDINATE; idx: INTEGER)
			-- Set value of point at index 'idx'
			-- Replace, not extend
		require
			valid_index: is_valid_index (idx)
		do
			points.put (v, idx)
		ensure
			is_set: point (idx) ~ v
		end

	extend (v: AEL_COORDINATE)
			-- Add new value to points at index 'idx'
		do
			points.force (v, count + 1)
		ensure
			is_set: point (count) ~ v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	shift_right (v: INTEGER)
			-- Shift ALL points right by 'v'
			-- Increase each point's X value by 'v'
		local
			i, lim: INTEGER
			p: AEL_COORDINATE
		do
			lim := count
			from i := 1
			until i > lim
			loop
				p := points.item (i)
				p.set_x (p.x + v)
				i := i + 1
			end
		end

	shift_left (v: INTEGER)
			-- Shift ALL points left by 'v'
			-- DEcrease each point's X value by 'v'
		do
			shift_right (-v)
		end

	shift_down (v: INTEGER)
			-- Shift ALL points down by 'v'
			-- Increase each point's Y value by 'v'
		local
			i, lim: INTEGER
			p: AEL_COORDINATE
		do
			lim := count
			from i := 1
			until i > lim
			loop
				p := points.item (i)
				p.set_y (p.y + v)
				i := i + 1
			end
		end

	shift_up (v: INTEGER)
			-- Shift ALL points up by 'v'
			-- DEcrease each point's Y value by 'v'
		do
			shift_down (-v)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	--RFO is_equal (other: like Current): BOOLEAN
	--RFO 		-- Is other equivalent to Current?
	--RFO 	do
	--RFO 		Result := x = other.x and y = other.y
	--RFO 			and width = other.width and height = other.height
	--RFO 	end

	--|--------------------------------------------------------------

	has_point (xp, yp: INTEGER): BOOLEAN
			-- Does position 'xp','yp' FALL WITHIN Current?
		do
			Result := xp >= left and xp <= right
				and yp >= top and yp <= bottom
		end

	contains (other: like Current): BOOLEAN
			-- Does Current contain 'other' entirely?
		do
			Result := x <= other.x and y <= other.y
				and other.bottom <= bottom and other.right <= right
		end

	intersects (other: like Current): BOOLEAN
			-- Does 'other' at least partially overlap Current?
		do
			if area > 0 then
				Result := other.area > 0 and then
					not (
						left >= other.right or right <= other.left or
						top >= other.bottom or bottom <= other.top)
			end
		end

	intersection (other: like Current): like Current
			-- Intersection of 'other' with Current
			-- If there is no intersection Result has default values and 
			-- no area.
		local
			tmax, bmin, lmax, rmin: INTEGER
		do
			create Result
			if intersects (other) then
				tmax := top.max (other.top)
				bmin := bottom.min (other.bottom)
				lmax := left.max (other.left)
				rmin := right.min (other.right)
				if tmax < 0 then
					Result.set_top (tmax)
					Result.set_bottom (bmin)
				else
					Result.set_bottom (bmin)
					Result.set_top (tmax)
				end

				if lmax < 0 then
					Result.set_left (lmax)
					Result.set_right (rmin)
				else
					Result.set_right (rmin)
					Result.set_left (lmax)
				end
			end
		ensure
			intersection_valid: (Result.width /= 0
				or else Result.height /= 0) implies Result.intersects (other)
			no_intersection_valid: (Result.width = 0
				or else Result.height = 0) implies not Result.intersects (other)
		end

--|========================================================================
feature -- External representation and serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_index (i: INTEGER): BOOLEAN
			-- Is 'i' a valid points index?
		do
			Result := i > 0 and i <= count
		end

--|========================================================================
feature {NONE} -- Implementation (private)
--|========================================================================

	point_falls within (x, y: DOUBLE): BOOLEAN
			-- Is (`x', `y') contained in Current? with 'points'?
			-- Based on code by Hanpeter van Vliet.
		local
			hits, n, i, nb, j, base, lim: INTEGER
			y_save, rx, ry, dx, dy, min, max: DOUBLE
			s, val: DOUBLE
			pa, pb: AEL_COORDINATE
		do
			if count <= 1 then
				Result := False
			elseif count = 2 then
				pa := points.item (1)
				pb := points.item (2)
				Result := point_on_segment (
					x, y, pa.x_precise, pa.y_precise, pb.x_precise, pb.y_precise, 6)
			else
				if all_on_horizontal_line (points) then
					min := points.item (1).x_precise
					max := min
					lim := count
					from i := 2
					until i > count
					loop
						val := points.item (i).x_precise
						min := min.min (val)
						max := max.max (val)
						i := i + 1
					end
					tp := points.item (1)
					Result := point_on_segment (
						x, y, min, tp.y_precise, max, tp.y_precise, 6)
				else
					if all_on_vertical_line (points) then
						min := points.item (1).y_precise
						max := min
						lim := count
						from i := 2
						until i > lim
						loop
							val := points.item (i).y_precise
							min := min.min (val)
							max := max.max (val)
							i := i + 1
						end
						tp := points.item (1)
						Result := point_on_segment (
							x, y, tp.x_precise, min, tp.x_precise, max, 6)
					else
						base := 0
						-- Find a vertex that is not on the halfline.
						from i := 1
						until
							not (i <= count and then points.item (i + base).y = y)
						loop
							i := i + 1
						end
						-- Walk edges of the polygon.
						lim := count
						from n := 1
						until n > lim
						loop
							j := (i + 1) \\ points.count
							dx := points.item (j + base).x_precise -
								points.item (i + base).x_precise
							dy := points.item (j + base).y_precise -
								points.item (i + base).y_precise
			
							-- Ignore horizontal edges completely.
							if dy /= 0 then
								-- Check to see if the edge intersects the
								-- horizontal halfline through (x, y).
								rx := x - points.item (i + base).x_precise
								ry := y - points.item (i + base).y_precise
			
								-- Deal with edges starting or ending the halfline.
								if points.item (j + base).y_precise = y and then
									points.item (j + base).x_precise >= x
								 then
									 y_save := points.item (i + base).y_precise
								end
								if points.item (i + base).y_precise = y and then
									points.item (i + base).x_precise >= x
								 then
									if (y_save > y) /=
										(points.item (j + base).y_precise > y)
									 then
										 hits := hits - 1
									end
								end
								-- Tally intersections with halfline.
								s := ry / dy
								if s >= 0.0 and then s <= 1.0 and then (s*dx) >= rx then
									hits := hits + 1
								end
							end
							i := j
							n := n + 1
						end
						-- Inside if number of intersections odd.
						Result := (hits \\ 2) /= 0
					end
				end
			end
		end

	--|--------------------------------------------------------------

	point_on_segment (x, y, x1, y1, x2, y2, width: DOUBLE): BOOLEAN
			-- Is (`x', `y') on segment [(`x2', `y2'), (`x1', `y1')] with `width'?
		local
			half_dx, half_dy, dpx, dpy: DOUBLE
		do
			if x1 = x2 and y1 = y2 then
				Result := distance (x, y, y1, x1) <= (width / 2)
			elseif (x1 - x2).abs < 3 then
				Result := between (y, y1, y2) and (x - x1).abs <= width / 2
			elseif (y1 - y2).abs < 3 then
				Result := between (x, x1, x2) and (y - y1).abs <= width / 2
			else
				half_dx := (x2 - x1) / 2
				half_dy := (y2 - y1) / 2
				dpx := x1 + half_dx - x
				dpy := y1 + half_dy - y
				Result := (dpx.abs <= half_dx.abs) and then
					(dpy.abs <= half_dy.abs) and then
					point_on_line (x, y, x1, y1, x2, y2, width)
			end
		end

	--|--------------------------------------------------------------

	all_on_horizontal_line: BOOLEAN
			-- Are all `points' an a vertical line?
			-- That is all y positions are equal.
		local
			i: INTEGER
			p, q: INTEGER
		do
			Result := True
			if count > 1 then
				from
					i := 1
					p := points.item (1).y
				until
					i > count or not Result
				loop
					q := points.item (i).y
					Result := p = q
					i := i + 1
					p := q
				end
			end
		end
		
	--|--------------------------------------------------------------

	modulo (a, b: DOUBLE): DOUBLE
			-- `a' modulo `b'.
			--| Should be in DOUBLE_REF.
		require
			divisible: b /= 0.0
		do
			if a >= 0.0 and a < b then
				Result := a
			elseif a >= 0.0 then
				Result := modulo (a - b, b)
			else
				Result := modulo (a + b, b)
			end
		ensure
			in_interval: Result >= 0.0 and Result < b
		end

	between (n, a, b: DOUBLE): BOOLEAN
			-- Is `n' a value between `a' and `b'?
		do
			Result := n >= a.min (b) and then n <= a.max (b)
		end		

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant
	one_based: (not points.is_empty) implies points.lower = 1

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Aug-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_POLYGON
