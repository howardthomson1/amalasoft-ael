note
	description: "{
Values describing a rectangle and its relative position
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2020 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_RECTANGLE

create
	make,
	default_create

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (xp, yp, w, h: INTEGER)
			-- Create Current with relative position (`xp',`yp'),
			-- width 'w' and height 'h'
		require
			width_positive: w >= 0
			height_positive: h >= 0
		do
			x := xp
			y := yp
			width := w
			height := h
		end

--|========================================================================
feature -- Status
--|========================================================================

	x: INTEGER
	left: INTEGER
		do
			Result := x
		end

	y: INTEGER
	top: INTEGER
		do
			Result := y
		end

	width: INTEGER
	height: INTEGER

	bottom: INTEGER
		do
			Result := top + height
		end

	right: INTEGER
		do
			Result := left + width
		end

	area: INTEGER
		do
			Result := width * height
		end

	--|--------------------------------------------------------------

	top_left: AEL_COORDINATE
			-- Top-left corner of Current
		do
			create Result.make (left, top)
		ensure
			valid: Result.x = x and then Result.y = y
		end

	top_right: AEL_COORDINATE
			-- Top-right corner of Current
		do
			create Result.make (right, top)
		ensure
			valid: Result.x = x + width and then Result.y = y
		end

	bottom_left: AEL_COORDINATE
			-- Bottom-left corner of Current
		do
			create Result.make (left, bottom)
		ensure
			valid: Result.x = x and then Result.y = y + height
		end

	bottom_right: AEL_COORDINATE
			-- Lower-right corner of `Current'.
		do
			create Result.make (right, bottom)
		ensure
			valid: Result.x = x + width and then Result.y = y + height
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_x (v: INTEGER)
		do
			x := v
		ensure
			is_set: x = v
		end

	set_y (v: INTEGER)
		do
			y := v
		ensure
			is_set: y = v
		end

	set_width (v: INTEGER)
		require
			not_negative: v >= 0
		do
			width := v
		ensure
			is_set: width = v
		end

	set_height (v: INTEGER)
		require
			not_negative: v >= 0
		do
			height := v
		ensure
			is_set: height = v
		end

	resize (w, h: INTEGER)
			-- Resize Current to width 'w' and height 'h'
			-- Do not change origin
		do
			width := w
			height := h
		ensure
			width_set: width = w
			height_set: height = h
			origin_unchanged: x = old x and y = old y
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	expand_to_include_point (v: AEL_COORDINATE)
			-- If necessary, adjust dimensions and origin of Current
			-- to ensure that point 'v' is included within Current's
			-- permiter
		do
			expand_to_include_xy (v.x, v.y)
		ensure
			included: has_point (v.x, v.y)
		end

	expand_to_include_xy (xp, yp: INTEGER)
			-- If necessary, adjust dimensions and origin of Current
			-- to ensure that point 'x','y' is included within Current's
			-- permiter
		do
			set_top (top.min (yp))
			set_left (left.min (xp))
			set_bottom (bottom.max (yp))
			set_right (right.max (xp))
		ensure
			included: has_point (xp, yp)
		end

	expand_to_include_other (other: like Current)
			-- If necessary, adjust dimensions and origin of Current
			-- to ensure that 'other' is fully contained within
			-- Current's permiter
		do
			set_top (top.min (other.top))
			set_left (left.min (other.left))
			set_bottom (bottom.max (other.bottom))
			set_right (right.max (other.right))
		ensure
			included: contains (other)
		end

	--|--------------------------------------------------------------

	expand_left (v: INTEGER)
			-- Expand width of Current by 'v' units along left edge
		do
			x := x - v
			width := width + v
		end

	expand_right (v: INTEGER)
			-- Expand width of Current by 'v' units along right edge
		do
			width := width + v
		end

	expand_top (v: INTEGER)
			-- Expand height of Current by 'v' units along top edge
		do
			y := y - v
			height := height + v
		end

	expand_bottom (v: INTEGER)
			-- Expand height of Current by 'v' units along bottom edge
		do
			height := height + v
		end

	--|--------------------------------------------------------------

	set_left (v: INTEGER)
			-- Set left edge to 'v', adjusting width accordingly
			-- Example:
			--    old x = 5, old width = 10, v = 2
			--    'v' "increases" x by xd=v-x=-3
			--    width "decreases" by xd 10-(-3)=13
		local
			xd: INTEGER
		do
			xd := v - x
			width := width - xd
			x := v
		ensure
			is_set: left = v
			right_unchanged: right = old right
			valid_width: width = right - left
		end

	set_right (v: INTEGER)
			-- Set right edge to 'v', adjusting width accordingly
			-- New width is new right minus left
		require
			in_bounds: v - x >= 0
		do
			width := v - x
		ensure
			left_unchanged: left = old left
			valid_width: width = right - left
		end

	set_top (v: INTEGER)
			-- Set top edge to 'v', adjusting height accordingly
			-- Example:
			--    old y = 5, old height = 10, v = 2
			--    'v' "increases" y by yd=v-y=-3
			--    height "decreases" by yd 10-(-3)=13
		require
			in_bounds: v <= bottom
		local
			yd: INTEGER
		do
			yd := v - y
			height := height - yd
			y := v
		ensure
			is_set: top = v
			bottom_unchanged: bottom = old bottom
			valid_height: height = bottom - top
		end

	set_bottom (v: INTEGER)
			-- Set bottom edge to 'v', adjusting height accordingly
			-- New height is new bottom minus top
		require
			in_bounds: v - y >= 0
		do
			height := v - y
		ensure
			top_unchanged: top = old top
			valid_height: height = bottom - top
		end

	--|--------------------------------------------------------------

	move (xp, yp: INTEGER)
			-- Move Current to new position at 'xp','yp'
			-- Do not change size
		do
			x := xp
			y := yp
		ensure
			is_set: x = xp and y = yp
			same_size: width = old width and height = old height
		end

	reconfigure (xp, yp, w, h: INTEGER)
			-- Reconfigure Current to origin at 'xp,'yp' with width 'w' 
			-- and height 'h'
		do
			x := xp
			y := yp
			width := w
			height := h
		ensure
			origin_set: x = xp and y = yp
			size_set: width = w and height = h
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	--RFO is_equal (other: like Current): BOOLEAN
	--RFO 		-- Is other equivalent to Current?
	--RFO 	do
	--RFO 		Result := x = other.x and y = other.y
	--RFO 			and width = other.width and height = other.height
	--RFO 	end

	--|--------------------------------------------------------------

	has_point (xp, yp: INTEGER): BOOLEAN
			-- Does position 'xp','yp' fall within Current?
		do
			Result := xp >= left and xp <= right
				and yp >= top and yp <= bottom
		end

	contains (other: like Current): BOOLEAN
			-- Does Current contain 'other' entirely?
		do
			Result := x <= other.x and y <= other.y
				and other.bottom <= bottom and other.right <= right
		end

	intersects (other: like Current): BOOLEAN
			-- Does 'other' at least partially overlap Current?
		do
			if area > 0 then
				Result := other.area > 0 and then
					not (
						left >= other.right or right <= other.left or
						top >= other.bottom or bottom <= other.top)
			end
		end

	intersection (other: like Current): like Current
			-- Intersection of 'other' with Current
			-- If there is no intersection Result has default values and 
			-- no area.
		local
			tmax, bmin, lmax, rmin: INTEGER
		do
			create Result
			if intersects (other) then
				tmax := top.max (other.top)
				bmin := bottom.min (other.bottom)
				lmax := left.max (other.left)
				rmin := right.min (other.right)
				if tmax < 0 then
					Result.set_top (tmax)
					Result.set_bottom (bmin)
				else
					Result.set_bottom (bmin)
					Result.set_top (tmax)
				end

				if lmax < 0 then
					Result.set_left (lmax)
					Result.set_right (rmin)
				else
					Result.set_right (rmin)
					Result.set_left (lmax)
				end
			end
		ensure
			intersection_valid: (Result.width /= 0
				or else Result.height /= 0) implies Result.intersects (other)
			no_intersection_valid: (Result.width = 0
				or else Result.height = 0) implies not Result.intersects (other)
		end

--|========================================================================
feature -- External representation and serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation (private)
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant
	valid_width: width >= 0
	valid_height: height >= 0

--|----------------------------------------------------------------------
--| History
--|
--| 001 14-Aug-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_RECTANGLE
