note
	description: "{
Simple encapsulation of a pair of row and column values
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2014 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_ROW_COL

inherit
	AEL_VALUE_PAIR
		redefine
			out, is_equal
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (r, c: INTEGER)
			-- Create Current with row 'r' and column 'c'
		do
			value_type := K_value_type_rc
			make_as_integers (r, c)
		end

--|========================================================================
feature -- Status
--|========================================================================

	col: INTEGER
		do
			Result := ivalue_2
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_col (v: INTEGER)
		do
			set_column (v)
		ensure
			is_set: column = v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_equal (other: like Current): BOOLEAN
			-- Is other equivalent to Current?
		do
			Result := ivalue_1 = other.ivalue_1 and ivalue_2 = other.ivalue_2
		end

--|========================================================================
feature -- External representation and serialization support
--|========================================================================

	out: STRING
		do
			Result := aprintf (pf_format_rc, << ivalue_1, ivalue_2 >>)
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation (private)
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 10-Jun-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_ROW_COL
