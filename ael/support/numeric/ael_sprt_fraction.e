--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A numeric value expressed as a whole and fractional parts
--|----------------------------------------------------------------------

class AEL_SPRT_FRACTION

inherit
	NUMERIC
		undefine
			is_equal, out
		end
	COMPARABLE
		undefine
			out
		redefine
			is_equal
		end
	AEL_SPRT_FRACTION_ROUTINES
		undefine
			is_equal, out
		redefine
			out
		end
	AEL_PRINTF
		undefine
			is_equal, default_create, out
		end

create
	default_create, make_with_components, make_with_value,
	make_with_value_and_denominators

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_with_components (wp, fn, fd: INTEGER)
			-- Create a fraction from the given component parts
		require
			valid_numerator: is_valid_numerator(fn)
			valid_denominator: is_valid_denominator(fd)
		do
			whole_part := wp
			numerator := fn
			denominator := fd
			reduce
		end

	--|--------------------------------------------------------------

	make_with_value (v: DOUBLE)
			-- Create a fraction from the given value, using the default
			-- sequence of permissible denominators and default precision
			-- (3 places) and fuzziness (2% at specified precision)
		local
			tf: AEL_SPRT_FRACTION
		do
			original_value := v
			tf := float_to_fraction (v)
			make_with_components (tf.whole_part, tf.numerator, tf.denominator)
			has_original_value := True
		end

	--|--------------------------------------------------------------

	make_with_value_and_denominators (v: DOUBLE; dl: ARRAY [INTEGER])
			-- Create a fraction from the given value, using the given
			-- sequence of permissible denominators and default precision
			-- (3 places) and fuzziness (2% at specified precision)
		require
			valid_denominators: denominators_are_valid(dl)
		local
			f3: like fuzzy_float_to_fractional_parts
		do
			private_denominators := dl
			original_value := v
			f3 :=  fuzzy_float_to_fractional_parts (v, 0, 0, dl)
			make_with_components (
				f3.integer_item(1),
				f3.integer_item(2),
				f3.integer_item(3))
			has_original_value := True
		end

--|========================================================================
feature -- Components
--|========================================================================

	whole_part: INTEGER
	numerator: INTEGER
	denominator: INTEGER

	--|--------------------------------------------------------------

	original_value: DOUBLE
			-- Original value given to convert to fraction

	has_original_value: BOOLEAN
			-- Has an original value been defined?

	--|--------------------------------------------------------------

	deviation: DOUBLE
			-- The difference between the original value and the fractional
			-- equivalent.  If a valid fraction, then the Result is always
			-- non-negative.  A negative result denotes and invalid fraction.
		do
			if has_original_value then
				if not is_valid then
					Result := -1
				else
					Result := (original_value -
						(whole_part + (numerator / denominator))).abs
				end
			end
		ensure
			valid_is_non_negative: is_valid implies Result >= 0.0
		end

	--|--------------------------------------------------------------

	denominators: like default_denominators
		do
			if attached private_denominators as t then
				Result := t
			else
				Result := default_denominators
			end
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_whole_part (v: INTEGER)
			-- Set the whole part value for this fraction
		do
			has_original_value := False
			whole_part := v
		end

	set_numerator (v: INTEGER)
			-- Set the numerator value for this fraction
		require
			is_valid_numerator (v)
		do
			has_original_value := False
			numerator := v
		end

	--|--------------------------------------------------------------

	set_denominator (v: INTEGER)
			-- Set the denominator value for this fraction
		require
			is_valid_denominator (v)
		do
			has_original_value := False
			denominator := v
		end

	--|--------------------------------------------------------------

	reduce
			-- Use the list of denominators to reduce the numerator and 
			-- denominator such that they have no common factors
			-- (e.g. 32/64->1/2).
			-- If the denominator is 1, combine the numerator with the whole part.
		local
			dl: like denominators
			i, lim, td: INTEGER
		do
			dl := denominators
			lim := dl.count
			from i := lim
			until i = 0
			loop
				td := dl.item (i)
				if (denominator > td) and ((numerator \\ td) = 0) and
					((denominator \\ td) = 0)
				 then
					 numerator := numerator // td
					 denominator := denominator // td
				end
				i := i - 1
			end
			if denominator = 1 then
				whole_part := whole_part + numerator
				numerator := 0
			end
		end

	--|========================================================================
feature -- Numeric values
	--|========================================================================

	one: like Current
			-- Neutral element for "*" and "/"
		do
			create Result.make_with_components (1, 0, 1)
		end

	zero: like Current
			-- Neutral element for "+" and "-"
		do
			create Result.make_with_components (0, 0, 1)
		end

	--|========================================================================
feature -- Numeric status
	--|========================================================================

	divisible (other: like Current): BOOLEAN
			-- May current object be divided by `other'?
		do
			Result := is_valid and other.is_valid
		end

	exponentiable (other: NUMERIC): BOOLEAN
			-- May current object be elevated to the power `other'?
		do
		end

	--|--------------------------------------------------------------

	is_less alias "<"  (other: like Current): BOOLEAN
			-- Is current object less than other?
		do
			Result := to_double < other.to_double
		end

	is_equal (other: like Current): BOOLEAN
		do
			Result := to_double = other.to_double
		end

	--|========================================================================
feature -- Numeric operations
	--|========================================================================

	increase_by (v: DOUBLE)
			-- Add the given value to the value of this fraction
		do
			make_with_value (to_double + v)
		end

	multiply_by (v: DOUBLE)
			-- Multiply the current value of this fraction by the given value
		do
			make_with_value (to_double * v)
		end

	--|--------------------------------------------------------------

	plus alias "+" (other: like Current): like Current
			-- Sum with `other' (commutative).
		do
			if denominator = other.denominator then
				create Result.make_with_value_and_denominators(
					whole_part + other.whole_part +
					((numerator + other.numerator) /
					denominator),
					denominators)
			else
				create Result.make_with_value_and_denominators(
					to_double + other.to_double,
					denominators)
			end
		end

	--|--------------------------------------------------------------

	minus alias "-" (other: like Current): like Current
			-- Result of subtracting `other'
		do
			if denominator = other.denominator then
				create Result.make_with_value_and_denominators(
					whole_part - other.whole_part +
					((numerator - other.numerator) /
					denominator),
					denominators)
			else
				create Result.make_with_value_and_denominators(
					to_double - other.to_double,
					denominators)
			end
		end

	--|--------------------------------------------------------------

	product alias "*" (other: like Current): like Current
			-- Product by `other'
		do
			create Result.make_with_value_and_denominators(
				to_double * other.to_double,
				denominators)
		end

	--|--------------------------------------------------------------

	quotient alias "/" (other: like Current): like Current
			-- Division by `other'
		do
			create Result.make_with_value_and_denominators(
				to_double / other.to_double,
				denominators)
		end

	--|--------------------------------------------------------------

	identity alias "+": like Current
			-- Unary plus
		do
			Result := Current
		end

	opposite alias "-": like Current
			-- Unary minus
		do
			Result := zero - Current
		end

	--|========================================================================
feature -- Validation
	--|========================================================================

	is_valid: BOOLEAN
			-- Is this a valid fraction?
		do
			Result := is_valid_denominator (denominator) and
				is_valid_numerator (numerator) 
		end

	--|========================================================================
feature -- External representation
	--|========================================================================

	out: STRING
			-- String representation of this fraction, with the whole part
			-- folded into the numerator (e.g. 22/7 instead of 3 1/7)
		do
			if denominator = 1 then
				Result := (whole_part + numerator).out
			else
				Result := aprintf ("%%d/%%d",
				    << (whole_part * denominator) + numerator, denominator >>)
			end
		end

	--|--------------------------------------------------------------

	out_long: STRING
			-- String representation of this fraction
			-- Fractions are normally handled with typographic mechanisms, but
			-- such is not the option here.  The output format is:
			--  <whole_part> <numerator>"/"<denominator>
		do
			if denominator = 1 then
				Result := (whole_part + numerator).out
			else
				Result := aprintf ("%%d %%d/%%d",
				                   << whole_part, numerator, denominator >>)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	to_double: DOUBLE
			-- The DOUBLE equivalent of the component parts (not the original
			-- value)
			-- Fraction MUST be valid to use this conversion function!!
		require
			valid_fraction: is_valid
		do
			if has_original_value then
				Result := original_value
			else
				Result := whole_part + (numerator / denominator)
			end
		end

	--|--------------------------------------------------------------

	to_integer: INTEGER
			-- The INTEGER equivalent of the component parts (not the original
			-- value), rounded
			-- Fraction MUST be valid to use this conversion function!!
		require
			valid_fraction: is_valid
		do
			Result := to_double.rounded
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_denominators: detachable like default_denominators

end -- class AEL_SPRT_FRACTION

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Objects of this class are typically created with values, as 
--| defined in the create clause above.  The most straightforward 
--| method is to use the 'make_with_value' procedure (this uses 
--| default parameters).
--| If the default_create is used, then the component values must be 
--| set (correctly), else the object will be invalid.
--| Some objects of this class can be considered invalid, if they have 
--| a zero or negative denominator, or a negative numerator.  Valid 
--| negative fractions have a negative whole part.
--| When objects are created with an original value, that value is 
--| kept and can be used later to calculate the deviation of the 
--| fractional value from the original.
--| Fractions can be added, subtacted, multiplied and divided with 
--| other fractions.  They can also be compared.
--|----------------------------------------------------------------------
