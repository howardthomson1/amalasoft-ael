--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| INTEGER-only routines for specialized numeric processing
--|----------------------------------------------------------------------

class AEL_SPRT_NUMERIC_INTEGER_ROUTINES

create
	default_create

--|========================================================================
feature -- Conversion
--|========================================================================

	rounded_to_next_even_mod (v, m: INTEGER; zok: BOOLEAN): INTEGER
			-- The given number 'v', rounded toward higher magnitude,
			-- to an even multiple of 'm'
			-- Positive numbers are rounded up
			-- Negative numbers are rounded down
			-- If zero is acceptable (the 'zok' flag = True) then
			-- a value of zero will yield a result of zero.  If not,
			-- then a value of zero will yield a result of 'm'
		require
			valid_mod: m > 1
		do
			Result := rounded_to_next_64_even_mod (v, m, zok).to_integer_32
		ensure
			sign_preserved: (v >= 0) = (Result >= 0)
			positive_on_request: (v = 0 and not zok) implies Result = m
			at_least: Result.abs >= v.abs
			even_mod: (Result \\ m) = 0
		end

	--|--------------------------------------------------------------

	rounded_to_next_64_even_mod (v, m: INTEGER_64; zok: BOOLEAN): INTEGER_64
			-- The given number 'v', rounded toward higher magnitude,
			-- to an even multiple of 'm'
			-- Positive numbers are rounded up
			-- Negative numbers are rounded down
			-- If zero is acceptable (the 'zok' flag = True) then
			-- a value of zero will yield a result of zero.  If not,
			-- then a value of zero will yield a result of 'm'
		require
			valid_mod: m > 1
		local
			tv: INTEGER_64
			is_neg: BOOLEAN
		do
			if v = 0 then
				if not zok then
					Result := m
				end
			elseif v \\ m = 0 then
				Result := v
			else
				if v < 0 then
					tv := -v
					is_neg := True
				else
					tv := v
				end
				Result := tv + (m - (tv \\ m))
				if is_neg then
					Result := -Result
				end
			end
		ensure
			sign_preserved: (v >= 0) = (Result >= 0)
			positive_on_request: (v = 0 and not zok) implies Result = m
			at_least: Result.abs >= v.abs
			even_mod: (Result \\ m) = 0
		end

	--|--------------------------------------------------------------

	even_8_multiple (v: INTEGER): INTEGER
			-- Nearest value at or greater than 'v' that is an even
			-- multiple of 8
		require
			non_negative: v >= 0
		do
			Result := c_even_8_multiple (v)
		end

	--|--------------------------------------------------------------

	even_10_multiple (v: INTEGER): INTEGER
			-- Nearest value at or greater than 'v' that is an even
			-- multiple of 10
		require
			non_negative: v >= 0
		do
			Result := c_even_10_multiple (v)
		end

	--|--------------------------------------------------------------

	even_8_multiple_64 (v: INTEGER_64): INTEGER_64
			-- Nearest 64 bit value at or greater than 'v' that is an even
			-- multiple of 8
		require
			non_negative: v >= 0
		do
			Result := rounded_to_next_64_even_mod (v, 8, False)
		end

	--|--------------------------------------------------------------

	even_10_multiple_64 (v: INTEGER_64): INTEGER_64
			-- Nearest 64 bit value at or greater than 'v' that is an even
			-- multiple of 10
		require
			non_negative: v >= 0
		do
			Result := rounded_to_next_64_even_mod (v, 10, False)
		end

	--|--------------------------------------------------------------

	even_512_multiple (v: INTEGER): INTEGER
			-- Nearest value at or greater than 'v' that is an even
			-- multiple of 512
		require
			non_negative: v >= 0
		do
			Result := c_even_512_multiple (v)
		end

--|========================================================================
feature -- Byte operations
--|========================================================================

	highest_bit_set_in_byte (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the most significant bit
			-- that is set in the given byte
			-- If none is set, then -1
		do
			if v = 0 then
				Result := -1
			else
				Result := hi_bits_by_value.item (v)
			end
		end

	highest_bit_unset_in_byte (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the most significant bit
			-- that is UNSET in the given byte
			-- If none is UNSET (all are set), then -1
		do
			Result := highest_bit_set_in_byte (byte_complement (v))
		end

	--|--------------------------------------------------------------

	lowest_bit_set_in_byte (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7), of the least significant bit
			-- that is set in the given byte
			-- If none is set, then -1
			--
			-- All odd numbers have 1 as lowest bit set
		local
			i: INTEGER
			m: NATURAL_8
		do
			Result := -1
			if v /= 0 then
				from
					i := 0
					m := 1
				until i > 7 or Result >= 0
				loop
					if v & m /= 0 then
						Result := i
					else
						i := i + 1
						m := m |<< 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	lowest_bit_unset_in_byte (v: NATURAL_8): INTEGER
			-- The number (from 0 to 7, of the least significant bit
			-- that is UNSET in the given byte
			-- If none is UNSET (all are set), then -1
		local
			i: INTEGER
			m: NATURAL_8
		do
			if v = 0 then
				Result := 0
			else
				Result := -1
				if v /= 0xff then
					from
						i := 0
						m := 1
					until i > 7 or Result >= 0
					loop
						if v & m = 0 then
							Result := i
						else
							i := i + 1
							m := m |<< 1
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	byte_complement (v: NATURAL_8): NATURAL_8
			-- Complement of 'v'
		do
			Result := c_byte_complement (v)
		end

--|========================================================================
feature -- Word and long word operations
--|========================================================================

	xor_n32 (v1, v2: NATURAL_32): NATURAL_32
			-- Bit-wise exclusive OR of values 'v1' and 'v2'
		do
			Result := v1.bit_xor (v2)
		end

	xor_n64 (v1, v2: NATURAL_64): NATURAL_64
			-- Bit-wise exclusive OR of values 'v1' and 'v2'
		do
			Result := v1.bit_xor (v2)
		end

	--|--------------------------------------------------------------

	highest_bit_set_in_n32 (v: NATURAL_32): INTEGER
			-- The number (from 0 to 31, of the most significant bit
			-- that is set in the given unsigned 32 bit integer
			-- If none is set, then -1
		do
			Result := -1
			if v /= 0 then
				Result := c_highest_bit_set_32 (v)
			end
		end

	highest_bit_set_in_n64 (v: NATURAL_64): INTEGER
			-- The number (from 0 to 63, of the most significant bit
			-- that is set in the given unsigned 64 bit integer
			-- If none is set, then -1
		do
			Result := -1
			if v /= 0 then
				if (v & 0xFFFFFFFF00000000) /= 0 then
					-- Hi bit is in upper 32 (32-63)
					Result := c_highest_bit_set_32 ((v |>> 32).as_natural_32)+32
				else
					Result := c_highest_bit_set_32 (v.as_natural_32)
				end
			end
		end

	--|--------------------------------------------------------------

	num_leading_zeroes_n64 (v: NATURAL_64): INTEGER
			-- Number of leading zeroes in 'v'
			-- Examples:
			--   if bit 63 is set, then result is 0
			--   if bit 62 is highest set, then result is 1, etc.
			--   if no bits are set, then result is 64
		local
			hbs: INTEGER
		do
			hbs := highest_bit_set_in_n64 (v)
			-- If no bits are set, hbs=-1
			Result := 63 - hbs
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	hi_bits_by_value: ARRAY [INTEGER]
			-- The highest set 0-based bit positions for a given value
			-- If no bits are set, the result is -1
			--
			-- NOTE WELL that this is a 0-based array!!
		local
			i: INTEGER
		once
			create Result.make_filled (0, 0, 255)
			Result.put (-1, 0) -- 00000000
			Result.put (0, 1)  -- 00000001
			Result.put (1, 2)  -- 00000010
			Result.put (1, 3)  -- 00000011
			Result.put (2, 4)  -- 00000100
			Result.put (2, 5)  -- 00000101
			Result.put (2, 6)  -- 00000110
			Result.put (2, 7)  -- 00000111
			from i := 8
			until i > 15
			loop
				Result.put (3, i) -- 00001000 - 00001111
				i := i + 1
			end
			from i := 16
			until i > 31
			loop
				Result.put (4, i) -- 00010000 - 00011111
				i := i + 1
			end
			from i := 32
			until i > 63
			loop
				Result.put (5, i) -- 00100000 - 00111111
				i := i + 1
			end
			from i := 64
			until i > 127
			loop
				Result.put (6, i) -- 01000000 - 01111111
				i := i + 1
			end
			from i := 128
			until i > 255
			loop
				Result.put (7, i) -- 10000000 - 11111111
				i := i + 1
			end
		ensure
			exists: Result /= Void
			valid_range: Result.lower = 0 and Result.upper = 255
		end

	--|--------------------------------------------------------------

	bit_masks_8: ARRAY [NATURAL_8]
			-- Single bit masks for each bit in an 8-bit natural,
			-- indexed from 0 through 7
			-- To support bitwise iteration from least to most 
			-- significant bits in a byte
		once
			Result := {ARRAY [NATURAL_8]}<< 1, 2, 4, 8, 16, 32, 64, 128 >>
			Result.rebase (0)
		end

--|========================================================================
feature -- Externals
--|========================================================================

	c_byte_complement (v: NATURAL_8): NATURAL_8
			-- Complement of 'v'
		require
			in_range: v >= 0 and v <= 255
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
				return ~($v);
			}
			]"
		end

	--|--------------------------------------------------------------

	c_even_8_multiple (v: INTEGER): INTEGER
		-- Nearest value at or greater than 'v' that is an even
		-- multiple of 8
		require
			non_negative: v >= 0
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
				int Result=8;
				int tv = $v % 8;
				if ($v != 0) {
					if (tv == 0)
						Result = $v;
					else
						Result = $v + 8 - tv;
				}
				return Result;
			}
			]"
		end

	c_even_8_multiple_64 (v: NATURAL_64): NATURAL_64
		-- Nearest value at or greater than 'v' that is an even
		-- multiple of 8
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
			unsigned long long Result=8;
			unsigned long long tv = $v % 8;
				if ($v != 0) {
					if (tv == 0)
						Result = $v;
					else
						Result = $v + 8 - tv;
				}
				return Result;
			}
			]"
		end

	--|--------------------------------------------------------------

	c_even_10_multiple (v: INTEGER): INTEGER
		-- Nearest value at or greater than 'v' that is an even
		-- multiple of 10
		require
			non_negative: v >= 0
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
				int Result=10;
				if ($v != 0) {
					if (($v % 10) == 0)
						Result = $v;
					else
						Result = $v + (10 - ($v % 10));
				}
				return Result;
			}
			]"
		end

	c_even_512_multiple (v: INTEGER): INTEGER
		-- Nearest value at or greater than 'v' that is an even
		-- multiple of 512
		require
			non_negative: v >= 0
		external
			"C inline use %"stdlib.h%""
		alias
			"[
			{
				int Result=512;
				int tv = $v % 512;
				if ($v != 0) {
					if (tv == 0)
						Result = $v;
					else
						Result = $v + 512 - tv;
				}
				return Result;
			}
			]"
		end

	--|--------------------------------------------------------------

	num_bits_set_8 (byte: NATURAL_8): INTEGER
			-- Number of bits set in the given byte
			-- Uses Kernighan's algorithm
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned char v; // count the number of bits set in v
				int Result=0; // c accumulates the total bits set in v
				v = $byte;
				for (; v; Result++) {
				  v &= v - 1; // clear the least significant bit set
				}
				return Result;
				}
			]"
		end

	num_bits_set_32 (num: NATURAL_32): INTEGER
			-- Number of bits set in the given 32 bit number
			-- Uses Kernighan's algorithm
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned int v; // count the number of bits set in v
				int Result=0; // c accumulates the total bits set in v
				v = $num;
				for (; v; Result++) {
				  v &= v - 1; // clear the least significant bit set
				}
				return Result;
				}
			]"
		end

	c_lowest_bit_set_8 (byte: NATURAL_8): INTEGER
			-- Least significant 1 bit in 'byte'
			-- -1 if none is set
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
				unsigned char v, m;
				int i=0;
				v = $byte;
				if (v==0) { return (-1); }
				for (m=1; i<8; i++) {
					if (v & m) { return (i); }
					i++;
					m << i;
				}
				return (-1);
				}
			]"
		end

	--|--------------------------------------------------------------

	c_highest_bit_set_32 (v: NATURAL_32): INTEGER
			-- Most significant 1 bit in 'v'
			-- -1 if none is set
		external
			"C inline use %"stdlib.h%""
		alias
			"[
				{
					unsigned int tv;
					unsigned int result = 0;
					tv = $v;
					while (tv >>= 1) {
						result++;
					}
					return (result);
				}
			]"
		end

end -- class AEL_SPRT_NUMERIC_INTEGER_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 005 19-Jul-2018
--|     Renamed highest_bit_set to highest_bit_set_in_byte
--|     Added highest_bit_set_in_n32, highest_bit_set_in_n64,
--|     num_leading_zeroes_n64, and c_highest_bit_set_32
--|     Updated syntax of manifest array in bit_masks_8
--|     Compiled void-safe with 18.1
--| 004 24-Feb-2013
--|     Compiled and tested using void-safe Eiffel 7.1
--|     Reimplemented lowest_bit_set and lowest_bit_unset to fix errors
--| 003 05-Nov-2012
--|     Compiled and tested using Eiffel 7.1
--|     Added c_lowest_bit_set_8.
--|     Reimplemented lowest_bit_set to use masks
--|     Reimplemented lowest_bit_unset to remove complement and use 
--|     simple loop instead
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|
--| This class is typically inherited by another class needing it
--| capabilities.
--| This class can be instantiated by itself if desired.
--|----------------------------------------------------------------------
