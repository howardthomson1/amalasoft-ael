--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A numeric value expressed as a whole and fractional parts,
--| specifically targeted to US bulding trades.
--|----------------------------------------------------------------------

class AEL_SPRT_USBLD_FRACTION

inherit
	AEL_SPRT_FRACTION
		redefine
			is_valid_denominator, default_denominators, denominators
		end

create
	make_with_value, make_with_value_and_denominators

create {AEL_SPRT_FRACTION}
	make_with_components

--|========================================================================
feature -- Components
--|========================================================================

	denominators: ARRAY [INTEGER]
			-- Current set of denominators for comparison and result
		do
			Result := power_of_2_denominators
		end

	default_denominators: ARRAY [INTEGER]
			-- Default set of denominators for comparison and result
		do
			Result := power_of_2_denominators
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_denominator (v: INTEGER): BOOLEAN
			-- Is the given value a valid denomiator?
		do
			Result := default_denominators.has (v)
		end

end -- class AEL_SPRT_USBLD_FRACTION

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|
--| Refer to the howto clause in the AEL_FRACTION parent class
--|----------------------------------------------------------------------
