note
	description: "{
Simple encapsulation of a pair of dimensional or positional values
Examples: row,column   x,y    width,height  lat/lon
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2020 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_VALUE_PAIR

inherit
	AEL_PRINTF
		redefine
			is_equal, out
		end

create
	make_x_y, make_row_col, make_size, make_distance,
	make_size_d, make_lat_lon, make_degrees,
	make_as_integers, make_as_doubles

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_x_y (v1, v2: INTEGER)
			-- Create Current as X and Y coordinates
		do
			value_type := K_value_type_xy
			make_as_integers (v1, v2)
		end

	make_row_col (v1, v2: INTEGER)
			-- Create Current as row and column
		do
			value_type := K_value_type_rc
			make_as_integers (v1, v2)
		end

	make_size (v1, v2: INTEGER)
			-- Create Current as integer width and height
		do
			value_type := K_value_type_sizei
			make_as_integers (v1, v2)
		end

	make_distance (v1, v2: INTEGER)
			-- Create Current as integer distance, left and right
		do
			value_type := K_value_type_dxi
			make_as_integers (v1, v2)
		end


	make_lat_lon (v1, v2: DOUBLE)
			-- Create Current as double-precision latitude/longitude
		do
			value_type := K_value_type_latlon
			make_as_doubles (v1, v2)
		end

	make_size_d (v1, v2: DOUBLE)
			-- Create Current as double-precision width and height
		do
			value_type := K_value_type_sized
			make_as_doubles (v1, v2)
		end

	make_degrees (v1, v2: DOUBLE; dir1, dir2: CHARACTER)
			-- Create Current as double-precision relative degrees
			-- 'dir1' is direction of 'v1' (NSEW)
			-- 'dir2' is direction of 'v2' (NSEW)
			-- There is no prohibition of illogical combinations (e.g. 'N', 'S')
		do
			value_type := K_value_type_deg
			direction_1 := dir1
			direction_2 := dir2
			make_as_doubles (v1, v2)
		end

	--|--------------------------------------------------------------

	make_as_integers (v1, v2: INTEGER)
		do
			ivalue_1 := v1
			ivalue_2 := v2
		end

	make_as_doubles (v1, v2: DOUBLE)
		do
			dvalue_1 := v1
			dvalue_2 := v2
		end

--|========================================================================
feature -- Status
--|========================================================================

	value_type: INTEGER

	ivalue_1: INTEGER
	ivalue_2: INTEGER

	dvalue_1: DOUBLE
	dvalue_2: DOUBLE

	is_double: BOOLEAN
			-- Are the value represented double-precision floats?
			-- Dflt is no, values are ints

	direction_1: CHARACTER
	direction_2: CHARACTER

	--|--------------------------------------------------------------

	pf_format_int: STRING
		do
			if attached private_pf_fmt_int as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_int_dflt
			end
		end

	pf_format_double: STRING
		do
			if attached private_pf_fmt_double as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_dbl_dflt
			end
		end

	pf_format_xy: STRING
		do
			if attached private_pf_fmt_xy as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_xy_dflt
			end
		end

	pf_format_rc: STRING
		do
			if attached private_pf_fmt_rc as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_rc_dflt
			end
		end

	pf_format_sizei: STRING
		do
			if attached private_pf_fmt_sizei as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_sizei_dflt
			end
		end

	pf_format_dxi: STRING
		do
			if attached private_pf_fmt_dxi as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_dxi_dflt
			end
		end

	pf_format_sized: STRING
		do
			if attached private_pf_fmt_sized as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_sized_dflt
			end
		end

	pf_format_latlon: STRING
		do
			if attached private_pf_fmt_latlon as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_latlon_dflt
			end
		end

	pf_format_degrees: STRING
		do
			if attached private_pf_fmt_deg as fmt then
				Result := fmt
			else
				Result := Ks_pf_fmt_deg_dflt
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_ivalue_1 (v: INTEGER)
		do
			ivalue_1 := v
		end

	set_ivalue_2 (v: INTEGER)
		do
			ivalue_2 := v
		end

	set_dvalue_1 (v: DOUBLE)
		do
			dvalue_1 := v
		end

	set_dvalue_2 (v: DOUBLE)
		do
			dvalue_2 := v
		end

	--|--------------------------------------------------------------

	set_x (v: INTEGER)
		do
			set_ivalue_1 (v)
		ensure
			is_set: x = v
		end

	set_y (v: INTEGER)
		do
			set_ivalue_2 (v)
		ensure
			is_set: y = v
		end

	set_row (v: INTEGER)
		do
			set_ivalue_1 (v)
		ensure
			is_set: row = v
		end

	set_column (v: INTEGER)
		do
			set_ivalue_2 (v)
		ensure
			is_set: column = v
		end

	set_width (v: INTEGER)
		do
			set_ivalue_1 (v)
		ensure
			is_set: width = v
		end

	set_height (v: INTEGER)
		do
			set_ivalue_2 (v)
		ensure
			is_set: height = v
		end

	set_dx_a (v: INTEGER)
		do
			set_ivalue_1 (v)
		ensure
			is_set: dx_a = v
		end

	set_dx_b (v: INTEGER)
		do
			set_ivalue_2 (v)
		ensure
			is_set: dx_b = v
		end

	--|--------------------------------------------------------------

	set_lat (v: DOUBLE)
		do
			dvalue_1 := v
		ensure
			is_set: lat = v
		end

	set_lon (v: DOUBLE)
		do
			dvalue_2 := v
		ensure
			is_set: lon = v
		end

	set_real_width (v: DOUBLE)
		do
			dvalue_1 := v
		ensure
			is_set: real_width = v
		end

	set_real_height (v: DOUBLE)
		do
			dvalue_2 := v
		ensure
			is_set: real_height = v
		end

	--|--------------------------------------------------------------

	set_pf_fmt_double (v: detachable STRING)
		do
			private_pf_fmt_double := v
		end

	set_pf_fmt_int (v: detachable STRING)
		do
			private_pf_fmt_int := v
		end

	set_pf_fmt_xy (v: detachable STRING)
		do
			private_pf_fmt_xy := v
		end

	set_pf_fmt_rc (v: detachable STRING)
		do
			private_pf_fmt_rc := v
		end

	set_pf_fmt_sizei (v: detachable STRING)
		do
			private_pf_fmt_sizei := v
		end

	set_pf_fmt_dxi (v: detachable STRING)
		do
			private_pf_fmt_dxi := v
		end

	set_pf_fmt_sized (v: detachable STRING)
		do
			private_pf_fmt_sized := v
		end

	set_pf_fmt_latlon (v: detachable STRING)
		do
			private_pf_fmt_latlon := v
		end

	set_pf_fmt_degrees (v: detachable STRING)
		do
			private_pf_fmt_deg := v
		end

--|========================================================================
feature -- Access
--|========================================================================

	x, row, width, dx_a: INTEGER
		do
			Result := ivalue_1
		end

	y, column, height, dx_b: INTEGER
		do
			Result := ivalue_2
		end

	--|--------------------------------------------------------------

	lat, real_width: DOUBLE
		do
			if is_double then
				Result := dvalue_1
			else
				Result := ivalue_1 * 1.0
			end
		end

	lon, real_height: DOUBLE
		do
			if is_double then
				Result := dvalue_2
			else
				Result := ivalue_2 * 1.0
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_equal (other: like Current): BOOLEAN
			-- Is other equivalent to Current?
		do
			if is_double and other.is_double then
				Result := dvalue_1 = other.dvalue_1 and dvalue_2 = other.dvalue_2
			elseif (not is_double) and (not other.is_double) then
				Result := ivalue_1 = other.ivalue_1 and ivalue_2 = other.ivalue_2
			end
		end

--|========================================================================
feature -- External representation and serialization support
--|========================================================================

	out: STRING
		local
			fmt: STRING
		do
			inspect value_type
			when K_value_type_xy then
				fmt := pf_format_xy
			when K_value_type_rc then
				fmt := pf_format_rc
			when K_value_type_sizei then
				fmt := pf_format_sizei
			when K_value_type_dxi then
				fmt := pf_format_dxi
			when K_value_type_sized then
				fmt := pf_format_sized
			when K_value_type_latlon then
				fmt := pf_format_latlon
			when K_value_type_deg then
				fmt := pf_format_degrees
			else
				-- Generic values	
				if is_double then
					fmt := pf_format_double
 				else
					fmt := pf_format_int
				end
			end
			Result := out_formatted (fmt)
		end

	--|--------------------------------------------------------------

	out_formatted (fmt: STRING): STRING
			-- String representaion of values, formatted (via aprintf) 
			-- to 'fmt'
		require
			valid: not fmt.is_empty
		do
			if is_double then
				Result := aprintf (fmt, << dvalue_1, dvalue_2 >>)
			else
				Result := aprintf (fmt, << ivalue_1, ivalue_2 >>)
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation (private)
--|========================================================================

	private_pf_fmt_double: detachable STRING
	private_pf_fmt_int: detachable STRING

	private_pf_fmt_xy: detachable STRING
	private_pf_fmt_rc: detachable STRING
	private_pf_fmt_sizei: detachable STRING
	private_pf_fmt_dxi: detachable STRING
	private_pf_fmt_sized: detachable STRING
	private_pf_fmt_latlon: detachable STRING
	private_pf_fmt_deg: detachable STRING

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_pf_fmt_dbl_dflt: STRING = "%%3.2f,%%3.2f"
			-- Default printf format string for generic double values
	Ks_pf_fmt_int_dflt: STRING = "%%d,%%d"
			-- Default printf format string for generic integer values

	Ks_pf_fmt_xy_dflt: STRING = "%%d,%%d"
			-- Default printf format string for X/Y values
	Ks_pf_fmt_rc_dflt: STRING = "%%d,%%d"
			-- Default printf format string for row/column size values
	Ks_pf_fmt_sizei_dflt: STRING = "%%d,%%d"
			-- Default printf format string for integer size values
	Ks_pf_fmt_dxi_dflt: STRING = "%%d,%%d"
			-- Default printf format string for integer distance values
	Ks_pf_fmt_sized_dflt: STRING = "%%3,2f,%%3.2f"
			-- Default printf format string for DP size values
	Ks_pf_fmt_latlon_dflt: STRING = "%%3.2,%%3.2f"
			-- Default printf format string for LAT/LON values
	Ks_pf_fmt_deg_dflt: STRING = "%%3.2,%%3.2f"
			-- Default printf format string for degree values

	--|--------------------------------------------------------------

	K_value_type_xy: INTEGER = 1
			-- Integer coordinate (x,y) value type
	K_value_type_rc: INTEGER = 2
			-- Integer grid position (row,col) value type
	K_value_type_sizei: INTEGER = 3
			-- Integer size (width,height) value type
	K_value_type_dxi: INTEGER = 4
			-- Integer distance (lr, tb) value type
	K_value_type_sized: INTEGER = 5
			-- Double-precision size (width,height) value type
	K_value_type_latlon: INTEGER = 6
			-- Double-precision geo position (lat,lon) value type
	K_value_type_deg: INTEGER = 7
			-- Double-precision degrees value type

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 10-Jun-2020
--|     Created original module (Eiffel 18.01)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_VALUE_PAIR
