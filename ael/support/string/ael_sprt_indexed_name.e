--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A common name format, having a base name and a numeric extension
--| All instances register with a shared registrar, so that unique
--| extensions can be assigned without the need for a client-based
--| tracking (uniqueness is for extension/base pair)
--|----------------------------------------------------------------------

class AEL_SPRT_INDEXED_NAME

inherit
	STRING
		rename
			make as string_make,
			append as string_append,
			extend as string_extend
		redefine
			is_less, substring, default_create
		end

create
	make, make_with_separator, make_with_next_index, make_with_first_index,
	make_to_purge, make_unregistered, make_for_substring

 --|========================================================================
feature {NONE}
 --|========================================================================

	make (b: STRING)
			-- Create a new indexed name with the first available index
			-- using the default separator
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			core_make (b, '%U', False, 0, True)
		end

	--|--------------------------------------------------------------

	make_unregistered (b: STRING)
			-- Create a new indexed name with the first available index
			-- using the default separator
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			core_make (b, '%U', False, 0, False)
		end

	--|--------------------------------------------------------------

	make_with_separator (b: STRING; sep: CHARACTER)
			-- Create a new indexed name with the first available index
			-- using the given separator (separator may be empty)
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			core_make (b, sep, False, 0, True)
		end

	--|--------------------------------------------------------------

	make_with_next_index (b: STRING; sep: CHARACTER)
			-- Create a new indexed name with the first available index
			-- using the default separator
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			core_make (b, sep, True, 0, True)
		end

	--|--------------------------------------------------------------

	make_with_first_index (b: STRING; sep: CHARACTER; idx: INTEGER)
			-- Create a new indexed name with the given index
			-- Remove from the regsitry all other instances of names with
			-- matching base_names
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			base_name.append (b)
			purge_like_names
			core_make (b, sep, False, idx, True)
		end

	--|--------------------------------------------------------------

	make_to_purge (b: STRING)
			-- Create a new indexed name for use by purge mechanism
		require
			base_arg_exists: b /= Void and then not b.is_empty
		do
			default_create
			core_make (b, '%U', False, 0, True)
			purge_like_names
		end

	--|--------------------------------------------------------------

	make_for_substring (b: STRING)
		require
			not_void: b /= Void
		do
			default_create
			string_make (32)
			string_append (b)
			base_name.append (b)
		end

	--|--------------------------------------------------------------

	core_make (
		b: STRING; sep: CHARACTER; next: BOOLEAN; idx: INTEGER;
		do_registration: BOOLEAN)
			-- Create a new indexed name with the first or next available index
		require
			base_arg_exists: b /= Void and then not b.is_empty
			index_overrides_next: idx /= 0 implies not next
		do
			default_create
			string_make (32)
			base_name.append (b)
			if next then
				index := next_index
			elseif idx /= 0 then
				index := idx
			else
				index := first_available_index
			end

			if sep /= '%U' then
				separator := sep
			end

			coelesce
			if do_registration then
				register
			end
		end

	--|--------------------------------------------------------------

	default_create
		do
			create private_base_name.make (32)
			separator := Kc_dflt_extension_separator
		end

--|========================================================================
feature --{AEL_SPRT_INDEXED_NAME}
--|========================================================================

	base_name: STRING
		do
			if not private_base_name.is_empty then
				Result := private_base_name
			else
				Result := string
			end
		end

--|========================================================================
feature
--|========================================================================

	separator: CHARACTER
	index: INTEGER

	--|--------------------------------------------------------------

	new_with_next_index: like Current
			-- Generate a new indexed name with the same base name and 
			-- separator as Current.
		do
			create Result.make_with_next_index (base_name, separator)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	index_in_use (v: INTEGER): BOOLEAN
		local
			rl: like registrar
			ti: like Current
		do
			rl := registrar
			from rl.start
			until rl.exhausted or Result
			loop
				ti := rl.item
				if ti.base_name ~ base_name then
					Result := ti.index = v
				end
				rl.forth
			end
			rl.start
		end

	--|--------------------------------------------------------------

	set_base (v: STRING)
		require
			arg_exists: v /= Void
		do
			base_name.wipe_out
			base_name.append (v)
			coelesce
		end

	--|--------------------------------------------------------------

	append_to_base (v: STRING)
		do
			if v /= Void and then not v.is_empty then
				base_name.append (v)
				coelesce
			end
		end

	--|--------------------------------------------------------------

	is_less alias "<"  (other: like Current): BOOLEAN
			-- Is Current less than other?
			-- Yes if base_name < other.name AND if index < other.index
		do
			if attached {like Current} other as tos then
				Result := base_name < other.base_name
				if not Result then
					if base_name.is_equal (other.base_name) then
						Result := index < other.index
					end
				end
			else
				-- Despite the type-anchoring signature, it is possible
				-- at run time to be given an 'other' that is a mere STRING.
				-- As such, we get a nasty segv when trying to compare
				-- base_names (the STRING object has no STRING attribute at
				-- that offset).  This is known as a "Polymorphic CAT Call".
				-- To avoid such behavior, we reinvent ourselves as a simple
				-- STRING (temporarily then perform the comparison.
				--
				-- NOTE: we can not use "other >= Current" because that in
				-- turn calls us again and we overflow the stack eventually
				-- with our infinite recursion.

				--create tstr.make_from_string (Current)
				Result := Current.string < other
			end
		end

	--|--------------------------------------------------------------

	substring (n1, n2: INTEGER): like Current
			-- We redefine substring here to ensure that the resulting object
			-- is created properly (with basename) to avoid violating the
			-- class invariant from STRING (inflexive_comparison)
			--
			-- Copy of substring containing all characters at indices
			-- between `n1' and `n2'
		local
			tstr: STRING
		do
			create tstr.make_from_string (Current)
			create Result.make_for_substring (tstr.substring (n1, n2))
		end

	--|--------------------------------------------------------------

	matches_string (v: STRING): BOOLEAN
		require
			string_exists: v /= Void
		local
			tstr: STRING
		do
			create tstr.make_from_string (Current)
			Result := tstr.is_equal (v)
		end

	--|--------------------------------------------------------------

	register
		do
			registrar.extend (Current)
		end

	unregister
		do
			registrar.start
			registrar.search (Current)
			if not registrar.exhausted then
				registrar.remove
			end
			registrar.start
		end

	--|--------------------------------------------------------------

	purge_all
		do
			registrar.wipe_out
		end

	--|========================================================================
feature -- Parsing foreign strings that might have this structure
	--|========================================================================

	index_from_string (v: STRING; sep: CHARACTER): INTEGER
			-- If the given string resembles an INDEXED_NAME, then extract from
			-- it the integer value of its index part.
			-- By our convention, only positive numbers are valid indices,
			-- so a result of zero denotes an ill-formed string.
		require
			other_exist: v /= Void
		do
			if attached {like Current} v as tn then
				Result := tn.index
			else
				Result := parts_from_string (v, sep, Void)
			end
		end

	--|--------------------------------------------------------------

	base_from_string (v: STRING; sep: CHARACTER): STRING
			-- If the given string resembles an INDEXED_NAME, then extract from
			-- it the substring value that represents its base_name part.
		require
			other_exist: v /= Void
		local
			spos: INTEGER
			tstr: STRING
		do
			Result := ""
			if attached {like Current} v as tn then
				Result := tn.base_name.twin
			else
				create tstr.make (v.count)
				spos := parts_from_string (v, sep, tstr)
				if not tstr.is_empty then
					Result := tstr
				end
			end
		end

	--|========================================================================
feature {NONE}
	--|========================================================================

	registrar: SORTED_TWO_WAY_LIST [AEL_SPRT_INDEXED_NAME]
		once
			create Result.make
			Result.compare_objects
		end

	--|--------------------------------------------------------------

	first_available_index: INTEGER
		do
			Result := next_available_index (0)
		end

	--|--------------------------------------------------------------

	next_index_after_current: INTEGER
		do
			Result := next_available_index (index)
		end

	--|--------------------------------------------------------------

	next_index: INTEGER
		local
			i: INTEGER
		do
			i := highest_index_in_use
			Result := next_available_index (i)
		end

	--|--------------------------------------------------------------

	next_available_index (v: INTEGER): INTEGER
		require
			not_negative: v >= 0
		local
			i: INTEGER
			big1: INTEGER
		do
			big1 := 2_147_000_000
			from i := v + 1
			until (Result /= 0) or (i < 0)
			loop
				if not index_in_use (i) then
					Result := i
				end
				i := i + 1
			variant big1 - i
			end
		end

	--|--------------------------------------------------------------

	highest_index_in_use: INTEGER
		local
			rl: like registrar
			ti: like Current
		do
			rl := registrar
			from rl.start
			until rl.exhausted
			loop
				ti := rl.item
				if ti.base_name ~ base_name then
					Result := ti.index
				end
				rl.forth
			end
			rl.start
		end

	--|--------------------------------------------------------------

	purge_like_names
		local
			rl: like registrar
			ti: like Current
			b: like base_name
		do
			b := base_name
			rl := registrar
			from rl.start
			until rl.exhausted or rl.is_empty
			loop
				ti := rl.item
				if ti.base_name ~ b then
					rl.remove
				else
					rl.forth
				end
			end
			rl.start
		end

	--|--------------------------------------------------------------

	coelesce
		local
			ts: STRING
			pf: AEL_PRINTF
		do
			create pf
			wipe_out
			if separator /= '%U' then
				ts := separator.out
			else
				ts := ""
			end
			string_append (pf.aprintf ("%%s%%s%%04d", << base_name, ts, index >>))
		end

	--|--------------------------------------------------------------

	parts_from_string (v: STRING; sep: CHARACTER; bn: detachable STRING): INTEGER
			-- If the given string resembles an INDEXED_NAME, then extract from
			-- it the substring value that represents its base_name part and 
			-- return the integer value of its index part.
			--
		require
			other_exist: v /= Void
		local
			ts: CHARACTER
			spos: INTEGER
			tstr: STRING
		do
			ts := sep
			if ts = '%U' then
				ts := separator
			end
			if ts /= '%U' then
				spos := v.index_of (ts, 1)
				if spos > 1 then
					tstr := v.substring (spos + 1, v.count)
					if tstr.is_integer then
						Result := tstr.to_integer
						if attached bn as b then
							b.wipe_out
							b.append (v.substring (1, spos - 1))
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	Kc_dflt_extension_separator: CHARACTER = '_'

	private_base_name: STRING

	--|--------------------------------------------------------------
invariant
	base_name_exists: base_name /= Void
	valid_count: count >= base_name.count

end -- class AEL_SPRT_INDEXED_NAME

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 28-Jul-2009
--|     Added history.  Changed name from AEL_STR_INDEXED_NAME.
--|     Moved from string cluster to support cluster.
--|     Compiled and tested using Eiffel 6.1 and 6.4
--| 003 21-Apr-2008
--|     Resurrected from much older version, converted to current style
--|----------------------------------------------------------------------
--| How-to
--|
--| Create a new instance using the make creation routine.
--| It will in turn create a new name with the first available index,
--| and the given base name, using the default separator.
--| Subsequent instances with the same base will have different indices
--|----------------------------------------------------------------------
	
