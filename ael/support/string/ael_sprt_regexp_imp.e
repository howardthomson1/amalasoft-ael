class AEL_SPRT_REGEXP_IMP
-- Regular expression adapting features from the GOBO library and a 
-- venerable but flawed 'other' library

inherit
	RX_PCRE_REGULAR_EXPRESSION
		rename
			make as rxpcre_make
		redefine
			compile
		end

create
	make_and_compile, make_and_compile_insensitive

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_and_compile (p: STRING)
			-- Create Current with pattern 'p'
		require
			exists: p /= Void and then not p.is_empty
		do
			rxpcre_make
			compile (p)
		end

	make_and_compile_insensitive (p: STRING)
			-- Create Current with pattern 'p' adapted for 
			-- case-insensitivitty
		require
			exists: p /= Void and then not p.is_empty
		do
			rxpcre_make
			set_caseless (True)
			compile (p)
			make_and_compile (p)
		end

--|========================================================================
feature -- Search and matching functions
--|========================================================================

	compile (p: STRING)
		do
			original_pattern := p.twin
			Precursor (p)
		end

	--|--------------------------------------------------------------

	search_forward (buf: STRING; sp, ep: INTEGER)
			-- Search for compiled pattern in 'buf', beginning at 'sp', 
			-- through 'ep', inclusive
			-- Set 'has_matched' to indicate success/failure
			-- Set 'start_position' and 'end_position' to denote 
			-- positions in 'buf' at which a match was found, if any
		require
			exists: buf /= Void
			valid_start: (not buf.is_empty) implies sp > 0 and sp <= buf.count
			valid_end: (not buf.is_empty) implies ep <= buf.count and ep >= sp
		do
			match_substring (buf, sp, ep)
		end

--|========================================================================
feature -- Status
--|========================================================================

	start_position: INTEGER
		do
			Result := captured_start_position (0)
		end

	end_position: INTEGER
		do
			Result := captured_end_position (0)
		end

	original_pattern: STRING

--|========================================================================
feature {AEL_SPRT_REGEXP} -- Status setting
--|========================================================================

	recompile
		require
			compiled: is_compiled
		do
			reset
			compile (original_pattern)
		end

end -- class AEL_SPRT_REGEXP_IMP
