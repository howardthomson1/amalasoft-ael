note
	description: "{
Stock string with hook to inspect string areas
NOTA BENE DO NOT USE AS A STRING REPLACEMENT!!
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/06/25 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_STRING_HANDLER

inherit
	STRING_8

create
	make_empty

create {STRING_GENERAL}
	make

--|========================================================================
feature -- Access
--|========================================================================

	str8_area (s: READABLE_STRING_8): SPECIAL [CHARACTER_8]
			-- area belonging to string 's'
		require
			exists: s /= Void
		do
			Result := s.area
		end

	str32_area (s: READABLE_STRING_32): SPECIAL [CHARACTER_32]
			-- area belonging to string 's'
		require
			exists: s /= Void
		do
			Result := s.area
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 25-Jn-2013
--|     Created original module (for Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_STRING_HANDLER
