--|----------------------------------------------------------------------
--| Copyright (c) 1995-2019, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| String manipulation and inspection routines
--|----------------------------------------------------------------------

class AEL_SPRT_STRING_ROUTINES

inherit
	AEL_PRINTF

create
	default_create

--|========================================================================
feature -- String classification by case
--|========================================================================

	is_upper_case (v: STRING): BOOLEAN
			-- Is the entire string UPPER CASE?
			-- True of each alpha character is upper case.
			-- False if any alpha character is not upper case
		require
			exists: v /= Void and then not v.is_empty
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			Result := True
			lim := v.count
			from i := 1
			until i > lim or not Result
			loop
				c := v.item (i)
				if c.is_alpha then
					Result := c.is_upper
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	is_lower_case (v: STRING): BOOLEAN
			-- Is the entire string lower case?
			-- True of each alpha character is lower case.
			-- False if any alpha character is not lower case
		require
			exists: v /= Void and then not v.is_empty
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			Result := True
			lim := v.count
			from i := 1
			until i > lim or not Result
			loop
				c := v.item (i)
				if c.is_alpha then
					Result := c.is_lower
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	is_capital (v: STRING): BOOLEAN
			-- Is the given string capitalized?
			-- True if all characters are alpha, first char is upper
			-- case and all other chars are lower case
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			if not v.is_empty and then
				(v.item (1).is_alpha and v.item (1).is_upper)
			 then
				 Result := True
				 lim := v.count
				from i := 2
				until i > lim or not Result
				loop
					c := v.item (i)
					if not c.is_alpha then
						Result := False
					elseif c.is_lower then
						Result := c.is_lower
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	is_title_case_worthy (v: STRING): BOOLEAN
			-- Is 'v' a word worthy of being capitalized (for a title 
			-- context)?
			-- Assume v is NOT the first word in a sentence
			-- articles, numbers and words beginning with punctuation 
			-- are not worthy; nor are most prepositions
			-- TODO, add option to force/exclude list of specials
			-- TODO, add multi-lingual support
		local
			low_v: STRING
			c: CHARACTER
			i, lim: INTEGER
		do
			if not v.is_empty then
				Result := True
				-- Find first alphanumeric in 'v'
				lim := v.count
				from i := 1
				until i > lim or not Result
				loop
					c := v.item (i)
					if c.is_alpha then
						low_v := v.substring (i, lim).as_lower
						if unworthy_words.has (low_v) then
							Result := False
						else
							i := lim
						end
					elseif c.is_digit then
						-- nope
						Result := False
					else
						-- Could be punctuation; keep looking
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	unworthy_words: HASH_TABLE [STRING, STRING]
			-- Words unworth of capitalization in title case
		once
			create Result.make (17)
			Result.extend ("a", "a")
			Result.extend ("am", "am")
			Result.extend ("an", "an")
			Result.extend ("and", "and")
			Result.extend ("are", "are")
			Result.extend ("as", "as")
			Result.extend ("at", "at")
			Result.extend ("in", "in")
			Result.extend ("is", "is")
			Result.extend ("of", "of")
			Result.extend ("the", "the")
			Result.extend ("that", "that")
-- Candidates: for, with, within
		end

--|========================================================================
feature -- String classification by type/token
--|========================================================================

	is_name (v: STRING; ac: ARRAY [CHARACTER]): BOOLEAN
			-- Does the string start with a letter and then contain
			-- only letters, numbers and other allowed characters?
		require
			exists: v /= Void and then not v.is_empty
			allowed_chars_exist: ac /= Void
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			Result := v.item (1).is_alpha
			lim := v.count
			from i := 2
			until i > lim or not Result
			loop
				c := v.item (i)
				if not c.is_alpha_numeric then
					Result := ac.has (c)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	is_boolean (v: STRING): BOOLEAN
			-- Is the given string a Boolean value?
		require
			exists: v /= Void and then not v.is_empty
		local
			us: STRING
		do
			us := v.as_upper
			Result := us.is_equal ("TRUE") or us.is_equal ("FALSE")
		end

	--|--------------------------------------------------------------

	is_decimal_integer (v: STRING): BOOLEAN
			-- Does the string represent a decimal integer?
			-- N.B. octal strings are also decimal strings
			-- If necessary to differentiate between octal and decimal (or
			-- hexadecimal), test first for octal, then decimal, then hexadecimal
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := v.is_integer
		end

	--|--------------------------------------------------------------

	is_positive_integer (v: STRING): BOOLEAN
			-- Does the string repressent a positive DECIMAL integer?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := v.item(1) /= '-' and v.is_integer
		end

	--|--------------------------------------------------------------

	is_negative_integer (v: STRING): BOOLEAN
			-- Does the string represtne a negative DECIMAL integer?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := v.item(1) = '-' and v.is_integer
		end

	--|--------------------------------------------------------------

	base_of_integer (v: STRING): INTEGER
			-- The base of the integer represented by the string 'v'
			-- Zero if v is not an integer of octal, decimal or 
			-- hexadecimal form
		do
			if is_hex_integer (v) then
				Result := 16
				if v.count = 1 then
					if v.is_integer then
						-- Non-hex char, is decimal
					end
				else
					-- Has more than one character
					-- Might also be decimal or octal
					if v.item (1) = '0' then
						-- Leading zero could be octal if 
						-- remainder of digits are in range.
						if is_octal_integer (v) then
							-- Leading zero, no hex chars, is octal
							Result := 8
						elseif v.is_integer then
							-- No hex chars, is decimal
							Result := 10
						else
							-- Must be hex
							Result := 16
						end
					elseif v.is_integer then
						-- No leading zero, no hex chars, is decimal
						Result := 10
					end
				end
			end
		ensure
			has_size_if_int: is_hex_integer (v) implies Result /= 0
		end

	--|--------------------------------------------------------------

	is_hex_integer (v: detachable STRING): BOOLEAN
			-- Does the given string represent a legitimate hexadecimal
			-- number?
			-- Can have leading 0x, but is not required.
			--
			-- If leading char is a zero, then it must be followed by an 'x',
			-- else the number is considered Octal, unless it contains one
			-- or more non-octal characters.
			-- If there is no leading 0, then even if the digits are all
			-- octal-compatible or decimal-compatible, then the value is
			-- still considered hexadecimal.
		do
			if v /= Void and then not v.is_empty then
				if v.item (1) = '0' and then is_octal_integer (v) then
					-- Is not hex, per rule above
				else
					Result := string_is_hex_integer (v)
				end
			end
		end

	--|--------------------------------------------------------------

	string_is_hex_integer (v: detachable STRING): BOOLEAN
			-- Does the given string represent a legitimate hexadecimal
			-- number?
			-- Can have leading 0x, but is not required.
			-- Can have a leading minus sign ('-')
			-- Cannot have a leading plus sign ('+')
		local
			spos: INTEGER
		do
			if v /= Void and then not v.is_empty then
				spos := 1
				if v.item (1) = '-' then
					spos := 2
				end
				if v.count > 2 then
					if v.item (spos) = '0' and v.item (spos + 1).as_lower = 'x' then
						spos := spos + 2
					end
				end
				Result := substring_is_hex_integer (v, spos, v.count)
			end
		end

	--|--------------------------------------------------------------

	substring_is_hex_integer (v: STRING; spos, epos: INTEGER): BOOLEAN
			-- Are each of the characters in the given sequence
			-- legal hexadecimal integers?
			-- Must NOT includes "0x" decoration
		require
			string_exists: v /= Void and then not v.is_empty
			valid_start: spos > 0 and spos <= v.count
			valid_end: epos >= spos and epos <= v.count
		local
			c: CHARACTER
			idx, lim: INTEGER
		do
			lim := epos
			Result := True
			from idx := spos
			until idx > lim or not Result
			loop
				c := v.item (idx)
				if not c.is_hexa_digit then
					Result := False
				else
					idx := idx + 1
				end
			end
		end

	--|--------------------------------------------------------------

	is_negative_hex_integer (v: STRING): BOOLEAN
			-- Does the given string represent a Negative hexadecimal
			-- number? (-0xabcd, -a, -15)
			-- Can have leading 0x (after the minus sign), but is not required.
			--
			-- If leading char after the minus sign is a zero, then it must be
			-- followed by an 'x', else the number is considered Octal,
			-- unless it contains one or more non-octal characters.
		require
			exists: v /= Void and then not v.is_empty
		do
			if v.item (1) = '-' then
				Result := string_is_hex_integer (v)
			end
		end

	--|--------------------------------------------------------------

	is_octal_integer (v: STRING): BOOLEAN
			-- Does the given string represent a legitimate octal number?
			-- Can have, but does not require a leading '0', and can have
			-- a leading '-' sign, but cannot have a leading '+' sign.
		require
			exists: v /= Void and then not v.is_empty
		local
			c: CHARACTER
			idx, lim: INTEGER
		do
			lim := v.count
			Result := True
			idx := 1
			if v.item (idx) = '-' then
				if v.count > 1 then
					idx := idx + 1
				else
					Result := False
				end
			end
			if Result and then (v.item (idx) = '0') then
				idx := idx + 1
			end
			from
			until idx > lim or not Result
			loop
				c := v.item (idx)
				inspect (c)
				when '0', '1', '2', '3', '4', '5', '6', '7' then
					idx := idx + 1
				else
					Result := False
				end
			end
		end

	--|--------------------------------------------------------------

	is_floating_point (v: STRING): BOOLEAN
			-- Does the string represent a floating point number?
			--
			-- N.B. - All INTEGERs are effectively also floating point number,
			-- but with a 0 fractional part.  The standard routine is_double
			-- will therefore return True for integer strings.
			-- This routine behaves differently, in that it returns False
			-- when the string also represents an integer.
		require
			exists: v /= Void and then not v.is_empty
		do
			if not v.is_integer then
				Result := v.is_double
			end
		end

	--|--------------------------------------------------------------

	is_character (v: STRING): BOOLEAN
			-- Does the string have a single character?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := v.count = 1
		end

	--|--------------------------------------------------------------

	is_character_digit (v: STRING): BOOLEAN
			-- Does the string have a single character that is a decimal digit?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := is_character (v) and then v.item (1).is_digit
		end

	--|--------------------------------------------------------------

	is_character_alpha (v: STRING): BOOLEAN
			-- Does the string have a single character that is alphabetic?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := is_character (v) and then v.item (1).is_alpha
		end

	--|--------------------------------------------------------------

	is_character_alphanumeric (v: STRING): BOOLEAN
			-- Does the string have a single character that is alphanumeric?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := is_character (v) and then v.item (1).is_alpha_numeric
		end

	--|--------------------------------------------------------------

	is_character_punctuation (v: STRING): BOOLEAN
			-- Does the string have a single character that is punctuation?
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := is_character (v) and then v.item (1).is_punctuation
		end

	--|--------------------------------------------------------------

	string_is_valid (
		v: detachable STRING;
		ef: BOOLEAN;
		vf: FUNCTION [CHARACTER, INTEGER, BOOLEAN]): BOOLEAN
			-- Is string 'v' valid for the context defined by the 
			-- function 'vf'
			-- Function 'vf' is called for each character in 'v', with 
			-- arguments of the character and the character position.
			-- If 'ef' is True then an empty string is valid, else not
		local
			i, lim: INTEGER
		do
			if v /= Void then
				Result := (not v.is_empty) or ef
				lim := v.count
				from i := 1
				invariant
					lim = v.count
				until i > lim or not Result
				loop
					vf.call ([v.item (i), i])
					Result := vf.last_result
					i := i + 1
				variant
					(lim - i) + 1
				end
			end
		end

	--|--------------------------------------------------------------

	string_conforms (
		v: detachable STRING;
		f1: FUNCTION [CHARACTER, BOOLEAN];
		fn: FUNCTION [CHARACTER, BOOLEAN];
		cln: ARRAY [CHARACTER]): BOOLEAN
			-- Does string 'v' conform to the context defined by the
			-- remaining arguments?
			--
			-- Function 'f1' is called for the first character in 'v'
			-- The result of calling 'f1' must be True for 'v' to conform
			--
			-- Function 'fn' is called for all characters in 'v' after 
			-- the first character.  If the result of calling 'fn' is 
			-- True, then (for that character) 'v' conforms.
			-- If the result of calling 'fn' is False, then IF the
			-- the character array 'cln' is non-void and non-empty, and
			-- then if the current character in 'v' is included in the
			-- character array, then 'v' conforms (for that character)
		require
			string_exists: v /= Void
			function_1_exists: f1 /= Void
			function_n_exists: fn /= Void
		local
			i, lim: INTEGER
		do
			if v /= Void then
				Result := not v.is_empty and then f1.item ([v.item(1)])
				lim := v.count
				from i := 2
				invariant
					lim = v.count
				until i > lim or not Result
				loop
					if not fn.item ([v.item (i)]) then
						if not cln.has (v.item (i)) then
							Result := False
						end
					end
					i := i + 1
				variant
					(lim - i) + 1
				end
			end
		end

--|========================================================================
feature -- Content checking
--|========================================================================

	string_has_characters (v: STRING; ca: ARRAY [CHARACTER]): BOOLEAN
		-- Does the given string 'v' contain any of the characters in
		-- the collection 'ca'?
		require
			string_exists: v /= Void
			characters_exist: ca /= Void
		local
			i, lim: INTEGER
		do
			lim := ca.upper
			from i := ca.lower
			until Result or ( i > lim)
			loop
				Result := v.has (ca.item (i))
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	substring_occurrences (str, sub: STRING): INTEGER
			-- Number of occurences, in 'str' of substring 'sub'
		require
			string_exists: str /= Void
			substring_exists: sub /= Void
		local
			searcher: STRING_SEARCHER
			sp, lim: INTEGER
		do
			create {STRING_8_SEARCHER} searcher.make
			lim := str.count
			from sp := 1
			until sp > lim
			loop
				sp := searcher.substring_index (str, sub, sp, lim)
				if sp = 0 then
					-- Not found
					sp := lim + 1
				else
					Result := Result + 1
					sp := sp + sub.count
				end
			end
		end

	--|--------------------------------------------------------------

	substring_matches (str, ss: STRING; spos: INTEGER): BOOLEAN
			-- Does string 'str' have substring 'ss' starting at 
			-- position 'spos'?
		require
			exists: str /= Void and then not str.is_empty
			valid_substring: ss /= Void and then not ss.is_empty
			valid_start: spos > 0 and spos <= str.count
		do
			Result := pfsr.substring_matches (str, ss, spos)
		end

 --|========================================================================
feature -- Integer to string Conversion
 --|========================================================================

	integer_32_to_hex (v: INTEGER; decorated: BOOLEAN): STRING
			-- Convert the given integer into its hexadecimal notation
			-- Because Eiffel integers are always signed, a negative
			-- value must be tweeked to derive the correct hex value.
		local
			tmps: STRING
			tv, tm: INTEGER
		do
			if decorated then
				Result := "0x"
			else
				create Result.make (8)
			end

			if v = 0 then
				Result.extend ('0')
			elseif v < 0 then
				Result.append (aprintf ("%%x", << v >>))
			else
				-- Build the result string backwards, then flip it

				create tmps.make (16)
				from tv := v
				until tv = 0
				loop
					tm := tv \\ 16
					tmps.extend (Ks_hex_digits.item (tm + 1))
					tv := tv // 16
				end
				Result.append (tmps.mirrored)
			end
		end

	--|--------------------------------------------------------------

	natural_32_to_hex (v: INTEGER; decorated: BOOLEAN): STRING
			-- Convert the given natural_32 into its hexadecimal notation
		do
			if decorated then
				create Result.make (10)
				Result.append ("0x")
			else
				create Result.make (8)
			end
			Result.append (v.to_hex_string)
		end

	--|--------------------------------------------------------------

	integer_to_hex_digit (v: INTEGER): CHARACTER
		require
			in_range: v >= 0 and v <= 15
		do
			Result := Ks_hex_digits.item (v + 1)
		end

	--|--------------------------------------------------------------

	integer_32_to_octal_str (v: INTEGER; decorated: BOOLEAN): STRING
			-- Convert the given integer into its octal notation
		local
			tmps: STRING
			tv, tm: INTEGER
		do
			if decorated then
				Result := "0"
			else
				create Result.make (12)
			end

			if v = 0 then
				Result.extend ('0')
			elseif v < 0 then
				Result.append (aprintf ("%%o", << v >>))
			else
				-- Build the result string backwards, then flip it

				create tmps.make (16)
				from tv := v
				until tv = 0
				loop
					tm := tv \\ 8
					tmps.extend (Ks_octal_digits.item (tm + 1))
					tv := tv // 8
				end
				Result.append (tmps.mirrored)
			end
		end

	--|--------------------------------------------------------------

	integer_32_to_binary (num: INTEGER): STRING
		local
			buf: STRING
			idx: INTEGER
		do
			create buf.make (44)  -- 32 for digits, 11 for blanks
			Result := buf

			from idx := 31
			until idx = 27
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append (" ")
			from
			until idx = 23
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append ("  ")
			from
			until idx = 19
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append (" ")
			from
			until idx = 15
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append ("   ")
			from
			until idx = 11
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append (" ")
			from
			until idx = 7
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append ("  ")
			from
			until idx = 3
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end

			buf.append (" ")
			from
			until idx < 0
			loop
				buf.append_integer (bitval_at (num, idx))
				idx := idx - 1
			end
		end

	--|--------------------------------------------------------------

	octet_to_binary (v: NATURAL_8): STRING
		local
			buf: STRING
			idx: INTEGER
		do
			create buf.make (8)
			Result := buf

			from idx := 7
			until idx < 0
			loop
				if v.bit_test (idx) then
					buf.extend ('1')
				else
					buf.extend ('0')
				end
				idx := idx - 1
			end
		end

 --|========================================================================
feature -- String to Integer Conversion
 --|========================================================================

	decimal_string_to_int (v: STRING): INTEGER
			-- Convert the given string, assumed to be decimal,
			-- into its integer value
		local
			idx, lim: INTEGER
			c: CHARACTER
			has_error: BOOLEAN
			is_negative: BOOLEAN
			pos1: INTEGER
		do
			if v /= Void and then not v.is_empty then
				c := v.item (1)
				if c = '-' then
					is_negative := True
					pos1 := 2
				elseif c = '+' then
					pos1 := 2
				else
					pos1 := 1
				end
				lim := v.count
				from idx := pos1
				until has_error or idx > lim
				loop
					c := v.item (idx)
					if c.is_digit then
						Result := (Result * 10) + (c |-| '0')
					else
						has_error := True
					end
					idx := idx + 1
				end
				if is_negative then
					Result := -Result
				end
			end
			if has_error then
				Result := 0
			end
		end

	--|--------------------------------------------------------------

	decimal_character_to_int (c: CHARACTER): INTEGER
			-- Integer represented by digit character 'c'
			-- Negative 1 (-1) if not a valid digit
		do
			if not c.is_digit then
				Result := -1
			else
				Result := c |-| '0'
			end
		end

	--|--------------------------------------------------------------

	decimal_string_to_integer_64 (v: STRING): INTEGER_64
		local
			dv, mag, pp: INTEGER_64
			tv, i, lim: INTEGER
			c: CHARACTER
		do
			lim := v.count
			mag := 1
			from i := lim
			invariant lim = v.count
			until i = 0
			loop
				c := v.item (i)
				if c.is_digit then
					tv := v.item (i).out.to_integer
					dv := tv.to_integer_64
					pp := dv * mag
					Result := Result + pp
					mag := mag * 10
				end
				i := i - 1
			variant i + 1
			end
		end

	--|--------------------------------------------------------------

	binary_string_to_natural_64 (num: STRING): NATURAL_64
			-- Given the binary string of up to 64 bits of the form:
			-- dddd_dddd
			-- with underscores optional and <d> being either '1' or '0'
			-- convert into a 64 bit unsigned integer (natural) value
		require
			arg_exits: num /= Void
		local
			buf: STRING
			c: CHARACTER
			ti: NATURAL_64
			idx, lim, pos: INTEGER
		do
			--    create buf.make_shared (num)
			buf := num
			lim := buf.count
			ti := 1
			from
				idx := 0
				pos := lim
			until idx > 63 or pos = 0
			loop
				c := buf.item (pos)
				if c = '1' then
					Result := Result + ti.bit_shift_left (idx)
					idx := idx + 1
				elseif c = '0' then
					idx := idx + 1
				elseif c = '_' then
					-- Ignore
				else
					-- ERROR
				end
				pos := pos - 1
			end
		end

	--|--------------------------------------------------------------

	binary_string_to_int32 (num: STRING): INTEGER_32
			-- Given the binary string of up to 32 bits fo the form:
			-- dddd dddd  dddd dddd   dddd dddd  dddd dddd
			-- with blanks optional and <d> being either '1' or '0'
			-- convert into a 32 bit integer value
		require
			arg_exits: num /= Void
		local
			buf: STRING
			c: CHARACTER
			ti, idx, lim, pos: INTEGER
		do
			--    create buf.make_shared (num)
			buf := num
			lim := buf.count
			ti := 1
			from
				idx := 0
				pos := lim
			until idx > 31 or pos = 0
			loop
				c := buf.item (pos)
				if c = '1' then
					--Result := Result + left_shift (1, idx)
					Result := Result + ti.bit_shift_left (idx)
					idx := idx + 1
				elseif c = '0' then
					idx := idx + 1
				else
					-- must be a blank
				end
				pos := pos - 1
			end
		end

	--|--------------------------------------------------------------

	binary_string_to_natural_8 (num: STRING): NATURAL_8
			-- Given the binary string of up to 8 bits fo the form:
			--   dddddddd
			-- with <d> being either '1' or '0', convert to 8 bit value
		require
			arg_exits: num /= Void
			short_enough: num.count <= 8
		local
			pos: INTEGER
		do
			from pos := 1
			until pos > num.count
			loop
				if num.item (pos) = '1' then
					Result := Result + 1
				end
				pos := pos + 1
				if pos <= num.count then
					Result := Result.bit_shift_left (1)
				end
			end
		end

 --|========================================================================
feature -- Comparison
 --|========================================================================

	strings_are_equal_case_insensitive (s1, s2: STRING): BOOLEAN
		require
			strings_exists: s1 /= Void and s2 /= Void
		do
			Result := s1.is_case_insensitive_equal (s2)
		end

	strings_are_same_case_insensitive (v1, v2: detachable STRING): BOOLEAN
			-- Are strings 'v1' and 'v2' the same, case-insensitive?
			-- True if BOTH are Void
			-- OR, BOTH are non-Void AND have same values when
			-- coerced to same case
		do
			if v1 = Void and v2 = Void then
				Result := True
			elseif attached v1 as ts1 and attached v2 as ts2 then
				Result := ts1.is_case_insensitive_equal (ts2)
			end
		end

	--|--------------------------------------------------------------

	is_less_than_case_insensitive (s1, s2: STRING): BOOLEAN
			-- Is s1 less than s2 alphabetically (case-insensitive)?
		require
			strings_exists: s1 /= Void and s2 /= Void
		local
			t1, t2: STRING
		do
			if s1 /= s2 then
				t1 := s1.as_lower
				t2 := s2.as_lower
				Result := t1 < t2
			end
		end

	--|--------------------------------------------------------------

	is_greater_than_case_insensitive (s1, s2: STRING): BOOLEAN
			-- Is s1 greater than s2 alphabetically (case-insensitive)?
		require
			strings_exists: s1 /= Void and s2 /= Void
		local
			t1, t2: STRING
		do
			if s1 /= s2 then
				t1 := s1.as_lower
				t2 := s2.as_lower
				Result := t1 > t2
			end
		end

	--|--------------------------------------------------------------

	substring_starts_with (s, ss: STRING; spos: INTEGER): BOOLEAN
			-- Does the substring in 's', begining at 'spos', start
			-- with 'ss'
		require
			stream_exists: s /= Void
			valid_start: spos > 0 and spos <= s.count
			valid_pattern: ss /= Void and then not ss.is_empty
		local
			i, lim: INTEGER
		do
			if s.count >= spos + ss.count - 1 then
				lim := s.count.min (ss.count)
				Result := True
				from i := 1
				until i > lim or not Result
				loop
					if s.item (spos + i - 1) /= ss.item (i) then
						Result := False
					else
						i := i + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	strings_deviation_point (s1, s2: READABLE_STRING_GENERAL): INTEGER
			-- Index, in 's1' where 's2' deviates from 's1'
			-- If same, Result is zero
		require
			strings_exists: s1 /= Void and s2 /= Void
		local
			i, lim: INTEGER
		do
			lim := s1.count.min (s2.count)
			-- Skip comparison if the same string
			if s1 /= s2 then
				from i := 1
				until i > lim or Result /= 0
				loop
					if s1.item (i) /= s2.item (i) then
						-- deviation
						Result := i
					else
						i := i + 1
					end
				end
				if Result = 0 and s1.count /= s2.count then
					Result := lim + 1
				end
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	non_void_string (v: detachable STRING): STRING
			-- If 'v' is of type STRING and attached, then 'v'
			-- else if 'v' is attached then 'v'.string
			-- else a new empty string
		do
			create Result.make (0)
			if v /= Void then
				if Result.same_type (v) then
					-- An ordinary string, return itself
					Result := v
				else
					Result := v.string
				end
			end
		ensure
			exists: Result /= Void
			same_value: v /= Void implies v ~ Result
		end

	--|--------------------------------------------------------------

	hex_string_to_integer (str: STRING): INTEGER
			-- The INTEGER value of the given hexadecimal string
			-- (with optional "0x" prefix)
		require
			is_hex_int: string_is_hex_integer (str)
		do
			Result := hex_substring_to_integer (str, 1, str.count)
		end

	obs_hex_string_to_integer (str: STRING): INTEGER
			-- The INTEGER value of the given hexadecimal string
			-- (with optional "0x" prefix)
		require
			is_hex_int: string_is_hex_integer (str)
		local
			tv, idx, lim: INTEGER
			c: CHARACTER
			is_neg: BOOLEAN
		do
			lim := str.count
			if str.item (1) = '-' then
				is_neg := True
				idx := idx + 1
			end
			if lim > (idx + 2) and then
				(str.item(idx+1) = '0') and (str.item(idx+2).lower = 'x')
			 then
				 -- Skip over the "0x"
				 idx := idx + 2
			end
			from idx := idx + 1
			until idx > lim
			loop
				c := str.item (idx)
				-- Precondition asserts that the chars are hex digits
				-- They can be either decimal digits or hex chars
				if c.is_digit then
					tv := (c |-| '0')
				else
					tv := (c.upper |-| 'A') + 10
				end
				Result := (Result * 16) + tv
				idx := idx + 1
			end
			if is_neg then
				Result := -Result
			end
		end

	--|--------------------------------------------------------------

	hex_substring_to_integer (str: STRING; sp, ep: INTEGER): INTEGER
			-- The INTEGER value of the given hexadecimal substring,
			-- from sp to ep (with optional "0x" prefix)
		require
			valid_start: sp > 0 and sp <= str.count
			valid_end: ep >= sp and ep <= str.count
			is_hex_int: string_is_hex_integer (str.substring (sp, ep))
		local
			tv, idx, lim: INTEGER
			c: CHARACTER
			is_neg: BOOLEAN
		do
			idx := sp - 1
			lim := ep
			if str.item (sp) = '-' then
				is_neg := True
				idx := idx + 1
			end
			if lim > (idx + 2) and then
				(str.item(idx+1) = '0') and (str.item(idx+2).lower = 'x')
			 then
				 -- Skip over the "0x"
				 idx := idx + 2
			end
			from idx := idx + 1
			until idx > lim
			loop
				c := str.item (idx)
				-- Precondition asserts that the chars are hex digits
				-- They can be either decimal digits or hex chars
				if c.is_digit then
					tv := (c |-| '0')
				else
					tv := (c.upper |-| 'A') + 10
				end
				Result := (Result * 16) + tv
				idx := idx + 1
			end
			if is_neg then
				Result := -Result
			end
		end

 --|--------------------------------------------------------------

	hex_string_to_natural_32 (str: STRING): NATURAL_32
			-- The NATURAL_32 value of the given hexadecimal string
			-- (with optional "0x" prefix)
		require
			is_hex_int: string_is_hex_integer (str)
		local
			idx, lim: INTEGER
			tv: NATURAL_32
			c: CHARACTER
		do
			lim := str.count
			idx := 1
			if lim > 2 and then
				(str.item(1) = '0') and (str.item(2).lower = 'x')
			 then
				 -- Skip over the "0x"
				 idx := 2
			end
			from
			until idx > lim
			loop
				c := str.item (idx)
				-- Precondition asserts that the chars are hex digits
				-- They can be either decimal digits or hex chars
				if c.is_digit then
					tv := (c |-| '0').to_natural_32
				else
					tv := ((c.upper |-| 'A') + 10).to_natural_32
				end
				Result := (Result * 16) + tv
				idx := idx + 1
			end
		end

 --|--------------------------------------------------------------

	hex_string_to_natural_64 (str: STRING): NATURAL_64
			-- The NATURAL_64 (unsigned long long) value of the
			-- given hexadecimal string
			-- (with optional "0x" prefix)
		require
			is_hex_int: string_is_hex_integer (str)
		do
			Result := hex_substring_to_natural_64 (str, 1, str.count)
		end

	--|--------------------------------------------------------------

	hex_substring_to_natural_64 (str: STRING; sp, ep: INTEGER): NATURAL_64
			-- The NATURAL_64 value of the given hexadecimal substring,
			-- from sp to ep (with optional "0x" prefix)
		require
			valid_start: sp > 0 and sp <= str.count
			valid_end: ep >= sp and ep <= str.count
			is_hex_int: string_is_hex_integer (str.substring (sp, ep))
		local
			tv, idx, lim: INTEGER
			c: CHARACTER
		do
			idx := sp - 1
			lim := ep
			if lim > (idx + 2) and then
				(str.item(idx+1) = '0') and (str.item(idx+2).lower = 'x')
			 then
				 -- Skip over the "0x"
				 idx := idx + 2
			end
			from idx := idx + 1
			until idx > lim
			loop
				c := str.item (idx)
				-- Precondition asserts that the chars are hex digits
				-- They can be either decimal digits or hex chars
				if c.is_digit then
					tv := (c |-| '0')
				else
					tv := (c.upper |-| 'A') + 10
				end
				Result := (Result * 16) + tv.to_natural_64
				idx := idx + 1
			end
		end

	--|--------------------------------------------------------------

	octal_string_to_integer (str: STRING): INTEGER
			-- The INTEGER value of the given Octal string
			-- (with optional leading "0")
		require
			is_octal_int: is_octal_integer (str)
		local
			tv, idx, lim: INTEGER
			c: CHARACTER
			is_neg: BOOLEAN
		do
			lim := str.count
			if str.item (1) = '-' then
				is_neg := True
				idx := idx + 1
			end
			from idx := idx + 1
			until idx > lim
			loop
				c := str.item (idx)
				tv := (c |-| '0')
				Result := (Result * 8) + tv
				idx := idx + 1
			end
			if is_neg then
				Result := -Result
			end
		end

--|========================================================================
feature -- Path and file name manipulation
--|========================================================================

	basename (v: STRING): STRING
			-- Filename component of a pathname, using the current
			-- operating environment's default delimiter
		require
			exists: v /= Void and then not v.is_empty
		local
			oe: OPERATING_ENVIRONMENT
			sep: CHARACTER
		do
			create oe
			sep := oe.directory_separator
			Result := basename_by_delimiter (v, sep)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	ux_basename (v: STRING): STRING
			-- Like basename, but always use a forward slash as delimiter
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := basename_by_delimiter (v, '/')
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	basename_by_delimiter (v: STRING; sep: CHARACTER): STRING
			-- Filename component of a pathname, using the given
			-- delimiter intead of the operating environment's default
		require
			exists: v /= Void and then not v.is_empty
		local
			spos: INTEGER
			ts: STRING
		do
			spos := v.last_index_of (sep, v.count)
			if spos < v.count then
				ts := v.substring (spos + 1, v.count)
			else
				ts := v.twin
			end
			create Result.make_from_string (ts)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	smart_basename (v: STRING): STRING
			-- Rightmost path component, where components are separated
			-- by either a forward or a back slash ('/' or '\')
			-- The last such character found is considered the separator
		require
			exists: v /= Void and then not v.is_empty
		local
			p1, p2: INTEGER
		do
			p1 := v.last_index_of ('/', v.count)
			p2 := v.last_index_of ('\', v.count)
			if p1 /= 0 then
				Result := basename_by_delimiter (v, '/')
			else
				Result := basename_by_delimiter (v, '\')
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	suffix (fn: STRING): STRING
			-- filename suffix of 'fn', if any
		require
			exists: fn /= Void
		local
			spos: INTEGER
		do
			spos := fn.last_index_of ('.',fn.count)
			if spos = 0 then
				Result := ""
			else
				Result := fn.substring (spos + 1, fn.count)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	root_name (fn: STRING): STRING
			-- filename root of 'fn', less suffix, if any
		require
			exists: fn /= Void
		local
			spos: INTEGER
		do
			spos := fn.last_index_of ('.',fn.count)
			if spos = 0 then
				Result := ""
			else
				Result := fn.substring (1, spos - 1)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	dirname (v: STRING): STRING
			-- Directory name component of a pathname, using the current
			-- operating environment's default delimiter
		local
			oe: OPERATING_ENVIRONMENT
			ds: CHARACTER
		do
			create oe
			ds := oe.directory_separator
			Result := dirname_by_delimiter (v, ds)
		end

	--|--------------------------------------------------------------

	ux_dirname (v: STRING): STRING
			-- Like dirname, but always use a forward slash as delimiter
		do
			Result := dirname_by_delimiter (v, '/')
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	smart_dirname (v: STRING): STRING
			-- Directory name component of a pathname, where components
			-- are separated by either a forward or a back slash ('/' or '\')
			-- The last such character found is considered the separator
		require
			exists: v /= Void and then not v.is_empty
		local
			p1, p2: INTEGER
		do
			p1 := v.last_index_of ('/', v.count)
			p2 := v.last_index_of ('\', v.count)
			if p1 /= 0 then
				Result := dirname_by_delimiter (v, '/')
			else
				Result := dirname_by_delimiter (v, '\')
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	dirname_by_delimiter (v: STRING; sep: CHARACTER): STRING
			-- Directory name component of a pathname, using the given
			-- delimiter intead of the operating environment's default
		require
			exists: v /= Void and then not v.is_empty
		local
			pos: INTEGER
			ts: STRING
		do
			pos := v.last_index_of (sep, v.count)
			if pos = 0 then
				ts := "."
			else
				ts := v.substring (1, pos - 1)
			end
			create Result.make_from_string (ts)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_dot (v: STRING): BOOLEAN
			-- Is the given string either "." or ".."?
			-- Denotes current and parent directory
		require
			exists: v /= Void
		do
			if not v.is_empty and then v.item (1) = '.' then
				if v.count = 1 then
					Result := True
				elseif v.count = 2 and then v.item (2) = '.' then
					Result := True
				end
			end
		end

	--|--------------------------------------------------------------

	path_from_env_path (v: STRING): PATH
			-- Path described by path string 'v', where one or more 
			-- segments in 'v' are environment variables
			-- e.g. "$PROJECTS/p1/foo.e" -> "c:/myprojects/active/p1/foo.e"
			-- e.g. "./$CONFIG" -> ./myconfig.dat"
		local
			ts: STRING
		do
			if not v.has ('$') then
				create Result.make_from_string (v)
			else
				ts := path_string_from_env (v)
				create Result.make_from_string (ts)
			end
		end

	--|--------------------------------------------------------------

	path_string_from_env (v: STRING): STRING
			-- Path string described by path string 'v', where one
			-- or more segments in 'v' are environment variables
			-- e.g. "$PROJECTS\p1\foo.e" -> "c:\myprojects\active\p1\foo.e"
			-- e.g. "./$CONFIG" -> ./myconfig.dat"
		local
			oe: OPERATING_ENVIRONMENT
			dsc: CHARACTER
		do
			create oe
			dsc := oe.directory_separator
			if not v.has (dsc) then
				if dsc = '\' then
					-- oe is Windows; accepts fwd slashes also
					dsc := '/'
				end
			end
			Result := segmented_path_from_env (v, dsc)
		end

	--|--------------------------------------------------------------

	url_from_env (v: STRING): STRING
			-- URL described by string 'v', where one or more 
			-- segments in 'v' are environment variables
			-- e.g. "$EIF_URL/welcome" -> "www.eiffel.or/welcome"
			--
			-- N.B. Replaces ONLY whole segments starting with '$'
		do
			Result := segmented_path_from_env (v, '/')
		end

	--|--------------------------------------------------------------

	segmented_path_from_env (v: STRING; dsc: CHARACTER): STRING
			-- Path, URL and similar segmented string, as described
			-- by string 'v', where one or more segments in 'v' are
			-- environment variables
			-- e.g. "$EIF_URL/welcome" -> "www.eiffel.or/welcome"
			-- e.g. "$PROJECTS\p1\foo.e" -> "c:\myprojects\active\p1\foo.e"
			-- e.g. "./$CONFIG" -> ./myconfig.dat"
			--
			-- N.B. Replaces ONLY whole segments starting with '$'
		local
			ts, ps: STRING
			sl: LIST [STRING]
			xe: EXECUTION_ENVIRONMENT
		do
			if not v.has ('$') then
				Result := v
			else
				create xe
				sl := v.split (dsc)
				create ts.make (v.count)
				from sl.start
				until sl.after
				loop
					ps := sl.item
					if not ps.is_empty and then ps.item (1) = '$' then
						if attached xe.item (ps.substring (2, ps.count)) as er then
							ps := er
						end
					end
					if not ts.is_empty then
						ts.extend (dsc)
					end
					ts.append (ps)
					sl.forth
				end
				Result := ts
			end
		end

--|========================================================================
feature -- String search
--|========================================================================

	character_is_whitespace (c: CHARACTER; nl_ok: BOOLEAN): BOOLEAN
		do
			Result := pfsr.character_is_whitespace (c, nl_ok)
		end

	is_quote (v: CHARACTER): BOOLEAN
			-- Is 'v' a single or double quote?
		do
			Result := v = '%'' or v = '%"'
		end

	--|--------------------------------------------------------------

	index_of_next_non_whitespace (str: STRING; spos: INTEGER;
	                              nl_is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of first non-whitespace character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If nl_is_ws, treat newlines as whitespace.
			-- If no whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		do
			Result := pfsr.index_of_next_space_yn (
				str, spos, str.count, nl_is_ws, False)
		end

	--|--------------------------------------------------------------

	index_of_next_non_tab (str: STRING; spos: INTEGER): INTEGER
			-- Index, within string 'str' of first non-TAB character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If no whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		do
			Result := index_of_next_non_char (str, '%T', spos)
		end

	--|--------------------------------------------------------------

	index_of_next_non_char (str: STRING; mc: CHARACTER; sp: INTEGER): INTEGER
			-- Index, within string 'str' of first character that is not 
			-- the same at 'mc' at or AFTER position 'spos' (searching
			-- forward from spos).
			-- If no non-matching char is found, then Result is 0
		require
			string_exists: str /= Void
			valid_startpos: sp > 0 and sp <= str.count
		local
			idx: INTEGER
			c: CHARACTER
		do
			from idx := sp
			until Result > 0 or idx > str.count
			loop
				c := str.item (idx)
				if c /= mc then
					Result := idx
				else
					idx := idx + 1
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_next_whitespace (str: STRING; spos: INTEGER;
	                          nl_is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of next whitespace character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If ns_is_ws, treat newlines as whitespace.
			-- If no whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_start: spos > 0 and spos <= str.count
		do
			Result := pfsr.index_of_next_space_yn (
				str, spos, str.count, nl_is_ws, True)
		end

	--|--------------------------------------------------------------

	index_of_next_space_yn (
		str: STRING; spos: INTEGER; nlf, is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of next (non)whitespace character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If nlf, treat newlines as whitespace.
			-- If no (non)whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_start: spos > 0 and spos <= str.count
		do
			Result := pfsr.index_of_next_space_yn (
				str, spos, str.count, nlf, is_ws)
		end

	--|--------------------------------------------------------------

	index_of_prev_non_whitespace (str: STRING; spos: INTEGER;
	                              nl_is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of first non-whitespace character
			-- at or BEFORE position 'spos' (searching backward from spos).
			-- If ns_is_ws, treat newlines as whitespace.
			-- If no non-whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		do
			Result := index_of_prev_space_yn (str, spos, nl_is_ws, False)
		end

	--|--------------------------------------------------------------

	index_of_prev_whitespace (str: STRING; spos: INTEGER;
	                              nl_is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of first whitespace character
			-- at or BEFORE position 'spos' (searching backward from spos).
			-- If ns_is_ws, treat newlines as whitespace.
			-- If no whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		do
			Result := index_of_prev_space_yn (str, spos, nl_is_ws, True)
		end

	--|--------------------------------------------------------------

	index_of_prev_space_yn (
		str: STRING; spos: INTEGER; nlf, is_ws: BOOLEAN): INTEGER
			-- Index, within string 'str' of previous (non)whitespace character
			-- at or BEFORE position 'spos' (searching backward from spos).
			-- If nlf, treat newlines as whitespace.
			-- If no (non)whitespace is found, then Result is 0
		require
			string_exists: str /= Void
			valid_start: spos > 0 and spos <= str.count
		local
			idx: INTEGER
			c: CHARACTER
		do
			from idx := spos
			until Result > 0 or idx = 0
			loop
				c := str.item (idx)
				if is_ws and character_is_whitespace (c, nlf) then
					Result := idx
				elseif (not is_ws) and not character_is_whitespace (c, nlf) then
					Result := idx
				else
					idx := idx - 1
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_next_non_alnum (str: STRING;
	                         spos: INTEGER; uscore: BOOLEAN): INTEGER
			-- Index, within string 'str' of first non-alphanumeric character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If uscore, treat underscores as alphanumeric.
			-- If none is found, then Result is 0.
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		local
			idx: INTEGER
			c: CHARACTER
		do
			from idx := spos
			until Result > 0 or idx > str.count
			loop
				c := str.item(idx)
				if isalnum (c, uscore) then
					idx := idx + 1
				else
					Result := idx
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_next_non_alnum_plus (
		str: STRING;
		spos: INTEGER; extras: ARRAY [CHARACTER]): INTEGER
			-- Index, within string 'str' of first character at or AFTER
			-- position 'spos' (searching forward from spos) that is 
			-- neither  alphanumeric nor among the character in 'extras'
			-- If none is found, then Result is 0.
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
			extras_exist: extras /= Void
		local
			idx: INTEGER
			c: CHARACTER
		do
			from idx := spos
			until Result > 0 or idx > str.count
			loop
				c := str.item(idx)
				if c.is_alpha_numeric or else extras.has (c) then
					idx := idx + 1
				else
					Result := idx
				end
			end
		end

	--|--------------------------------------------------------------

	index_of_next_alnum (str: STRING;
	                     spos: INTEGER; uscore: BOOLEAN): INTEGER
			-- Index, within string 'str' of first alphanumeric character
			-- at or AFTER position 'spos' (searching forward from spos).
			-- If uscore, treat underscores as alphanumeric.
			-- If none is found, then Result is 0.
		require
			string_exists: str /= Void
			valid_startpos: spos > 0 and spos <= str.count
		local
			idx: INTEGER
			c: CHARACTER
		do
			from idx := spos
			until Result > 0 or idx > str.count
			loop
				c := str.item (idx)
				if isalnum (c, uscore) then
					Result := idx
				else
					idx := idx + 1
				end
			end
		end

 --|--------------------------------------------------------------

	old_index_of_next (ch: CHARACTER; v: STRING; spos: INTEGER): INTEGER
			-- Index, in given string beginning at spos, of UNQUOTED character 'ch'
		require
			exists: v /= Void
		local
			q1, q2: BOOLEAN
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			from i := spos
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = '%'' then
					-- single ' fouls up editor indentation and highlighting
					if not q2 then
						if q1 then
							q1 := False
						else
							q1 := True
						end
					end
				elseif c = '%"' then
					-- What if we're looking for the next quote??
					if not q1 then
						if q2 then
							q2 := False
						else
							q2 := True
						end
					end
				elseif c = ch then
					if not (q1 or q2) then
						Result := i
					end
				end
				i := i + 1
			end
		end

 --|--------------------------------------------------------------

	index_of_next (ch: CHARACTER; v: STRING; spos: INTEGER): INTEGER
			-- Index, in given string beginning at spos, of UNQUOTED character 
			-- 'ch'

-- DO NOT USE UNLESS YOU MEAN IT!!!!!!!!!!!!
		require
			exists: v /= Void
		local
			q1, q2: BOOLEAN
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			from i := spos
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = '%'' then
					-- single ' fouls up editor indentation and highlighting
					if not q2 then
						if q1 then
							q1 := False
						else
							q1 := True
						end
					end
				elseif c = '%"' then
					-- What if we're looking for the next quote??
					if not q1 then
						if q2 then
							q2 := False
						else
							q2 := True
						end
					end
				elseif c = ch then
					if not (q1 or q2) then
						Result := i
					end
				end
				i := i + 1
			end
		end

 --|--------------------------------------------------------------

	index_of_next_unquoted (ch: CHARACTER; v: STRING; spos: INTEGER): INTEGER
			-- Index, in given string beginning at spos, of UNQUOTED character 'ch'
		require
			exists: v /= Void
		local
			q1, q2: BOOLEAN
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			from i := spos
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = '%'' then
					-- single ' fouls up editor indentation and highlighting
					if not q2 then
						if q1 then
							q1 := False
						else
							q1 := True
						end
					end
				elseif c = '%"' then
					-- What if we're looking for the next quote??
					if not q1 then
						if q2 then
							q2 := False
						else
							q2 := True
						end
					end
				elseif c = ch then
					if not (q1 or q2) then
						Result := i
					end
				end
				i := i + 1
			end
		end

	index_of_next_unquoted2 (ch: CHARACTER; v: STRING; spos: INTEGER): INTEGER
			-- Index, in given string beginning at spos, of UNQUOTED character 
			-- 'ch', where ONLY double-quotes count
		require
			exists: v /= Void
		local
			q2: BOOLEAN
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			from i := spos
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = '%"' then
					-- What if we're looking for the next quote??
					if q2 then
						q2 := False
					else
						q2 := True
					end
				elseif c = ch then
					if not q2 then
						Result := i
					end
				end
				i := i + 1
			end
		end

 --|--------------------------------------------------------------

	index_of_next_digit (v: STRING; spos: INTEGER): INTEGER
			-- Index, in 'v', beginning at spos, of next UNQUOTED digit
		require
			exists: v /= Void
		local
			q1, q2: BOOLEAN
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			from i := spos
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = ''' then
					-- single ' fouls up editor indentation and highlighting
					if not q2 then
						if q1 then
							q1 := False
						else
							q1 := True
						end
					end
				elseif c = '%"' then
					if not q1 then
						if q2 then
							q2 := False
						else
							q2 := True
						end
					end
				elseif c.is_digit then
					if not (q1 or q2) then
						Result := i
					end
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	index_of_matching_close (v: STRING; sp, ep: INTEGER; esc: CHARACTER):INTEGER
			-- Index, in 'v', of the unescaped closing character that
			-- matches the opening character at 'sp'.
			-- If v.item (sp) is not an opening character, then error,
			-- in which case Result = -1
			-- If opening char and escape are the same, they must be 
			-- double quotes, else an error, and Result = -1
			-- If not found at or before position 'ep', then Result = 0
		require
			exists: v /= Void
			valid_start: sp >= 0 and sp <= v.count
		local
			i, lim, ecp: INTEGER
			ccount: INTEGER
			esame: BOOLEAN
			c, oc, cc: CHARACTER
		do
			-- Grab opening char and determine closing char
			oc := v.item (sp)
			inspect oc
			when '%"' then
				cc := '%"'
			when '%'' then
				cc := '%''
			when '(' then
				cc := ')'
			when '[' then
				cc := ']'
			when '{' then
				cc := '}'
			when '<' then
				cc := '>'
			else
				Result := -1
			end
			if oc = esc then
				if oc /= '%"'  then
					-- Don't do it; Only allow dbl quotes as terminals and 
					-- escapes in the same stream
					Result := -1
				else
					esame := True
				end
			end
			-- For every 'oc', add one
			-- For every 'cc' substract one
			-- When ccount = 0, done
			--
			-- When escape char is same as opening char,
			-- it's more interesting
			-- e.g.  "foo has ""embedded"" quotes"
			--       o        ec        ec       c
			-- Open, close and escape chars are all the same
			-- There needs to be net zero, but that's not good enough
			-- e.g. "foo has ""embedded"" quotes"
			--      1        0
			-- But, when looking first for escapes, the picture improves
			-- e.g. "foo has ""embedded"" quotes"
			--      1        e         e        0
			-- By counting only the unescaped instances, and tracking 
			-- the escaped-ness state, the zero-sum logic should work
			ccount := 1
			lim := ep.min (v.count)
			from i := sp + 1
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = esc then
					-- Escape char
					if esame then
						-- Escape and oc are same and so MUST be dbl quotes,
						-- so too, the cc MUST be a dble quote
						-- Could be confusion.  Check for double char.  If 
						-- double, then really is escape, else it's not
						if i < lim and then v.item (i + 1) = oc then
							-- Is really an escape char
							-- Find closing escape pair
							ecp := v.substring_index ("%"%"", i)
							if ecp = 0 then
								-- Trouble, no matching end of escape seq
								Result := -1
							else
								-- Neither increment nor decrement count,
								-- just move past the escape seq
								i := ecp + 2
							end
						else
							-- Not actually an escape
							-- Count it as a close?
							ccount := ccount - 1
							if ccount = 0 then
								Result := i
							else
								i := i + 1
							end
						end
					else
						-- Not same, so behave "normally" for escape
						-- As c is an escape char, the next char is immune 
						-- to counting logic, no matter what it is
						-- Skip over escape and following char
						i := i + 2
					end
				else
					-- Not an escape char
					if c = cc then
						ccount := ccount - 1
					elseif c = oc then
						ccount := ccount + 1
					end
					if ccount = 0 then
						Result := i
					else
						i := i + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	old_index_of_matching_close (v: STRING; sp, ep: INTEGER; esc: CHARACTER):INTEGER
			-- Index, in 'v', of the unescaped closing character that
			-- matches the opening character at 'sp'.
			-- If v.item (sp) is not an opening character, then error,
			-- in which case Result = -1
			-- If not found at or before position 'ep', then Result = 0
		require
			exists: v /= Void
			valid_start: sp >= 0 and sp <= v.count
		local
			i, lim: INTEGER
			ccount: INTEGER
			ee: BOOLEAN
			c, oc, cc, pc: CHARACTER
		do
			oc := v.item (sp)
			inspect oc
			when '%"' then
				cc := '%"'
			when '%'' then
				cc := '%''
			when '(' then
				cc := ')'
			when '[' then
				cc := ']'
			when '{' then
				cc := '}'
			when '<' then
				cc := '>'
			else
				Result := -1
			end
			--| For every 'oc', add one
			--| For every 'cc' substract one
			--| When ccount = 0, done
			ccount := 1
			lim := ep.min (v.count)
			from i := sp + 1
			until i > lim or Result /= 0
			loop
				c := v.item (i)
				if c = esc then
					-- Escape char
					if pc = esc then
						-- Escaped escape, ignore and clear state
						--c := '%U'
						ee := True
					else
						-- Remember as pc (see bottom of loop)
						ee := False
					end
				elseif c = cc then
--					if pc /= esc and esc /= '%U' then
					if pc /= esc and not ee then
						ccount := ccount - 1
					end
					ee := False
				elseif c = oc then
--					if pc /= esc and esc /= '%U' then
					if pc /= esc and not ee then
						ccount := ccount + 1
					end
--					ee := not ee
					ee := False
				end
				if ccount = 0 then
					Result := i
				end
				pc := c
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	index_of_tag_end (v: STRING; spos: INTEGER): INTEGER
			-- Index, in given string beginning at spos, of the next
			-- unquoted '>' character
		require
			exists: v /= Void
		do
			Result := index_of_next ('>', v, spos)
		end

	--|--------------------------------------------------------------

	index_of_unescaped_char (
		v: STRING; ch, esc: CHARACTER; spos: INTEGER): INTEGER
			-- The next instance of character 'ch', in string 'v',
			-- starting at position spos, that is not preceded by
			-- an escape character 'esc'
			--
			-- Treat any escape characters inside of unescaped SINGLE
			-- quotes as literal
		require
			string_exists: v /= Void
			index_in_range: spos > 0 and spos <= v.count
		do
			Result := substring_index_of_unescaped_char (v, ch, esc, spos, v.count)
		end

	--|--------------------------------------------------------------

	substring_index_of_unescaped_char (
		v: STRING; ch, esc: CHARACTER; sp, ep: INTEGER): INTEGER
			-- The next instance of character 'ch', in string 'v',
			-- between start position sp and end position ep,
			-- that is not preceded by an escape character 'esc'
			--
			-- Treat any escape characters inside of unescaped SINGLE
			-- quotes as literal
		local
			idx, lim: INTEGER
			sgl_quote: BOOLEAN
			c, prevc: CHARACTER
		do
			lim := ep
			from idx := sp
			until idx > lim or (Result > 0)
			loop
				c := v.item (idx)
				if c = ch then
					if sgl_quote then
						-- treat as literal (skip it)
					else
						if (idx > sp) and then (prevc /= esc) then
							-- it's a match
							Result := idx
						elseif idx = sp then
							Result := idx
						end
					end
				elseif (c = '%'') and (prevc /= esc) then
					sgl_quote := not sgl_quote
				else
					-- not match so far
				end
				prevc := c
				idx := idx + 1
			end
		end

	--|--------------------------------------------------------------

	next_line (str: STRING; lp: CELL [INTEGER]): STRING
			-- Substring from the string 'str' that includes
			-- the characters up to but NOT including the next unescaped
			-- newline character.
			-- The line position is stored in 'lp' such that it denotes
			-- the read start position on entry and one character past
			-- the end position on exit.
			-- If there are no more characters between the starting
			-- line position and the end of the string, then Result
			-- is empty and the line position is set to zero.
			-- If the end of the string is found before a newline, then
			-- the characters from the starting point to the end of
			-- the string are returned and the line position set to
			-- one greater than the length of the string.
		require
			stream_exists: str /= Void
			position_exists: lp /= Void
			valid_start: lp.item > 0 and lp.item <= str.count
		local
			spos, epos: INTEGER
		do
			Result := ""
			spos := lp.item
			epos := index_of_next ('%N', str, spos)
			if epos = 0 then
				-- Not found
				epos := str.count + 1
			elseif epos = spos then
				-- An empty line (but a line just the same)
				lp.put (epos + 1)
			else
				-- Could be some crud at end of line
				-- One example is a %R, but a more exotic one is a %N%U pair
				-- %R%N
				if str.item (epos - 1) = '%R' then
					-- a CRLF situation; lose it
					Result := str.substring (spos, epos - 2)
				elseif str.item (epos - 1) = '%U' then
					-- a Null following a return; lose it
					Result := str.substring (spos, epos - 2)
				else
					Result := str.substring (spos, epos - 1)
				end
				lp.put (epos + 1)
			end
		ensure
			exists: Result /= Void
		end

	next_line_iq (str: STRING; lp: CELL [INTEGER]): STRING
			-- Substring from the string 'str' that includes
			-- the characters up to but not including the next
			-- newline character (quoted or NOT).
			-- The line position is stored in 'lp' such that it denotes
			-- the read start position on entry and one character past
			-- the end position on exit.
			-- If there are no more characters between the starting
			-- line position and the end of the string, then Result
			-- is empty and the line position is set to zero.
			-- If the end of the string is found before a newline, then
			-- the characters from the starting point to the end of
			-- the string are returned and the line position set to
			-- one greater than the length of the string.
		require
			stream_exists: str /= Void
			position_exists: lp /= Void
			valid_start: lp.item > 0 and lp.item <= str.count
		local
			spos, epos: INTEGER
			is_crlf: BOOLEAN
		do
			spos := lp.item
			epos := str.index_of ('%N', spos)
			if epos = 0 then
				-- Not found
				epos := str.count + 1
			end
			if epos = spos then
				-- An empty line (but a line just the same)
				Result := ""
				lp.put (epos + 1)
			else
				if str.item (str.count) = '%R' then
					-- a CRLF situation; lose it
					epos := epos - 1
					is_crlf := True
				end
				Result := str.substring (spos, epos - 1)
				if Result.is_empty then
					lp.put (0)
				else
					if is_crlf then
						lp.put (epos + 2)
					else
						lp.put (epos + 1)
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	next_non_empty_line (str: STRING; lp: CELL [INTEGER]): STRING
			-- Substring from the string 'str' that includes the
			-- characters beginning with the first non-newline,
			--  and up to but not including the next unescaped
			-- newline character.
			-- The line position is stored in 'lp' such that it denotes
			-- the read start position on entry and one character past
			-- the end position on exit.
			-- If there are no more characters between the starting
			-- line position and the end of the string, then Result
			-- is empty and the line position is set to zero.
			-- If the end of the string is found before a newline, then
			-- the characters from the starting point to the end of
			-- the string are returned and the line position set to
			-- one greater than the length of the string.
		require
			stream_exists: str /= Void
			position_exists: lp /= Void
			valid_start: lp.item > 0 and lp.item <= str.count
		local
			spos, epos: INTEGER
			is_crlf: BOOLEAN
		do
			from
				spos := lp.item
				epos := index_of_next ('%N', str, spos)
			until
				epos = 0 or str.item (epos - 1) /= '%N'
			loop
				spos := epos + 1
				epos := index_of_next ('%N', str, spos)
			end
			if epos = 0 then
				-- Not found
				epos := str.count + 1
			end
			if str.item (str.count) = '%R' then
				-- a CRLF situation; lose it
				epos := epos - 1
				is_crlf := True
			end
			Result := str.substring (spos, epos - 1)
			if Result.is_empty then
				lp.put (0)
			else
				if is_crlf then
					lp.put (epos + 1)
				else
					lp.put (epos)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	next_real_line (str, cs: STRING; ci: BOOLEAN): STRING
			-- Next line in 'str' that is neither empty nor begins
			-- with the given (comment) string (e.g. "#" or "REM" )
			-- The 'ci' arg is a flag denoting "case insensitive"
			-- match on comment string
			-- Void if no such lines remain in 'str'
		require
			stream_exists: str /= Void
		local
			ln: STRING
			is_comment: BOOLEAN
			ic: CELL [INTEGER]
		do
			create ic.put (1)
			from
				ln := next_non_empty_line (str, ic)
				is_comment := is_comment_line (ln, cs, ci)
			until
				ln = Void or else ((not ln.is_empty) and not is_comment)
			loop
				ln := next_non_empty_line (str, ic)
				is_comment := is_comment_line (ln, cs, ci)
			end
			Result := ln
		end

	--|--------------------------------------------------------------

	is_comment_line (ln, cs : STRING; ci: BOOLEAN): BOOLEAN
			-- Does line 'ln' have a comment sequence 'cs'
			-- as its first non-whitespace characters?
			-- Flag 'ci' enables case-insenstive match
		require
			line_exists: ln /= Void
		local
			cp: STRING
			ls: STRING
			sp: INTEGER
		do
			if (cs /= Void) and (ln /= Void) and then (not ln.is_empty) then
				sp := index_of_next_non_whitespace (ln, 1, False)
				if sp /= 0 then
					ls := ln.substring (sp, cs.count)
					if ci then
						cp := cs.as_upper
						ls.to_upper
					else
						cp := cs
					end
					if ls.is_equal (cp) then
						Result := True
					end
				end
			end
		end

	--|--------------------------------------------------------------

	quoted_substring_at_position (
		str: STRING; spos: INTEGER; esc: CHARACTER): detachable STRING
			-- Quoted value, included start and end quotes, beginning at 
			-- position 'spos' of stream 'str'
			-- Escaped quotes (using character 'esc') are ignored
			-- Mismatch results in Void Result
		require
			valid_stream: str /= Void and then not str.is_empty
			valid_start: spos > 0 and spos <= str.count
			first_is_alpha: is_quote (str.item (spos))
		local
			ep: INTEGER
		do
			ep := index_of_matching_close (str, spos, str.count, esc)
			if ep > 0 then
				Result := str.substring (spos, ep)
			end
		end

	--|--------------------------------------------------------------

	unquoted_word_at_position (
		str: STRING; spos: INTEGER; extras: ARRAY [CHARACTER]): STRING
			-- Unquoted token beginning at position 'spos' of stream 'str'
			--
			-- Unquoted words can contain only alphanumeric characters
			-- and those including in 'extras'
			--
			-- Error yields Void Result
		require
			valid_stream: str /= Void and then not str.is_empty
			valid_start: spos > 0 and spos <= str.count
			extras_exist: extras /= Void
		local
			ep: INTEGER
		do
			ep := index_of_next_non_alnum_plus (str, spos, extras)
			if ep = 0 then
				ep := str.count
			else
				ep := ep - 1 -- do not include the nonconforming char
			end
			Result := str.substring (spos, ep)
		end

	--|--------------------------------------------------------------

	num_instances (c: CHARACTER; v: STRING): INTEGER
			-- The number of character 'c' in string 'v'
		require
			exists: v /= Void
		local
			i, lim: INTEGER
		do
			lim := v.count
			from i := 1
			until i > lim
			loop
				if v.item (i) = c then
					Result := Result + 1
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	num_leading_chars (c: CHARACTER; v: STRING): INTEGER
			-- The number of leading characters 'c' in string 'v', if any
		require
			exists: v /= Void
		local
			i, lim, nc: INTEGER
		do
			lim := v.count
			from i := 1
			until i > lim or Result /= 0
			loop
				if v.item (i) /= c then
					Result := nc
				else
					nc := nc + 1
					i := i + 1
				end
			end
			Result := nc -- case of all chars in 'v' being 'c'
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	substring_split (str, sep: STRING): LINKED_LIST [STRING]
			-- Separate string 'str' into fields as delimited by
			-- separator 'sep'
		require
			string_exists: str /= Void
			sep_exists: sep /= Void and then not sep.is_empty
		local
			part: STRING
			i, j, lim: INTEGER
		do
			create Result.make
			if not str.is_empty then
				lim := str.count
				from i := 1
				until i > lim
				loop
					j := str.substring_index (sep, i)
					if j = 0 then
							-- No separator was found, item is remainder of 
							-- 'str' from here to end
						j := lim + 1
					end
					part := str.substring (i, j - 1)
					Result.extend (part)
					i := j + sep.count
				end
				if j = (lim - sep.count) + 1 then
					-- A separator was found at the end of the string
					-- This results in an empty last item
					check
						end_is_separator: str.substring (j, sep.count) ~ sep
					end
					Result.extend ("")
				end
			else
				-- Extend empty string, since Current is empty.
				Result.extend ("")
			end
			check
				-- Result.count = substring_occurrences (sep) + 1
			end
		ensure
			Result /= Void
		end

	--|--------------------------------------------------------------

	string_to_words (str: STRING): LINKED_LIST [STRING]
			-- Separate this string into fields separated by whitespace
		require
			string_exists: str /= Void
		local
			i, j: INTEGER
		do
			create Result.make
			if not str.is_empty then
				from i := index_of_next_non_whitespace (str, 1, True)
				until i = 0
				loop
					j := index_of_next_whitespace (str, i, True)
					if j > i then
						Result.extend (str.substring (i, j - 1))
						i := index_of_next_non_whitespace (str, j, True)
					elseif i <= str.count then
						Result.extend (str.substring (i, str.count))
						i := 0
					else
						i := 0
					end
				end
			end
		end

	--|--------------------------------------------------------------

	string_to_field_array (v: STRING; sep: CHARACTER): ARRAYED_LIST [STRING]
			-- Separate this string into fields separated by the character
			-- passed as argument
		require
			exists: v /= Void
		local
			tl: LIST [STRING]
			fld: STRING
			i, lim, j: INTEGER
		do
			tl := v.split (sep)
			if attached {ARRAYED_LIST [STRING]} tl as al then
				Result := al
			else
				-- Do it the hard way
				lim := v.count
				-- Worse case allocation: every character is a separator
				create Result.make (lim + 1)
				if lim > 0 then
					from i := 1
					until i > lim
					loop
						j := v.index_of (sep, i)
						if j = 0 then
							-- No separator found, skip to end
							-- field will be 'v' from current position to end
							j := lim + 1
						end
						fld := v.substring (i, j - 1)
						Result.extend (fld)
						i := j + 1
					end
					if j = lim then
						-- A separator was found at the end of the string
						Result.extend ("")
					end
				else
					-- v is empty, field is solo, and empty
					Result.extend ("")
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	string_to_field_array_x (v: STRING; sep, esc: CHARACTER): ARRAY [STRING]
			-- Separate string 'v' into fields separated by the character
			-- passed as argument
			-- Treat any separators inside of unescaped quotes as
			-- literal, where 'esc' is the escape character used
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			dbl_quote: BOOLEAN
			sgl_quote: BOOLEAN
			fl: LINKED_LIST [STRING]
			tstr: STRING
			c, prevc: CHARACTER
		do
			create fl.make
			create tstr.make (16)
			fl.extend (tstr)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				if c = sep then
					if dbl_quote or sgl_quote then
						 -- treat as literal
						 tstr.extend (c)
					else
						-- it's a separator
						create tstr.make (16)
						fl.extend (tstr)
					end
				elseif (c = '%'') and (prevc /= esc) then
					 sgl_quote := not sgl_quote
				elseif (c = '%"') and (prevc /= esc) then
					 dbl_quote := not dbl_quote
				else
					tstr.extend (c)
				end
				prevc := c
				i := i + 1
			end

			create Result.make_filled ("", 1, fl.count)
			i := 0
			from fl.start
			until fl.after
			loop
				tstr := fl.item
				if tstr /= Void and then not tstr.is_empty then
					 i := i + 1
					 Result.put (fl.item, i)
				end
				fl.forth
			end
			if i /= fl.count then
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	string_to_arg_vector (v: STRING; sep, esc: CHARACTER): ARRAY [STRING]
			-- Convert the current string into a command-like argument vector
			-- sep is the field separator character
			-- esc is the escape character which precedes the separator if and
			-- when the separator should be interpreted literally
		require
			exists: v /= Void
		do
			Result := string_to_field_array_x (v, sep, esc)
		end

	--|--------------------------------------------------------------

	line_to_arg_vector (v: STRING; sep, esc: CHARACTER): ARRAY [STRING]
			-- Convert the current string (must have no newlines) into
			-- a command-like argument vector
			-- Separator is assumed to be white space
		require
			exists: v /= Void
			no_newlines: not v.has ('%N')
		local
			tl: LINKED_LIST [STRING]
			i: INTEGER
		do
			tl := string_to_words (v)
			create Result.make_filled ("", 1, tl.count)
			from
				i := 1
				tl.start
			until tl.exhausted
			loop
				Result.put (tl.item, i)
				i := i + 1
				tl.forth
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	string_to_list (v: STRING; sep, esc: CHARACTER): LINKED_LIST [STRING]
			-- Separate string 'v' into fields separated by 'sep' if not 
			-- itself preceded by escape character 'esc' or embedded in
			-- quotes
			-- If string starts with 'sep', then first field is empty
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			tstr: STRING
			c, prevc, qc: CHARACTER
		do
			create Result.make
			create tstr.make (16)
			Result.extend (tstr)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				if c = sep then
					-- Was it escaped or quoted?
					if prevc = esc then
						 -- treat as literal
						 tstr.extend (c)
						 prevc := '%U'
					elseif qc /= '%U' then
						-- quoted, treat as literal
						 tstr.extend (c)
						 prevc := '%U'
					else
						-- it's a separator
						create tstr.make (16)
						Result.extend (tstr)
						prevc := c
					end
				elseif c = qc then
					-- Closing quote unless escaped
					tstr.extend (c)
					if prevc /= esc then
						qc := '%U'
					end
					prevc := c
				else
					tstr.extend (c)
					prevc := c
				end
				i := i + 1
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Transformations
--|========================================================================

	quotes_stripped (v: STRING): STRING
			-- The given string stripped of matching enclosing quotes, if 
			-- any
			-- N,B  If 'v' has no enclosing quotes, then Result = v
		local
			qc: CHARACTER
		do
			Result := v
			if v.count >= 2 then
				if v.item (1) = '%'' then
					qc := '%''
				elseif v.item (1) = '"' then
					qc := '"'
				else
					Result := v
				end
			end
			if qc /= '%U' then
				if v.item (v.count) = qc then
					if v.count = 2 then
						Result := ""
					else
						Result := v.substring (2,v.count - 1)
					end
				else
					Result := v
				end
			end
		end

	--|--------------------------------------------------------------

	quotes_escaped (v: STRING; esc: CHARACTER; nsf: BOOLEAN): STRING
			-- The given string with any leading, trailing or embedded 
			-- (and not yet escaped) quotes escaped with character 'esc'
			-- By default, all quotes are escaped, but if 'nsf' is True,
			--	then do not escape single quotes, only doubles
		require
			exists: v /= Void
			valid_escape: esc /= '%U'
		local
			c, prevc: CHARACTER
			i, lim: INTEGER
		do
			create Result.make (0)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				inspect c
				when '%'', '%"' then
					if prevc /= esc and not nsf then
						Result.extend (esc)
					end
				else
				end
				Result.extend (c)
				prevc := c
				i := i + 1
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	dos_to_unix (v: STRING): STRING
			-- The given string wtth %R%N pairs converted to %N only
			-- i.e. DOS to UNIX eol convention
		do
			Result := v.twin
			Result.replace_substring_all ("%R%N", "%N")
		end

	--|--------------------------------------------------------------

	trim (v: STRING)
			-- Trim from the given string all leading and trailing whitespace
		require
			exists: v /= Void
		do
			--| right adjust first to shorten string before left
			--| adjustment, even if just a little
			v.right_adjust
			v.left_adjust
		end

	--|--------------------------------------------------------------

	trimmed (v: STRING): STRING
			-- The given string trimmed of leading and trailing whitespace
		require
			exists: v /= Void
		do
			Result := substring_trimmed (v, 1, v.count)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	substring_trimmed (v: STRING; spos, epos: INTEGER): STRING
			-- The given string trimmed of leading and trailing whitespace,
			-- within the given substring range
		require
			exists: v /= Void
			valid_start: spos > 0 and spos <= v.count
			valid_end: epos > 0 and epos >= spos
		local
			i, lim: INTEGER
			done: BOOLEAN
		do
			if v.is_empty then
				Result := v
			else
				lim := v.count.min (epos)
				create Result.make (lim)
				-- Trim ws beginning with spos
				from i := spos
				until i > lim or done
				loop
					if not v.item (i).is_space then
						done := True
					else
						i := i + 1
					end
				end
				-- Work backward from end of range,
				-- reducing end index to exclude whitespace
				from done := False
				until lim <= i or done
				loop
					if not v.item (lim).is_space then
						done := True
					else
						lim := lim - 1
					end
				end
				from
				until i > lim
				loop
					Result.extend (v.item (i))
					i := i + 1
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	crop_start (v: STRING; n: INTEGER)
			-- Crop string 'v' by removing the first 'n' characters
			-- NB this can be expensive as all remaining characters must 
			-- be shifted left by 'n' positions
			-- If v is shorter than n, leave as is
		require
			exists: v /= Void
		do
			if n < v.count then
				v.keep_tail (v.count - n)
			end
		end

	--|--------------------------------------------------------------

	truncate_string_at_first_newline (v: STRING)
			-- As it says, truncate the string 'v' at the first newline
			-- found, exclusive of the newline char itself.
			-- If first char is newline, string will be empty
		local
			i: INTEGER
		do
			i := v.index_of ('%N', 1)
			if i > 1 then
				v.keep_head (i - 1)
			else
				v.wipe_out
			end
		end

	--|--------------------------------------------------------------

	highlighted (v: STRING): STRING
			-- The given string, but highlighted
		require
			exists: v /= Void
		do
			if v.is_empty then
				Result := v
			else
				Result := Ks_hilit_start + v + Ks_hilit_end
			end
		end

	--|--------------------------------------------------------------

	recase_substring (str: STRING; spos, epos, cv: INTEGER)
			-- Force case of chars in substring from spos through epos
			-- to 'cv'
			-- Where:
			--  cv = 1 -> all upper
			--  cv = -1 -> all lower
			--  cv = 10 -> title case
		require
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			sp, ep, len: INTEGER
			buf, ts: STRING
		do
			create buf.make (1 + epos - spos)
			len := 1 + epos - spos
			-- Always capitalize first word
			sp := spos
			ep := index_of_next_whitespace (str, sp, True)
			if ep /= 0 and ep > sp then
				ts := str.substring (sp, ep - 1)
				ts := word_capitalized (ts)
			else
				-- no more whitespace
				ts := word_capitalized (str)
			end
			buf.append (ts)
			if ep /= 0 then
				sp := ep
				ep := index_of_next_non_whitespace (str, sp, True)
			end
			if ep = 0 then
				-- we're done changing text, but there could be trailing 
				-- whitespace and that needs to be included
				if sp /= epos then
					-- add trailing text to buffer
 					buf.append (str.substring (sp, epos))
				else
					-- no more text
				end
			else
				ts := str.substring (sp, ep - 1)
				buf.append (ts)
				from sp := ep
				until sp > epos
				loop
					ep := index_of_next_whitespace (str, sp, True)
					if ep = 0 then
						-- No whitespace remaining in 'str'
						ts := str.substring (sp, epos)
						ep := epos
					else
						ts := str.substring (sp, ep - 1)
					end
					if is_title_case_worthy (ts) then
						buf.append (word_capitalized (ts))
					else
						buf.append (ts.as_lower)
					end
					if ep = epos then
						sp := epos + 1
					else
						sp := ep
						ep := index_of_next_non_whitespace (str, sp, True)
						if ep = 0 then
							-- No whitespace remaining in 'str'
							-- add trailing whitespace to buffer
							buf.append (str.substring (sp, epos))
							sp := epos + 1
						else
							-- add intervening whitespace to buffer
							ts := str.substring (sp, ep - 1)
							buf.append (ts)
							sp := ep
						end
 					end
				end
			end
			if buf.count /= len then
				-- add trailing text to buffer
				sp := buf.count + 1
				buf.append (str.substring (sp, epos))
			end
			str.replace_substring (buf, spos, epos)
		end

	--|--------------------------------------------------------------

	word_capitalized (str: STRING): STRING
			-- Contiguous word 'str', with first letter as upper case 
			-- and all others as lower case
			-- N.B. first letter might NOT be first character
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			Result := str.as_lower
			lim := str.count
			from i := 1
			until i > lim
			loop
				c := str.item (i)
				if c.is_alpha then
					Result.put (c.as_upper, i)
					i := lim
				end
				i := i + 1
			end
		ensure
			same_size: Result.count = str.count
		end

	--|--------------------------------------------------------------

	substitute_character_all (str: STRING; oldc, newc: CHARACTER)
			-- Replace all instances of the character oldc with the
			-- character newc
		require
			string_exists: str /= Void
		local
			i, lim: INTEGER
		do
			lim := str.count
			from i := 1
			until i > lim
			loop
				if str.item (i) = oldc then
					str.put (newc, i)
				end
				i := i + 1
			end
		ensure
			same_count: str.count = old str.count
		end

	--|--------------------------------------------------------------

	rotate (v: STRING)
			-- Rotate the characters in the given string one position
			-- to the left.  Wrap the original first character to be
			-- at the end.
		require
			exists: v /= Void
			rotatable: not v.is_empty
		local
			c: CHARACTER
			i, lim: INTEGER
		do
			lim := v.count
			if lim > 1 then
				c := v.item (1)
				from i := 1
				until i = lim
				loop
					v.put (v.item (i + 1), i)
					i := i + 1
				end
				v.put (c, v.count)
			end
		ensure
			same_length: v.count = old v.count
		end

	--|--------------------------------------------------------------

	switched (v: STRING; x, y: INTEGER): STRING
			-- A copy of the given string, with characters at
			-- positions x and y swapped
		require
			exists: v /= Void
			valid_positions: x > 0 and x <= v.count and y > 0 and y <= v.count
		do
			Result := v.twin
			if x /= y then
				Result := v.twin
				Result.put (v.item (y), x)
				Result.put (v.item (x), y)
			end
		ensure
			exists: Result /= Void
			same_length: Result.count = v.count
			original_unchanged: v.is_equal (old v)
			different_object: Result /= v
		end

	--|--------------------------------------------------------------

	vertical_slice (buf: STRING; ns, sn: INTEGER): STRING
			-- The 'n'_th vertical slice through buffer 'buf',
			-- where buffer has 'ns' slices
			--
			-- Example, if 'ns' is 4 and buf.count is 21, treat 'buf'
			-- as if it were a matrix of 6 rows of 4 columns each
			-- Result is the 'sn'_th such slice, or column
			--
			-- If 'sn' is 1 (the first column) in the example, Result
			-- would have the form:
			--  b01  b05  b09  b13  b17  b21
			--
			-- If 'sn' were 2, because the buffer size is not an even
			-- multiple of 'ns', the slice would have one fewer elements
			-- than slice #1, as in:
			--  b02  b06  b10  b14  b18
			--
			-- If buf's contents were:
			--  000011112222333345678
			-- slice #1 would be:
			--  012348
			-- and slice #2 would be:
			--  01235
			-- and slice #3 would be:
			--  01236
			-- and slice #4 would be:
			--  01237
			--
			-- Client can, having checked the length of slice 1,
			-- presumably append a padding character, if desired, to
			-- shorter slices.
		require
			exists: buf /= Void
			valid_slice_count: ns > 0
			valid_slice_number: sn > 0 and sn <= ns
		local
			slen, blim, bp, t_row: INTEGER
		do
			blim := buf.count
			-- Slice length is size of 'buf' divided by slice count,
			-- rounded to next even modulo 'ns'
			slen := blim // ns
			if blim \\ ns /= 0 then
				slen := slen + 1
			end
			create Result.make (slen)

			from
				t_row := 1
				bp := sn
			until t_row > slen
			loop
				if bp <= blim then
					-- Copy from buffer only if index is valid
					-- 'bp' can be off end if size is uneven
					Result.extend (buf.item (bp))
				end
				-- Bump input index to next input row
				bp := bp + ns
				t_row := t_row + 1
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	buffer_inverted (buf: STRING; ns: INTEGER): STRING
			-- The buffer 'buf' sliced vertically into 'ns' slices
			-- and reassembled horizontally (null-padded to an even
			-- multiple of 'ns')
			--
			-- Example, if 'ns' is 4 and buf.count is 24, treat 'buf'
			-- as if it were a matrix of 6 rows of 4 columns each,
			-- and Result as if it had 4 rows of 6 columns each, where
			-- each column of Result represents a row in 'buf'.
			-- When matrix is not square, short rows are padded with
			-- a null character
			--
			-- Result in the example would then have the form:
			--  b01  b05  b09  b13  b17  b21  b24
			--  b02  b06  b10  b14  b18  b22  Null
			--  b03  b07  b11  b15  b19  b23  Null
			--  b04  b08  b12  b16  b20  b24  Null
			--
			-- If buf's contents were:
			--  000011112222333345678
			-- with a count not an even modulo 'ns', the Result would be:
			--  01234801235_01236_01237_
			-- Where '-' represents nulls (to show alignment better)
		require
			exists: buf /= Void
			valid_slice_count: ns > 0
		local
			num_i_rows, blim, bp, i_col, t_row: INTEGER
		do
			blim := buf.count
			create Result.make (blim)
			-- Result row width (input row count) is size of 'buf',
			-- divided by slice count, rounded to next even modulo 'ns'
			num_i_rows := blim // ns
			if blim \\ ns /= 0 then
				num_i_rows := num_i_rows + 1
			end

			-- Outer loop indexes by input column
			-- There are 'ns' input columns, representing the vertical
			-- slices of the input
			-- There are a corresponding number of output rows
			from i_col := 1
			until i_col > ns
			loop
				-- Inner loop fills output rows from each input column,
				-- 'buf' is indexed by 'bp' that starts at the first index
				-- in a given input column and increments by row width
				from
					t_row := 1
					bp := i_col
				until t_row > num_i_rows
				loop
					if bp <= blim then
						-- Copy from buffer only if index is valid
						-- 'bp' can be off end if size is uneven
						Result.extend (buf.item (bp))
					else
						-- Null-fill row when uneven
						Result.extend ('%U')
					end
					-- Bump input index to next input row
					bp := bp + ns
					t_row := t_row + 1
				end
				i_col := i_col + 1
			end
		ensure
			exists: Result /= Void
			not_smaller: Result.count >= buf.count
		end

	--|--------------------------------------------------------------

	buffer_reverted (buf: STRING; ns, olen: INTEGER): STRING
			-- The buffer 'buf', having been previously inverted using
			-- buffer_inverted, reverted to it original form
			--
			-- Buffer is laid out, relative to original, as:
			--  o(ns+0)+1 o(ns*1)+1 o(ns*2)+1 ...
			--
			-- Inverted stream has columns that represent rows in the
			-- original
			--
			-- Not as simple as re-running "buffer_inverted" because of
			-- null padding for uneven modulo in original
		require
			exists: buf /= Void
			valid_slice_count: ns > 0
			valid_length: olen >= 0 and olen <= buf.count
		local
			num_i_cols, blim, bp, t_row, i_col, last_full_row: INTEGER
		do
			blim := buf.count
			create Result.make (olen)
			-- Thanks to padding during inversion, number of input
			-- columns is equal to buffer size divided by slice count
			num_i_cols := blim // ns

			-- Some rows are null-padded (with at most one null per row)
			-- Null padding is used when the original buffer size is not
			-- an even modulo 'ns'.  We can determine the number of full
			-- rows by using olen, the original buffer length.

			last_full_row := olen \\ ns
			if last_full_row = 0 then
				last_full_row := ns
			end

			-- Outer loop indexes by input column
			-- There are 'num_i_cols' input columns, representing the
			-- horizontal rows of the original (the intended output)
			-- Each input column has a height of 'ns' (slice count)
			from i_col := 1
			until i_col > num_i_cols
			loop
				-- Inner loop fills output rows from input columns
				-- 'buf' is indexed by 'bp' that starts at the first index
				-- in a row and increments by row width (num columns)
				from
					t_row := 1
					bp := i_col
				until t_row > ns
				loop
					-- The last input column will be short, if the
					-- original size was not an even modulo 'ns'
					if i_col = num_i_cols and then t_row > last_full_row then
						-- Short column, padded row; skip it and the rest
						-- of the column
						t_row := ns + 1
					else
						Result.extend (buf.item (bp))
						bp := bp + num_i_cols
						t_row := t_row + 1
					end
				end
				i_col := i_col + 1
			end
		ensure
			exists: Result /= Void
			valid_size: Result.count = olen
		end

--|========================================================================
feature -- Element change
--|========================================================================

	append_substring (dst, src: STRING; sp, ep: INTEGER)
			-- Append to string 'dst' substring from 'src' bounded by 
			-- start position 'sp' and end position 'ep'
			-- Leave 'src' intact
		require
			src_exists: src /= Void
			dst_exists: dst /= Void
			valid_start: sp > 0 and sp <= src.count
			valid_end: ep >= sp and ep <= src.count
		local
			i: INTEGER
		do
			dst.resize (dst.count + (ep - sp) + 1)
			from i := sp
			until i > ep
			loop
				dst.extend (src.item (i))
				i := i + 1
			end
		ensure
			count_updated: dst.count = (old dst.count) + (ep - sp + 1)
			appended: dst.ends_with (src.substring (sp, ep))
		end

 --|========================================================================
feature {NONE} -- Private support routines
 --|========================================================================

	bitval_at (num, idx: INTEGER_32): INTEGER
			-- The value (0 or 1) of the bit in v at position pos
			-- (where least significant is bit zero)
		do
			if num.bit_test (idx) then
				Result := 1
			else
				Result := 0
			end
		end

 --|------------------------------------------------------------------------

	isalnum (v: CHARACTER; uscore: BOOLEAN): BOOLEAN
			-- Is the given character alphanumeric?
			-- If 'uscore' then an underscore also qualifies
		do
			if v = '_' then
				Result := uscore
			else
				Result := v.is_alpha_numeric
			end
		end

	char_is_alpha (c: CHARACTER): BOOLEAN
			-- Is character 'c' alpha?
			-- (can be used as agent argument to conformance functions)
		do
			Result := c.is_alpha
		end

	char_is_alpha_numeric (c: CHARACTER): BOOLEAN
			-- Is character 'c' alphanumeric?
			-- (can be used as agent argument to conformance functions)
		do
			Result := c.is_alpha_numeric
		end

--|========================================================================
feature -- String iteration
--|========================================================================

	string_do_all (str: STRING; action: PROCEDURE [CHARACTER])
			-- Apply 'action' to every characer in string 'str'.
		require
			exists: str /= Void
			action_exists: action /= Void
		local
			i, lim: INTEGER
		do
			lim := str.count
			from i := 1
			until i > lim
			loop
				action.call ([str.item (i)])
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	substring_do_all (
		str: STRING; spos, epos: INTEGER;
		action: PROCEDURE [CHARACTER])
			-- Apply 'action' to every characer in string 'str' between 
			-- 'spos' and 'epos' (spos and epos are 1-based).
		require
			exists: str /= Void
			action_exists: action /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			sa: SPECIAL [CHARACTER]
		do
			sa := str.area
			-- area uses 0-based indices
			sa.do_all_in_bounds (action, spos - 1, epos - 1)
		end

	--|--------------------------------------------------------------

	string_do_if (
		str: STRING;
		action: PROCEDURE [CHARACTER];
		test: FUNCTION [CHARACTER, BOOLEAN])
			-- Apply 'action' to every character in 'str' that satisfies 'test'.
		require
			exists: str /= Void
			action_exists: action /= Void
			test_exists: test /= Void
		local
			i, lim: INTEGER
		do
			lim := str.count
			from i := 1
			until i > lim
			loop
				if test.item ([str.item (i)]) then
					action.call ([str.item (i)])
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	substring_do_if (
		str: STRING; spos, epos: INTEGER;
		action: PROCEDURE [CHARACTER];
		test: FUNCTION [CHARACTER, BOOLEAN])
			-- Apply 'action' to every character in 'str' between 'spos' 
			-- and 'epos' that satisfies 'test'.
		require
			exists: str /= Void
			action_exists: action /= Void
			test_exists: test /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			sa: SPECIAL [CHARACTER]
		do
			sa := str.area
			-- area uses 0-based indices
			sa.do_if_in_bounds (action, test, spos - 1, epos - 1)
		end

	--|--------------------------------------------------------------

	string_there_exists (
		str: STRING;
		test: FUNCTION [CHARACTER, BOOLEAN]): BOOLEAN
			-- Is 'test' true for at least one character in 'str'?
		require
			exists: str /= Void
			test_exists: test /= Void
		local
			i, lim: INTEGER
		do
			lim := str.count
			from i := 1
			until i > lim or Result
			loop
				Result := test.item ([str.item (i)])
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	substring_there_exists (
		str: STRING; spos, epos: INTEGER;
		test: FUNCTION [CHARACTER, BOOLEAN]): BOOLEAN
			-- Is 'test' true for at least one character in 'str' 
			-- between 'spos' and 'epos'?
		require
			exists: str /= Void
			test_exists: test /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			sa: SPECIAL [CHARACTER]
		do
			sa := str.area
			-- area uses 0-based indices
			Result := sa.there_exists_in_bounds (test, spos - 1, epos - 1)
		end

	--|--------------------------------------------------------------

	string_for_all (
		str: STRING; test: FUNCTION [CHARACTER, BOOLEAN]): BOOLEAN
			-- Is 'test' true for all characters in 'str'?
		require
			exists: str /= Void
			test_exists: test /= Void
		local
			i, lim: INTEGER
		do
			lim := str.count
			Result := True
			from i := 1
			until i > lim or not Result
			loop
				Result := test.item ([str.item (i)])
				i := i + 1
			end
		ensure
			empty: str.is_empty implies Result
		end

	--|--------------------------------------------------------------

	substring_for_all (
		str: STRING; spos, epos: INTEGER;
		test: FUNCTION [CHARACTER, BOOLEAN]): BOOLEAN
			-- Is 'test' true for all characters in 'str' between 'spos' 
			-- and 'epos'?
		require
			exists: str /= Void
			test_exists: test /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			sa: SPECIAL [CHARACTER]
		do
			sa := str.area
			-- area uses 0-based indices
			Result := sa.for_all_in_bounds (test, spos - 1, epos - 1)
		ensure
			empty: str.is_empty implies Result
		end

	--|--------------------------------------------------------------

	substring_do_all_with_index (
		str: STRING; spos, epos: INTEGER;
		action: PROCEDURE [CHARACTER, INTEGER])
			-- Apply 'action' to every characer in string 'str' between 
			-- 'spos' and 'epos' (spos and epos are 1-based).
			-- 'action' receives item and its index _within_the_substring_
		require
			exists: str /= Void
			action_exists: action /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			i, j, lim: INTEGER
		do
			from
				i := spos
				j := 1
				lim := epos
			until
				i > lim
			loop
				action.call ([str.item (i), j])
				j := j + 1
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	substring_do_if_with_index (
		str: STRING; spos, epos: INTEGER;
		action: PROCEDURE [CHARACTER, INTEGER];
		test: FUNCTION [CHARACTER, INTEGER, BOOLEAN])
			-- Apply 'action' to every character in 'str' between 'spos' 
			-- and 'epos' that satisfies 'test'.
			-- 'action' and 'test' each receive item and its index
			-- _within_the_substring_
		require
			exists: str /= Void
			action_exists: action /= Void
			test_exists: test /= Void
			valid_start: spos > 0 and spos <= str.count
			valid_end: epos >= spos and epos <= str.count
		local
			i, j, lim: INTEGER
		do
			from
				i := spos
				j := 1
				lim := epos
			until
				i > lim
			loop
				if test.item ([str.item (i), j]) then
					action.call ([str.item (i), j])
				end
				j := j + 1
				i := i + 1
			end
		end

--|========================================================================
feature -- Special hashing
--|========================================================================

	string_rl_encoded (v: STRING): STRING
			-- String 'v' in reversible encoded form
			--
			-- Transforms a character string into a string of digits
			-- (not an index, as the string could represent a number
			-- larger than supported integers)
			-- Desire is to have a good range of orders of magnitude
			-- (at least 3) and a reasonable distribution of digits,
			-- but not create strings that are going to cause problems 
			-- because of their length.
			--
			-- Hash is reversible as the first n digits denote the 
			-- original string's length and the last digit denotes the 
			-- number of digits in the length
			--
			-- There are 94 "graphical" ASCII characters and so this
			-- scheme can encode all of them in at most 2 digits.
			-- To deal with non-ASCII characters, additional digits
			-- are needed, but there is no need to change the ASCII
			-- character handling.  This means there needs to be kind
			-- of marker in the first position of a character that uses
			-- more than 2 positions encoded.
			--
			-- Because the ASCII encoding uses only decimal digits, it
			-- is possible to use a non-decimal marker.  This will cause
			-- problems for a certain hashing system, but a tweak
			-- and flag can work around that issue (the flag would tell 
			-- the hashing system to look for and skip any non-decimal 
			-- characters.)
			--
			-- Non-ASCII characters (w/ code values > 126 and biased 
			-- values > 94) are encoded with a leading single character:
			-- 'b' -> 'j' denoting the number of positions that character
			-- occupies, with 'b'denoting 2 digits, 'c' denoting 3 digits
			-- and so forth up to 'j' denoting 10 decimal digits.
			--
			-- For completeness, a marker of 'a' denotes a single 
			-- position character.  Note that 'b', for 2 position 
			-- characters, is needed for some non-ASCII characters in
			-- this scheme.
			--
			-- The graphical ASCII characters transformed using this 
			-- method produce the following results:
			--
			--    Char  Val     Char  Val     Char  Val
         --     ' '  01         A  33         a  65
         --       !  02         B  34         b  66
         --       "  03         C  35         c  67
         --       #  04         D  36         d  68
         --       $  05         E  37         e  69
         --       %  06         F  38         f  70
         --       &  07         G  39         g  71
         --       {  08         H  40         h  72
         --       }  09         I  41         i  73
         --       *  10         J  42         j  74
         --       +  11         K  43         k  75
         --       ,  12         L  44         l  76
         --       -  13         M  45         m  77
         --       .  14         N  46         n  78
         --       /  15         O  47         o  79
         --       0  16         P  48         p  80
         --       1  17         Q  49         q  81
         --       2  18         R  50         r  82
         --       3  19         S  51         s  83
         --       4  20         T  52         t  84
         --       5  21         U  53         u  85
         --       6  22         V  54         v  86
         --       7  23         W  55         w  87
         --       8  24         X  56         x  88
         --       9  25         Y  57         y  89
         --       :  26         Z  58         z  90
         --       ;  27         (  59         [  91
         --       <  28         \  60         |  92
         --       =  29         )  61         ]  93
         --       >  30         ^  62         ~  94
         --       ?  31         _  63    
         --       @  32         `  64    
			--
			-- Note well that tab (ctl-i) is not a graphical character 
			-- and is not included in this encoding.  It is possible to 
			-- add it but it would require a special case check.  It 
			-- could have encoded value of 95 as that would otherwise 
			-- correspond to delete, an ASCII control character, and so
			-- not at risk of being confused with a non-ASCII character.
		local
			i, lim, tc, cv, nb: INTEGER
			cs: STRING
		do
			create Result.make (32)
			lim := v.count
			cs := lim.out
			Result.append (cs)
			-- Add the code for each character
			from i := 1
			until i > lim
			loop
				tc := v.item (i).code
				if tc <= K_ascii_blank_bias then
					-- A control character
					-- Will NOT produce a valid rl encoded string in that 
					-- it will not be all digits.  Control characters 
					-- should not appear, but if they do, the result is 
					-- still a faithful encoding.  Control characters are 
					-- encoded as upper-case letters, beginning with 'A' 
					-- denoting Null, through 
				elseif tc > K_ascii_last_graphic then
					-- Is non-ASCII, could be UTF 8 or UCS 2
					-- Prefer not to bring in I18N cluster, as that would
					-- be a layering violation, so assume it's UCS 2 and
					-- provide an equivalent function in the i18N cluster
					-- to handle chars longer than 2 bytes
					-- Show all UCS 2 chars as 5 digits (wasteful yes but
					-- by only 40%)
					nb := bytes_in_utf8_char (v.item (i))
				else
					cv := tc - K_ascii_blank_bias
				end
				if cv < 10 then
					Result.extend ('0')
				end
				Result.append_integer (cv)
				i := i + 1
			end
			Result.append_integer (cs.count)
		ensure
			exists: Result /= Void
			encoded: is_valid_rl_encoded_string (Result)
		end

	--|--------------------------------------------------------------

	rl_string_decoded (v: STRING_8): STRING_8
			-- Function to transform a special hash into its original 
			-- character string.
			-- Hash begins with decimal length of original and ends with 
			-- the length of the decimal length (number of digits)
			-- Every other pair of digits represents the biased code of 
			-- the character in the original string
		require
			exists: v /= Void and then not v.is_empty
			valid_tail: v.item (v.count).is_digit
			encoded: is_valid_rl_encoded_string (v)
		local
			i, lim, lenlen, len, ti: INTEGER
			ts: STRING
			has_error: BOOLEAN
			c: CHARACTER_8
		do
			lenlen := v.item (v.count).out.to_integer
			ts := v.substring (1, lenlen)
			if not ts.is_integer then
				-- ERROR
				has_error := True
				create Result.make (0)
			else
				len := ts.to_integer
				create Result.make (len)
				lim := v.count - 1
				-- Each (ascii) character of the original has been 
				-- encoded as the decimal code, zero-padded to 2
				-- characters, biased down by the code for blank,
				-- less 1 (31)
				from i := lenlen + 1
				until i > lim or has_error
				loop
					create ts.make (2)
					ts.extend (v.item (i))
					ts.extend (v.item (i+1))
					if not ts.is_integer then
						has_error := True
					else
						ti := ts.to_integer
						c := ' ' + ti - 1
						Result.extend (c)
					end
					i := i + 2
				end
			end
			if has_error then
				Result.wipe_out
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_valid_rl_encoded_string (v: detachable STRING): BOOLEAN
			-- Is 'v' a valid encoded rl-encoded string?
		local
			i, lim, len, lenlen: INTEGER
			ts: STRING
		do
			if v /= Void and then not v.is_empty then
				-- Last character is length of the length, as a string, 
				-- of the original (decoded) string
				ts := v.substring (v.count, v.count)
				if ts.is_integer then
					lenlen := ts.to_integer
					ts := v.substring (1, lenlen)
					if ts.is_integer then
						len := ts.to_integer
						Result := True
						-- All characters in encoded stream must be decimal digits
						lim := v.count - 1
						from i := lenlen + 1
						until i > lim or not Result
						loop
							if not v.item (i).is_digit then
								Result := False
							end
							i := i + 1
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	string_rlx_encoded (v: STRING): STRING
			-- String 'v' in reversible encoded form
			--
			-- Transforms a character string into a string of hex digits
			-- (not an index, as the string could represent a number
			-- larger than supported integers)
			-- Desire is to have a good range of orders of magnitude
			-- (at least 3) and a reasonable distribution of digits,
			-- but not create strings that are going to cause problems 
			-- because of their length.
			--
			-- Hash is reversible as the first n digits denote the 
			-- original string's length and the last hex digit
			-- denotes the number of hex digits in the length.
			--
		local
			i, lim: INTEGER
			cs: STRING
			tc: INTEGER
		do
			create Result.make (32)
			lim := v.count
			-- Start with original string length, with no padding
			cs := aprintf ("%%x", << lim >>)
			Result.append (cs)
			-- Add the code for each character (byte)
			from i := 1
			until i > lim
			loop
				tc := v.item (i).code
				Result.append (aprintf ("%%02x", << tc >>))
				i := i + 1
			end
			-- Finish with length of length 
			Result.append (cs.count.to_hex_character.out)
		ensure
			exists: Result /= Void
			encoded: is_valid_rlx_encoded_string (Result)
		end

	--|--------------------------------------------------------------

	rlx_string_decoded (v: STRING_8): STRING_8
			-- Function to transform a special hash into its original 
			-- character string.
			-- Hash begins with hexadecimal length of original and ends with 
			-- the length of the hexadecimal length (number of digits)
			-- Every other pair of hex digits represents the code of 
			-- the character in the original string
		require
			exists: v /= Void and then not v.is_empty
			valid_tail: v.item (v.count).is_hexa_digit
			encoded: is_valid_rlx_encoded_string (v)
		local
			i, lim, lenlen, len, ti: INTEGER
			has_error: BOOLEAN
			c: CHARACTER_8
		do
			lenlen := hex_substring_to_integer (v, v.count, v.count)
			if not substring_is_hex_integer (v, 1, lenlen) then
				-- ERROR
				has_error := True
				create Result.make (0)
			else
				len := hex_substring_to_integer (v, 1, lenlen)
				create Result.make (len)
				lim := v.count - 1
				-- Each character of the original string has been 
				-- encoded as the hex code, zero-padded to 2
				-- characters
				from i := lenlen + 1
				until i > lim or has_error
				loop
					if not substring_is_hex_integer (v, i, i+1) then
						has_error := True
					else
						ti := hex_substring_to_integer (v, i, i+1)
						c := '%U' + ti
						Result.extend (c)
					end
					i := i + 2
				end
			end
			if has_error then
				Result.wipe_out
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	is_valid_rlx_encoded_string (v: detachable STRING): BOOLEAN
			-- Is 'v' a valid encoded rl-encoded string?
		local
			i, lim, len, lenlen: INTEGER
			--ts: STRING
		do
			if v /= Void and then not v.is_empty then
				-- Last character is length of the length, as a string, 
				-- of the original (decoded) string
				lenlen := hex_substring_to_integer (v, v.count, v.count)
				if substring_is_hex_integer (v, 1, lenlen) then
					len := hex_substring_to_integer (v, 1, lenlen)
					Result := True
					-- All characters in encoded stream must be decimal digits
					lim := v.count - 1
					from i := lenlen + 1
					until i > lim or not Result
					loop
						if not v.item (i).is_hexa_digit then
							Result := False
						end
						i := i + 1
					end
				end
			end
		end

--|========================================================================
feature -- URL Encoding
--|========================================================================

	string_url_encoded (v: STRING): STRING
		-- The URL-encoded equivalent of the given string
		require
			exists: v /= Void
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			create Result.make (v.count + v.count // 10)
			lim := v.count
			from i := 1
			until i > lim
			loop
				c := v.item (i)
				inspect c.lower
				when
					'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
					'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
					'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'.', '-', '~', '_'
				 then
					 Result.extend (c)
				when ' ' then
					Result.extend ('+')
				else
					Result.append (url_encoded_char (c))
				end
				i := i + 1
			end
		ensure
			exists: Result /= Void
			not_changed: v.is_equal (old v)
		end

	--|--------------------------------------------------------------

	hex_to_ascii_pos (c1, c2: CHARACTER): INTEGER
			-- ASCII code represented by the given Hex digits
			-- For URL encoding
		local
			ts: STRING
		do
			create ts.make (2)
			ts.extend (c1)
			ts.extend (c2)
			Result := hex_string_to_integer (ts)
		end

	--|--------------------------------------------------------------

	url_encoded_char (v: CHARACTER): STRING
		local
			i: INTEGER
			uc: CHARACTER_32
			ex: EXCEPTIONS
		do
			create Result.make (3)
			uc := v.to_character_32
			if uc.is_character_8 then
				i := v.code
				Result.extend ('%%')
				Result.append (i.to_hex_string)
--				Result.append (integer_32_to_hex (i, False))
			else
				create ex
				ex.raise ("Non-ascii escape not currently supported")
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Character encoding
--|========================================================================

	bytes_in_utf8_char (v: CHARACTER_8): INTEGER
			--	If the given byte a legal first byte element in a utf8 sequence,
			--	then the number of bytes in that character
			--	Zero denotes an error, i.e. not a legal UTF8 char
			--
			-- The first byte of a UTF8 encodes the length
		local
			c: NATURAL_8
		do
			c := v.code.to_natural_8
			Result := 1 -- 7 bit ASCII
			if (c & 0x80) /= 0 then
				-- Hi bit means not ASCII
				Result := 0
				if (c & 0xe0) = 0xc0 then
					-- If we see a first byte as b110xxxxx
					-- then we expect a two-byte character
					Result := 2
				elseif (c & 0xf0) = 0xe0 then
					-- If we see a first byte as b1110xxxx
					-- then we expect a three-byte character
					Result := 3
				elseif (c & 0xf8) = 0xf0 then
					-- If we see a first byte as b11110xxx
					-- then we expect a four-byte character
					Result := 4
				elseif (c & 0xfc) = 0xf8 then
					-- If we see a first byte as b111110xx
					-- then we expect a five-byte character
					Result := 5
				elseif (c & 0xfe) = 0xfc then
					-- If we see a first byte as b1111110x
					-- then we expect a six-byte character
					Result := 6
				end
			end
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	pfsr: AEL_PF_SCAN_ROUTINES
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_hilit_start: STRING = "[7m"
	Ks_hilit_end: STRING = "[0m"

	Ks_hex_digits: STRING = "0123456789abcdef"
	Ks_octal_digits: STRING = "01234567"

	K_ascii_blank_bias: INTEGER = 31
			-- Bias used to shift ASCII character code to start at 1 for 
			-- first printable character (blank)
			-- {ASCII}.blank - 1

	K_ascii_last_graphic: INTEGER = 126
			-- Highest graphical (printable) ASCII character code (~)

end -- class AEL_SPRT_STRING_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 010 13-Feb-2021
--|     Tweaked logic in next_line to deal with trailing char 
--|     sequences better
--|     Added num_instances function to count number of instances of 
--|     a given character
--| 009 07-Sep-2019
--|     Added recase_substring, is_title_case_worthy, and 
--|     unworthy_words to enable title-case conversions.  Limited to 
--|     English at present.
--| 008 20-Apr-2019
--|     Reworked logic in index_of_matching_close to deal with case 
--|     of open, close and escape chars all being dbl quotes (a 
--|     common ocurrence in CSV content)
--| 007 20-Apr-2011
--|     Added AEL_PF_SCAN_ROUTINES as a onced supplier, moving some 
--|     routine implementations into that class to a avoid dupliction 
--|     in the printf cluster, and layering violations.
--|     Added string_conforms general validation function.
--|     Added substring_matches function.
--|     Added index_of_next_non_alnum_plus function.
--|     Fixed potential over-run problem with index_of_matching_close.
--|     Added quoted_substring_at_position and unquoted_word_at_position.
--|     Update array makes to use make_filled in split-oriented 
--|     functions.
--|     Added string_to_list with support for escape char.
--|     Added quotes_escaped function.
--|     Added char_is_alpha and char_is_alpha_numeric functions.
--|     Added string and substring iteration routines.
--|     Added integer_to_base32 and base32_to_integer.
--| 006 13-Jan-2011
--|     Replaced old (and incorrect) base32 routines with 
--|     rfc4648-compliant ones
--| 005 08-Feb-2010
--|     Moved utf/uc2 routines to new cluster (i18n)
--| 004 28-Jul-2009
--|     Added history.  Changed name from AEL_STR_ROUTINES
--|     Compiled and tested using Eiffel 6.1 and 6.4
--|     Split into 2 versions, for legacy and void-safe.
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries and the Amalasoft Printf cluster.
--|----------------------------------------------------------------------
