note
	description: "{
Collection of routines for working with Tab-separated lists (text), as
might be exported from Excel or similar tools
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_SPRT_TSL_ROUTINES

create
	default_create


--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_max_ss_column_label (v: STRING)
			-- Set, to 'v', the highest supported column label
		do
			Ks_max_ss_column_label.wipe_out
			Ks_max_ss_column_label.append (v)
		ensure
			is_set: Ks_max_ss_column_label ~ v
		end

	reset_max_ss_column_label
			-- Reset, to default value, the highest supported 
			-- column label
		do
			Ks_max_ss_column_label.wipe_out
			Ks_max_ss_column_label.append (Ks_dflt_max_ss_col_lbl)
		ensure
			is_set: Ks_max_ss_column_label ~ Ks_dflt_max_ss_col_lbl
		end

--|========================================================================
feature -- Access
--|========================================================================

	next_ss_column_label (sc: STRING): STRING
			-- Label of spreadsheet column AFTER starting column 'sc'
		require
			valid_column: 	is_valid_ss_column_label (sc)
		do
			Result := ss_column_added_label (sc, 1)
		end

	--|--------------------------------------------------------------

	ss_column_added_label (sc: STRING; n: INTEGER): STRING
			-- Label of spreadsheet column 'n' columns AFTER 
			-- starting col 'sc'
			-- e.g. sc="C", n=3 -> 3+3=6 -> Result="F"
		require
			valid_column: 	is_valid_ss_column_label (sc)
		local
			v1, v2, vt: INTEGER
		do
			Result := ""
			if sc.count = 1 then
				v1 := char_ordinal_value (sc.item (1))
				if v1 /= 0 then
					vt := v1 + n
					Result := ss_column_num_to_label (vt)
				end
			elseif sc.count = 2 then
				v1 := char_ordinal_value (sc.item (1))
				if v1 > 0 then
					v2 := char_ordinal_value (sc.item (2))
					if v2 > 0 then
						v1 := v1 * K_alphabet_az_size
						vt := v1 + v2 + n
						Result := ss_column_num_to_label (vt)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	next_non_empty_ss_column (str, sc: STRING): STRING
			-- Label for next spreadsheet column, after starting
			-- column 'sc', in which there is text
			-- If no later columns have text, Result is empty
		require
			stream_exists: attached str
			valid_column: 	is_valid_ss_column_label (sc)
		local
			sp, lim: INTEGER
			lbl: STRING
		do
			Result := ""
			sp := ss_column_to_index (str, sc)
			lbl := next_ss_column_label (sc)
			lim := str.count
			from
			until (not Result.is_empty) or
				(sp > lim) or
				not is_valid_ss_column_label (lbl)
			loop
				if not text_at_ss_column (str, lbl).is_empty then
					Result := lbl
				else
					sp := sp + 1
					lbl := next_ss_column_label (lbl)
				end
			end
		ensure
			exists: attached Result
		end

	--|--------------------------------------------------------------

	text_at_ss_column (str, cnm: STRING): STRING
			-- Text in Spreadsheet column 'cnm', in text stream 'str'
			-- If no text, Result is empty
			-- If there is an error, result is an error message, 
			-- beginning with "ERROR: "
		require
			stream_exists: attached str
			valid_column: 	is_valid_ss_column_label (cnm)
		local
			sp, ep: INTEGER
		do
			sp := ss_column_to_index (str, cnm)
			if sp <= 0 or sp > str.count then
				Result := "ERROR: Invalid column: " + cnm
			elseif str.item (sp) = '%T' then
				Result := ""
			else
				ep := str.index_of ('%T', sp)
				if ep = 0 then
					ep := str.count
				else
					ep := ep - 1
				end
				Result := str.substring (sp, ep)
			end
		ensure
			exists: attached Result
		end

	text_at_ss_column_number (str: STRING; v: INTEGER): STRING
			-- Text in Spreadsheet column 'v', in text stream 'str'
		require
			valid_index: is_valid_ss_column_number (v)
		local
			cnm: STRING
		do
			cnm := ss_column_num_to_label (v)
			Result := text_at_ss_column (str, cnm)
		ensure
			exists: attached Result
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	ss_columns_difference (sc, ec: STRING): INTEGER
			-- Difference between spreadsheet columns sc and ec
			-- e.g. 'A' to 'C' -> 'C' - 'A' -> 3 - 1 -> 2
			-- If sc < ec then Result is positive
			-- If sc ~ ec then Result is 0
			-- If sc > ec then Result is negative
		require
			valid_start: is_valid_ss_column_label (sc)
			valid_end: is_valid_ss_column_label (ec)
		local
			v1, v2: INTEGER
		do
			v1 := ss_column_label_to_number (sc)
			v2 := ss_column_label_to_number (ec)
			Result := v2 - v1
		end

	--|--------------------------------------------------------------

	compare_ss_column_labels (c1, c2: STRING): INTEGER
			-- Compare column labels 'c1' and 'c2.
			-- If 'c2' > 'c1' then Result > 0
			-- If 'c1' > 'c2' then Result < 0
			-- If 'c1' ~ 'c2' then Result = 0
		require
			valid_first: is_valid_ss_column_label (c1)
			valid_second: is_valid_ss_column_label (c2)
		do
			Result := ss_columns_difference (c1, c2)
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

	ss_column_to_index (str, col: STRING): INTEGER
			-- Position (1-based) in 'str' equivalent to column 'col' in 
			-- a tab-separated Excel output
			--
			-- Example: ts :="%Tbozo%Ttonto%T%T%Tsumpin%T"
			--
			--   2       12  13
			--   |         ||
			--  _bozo_tonto___sumpin_
			--  |     |      ||
			--  1     7    14  15
			--
			--  _bozo_tonto___sumpin_
			--  |^---^^----^||^-----^
			--  _bozo_tonto___sumpin_
			--  A     |     |||
			--   B    C     DEF
			--
			-- ss_column_to_index (ts, "A") -- want 1
			-- ss_column_to_index (ts, "B") -- want 2
			-- ss_column_to_index (ts, "C") -- want 7
			-- ss_column_to_index (ts, "D") -- want 13
			-- ss_column_to_index (ts, "E") -- want 14
			-- ss_column_to_index (ts, "F") -- want 15
		require
			stream_exists: attached str as tx and then not tx.is_empty
			valid_column: attached col as cx and then (cx.count=1 or cx.count=2)
		local
			cc, sp, tc, cn: INTEGER
			clist: LIST [STRING]
			buf: STRING
			last_was_empty: BOOLEAN
		do
			cn := ss_columns_difference ("A", col) + 1
			-- Count tabs; there's a tab AFTER every cell
			if cn = 1 then
				-- First column special case
				Result := 1
			else
				if str.item (str.count) = '%T' then
					-- Prune it
					buf := str.substring (1, str.count - 1)
				else
					buf := str
				end
				clist := buf.split ('%T')
				tc := 0
				cc := 1
				sp := 1
				from clist.start
				until clist.after or Result /= 0
				loop
					if cc = cn then
						Result := sp
					else
						if clist.item.is_empty then
							-- Was just a separator, count as 1
							sp := sp + 1
							last_was_empty := False
						else
							-- each non-empty item had a trailing tab in the 
							-- orginal string
							sp := sp + clist.item.count + 1
							last_was_empty := False
						end
						clist.forth
						cc := cc + 1
					end
				end
			end
		ensure
			stream_unchanged: str ~ old str
		end

	index_to_ss_column_label (str: STRING; idx: INTEGER): STRING
			-- Label of column corresponding to position 'idx', in 
			-- stream 'str'
		require
			exists: attached str
			valid_index: idx > 0 and idx <= str.count
		local
			i, tc: INTEGER
		do
			from i := 1
			until i > idx
			loop
				if str.item (i) = '%T' then
					tc := tc + 1
				end
				if i = idx then
					-- stop counting tabs
				end
				i := i + 1
			end
			if tc = 0 then
				Result := ""
			else
				Result := ss_column_num_to_label (tc)
			end
		end

	--|--------------------------------------------------------------

	ss_column_num_to_label (v: INTEGER): STRING
			-- Symbolic column label (A-ZZ) equivalent of index 'v'
		require
			valid_index: is_valid_ss_column_number (v)
		local
			v1, v2: INTEGER
			c1, c2: STRING
		do
			if v <= K_alphabet_az_size then
				Result := Ks_alphabet_az.item (v).out
			else
				-- Fist letter is v div 26
				v1 := v // 26
				c1 := Ks_alphabet_az.item (v1).out
				-- Second letter is v mod 26
				v2 := v \\ 26
				c2 := Ks_alphabet_az.item (v2).out
				Result := c1 + c2
			end
		ensure
			valid: attached Result as tr and then not tr.is_empty
		end

	--|--------------------------------------------------------------

	ss_column_label_to_number (v: STRING): INTEGER
			-- Column number corresponding to Spreadsheet column label 'v'
			-- e.g. "A" -> 1,  "AA" -> 27
		require
			valid_label: is_valid_ss_column_label (v)
		local
			v1, v2: INTEGER
		do
			if v.count = 1 then
				Result := char_ordinal_value (v.item (1))
			elseif v.count = 2 then
				v1 := char_ordinal_value (v.item (1))
				if v1 > 0 then
					v2 := char_ordinal_value (v.item (2))
					if v2 > 0 then
						v1 := v1 * K_alphabet_az_size
						Result := v1 + v2
					end
				end
			end
		ensure
			valid: is_valid_ss_column_number (Result)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_ss_column_label (v: STRING): BOOLEAN
			-- Is 'v' a valid spreadsheet column label?
			-- Must be >= 'A' and <= 'ZZ'
		do
			if attached v as tv and then not tv.is_empty then
				if char_ordinal_value (tv.item (1)) /= 0 then
					Result := True
					if tv.count = 2 then
						if char_ordinal_value (tv.item (2)) = 0 then
							Result := False
						end
					elseif tv.count > 2 then
						Result := False
					end
				end
			end
		end

	is_valid_ss_column_number (v: INTEGER): BOOLEAN
			-- Is 'v' a valid spreadsheet column number?
			-- Must be >= 1 ('A') and <= 676 ('ZZ')
		do
			Result := v > 0 and v <= K_max_ss_column_number
		end

	--|========================================================================
feature {NONE} -- Implementation
	--|========================================================================

	--|========================================================================
feature {NONE} -- Constants
	--|========================================================================

	Ks_alphabet_az: STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	K_alphabet_az_size: INTEGER
			-- Number of chars in Ks_alphabet_az (for SS cols)
		once
			Result := Ks_alphabet_az.count
		end

	char_ordinal_value (c: CHARACTER): INTEGER
			-- Value, within alphabet, associated with character 'c'
			-- If 'c' is not in the alphabet, 0
		do
			Result := Ks_alphabet_az.index_of (c.as_upper, 1)
		end

	K_max_ss_column_number: INTEGER
			-- Max supported spreadsheet column number
			-- (26 * 26) + 26 -> 702
		once
			Result := K_alphabet_az_size * (K_alphabet_az_size + 1)
		end

	--|--------------------------------------------------------------

	Ks_max_ss_column_label: STRING
			-- Label to highest support spreadsheet column
		once
			Result := Ks_dflt_max_ss_col_lbl.twin
		end

	Ks_dflt_max_ss_col_lbl: STRING = "ZZ"
			-- Label to highest support spreadsheet column

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 17-Mar-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|     Borrowed from existing, less library-like work
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_SPRT_TSL_ROUTINES
