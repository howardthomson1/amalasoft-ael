class AEL_SPRT_WILDCARD
-- Wildcard expression

create
	make, make_insensitive

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make (p: STRING)
			-- Create Current with pattern 'p'
		require
			exists: p /= Void and then not p.is_empty
		do
			create implementation.make_and_compile (p)
		end

	make_insensitive (p: STRING)
			-- Create Current with pattern 'p' adapted for 
			-- case-insensitivitty
		require
			exists: p /= Void and then not p.is_empty
		do
			create implementation.make_and_compile_insensitive (p)
		end

--|========================================================================
feature -- Search and match
--|========================================================================

	search_forward (buf: STRING; sp, ep: INTEGER)
			-- Search for compiled pattern in 'buf', beginning at 'sp', 
			-- through 'ep', inclusive
			-- Set 'found' to indicate success/failure
			-- Set 'start_position' and 'end_position' to denote 
			-- positions in 'buf' at which a match was found, if any
		require
			exists: buf /= Void
			valid_start: (not buf.is_empty) implies sp > 0 and sp <= buf.count
			valid_end: (not buf.is_empty) implies ep <= buf.count and ep >= sp
		do
			implementation.search_forward (buf, sp, ep)
		end

	--|--------------------------------------------------------------

	substring_matches (buf: STRING; sp, ep: INTEGER): BOOLEAN
			-- Does the substring in 'buf' between 'sp' and 'ep' match 
			-- Current?
			--
			-- 'found' will indicate success/failure
			-- If True, then 'start_position' will be 'sp' and
			-- 'end_postion' will be 'ep'
		require
			exists: buf /= Void
			valid_start: (not buf.is_empty) implies sp > 0 and sp <= buf.count
			valid_end: (not buf.is_empty) implies ep <= buf.count and ep >= sp
		do
			implementation.match_substring (buf, sp, ep)
			Result := found
		end

	--|--------------------------------------------------------------

	matches (buf: STRING): BOOLEAN
			-- Does the substring in 'buf' between 'sp' and 'ep' match 
			-- the compiled pattern of Current?
			--
			-- 'found' will indicate success/failure
			-- If True, then 'start_position' will be 1 and
			-- 'end_postion' will be buf.count
		require
			exists: buf /= Void
		do
			Result := substring_matches (buf, 1, buf.count)
		end

--|========================================================================
feature -- Status
--|========================================================================

	compiled: BOOLEAN
		do
			Result := implementation.is_compiled
		end

	found: BOOLEAN
		do
			Result := implementation.has_matched
		end

	start_position: INTEGER
		do
			Result := implementation.start_position
		end

	end_position: INTEGER
		do
			Result := implementation.end_position
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	implementation: AEL_SPRT_WILDCARD_IMP

	--|--------------------------------------------------------------
invariant
	has_implementation: implementation /= Void

end -- class AEL_SPRT_WILDCARD
