deferred class AEL_SPRT_ASSOCIATIVE
-- Abstract notion of associating a label with a value

--|========================================================================
feature -- Label-based queries
--|========================================================================

	value_by_label (v: STRING): ANY
			-- Value associated with the given label
		require
			valid_label: is_valid_value_label (v)
		deferred
		ensure
			known_if_exists: Result /= Void implies value_label_index (v) /= 0
		end

	--|--------------------------------------------------------------

	string_value_by_label (v: STRING): STRING
		require
			exists: v /= Void and then not v.is_empty
		do
			create Result.make (0)
			if is_valid_value_label (v) then
				if attached value_by_label (v) as ta then
					if attached {STRING} ta as lta then
						Result := lta
					end
					if Result.is_empty then
						if attached {BOOLEAN_REF} ta as br then
							-- Use popular js/java/html/xml lower-case bool 
							-- convention instead of Eiffel convention
							Result := br.out.as_lower
						else
							Result := ta.out
						end
					end
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	values_by_labels (ll: ARRAY [STRING]) : LINKED_LIST [STRING]
			-- String values corresponding to the given labels
		require
			labels_exist: ll /= Void
		local
			i, lim: INTEGER
		do
			create Result.make
			lim := ll.count
			from i := 1
			until i > lim
			loop
				Result.extend (string_value_by_label (ll.item (i)))
				i := i + 1
			end
		ensure
			exists: Result /= Void
			consistent_count: Result.count = ll.count
		end

--|========================================================================
feature -- Label-based comparison
--|========================================================================

	is_less_by_value (other: like Current; lbl: STRING; af: BOOLEAN): BOOLEAN
			-- Is Current less than 'other' by the value associated with 
			-- label 'lbl'?
			-- If 'af' then use alpha comparison (ie compare upper case
			-- forms), else use default
		require
			other_exists: other /= Void
			valid_label: is_valid_value_label (lbl)
			is_comparable: attached {COMPARABLE}value_by_label (lbl)
			alpha_valid: af implies attached {STRING}value_by_label (lbl)
		local
			v1, v2: ANY
		do
			v1 := value_by_label (lbl)
			v2 := other.value_by_label (lbl)
			if af then
				if attached {STRING}v1 as s1
					and then attached {STRING}v2 as s2
				 then
					 Result := s1.as_upper < s2.as_upper
				end
			else
				if attached {COMPARABLE}v1 as c1
				 and then attached {COMPARABLE}v2 as c2
				 then
					 Result := c1 < c2
				end
			end
		end

--|========================================================================
feature -- Label-based value setting
--|========================================================================

	set_value_by_label (lb: AEL_SPRT_VALUE_LABEL; v: ANY)
			-- Set the value for the attribute associated
			-- with the given label
		require
			valid_label: is_valid_value_label (lb)
			is_settable: lb.is_settable
		deferred
		end

--|========================================================================
feature -- Label validation
--|========================================================================

	is_valid_value_label (v: STRING): BOOLEAN
		do
			if v /= Void and then not v.is_empty then
				Result := value_labels.has (v)
			end
		end

	is_valid_value_label_id (v: INTEGER): BOOLEAN
		do
			Result := value_label_by_id (v) /= Void
		end

--|========================================================================
feature -- Label lookup and conversion
--|========================================================================

	value_label_by_id (v: INTEGER): detachable AEL_SPRT_VALUE_LABEL
			-- Value label corresponding to the given ID 'v'
		require
			valid_id: v > 0
		do
			from
				value_labels.start
			until
				value_labels.after or Result /= Void
			loop
				if value_labels.item_for_iteration.id = v then
					Result := value_labels.item_for_iteration
				else
					value_labels.forth
				end
			end
		ensure
			belongs_if_exists: Result /= Void implies value_labels.has (Result)
		end

	--|--------------------------------------------------------------

	value_label_index (v: STRING): INTEGER
		require
			exists: v /= Void and then not v.is_empty
		local
			tvl: like value_label_by_id
		do
			if attached {like value_label_by_id} v as tv then
				tvl := tv
				Result := tvl.id
			else
				tvl := value_labels.item (v)
				if tvl /= Void then
					Result := tvl.id
				end
			end
		end

	--|--------------------------------------------------------------

	value_labels: AEL_SPRT_VALUE_LABEL_TABLE
			-- Table containing value labels, indexed by label string
			-- Redefined in each descendent that requires a different
			-- table subtype.  Each table subtype has specific items
			-- in it.
		once
			create Result.make_initialized (17)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update_values
			-- Ensure that values, if any, reflect state of Current
		do
		end

end -- class AEL_SPRT_ASSOCIATIVE
