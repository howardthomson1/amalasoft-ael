note
	description: "{
Abstract notion of having attributes that are settable from a serialized
string form, and that can be presented in a serialized string form.
}"

class AEL_SPRT_ATTRIBUTABLE_2

inherit
	AEL_SPRT_MULTI_FALLIBLE

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
			-- Create Current in default state
		do
			core_make
		end

	--|--------------------------------------------------------------

	make_from_stream (v: STRING)
			-- Create Current from stream 'v'
		require
			is_valid_stream: is_valid_input_stream (v)
		do
			make_from_substream (v, 1, v.count)
		end

	--|--------------------------------------------------------------

	make_from_substream (v: STRING; sp, ep: INTEGER)
		do
			core_make
			init_from_substream (v, sp, ep)
		end

	--|--------------------------------------------------------------

	core_make
		do
			is_initializing := True
			create attributes.make (3)
			make_elements
--RFO 			make_serial_params
--RFO 			make_serializer
			reinitialize
			default_created := True
			is_initializing := False
		end

	reinitialize
		require
			is_initializing: is_initializing
		do
			attributes.wipe_out
			serial_params.initialize_parameters
			initialize_serial_params
		end

--|========================================================================
feature {NONE} -- Private initialization
--|========================================================================

--RFO 	make_serial_params
--RFO 		deferred
--RFO 		end

	--|--------------------------------------------------------------

	initialize_serial_params
		require
			has_params: serial_params /= Void
		do
			serial_params.set_pattern ("tag_label", "")
			serial_params.set_pattern ("elements_tag_label", "elements")
			serial_params.set_attribute_element_separator ("")
		end

--RFO 	make_serializer
--RFO 			-- Create the 'serializer'
--RFO 		deferred
--RFO 		ensure
--RFO 			serializer_exists: has_valid_serializer
--RFO 		end

	make_elements
			-- Create the 'elements' container
		do
			--| Redefine in child
			set_element_structure
		ensure
			structure_defined: elements_is_a_table or elements_is_a_list
			elements_exist: (default_created and then has_elements) implies
				elements /= Void
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_substream (v: STRING; sp, ep: INTEGER)
			-- Initialize current from stream 'v' between positions 
			-- 'sp' and 'ep'
		require
			exists: v /= Void
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep >= sp and ep <= v.count
			is_valid_stream: is_valid_input_stream (v.substring (sp, ep))
			made: default_created
--RFO 		deferred
		do
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_input_stream (v: STRING): BOOLEAN
			-- Is the string 'v' a valid stream from which to initialize 
			-- Current?
		do
			Result := v /= Void
			if Result and has_valid_serializer then
				Result := serializer.is_valid_input_stream (v)
			end
		end

	--|--------------------------------------------------------------

	has_valid_serializer: BOOLEAN
			-- Does Current have a non-void abnd valid serializer?
		do
			Result := is_valid_serializer (serializer)
		end

	is_valid_serializer (v: like serializer): BOOLEAN
			-- Is the serializer 'v' valid?
		do
			Result := v /= Void
		end

--|========================================================================
feature -- Access
--|========================================================================

	value_for_attribute (v: STRING): STRING
			-- Value, if any, for attribute with label 'v'
			-- An empty result can occur for a non-corresponding label 
			-- or for a corresponding label whose associated value is 
			-- empty or Void
		require
			exists: v /= Void
		do
			Result := attributes.item (v)
			if Result = Void then
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	value_for_required_attribute (v: STRING): STRING
			-- Value for attribute of given label
			-- If nonexistent, then set has_attribute_error
		do
			Result := value_for_attribute (v)
			if Result = Void or else Result.is_empty then
				set_error_message ("Required attribute missing")
			end
		ensure
			no_error_exists: (not has_error) implies Result /= Void
			error_no_exists: Result = Void implies has_error
		end

	--|--------------------------------------------------------------

	attribute_by_name (v: STRING): AEL_SPRT_ATTRIBUTE
			-- Attribute name and value associated with label 'v', if any
		require
			exists: v /= Void
		do
			Result := serializer.attribute_by_name (attributes, v)
		end

--|========================================================================
feature -- Status
--|========================================================================

	default_created: BOOLEAN
			-- Has default_create been called?

	is_initializing: BOOLEAN
			-- Is Current in the processor initializing?

	--|--------------------------------------------------------------

	has_attribute (v: STRING): BOOLEAN
			-- Does Current have an attribute by the label 'v'
		require
			exists: v /= Void and then not v.is_empty
		do
			Result := attributes.has (v)
		end

	--|--------------------------------------------------------------

	has_filter_tag (v: STRING): BOOLEAN
			-- Does Current have a tag filter with the given string?
		require
			exists: v /= Void
		do
			if attached {CONTAINER [STRING]} tag_filter as tc then
				Result := tc.has (v)
			end
		end

	tag_filter: LIST [STRING]
			-- Set of only tags to include in serialized output
			-- If Void, then all (default) tags are included

	has_item_filter_tag (v: STRING): BOOLEAN
			-- Does Current have a tag filter with the given string?
		require
			exists: v /= Void
		do
			if attached {CONTAINER [STRING]} item_tag_filter as tc then
				Result := tc.has (v)
			end
		end

	item_tag_filter: LIST [STRING]
			-- Set of only tags to include in serialized output of
			-- subordinate items
			-- If Void, then all (default) tags are included

	item_tag_filter_inverted: BOOLEAN
			-- Is the behavior of the item tag filter (if any) inverted?
			-- Default behavior is to included only attrs matching filter 
			-- tags. Inverted behavior excludes only attrs matching filter.
		do
			Result := serial_params.item_tag_filter_inverted
		end

	serial_element_range: detachable INTEGER_INTERVAL
			-- Range (lower/upper) of indices of elements (if any)
			-- to be included in serial output
			-- If Void, then all elements are included

	elements_is_a_list: BOOLEAN
			-- Is the 'elements' structure a list?

	elements_is_a_table: BOOLEAN
			-- Is the 'elements' structure a HASH TABLE of identifiable 
			-- items?

--|========================================================================
feature -- Value setting
--|========================================================================

--RFO 	extract_attributes
--RFO 			-- From recorded attributes, extract and apply values to 
--RFO 			-- Current
--RFO 			-- Example:
--RFO 			-- set_name (value_for_required_attribute ("name"))
--RFO 		require
--RFO 			attrs_exist: attributes /= Void
--RFO 		deferred
--RFO 		end

--|========================================================================
feature -- External representation
--|========================================================================

	tag_label: STRING
		do
			Result := serial_params.tag_label
		end

	tag_label_suppressed: BOOLEAN
			-- Should tag label be suppressed on serial output?
		do
			Result := serial_params.tag_label_suppressed
		end

	tag_filter_inverted: BOOLEAN
			-- Is the behavior of the tag filter (if any) inverted?
			-- Default behavior is to included only attrs matching filter 
			-- tags. Inverted behavior excludes only attrs matching filter.
		do
			Result := serial_params.tag_filter_inverted
		end

	--|--------------------------------------------------------------

 	serial_out: STRING
 			-- Serialized string representation of Current, per 
 			-- serial parameters
 		do
 			if serial_out_supported then
				Result := serializer.serial_out (Current, serial_params)
			end
		end

	--|--------------------------------------------------------------

	serial_out_filtered (ll: like tag_filter): STRING
			-- Serialized string representing Current, but with only
			-- those attributes matching the labels in 'll'
		require
			supports_serial_out: serial_out_supported
			labels_exist: ll /= Void
		local
			cl: LINKED_LIST [STRING]
		do
			if ll.object_comparison then
				tag_filter := ll
			else
				create cl.make
				cl.compare_objects
				cl.fill (ll)
				tag_filter := cl
			end
			Result := serial_out
			tag_filter := Void
		end

	--|--------------------------------------------------------------

	attributes_serial_out: STRING
			-- Serialized representation of attributes of Current
		do
			Result := serializer.attributes_serial_out (Current, attributes)
		end

	--|--------------------------------------------------------------

	attributes_out_by_tag (ll: LIST [STRING]): STRING
			-- Attributes corresponding to the given labels 'll' only
		do
			Result := serializer.attributes_out_by_tag (Current, attributes, ll)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attribute_out (t, v: STRING): STRING
			-- Attribute, represented by tag 't' and value 'v' in
			-- string serialized form
		require
			valid_tag: t /= Void and then not t.is_empty
		do
			Result := serializer.attribute_out (serial_params, t, v)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attributes: HASH_TABLE [STRING, STRING]
			-- Attributes from corresponding tag, if any

	cached_serial_elements: like serial_elements
			-- Serializable subordinate elements, if any,
			-- cached as a result of calling 'update_elements',
			-- if 'elements_are_from_cache' is True
			--
			-- In descendent, typical implementation of this feature 
			-- would be as attribute
		do
		end

	serial_elements: CONTAINER [like element_type]
			-- Serializable subordinate elements, if any
		do
		end

	elements: like serial_elements
			-- Serializable subordinate elements, if any
			-- It is this function that serializers use to get the 
			-- elements for serialization.  To change elements,
			-- redefine the serial_elements function
		do
			if elements_are_from_cache then
				Result := cached_serial_elements
			else
				Result := serial_elements
			end
		end

	serializer: AEL_SPRT_ATTRIBUTE_SERIALIZER_2
			-- Serializing service for Current
--RFO 		deferred
		do
		ensure
			exists: Result /= Void
		end

	serial_params: AEL_SPRT_SERIAL_PARAMS_2
			-- Parameters directing serialization
		do
		end

	--|--------------------------------------------------------------

	has_elements: BOOLEAN
			-- Does this tag have any elements (currently) ?
		do
			Result := serial_params.has_elements
		end

	elements_are_from_cache: BOOLEAN
			-- Are elements cached for serialization?
		do
		end

	serial_elements_is_empty: BOOLEAN
			-- Is 'elements', if it exists, empty?
		do
			if has_elements then
				Result := elements.is_empty
			end
		end

	--|--------------------------------------------------------------

	element_by_name (v: STRING): like element_type
			-- Element with name 'v' if any
		require
			has_elements: has_elements
		do
			if elements_is_a_list then
				Result := list_element_by_name (v)
			elseif elements_is_a_table then
				Result := table_element_by_name (v)
			else
			end
		end

	--|--------------------------------------------------------------

	serial_out_supported: BOOLEAN
			-- Is serialized output supported?
		do
		end

	serial_max_depth: INTEGER
			-- Current value for max depth of serial output (element 
			-- recursion)
			-- Zero denotes 'unconstrained'
		do
			Result := serializer.serial_max_depth
		end

--|========================================================================
feature -- Serialization parameter setting
--|========================================================================

	set_attribute_separator (v: STRING)
			-- Set the string that separates attributes in serial output 
			-- to 'v'
		do
--			serializer.set_attribute_separator (v)
--			private_attribute_separator := v
			serial_params.set_attribute_separator (v)
		end

	--|--------------------------------------------------------------

	set_attribute_element_separator (v: STRING)
			-- Set the string that separates attributes from elements
			-- in serial output to 'v'
		do
			serial_params.set_attribute_element_separator (v)
		end

	--|--------------------------------------------------------------

	enable_include_elements_tag
			-- Enable including in serial output a separate elements prolog
			-- and epilog
		do
			serial_params.enable_include_elements_tag
		end

	disable_include_elements_tag
			-- Disable including in serial output a separate elements prolog
			-- and epilog
		do
			serial_params.disable_include_elements_tag
		end

	--|--------------------------------------------------------------

	set_tag_label_suppressed (tf: BOOLEAN)
			-- Set tag_label_suppressed to 'tf'
		do
			serial_params.set_tag_label_suppressed (tf)
		end

	invert_tag_filter
			-- Invert the sense of the attribute tag filter (if any)
			-- from include to exclude
		do
			serial_params.invert_tag_filter
		end

	revert_tag_filter
			-- Revert the sense of the attribute tag filter (if any)
			-- to include attributes matching tags
		do
			serial_params.revert_tag_filter
		end

	--|--------------------------------------------------------------

	set_serial_max_depth (v: INTEGER)
			-- Set the maximum depth (element recursion) to 'v'
		do
			serializer.set_max_depth (v)
		end

	unset_serial_max_depth (v: INTEGER)
			-- Set the maximum depth (element recursion) to unconstrained
		do
			serializer.unset_max_depth
		end

	--|--------------------------------------------------------------

	increase_indent
		do
			serializer.increase_indent
		end

	decrease_indent
		do
			serializer.decrease_indent
		end

--|========================================================================
feature {AEL_SPRT_ATTRIBUTE_SERIALIZER_2} -- External representation
--|========================================================================

	update_elements
			-- If necessary in anticipation of serializing 'elements',
			-- update the contents of 'elements'
			--
			-- To support both linear and hashed elements, the flags
			-- 'elements_is_a_table' and 'elements_is_a_list' are set
			-- here, at the start of serialization.
			-- Attachment tests drive type-equivalence functions
			-- 
			-- In redefined descendents, call Precursor after contents
			-- of elements are updated and cache is refreshed
		require
			has_structure: elements_is_a_table or elements_is_a_list
		do
			if elements_are_from_cache then
				update_elements_cache
			end
			if elements_is_a_table then
				update_elements_as_table
			else
				update_elements_as_list
			end
		end

	update_elements_as_list
			-- If necessary in anticipation of serializing 'elements',
			-- update the contents of 'elements_as_list'
		require
			is_list: elements_is_a_list
		do
			if attached {like elements_as_list} elements as el then
				elements_as_list := el
			end
		end

	update_elements_as_table
			-- If necessary in anticipation of serializing 'elements',
			-- update the contents of 'elements_as_table'
		require
			is_table: elements_is_a_table
		do
			if attached {like elements_as_table} elements as et then
				elements_as_table := et
			end
		end

	--|--------------------------------------------------------------

	set_element_structure
		local
			te: like elements
		do
			te := elements
			if attached {like elements_as_table} te as et then
				elements_is_a_table := True
				elements_is_a_list := False
				elements_as_table := et
			elseif attached {like elements_as_list} te as el then
				elements_is_a_table := False
				elements_is_a_list := True
				elements_as_list := el
			end
		ensure
			structure_defined: elements_is_a_table or elements_is_a_list
			single_structure: not (elements_is_a_table and elements_is_a_list)
		end

	--|--------------------------------------------------------------

	update_elements_cache
			-- If necessary in anticipation of serializing 'elements',
			-- update the contents of 'cached_serial_elements'
			--
			-- In some cases it might be advantageous to assemble and 
			-- cache elements
			-- To enable this, redefine elements_are_from_cache to True
			-- and redefine this routine to refresh the cached elements
		require
			uses_cache: elements_are_from_cache
		do
		ensure
			cache_exists: cached_serial_elements /= Void
		end

	--|--------------------------------------------------------------

	elements_serial_out (oa: like out_agent): STRING
			-- Serialized elements subordinate to client, if any
			-- 'oa' is an agent that generates serialized output per 
			-- element
			-- This allows serializer to manage the format, but Current
			-- is better suited to manage traversal of elements
		require
			function_exists: oa /= Void
			has_elements: has_elements
		do
			update_elements
			if elements_is_a_list then
				Result := element_list_serial_out (elements_as_list, oa)
			elseif elements_is_a_table then
				Result := element_table_serial_out (elements_as_table, oa)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attribute_separator: STRING
			-- String that separates attributes in serial output
		do
			Result := serial_params.attribute_separator
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attribute_element_separator: STRING
			-- String that separates attributes from elements
			-- in serial output
		do
			Result := serial_params.attribute_element_separator
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- Serial implementation
--|========================================================================

	element_list_serial_out (el: LIST [like element_type]; oa: like out_agent): STRING
			-- Serialized elements subordinate to client, if any
			-- 'oa' is an agent that generates serialized output per 
			-- element
			-- This allows serializer to manage the format, but Current
			-- is better suited to manage traversal of elements
		require
			list_exists: el /= Void
			function_exists: oa /= Void
			has_elements: has_elements
		local
			oc: CURSOR
			sf: BOOLEAN
		do
			create Result.make (128)
			oc := el.cursor
			from el.start
			until el.exhausted
			loop
				sf := el.index < el.count
				Result.append (element_sout (el.item, oa, sf))
				el.forth
			end
			el.go_to (oc)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	element_table_serial_out (et: like elements_as_table; oa: like out_agent): STRING
		require
			table_exists: et /= Void
			function_exists: oa /= Void
			has_elements: has_elements
		local
			oc: CURSOR
		do
			create Result.make (128)
			oc := et.cursor
			from et.start
			until et.after
			loop
				--sf := et.iteration_position < et.keys.count
				--Result.append (oa.item ([et.item_for_iteration, False]))
				Result.append (element_sout (et.item_for_iteration, oa, False))
				et.forth
				if not et.after then
					Result.append (serializer.element_separator)
				end
			end
			et.go_to (oc)
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {NONE} -- External representation
--|========================================================================

	element_sout (el: like element_type; oa: like out_agent; sf: BOOLEAN): STRING
			-- Serialized element 'el'
			-- 'oa' is an agent that generates serialized element output
		require
			element_exists: el /= Void
			function_exists: oa /= Void
		do
			Result := oa.item ([el, sf])
		end

--|========================================================================
feature -- Attribute setting
--|========================================================================

	set_attributes_from_current_values
			-- Add to attributes for each relevant feature of Current
			-- Example:
			--  add_attribute ("name", name)
		do
			if attributes = Void then
				create attributes.make (7)
			end
			--RFO Descendent should be responsible for removing
			-- any stale attributes
			--attributes.wipe_out
		ensure
			attrs_set: attributes /= Void
		end

	--|--------------------------------------------------------------

	add_attribute (t, v: STRING)
			-- Add to attributes an item with value 'v' by tag 't'
		require
			tag_exists: t /= Void and then not t.is_empty
			attributes_exist: attributes /= Void
		local
			ts: STRING
		do
			if v = Void then
				ts := ""
			else
				ts := v
			end
			-- Use force rather than extend, to overwrite any previous 
			-- values
			attributes.force (v, t)
		ensure
			added: has_attribute (t)
		end

	--|--------------------------------------------------------------

	set_value_for_attribute (t, v: STRING)
			-- For attribute with tag 't', set value to 'v'
			-- If no such attribute exists, create it now
		require
			tag_exists: t /= Void and then not t.is_empty
			attributes_exist: attributes /= Void
		do
			if attributes.has (t) then
				attributes.replace (v, t)
			else
--RFO 				attributes.extend (v, t)
				attributes.force (v, t)
			end
		ensure
			added: has_attribute (t)
		end

	--|--------------------------------------------------------------

	remove_attribute_by_name (t: STRING)
			-- Remove attribute with tag 't'
		require
			tag_exists: t /= Void and then not t.is_empty
			attributes_exist: attributes /= Void
		do
			if attributes.has (t) then
				attributes.remove (t)
			end
		ensure
			removed: not has_attribute (t)
		end

	--|--------------------------------------------------------------

	add_element_by_name (v: like element_by_name; nm: STRING)
			-- Add to elements item 'v'
		require
			exists: v /= Void
			has_elements: has_elements
		local
			el: like elements_as_list
			et: like elements_as_table
		do
			if elements_is_a_list then
				el := elements_as_list
				el.extend (v)
			elseif elements_is_a_table then
				et := elements_as_table
--RFO 				et.extend (v, nm)
				et.force (v, nm)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_tag_filter (v: like tag_filter)
			-- Set 'tag_filter' to 'v'
		require
			valid_comparison: v /= Void implies v.object_comparison
		do
			tag_filter := v
		end

	unset_tag_filter
		do
			tag_filter := Void
		end

	set_item_tag_filter (v: like item_tag_filter)
			-- Set 'item_tag_filter' to 'v'
		require
			valid_comparison: v /= Void implies v.object_comparison
		do
			item_tag_filter := v
		end

	unset_item_tag_filter
		do
			item_tag_filter := Void
		end

	invert_item_tag_filter
			-- Invert the sense of the item tag filter (if any)
			-- from include to exclude
		do
			serial_params.invert_item_tag_filter
		end

	revert_item_tag_filter
			-- Revert the sense of the item tag filter (if any)
			-- to include attributes matching tags
		do
			serial_params.revert_item_tag_filter
		end

	--|--------------------------------------------------------------

	set_serial_element_range (v: like serial_element_range)
			-- Set 'serial_element_range' to 'v'
		require
			has_elements: has_elements
		do
			serial_element_range := v
		end

	unset_serial_element_range
		require
			has_elements: has_elements
		do
			serial_element_range := Void
		end

--|========================================================================
feature {AEL_SPRT_ATTRIBUTE_SERIALIZER_2} -- Implementation
--|========================================================================

	tag_lookup_function: FUNCTION [STRING, STRING]
			-- Function to call to find attribute value that is not
			-- otherwise directly accessible from 'attributes'
			-- Useful for derived values and custom representations
		do
		end

--|========================================================================
feature -- Validation
--|========================================================================

	elements_is_valid: BOOLEAN
			-- Is the 'elements' feature valid?
		do
			Result := elements_has_valid_structure
		end

	elements_has_valid_structure: BOOLEAN
			-- Does elements, if defined, have a valid structure?
			-- Elements can be lists or hash tables, but if hash tables, 
			-- items in elements must be identifiable and keyed by their 
			-- respective identifiers
		require
			created: default_created
		do
			if not has_elements then
				Result := True
			else
				Result := elements_is_a_list or elements_is_a_table
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	list_element_by_name (v: STRING): like element_by_name
			-- Element with name 'v' if any, when 'elements' is a list
		require
			is_list: elements_is_a_list
		local
			oc: CURSOR
			el: like elements_as_list
		do
			el := elements_as_list
			if el /= Void then
				oc := el.cursor
				from el.start
				until el.exhausted or Result /= Void
				loop
					if attached {AEL_SPRT_IDENTIFIABLE_ATTRIBUTABLE_2} el.item as te
					 then
						if te.identifier ~ v then
							Result := te
						end
					end
					el.forth
				end
				el.go_to (oc)
			end
		end

	--|--------------------------------------------------------------

	table_element_by_name (v: STRING): like element_by_name
			-- Element with name 'v' if any, when 'elements' is a table
		require
			is_table: elements_is_a_table
		do
			Result := elements_as_table.item (v)
		end

	--|--------------------------------------------------------------

	elements_as_list: LIST [like element_type]
			-- If 'elements' is a LIST, then a LIST handle to 'elements'

	elements_as_table: HASH_TABLE [like element_type, STRING]
			-- If 'elements' is a HASH_TABLE of identifiable items,
			-- then a HASH_TABLE handle to 'elements'

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	out_agent: FUNCTION [BOOLEAN, STRING]
		-- Type anchor for agent to output an element
		do
		ensure
			False
		end

	frozen element_type: AEL_SPRT_ATTRIBUTABLE_2
		-- Type anchor for generic element
		do
		ensure
			False
		end

--|========================================================================
feature {NONE} -- Services
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant
	created: (not is_initializing) implies default_created
	has_attributes: attributes /= Void
	has_serial_params: default_created implies serial_params /= Void
	serializer_valid: default_created implies has_valid_serializer
	elements_valid: (default_created and has_elements) implies elements_is_valid

end -- class AEL_SPRT_ATTRIBUTABLE_2
