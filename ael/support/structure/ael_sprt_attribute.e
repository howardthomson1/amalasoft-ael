class AEL_SPRT_ATTRIBUTE
-- A simple tag and value reprsenting a named attribute

inherit
	COMPARABLE
		redefine
			default_create
		end
	AEL_SPRT_FALLIBLE
		undefine
			is_equal
		redefine
			default_create
		end

create
	make, make_with_tag_and_value

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create
			-- Create Current in default state
		do
			create tag.make (0)
			create value.make (0)
		end

	--|--------------------------------------------------------------

	make (t: STRING)
			-- Create Current with tag 't'
		do
			default_create
			set_tag (t)
		end

	--|--------------------------------------------------------------

	make_with_tag_and_value (t, v: STRING)
			-- Create Current from the given tag and value components
		require
			valid_tag: t /= Void and then not t.is_empty
			value_exists: v /= Void
		do
			make (t)
			set_value (v)
		ensure
			tag_set: tag ~ t
			value_set: value ~ v
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_value (v: STRING)
			-- Set value from 'v'
		require
			value_exists: v /= Void
		do
			value.wipe_out
			value.append (v)
		ensure
			value_set: value ~ v
		end

--|========================================================================
feature {NONE} -- Private status setting
--|========================================================================

	set_tag (v: STRING)
			-- Set tag from 'v'
		require
			valid_tag: v /= Void and then not v.is_empty
		do
			tag.wipe_out
			tag.append (v)
		ensure
			tag_set: tag ~ v
		end

--|========================================================================
feature -- Access
--|========================================================================

	tag: STRING
			-- Name used to identify Current

	value: STRING
			-- Value associated with Current

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := tag > other.tag
		end

	--|--------------------------------------------------------------
invariant
	tag_exists: tag /= Void
	valid_tag: not has_error implies not tag.is_empty
	value_exists: value /= Void

end -- class AEL_SPRT_ATTRIBUTE
