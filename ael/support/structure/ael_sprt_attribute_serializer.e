note
	description: "{
Abstract notion of having attributes that are settable from a serialized
string form, and that can be presented in a serialized string form.
}"

deferred class AEL_SPRT_ATTRIBUTE_SERIALIZER

--|========================================================================
feature -- Status
--|========================================================================

	has_attribute_by_name (al: like attrs; v: STRING): BOOLEAN
 			-- Does attribute table have an attribute with name 'v'?
 		require
 			exists: v /= Void
 		do
 			Result := al.has (v)
 		end

	attribute_tags_are_quoted: BOOLEAN
			-- Should there be quotes around attribute tags?
		do
			Result := value_for_flag ("attribute_tags_are_quoted")
		end

	element_tags_are_quoted: BOOLEAN
			-- Should there be quotes around element tags?
		do
			Result := value_for_flag ("element_tags_are_quoted")
		end

	--|--------------------------------------------------------------

	value_for_flag (nm: STRING): BOOLEAN
			-- Value for flag by name 'nm'
			-- Result is also false if flag is unknown
		do
			Result := shared_flags.item (nm)
		end

	value_for_pattern (nm: STRING): STRING
			-- Value for pattern by name 'nm'
			-- Result is empty if pattern is unknown
		do
			if attached shared_patterns.item (nm) as ls then
				Result := ls
			else
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- External representation
--|========================================================================

	serial_out (cl: like client; sp: AEL_SPRT_SERIAL_PARAMS): STRING
			-- Serialized string representation of attributes
		require
			client_exists: cl /= Void
		do
			create Result.make (128)
			cl.set_attributes_from_current_values
			increase_serial_depth
			if include_element_id then
				increment_element_id
			end
			Result.append (serial_out_prolog (cl))
			if attached cl.attribute_values as lca then
				Result.append (attributes_serial_out (cl, lca))
			end
			Result.append (serial_out_prolog_end (cl))

			if (current_serial_depth < serial_max_depth or
				serial_max_depth = 0) and sp.has_elements
			 then
				 expand_element_id
				if sp.include_elements_tag then
					increase_indent
					if cl.serial_elements_is_empty then
						Result.append (sp.attribute_element_separator)
					end
					Result.append (elements_out_prolog (sp))
				end
				Result.append (elements_out (cl))
				if sp.include_elements_tag then
					Result.append (elements_out_epilog (sp))
					decrease_indent
				end
			end
			contract_element_id
			Result.append (serial_out_epilog (cl))
			decrease_serial_depth
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature {AEL_SPRT_ATTRIBUTABLE} -- External representation
--|========================================================================

	elements_out (cl: like client): STRING
			-- Serialzed elements subordinate to client, if any
		require
			client_exists: cl /= Void
			has_elements: cl.has_elements
		do
			Result := cl.elements_serial_out (agent element_out (cl, ?, ?))
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	element_out (cl: like client; v: AEL_SPRT_ATTRIBUTABLE; sf: BOOLEAN): STRING
			-- Serialzed element subordinate to client
		require
			exists: v /= Void
			client_exists: cl /= Void
			has_elements: cl.has_elements
		local
			nrf: BOOLEAN
			sp: AEL_SPRT_SERIAL_PARAMS
		do
			sp := cl.serial_params
			increase_indent
			-- N.B. Call is to v.serial_out rather than serial_out (v)
			-- so that 'v' can perform any pre-serialization checks
			-- or settings
			nrf := v.tag_label_suppressed
			if sp.elements_as_array_enabled then
				v.set_tag_label_suppressed (True)
			end
			Result := v.serial_out
			v.set_tag_label_suppressed (nrf)

			if sf then
				Result.append (element_separator)
			end
			decrease_indent
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	serial_out_prolog (cl: like client): STRING
			-- String that precedes attributes in serial output
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	serial_out_prolog_end (cl: like client): STRING
			-- String the terminates the serial_out_prolog,
			-- after attributes have been added
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	serial_out_epilog (cl: like client): STRING
			-- String that follows attributes and elements in serial output
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	elements_out_prolog (sp: AEL_SPRT_SERIAL_PARAMS): STRING
			-- String that precedes elements in serial output
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	elements_out_epilog (sp: AEL_SPRT_SERIAL_PARAMS): STRING
			-- String that follows elements in serial output
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	element_separator: STRING
			-- String that separates elements in serial output
		do
			Result := value_for_pattern ("element_separator")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attributes_serial_out (cl: like client; al: like attrs): STRING
			-- Serialized representation of attributes
		require
			attrs_exist: al /= Void
		local
			ts: STRING
			oc: CURSOR
			on_separate_lines: BOOLEAN
			sp: AEL_SPRT_SERIAL_PARAMS
			sl: SORTED_TWO_WAY_LIST [AEL_SPRT_TV_PAIR]
			tv: AEL_SPRT_TV_PAIR
			fl: TWO_WAY_LIST [AEL_SPRT_TV_PAIR] -- filtered list
			til: AEL_DS_LINKED_LIST [STRING] -- inclusion list
			txl: AEL_DS_LINKED_LIST [STRING] -- exclusion list
		do
			sp := cl.serial_params
			create til.make
			til.compare_objects
			if attached cl.tag_filter as ltf and not sp.tag_filter_inverted then
				if attached sp.include_exclude_attrs as lsx and
					not sp.exclude_inex_attrs
				 then
					 til.fill (lsx)
				end
				til.fill_unique (ltf)
				Result := attributes_out_by_tag (cl, al, til)
			elseif attached sp.include_exclude_attrs as lsx and
				not sp.exclude_inex_attrs
			 then
				 Result := attributes_out_by_tag (cl, al, lsx)
			else
				-- Is either unfiltered or filter is exclusive
				create Result.make (128)
				create txl.make
				txl.compare_objects
				if attached sp.include_exclude_attrs as lsx and
					sp.exclude_inex_attrs
				 then
					txl.fill (lsx)
				end
				if attached cl.tag_filter as ltf and sp.tag_filter_inverted then
					txl.fill_unique (ltf)
				end
				oc := al.cursor
				on_separate_lines := sp.attribute_separator.has ('%N')
				if include_element_id then
					--RFO for unique node IDs in output
					Result.append (left_pad)
					Result.append (attribute_out (sp, "serialelementid", element_id))
					if not al.is_empty then
						Result.append (sp.attribute_separator)
					elseif cl.has_elements and then not cl.serial_elements_is_empty then
						Result.append (sp.attribute_element_separator)
					end
				end
				if sp.attributes_sorted then
					create sl.make
					fl := sl
				else
					create fl.make
				end
				from al.start
				until al.after
				loop
					ts := al.item_for_iteration
					if ts = Void then
						ts := ""
					end
					if txl /= Void and then txl.has (al.key_for_iteration) then
						-- Ommit matching attr
					else
						create tv.make_from_tag_and_value (al.key_for_iteration, ts)
						fl.extend (tv)
					end
					al.forth
				end
				al.go_to (oc)
				from fl.start
				until fl.exhausted
				loop
					tv := fl.item
					if Result.is_empty or on_separate_lines then
						Result.append (left_pad)
					end
					Result.append (attribute_out (sp, tv.tag, tv.value))
					fl.forth
					if not fl.after then
						Result.append (sp.attribute_separator)
					elseif cl.has_elements and then
						not cl.serial_elements_is_empty then
							Result.append (sp.attribute_element_separator)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	attributes_out_by_tag (
		cl: like client; al: like attrs; ll: LIST [STRING]): STRING
			-- Attributes corresponding to the given labels 'll' only
			-- If tag_filter_inverted, then all attributes _except_ 
			-- those corresponding to given labels 'll'
		require
			attrs_exist: al /= Void
			labels_exist: ll /= Void
			valid_comparison: ll.object_comparison
		local
			tt, ts: STRING
			oc: CURSOR
			on_separate_lines: BOOLEAN
			sp: AEL_SPRT_SERIAL_PARAMS
		do
			sp := cl.serial_params
			create Result.make (128)
			on_separate_lines := sp.attribute_separator.has ('%N')
			if include_element_id then
				--RFO for unique node IDs in output
				Result.append (left_pad)
				Result.append (attribute_out (sp, "serialelementid", element_id))
				if not al.is_empty then
					Result.append (sp.attribute_separator)
				elseif cl.has_elements then
					Result.append (sp.attribute_element_separator)
				end
			end
			oc := ll.cursor
			from ll.start
			until ll.exhausted
			loop
				tt := ll.item
				if cl.has_attribute (tt) then
					ts := cl.value_for_attribute (tt)
					if ((not include_element_id) and ll.isfirst) or
						on_separate_lines
					 then
						 Result.append (left_pad)
					end
					Result.append (attribute_out (sp, tt, ts))
				end
				ll.forth
				if not Result.is_empty then
					if not ll.after then
						Result.append (sp.attribute_separator)
					elseif cl.has_elements then
						--RFO Is this supposed to check also for empty elements?
						Result.append (sp.attribute_element_separator)
					end
				end
			end
			ll.go_to (oc)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	obs_value_for_attribute (cl: like client; al: like attrs; t: STRING): STRING
		do
			if attached al.item (t) as lt then
				Result := lt
			else
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attribute_out (sp: AEL_SPRT_SERIAL_PARAMS; t, v: STRING): STRING
			-- Attribute, represented by tag 't' and value 'v' in
			-- string serialized form
		require
			has_params: sp /= Void
			valid_tag: t /= Void and then not t.is_empty
		deferred
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	pad_string: STRING
			-- Per-level string to pad left indent
		do
			Result := "  "
		end

	left_pad: STRING
			-- Padding string to use when generating serial output
		do
			Result := value_for_pattern ("left_pad")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	include_element_id: BOOLEAN
		do
			Result := value_for_flag ("include_element_id")
		end

	element_id: STRING
			-- Current element's node ID
			-- ID is hierarchical string of dot-separated numbers
			-- Example:  "1.1.2.1"
			-- The number of fields represents the depth of indentation
			-- The first node at a level is "1", second is "2", etc
			-- Subordinate elements are dotted relative to their parents.
		do
			Result := value_for_pattern ("element_id")
		ensure
			exists: Result /= Void
		end

	element_id_depth: INTEGER
			-- Depth/complexity of current element id
			-- Each field in the id represents a level of depth
		do
			Result := element_id.occurrences ('.') + 1
		end

	--|--------------------------------------------------------------

	indent_width: INTEGER
		do
			Result := left_pad.count
		end

	--|--------------------------------------------------------------

	serial_max_depth: INTEGER
			-- Maximum element depth for serial out
			-- Zero denotes 'no maximum'
		local
			ts: STRING
		do
			ts := value_for_pattern ("serial_max_depth")
			if ts.is_integer then
				Result := ts.to_integer
			end
		ensure
			valid_depth: Result >= 0
		end

--|========================================================================
feature -- Status
--|========================================================================

	current_serial_depth: INTEGER
			-- Current element depth in serial out
		do
			Result := shared_current_serial_depth.item
		ensure
			valid_depth: Result >= 0
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	reset_element_ids
			-- Reset shared element ID to initial value
		do
			set_shared_pattern ("element_id", "0")
		end

	--|--------------------------------------------------------------

	increment_element_id
			-- Increment the individual serial node element ID
		local
			ts, bs, es: STRING
			ep, ti: INTEGER
		do
			if include_element_id then
				ts := element_id
				if ts.has ('.') then
					ep := ts.last_index_of ('.', ts.count)
					bs := ts.substring (1, ep - 1)
					es := ts.substring (ep + 1, ts.count)
					ti := es.to_integer + 1
					ts := bs + "." + ti.out
				else
					-- A top-level element
					ti := ts.to_integer + 1
					ts := ti.out
				end
				set_shared_pattern ("element_id", ts)
			end
		end

	expand_element_id
			-- Add a subfield to the serial node element ID
		do
			if include_element_id then
				set_shared_pattern ("element_id", element_id + ".1")
			end
		ensure
			id_increased: include_element_id implies
				(element_id ~ ((old element_id) + ".1"))
		end

	contract_element_id
			-- Truncate the serial node element ID by one field
		local
			ts: STRING
			ep: INTEGER
		do
			if include_element_id then
				ts := element_id
				if ts.has ('.') then
					ep := ts.last_index_of ('.', ts.count)
					set_shared_pattern ("element_id", ts.substring (1, ep-1))
				else
					-- Something is wrong
				end
			end
		ensure
			id_decreased: include_element_id implies
				(element_id.occurrences ('.') = ((old element_id).occurrences ('.')) - 1)
		end

	--|--------------------------------------------------------------

	reset_serial_depth
			-- Reset shared serial depth to initial value
		do
			shared_current_serial_depth.put (0)
		end

	--|--------------------------------------------------------------

	increase_serial_depth
			-- Increment current_serial_depth
		do
			shared_current_serial_depth.put (current_serial_depth + 1)
		ensure
			incremented: current_serial_depth = old current_serial_depth + 1
		end

	decrease_serial_depth
			-- Decrement current_serial_depth
		require
			decrementable: current_serial_depth > 0
		do
			shared_current_serial_depth.put (current_serial_depth - 1)
		ensure
			decremented: current_serial_depth = old current_serial_depth - 1
		end

	--|--------------------------------------------------------------

	increase_indent
			-- Increase the indentation level to denote descending into 
			-- a subordinate element
		do
			left_pad.append (pad_string)
		ensure
			increased: left_pad.count = (old left_pad.count) + pad_string.count
		end

	--|--------------------------------------------------------------

	decrease_indent
		local
			len: INTEGER
		do
			len := left_pad.count
			left_pad.keep_head (len - pad_string.count)
		ensure
			decreased: left_pad.count = (old left_pad.count) - pad_string.count
		end

	--|--------------------------------------------------------------

	disable_attribute_tag_quotes
			-- Disable quotes around attribute tags
		do
			set_shared_flag ("attribute_tags_are_quoted", False)
		end

	disable_element_tag_quotes
			-- Disable quotes around element tags
		do
			set_shared_flag ("element_tags_are_quoted", False)
		end

	--|--------------------------------------------------------------

	enable_attribute_tag_quotes
			-- Enable quotes around attribute tags
		do
			set_shared_flag ("attribute_tags_are_quoted", True)
		end

	enable_element_tag_quotes
			-- Enable quotes around element tags
		do
			set_shared_flag ("element_tags_are_quoted", True)
		end

	--|--------------------------------------------------------------

	set_shared_flag (nm: STRING; tf: BOOLEAN)
			-- Add shared flag by name 'nm', with state 'tf'
			-- Example: set_flag ("tag_label_suppressed", True)
		do
			shared_flags.force (tf, nm)
		end

	--|--------------------------------------------------------------

	set_shared_pattern (nm, p: STRING)
			-- Add shared pattern by name 'nm', with value 'p'
			-- Example: set_shared_pattern ("serial_out_prolog", "{")
		do
			shared_patterns.force (p, nm)
		end

	--|--------------------------------------------------------------

	remove_shared_pattern (nm: STRING)
			-- Remove from shared patterns item by name 'nm'
		do
			if shared_patterns.has (nm) then
				shared_patterns.remove (nm)
			end
		end

	set_max_depth (v: INTEGER)
		require
			valid: v >= 0
		do
			shared_patterns.force (v.out, "serial_max_depth")
		end

	unset_max_depth
		do
			shared_patterns.force ("0", "serial_max_depth")
		end

--|========================================================================
feature -- Access
--|========================================================================

 	attribute_by_name (al: like attrs; v: STRING): detachable AEL_SPRT_ATTRIBUTE
 			-- Attribute name and value associated with label 'v', if any
 		require
 			exists: v /= Void
 		do
 			if attached al.item (v) as ts then
 				create Result.make_with_tag_and_value (v, ts)
 			end
 		ensure
 			exists_if_has: has_attribute_by_name (al,v) implies Result /= Void
 		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	shared_flags: HASH_TABLE [BOOLEAN, STRING]
			-- Flags shared by all instances of this class or its 
			-- descendents
		once
			create Result.make (11)
			--RFO let descendents decide
			--Result.force (True, "include_element_id")
		end

	--|--------------------------------------------------------------

	shared_patterns: HASH_TABLE [STRING, STRING]
			-- Patterns (strings) shared by all instances of this
			-- class or its descendents
		once
			create Result.make (11)
			Result.force ("", "left_pad")
			Result.force ("0", "element_id")
			Result.force ("0", "serial_max_depth")
			Result.force ("", "element_separator")
		end

	--|--------------------------------------------------------------

	shared_current_serial_depth: CELL [INTEGER]
		once
			create Result.put (0)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_input_stream (v: STRING): BOOLEAN
			-- Is the string 'v' a valid stream from which to initialize 
			-- attributes associated with Current?
		do
			if v /= Void and then not v.is_empty then
				Result := True
			end
		ensure
			if_exists: Result implies v /= Void and then not v.is_empty
		end

--|========================================================================
feature -- Services
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

--|========================================================================
feature {NONE} -- Type anchors
--|========================================================================

	attrs (al: like attrs): HASH_TABLE [STRING, STRING]
			-- Dummy function for type anchor
		do
			Result := al
		end

	client (cl: like client): AEL_SPRT_ATTRIBUTABLE
		do
			Result := cl
		end

	--|--------------------------------------------------------------
invariant

end -- class class AEL_SPRT_ATTRIBUTE_SERIALIZER
