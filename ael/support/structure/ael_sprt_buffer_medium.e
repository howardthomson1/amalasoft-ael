class AEL_SPRT_BUFFER_MEDIUM
-- An IO Medium implemented as an Eiffel STRING

inherit
	IO_MEDIUM
		undefine
			out
		end
	AEL_SPRT_FALLIBLE
		redefine
			out
		end

create
	make,
	make_with_size

 --|========================================================================
feature -- Initialization
 --|========================================================================

	make
			-- Create Current with a default initial capacity
		do
			make_with_size (1024)
		end

	--|--------------------------------------------------------------

	make_with_size (n: INTEGER)
			-- Create Current with an initial capacity of `n' bytes
		require
			valid_size: n >= 0
		do
			create buffer.make (n)
			create last_string.make_empty
		end

 --|========================================================================
feature -- Status report
 --|========================================================================

	support_storable: BOOLEAN = False
			-- Can medium be used to store an Eiffel structure? Nope.

	cursor_is_valid: BOOLEAN
			-- Is Cursor valid?
		do
			if cursor >= 0 and cursor <= (count + 1) then
				if is_empty then
					Result := cursor = 0
				else
					Result := cursor > 0
				end
			end
		end

	--|--------------------------------------------------------------

	before, off_left: BOOLEAN
			-- Is cursor off left?
		do
			Result := cursor = 0
		end

	--|--------------------------------------------------------------

	after, off_right: BOOLEAN
			-- Is cursor off right?
		do
			Result := (is_empty and cursor = 0) or cursor = buffer.count + 1
		end

	--|--------------------------------------------------------------

	out: STRING
			-- Current contents, regardless of cursor position
		do
			Result := buffer.twin
		end

	out_after: STRING
			-- Current contents, from cursor rightward
		do
			create Result.make (0)
			if not buffer.is_empty and cursor <= buffer.count then
				Result := buffer.substring (cursor, buffer.count)
			end
		ensure
			exists: Result /= Void
		end

 --|========================================================================
feature -- Access
 --|========================================================================

	cursor: INTEGER
			-- Position in buffer from which next IO operation will begin
			-- When empty, cursor=0
			-- Cursor is advanced on writes and reads, to first position 
			-- after the last position read or written
			-- On appends, cursor moves off right (=count + 1)
			-- Writes insert/append items at cursor, shifting any items 
			-- rightward items farther rightward.
			-- Reads begin with item at cursor and move the cursor 
			-- rightward by the number of items read

 --|========================================================================
feature -- Status report
 --|========================================================================

	count: INTEGER
			-- Size of stream
		do
			Result := buffer.count
		end

	is_empty: BOOLEAN
			-- Is Current devoid of content?
		do
			Result := buffer.is_empty
		end

	bytes_remaining: INTEGER
			-- Number of bytes (chars) in buffer from current position 
			-- through end of buffer, inclusive
		do
			if (not is_empty) and cursor <= count then
				Result := (count - cursor) + 1
			end
		end

	--|--------------------------------------------------------------

	exists: BOOLEAN = True
			-- Stream exists in any cases.

	is_open_read: BOOLEAN
			-- Is stream open for input?
		do
			Result := not is_closed
		end

	is_open_write: BOOLEAN
			-- Is stream open for output?
		do
			Result := not is_closed
		end

	is_readable: BOOLEAN = True

	is_executable: BOOLEAN
			-- Is stream executable? Nope.

	is_writable: BOOLEAN = True
			-- Stream is writable.

	readable: BOOLEAN
			-- Is there a current item that may be read?
		do
			Result := count > 0 and cursor <= count
		end

	extendible: BOOLEAN
			-- May new items be added?
		do
			Result := True
		end

	is_closed: BOOLEAN
			-- Is the I/O medium not open?

 --|========================================================================
feature -- Status setting
 --|========================================================================

	close
			-- Close medium.
		do
			is_closed := True
			buffer.wipe_out
			cursor := 0
			clear_error
		end

	reopen, open
			-- Open medium (is open in default state)
		do
			is_closed := False
			buffer.wipe_out
			set_cursor (0)
			clear_error
		end

	start
			-- Set cursor to start of stream
		require
			not_empty: not is_empty
		do
			set_cursor (1)
		end

	set_cursor (p: INTEGER)
			-- Set the cursor to position 'p'
		require
			valid_if_empty: is_empty implies p = 0
			valid_not_empty: (not is_empty) implies p > 0 and p <= (count + 1)
		do
			cursor := p
		ensure
			is_set: cursor = p
			is_valid: cursor_is_valid
		end

 --|========================================================================
feature -- Output
 --|========================================================================

	put_new_line, new_line
			-- Write a new line character to medium at cursor
		require else
			stream_exists: exists
		do
			write_char ('%N')
		end

	put_string, putstring (v: STRING)
			-- Write 'v' to medium at cursor, advancing cursor
		do
			write (v)
		end

	put_character, putchar (v: CHARACTER)
			-- Write 'v' to medium at cursor, advancing cursor
		do
			write_char (v)
		end

	put_real, putreal (v: REAL)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_integer, putint, put_integer_32 (v: INTEGER)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_integer_8 (v: INTEGER_8)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_integer_16 (v: INTEGER_16)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_integer_64 (v: INTEGER_64)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_natural_8 (v: NATURAL_8)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_natural_16 (v: NATURAL_16)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_natural, put_natural_32 (v: NATURAL_32)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_natural_64 (v: NATURAL_64)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_boolean, putbool (v: BOOLEAN)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_double, putdouble (v: DOUBLE)
			-- Write 'v' to medium at cursor, advancing cursor.
		do
			write (v.out)
		end

	put_managed_pointer (p: MANAGED_POINTER; start_pos, nb_bytes: INTEGER)
			-- Put data of length `nb_bytes' pointed by `start_pos' index in `p' at
			-- current position.
		do
		end

 --|========================================================================
feature -- Input
 --|========================================================================

	read_real, readreal
			-- Read an ASCII representation of a real number from stream.
			-- Make result available in `last_real'.
			-- Real has the form:
			--   <whole_part>[<decimal>[<fractional_part>]]
			-- Whole part and fractional part must be within range
			-- If cursor is not at a real, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_float (32)
			last_real := last_double_result.truncated_to_real
		end

	read_double, readdouble
			-- Read an ASCII representation of a double from stream.
			-- Make result available in `last_double'.
			-- Double has the form:
			--   <whole_part>[<decimal>[<fractional_part>]]
			-- Whole part and fractional part must be within range
			-- If cursor is not at a double, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_float (64)
			last_double := last_double_result
		end

	read_character, readchar
			-- Read a new character.
			-- Make result available in `last_character'.
		do
			read (1)
			last_character := last_read_result.item (1)
		end

	read_integer, readint, read_integer_32
			-- Read an ASCII representation of an integer from stream.
			-- Make result available in `last_integer'.
			-- Decimal format only.
			-- If cursor is not at an integer, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (32, True)
			if not has_error then
				last_integer := last_integer_result.as_integer_32
			end
		end

	read_integer_8
			-- Read an ASCII representation of an integer_8 from stream.
			-- Make result available in `last_integer_8'.
			-- Decimal format only.
			-- If cursor is not at an integer_8, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (8, True)
			if not has_error then
				last_integer_8 := last_integer_result.as_integer_8
			end
		end

	read_integer_16
			-- Read an ASCII representation of an integer_16 from stream.
			-- Make result available in `last_integer_16'.
			-- Decimal format only.
			-- If cursor is not at an integer_16, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (16, True)
			if not has_error then
				last_integer_16 := last_integer_result.as_integer_16
			end
		end

	read_integer_64
			-- Read an ASCII representation of an integer_64 from stream.
			-- Make result available in `last_integer_64'.
			-- Decimal format only.
			-- If cursor is not at an integer_64, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (64, True)
			if not has_error then
				last_integer_64 := last_integer_result
			end
		end

	read_natural_8
			-- Read an ASCII representation of a natural_8 from stream.
			-- Make result available in `last_natural_8'.
			-- Decimal format only.
			-- If cursor is not at a natural_8, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (8, False)
			if not has_error then
				last_natural_8 := last_natural_result.as_natural_8
			end
		end

	read_natural_16
			-- Read an ASCII representation of a natural_16 from stream.
			-- Make result available in `last_natural_16'.
			-- Decimal format only.
			-- If cursor is not at a natural_16, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (16, False)
			if not has_error then
				last_natural_16 := last_natural_result.as_natural_16
			end
		end

	read_natural, read_natural_32
			-- Read an ASCII representation of a natural_32 from stream.
			-- Make result available in `last_natural_32'.
			-- Decimal format only.
			-- If cursor is not at a natural_32, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (32, False)
			if not has_error then
				last_natural := last_natural_result.as_natural_32
			end
		end

	read_natural_64
			-- Read an ASCII representation of a natural_64 from stream.
			-- Make result available in `last_natural_64'.
			-- Decimal format only.
			-- If cursor is not at a natural_64, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		do
			read_sized_integer (64, False)
			if not has_error then
				last_natural_64 := last_natural_result
			end
		end

	read_stream, readstream (n: INTEGER)
			-- Read a string of at most `n' bound characters
			-- or until end of medium is encountered.
			-- Make result available in `last_string'.
		do
			read (n)
			if not has_error then
				last_string.append (last_read_result)
			end
		end

	read_line, readline
			-- Read characters until a new line or
			-- end of medium.
			-- Make result available in `last_string'.
			-- Newline character is consumed by read, but it not present 
			-- in last_string
		local
			ep: INTEGER
		do
			ep := buffer.index_of ('%N', cursor)
			read (ep - cursor + 1)
			last_string.append (last_read_result)
			last_string.keep_head (last_string.count - 1)
		end

	read_to_managed_pointer (p: MANAGED_POINTER; start_pos, nb_bytes: INTEGER)
			-- Read at most `nb_bytes' bound bytes and make result
			-- available in `p' at position `start_pos'.
		do
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	buffer: STRING
			-- Direct access to stored/retrieved data

	last_read_result: STRING
			-- The last string read by any read operation
		do
			Result := ""
			if attached private_last_read_result as lrr then
				Result := lrr
			end
		end

	private_last_read_result: detachable STRING
			-- The last string read by any read operation

	last_integer_result: INTEGER_64
			-- The last integer read by any integer read operation

	last_natural_result: NATURAL_64
			-- The last natural read by any natural read operation

	last_double_result: DOUBLE
			-- The last floating point read by any fp read operation

	--|--------------------------------------------------------------

	read (n: INTEGER)
			-- Read 'n' characters, moving the cursor 'n' places
		require
			valid_count: n >= 0
		local
			sp, ep: INTEGER
		do
			sp := cursor
			-- Reset all 'last' values
			last_string.wipe_out
			last_character := '%U'
			last_integer := 0
			last_integer_8 := 0
			last_integer_16 := 0
			last_integer_64 := 0
			last_natural := 0
			last_natural_8 := 0
			last_natural_16 := 0
			last_natural_64 := 0
			last_real := 0.0
			last_double := 0.0
			if n /= 0 then
				ep := (cursor + n.min (bytes_remaining)) - 1
				private_last_read_result := buffer.substring (cursor, ep)
				cursor := (ep + 1).min (count + 1)
			end
		ensure
			cursor_moved_near_end: ((old cursor) + n) > count implies after
			cursor_moved: ((old cursor) + n) <= count implies
				cursor = (old cursor) + n
		end

	--|--------------------------------------------------------------

	read_sized_integer (sz: INTEGER; is_signed: BOOLEAN)
			-- Read an ASCII representation of an integer_'sz' from 
			-- stream.
			-- Treat value as signed if 'is_signed', or unsigned if not 
			-- (aka 'natural')
			-- Make result available in last_integer_'sz' or 
			-- last_natural_'sz' as appropriate.
			-- 
			-- Decimal format only.
			--
			-- If cursor is not at an integer, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		require
			valid_integer_size: sz = 8 or sz = 16 or sz = 32 or sz = 64
		local
			nd: INTEGER
			maxn: NATURAL_64
			maxi: INTEGER_64
		do
			-- Find the extent, if any, of the integer from the current
			-- cursor position to the next non-compliant character and
			-- use the core read routine to handle the actual read and
			-- cursor placement.
			-- Decimal integer values can have up to 'nd' digits, and a 
			-- sign
			inspect sz
			when 8 then -- 127/255
				nd := 3
				maxi := {INTEGER_8}.max_value
				maxn := {NATURAL_8}.max_value
			when 16 then -- 32767/65535
				nd := 5
				maxi := {INTEGER_16}.max_value
				maxn := {NATURAL_16}.max_value
			when 32 then -- 2147483647/4294967295
				nd := 10
				maxi := {INTEGER_32}.max_value
				maxn := {NATURAL_32}.max_value
			when 64 then -- 9223372036854775807/18446744073709551614
				if is_signed then
					nd := 19
				else
					nd := 20
				end
				maxi := {INTEGER_64}.max_value
				maxn := {NATURAL_64}.max_value
			end -- NO ELSE CASE
			read_sized_decimal_sequence (nd, is_signed)
			if not has_error then
				if is_signed then
					if not last_read_result.is_integer_64 then
						set_error_message ("Not an integer")
					else
						last_integer_result := last_read_result.to_integer_64
						if last_integer_result > maxi then
							set_error_message ("Integer value exceeds range")
						end
					end
				else
					if not last_read_result.is_natural_64 then
						set_error_message ("Not a natural")
					else
						last_natural_result := last_read_result.to_natural_64
						if last_natural_result > maxn then
							set_error_message ("Natural value exceeds range")
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	read_sized_decimal_sequence (sz: INTEGER; is_signed: BOOLEAN)
			-- Read a sequence of at most 'sz' decimal digits from stream.
			-- If cursor is not at an integer, then error is set and cursor 
			-- remains as it was before the read attempt
			-- On success, cursor is moved to first position after value 
			-- just read.
		require
			valid_size: sz > 0 and sz <= 35
		local
			c: CHARACTER
			n, i, lim: INTEGER
			is_int: BOOLEAN
		do
			lim := buffer.count.min (cursor + sz)
			c := buffer.item (cursor)
			if is_signed and c = '-' then
				if cursor < count and then buffer.item (cursor + 1).is_digit then
					n := 2
				end
			elseif c.is_digit then
				n := 1
			end
			if n /= 0 then
				is_int := True
				from i := cursor + n
				until i > lim or not is_int
				loop
					is_int := buffer.item (i).is_digit
					if is_int then
						n := n + 1
					end
					i := i + 1
				end
			end
			if n /= 0 then
				read (n)
			else
				read (0)
				set_error_message ("Not an integer")
			end
		end

	--|--------------------------------------------------------------

	read_sized_float (sz: INTEGER)
			-- Read an ASCII representation of a floating point number of
			-- type REAL_'sz' from stream.
			-- Make result available in last_real or last_double
			-- as appropriate.
			--
			-- If cursor is not at a floating point number, then error is
			-- set and cursor remains as it was before the read attempt.
			-- On success, cursor is moved to first position after value 
			-- just read.
			--
		require
			valid_size: sz = 32 or sz = 64
		local
			sp, ep: INTEGER
			cvtr: AEL_SPRT_STR_REAL_CONVERTOR
--			cvtr: STRING_TO_REAL_CONVERTOR
		do
			sp := cursor
			create cvtr.make
			cvtr.set_leading_separators (" ")
			cvtr.set_trailing_separators (" ")
			cvtr.set_leading_separators_acceptable (True)
			cvtr.set_trailing_separators_acceptable (True)
			cvtr.parse_substring_with_type (
				buffer, {NUMERIC_INFORMATION}.type_no_limitation, sp, buffer.count)
			ep := cvtr.last_substring_parse_position
			if ep > sp then
				read (ep - sp)
			else
				read (0)
				set_error_message ("Not a floating point number")
			end
			last_double_result := cvtr.parsed_double
		end

	--|--------------------------------------------------------------

	write (v: STRING)
			-- Write into buffer at cursor, advancing the cursor
			-- to the point after the last character written
		require
			exists: v /= Void
		do
			if is_empty or cursor >= buffer.count then
				buffer.append (v)
				cursor := buffer.count + 1
			else
				buffer.insert_string (v, cursor)
				cursor := cursor + v.count
			end
		ensure
			extended: count = (old count) + v.count
			end_on_append: (old is_empty or after) implies after
			cursor_moved: not (old is_empty) implies
				cursor = (old cursor) + v.count
		end

	--|--------------------------------------------------------------

	write_char (v: CHARACTER)
			-- Write char into buffer at cursor, advancing the cursor
			-- to the point after the last character written
		do
			if is_empty or cursor >= buffer.count then
				buffer.extend (v)
			else
				buffer.insert_character (v, cursor)
			end
			cursor := cursor + 1
		ensure
			extended: count = (old count) + 1
			cursor_moved: cursor = (old cursor) + 1
		end

--|========================================================================
feature -- Storage support (not yet available)
--|========================================================================

	retrieved: ANY
			-- Retrieved object structure
			-- To access resulting object under correct type,
			-- use assignment attempt.
			-- Will raise an exception (code `Retrieve_exception')
			-- if content is not a stored Eiffel structure.
		require else
			True
--RFO 		local
--RFO 			size: INTEGER
		do
--RFO 			(create {MISMATCH_CORRECTOR}).mismatch_information.do_nothing
--RFO 			Result := c_retrieved (internal_buffer_access, buffer_size, 0, $size)
--RFO 			object_stored_size := size
			check False then end
		end

 --|========================================================================
feature -- Element change (for storable)
 --|========================================================================

	basic_store (object: ANY)
			-- Produce an external representation of the
			-- entire object structure reachable from `object'.
			-- Retrievable within current system only.
--RFO 		local
--RFO 			size: INTEGER
		do
--RFO 			buffer_size := c_stream_basic_store (internal_buffer_access, buffer_size, $object, $size)
--RFO 			object_stored_size := size
		end

	--|--------------------------------------------------------------

	general_store (object: ANY)
			-- Produce an external representation of the
			-- entire object structure reachable from `object'.
			-- Retrievable from other systems for same platform
			-- (machine architecture).
			--| This feature may use a visible name of a class written
			--| in the `visible' clause of the Ace file. This makes it
			--| possible to overcome class name clashes.
--RFO 		local
--RFO 			size: INTEGER
		do
--RFO 			buffer_size := c_stream_general_store (internal_buffer_access, buffer_size, $object, $size)
--RFO 			object_stored_size := size
		end

	--|--------------------------------------------------------------

	independent_store (object: ANY)
			-- Produce an external representation of the
			-- entire object structure reachable from `object'.
			-- Retrievable from other systems for the same or other
			-- platform (machine architecture).
--RFO 		local
--RFO 			size: INTEGER
		do
--RFO 			buffer_size := c_stream_independent_store (internal_buffer_access, buffer_size, $object, $size)
--RFO 			object_stored_size := size
		end

--RFO 	set_additional_size (new_size: INTEGER)
--RFO 			-- Set `new_size' to BUFFER_SIZE, internal value used to
--RFO 			-- increment `buffer_size' during storable operations.
--RFO 		external
--RFO 			"C use %"eif_store.h%""
--RFO 		alias
--RFO 			"set_buffer_size"
--RFO 		end

 --|========================================================================
feature {NONE} -- Not exported
 --|========================================================================

	name: detachable STRING
			-- Not meaningful
		do
		end

	handle: INTEGER
			-- Handle to medium
		do
		end

	handle_available: BOOLEAN
			-- Is the handle available after class has been
			-- created?
		do
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

end -- AEL_SPRT_BUFFER_MEDIUM
