--|----------------------------------------------------------------------
--| Copyright (c) 1995-2010, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Data structure manipulation and inspection routines
--|----------------------------------------------------------------------

class AEL_SPRT_DS_ROUTINES

create
	default_create

--|========================================================================
feature -- Hash table routines
--|========================================================================

	last_fill_hash_table_successful: BOOLEAN
			-- was most recent call to fill_hash_table successful?

	fill_hash_table (
		dst: HASH_TABLE [ANY,HASHABLE];
		src: CONTAINER [G];
		kf: FUNCTION [ANY, HASHABLE])
			-- Fill 'dst' with as many items of `src' as possible.
			-- For each item in 'src', call key function 'kf' to obtain 
			-- the key to use.
			--
			-- NOTE WELL:  Duplicate keys will be skipped
		require
			destination_exists: dst /= Void
		local
			k: STRING
			lin_rep: LINEAR [ANY]
		do
			last_fill_hash_table_successful := True
			from lin_rep.start
			until lin_rep.off or not last_fill_hash_table_successful
			loop
				k := kf.item (lin_rep.item)
				if k = Void then
					-- ERROR, skip and quit
					last_fill_hash_table_successful := False
				elseif not dst.has (k) then
					dst.extend (lin_rep.item, k)
				end
				lin_rep.forth
			end
		end

	--|--------------------------------------------------------------

	copy_hash_table (src, dst: HASH_TABLE [ANY, HASHABLE])
			-- Copy (unique) contents of src into dst
		local
			k: STRING
		do
			from src.start
			until src.after
			loop
				k := src.key_for_iteration
				if not dst.has (k) then
					dst.extend (src.item_for_iteration, k)
				end
				src.forth
			end
		end

end -- class AEL_SPRT_DS_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 004 02-Apr-2010
--|     Created original from routines elsewhere
--|----------------------------------------------------------------------
--| How-to
--|
--| This class can be inherited or instantiated.  It relies on classes
--| from the Eiffel Base libraries
--|----------------------------------------------------------------------
