note
	description: "An ordered collection of history items"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class AEL_SPRT_HISTORY [G->AEL_SPRT_HISTORY_ITEM]

inherit
	SORTED_TWO_WAY_LIST [G]

create
	make

create {AEL_SPRT_HISTORY}
	make_sublist

end -- class HISTORY
