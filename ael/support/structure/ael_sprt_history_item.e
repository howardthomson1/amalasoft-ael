note
	description: "An item in a history collection"
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class AEL_SPRT_HISTORY_ITEM

inherit
	COMPARABLE

create
	make, make_with_text

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
			-- Create Current with creation date of now
		do
			create text.make (0)
			create time_created.make_now
		end

	--|--------------------------------------------------------------

	make_with_text (v: STRING)
			-- Create current with text 'v'
		do
			make
			set_text (v)
		ensure
			has_text: text ~ v
		end

--|========================================================================
feature -- Status
--|========================================================================

	time_created: TIME
			-- Time of creation of this history item

	--|--------------------------------------------------------------

	last_modified: TIME
			-- Time this item was last modified
		do
			if attached private_mod_time as lmt then
				Result := lmt
			else
				Result := time_created
			end
		end

	--|--------------------------------------------------------------

	text: STRING
			-- Text describing this history item

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text (v: STRING)
		do
			text.wipe_out
			text.append (v)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := time_created < other.time_created
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_mod_time: detachable TIME
			-- Time at which this item was last modified
			-- if different that creation time

end -- class HISTORY_ITEM
