deferred class AEL_SPRT_ID_COMPARABLE
-- Abstract notion of being identifiable (by 'identifier') and being
-- comparable (by 'identifier')

inherit
	COMPARABLE
	AEL_SPRT_IDENTIFIABLE
		undefine
			is_equal
		end

 --|========================================================================
feature -- Comparison
 --|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := identifier < other.identifier
		end

end -- class AEL_SPRT_ID_COMPARABLE
