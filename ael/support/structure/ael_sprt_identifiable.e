deferred class AEL_SPRT_IDENTIFIABLE
-- Abstract notion of being identifiable

--|========================================================================
feature -- Status
--|========================================================================

	identifier: STRING
		deferred
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_identifier (v: STRING)
		require
			valid: is_valid_identifier (v)
		deferred
		ensure
			is_set: v.same_string (identifier)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_identifier (v: detachable STRING): BOOLEAN
		do
			Result := v /= Void and then not v.is_empty
		end

end -- class AEL_SPRT_IDENTIFIABLE
