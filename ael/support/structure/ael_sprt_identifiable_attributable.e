deferred class AEL_SPRT_IDENTIFIABLE_ATTRIBUTABLE
-- Abstract notion of having attributes that are settable from a serialized
-- string form, and that can be presented in a serialized string 
-- form, AND of being identifiable

inherit
	AEL_SPRT_ATTRIBUTABLE
	AEL_SPRT_IDENTIFIABLE

end -- class AEL_SPRT_IDENTIFIABLE_ATTRIBUTABLE
