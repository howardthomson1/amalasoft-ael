deferred class AEL_SPRT_INDEX_COMPARABLE
-- Abstract notion of being identifiable (by integer 'id') and being
-- comparable (by 'id')

inherit
	AEL_SPRT_ID_COMPARABLE
		rename
			identifier as id_str,
			set_identifier as set_id_str
		redefine
			is_less
		end

--|========================================================================
feature -- Creation
--|========================================================================

	make_with_id (v: INTEGER)
			-- Create Current with ID 'v'
		require
			valid: is_valid_id (v)
		do
			default_create
			set_id (v)
		ensure
			is_set: id = v
			has_valid_id: is_valid_id (id)
		end

--|========================================================================
feature -- Status
--|========================================================================

	id: INTEGER
			-- Unique-to-context ID of Current

	--|--------------------------------------------------------------

	id_str: STRING
			-- String form of ID
		do
			Result := id.out
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_id (v: INTEGER)
			-- Set the ID of Current to 'v'
		require
			valid: is_valid_id (v)
		do
			id := v
		ensure
			is_set: id = v
			has_valid_id: is_valid_id (id)
		end

	--|--------------------------------------------------------------

	set_id_str (v: STRING)
			-- Set id by string form 'v'
		do
			id := v.to_integer
		ensure then
			valid_id: is_valid_id (id)
			id_equivalent: id = v.to_integer
		end

 --|========================================================================
feature -- Comparison
 --|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			-- Want numeric comparison, not ascii or other collating sequence
			Result := id < other.id
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_id (v: INTEGER): BOOLEAN
			-- Is 'v' a valid ID?
		do
			Result := v > 0
		end

	is_valid_id_str (v: detachable STRING): BOOLEAN
			-- Is 'v' a valid string from of a valid ID?
		do
			if attached v as tv and then not tv.is_empty then
				Result := tv.is_integer and then is_valid_id (tv.to_integer)
			end
		end

	--|--------------------------------------------------------------
invariant
	id_valid_or_undefined: id >= 0

end -- class AEL_SPRT_INDEX_COMPARABLE
