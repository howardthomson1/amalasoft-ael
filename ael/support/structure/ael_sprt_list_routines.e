class AEL_SPRT_LIST_ROUTINES

 --|========================================================================
feature -- Comparison
 --|========================================================================

	compare_lists (l1, l2, only_1, only_2, both: LIST [STRING])
			-- Compare the two original lists 'l1' and 'l2'
			--
			-- Build three result lists: 'only_1' that contains items that 
			-- were unique to the first original list 'l1', 'only_2' that 
			-- contains items that were unique to the original list 'l2',
			-- and 'both' that contains items that were common to both
			-- original lists.
			--
			-- Leave the original lists unchanged.
		require
			lists_exists: l1 /= Void and l2 /=Void
			results_exist: only_1 /= Void and only_2 /= Void and both /= Void
		local
			done: BOOLEAN
		do
			from 
				l1.start
				l2.start
			until l1.exhausted or l2.exhausted
			loop
				if l1.item < l2.item then
					-- Item in l1 must be unique to l1 (has been removed)
					only_1.extend (l1.item)
					-- run through l1 until it is in sync with l2
					done := False
					from l1.forth
					until l1.exhausted or done
					loop
						if l2.exhausted then
							done := True
						elseif l1.item >= l2.item then
							done := True
						else
							only_1.extend (l1.item)
							l1.forth
						end
					end
				elseif l2.item < l1.item then
					-- Item in l2 must be unique to l2, has been added
					only_2.extend (l2.item)
					-- run through l2 until it is in sync with l1
					done := False
					from l2.forth
					until l2.exhausted or done
					loop
						if l1.exhausted then
							done := True
						elseif l2.item >= l1.item then
							done := True
						else
							-- Item in l2 must be unique to l2 (added)
							only_2.extend (l2.item)
							l2.forth
						end
					end
				else
					-- Items have the same name
					both.extend (l1.item)
					l1.forth
					l2.forth
				end
			end

			if l1.exhausted or l1.is_empty then
				if l2.exhausted or l2.is_empty then
					-- both have run out, we're done
				else
					-- ran out of items in l1 first,
					-- all remaining items in l2 are unique to l2 (added)
					from
					until l2.exhausted 
					loop
						only_2.extend (l2.item)
						l2.forth
					end
				end
			elseif l2.exhausted or l2.is_empty then
				-- ran out of items in l2 first,
				-- all remaining items in l1 are unique to l1 (removed)
				from
				until l1.exhausted 
				loop
					only_1.extend (l1.item)
					l1.forth
				end
			else
				-- Cannot compare the lists why?
			end
		end

	--|--------------------------------------------------------------

	list_has_void (tl: LINEAR [detachable ANY]): BOOLEAN
			-- Does 'tl' have any items that are Void?
		local
			li: LINEAR_ITERATOR [detachable ANY]
		do
			create li.set (tl)
			from li.start
			until li.exhausted or Result
			loop
				if li.item = Void then
					Result := True
				else
					li.forth
				end
			end
		end

	--|--------------------------------------------------------------

	list_has_item_matching (
		tl: LINEAR [ANY]; test: FUNCTION [ANY, BOOLEAN]): BOOLEAN
		-- Does the list 'tl have an item that matches according to the
		-- function 'test'
		--
		-- NOTE: it would be much better to have this routine in the LINEAR
		-- class so that the arguments and result could benefit from
		-- constrained genericity.
		require
			list_exists: tl /= Void
			test_exists: test /= Void
		do
			Result := tl.there_exists (test)
		end

	--|--------------------------------------------------------------

	i_th_item_matching (
		tl: LINEAR [ANY]; idx: INTEGER;
		test: FUNCTION [ANY, BOOLEAN]): detachable ANY
		-- The 'i_th' item in the given list that matches according to the
		-- function 'test'p
		--
		-- NOTE: it would be much better to have this routine in the LINEAR
		-- class so that the arguments and result could benefit from
		-- constrained genericity.
		require
			list_exists: tl /= Void
			test_exists: test /= Void
		local
			list: like items_matching
		do
			list := items_matching (tl, test)
			if list.count >= idx then
				Result := list.i_th (idx)
			end
		end

	--|--------------------------------------------------------------

	first_item_matching (
		tl: LINEAR [ANY];
		test: FUNCTION [ANY, BOOLEAN]): detachable ANY
		-- The first item in the given list that matches according to the
		-- function 'test'
		-- If none matches, Result is Void
		--
		-- NOTE: it would be much better to have this routine in the LINEAR
		-- class so that the arguments and result could benefit from
		-- constrained genericity.
		require
			list_exists: tl /= Void
			test_exists: test /= Void
		do
			Result := i_th_item_matching (tl, 1, test)
		end

	--|--------------------------------------------------------------

	items_matching (
		tl: LINEAR [ANY];
		test: FUNCTION [ANY, BOOLEAN]): LINKED_LIST [ANY]
		-- All items in the given list that match according to the
		-- function 'test'
		--
		-- NOTE: it would be much better to have this routine in the LINEAR
		-- class so that the arguments and result could benefit from
		-- constrained genericity.
		require
			list_exists: tl /= Void
			test_exists: test /= Void
		local
			oc: detachable CURSOR
			ti: ANY
		do
			create Result.make
			if attached {CURSOR_STRUCTURE [ANY]} tl as cs then
				oc := cs.cursor
			end
			from tl.start
			until tl.exhausted
			loop
				ti := tl.item
				test.call ([ti])
				if test.last_result then
					Result.extend (ti)
				end
				tl.forth
			end
			if attached {CURSOR_STRUCTURE [ANY]} tl as cs then
				if attached oc then
					cs.go_to (oc)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	has_duplicates (lst: LIST [ANY]): BOOLEAN
			-- Does 'lst' have more than one instance of any items?
		local
			sl: SORTED_TWO_WAY_LIST [COMPARABLE]
			tl: ARRAYED_LIST [ANY]
			pi: ANY
		do
			if lst.count >= 2 then
				if attached {LIST [COMPARABLE]} lst as cl then
					create sl.make
					sl.fill (cl)
					sl.start
					pi := sl.item
					from sl.forth
					until sl.after or Result
					loop
						if pi = sl.item then
							Result := True
						else
							pi := sl.item
							sl.forth
						end
					end
				else
					create tl.make (lst.count)
					from lst.start
					until lst.after or Result
					loop
						if tl.has (lst.item) then
							Result := True
						else
							tl.extend (lst.item)
							lst.forth
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	duplicates (lst: LIST [ANY]): LINKED_LIST [ANY]
			-- Does 'lst' have more than one instance of any items?
		local
			tl: ARRAYED_LIST [ANY]
		do
			create Result.make
			if lst.count >= 2 then
				create tl.make (lst.count)
				from lst.start
				until lst.after
				loop
					if tl.has (lst.item) then
						Result.extend (lst.item)
					end
					tl.extend (lst.item)
					lst.forth
				end
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

	each_transformed (
		tl: LIST [ANY];
		xfrm: FUNCTION [ANY, ANY]): LINKED_LIST [ANY]
		-- A list containing the result of each item in the original list
		-- being transformed by the given transformation function
		--
		-- NOTE: it would be much better to have this routine in the LINEAR
		-- class so that the arguments and result could benefit from
		-- constrained genericity.
		require
			original_exists: tl /= Void
			transformation_exists: xfrm /= Void
		do
			create Result.make
			tl.do_all (agent add_transformed_item_to_list (?, Result, xfrm))
		ensure
			exists: Result /= Void
			same_count: Result.count = tl.count
		end

	--|--------------------------------------------------------------

	prune_list (src, fltr: LIST [ANY])
			-- Remove from 'src' any items also in 'fltr'
		local
			sof, fof: BOOLEAN
		do
			sof := src.object_comparison
			fof := src.object_comparison
			src.compare_objects
			fltr.compare_objects
			from src.start
			until src.after
			loop
				if fltr.has (src.item) then
					src.remove
				else
					src.forth
				end
			end
			if not sof then
				src.compare_references
			end
			if not fof then
				fltr.compare_references
			end
		ensure
			not_grown: src.count <= old src.count
			filter_same: fltr.count = old fltr.count
		end

 --|========================================================================
feature -- String list support
 --|========================================================================

	list_to_string (tl: LIST [STRING_GENERAL]; sep: STRING_GENERAL): STRING
			-- String representation of given list, with separator 'sep' between
			-- adjacent items (no leading or trailing instance of 'sep')
		require
			list_exists: tl /= Void
			separator_exists: sep /= Void
 		local
			r8: STRING_8
			r32: STRING_32
		do
			create Result.make (0)
			if attached {LIST [STRING_8]} tl as tl8 then
				create r8.make (tl.count * 3)
				tl8.do_all (agent append_list_item_to_string_8 (?, r8, sep, tl8))
				Result := r8
			elseif attached {LIST [STRING_32]} tl as tl32 then
				create r32.make (tl.count * 3)
				tl32.do_all (agent append_list_item_to_string_32 (?, r32, sep, tl32))
				Result := r32
			end
		ensure
			exists: Result /= Void
		end

	any_list_to_string (tl: LIST [ANY]; sep: STRING): STRING
			-- String representation of given list, with separator 'sep' between
			-- adjacent items (no leading or trailing instance of 'sep')
			-- RFO TODO FIX CATCALL ISSUE
		require
			list_exists: tl /= Void
			separator_exists: sep /= Void
		do
			create Result.make (tl.count * 3)
			tl.do_all (agent append_any_list_item_to_string (?, Result, sep, tl))
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	string_sublist (
		v: like string_sublist; spos, epos: INTEGER): LIST [STRING]
		-- List containing items from the original list 'v' at positions
		-- between spos and epos.
		--
		-- Original list remains unchanged
		require
			list_exists: v /= Void
			valid_start: spos > 0 and spos <= v.count
			valid_end: epos >= spos and epos <= v.count
		local
			oc: CURSOR
			i: INTEGER
		do
			create {LINKED_LIST [STRING]}Result.make
			oc := v.cursor
			i := spos
			from v.go_i_th (i)
			until i > epos or v.exhausted
			loop
				Result.extend (v.item)
				v.forth
				i := i + 1
			end
			if v.valid_cursor (oc) then
				v.go_to (oc)
			end
		ensure
			exists: Result /= Void
				original_unchanged: v.is_equal (old v)
		end

	--|--------------------------------------------------------------

	string_list_split (v: LIST [STRING]; p: INTEGER): ARRAY [LIST [STRING]]
			-- Array of two sublists resulting from splitting 'v' after 
			-- position 'p'
			--
			-- Original list remains unchanged
		require
			list_exists: v /= Void
			valid_position: p > 0 and p <= v.count
		local
			oc: CURSOR
			i: INTEGER
			r1, r2: ARRAYED_LIST [STRING]
		do
			create r1.make (p)
			create r2.make (v.count - p)
			Result := {ARRAY[LIST [STRING]]}<<r1, r2>>
			oc := v.cursor
			from
				v.start
				i := 1
			until v.exhausted
			loop
				if i <= p then
					r1.extend (v.item)
				else
					r2.extend (v.item)
				end
				v.forth
				i := i + 1
			end
			if v.valid_cursor (oc) then
				v.go_to (oc)
			end
		ensure
			exists: Result /= Void
			has_two: Result.count = 2
			original_unchanged: v.is_equal (old v)
		end

	--|--------------------------------------------------------------

	list_has_expression (tl: LINEAR [STRING]; pat: AEL_SPRT_REGEXP): BOOLEAN
			-- Does the given list include any items that conform
			-- to the given pattern?
		require
			list_exists: tl /= Void
			pattern_exists: pat /= Void
			pattern_compiled: pat.compiled
		do
			Result := tl.there_exists (agent string_matches_expression (?, pat))
		end

	--|--------------------------------------------------------------

	i_th_item_matching_expression (
		tl: LINEAR [STRING]; idx: INTEGER; pat: AEL_SPRT_REGEXP): detachable STRING
		-- i_th item within the list 'tl' that conforms to the pattern 
		-- 'pat', where 'idx' is the one-based position from the start
		-- Result is void if none conforms
		require
			list_exists: tl /= Void
			pattern_exists: pat /= Void
			pattern_compiled: pat.compiled
		local
			list: like items_matching_expression
		do
			list := items_matching_expression (tl, pat)
			if list.count >= idx then
				Result := list.i_th (idx)
			end
		ensure
			no_false_match: Result /= Void implies list_has_expression (tl,pat)
		end

	--|--------------------------------------------------------------

	first_item_matching_expression (
		tl: LINEAR [STRING]; pat: AEL_SPRT_REGEXP): detachable STRING
		-- First item within the list 'tl' that conforms to the pattern 
		-- 'pat'
		-- If none matches, then Void
		require
			list_exists: tl /= Void
			pattern_exists: pat /= Void
			pattern_compiled: pat.compiled
		do
			Result := i_th_item_matching_expression (tl, 1, pat)
		end

	--|--------------------------------------------------------------

	items_matching_expression (
		tl: LINEAR [STRING]; pat: AEL_SPRT_REGEXP): LINKED_LIST [STRING]
		-- All items within the list 'tl' that conforms to the pattern 'pat'
		require
			list_exists: tl /= Void
			pattern_exists: pat /= Void
			pattern_compiled: pat.compiled
		local
			oc: detachable CURSOR
		do
			create Result.make
			if attached {CURSOR_STRUCTURE [ANY]} tl as cs then
				oc := cs.cursor
			end
			from tl.start
			until tl.exhausted
			loop
				if string_matches_expression (tl.item, pat) then
					Result.extend (tl.item)
				end
				tl.forth
			end
			if attached {CURSOR_STRUCTURE [ANY]} tl as cs then
				if attached oc then
					cs.go_to (oc)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	array_to_tv_pairs (v: ARRAY [STRING]): AEL_SPRT_TV_PAIR_LIST
			-- A newly created t-v pair list containing the 
			-- adjacent pairs in 'v'
			-- 'v' must contain an even nummber of items as they are 
			-- paired together as odd=tag, even=value
		require
			exists: v /= Void
			paired: v.count \\ 2 = 0
		local
			i, lim: INTEGER
		do
			create Result.make
			lim := v.upper
			from i := v.lower
			until i > lim
			loop
				Result.add_item (v.item (i), v.item (i+1))
				i := i + 2
			end
		end

	--|--------------------------------------------------------------

	tuples_to_tv_pairs (v: ARRAY [TUPLE [STRING, STRING]]): AEL_SPRT_TV_PAIR_LIST
			-- A newly created t-v paur list containing the pairs in 'v'
		local
			i, lim: INTEGER
			tp: TUPLE [STRING, STRING]
		do
			create Result.make
			lim := v.upper
			from i := v.lower
			until i > lim
			loop
				tp := v.item (i)
				if attached {STRING} tp.reference_item (1) as tag
					and then  attached {STRING} tp.reference_item (2) as val
				 then
					 Result.add_item (tag, val)
				end
				i := i + 1
			end
		end

--|========================================================================
feature -- List merging
--|========================================================================

	string_lists_merged (s1,s2: LIST [STRING]; uf: BOOLEAN): LINKED_LIST [STRING]
			-- A list resulting from the merger of s1 and s2, 
			-- affecting neither original.
			-- If 'uf' then merged list does have any duplicate items
		require
			exist: s1 /= Void and s2 /= Void
		local
			oc: CURSOR
			ts: STRING
		do
			create Result.make
			Result.compare_objects
			Result.fill (s1)
			if uf then
				oc := s2.cursor
				from s2.start
				until s2.exhausted
				loop
					ts := s2.item
					if not Result.has (ts) then
						Result.extend (ts)
					end
					s2.forth
				end
				if s2.valid_cursor (oc) then
					s2.go_to (oc)
				end
			else
				Result.fill (s2)
			end
		ensure
			exists: Result /= Void
			comparison: Result.object_comparison
		end
	
 --|========================================================================
feature -- Array conversion
 --|========================================================================

	fill_list_from_array (tl: LIST [ANY]; a: ARRAY [ANY])
			-- Because code generate is little different for manifest arrays,
			-- and becuase this leads to some interesting problems, this routine
			-- treats arrays of _expanded_ types with special care, using 
			-- supporting routines for each of the three major expandeds.
		require
			list_exists: tl /= Void
			array_exists: a /= Void
		local
			ex: EXCEPTIONS
		do
			if not a.is_empty then
				if is_reference_type (a.item (a.lower)) then
					tl.fill (a)
				else
					if attached {ARRAY [INTEGER]} a as ia then
						fill_list_from_integer_array (tl, ia)
					elseif attached {ARRAY [BOOLEAN]} a as ba then
						fill_list_from_boolean_array (tl, ba)
					elseif attached {ARRAY [CHARACTER]} a as ca then
						fill_list_from_character_array (tl, ca)
					else
						create ex
						ex.raise ("Attempting to fill list from expanded array")
					end
				end
			end
		end

	--|--------------------------------------------------------------

	fill_list_from_integer_array (tl: LIST [ANY]; a: ARRAY [INTEGER])
		require
			list_exists: tl /= Void
			array_exists: a /= Void
		local
			i, lim: INTEGER
			ref: INTEGER_REF
		do
			lim := a.count
			from i := 1
			until i > lim
			loop
				create ref
				ref.set_item (a.item (i))
				tl.extend (ref.item)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	fill_list_from_boolean_array (tl: LIST [ANY]; a: ARRAY [BOOLEAN])
		require
			list_exists: tl /= Void
			array_exists: a /= Void
		local
			i, lim: INTEGER
			ref: BOOLEAN_REF
		do
			lim := a.count
			from i := 1
			until i > lim
			loop
				create ref
				ref.set_item (a.item (i))
				tl.extend (ref.item)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	fill_list_from_character_array (tl: LIST [ANY]; a: ARRAY [CHARACTER])
		require
			list_exists: tl /= Void
			array_exists: a /= Void
		local
			i, lim: INTEGER
			ref: CHARACTER_REF
		do
			lim := a.count
			from i := 1
			until i > lim
			loop
				create ref
				ref.set_item (a.item (i))
				tl.extend (ref.item)
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	fill_array_from_list (a: ARRAY [ANY]; tl: LIST [ANY])
		require
			list_exists: tl /= Void
			array_exists: a /= Void
			same_size: a.capacity = tl.count
		local
			i: INTEGER
			oc: CURSOR
		do
			if not tl.is_empty then
				oc := tl.cursor
				from
					tl.start
					i := a.lower
				until tl.exhausted
				loop
					a.put (tl.item, i)
					i := i + 1
					tl.forth
				end
				if tl.valid_cursor (oc) then
					tl.go_to (oc)
				end
			end
		ensure
			list_cursor_unchanged: tl.cursor ~ old tl.cursor
			list_count_unchanged: tl.count = old tl.count
		end

	--|--------------------------------------------------------------

	fill_string_from_list (a: ARRAY [STRING]; tl: LIST [STRING])
		require
			list_exists: tl /= Void
			array_exists: a /= Void
			same_size: a.capacity = tl.count
		local
			i: INTEGER
			oc: CURSOR
		do
			if not tl.is_empty then
				oc := tl.cursor
				from
					tl.start
					i := a.lower
				until tl.exhausted
				loop
					a.put (tl.item, i)
					i := i + 1
					tl.forth
				end
				if tl.valid_cursor (oc) then
					tl.go_to (oc)
				end
			end
		ensure
			list_cursor_unchanged: tl.cursor ~ old tl.cursor
			list_count_unchanged: tl.count = old tl.count
		end

	--|--------------------------------------------------------------

	string_list_to_integer_array (tl: LIST [STRING]): ARRAY [INTEGER]
			-- Array of INTEGERs derived from list of 
			-- integers-as-strings 
			-- If any items in tl are not integers, then Result is Void
		require
			list_exists: tl /= Void and then not tl.is_empty
		local
			ts: STRING
			i, ti: INTEGER
			oc: CURSOR
		do
			create Result.make_filled (0, 1, tl.count)
			oc := tl.cursor
			from
				tl.start
				i := 1
			until tl.exhausted or Result = Void
			loop
				ts := tl.item
				if not ts.is_integer then
					Result := {ARRAY[INTEGER]}<<>>
				else
					ti := ts.to_integer
					Result.put (ti, i)
					i := i + 1
					tl.forth
				end
			end
			if tl.valid_cursor (oc) then
				tl.go_to (oc)
			end
		ensure
			same_size: Result.count = tl.count
			list_cursor_unchanged: tl.cursor ~ old tl.cursor
			list_count_unchanged: tl.count = old tl.count
		end

 --|========================================================================
feature {NONE} -- Agents and callbacks
 --|========================================================================

	append_list_item_to_string (v: STRING; str, sep: STRING_GENERAL; tl: LIST [STRING_GENERAL])
		require
			item_exists: v /= Void
			string_exists: str /= Void
			separator_exists: sep /= Void
			list_exists: tl /= Void
		do
			str.append (v)
			if v /= tl.last then
				str.append (sep)
			end
		end

	append_list_item_to_string_8 (v: STRING_8; str, sep: STRING_GENERAL; tl: LIST [STRING_8])
		require
			item_exists: v /= Void
			string_exists: str /= Void
			separator_exists: sep /= Void
			list_exists: tl /= Void
		do
			str.append (v)
			if v /= tl.last then
				str.append (sep)
			end
		end

	append_list_item_to_string_32 (v: STRING_32; str, sep: STRING_GENERAL; tl: LIST [STRING_32])
		require
			item_exists: v /= Void
			string_exists: str /= Void
			separator_exists: sep /= Void
			list_exists: tl /= Void
		do
			str.append (v)
			if v /= tl.last then
				str.append (sep)
			end
		end

	append_any_list_item_to_string (v: ANY; str, sep: STRING; tl: LIST [ANY])
		require
			item_exists: v /= Void
			string_exists: str /= Void
			separator_exists: sep /= Void
			list_exists: tl /= Void
			--RFO TODO CATCALL ISSUE
		do
			str.append (v.out)
			if v /= tl.last then
				str.append (sep)
			end
		end

	--|--------------------------------------------------------------

	add_transformed_item_to_list (
		v: ANY; tl: LIST [ANY]; xfrm: FUNCTION[ANY, ANY])
			-- Add to the list 'tl' the result of transforming the given item 
			-- 'v' using the transformation function 'xfrm'
		require
			item_exists: v /= Void
			list_exists: tl /= Void
			transformation_exists: xfrm /= Void
		local
			ti: detachable ANY
		do
			xfrm.call (v)
			ti := xfrm.last_result
			if attached ti then
				tl.extend (ti)
			end
		end

	--|--------------------------------------------------------------

	string_matches_expression (ts: STRING; pat: AEL_SPRT_REGEXP): BOOLEAN
			-- Does the string 'ts' conform to the given pattern 'pat'?
		require
			string_exists: ts /= Void
			pattern_exists: pat /= Void
			pattern_compiled: pat.compiled
		do
			if not ts.is_empty then
				pat.search_forward (ts, 1, ts.count)
				Result := pat.found
			end
		end

--|========================================================================
feature {NONE} -- Onces and constants
--|========================================================================

	eif_internal: INTERNAL
		once
			create Result
		end

	eif_runtime: ISE_RUNTIME
		once
			create Result
		end

	is_reference_type (v: ANY): BOOLEAN
		local
			tt, dt: INTEGER
			typ: TYPE [detachable ANY]
		do
			typ := eif_internal.type_of (v)
			if attached typ then
				tt := typ.type_id
				dt := eif_runtime.attached_type (tt)
				Result := not eif_internal.is_attached_type (dt)
			end
		end

--------------------------------------------------------------------
-- Author Identification
--
-- RFO  Roger F. Osmond
--------------------------------------------------------------------
-- Audit Trail
--
-- 001 RFO 04-AUG-2010  Renamed from aold a_list_utils; updated to 6.6
-- 000 RFO 06-Apr-2002  Created original module.
--------------------------------------------------------------------

end -- class AEL_SPRT_LIST_ROUTINES
