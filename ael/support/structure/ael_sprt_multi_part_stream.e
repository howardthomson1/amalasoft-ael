note
	description: "Encapsulation of a multi-part MIME stream"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	author: "Jon Hermansen"
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
Create and instance of this class by using the 'make_from_stream' or
simple 'make' routines
}"

class AEL_SPRT_MULTI_PART_STREAM

inherit
	AEL_SPRT_FALLIBLE

create
	make, make_from_stream

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make
			-- Create an empty instance
		do
			create parts.make
			create searcher.make
			create stream.make (0)
			create boundary.make (0)
		end

	--|--------------------------------------------------------------

	make_from_stream (v, ct: STRING; sp: INTEGER)
			-- Create a new instance from stream 'v' with master
			-- content-type 'ct'.
		require
			stream_exists: v /= Void
			valid_content_type: is_valid_content_type (ct)
		do
			private_start_position := sp
			make
			set_content_type (ct)
			if not has_error then
				set_stream (v)
			end
			if not has_error then
				read_whole_stream
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_initialized: BOOLEAN
			-- Has the stream been initialized to the point when it can be read?
		do
			Result := not stream.is_empty and not boundary.is_empty
		end

	content_type_header: detachable STRING
		note
			option: stable
			attribute
		end

	boundary: STRING
			-- String used as boundary between parts

	parts: TWO_WAY_LIST [AEL_SPRT_MULTIPART_STREAM_PART]

	stream: STRING
			-- Current input stream

	current_position: INTEGER
		do
			Result := private_position.max (1)
		ensure
			positive: is_initialized implies Result > 0
		end

	--|--------------------------------------------------------------

	stream_is_exhausted: BOOLEAN
			-- Is the input stream exhausted at the given position?
			--TODO this should be broken into a command and a query
		local
			newbuf: detachable STRING
		do
			Result := current_position > stream.count
			if Result then
				-- Check for more input from client
				if input_update_function /= Void then
					input_update_function.call ([Current])
					newbuf := input_update_function.last_result
				end
				if newbuf /= Void and then not newbuf.is_empty then
					-- Add newbuf to stream list
					Result := False
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_content_type (ct: STRING)
			-- Set content type, and extract boundary string from
			-- content-type header
		require
			exists: ct /= Void
			has_boundary: ct.substring_index ("; boundary=", 1) /= 0
		local
			sp, ep1, ep2, ep: INTEGER
		do
			content_type_header := ct
			sp := ct.substring_index ("; boundary=", 1) + 11
			-- Look for end of boundary before end of header
			ep1 := ct.index_of ('%"', sp)
			ep2 := ct.index_of (';', sp)
			if ep1 = 0 then
				if ep2 = 0 then
					ep := ct.count
				else
					ep := ep2 - 1
				end
			elseif ep2 = 0 then
				ep := ep1 - 1
			else
				ep := ep1.min (ep2) - 1
			end
			boundary := ct.substring (sp, ep)
		ensure
			assigned: not has_error implies content_type_header = ct
			has_boundary: not has_error implies
				boundary /= Void and then not boundary.is_empty
		end

	--|--------------------------------------------------------------

	set_stream (v: STRING)
			-- Set the input stream to the given string
		require
			exists: v /= Void
		do
			stream := v
		ensure
			assigned: stream = v
		end

	--|--------------------------------------------------------------

	set_input_update_function (v: like input_update_function)
		require
			exists: v /= Void
		do
			if v /= Void then
				input_update_function := v
			end
		ensure
			set: v /= Void implies v = input_update_function
		end

	set_current_position (v: INTEGER)
		do
			private_position := v
		end

--|========================================================================
feature -- Input processing
--|========================================================================

	read_whole_stream
		require
			is_initialized: is_initialized
		do
			from
				set_current_position (private_start_position.max (1))
			until
				stream_is_exhausted
			loop
				read_next_part
			variant
				(stream.count - current_position) + 1
			end
		end

	--|--------------------------------------------------------------

	read_next_part
			-- Read the next part from the input stream beginning at startpos
		require
			valid_start: current_position <= stream.count
			is_initialized: is_initialized
		local
			spos: INTEGER
			hdr_lines: like next_part_headers
			content_bounds: INTEGER_INTERVAL
		do
			spos := current_position
			hdr_lines := next_part_headers
			if hdr_lines = Void then
				-- Error
			else
				-- Capture MIME header for this part
				-- Find the next occurence of boundary
				-- MIME header begins on the next line
				-- and continues until the next blank line
				spos := current_position
				content_bounds := next_content_bounds
				parts.extend (
					create {AEL_SPRT_MULTIPART_STREAM_PART}.make_from_values(
						[hdr_lines, content_bounds]))
--				spos := content_bounds.upper + 1
			end
--			set_current_position (spos)
		ensure
			monotonic: current_position >= old current_position
		end

	--|--------------------------------------------------------------

	next_part: detachable AEL_SPRT_MULTIPART_STREAM_PART
			-- The next part in the multipart stream starting
			-- on of after the current position
		do
			-- TODO
			if not stream_is_exhausted then
				read_next_part
				Result := parts.last
			end
		end

	next_part_headers: detachable AEL_SPRT_MULTIPART_PART_HEADER_LIST
			-- Read the next part headers from the input stream beginning at current position
		require
			valid_start: current_position <= stream.count
			is_initialized: is_initialized
		local
			spos: INTEGER
			hdr_bounds: INTEGER_INTERVAL
			mph: AEL_SPRT_MULTIPART_PART_HEADER
		do
			spos := current_position
			-- Capture MIME header for this part
			-- Find the next occurence of boundary
			-- MIME header begins on the next line
			-- and continues until the next blank line
			hdr_bounds := next_mime_header_bounds
			if hdr_bounds.is_empty then
				-- No more headers
				spos := stream.count + 1
				set_current_position (stream.count + 1)
			else
				create Result.make
				from
					spos := hdr_bounds.lower
					create mph.make_from_stream (stream, spos)
				until
					not mph.is_valid
				loop
					Result.extend (mph)
					spos := mph.last_parse_position + 1
					-- Skip over CRLF
					if spos + 2 > stream.count then
						create mph
					else
						if stream.item (spos) = '%R' and stream.item (spos + 1) = '%N' then
							spos := spos + 2
						end
						create mph.make_from_stream (stream, spos)
					end
				end
--				ts := stream.substring (hdr_bounds.lower, hdr_bounds.upper)
--				Result := ts.split ('%N')
--				Result.do_all (agent prune_trailing_cr)
--				spos := hdr_bounds.upper + 1
			end
--			set_current_position (spos)
		end

	--|--------------------------------------------------------------

	next_content_bounds: INTEGER_INTERVAL
		-- Beginning and end position of content for this part
		require
--			starts_with_blank_line: stream.substring_index (Ks_srch_crlf2, current_position) = current_position
			valid_start: current_position > 0 and current_position <= stream.count
			pattern_exists: boundary /= Void and then not boundary.is_empty
		local
			v: STRING
			spos, ppos: INTEGER
		do
			v := stream
			spos := current_position
			-- ppos indicates start of boundary
			-- We need to back up 5 characters over the first character
			-- of the pattern, as well as CR LF CR LF
			ppos := searcher.substring_index (v, boundary, spos, v.count)
			create Result.make (spos, ppos - 5)
			set_current_position (ppos)
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_content_type (ct: STRING): BOOLEAN
			-- Is the given string a valid content type?
		local
			hc: AEL_HTTP_CONSTANTS
		do
			create hc
			Result := hc.is_valid_multipart_content_type (ct)
		end

	is_valid: BOOLEAN
			-- Is Current valid?

--|========================================================================
feature -- Implementation
--|========================================================================

	next_mime_header_bounds: INTEGER_INTERVAL
		-- Beginning and end position of next MIME header
		require
			is_initialized: is_initialized
			valid_start: current_position > 0 and current_position <= stream.count
		local
			v: STRING
			spos, ppos: INTEGER
			pos: INTEGER
		do
			v := stream
			spos := current_position
			create Result.make (1, 0)
			ppos := searcher.substring_index (v, boundary, spos, v.count)
			if ppos /= 0 then
				-- skip over CR LF
				spos := ppos + boundary.count + 2
				pos := index_of_next_blank_line (v, spos)
				if pos = 0 then
						-- We're at the end, nothing left to process
				else
					Result.resize_exactly (spos, pos - 3)
					set_current_position (pos + 2)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	index_of_next_blank_line (v: STRING; spos: INTEGER): INTEGER
			-- Starting position of the next blank line in given string
		local
			i: INTEGER
		do
			i := v.substring_index (Ks_srch_crlf2, spos)
			if i > 0 then
				-- Search pattern includes 4 characters (CRLFCRLF),
				-- start of blank line is two characters into the pattern
				Result := i + 2
			end
		end

	--|--------------------------------------------------------------

	prune_trailing_cr (v: STRING)
			-- Remove carriage return from end of string, if any
		require
			exists: v /= Void
		do
			if not v.is_empty and then v.item (v.count) = '%R' then
				v.remove (v.count)
			end
		ensure
			pruned: (not old v.is_empty and then (old v).item (old v.count) = '%R') implies v.count = old v.count - 1
		end

	--|--------------------------------------------------------------

	searcher: STRING_8_SEARCHER

	input_update_function: detachable FUNCTION [TUPLE [like Current], STRING]
		note
			option: stable
			attribute
		end

	private_position: INTEGER

	private_start_position: INTEGER

--|========================================================================
feature -- Constants
--|========================================================================

	Ks_srch_crlf2: STRING = "%R%N%R%N"

	--|--------------------------------------------------------------
invariant
	has_parts: parts /= Void

end -- class AEL_SPRT_MULTI_PART_STREAM
