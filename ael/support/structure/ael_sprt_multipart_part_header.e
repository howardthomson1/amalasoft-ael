note
	description: "A single header line from a part of a multi-part stream"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	author: "Jon Hermansen"
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
}"

class AEL_SPRT_MULTIPART_PART_HEADER

inherit
	ANY
		redefine
			default_create, out
		end

create
	default_create, make_from_stream, make_with_values

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	default_create
		do
			create tag.make (32)
			create value.make (64)
		end

	make_from_stream (v: STRING; spos: INTEGER)
			-- Create an instance from the given stream starting at spos 
			-- and through the end of line
		require
			exists: v /= Void
			valid_start: spos > 0 and spos <= v.count
		local
			i, lim: INTEGER
			ts: STRING
		do
			default_create
			ts := tag_from_stream (v, spos)
			if ts /= Void then
				tag := ts
				lim := v.count.min (spos + max_header_length)
				-- Skip over whitespace
				from
					i := spos + ts.count + 1
				until
					i > lim or else not v.item (i).is_space
				loop
					i := i + 1
				end
				-- Extract value, if any
				from
				until
					i > lim or else v.item (i) = '%R'
				loop
					value.extend (v.item (i))
					i := i + 1
				end
			end
			last_parse_position := spos.max (i - 1)
		ensure
			position_set: is_valid implies last_parse_position > spos
		end

	--|--------------------------------------------------------------

	make_with_values (t, v: STRING)
			-- Create an instance from the given tag and value
		require
			tag_exists: t /= Void and not t.is_empty
		do
			default_create
			tag.append (t)
			if v /= Void then
				value.append (v)
			end
		end

 --|========================================================================
feature -- Measurement
 --|========================================================================

	last_parse_position: INTEGER
			-- Index in original stream of last character in part header

	max_header_length: INTEGER = 1000

 --|========================================================================
feature -- Status
 --|========================================================================

	tag: STRING

	value: STRING

	out: STRING
			-- String representation of 'Current'
		do
			Result := tag + ": " + value
		end

 --|========================================================================
feature -- Status setting
 --|========================================================================

	set_value (v: STRING)
		require
			exists: v /= Void
		do
			value.wipe_out
			value.append (v)
		end

 --|========================================================================
feature -- Validation
 --|========================================================================

	substring_is_valid_header (v: STRING; spos: INTEGER): BOOLEAN
			-- Is the given substring a valid header?
			-- Header is valid if it has a tag
			-- followed by a colon.
			-- Value is optional
		require
			exists: v /= Void
			valid_start: spos > 0 and spos <= v.count
		do
			Result := tag_from_stream (v, spos) /= Void
		end

	is_valid: BOOLEAN
			-- Is 'Current' a valid header?
		do
			Result := not tag.is_empty
		end

 --|========================================================================
feature -- Implementation
 --|========================================================================

	tag_from_stream (v: STRING; spos: INTEGER): STRING
			-- Tag extracted from given stream, if any
		require
			exists: v /= Void
			valid_start: spos > 0 and spos <= v.count
		local
			i, lim: INTEGER
		do
			create Result.make (32)
			lim := v.count.min (spos + max_header_length)
			from
				i := spos
			until
				i > lim or else v.item (i) = ':' or v.item (i) = '%R'
			loop
				Result.extend (v.item (i))
				i := i + 1
			end
			if i > lim or else v.item (i) /= ':' then
				Result.wipe_out
			end
		end

	--|--------------------------------------------------------------
invariant
	has_tag: tag /= Void and (is_valid implies not tag.is_empty)
	has_value: value /= Void

end -- class AEL_SPRT_MULTIPART_PART_HEADER
