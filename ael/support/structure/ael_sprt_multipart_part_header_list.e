note
	description: "A list of headers line from a part of a multi-part stream"
	license: "Eiffel Forumn License v2 - See license.txt"
	source: "{
		Amalasoft Corporation
		273 Harwood Avenue
		Littleton, MA 01460 USA
		}"
	status: "Copyright 1995-2010, Amalasoft; NOT EVEN CLOSE TO DONE";
	author: "Jon Hermansen"
	date: "$Date: 2008/04/21 $";
	revision: "$Revision: 001$";
	howto: "{
}"

class AEL_SPRT_MULTIPART_PART_HEADER_LIST

inherit
	LINKED_LIST [AEL_SPRT_MULTIPART_PART_HEADER]
	AEL_HTTP_CONSTANTS
		undefine
			default_create, copy, is_equal
		end

create
	make

 --|========================================================================
feature -- Access
 --|========================================================================

	item_by_tag (v: STRING): detachable like item
			-- Item in list whose tag corresponds to 'v'
		local
			oc: like cursor
		do
			oc := cursor
			from
				start
			until
				exhausted or Result /= Void
			loop
				if tag_matches (v, item) then
					Result := item
				end
				forth
			end
			go_to (oc)
		ensure
			Result /= Void implies has (Result)
		end

	--|--------------------------------------------------------------

	has_item_by_tag (v: STRING): BOOLEAN
			-- Does Current have an item with a tag 'v'?
		do
			Result := there_exists (agent tag_matches (v, ?))
		end

	has_content_disposition_hdr: BOOLEAN
			-- Does Current have a header for Content-Disposition?
		do
			Result := has_item_by_tag (
				{AEL_HTTP_CONSTANTS}.Ks_hthdr_content_disposition)
		end

	--|--------------------------------------------------------------

	value_by_tag (v: STRING): STRING
			-- Value for item in list whose tag corresponds to 'v'
		do
			if attached item_by_tag (v) as ti then
				Result := ti.value
			else
				Result := ""
			end
		ensure
			exists_if_matches: not Result.is_empty implies has_item_by_tag (v)
		end

	--|--------------------------------------------------------------

	content_disposition_hdr: STRING
			-- Text of Content-Disposition header
		do
			Result := value_by_tag (
				{AEL_HTTP_CONSTANTS}.Ks_hthdr_content_disposition)
		ensure
			exists: not is_empty implies Result /= Void
		end

	--|--------------------------------------------------------------

	tv_list (lbl: STRING): AEL_SPRT_TV_PAIR_LIST
			-- List of tag-value pairs in header with label 'lbl'
		local
			hdr: STRING
		do
			hdr := value_by_tag (lbl)
			if hdr /= Void then
				create Result.make_from_stream (
					hdr,
					{AEL_HTTP_CONSTANTS}.Ks_hthdr_multipart_tag_value_separator,
					True)
			else
				create Result.make
			end
		ensure
			exists_for_valid_tag: has_item_by_tag (lbl) implies not Result.is_empty
		end

	--|--------------------------------------------------------------

	content_disposition_tv_list: like tv_list
			-- TV pair list extracted from the content-disposition 
			-- header (if any)
		do
			Result := tv_list ({AEL_HTTP_CONSTANTS}.Ks_hthdr_content_disposition)
		ensure
			exists_if_valid: has_content_disposition_hdr implies Result /= Void
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	tag_matches (v: STRING; ti: like item): BOOLEAN
		do
			Result := ti.tag.is_equal (v)
		end

	--|--------------------------------------------------------------
invariant
	invariant_clause: True -- Your invariant here

end -- class AEL_SPRT_MULTIPART_PART_HEADER_LIST
