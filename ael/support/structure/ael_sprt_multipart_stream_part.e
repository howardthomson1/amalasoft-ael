class AEL_SPRT_MULTIPART_STREAM_PART

create
	make_from_values

 --|========================================================================
feature
 --|========================================================================

	make_from_values (values: TUPLE [like headers, like content_bounds])
		do
			create headers.make
			create content_bounds.make (0, 0)
			if attached {like headers} values.item (1) as v1 then
				headers := v1
			end
			if attached {like content_bounds} values.item (2) as v2 then
				content_bounds := v2
			end
		ensure
			made_with_header: attached {like headers} values.item (1) as v1
				and then v1 = headers
			made_with_bounds: attached {like content_bounds} values.item (2) as v2
				and then content_bounds = v2
		end

--|========================================================================
feature -- Status
--|========================================================================

	headers: AEL_SPRT_MULTIPART_PART_HEADER_LIST
	content_bounds: INTEGER_INTERVAL

end -- class AEL_SPRT_MULTIPART_STREAM_PART
