note
	description: "{
Collection of parameters driving serialization/deserialization
}"

class AEL_SPRT_SERIAL_PARAMS_2

inherit
	ANY
		redefine
			default_create
		end

create
	default_create

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	default_create
		do
			create flags.make (11)
			create patterns.make (11)
			initialize_parameters
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_parameters
		require
			flags_exist: flags /= Void
			patterns_exist: patterns /= Void
		do
			flags.wipe_out
			patterns.wipe_out
			include_exclude_attrs := Void
			initialize_flags
			initialize_patterns
		end

	--|--------------------------------------------------------------

	initialize_flags
			-- Initialize common and special flags
		do
		end

	initialize_patterns
			-- Initialize common and special patterns
		do
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_flag (nm: STRING; tf: BOOLEAN)
			-- Add flag by name 'nm', with state 'tf'
			-- Example: set_flag ("tag_label_suppressed", True)
		do
			flags.force (tf, nm)
		end

	--|--------------------------------------------------------------

	set_pattern (nm, p: STRING)
			-- Add pattern by name 'nm', with value 'p'
			-- Example: set_pattern ("serial_out_prolog", "{")
		do
			patterns.force (p, nm)
		end

	--|--------------------------------------------------------------

	remove_pattern (nm: STRING)
			-- Remove from patterns item by name 'nm'
		do
			if patterns.has (nm) then
				patterns.remove (nm)
			end
		end

	--|--------------------------------------------------------------

--RFO 	set_tag_filter (v: like tag_filter)
--RFO 			-- Set 'tag_filter' to 'v'
--RFO 		do
--RFO 			tag_filter := v
--RFO 		end

--RFO 	unset_tag_filter
--RFO 		do
--RFO 			tag_filter := Void
--RFO 		end

	--|--------------------------------------------------------------

	set_serial_element_range (v: like serial_element_range)
			-- Set 'serial_element_range' to 'v'
		do
			serial_element_range := v
		end

	unset_serial_element_range
		do
			serial_element_range := Void
		end

--|========================================================================
feature -- Status
--|========================================================================

	flags: HASH_TABLE [BOOLEAN, STRING]
			-- Flags denoting states, options

	patterns: HASH_TABLE [STRING, STRING]
			-- Patterns used in serialized forms

	include_exclude_attrs: detachable LINKED_LIST [STRING]
			-- Attributes to include or exclude in/from all attribute 
			-- lists (if any)
			-- If 'exclude_inex_attrs', then sense is 'exclude', else 
			-- sense is 'include'

	--|--------------------------------------------------------------

--RFO 	tag_filter: LIST [STRING]
			-- Set of only tags to include in serialized output
			-- If Void, then all (default) tags are included
			--
			--RFO currently resides in AEL_SPRT_ATTRIBUTABLE but
			-- will move here eventually

	serial_element_range: detachable INTEGER_INTERVAL
			-- Range (lower/upper) of indices of elements (if any)
			-- to be included in serial output
			-- If Void, then all elements are included

	--|--------------------------------------------------------------

--RFO 	has_filter_tag (v: STRING): BOOLEAN
--RFO 			-- Does Current have a tag filter, and does it include 'v'?
--RFO 			--
--RFO 			--RFO currently resides in AEL_SPRT_ATTRIBUTABLE but
--RFO 			-- will move here eventually
--RFO 		require
--RFO 			exists: v /= Void
--RFO 		local
--RFO 			old_cmp: BOOLEAN
--RFO 		do
--RFO 			if attached tag_filter as tc then
--RFO 				old_cmp := tc.object_comparison
--RFO 				tc.compare_objects
--RFO 				Result := tc.has (v)
--RFO 				if not old_cmp then
--RFO 					tc.compare_references
--RFO 				end
--RFO 			end
--RFO 		end

--|========================================================================
feature -- Access
--|========================================================================

	value_for_flag (nm: STRING): BOOLEAN
			-- Value for flag by name 'nm'
			-- Result is also false if flag is unknown
		do
			Result := flags.item (nm)
		end

	value_for_pattern (nm: STRING): STRING
			-- Value for pattern by name 'nm'
			-- Result is empty if pattern is unknown
		do
			Result := patterns.item (nm)
			if Result = Void then
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Common flags
--|========================================================================

	has_elements: BOOLEAN
			-- Does Current -POTENTIALLY- have subordinate elements?
		do
			Result := value_for_flag ("has_elements")
		end

	include_elements_tag: BOOLEAN
			-- Include in serial output a separate elements prolog
			-- and epilog?
		do
			Result := value_for_flag ("include_elements_tag")
		end

	elements_as_array_enabled: BOOLEAN
			-- Are elements (if any) to be treated as an array?
			-- Default is as separate objects
		do
			Result := value_for_flag ("elements_as_array_enabled")
		end

	tag_label_suppressed: BOOLEAN
			-- Should tag label be suppressed on serial output?
		do
			Result := value_for_flag ("tag_label_suppressed")
		end

	attribute_tags_quoted: BOOLEAN
			-- Should attribute tags be quoted?
		do
			Result := value_for_flag ("attribute_tags_quoted")
		end

	element_tags_quoted: BOOLEAN
			-- Should element tags be quoted?
		do
			Result := value_for_flag ("element_tags_quoted")
		end

	attributes_sorted: BOOLEAN
			-- Should attributes be sorted by tag?
		do
			Result := value_for_flag ("sort_attributes")
		end

	tag_filter_inverted: BOOLEAN
		do
			Result := value_for_flag ("tag_filter_inverted")
		end

	item_tag_filter_inverted: BOOLEAN
		do
			Result := value_for_flag ("item_tag_filter_inverted")
		end

	has_inex_attrs: BOOLEAN
			-- Does current have any include/exclude attributes?
		do
			Result := include_exclude_attrs /= Void and then
				not include_exclude_attrs.is_empty
		end

	exclude_inex_attrs: BOOLEAN
			-- Should include/exclude attributes (if any) be excluded?
			-- (default is for them to be included)
		do
			Result := value_for_flag ("exclude_inex_attrs")
		end

--|========================================================================
feature -- Common patterns
--|========================================================================

	tag_label: STRING
		do
			Result := value_for_pattern ("tag_label")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	elements_tag_label: STRING
			-- Label introducing elements, if any
		do
			Result := value_for_pattern ("elements_tag_label")
		end

	--|--------------------------------------------------------------

	serial_out_prolog: STRING
			-- String that precedes attributes in serial output
		do
			Result := value_for_pattern ("serial_out_prolog")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	serial_out_prolog_end: STRING
			-- String the terminates the serial_out_prolog,
			-- after attributes have been added
		do
			Result := value_for_pattern ("serial_out_prolog_end")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	serial_out_epilog: STRING
			-- String that follows attributes and elements in serial output
		do
			Result := value_for_pattern ("serial_out_epilog")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	elements_out_prolog: STRING
			-- String that precedes elements in serial output
		do
			Result := value_for_pattern ("elements_out_prolog")
		ensure
			exists: Result /= Void
		end

	elements_out_epilog: STRING
			-- String that follows elements in serial output
		do
			Result := value_for_pattern ("elements_out_epilog")
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attribute_separator: STRING
			-- String that separates attributes in serial output
		do
			Result := value_for_pattern ("attribute_separator")
		ensure
			exists: Result /= Void
		end

	attribute_element_separator: STRING
			-- String that separates attributes from elements
			-- in serial output
		do
			Result := value_for_pattern ("attribute_element_separator")
		ensure
			exists: Result /= Void
		end

	element_separator: STRING
			-- String that separates elements in serial output
		do
			Result := value_for_pattern ("element_separator")
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Common parameter setting
--|========================================================================

	set_has_elements (tf: BOOLEAN)
		do
			set_flag ("has_elements", tf)
		end

	set_attribute_separator (v: STRING)
			-- Set the string that separates attributes in serial output 
			-- to 'v'
		do
			if v /= Void and then not v.is_empty then
				set_pattern ("attribute_separator", v)
			else
				remove_pattern ("attribute_separator")
			end
		end

	--|--------------------------------------------------------------

	set_attribute_element_separator (v: STRING)
			-- Set the string that separates attributes from elements
			-- in serial output to 'v'
		do
			if v /= Void then
				set_pattern ("attribute_element_separator", v)
			else
				remove_pattern ("attribute_element_separator")
			end
		end

	--|--------------------------------------------------------------

	enable_include_elements_tag
			-- Enable including in serial output a separate elements prolog
			-- and epilog
		do
			set_flag ("include_elements_tag", True)
		end

	disable_include_elements_tag
			-- Disable including in serial output a separate elements prolog
			-- and epilog
		do
			set_flag ("include_elements_tag", False)
		end

	--|--------------------------------------------------------------

	set_tag_label (v: STRING)
		do
			if v /= Void then
				set_pattern ("tag_label", v)
			else
				remove_pattern ("tag_label")
			end
		end

	set_tag_label_suppressed (tf: BOOLEAN)
			-- Set tag_label_suppressed to 'tf'
		do
			set_flag ("tag_label_suppressed", tf)
		end

	set_attribute_tags_quoted (tf: BOOLEAN)
			-- attribute_tags_quoted to 'tf'
		do
			set_flag ("attribute_tags_quoted", tf)
		end

	--|--------------------------------------------------------------

	enable_elements_as_array
			-- Enable showing elements in array form
		do
			set_flag ("elements_as_array_enabled", True)
		end

	disable_elements_as_array
			-- Disable showing elements in array form
		do
			set_flag ("elements_as_array_enabled", False)
		end

	enable_attribute_sort
			-- Enable sorting attributes (ascii, by tag)
		do
			set_flag ("sort_attributes", True)
		end

	disable_attribute_sort
			-- Enable sorting attributes (ascii, by tag)
		do
			set_flag ("sort_attributes", False)
		end

	invert_tag_filter
			-- Change the sense of the attribute tag filter (if any)
			-- to inverted.
			-- By default, presense of tag filter denotes the set of 
			-- attribute tags to include in the serial output, but when 
			-- inverted, the filter denots the set of tags to exclude
		do
			set_flag ("tag_filter_inverted", True)
		end

	revert_tag_filter
			-- Change the sense of the attribute tag filter (if any)
			-- to normal/default behavior
			-- By default, presense of tag filter denotes the set of 
			-- attribute tags to include in the serial output
		do
			set_flag ("tag_filter_inverted", False)
		end

	--|--------------------------------------------------------------

	invert_item_tag_filter
			-- Change the sense of the item tag filter (if any)
			-- to inverted.
			-- By default, presense of tag filter denotes the set of 
			-- attribute tags to include in the serial output, but when 
			-- inverted, the filter denots the set of tags to exclude
		do
			set_flag ("item_tag_filter_inverted", True)
		end

	revert_item_tag_filter
			-- Change the sense of the item tag filter (if any)
			-- to normal/default behavior
			-- By default, presense of tag filter denotes the set of 
			-- attribute tags to include in the serial output
		do
			set_flag ("item_tag_filter_inverted", False)
		end

	--|--------------------------------------------------------------

	set_include_exclude_attrs (v: like include_exclude_attrs; xf: BOOLEAN)
			-- Set include/exclude attributes to 'v'
			-- If 'xf', sense is 'exclude', else sense is 'include'
		do
			include_exclude_attrs := v
			set_flag ("exclude_inex_attrs", xf)
		end

end -- class AEL_SPRT_SERIAL_PARAMS_2
