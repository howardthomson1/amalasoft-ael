class AEL_SPRT_SPLITTABLE_LINKED_LIST [G]
-- A LIST that supports sublisting

inherit
	LINKED_LIST [G]
	AEL_SPRT_SPLITTABLE_LIST [G]
		undefine
			prune_all, prune, first, last, new_cursor, readable,
			after, before, off, isfirst, islast, is_inserted,
			start, finish, move, go_i_th, remove, copy
		end

create
	make

end -- class AEL_SPRT_SPLITTABLE_LINKED_LIST
