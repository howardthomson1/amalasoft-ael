deferred class AEL_SPRT_SPLITTABLE_LIST [G]
-- A LIST that supports sublisting

inherit
	LIST [G]

--|========================================================================
feature -- Transformation
--|========================================================================

	split_at (p: INTEGER): ARRAY [like Current]
			-- Array of two sublists resulting from splitting Current 
			-- after position 'p'
			--
			-- Original remains unchanged
		require
			valid_position: p > 0 and p <= count
		local
			i, lim: INTEGER
			r1, r2: like Current
		do
			r1 := twin
			r2 := twin
			Result := <<r1, r2>>
			from
				r1.go_i_th (p)
				r1.forth
			until r1.exhausted
			loop
				r1.remove
			end
			-- 'p' is also number of items to remove from front of 'r2'
			from
				r2.start
				i := 1
			until r2.exhausted or r2.is_empty or i > p
			loop
				if i <= p then
					r2.remove
				else
					r2.forth
				end
				i := i + 1
			end
		ensure
			exists: Result /= Void
			has_two: Result.count = 2
			counts_add: Result.item (1).count + Result.item (2).count = count
			original_unchanged: Current.is_equal (old Current)
		end

end -- class AEL_SPRT_SPLITTABLE_LIST
