class AEL_SPRT_STRING_PAIR
-- A TUPLE of 2 STRINGs

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (v1, v2: STRING)
		do
			first := v1
			second := v2
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (i: INTEGER): STRING
		require
			valid_index: i = 1 or i = 2
		do
			if i = 1 then
				Result := first
			else
				Result := second
			end
		end

	--|--------------------------------------------------------------

	first: STRING
			-- First string item

	second: STRING
			-- Second string item

	last: STRING
			-- Last (second) string item
		do
			Result := second
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put_first (v: like item)
		do
			first := v
		end

	put_last (v: like item)
		do
			second := v
		end

--|========================================================================
feature -- Status
--|========================================================================

	lower: INTEGER = 1
	upper: INTEGER = 2

end -- class AEL_SPRT_STRING_PAIR
