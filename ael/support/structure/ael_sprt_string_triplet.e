class AEL_SPRT_STRING_TRIPLET
-- A TUPLE of 3 STRINGs

create
	make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (v1, v2, v3: STRING)
		do
			first := v1
			second := v2
			third := v3
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (i: INTEGER): STRING
		require
			valid_index: i >= 1 and i <= 3
		do
			inspect i
			when 1 then
				Result := first
			when 2 then
				Result := second
			else
				Result := third
			end
		end

	--|--------------------------------------------------------------

	first: STRING
			-- First string item

	second: STRING
			-- Second string item

	third: STRING
			-- Third string item

	last: STRING
			-- Last (3rd) string item
		do
			Result := third
		end

--|========================================================================
feature -- Element change
--|========================================================================

	put_first (v: like item)
		do
			first := v
		end

	put_second (v: like item)
		do
			second := v
		end

	put_last (v: like item)
		do
			third := v
		end

--|========================================================================
feature -- Status
--|========================================================================

	lower: INTEGER = 1
	upper: INTEGER = 3

end -- class AEL_SPRT_STRING_TRIPLET
