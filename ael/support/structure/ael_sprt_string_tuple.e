class AEL_SPRT_STRING_TUPLE
-- A TUPLE-like collection of one or more STRINGs

inherit
	ARRAYED_LIST [STRING]
		rename
			item as al_item
		end

create
	make, make_from_strings

create {AEL_SPRT_STRING_TUPLE}
	make_filled

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_strings (sl: FINITE [STRING])
		require
			exists: sl /= Void
		do
			make (sl.count)
			fill (sl)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item (i: INTEGER): STRING
		require
			valid_index: i > 0 and i <= count
		do
			Result := i_th (i)
		end

end -- class AEL_SPRT_STRING_TUPLE
