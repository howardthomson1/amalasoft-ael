--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Tag-value pair of the form:
--|   <tag>"="<value>
--|----------------------------------------------------------------------

class AEL_SPRT_TV_PAIR

inherit
	COMPARABLE
		redefine
			default_create, out
		end

create
	make, make_from_string, make_from_string_unquote,
	make_from_tag_and_value, make_from_tag_and_value_copied

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	default_create
		do
			create tag.make (0)
			create private_value.make (0)
			Precursor
		end

	--|--------------------------------------------------------------

	make
			-- Create Current with empty tag and value
		do
			default_create
--RFO 			initialize
		end

	--|--------------------------------------------------------------

	make_from_string (v: STRING)
			-- Create Current from well-formed tag=value string
		require
			valid: is_tv_pair (v)
		do
			make
			initialize_from_string (v)
		end

	--|--------------------------------------------------------------

	make_from_string_unquote (v: STRING)
			-- Create Current from well-formed tag=value string, where
			-- the value component might be quoted and the quotes are
			-- not intended to be part of the value
		require
			valid: is_tv_pair (v)
		local
			ts: STRING
		do
			make_from_string (v)
			ts := asr.quotes_stripped (value)
			if ts /= value then
				set_value (ts)
			end
		end

	--|--------------------------------------------------------------

	make_from_tag_and_value (t, v: STRING)
			-- Create Current from the given tag and value components
			-- Do not make copies of 't' and 'v'; rather, refer to
			-- them from tag and value
		require
			valid_tag: is_valid_tag (t)
			value_exists: v /= Void
		do
			make
			set_tag (t)
			set_value (v)
--			initialize
		end

	--|--------------------------------------------------------------

	make_from_tag_and_value_copied (t, v: STRING)
			-- Create Current from the given tag and value components
			-- Make unique copies of given tag and value
		require
			valid_tag: is_valid_tag (t)
			value_exists: v /= Void
		do
			default_create
			tag.append (t)
			set_value (v)
--			initialize
		end

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

--RFO 	initialize
--RFO 		require
--RFO 			has_tag: tag /= Void
--RFO 			has_value: value /= Void
--RFO 		do
--RFO 		end

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_from_string (v: STRING)
			-- Initialize Current from the given tag=value string
		require
			exists: v /= Void
			is_tv_pair: is_tv_pair(v)
		do
			initialize_from_substring (v, 1, v.count)
		end

	--|--------------------------------------------------------------

	initialize_from_substring (v: STRING; sp, ep: INTEGER)
			-- Initialize Current from the tag=value sub string
			-- in 'v' between positions sp and ep
		require
			exists: v /= Void
			is_tv_pair: substring_is_tv_pair(v, sp)
			valid_start: sp > 0
			valid_end: ep >= sp and ep <= v.count
		local
			qpos: INTEGER
		do
			qpos := v.index_of ('=', sp)
			set_tag (v.substring (sp, qpos - 1))
			set_value (v.substring (qpos + 1, ep))
			--| Trim off any leading or trailing whitespace
			asr.trim (value)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_tv_pair (v: STRING): BOOLEAN
			-- Is the given string a valid tv pair?
		do
			if v /= Void and then v.count >= 2 then
				Result := v.index_of('=',1) >= 2
			end
		end

	substring_is_tv_pair (v: STRING; sp: INTEGER): BOOLEAN
			-- Is the substring of 'v' beginning at position 'sp'
			-- a valid tv pair?
		require
			valid_start: sp > 0
		do
			if v /= Void and then v.count >= (sp + 2) then
				Result := v.index_of ('=', sp) >= (sp + 2)
			end
		end

 --|------------------------------------------------------------------------

	tag: STRING
        note
            option: stable
        attribute
		end

	value: STRING
		do
			Result := private_value
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of Current
		do
			Result := tag + "=" + value
		end

	--|--------------------------------------------------------------

	decorated_out (pf, sf: STRING): STRING
			-- String representation of Current with an optional
			-- prefix and suffix string
		local
			tp, ts: STRING
		do
			tp := ""
			if pf /= Void then
				tp := pf
			end
			ts := ""
			if sf /= Void then
				ts := sf
			end
			Result := pf + tag + "=" + value + sf
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_tag (v: STRING)
		require
			valid_tag: is_valid_tag (v)
			not_set: tag = Void or else tag.is_empty
		do
			if tag = Void then
				create tag.make (v.count)
			else
				tag.wipe_out
			end
			if not v.is_empty then
				tag.append (asr.quotes_stripped (v))
			end
		ensure
			tag_exists: tag /= Void
			is_set: tag ~ v or tag ~ asr.quotes_stripped (v)
		end

	--|--------------------------------------------------------------

	set_value (v: STRING)
		do
			if value = Void then
				create private_value.make (0)
			else
				value.wipe_out
			end
			if v /= Void and then not v.is_empty then
				value.append (asr.quotes_stripped (v))
			end
		ensure
			is_set: v /= Void and then not v.is_empty implies
				(value ~ v or value ~ asr.quotes_stripped (v))
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_tag (v: STRING): BOOLEAN
		do
			Result := v /= Void and then not v.is_empty
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is Current less than `other'?
		do
			Result := tag < other.tag
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_value: STRING

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

end -- class AEL_SPRT_TV_PAIR

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 08-Aug-2012
--|     Compiled and tested using Eiffel 7.0 for void-safe mode
--|     Fixed insidious issue with unquoted values in make_from_string
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using any of the creation 
--| routines.
--| 'make' creates an empty pair (i.e. no tag, no value)
--| 'make_from_string' accepts a well-formed string of the form:
--|      my_tag=my_value
--| 'make_from_string_unquote' accepts a well-formed string of the form:
--|      my_tag="my_value" or my_tag='my_value'
--| Enclosing quotes are stripped from the associated value
--|
--| To ensure that a tag=value pair is valid before calling one of the
--| make_from_string* routines, create an empty instance and call its
--| is_tv_pair function.
--|
--| The tag and value strings are accessible via the 'tag' and 'value'
--| queries
--| The tag string can be set by calling set_tag, but the tag cannot 
--| be set if it is already non-empty (i.e. it cannot be overwritten
--| once set.
--| The value string can be set by calling set_value.  The value can 
--| be reset as often as desired.  If the argument to set_value is 
--| Void, the value string will be set to an empty string.
--|
--| Instances of this class can be compared, with comparison based on
--| the tag string.
--|----------------------------------------------------------------------
