--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A table of tag-value pairs, searchable by tag, and with a
--| projection sorted by tag
--|----------------------------------------------------------------------

class AEL_SPRT_TV_PAIR_LIST

inherit
	AEL_DS_KV_TABLE [AEL_SPRT_TV_PAIR]
--RFO 		redefine
--RFO 			new_item_from_string, new_item_from_tag_and_value
--RFO 		end

create
	make, make_from_stream

create {AEL_SPRT_TV_PAIR_LIST}
	aht_make

--|========================================================================
feature -- Support
--|========================================================================

	--RFO new_item_from_string (s: detachable STRING; qf: BOOLEAN): like item_for_iteration
	--RFO 	do
	--RFO 		if s = Void then
	--RFO 			create Result.make
	--RFO 		else
	--RFO 			if qf then
	--RFO 				create Result.make_from_string_unquote (s)
	--RFO 			else
	--RFO 				create Result.make_from_string (s)
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--RFO new_item_from_tag_and_value (t, v: STRING): like item_for_iteration
	--RFO 	do
	--RFO 		create Result.make_from_tag_and_value (t, v)
	--RFO 	end

	--|--------------------------------------------------------------
invariant

end -- class AEL_SPRT_TV_PAIR_LIST

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using the make routine.
--| Add items using extend.
--| Use value_for_tag query (with tag string) to find items in the
--| list that match the tag.
--| Output format comes from multiple routines and offers some
--| tweaking by taking output prefix and/or suffix arguments.
--| If you want to mark local time, use either make or make_fine_grained.
--| If you want to mark UTC time, use either make_utc or make_fine_utc.
--| Because this is a descendent of STRING, the string representation
--| is simply Current and references to objects of this class can be
--| treated as if they were strings.
--| If you set the associated date bia set_from_date_time, be sure to
--| update the string rendition every time the external data object is
--| updated (if at all).  The string representation is kept up-to-date
--| automatically in all other cases.
--| This class depends on the Eiffel Base libraries
--|----------------------------------------------------------------------
