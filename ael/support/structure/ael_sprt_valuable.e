deferred class AEL_SPRT_VALUABLE
-- Abstract notion of having a (comparable) value

inherit
	COMPARABLE

--|========================================================================
feature -- Status
--|========================================================================

	value: like Kta_value
		deferred
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_value (v: like Kta_value)
		require
			exists: v /= Void
		deferred
		ensure
			is_set: value ~ v
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_value (v: like value): BOOLEAN
		deferred
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is Current less than other?
		do
			Result := value < other.value
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	Kta_value: COMPARABLE
			-- Type anchor for value
		require False
		do
			check False then end
		ensure False
		end

	--| If wanting to use an expanded type for value, expansion 
	--| becomes a problem because COMPARABLE is not expanded and 
	--| of course expanded types are.  This means the descendent 
	--| must not only redefine Kta_value to the desired type, 
	--| but must also rename value to value_ref, set_value to 
	--| set_value_ref and is_valid_valid to is_valid_value_ref 
	--| and implement this simple routines
	--| Example:  value of type REAL_64
	--|
	--| value_ref: COMPARABLE -- as attribute
	--|
	--| set_value (v: like value)
	--|   do
	--|     value_ref.set_item (v)
	--|   end
	--|
	--| set_value_ref (v: like Kta_value)
	--|   do
	--|     value_ref := v
	--|   end
	--|
	--| is_valid_value_ref (v: like value_ref): BOOLEAN
	--|   do
	--|     Result := True
	--|   end
	--|
	--| Kta_value: REAL_64
	--|     -- Type anchor for value
	--|   do
	--|     check False then end
	--|   end

end -- class AEL_SPRT_IDENTIFIABLE
