class AEL_SPRT_VALUE_LABEL

inherit
	STRING
		rename
			make as str_make
		end

create
	make

create {AEL_SPRT_VALUE_LABEL}
	str_make

--|========================================================================
feature {NONE} -- Initialization
--|========================================================================

	make (lb: STRING; iv: INTEGER; sf: BOOLEAN)
		require
			exists: lb /= Void and then not lb.is_empty
			valid_id: iv > 0
		do
			make_from_string (lb)
			id := iv
			is_settable := sf
		end

--|========================================================================
feature -- Status
--|========================================================================

	id: INTEGER
			-- Integer ID equivalent

	is_settable: BOOLEAN
			-- Is the value associated with this label settable?

end -- class AEL_SPRT_VALUE_LABEL
