class AEL_SPRT_VALUE_LABEL_TABLE
-- Table of value labels

inherit
	HASH_TABLE [AEL_SPRT_VALUE_LABEL, STRING]
		redefine
			make
		end

create
	make, make_initialized

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make (sz: INTEGER)
		do
			Precursor (sz)
			compare_objects
		end

	make_initialized (sz: INTEGER)
		do
			make (sz)
			set_values
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_values
		do
		end

end -- class AEL_SPRT_VALUE_LABEL_TABLE
