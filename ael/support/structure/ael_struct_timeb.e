note
	description: "{
Encapsulation of C timeb struct
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_STRUCT_TIMEB

inherit
	AEL_STRUCT
		redefine
			c_struct_size
		end

create
	make, make_from_c, make_from_utc, make_from_present

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_utc (v: INTEGER)
		do
			make
			set_time (v)
			set_millitm (0)
		end

	--|------------------------------------------------------------------------

	make_from_present
		do
			make
			set_to_present
		end

--|========================================================================
feature -- Status
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_time (v: INTEGER)
			-- Set seconds since epoch
		do
			c_set_tb_time (address, v)
		end

	--|------------------------------------------------------------------------

	set_millitm (v: INTEGER)
			-- Set additional milliseconds since epoch
		require
			non_negative: v >= 0
		do
			c_set_tb_millitm (address, v)
		end

	--|--------------------------------------------------------------

	set_to_present
		do
			c_ftime (address)
		end

--|========================================================================
feature -- Access
--|========================================================================

	time: INTEGER
			-- Seconds since epoch
		do
			Result := c_tb_time (address)
		end

	millitm: INTEGER
			-- Additional milliseconds since epoch
		do
			Result := c_tb_millitm (address)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	c_tb_time (p: POINTER): INTEGER
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb): EIF_INTEGER"
		alias
		"time"
		end

	--|------------------------------------------------------------------------

	c_tb_millitm (p: POINTER): INTEGER
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb): EIF_INTEGER"
		alias
		"millitm"
		end

	--|--------------------------------------------------------------

	c_set_tb_time (p: POINTER; v: INTEGER)
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb, int)"
		alias
		"time"
		end

	--|------------------------------------------------------------------------

	c_set_tb_millitm (p: POINTER; v: INTEGER)
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb, int)"
		alias
		"millitm"
		end

	--|--------------------------------------------------------------

	c_ftime (p: POINTER)
		require
			valid_address: p /= Default_pointer
		external
		"C [macro <time.h>] (struct timeb *)"
		alias
		"ftime"
		end

	--|--------------------------------------------------------------

	c_struct_size: INTEGER
			-- We use ael_types.h because struct timeb is defined in 
			-- sys/time.h on UNIX systems, but in winsock.h on Windows, GRRRR
		external
		"C [macro %"ael_types.h%"]: EIF_INTEGER"
		alias
		"sizeof(struct timeb)"
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-Oct-2013
--|     Created original module (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_STRUCT_TIMEB
