class AEL_SPRT_XATTR_ROUTINES
-- Warppers for system calls to get/set/list/remove ext3 extended 
-- attributes

inherit
	AEL_SPRT_XATTR_CONSTANTS

--|========================================================================
feature -- Queries
--|========================================================================

	has_error: BOOLEAN
			-- Has there been an error dealing with xattrs in this object?
		do
			Result := error_msg /= Void
		end

	error_msg: STRING
			-- Message descibing most recent xattr error, if any

	set_error (msg: STRING)
		require
			exists: msg /= Void
		do
			error_msg := msg
		ensure
			has_error: has_error
		end

	clear_error
		do
			error_msg := Void
			clear_errno
		ensure
			no_error: not has_error
		end

	system_error_msg: STRING
		-- System-supplied error message for current value of errno
		do
			create Result.make_from_c (sterror)
		end

	--|--------------------------------------------------------------

	is_valid_xattr_namespace (v: STRING): BOOLEAN
			-- Is the given string a proper namespace ID?
		do
			if v /= Void and then not v.is_empty then
				Result := legal_xattr_namespaces.has (v)
			end
		end

	--|--------------------------------------------------------------

	is_valid_xattr_tag (v: STRING): BOOLEAN
			-- Is the given string a proper attribute tag?
		local
			flds: LIST [STRING]
		do
			if v /= Void and then not v.is_empty then
				flds := v.split (Kc_ns_separator)
				if flds.count >= 2 then
					Result := legal_xattr_namespaces.has (flds.first)
				end
			end
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	set_xattr (pn, t, v: STRING)
		-- Write to disk the value for the given path
		require
			path_exists: pn /= Void and then not pn.is_empty
			valid_tag: is_valid_xattr_tag (t)
		local
			c_v, c_pn, c_t: C_STRING
			rval: INTEGER
		do
			clear_error
			c_pn := string_to_c_string (pn)
			c_t := string_to_c_string (t)

			c_v := string_to_c_string (v)
			rval := setxattr (c_pn.item, c_t.item, c_v.item, c_v.count)
			if rval /= 0 then
				set_error ("Call to setxattr failed. " + system_error_msg)
			end
		end

	--|--------------------------------------------------------------

	get_xattr (pn, t: STRING): STRING
			-- Read from disk for file at path 'pn' the attribute 't'
		require
			path_exists: pn/= Void and then not pn.is_empty
			valid_tag: is_valid_xattr_tag (t)
		local
			c_v, c_pn, c_t: C_STRING
			rval, vlen: INTEGER
		do
			clear_error
			c_pn := string_to_c_string (pn)
			c_t := string_to_c_string (t)
			vlen := getxattr_size (c_pn.item, c_t.item)
			if vlen = 0 then
				Result := ""
			elseif vlen < 0 then
				-- An error, probably has no attrs,
				--RFO TODO make some sense of this
				Result := ""
			else
				create Result.make (vlen)
				create c_v.make_empty (vlen + 1)
				rval := getxattr (c_pn.item, c_t.item, c_v.item, vlen)
				if rval = 0 then
					Result := ""
				elseif rval > 0 then
					rval := 0
					Result.from_c_substring (c_v.item, 1, vlen)
				end
			end
			if rval /= 0 then
				set_error ("Call to getxattr failed. " + system_error_msg)
			end
		end

	--|--------------------------------------------------------------

	list_xattrs (pn: STRING): LINKED_LIST [STRING]
			-- List of all extended attributes read from disk,
			-- for the associated pathname
			-- N.B. These are the xattr tags, there are NO VALUES in 
			-- these string!!
		require
			path_exists: pn/= Void and then not pn.is_empty
		local
			c_pn: C_STRING
			vlen: INTEGER
			rval: INTEGER
			l_p: MANAGED_POINTER
		do
			clear_error
			c_pn := string_to_c_string (pn)
			vlen := listxattr_size (c_pn.item)
			if vlen = 0 then
				-- No attributes at all, or an error?
				rval := errno
				if rval /= 0 then
					set_error ("Call to listxattr_size failed. " + system_error_msg)
				else
					create Result.make
				end
			elseif vlen <= 0 then
				rval := vlen
			else
				create l_p.make (vlen)
				rval := listxattr (c_pn.item, l_p.item, vlen)
				if rval /= 0 then
					Result := list_from_c_vector (l_p)
				end
				rval := 0
			end
			if rval /= 0 then
				set_error ("Call to listxattr failed. " + system_error_msg)
			end
		ensure
			exists: not has_error implies Result /= Void
		end

--|========================================================================
feature {NONE} -- Support routines
--|========================================================================

	string_to_c_string (v: STRING): C_STRING
		require
			exists: v /= Void
		do
			if v = Void then
				create Result.make_empty (0)
			else
				create Result.make_empty (v.count + 1)
				Result.set_string (v)
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	list_from_c_vector (p: MANAGED_POINTER): LINKED_LIST [STRING]
			-- A list of strings derived from the given  C vector
			-- (sequence of NULL-terminated C strings)
		require
			pointer_exists: p /= Void and then p.item /= default_pointer
		local
			ts: STRING
			i, lim, spos: INTEGER
			tn: NATURAL_8
		do
			create Result.make
			lim := p.count
			from i := 0
			until i >= lim
			loop
				tn := p.read_natural_8 (i)
				if tn = 0 then
					create ts.make (i - spos + 1)
					-- from_c_substring expects 1-based indexing
					ts.from_c_substring (p.item, spos + 1, i)
					Result.extend (ts)
					spos := i + 1
				end
				i := i + 1
			end
		end

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	listxattr (pn, lst: POINTER; sz: INTEGER): INTEGER
			-- For the given filename 'pn', List the values for the
			-- defined extended attributes, writing into *lst.
			-- If 'pn' refers to a symbolic link, get the attributes
			-- for the file to which the link refers.
		external
			"C inline use %"attr/xattr.h%""
		alias
			"return listxattr ($pn, $lst, $sz);"
		end

	--|--------------------------------------------------------------

	listxattr_size (pn: POINTER): INTEGER
			-- For the given filename 'pn', Get the size of the list
			-- of extended attributes
			-- If 'pn' refers to a symbolic link, get the size of the
			-- attribute list for the file to which the link refers
		external
			"C inline use %"attr/xattr.h%""
		alias
			"[
				char buf[1];
				return listxattr ($pn, buf, 0);
			]"
		end

	--|--------------------------------------------------------------

	getxattr (pn, nm, v: POINTER; sz: INTEGER): INTEGER
			-- For the given filename 'pn', Get the value for the
			-- extended attribute 'nm' and write 'sz' bytes into
			-- address of value ('v')
			-- If 'pn' refers to a symbolic link, get the attribute value
			-- for the file to which the link refers.
		external
			"C inline use %"attr/xattr.h%""
		alias
			"return getxattr ($pn, $nm, $v, $sz);"
		end

	--|--------------------------------------------------------------

	getxattr_size (pn, nm: POINTER): INTEGER
			-- For the given filename 'pn', Get the size of the
			-- extended attribute 'nm', to be used by a subsequent
			-- call to getxattr for the actual value
			-- If 'pn' refers to a symbolic link, get the size of the
			-- attribute for the file to which the link refers
		external
			"C inline use %"attr/xattr.h%""
		alias
			"[
				char buf[1];
				return getxattr ($pn, $nm, buf, 0);
			]"
		end

	--|--------------------------------------------------------------

	setxattr (pn, nm, v: POINTER; sz: INTEGER): INTEGER
			-- For the given filename 'pn', set the extended attribute
			-- 'nm' to value 'v' (of size 'sz')
			-- Value can be any data, hence the need for the size
			-- parameter
			-- If 'pn' refers to a symbolic link, set the attribute for
			-- the file to which the link refers, and not the link itself.
		external
			"C inline use %"attr/xattr.h%""
		alias
			"return setxattr ($pn, $nm, $v, $sz, 0);"
		end

	--|--------------------------------------------------------------

	lsetxattr (pn, nm, v: POINTER; sz: INTEGER): INTEGER
			-- For the given filename 'pn', set the extended attribute
			-- 'nm' to value 'v' (of size 'sz')
			-- Value can be any data, hence the need for the size 
			-- parameter
			-- If 'pn' refers to a symbolic link, set the attribute for
			-- the link and not for the file to which the link refers
		external
			"C inline use %"attr/xattr.h%""
		alias
			"return lsetxattr ($pn, $nm, $v, $sz, XATTR_REPLACE);"
		end

	--|--------------------------------------------------------------

	sterror: POINTER
		-- System-supplied error message for current value of errno
		external
			"C inline use <unistd.h>"
		alias
			"return strerror(errno);"
		end

	--|--------------------------------------------------------------

	errno: INTEGER
		-- System-generated error number (0 denotes no error)
		external
			"C inline use <unistd.h>"
		alias
			"return errno;"
		end

	clear_errno
		-- Clear system error code
		external
			"C inline use <unistd.h>"
		alias
			"errno = 0;"
		end

end -- class AEL_SPRT_XATTR_ROUTINES
