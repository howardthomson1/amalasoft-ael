class AEL_SPRT_XATTR
-- An extended attribute (as used in ext3 and other file systems)

inherit
	COMPARABLE
		redefine
			out
		end
	AEL_SPRT_XATTR_ROUTINES
		undefine
			out, is_equal
		end

create
	make_from_string

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_string (fn, v: STRING)
		require
			filename_exists: fn /= Void and then not fn.is_empty
			string_exists: v /= Void and then not v.is_empty
		do
			path := fn
			init_from_string (v)
		end

--|========================================================================
feature -- Status
--|========================================================================

	path: STRING
	namespace: STRING
	label: STRING
	value: STRING

	--|--------------------------------------------------------------

	tag_out: STRING
		do
			if has_error then
				Result := "Uninitialized"
			else
--RFO				create Result.make (namespace.count + label.count + 1)
				create Result.make (label.count)
--RFO				Result.append (namespace)
--RFO				Result.extend (Kc_ns_separator)
				Result.append (label)
			end
		ensure
			exists: not has_error implies Result /= Void
		end

	--|--------------------------------------------------------------

	value_out: STRING
		do
			if value = Void then
				Result := ""
			else
				Result := value
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	out: STRING
		local
			ts, vs: STRING
		do
			if has_error then
				Result := "Uninitialized"
			else
				ts := tag_out
				vs := value_out
				create Result.make (ts.count + vs.count + 1)
				Result.append (ts)
				Result.extend (Kc_tv_separator)
				Result.append (vs)
			end
		end

	--|--------------------------------------------------------------

--|========================================================================
feature -- Status setting
--|========================================================================
	
	init_from_string (v: STRING)
		require
			string_exists: v /= Void and then not v.is_empty
		local
			fields: LIST [STRING]
			t: STRING
		do
			fields := v.split (Kc_tv_separator)
			if fields.count /= 2 then
				set_error ("Malformed string on initialization")
			else
				t := fields.first
				value := fields.last
				fields := t.split (Kc_ns_separator)
				if fields.count /= 2 then
					set_error ("Malformed tag on initialization")
				else
					if not is_valid_xattr_namespace (fields.first) then
						set_error ("Invalid namespace field")
					else
						namespace := fields.first
						label := fields.last
					end
				end
			end
		ensure
			initialized: not has_error implies label /= Void and namespace /= Void
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is Current less than other?
		do
			Result := out < other.out
		end

end -- class AEL_SPRT_XATTR
