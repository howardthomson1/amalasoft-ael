README.txt - Intro to the AEL_TIME cluster

The Amalasoft Eiffel time cluster is a collection of classes that extend
the time library from Eiffel Software.  It depends on the Eiffel base and
time libraries (available in open source or commercial form from Eiffel 
Software www.eiffel.com) and is, like other Eiffel code, portable across 
platforms.

It also depends on the Amalasoft Printf, Support and Numeric clusters.

     ------------------------------------------------------------

The cluster includes the following classes:

  AEL_DATE_TIME_CONSTANTS
    Provides constant values used by the other members of the cluster

  AEL_DATE_TIME_ROUTINES
    Provides common computation, conversion and validation routines.

  AEL_DATE_TIME_NAMES
    Provides string constant values used by the other members of the
    cluster.  These are separated to allow for easier internationalization
    using Eiffel clusters.

  AEL_DT_CALENDAR_WEEK
    Representation of a week in the context of a calendar.

  AEL_DT_CALENDAR_MONTH
    Representation of a month in the context of a calendar.

  AEL_DT_CALENDAR_YEAR
    Representation of a year in the context of a calendar.

  AEL_TIMESTAMP
    A timestamp string of the form: YYYY-MM-DD hh:mm:ss
    Can be made optionally with a finer granularity (subsecond)
    This format will sort naturally because the fields are in
    descending orders of magnitude.  This is especially handy for
    log files that you might want to concatenate after the fact
    to correlate them w/r/t time.

  AEL_UNIX_DATE
    An extension of the Eiffel Software C_DATE class.
    Adds initialization from time_t, default output format

  AEL_UNIX_DATE_TIME
    An extension of the Eiffel Software DATE_TIME class
    Adds initialization from time_t, default output format

There are 2 subdirectories, 'legacy' and 'void-safe' containing classes
that differ between contexts.  In all but AEL_UNIX_DATE_TIME_DURATION,
the classes hold only the header information from the 'indexing' clause
(legacy) or 'note' clause for the associated class.   The fact that this
is necessary is simply ridiculous.

