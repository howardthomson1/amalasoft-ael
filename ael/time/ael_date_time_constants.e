--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Constants supporting calendars and date calculations
--|----------------------------------------------------------------------

class AEL_DATE_TIME_CONSTANTS

inherit
	AEL_DATE_TIME_NAMES
	AEL_DATE_TIME_TYPES

feature -- Constants

	K_thursday: INTEGER = 4	-- for Julian reformation
	K_saturday: INTEGER = 6	-- 1 Jan 1 was a Saturday

	K_first_missing_day: INTEGER = 639787	-- 3 Sep 1752
	K_number_missing_days: INTEGER = 11		-- 11 day correction

	K_maxdays: INTEGER = 43
	K_space: INTEGER = -1

	K_day_len: INTEGER = 3		-- 3 spaces per day */
	K_j_day_len: INTEGER = 4		-- 4 spaces per day */
	K_week_len: INTEGER = 21		-- 7 days * 3 characters */
	K_j_week_len: INTEGER = 28		-- 7 days * 4 characters */
	K_head_sep: INTEGER = 2		-- spaces between day headings */
	K_j_head_sep: INTEGER = 2

	K_max_weeks_per_month: INTEGER = 6

	K_range_tu_nanosecond: INTEGER_INTERVAL
		once
			create Result.make (0, 999)
		end

	K_range_tu_microsecond: INTEGER_INTERVAL
		once
			create Result.make (0, 999)
		end

	K_range_tu_millisecond: INTEGER_INTERVAL
		once
			create Result.make (0, 999)
		end

	K_range_tu_second: INTEGER_INTERVAL
		once
			create Result.make (0, 59)
		end

	K_range_tu_minute: INTEGER_INTERVAL
		once
			create Result.make (0, 59)
		end

	K_range_tu_hour: INTEGER_INTERVAL
		once
			create Result.make (0, 23)
		end

	K_range_tu_month: INTEGER_INTERVAL
		once
			create Result.make (1, 12)
		end

	K_range_tu_week: INTEGER_INTERVAL
		once
			create Result.make (1, 7)
		end

	K_range_tu_year: INTEGER_INTERVAL
		once
			create Result.make (0, 2037)
		end

	K_time_unit_nanosecond: INTEGER = -5
	K_time_unit_microsecond: INTEGER = -4
	K_time_unit_millisecond: INTEGER = -3
			-- reserve -1, -2 for 1/10 and 1/100 of second
	K_time_unit_second: INTEGER = 0
	K_time_unit_minute: INTEGER = 1
	K_time_unit_hour: INTEGER = 2
	K_time_unit_day: INTEGER = 3
	K_time_unit_week: INTEGER = 4
	K_time_unit_month: INTEGER = 5
	K_time_unit_year: INTEGER = 6

	K_seconds_per_minute: INTEGER = 60
	K_minutes_per_hour: INTEGER = 60
	K_hours_per_day: INTEGER = 24
			-- not true at DST changeover
	K_days_per_week: INTEGER = 7
	K_min_days_per_month: INTEGER = 28
	K_max_days_per_month: INTEGER = 31
	K_weeks_per_year: INTEGER = 52
	K_months_per_year: INTEGER = 12
	K_min_days_per_year: INTEGER = 365
	K_max_days_per_year: INTEGER = 366

	K_seconds_per_hour: INTEGER
		once
			Result := K_seconds_per_minute * K_minutes_per_hour
		end

	K_seconds_per_day: INTEGER
			-- Typical number of seconds per day
			-- Does NOT consider DST transitions (23 and 25 your days)
		once
			Result := 60 * 60 * 24
		end

	K_seconds_per_week: INTEGER
			-- Typical number of seconds per week
			-- Does NOT consider DST transitions (23 and 25 your days)
		once
			Result := K_seconds_per_day * 7
		end

	days_in_month_table: ARRAY [ INTEGER ]
			-- The first 12 elements are non-leap year values,
			-- the second 12 elements are leap-year values
		once
			Result := << 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
			             31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
							 >>
		end

end -- class AEL_DATE_TIME_CONSTANTS

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 16-Oct-2013
--|     Added inherit of AEL_DATE_TIME_TYPES
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class is inherited by other classes in the Amalasoft Time cluster
--|----------------------------------------------------------------------
