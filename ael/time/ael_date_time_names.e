--|--------------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Name constants supporting calendars and dates
--|----------------------------------------------------------------------

class AEL_DATE_TIME_NAMES

create
	default_create

feature -- Calendar names

	month_long_names: ARRAY [STRING]
		once
			Result := << "January",   "February", "March",    "April",
			             "May",       "June",     "July",     "August",
							 "September", "October",  "November", "December"
							 >>
		end

	month_long_names_upper: ARRAY [STRING]
		once
			Result := << "JANUARY",   "FEBRUARY", "MARCH",    "APRIL",
			             "MAY",       "JUNE",     "JULY",     "AUGUST",
							 "SEPTEMBER", "OCTOBER",  "NOVEMBER", "DECEMBER"
							 >>
		end

	month_short_names: ARRAY [STRING]
		once
			Result := << "Jan", "Feb", "Mar", "Apr",
			             "May", "Jun", "Jul", "Aug",
							 "Sep", "Oct", "Nov", "Dec"
							 >>
		end

	month_short_names_upper: ARRAY [STRING]
		once
			Result := << "JAN", "FEB", "MAR", "APR",
			             "MAY", "JUN", "JUL", "AUG",
							 "SEP", "OCT", "NOV", "DEC"
							 >>
		end

	day_long_names: ARRAY [STRING]
			-- Full-word English names of days, where 1 is Sunday,
			-- 2 is Monday, etc
		once
			Result := << "Sunday",   "Monday", "Tuesday", "Wednesday",
			             "Thursday", "Friday", "Saturday"
							 >>
		end

	zero_7_day_long_names: ARRAY [STRING]
			-- Full-word English names of days, where 0 and 7 are Sunday
			-- And Monday is 1
		local
			ts: STRING
		once
			ts := "Sunday"
			create Result.make_filled (ts, 0, 7)
			Result.put ("Sunday", 0)
			Result.put ("Monday", 1)
			Result.put ("Tuesday", 2)
			Result.put ("Wednesday", 3)
			Result.put ("Thursday", 4)
			Result.put ("Friday", 5)
			Result.put ("Saturday", 6)
			Result.put ("Sunday", 7)
		end

	day_short_names: ARRAY [STRING]
			-- Abbreviated English names of days, where 1 is Sunday,
			-- 2 is Monday, etc
		once
			Result := << "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" >>
		end

	zero_7_day_short_names: ARRAY [STRING]
			-- Abbreviated English names of days, where 0 and 7 are Sunday
			-- And Monday is 1
			-- Zero-based to interface with C struct_tm and other 
			-- externals
		local
			ts: STRING
		once
			ts := "Sun"
			create Result.make_filled (ts, 0, 7)
			Result.put ("Sun", 0)
			Result.put ("Mon", 1)
			Result.put ("Tue", 2)
			Result.put ("Wed", 3)
			Result.put ("Thu", 4)
			Result.put ("Fri", 5)
			Result.put ("Sat", 6)
			Result.put ("Sun", 7)
		end

	ordinal_days: ARRAY [STRING]
		once
			Result := << "1st", "2nd", "3rd", "4th", "5th", "6th", "7th",
			             "8th", "9th", "10th", "11th", "12th", "13th", "14th",
							 "15th", "16th", "17th", "18th", "19th", "20th", "21st",
							 "22nd", "23rd", "24th", "25th", "26th", "27th", "28th",
							 "29th", "30th", "31st"
							 >>
		ensure
			exists: Result /= Void
		end

	day_strings: ARRAY [STRING]
		once
			Result := << " 1", " 2", " 3", " 4", " 5", " 6", " 7",
			             " 8", " 9", "10", "11", "12", "13", "14",
							 "15", "16", "17", "18", "19", "20", "21",
							 "22", "23", "24", "25", "26", "27", "28",
							 "29", "30", "31"
							 >>
		end

	Ks_time_unit_nanosecond: STRING = "nanosecond"
	Ks_time_unit_microsecond: STRING = "microsecond"
	Ks_time_unit_millisecond: STRING = "millisecond"
	Ks_time_unit_second: STRING = "second"
	Ks_time_unit_minute: STRING = "minute"
	Ks_time_unit_hour: STRING = "hour"
	Ks_time_unit_day: STRING = "day"
	Ks_time_unit_week: STRING = "week"
	Ks_time_unit_month: STRING = "month"
	Ks_time_unit_year: STRING = "year"

	Ks_time_am: STRING = "AM"
	Ks_time_pm: STRING = "PM"

end -- class AEL_DATE_TIME_NAMES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.1 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class is inherited by other classes in the Amalasoft Time cluster
--|----------------------------------------------------------------------
