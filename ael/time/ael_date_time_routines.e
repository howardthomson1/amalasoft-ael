--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Routines supporting calendars
--|----------------------------------------------------------------------

class AEL_DATE_TIME_ROUTINES

inherit
	AEL_DATE_TIME_CONSTANTS

create
	default_create

feature -- Conversion and calculation

	num_days_in_month (mo: INTEGER; is_leap: BOOLEAN): INTEGER
			-- The number of days in the given month (jan=1, dec=12)
		local
			idx: INTEGER
		do
			idx := mo
			if is_leap then
				idx := idx + K_months_per_year
			end
			Result := days_in_month_table.item (idx)
		end

	year_is_leap (v: INTEGER): BOOLEAN
			-- Is the given year (AD) a leap year?
			-- If the year is divisible by 4, then it is,
			-- UNLESS it is a century and it is NOT divisible by 400
		do
			if v <= 1752 then
				-- Before the Gregorian reformation of 1752,
				-- leap years were simply every fourth year

				Result := (v \\ 4) = 0
			else
				Result := ((v \\ 4) = 0)
					and (((v \\ 100) /= 0) or ((v \\ 400) = 0))
			end
		end

	centuries_since_1700 (v: INTEGER): INTEGER
			-- Number of centuries since 1700 (exclusive)
			-- If 1700 or earlier, result is 0
		do
			if v > 1700 then
				Result := v // 100 - 17
			end
		end

	leap_centuries_since_1700 (v: INTEGER): INTEGER
			-- Number of centuries since 1700 whose modulo of 400 is 0
			-- If year is before 1700, result is 0
		do
			if v > 1600 then
				Result := (v - 1600) // 400
			end
		end

	leap_years_since_year_1 (v: INTEGER): INTEGER
			-- Number of leap years between year 1 and the given year
			-- (exclusive)
		do
			Result := ((v // 4) - centuries_since_1700 (v)) + leap_centuries_since_1700 (v)
		end

	day_in_year (day, mo, yr: INTEGER): INTEGER
			-- The 1 based day number within the year
		local
			idx: INTEGER
			is_leap: BOOLEAN
		do
			Result := day
			is_leap := year_is_leap (yr)
			from idx := 1
			until idx >= mo
			loop
				Result := Result + num_days_in_month (idx, is_leap)
				idx := idx + 1
			end
		end

	day_in_week (day, mo, yr: INTEGER): INTEGER
			-- The 1-based day number for any date from 1 Jan. 1 to
			-- 31 Dec. 9999.  Assumes the Gregorian reformation eliminates
			-- 3 Sep. 1752 through 13 Sep. 1752.  Returns Thursday for all
			-- missing days.
		local
			temp: INTEGER
		do
			temp := ((yr - 1) * 365) + leap_years_since_year_1 (yr - 1) + day_in_year (day, mo, yr)

			if temp < K_first_missing_day then
				Result := ((temp - 1 + K_saturday) \\ 7) + 1
			elseif (temp >= (K_first_missing_day + K_number_missing_days)) then
				Result := (((temp - 1 + K_saturday) - K_number_missing_days) \\ 7) + 1
			else
				Result := K_thursday + 1
			end
		end

	--|--------------------------------------------------------------

	week_in_month (v: DATE): INTEGER
			-- The week number (from 1) in the month for the give date
			-- e.g.  If month starts on a Saturday, and date 'v' is the
			-- 4th, then Result is 2 (second week of month)
		require
			date_exists: v /= Void
		local
			d1, dd, dno, m, y, dm, dq: INTEGER
		do
			dno := v.day
			if dno = 1 then
				Result := 1
			else
				m := v.month
				y := v.year
				-- Day of week of the first day of the month
				d1 := day_in_week (1, m, y)
				dd := d1 - 1
				dq := (dno + dd) // 7
				dm := (dno + dd) \\ 7
				if dm /= 0 or (dm = 0 and dq = 0) then
					dq := dq + 1
				end
				Result := dq
			end
		end

	--|--------------------------------------------------------------

	ordinal_day (v: INTEGER): STRING
			-- The ordinal representation (1st, 2nd, etc) of the given day of
			-- month
		require
			valid_day_of_month: v > 0 and v <= 31
		do
			Result := ordinal_days.item (v)
		ensure
			exists: Result /= Void
		end

	ascii_day (v: INTEGER; is_jul: BOOLEAN): STRING
		local
			val, day: INTEGER
			display: BOOLEAN
		do
			day := v
			if day = K_space or day > 31 then
				if is_jul then
					Result := "   "
				else
					Result := "  "
				end
			elseif is_jul then
				val := day // 100
				if val /= 0 then
					day := day \\ 100
					Result := val.out
					display := True
				else
					Result := " "
					display := False
				end
				val := day // 10
				if (val /= 0) or display then
					Result := val.out
				else
					Result := " "
					Result.append ((day \\ 10).out)
				end
			else
				Result := ""
				Result.append (day_strings.item (day))
			end
			Result.append (" ")
		end

	month_name_to_integer (mon: STRING; sf: BOOLEAN): INTEGER
		local
			names: like month_long_names_upper
			i: INTEGER
			umon, ts: STRING
		do
			if sf then
				names := month_short_names_upper
			else
				names := month_long_names_upper
			end
			umon := mon.twin
			umon.to_upper
			from i := names.lower
			until i > names.upper or Result > 0
			loop
				ts := names.item (i)
				if ts.is_equal (umon) then
					Result := i
				else
					i := i + 1
				end
			end
		end

	from_24_hour_to_12 (str: STRING): TUPLE [ INTEGER, STRING ]
		require
			string_exists: str /= Void
		local
			v: INTEGER
		do
			create Result
			if not str.is_integer then
				Result.put (0, 1)
				Result.put ("", 2)
			else
				v := str.to_integer
				Result.put (v, 1)

				if v = 0 then
					Result.put (12, 1)
					Result.put (Ks_time_am, 2)
				end

				if v = 12 then
					Result.put (Ks_time_pm, 2)
				elseif v > 12 then
					Result.put (v - 12, 1)
					Result.put (Ks_time_pm, 2)
				else
					Result.put (Ks_time_am, 2)
				end
			end
		end

	tod_has_occurred (h, m, s: INTEGER): BOOLEAN
			-- Has the given Hour, Minute and Second occurred today?
		local
--			now: AEL_DATE_TIME
			now: DATE_TIME
		do
			create now.make_now
			if now.hour > h then
				Result := True
			else
				if now.hour = h then
					if now.minute > m then
						Result := True
					elseif now.minute = m then
						Result := now.second > s
					end
				end
			end
		end

	is_valid_hour_value (ts: STRING): BOOLEAN
		local
			v: INTEGER
		do
			if ts.is_integer then
				v := ts.to_integer
				Result := v >= 0 and v <= 23
			end
		end

	is_valid_minute_value (ts: STRING): BOOLEAN
		local
			v: INTEGER
		do
			if ts.is_integer then
				v := ts.to_integer
				Result := v >= 0 and v <= 59
			end
		end

	is_valid_second_value (ts: STRING): BOOLEAN
		local
			v: INTEGER
		do
			if ts.is_integer then
				v := ts.to_integer
				Result := v >= 0 and v <= 59
			end
		end

	is_valid_week_number (wn: INTEGER): BOOLEAN
		do
			Result := wn >= 0 and wn <= K_max_weeks_per_month
		end

	is_valid_time_unit (v: INTEGER): BOOLEAN
		do
			inspect v
			when
				K_time_unit_nanosecond, K_time_unit_microsecond,
				K_time_unit_millisecond, K_time_unit_second,
				K_time_unit_minute, K_time_unit_hour,
				K_time_unit_day, K_time_unit_week,
				K_time_unit_month, K_time_unit_year
			 then
				 Result := True
			else
			end
		end

	time_unit_to_string (v: INTEGER; case: CHARACTER): STRING
			-- String represtation of given (valid) time unit.
			--
			-- If the case arg is 'u' then the string is all upper case,
			-- elseif case arg is 'c' then the string's first character is
			-- upper case (capitalized), else the string is all lower case
		require
			valid_time_unit: is_valid_time_unit (v)
		do
			inspect v
			when K_time_unit_nanosecond then
				Result := Ks_time_unit_nanosecond
			when K_time_unit_microsecond then
				Result := Ks_time_unit_microsecond
			when K_time_unit_millisecond then
				Result := Ks_time_unit_millisecond
			when K_time_unit_second then
				Result := Ks_time_unit_second
			when K_time_unit_minute then
				Result := Ks_time_unit_minute
			when K_time_unit_hour then
				Result := Ks_time_unit_hour
			when K_time_unit_day then
				Result := Ks_time_unit_day
			when K_time_unit_week then
				Result := Ks_time_unit_week
			when K_time_unit_month then
				Result := Ks_time_unit_month
			when K_time_unit_year then
				Result := Ks_time_unit_year
				-- no else case, handled by precondition
			end

			inspect case
			when 'u' then
				Result.to_upper
			when 'c' then
				Result.put (Result.item (1).upper, 1)
			else
				-- default is lower case
			end
		ensure
			exists: Result /= Void
		end

	seconds_per_time_unit (v: INTEGER): INTEGER
		require
			valid_time_unit: is_valid_time_unit (v)
		do
			inspect v
			when
				K_time_unit_nanosecond,
				K_time_unit_microsecond,
				K_time_unit_millisecond
			 then
				 Result := 0
			when K_time_unit_second then
				Result := 1
			when K_time_unit_minute then
				Result := K_seconds_per_minute
			when K_time_unit_hour then
				Result := K_seconds_per_hour
			when K_time_unit_day then
				Result := K_seconds_per_day
			when K_time_unit_week then
				Result := K_days_per_week * K_seconds_per_day
			when K_time_unit_month then
				-- Use the MINIMUM value of 28 days
				Result := K_min_days_per_month * K_seconds_per_day
			when K_time_unit_year then
				Result := K_min_days_per_year * K_seconds_per_day
				-- no else case, handled by precondition
			end
		end

	range_for_time_unit (tu, ti: INTEGER): INTEGER_INTERVAL
			-- The valid range of values for the given time unit.
			-- The time instance arg (ti) supports variable-range time units
			-- such as months.
		require
			valid_time_unit: is_valid_time_unit (tu)
		do
			inspect tu
			when K_time_unit_nanosecond then
				Result := K_range_tu_nanosecond
			when K_time_unit_microsecond then
				Result := K_range_tu_microsecond
			when K_time_unit_millisecond then
				Result := K_range_tu_millisecond
			when K_time_unit_second then
				Result := K_range_tu_second
			when K_time_unit_minute then
				Result := K_range_tu_minute
			when K_time_unit_hour then
				Result := K_range_tu_hour
			when K_time_unit_week then
				Result := K_range_tu_week
			when K_time_unit_month then
				Result := K_range_tu_month
			when K_time_unit_year then
				Result := K_range_tu_year
			when K_time_unit_day then
				-- varies per month; ignore leap-year febs for now at least
				Result := day_range_for_month (ti, False)
			else
				Result := K_range_tu_nanosecond
			end
		ensure
			exists: Result /= Void
		end

	day_range_for_month (m: INTEGER; lf: BOOLEAN): INTEGER_INTERVAL
		local
			nd: INTEGER
		do
			nd := num_days_in_month (m, lf)
			create Result.make (1, nd)
		end

--|========================================================================
feature -- Local/GMT conversion
--|========================================================================

	hours_behind_gmt: INTEGER
			-- The number of hours that local time is behind GMT
		local
			lt, gt: TIME
			td: TIME_DURATION
		do
			create lt.make_now
			create gt.make_now_utc
			td := gt.relative_duration (lt)
			Result := td.hour
		end

	--|--------------------------------------------------------------

	utc_to_string (v: like Kta_time_t): STRING
			-- "Human" representation of given UTC date/time value
		local
			dt: AEL_UNIX_DATE_TIME
		do
			create dt.make_from_timet_utc (v)
			Result := dt.out
		ensure
			exists: Result /= Void
		end

end -- class AEL_DATE_TIME_ROUTINES

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 16-Oct-2013
--|     Changed type of utc from natural to Kta_time_t (int 32)
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class can be instantiated or inherited as needed
--|----------------------------------------------------------------------

