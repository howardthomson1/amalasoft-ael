--|----------------------------------------------------------------------
--| Copyright (c) 1995-2013, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Constants supporting calendars and date calculations
--|----------------------------------------------------------------------

class AEL_DATE_TIME_TYPES

--|========================================================================
feature {NONE} -- Type anchors
--|========================================================================

	Kta_time_t: INTEGER_32
			-- Type anchor for UNIX time_t

end -- class AEL_DATE_TIME_CONSTANTS

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 16-Oct-2013
--|     Original. Compiled and tested using Eiffel 7.2
--|----------------------------------------------------------------------
--| How-to
--| This class is inherited by other classes in the Amalasoft Time cluster
--|----------------------------------------------------------------------
