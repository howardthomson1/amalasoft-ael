--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Representation of a month w/r/t a calendar
--|----------------------------------------------------------------------

class AEL_DT_CALENDAR_MONTH

inherit
	ANY
		redefine
			out
		end

create
	make

feature {NONE} -- Creation

	make (m: INTEGER; y: AEL_DT_CALENDAR_YEAR)
		require
			valid_month: m > 0 and then m <= 12
			year_exists: y /= Void
		do
			month_number := m
			year := y
			set_first_day_position_in_week
			set_number_of_days
			private_day_labels := julian_day_labels
		end

feature -- Status

	name: STRING
		do
			Result := cal_const.month_long_names.item (month_number)
		end

	month_number: INTEGER
	year: AEL_DT_CALENDAR_YEAR

	is_julian: BOOLEAN
		do
			Result := year.is_julian
		end

	first_day_position_in_week: INTEGER
		-- On which day of the week does the
		-- first day of the month fall?

	first_day_in_week (v: INTEGER): INTEGER
		-- For a given week in this month, what is the 1-based day
		-- that begins the week?  If the week does not begin with
		-- a day from this month, then the result is negative.
		require
			valid_week: is_valid_week_number (v)
		local
			fd: INTEGER
		do
			fd := first_day_position_in_week
			if v = 1 then
				if fd = 1 then
					Result := 1
				else
					Result := -1
				end
			elseif v > number_of_weeks then
				Result := -1
			else
				fd := 9 - fd -- second week, first day
				Result := (7 * (v - 2)) + fd
			end
		end

	first_day_of_week: INTEGER
		-- Which day of week is first? Sunday or Monday?
		-- Is 1 if Sunday, 2 if Monday
		do
			Result := private_first_day_of_week + 1
		end

	number_of_days: INTEGER

	number_of_weeks: INTEGER
		local
			pos: INTEGER
			dt: INTEGER
			tw: INTEGER
		do
			pos := first_day_position_in_week
			dt := number_of_days
			tw := dt - (7 - pos)
			Result := ((tw / 7) + 1).ceiling
		ensure
			result_valid: Result >= 4 and Result <= 6
		end

feature -- String representation

	out: STRING
		local
			idx, midx: INTEGER
			lim: INTEGER
			ij: BOOLEAN
			tstr: STRING
			fd, tl: INTEGER
			pf: AEL_PRINTF
			fmt: STRING
		do
			lim := number_of_days
			ij := is_julian

			create Result.make (256)

			if ij then
				tl := 28
			else
				tl := 21
			end
			create tstr.make (tl)
			tstr.append (cal_const.month_long_names.item (month_number))
			tstr.append (" ")
			tstr.append_integer (year.year_number)
			create pf
			fmt := "%%="
			fmt.append_integer (tl)
			fmt.extend ('s')
			Result.append (pf.aprintf (fmt, << " " >>))
			Result.prune_all_trailing (' ')
			Result.extend ('%N')
			Result.append (day_labels)
			Result.prune_all_trailing (' ')
			Result.extend ('%N')

			fd := first_day_position_in_week
			from idx := 1
			until idx >= fd
			loop
				Result.append (spacer_string)
				idx := idx + 1
			end

			from midx := 1
			until midx > lim
			loop
				if idx > 7 then
					Result.prune_all_trailing (' ')
					Result.extend ('%N')
					idx := 1
				end
				Result.append (cal_routines.ascii_day (midx, ij))
				midx := midx + 1
				idx := idx + 1
			end
			Result.prune_all_trailing (' ')
		end

	week_out (wn: INTEGER): STRING
		require
			valid_week: is_valid_week_number (wn)
		local
			idx, midx: INTEGER
			lim: INTEGER
			ij: BOOLEAN
			fd, tl: INTEGER
		do
			lim := number_of_days
			ij := is_julian

			create Result.make (256)

			if wn = 0 then
				Result.append (day_labels)
			elseif wn = 1 then
				fd := first_day_position_in_week
				from idx := 1
				until idx >= fd
				loop
					Result.append (spacer_string)
					idx := idx + 1
				end
				from midx := 1
				until idx > 7
				loop
					Result.append (cal_routines.ascii_day (midx, ij))
					idx := idx + 1
					midx := midx + 1
				end
			else
				-- Output the given week
				fd := first_day_in_week (wn)
				from idx := 0
				until idx = 7
				loop
					if fd < 0 then
						tl := cal_const.K_space
					else
						tl := fd + idx
					end
					if tl > lim then
						tl := cal_const.K_space
					end
					Result.append (cal_routines.ascii_day (tl, is_julian))
					idx := idx + 1
				end
			end
		end

	day_labels: STRING
		do
			if day_labels_defined then
				Result := private_day_labels
			else
				if is_julian then
					Result := julian_day_labels
				else
					Result := nonjulian_day_labels
				end
			end
		end

	spacer_string: STRING
		-- Spaces equivalent of 2 digits and a blank
		do
			if is_julian then
				Result := "    "
			else
				Result := "   "
			end
		end

	is_valid_week_number (wn: INTEGER): BOOLEAN
		do
			Result := cal_routines.is_valid_week_number (wn)
		end

feature -- Status setting

	set_first_day_position_in_week
		do
			first_day_position_in_week := cal_routines.day_in_week (1, month_number, year.year_number)
		end

	set_monday_first
		do
			private_first_day_of_week := 1
		end

	set_sunday_first
		do
			private_first_day_of_week := 0
		end

	set_number_of_days
		do
			number_of_days := cal_routines.num_days_in_month (month_number, year.is_leap_year)
		end

	set_day_labels (v: like day_labels)
		do
			private_day_labels := v
			day_labels_defined := True
		end

feature {NONE} -- Implementation and constants

	day_labels_defined: BOOLEAN

	private_day_labels: like day_labels
	private_first_day_of_week: INTEGER

	julian_day_labels: like day_labels = "  S   M  Tu   W  Th   F   S "
	nonjulian_day_labels: like day_labels = " S  M Tu  W Th  F  S "

	cal_const: AEL_DATE_TIME_CONSTANTS
		once
			create Result
		end

	cal_routines: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

end -- class AEL_DT_CALENDAR_MONTH

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class can be instantiated using the make routine, and 
--| providing a 1-based month number, and the year in which the actual
--| month would reside as an instance of AEL_DT_CALENDAR_YEAR
--|----------------------------------------------------------------------
