--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Representation of a week w/r/t a calendar
--|----------------------------------------------------------------------

class AEL_DT_CALENDAR_WEEK

inherit
	ARRAY [INTEGER]
		rename
			make as ar_make
		redefine
			make_filled, make_empty
		end

create
	make

 --|========================================================================
feature {NONE} -- Creation and initialization
 --|========================================================================

	make (wn: INTEGER; m: like month)
		require
			week_number_valid: is_valid_week_number (wn)
			month_exists: m /= Void
		do
			make_filled (0, 1, 7)
			week_number := wn
			month := m
			init_days
		end

	--|--------------------------------------------------------------

	make_empty
			-- Allocate empty array starting at `1'.
		do
			default_initialize
			Precursor
		end

	--|--------------------------------------------------------------

	make_filled (v: INTEGER; min_index, max_index: INTEGER)
			-- Allocate array; set index interval to
			-- `min_index' .. `max_index'; set all values to default.
			-- (Make array empty if `min_index' = `max_index' + 1).
		do
			default_initialize
			Precursor (v, min_index, max_index)
		end

	--|--------------------------------------------------------------

	default_initialize
		do
			create days.make_filled ("", 1, 7)
			create month.make (1, create {AEL_DT_CALENDAR_YEAR}.make (1970, False))
		end

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	init_days
		local
			fd, pos, idx, md: INTEGER
		do
			if week_number = 1 then
				fd := 1
				pos := month.first_day_position_in_week
			else
				pos := 1
				fd := month.first_day_in_week (week_number)
			end
			md := month.number_of_days

			-- Days were initialized on create to be "" in each place
			-- Current was initialized on create to be 0 in each place
			-- No need to init any days other than the ones in range
			from idx := pos
			until idx > 7 or fd > md
			loop
				put (fd, idx)
				days.put (fd.out, idx)
				idx := idx + 1
				fd := fd + 1
			end
		end

 --|========================================================================
feature -- Status
 --|========================================================================

	days: ARRAY [STRING]

	week_number: INTEGER
	month: AEL_DT_CALENDAR_MONTH

	is_valid_week_number (wn: INTEGER): BOOLEAN
		do
			Result := cal_routines.is_valid_week_number (wn)
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	cal_const: AEL_DATE_TIME_CONSTANTS
		once
			create Result
		end

	cal_routines: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

end -- class AEL_DT_CALENDAR_WEEK

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class can be instantiated using the make routine, and 
--| providing a 1-based week number, and the month in which the actual
--| month would reside as an instance of AEL_DT_CALENDAR_MONTH
--|----------------------------------------------------------------------
