--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Representation of a year w/r/t a calendar
--|----------------------------------------------------------------------

class AEL_DT_CALENDAR_YEAR

inherit
	ARRAY [AEL_DT_CALENDAR_MONTH]
		rename
			make as array_make,
			out as array_out
		end

create
	make_empty, make, make_initialized

 --|========================================================================
feature {NONE} -- Creation and initialization
 --|========================================================================

	make (v: INTEGER; ij: BOOLEAN)
		require
			valid_year: v > 0 and then v <= 9999
		do
			make_filled (K_month_0, 1, 12)
			year_number := v
			is_julian := ij
			is_leap_year := cal_routines.year_is_leap (year_number)
		end

	make_initialized (v: INTEGER; ij: BOOLEAN)
		do
			make (v, ij)
			make_months
		end

	make_months
		local
			mo: AEL_DT_CALENDAR_MONTH
			idx: INTEGER
		do
			from idx := 1
			until idx > 12
			loop
				create mo.make (idx, Current)
				put (mo, idx)
				idx := idx + 1
			end
		end

feature -- Status

	year_number: INTEGER

	is_leap_year: BOOLEAN
			-- Is this year a leap year?

	is_julian: BOOLEAN
			-- Is this to be interpreted as a Julian date?

feature -- String representation

	out: STRING
		local
			idx: INTEGER
			col: INTEGER
			tl: INTEGER
			row: INTEGER
			mn: INTEGER
		do
			create Result.make (2048)

			if is_julian then
				tl := 28
			else
				tl := 21
			end

			Result.append ("%N")
			Result.append (title_leader)
			Result.append_integer (year_number)
			Result.append ("%N%N")

			from idx := 1
			until idx > 4 -- 12 // 3
			loop
				Result.append (month_labels.item (idx))
				Result.extend ('%N')

				from row := 0
				until row > 6
				loop
					from col := 1
					until col > 3
					loop
						mn := ( (idx - 1) * 3) + col
						Result.append (item (mn).week_out (row))
						col := col + 1
						Result.append (Ks_gutter)
					end
					Result.prune_all_trailing (' ')
					Result.extend ('%N')
					row := row + 1
				end
				idx := idx + 1
			end
			Result.prune_all_trailing (' ')
			Result.extend ('%N')
		end

feature {NONE} -- Rendition support

	month_labels: ARRAY [STRING]
		once
			Result := <<"	 Jan			Feb		       Mar",
			"	 Apr			May		       Jun",
			"	 Jul			Aug		       Sep",
			"	 Oct			Nov		       Dec"
			>>
		end

	title_leader: STRING
		do
			if is_julian then
				Result := "					  "
			else
				Result := "				"
			end
		end

feature {NONE} -- Constants

	Ks_gutter: STRING = "  "

	cal_const: AEL_DATE_TIME_CONSTANTS
		once
			create Result
		end

	cal_routines: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

	K_month_0: AEL_DT_CALENDAR_MONTH
			-- Dummy month for year 0; used for initialization ony
		once
			create Result.make (
				1,
				create {AEL_DT_CALENDAR_YEAR}.make_empty)
		end

end -- class AEL_DT_CALENDAR_YEAR

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| This class can be instantiated using the make routine, and 
--| providing a year value (e.g. 2009), and a BOOLEAN denoting that 
--| the year is intended bo Julian or not (False denotes Gregorian)
--|----------------------------------------------------------------------
