--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An identifiable fine-grained interval of time
--|----------------------------------------------------------------------

class AEL_TIME_INTERVAL

inherit
	AEL_TIME_INTERVAL_CLIENT
		redefine
			out
		end

create
	make

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make (lbl: STRING; mark_on_make: BOOLEAN)
		do
			id := next_instance
			label := id.out
			label.extend (':')
			if lbl /= Void then
				label.append (lbl)
			end
			if mark_on_make then
				mark_begin
			end
		end

--|========================================================================
feature {NONE} -- Private status setting
--|========================================================================

	mark_begin
		do
			if intervals_enabled then
				create starting_tval.make_now
--RFO				create starting_time.make_now
			end
		end

	--|--------------------------------------------------------------

	mark_end
		require
			started: intervals_enabled implies starting_tval /= Void
		local
			etv: like ending_tval
		do
			if intervals_enabled and then attached starting_tval as stv then
				create etv.make_now
				ending_tval := etv
				elapsed_tval := etv.difference (stv)
			end
		ensure
			marked: intervals_enabled and then
				attached starting_tval as sv implies
					attached ending_tval and attached elapsed_tval
		end

--|========================================================================
feature -- Control
--|========================================================================

	start
		do
			mark_begin
		end

	--|--------------------------------------------------------------

	stop
		do
			if intervals_enabled then
				mark_end
			end
		end

	--|--------------------------------------------------------------

	stop_and_print (pl: INTEGER)
		do
			if intervals_enabled then
				stop
				if interval_reporting_threshold = 0 or else
					(fine_duration > interval_reporting_threshold)
				 then
					 print (formatted_out (pl))
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_label (v: STRING)
		do
			label := id.out
			label.extend (':')
			if v /= Void then
				label.append (v)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	out: STRING
		do
			Result := apf.aprintf ("%%d:%%3.6f", << id, fine_duration >>)
		end

	--|--------------------------------------------------------------

	out_verbose: STRING
		do
			create Result.make (48)
			if attached starting_tval as stv
				and then attached ending_tval as etv
			 then
				 Result.append (
					 apf.aprintf (
						 "%%-24s From:%%3.6f To:%%3.6f Net:%%3.6f",
						 << label, stv.to_double, etv.to_double, fine_duration >>)
									)
			end
		end

	--|--------------------------------------------------------------

	formatted_out (pl: INTEGER): STRING
			-- Formatted output of Current
		do
			if not intervals_enabled then
				Result := ""
			elseif interval_reporting_threshold = 0 or else
				(fine_duration > interval_reporting_threshold)
			 then
				inspect pl
				when 1 then
					Result := apf.aprintf ("%%03d: %%3.6f ",<<id, fine_duration>>)
				when 3 then
					Result := out_verbose
				else
					Result := apf.aprintf (
						"%%-16s: %%3.6f%N", <<label, fine_duration>>)
				end
			else
				Result := ""
			end
		ensure
			exists: attached Result
		end

	--|--------------------------------------------------------------

--RFO	starting_time: TIME
--RFO	ending_time: TIME
--RFO	elapsed_time: TIME_DURATION

	starting_tval: detachable AEL_TIMEVAL
	ending_tval: detachable AEL_TIMEVAL
	elapsed_tval: detachable AEL_TIMEVAL

	--|--------------------------------------------------------------

	fine_duration: DOUBLE
		do
--RFO			if elapsed_time /= Void then
--RFO				Result := elapsed_time.fine_seconds_count
--RFO			end
			if attached elapsed_tval as te then
				Result := te.to_double
			end
		end

	id: INTEGER
	label: STRING

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	apf: AEL_PRINTF
		once
			create Result
		end

end -- class AEL_TIME_INTERVAL

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 18-Apr-2009
--|     Original module in this context; compiled under ec 6.5
--|     Adapted from much older existing module
--|----------------------------------------------------------------------
--| How-to
--| This class is used by AEL_TIME_INTERVAL_CLIENT, and instantiation
--| and control of this class is driven by routines there.
--| Instantiation is via the new_interval factory routine.
--|
--| This class depends on the Eiffel Base libraries, the TIME library
--| from Eiffel Software, and the AEL_PRINTF library from Amalasoft.
--|----------------------------------------------------------------------
