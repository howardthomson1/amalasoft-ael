--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An object capable of creating and maniulatuing fine-grained 
--| intervals of type AEL_TIME_INTERVAL
--|----------------------------------------------------------------------

class AEL_TIME_INTERVAL_CLIENT

--|========================================================================
feature -- Factory
--|========================================================================

	new_interval (lbl: STRING; sf: BOOLEAN): AEL_TIME_INTERVAL
		do
			create Result.make (lbl, sf)
		end

 --|========================================================================
feature -- Control
 --|========================================================================

	start_interval (v: AEL_TIME_INTERVAL)
		require
			interval_exists: v /= Void
		do
			enable_intervals
			v.start
		end

	--|------------------------------------------------------------------------

	stop_interval (v: AEL_TIME_INTERVAL)
		require
			interval_exists: v /= Void
		do
			v.stop
		end

--|========================================================================
feature -- Status
--|========================================================================

	next_instance: INTEGER
		do
			Result := instance
			instance_ref.put (Result + 1)
		end

	--|------------------------------------------------------------------------

	instance: INTEGER
		do
			Result := instance_ref.item
		end

	--|------------------------------------------------------------------------

	intervals_enabled: BOOLEAN
		do
			Result := intervals_enabled_ref.item
		end

	--|------------------------------------------------------------------------

	interval_reporting_threshold: DOUBLE
			-- Number of fine time units below which reporting does not occur
		do
			Result := interval_reporting_threshold_ref.item
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	disable_intervals
		do
			intervals_enabled_ref.put (False)
		end

	enable_intervals
		do
			intervals_enabled_ref.put (True)
		end

	set_interval_reporting_threshold (v: DOUBLE)
			-- Set the duration below which reporting is ignored
		do
			interval_reporting_threshold_ref.put (v)
		end

--|========================================================================
feature {NONE} -- Shared flags and indices
--|========================================================================

	instance_ref: CELL [INTEGER]
		once
			create Result.put (0)
		end

	intervals_enabled_ref: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

	interval_reporting_threshold_ref: CELL [DOUBLE]
			-- Number of fine time units below which reporting does not occur
		once
			create Result.put (0)
		end

end -- class AEL_TIME_INTERVAL_CLIENT

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 18-Apr-2009
--|     Original module in this context; compiled under ec 6.5
--|     Adapted from much older existing module
--|----------------------------------------------------------------------
--| How-to
--| Inherit this class into another class in which you wish to measure
--| time intervals closely, for example for routine invocations.
--| Create a new instance of AEL_TIME_INTERVAL by calling the
--| new_interval routine.
--| To begin the interval, call start_interval with an interval 
--| object.
--| To stop the interval, call stop_interval.
--| All intervals can be disabled using disable_intervals, and enabled
--| by calling enabled_intervals.
--| Intervals are not reported until a reporting threshold is reached.
--| The threshold is settable.
--| This class is used by AEL_TIME_INTERVAL_CLIENT, and instantiation
--| and control of this class is driven by routines there.
--|
--| This class depends on the Eiffel Base libraries, the TIME library
--| from Eiffel Software, and the AEL_PRINTF library from Amalasoft.
--|----------------------------------------------------------------------
