--|----------------------------------------------------------------------
--| Copyright (c) 2018, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A timeval-like pair of seconds and microseconds since epoch
--|----------------------------------------------------------------------

class AEL_TIME_MARKER

inherit
	COMPARABLE
		redefine
			out, default_create
		end

create
	make, make_utc

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make (lb: STRING)
			-- Create Current as a marker for the present local time, with 
			-- label 'lb'
		do
			label := lb.twin
			default_create
		end

	make_utc (lb: STRING)
			-- Create Current as a marker for the present UTC time, with 
			-- label 'lb'
		do
			is_utc := True
			make (lb)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create current in its default state
		do
			if is_utc then
				create timeval.make_now
			else
				create timeval.make_now
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	label: STRING
			-- Label identifying Current
 
	timeval: AEL_TIMEVAL
			-- Implementation of timeval used by Current

	is_utc: BOOLEAN
			-- Is Current based on UTC time?

	--|--------------------------------------------------------------

	seconds: INTEGER
			-- Whole seconds since epoch
		do
			Result := timeval.seconds
		end

	milliseconds: INTEGER
			-- Additional milliseconds since epoch, in addition to seconds
		do
			Result := timeval.milliseconds
		end

	total_milliseconds: INTEGER_64
			-- Total milliseconds (INCLUDING all seconds) since epoch
		do
			Result := timeval.total_milliseconds
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String reprentation of Current
		do
			create Result.make (16 + label.count)
			Result.append (label)
			Result.append (": {")
			Result.append (timeval_out)
			Result.extend ('}')
		end

	timeval_out: STRING
			-- String reprentation of timeval associated with Current
		do
			Result := timeval.out
		end

	out_with_difference (other: detachable like Current): STRING
			-- String representation of Current, including difference, 
			-- in time, between 'other' and Current, where 'other' is 
			-- assumed to be an earlier time
		do
			create Result.make (32 + label.count)
			Result.append (out)
			if attached other as tm then
				Result.append (" [" + difference(tm).out + "]")
			end
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := timeval.is_less (other.timeval)
		end

	--|--------------------------------------------------------------

	difference (other: like Current): AEL_TIMEVAL
			-- Number of seconds and milliseconds between Current and
			-- other as in Current - other
		do
			Result := timeval.difference (other.timeval)
		end

	--|--------------------------------------------------------------

	difference_in_milliseconds (other: like Current): INTEGER_64
			-- Number of total milliseconds between Current and
			-- other as in Current - other
		do
			Result := timeval.difference_in_milliseconds (other.timeval)
		end

 --|------------------------------------------------------------------------
invariant

end -- class AEL_TIME_MARKER

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 001 21-Jul-2018
--|     Original module, adapted from and using AEL_TIMEVAL
--|     Compiled and tested using Eiffel 18.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class using 'make', supplying a 
--| label by which to identify Current in a collection of similar
--| (e.g. an instance of AEL_TIME_SEQUENCE)
--| Instances of this class are comparable and computable via the
--| is_less ("<") and difference routines.
--| The timeval_out routine provides a two-field string, with a colon 
--| separator.
--| The out routine prepends the label to the timeval_out resul
--|
--| This class depends on the Eiffel Base libraries, the TIME library
--| from Eiffel Software
--|----------------------------------------------------------------------
