note
	description: "{
An ordered sequence of time markers (absolute times)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2018 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_TIME_SEQUENCE

inherit
	AEL_DS_INDIRECT_LIST [AEL_TIME_MARKER]
		rename
			make as make_with_list
		redefine
			out
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
		do
			make_with_list (create {TWO_WAY_LIST [AEL_TIME_MARKER]}.make)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_utc: BOOLEAN
			-- Are items in Current based on UTC time?
			-- True also if empty
		do
			Result := is_empty or first.is_utc
		ensure
			reflexive: Result implies is_empty or not is_local
		end

	is_local: BOOLEAN
			-- Are items in Current based on local time?
			-- True also if empty
		do
			Result := is_empty or (not first.is_utc)
		ensure
			reflexive: Result implies is_empty or not is_utc
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String representation of contents of Current
		local
			pt: detachable like item
			tt: AEL_TIMEVAL
		do
			if is_empty then
				Result := ""
			else
				create Result.make (256)
				if count = 1 then
					Result.append ("Time Sequence (Only 1 marker):%N")
				else
					Result.append ("Time Sequence (" + count.out + " markers):%N")
				end
				from start
				until exhausted
				loop
					Result.append ("  ")
					Result.append (item.out_with_difference (pt))
					pt := item
					forth
					Result.append ("%N")
				end
				tt := total_elapsed_time
				Result.append (
					"Total:  " + tt.out + " [" +
					tt.total_milliseconds.out + " msecs]")
			end
		end

	--|--------------------------------------------------------------

	total_elapsed_time: AEL_TIMEVAL
			-- Total elapsed time, from the first item
			-- in Current to the last
		do
			if count <= 1 then
				create Result.make_with_total_milliseconds (0)
			else
				Result := last.timeval.difference (first.timeval)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	mark_now (lb: STRING)
			-- Add a new marker to end, that marker being created at the 
			-- present local time, and having label 'lb;
		require
			label_exists: attached lb
			valid_base: is_local
		do
			extend (create {like item}.make (lb))
		end

	mark_now_utc (lb: STRING)
			-- Add a new marker to end, that marker being created at the 
			-- present UTC time, and having label 'lb;
		require
			label_exists: attached lb
			valid_base: is_utc
		do
			extend (create {like item}.make_utc (lb))
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	item_base_consistent (v: like item): BOOLEAN
			-- Does 'v' have a time base consistent with the other items 
			-- in Current?  Time base is either local or UTC
		do
			if is_empty then
				Result := True
			else
				Result := first.is_utc = v.is_utc
			end
		end

	--|--------------------------------------------------------------

	all_items_have_same_base: BOOLEAN
			-- Do all items in Current have the same time base
			-- (local vs utc)?
		local
			uf: BOOLEAN
		do
			Result := True
			if not is_empty then
				uf := first.is_utc
				from start
				until exhausted or not Result
				loop
					Result := item.is_utc = uf
					forth
				end
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 21-Jul-2018
--|     Original module, adapted from and using AEL_TIMEVAL
--|     Compiled and tested using Eiffel 18.1, void-safe
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class using make
--| Add time markers by calling 'extend' with an instance of 
--| AEL_TIME_MARKER or by calling 'mark_now' or 'mark_now_utc'
--| (creating a new time marker at present time and added to end)
--| The out routine displays the contents of Current, in order
--|----------------------------------------------------------------------

end -- class AEL_TIME_SEQUENCE
