note
	description: "{
A shareable time service
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_TIME_SERVICE

inherit
	ANY
		redefine
			default_create
		end

create
	default_create, make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
			-- Create Current and begin operation
		do
			default_create
			start
		ensure
			running: is_running
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default state
		do
			create common_time.make_now
			current_time := [0, 0]
			create termination_flag_cell.put (False)
			update
		ensure then
			in_default_state: not (is_running or has_stopped)
		end

--|========================================================================
feature -- Status
--|========================================================================

	current_time_t: INTEGER_32
			-- Current UTC as time_t value
			-- Value is most recently calculated
		do
			Result := current_seconds
		end

	current_time: TUPLE [INTEGER_32, INTEGER_32]
			-- Current time as timeval-like TUPLE of seconds, milliseconds
			-- Values are most recently calculated

	current_seconds: INTEGER_32
			-- Current seconds since epoch (UTC)
			-- Value is most recently calculated

	current_milliseconds: INTEGER_32
			-- Current additional milliseconds since epoch
			-- Value is most recently calculated

	--|--------------------------------------------------------------

	updating: BOOLEAN
			-- Is current updating time right now?

	is_running: BOOLEAN
			-- Is Current presently running?
		do
			Result := state = K_st_running
		ensure
			not_stopped: Result implies not has_stopped
		end

	has_stopped: BOOLEAN
			-- Has Current started operation?
		do
			Result := state = K_st_stopped
		ensure
			not_running: Result implies not is_running
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	update
			-- Update time values to present
		do
			if not updating then
				updating := True
				common_time.set_to_present
				current_seconds := common_time.seconds
				current_milliseconds := common_time.milliseconds
				current_time := [current_seconds, current_milliseconds]
				updating := False
			end
		end

	--|--------------------------------------------------------------

	start
			-- Begin operation
		require
			not_started: not is_running
		local
			lt: like timer
		do
			if attached timer as tt then
				lt := tt
			else
				create lt.make (termination_flag_cell, agent on_timer_tick)
			end
			lt.set_interval (10)
			timer := lt
			lt.launch
			state := K_st_running
		end

	--|--------------------------------------------------------------

	stop
			-- Stop operation
		require
			started: is_running
		do
			state := K_st_stopped
			termination_flag_cell.put (True)
		end

	set_timer_interval (v: INTEGER)
			-- Set to 'v' the interval, in milliseconds, at which the 
			-- timer should update the current time
		do
			if attached timer as lt then
				lt.set_interval (v)
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	difference_in_millisecs (v1, v2: TUPLE [INTEGER_32, INTEGER_32]): INTEGER_64
			-- Number of total milliseconds between time value pairs 
			-- 'v1' and 'v2
			-- As in v2 - v1
		require
			values_exist: v1 /= Void and v2 /= Void
			msecs1_in_range: is_valid_milliseconds (v1.integer_item (2))
			msecs2_in_range: is_valid_milliseconds (v2.integer_item (2))
		do
			Result := common_time.difference_in_values (v1, v2)
		end

	--|--------------------------------------------------------------

	elapsed_milliseconds (v: TUPLE [INTEGER_32, INTEGER_32]): INTEGER_64
			-- Number of total milliseconds between current time and
			-- time values 'v'
			-- As in current - v
		require
			values_exist: v /= Void
			msecs_in_range: is_valid_milliseconds (v.integer_item (2))
		do
			Result := common_time.difference_in_values (v, current_time)
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_milliseconds (v: INTEGER): BOOLEAN
			-- Is 'v  a valid number of additional milliseconds?
		do
			if v < 0 then
				Result := K_range_milliseconds.has (-v)
			else
				Result := K_range_milliseconds.has (v)
			end
		end

	--|--------------------------------------------------------------

	is_valid_state (v: INTEGER): BOOLEAN
			-- Is 'v' a valid state?
		do
			inspect v
			when K_st_initial, K_st_running, K_st_stopped then
				Result := True
			else
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	common_time: AEL_TIMEVAL
			-- Time, in timeval format, most recently updated by Current

	state: INTEGER
			-- Operating state of Current

	timer: detachable AEL_SPRT_TIMER_THREAD
			-- Actual tickmaster for Current

	termination_flag_cell: CELL [BOOLEAN]
			-- Cell containing timer thread termination flag

	on_timer_tick
			-- Respond to timer tick
		do
			io.put_string ("tick.. ")
			update
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_range_milliseconds: INTEGER_INTERVAL
			-- Range of legit values for milliseconds
		once
			create Result.make (0, 999)
		end

	--|--------------------------------------------------------------

	K_st_initial: INTEGER = 0
	K_st_running: INTEGER = 1
	K_st_stopped: INTEGER = 2

	--|--------------------------------------------------------------
invariant
	valid_state: is_valid_state (state)

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-Oct-2013
--|     Created original module (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_TIME_SERVICE
