note
	description: "{
Client that uses a common time service
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_TIME_SERVICE_CLIENT

--|========================================================================
feature -- Status
--|========================================================================

	time_svc: AEL_TIME_SERVICE
		once
			create Result.make
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	stop_timer
			-- Stop the central timing service
		do
			time_svc.stop
		end

	set_timer_interval (v: INTEGER)
			-- Set to 'v' the interval, in milliseconds, at which the 
			-- timer service should update the current time
		do
			time_svc.set_timer_interval (v)
		end

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-Oct-2013
--|     Created original module (Eiffel 7.2)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class AEL_TIME_SERVICE_CLIENT
