note
	description: "A timestamp string of the form: YYYY-MM-DD hh:mm:ss"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2013/04/30 $"
	revision: "$Revision: 004$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class AEL_TIMESTAMP

inherit
	STRING
		rename
			make as str_make
		redefine
			str_make
		end
	AEL_DATE_TIME_TYPES
		undefine
			is_equal, copy, out
		end

create
	make, make_utc, make_fine_grained, make_fine_utc, make_from_time_t,
	make_slashed

create {STRING}
	str_make

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
			-- Create a new timestamp object and initialize it to the current
			-- local time
		do
			str_make (24)
			set_to_present
		end

	--|--------------------------------------------------------------

	make_slashed
			-- Create a new timestamp object and initialize it to the current
			-- local time
			-- Generate slash-separated date fields
		do
			str_make (24)
			set_to_present
			generate_slashed
		end

--|--------------------------------------------------------------

	make_utc
			-- Create a new timestamp object and initialize it to the current
			-- UTC time (GMT)
		do
			is_utc := True
--			cached_time_t := current_utc
--			make_from_epoch (cached_time_t, True)
			make
		end

--|--------------------------------------------------------------

	make_fine_grained
			-- Create a new fine-grained timestamp object and initialize it to
			-- the current local time
		do
			is_fine_grained := True
			make
		end

--|--------------------------------------------------------------

	make_fine_utc
			-- Create a new fine-grained timestamp object and initialize it to
			-- the current UTC time (GMT)
		do
			is_utc := True
			make_fine_grained
		end

	--|--------------------------------------------------------------

	make_from_time_t (v: like Kta_time_t; uf: BOOLEAN)
			-- Create Current as seconds since UNIX-style epoch
			-- (1 Jan 1970 00:00:00)
		do
			is_utc := uf
			make
			set_from_time_t (v)
		end

	--|--------------------------------------------------------------

	str_make (sz: INTEGER)
		do
			Precursor (sz)
			--| need init here cuz str_make can be called from copies
			--| and such, and for void-safe compliance during creation
			create date.make_now
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_fine_grained: BOOLEAN
			-- Is this timestamp fine-grained (i.e. does it show partial seconds)?

	is_utc: BOOLEAN
			-- Is this timestamp based on UTC?

	date: DATE_TIME
			-- Date-time object currently associated with this timestamp

--RFO 	cached_time_t: like Kta_time_t
			-- Number of seconds since epoch (typedef time_t)
			-- NOT coherent
			-- NOT relevant to fine-grained forms

	current_utc: like Kta_time_t
		do
			Result := (create{AEL_UNIX_DATE}).current_utc
		end

	time_t_seconds_behind_utc: like Kta_time_t
			-- Number of seconds that local time is __BEHIND__ GMT
		do
			Result := (atr.hours_behind_gmt * 3600)
		end

	seconds_behind_utc: INTEGER_32
			-- Number of seconds that local time is __BEHIND__ GMT
		do
			Result := (atr.hours_behind_gmt * 3600)
		end

	--|--------------------------------------------------------------

	to_time_t: like Kta_time_t
			-- Current's datetime in time_t form
			-- Does not consider sub-second values
		local
			ep: DATE_TIME
			et: DATE_TIME_DURATION
		do
			create ep.make_fine (1970, 1, 1, 0, 0, 0.0)
			--et := ep.relative_duration (date)
			et := date.relative_duration (ep)
			Result := et.seconds_count.to_integer_32
		end

	--|--------------------------------------------------------------

	to_seconds_and_parts: DOUBLE
			-- Current's datetime in seconds and partial seconds since
			-- UNIX epoch (1 Jan 1970 00:00:00)
		local
			ep: DATE_TIME
			et: DATE_TIME_DURATION
		do
			create ep.make_fine (1970, 1, 1, 0, 0, 0.0)
			--et := ep.relative_duration (date)
			et := date.relative_duration (ep)
			Result := et.seconds_count
		end

--|========================================================================
feature -- Status Setting
--|========================================================================

	old_set_to_present
			-- Reset this timestamp to the current time (either local of UTC)
		do
			if is_utc then
				create date.make_now_utc
			else
				create date.make_now
			end
			generate
		end

	--|--------------------------------------------------------------

	set_to_present
			-- Reset this timestamp to the current time (either local of UTC)
		do
			-- Non-fine-grained date_time still shows fractional
			-- seconds
			-- Making from time_t removes them.  This is not a good
			-- thing if dates are to be compared as the time_t date
			-- is effectived rounded down
			if is_fine_grained or True then
				if is_utc then
					create date.make_now_utc
				else
					create date.make_now
				end
			else
				set_from_time_t (current_utc - time_t_seconds_behind_utc)
			end
			generate
		end

	--|--------------------------------------------------------------

	set_from_date_time (v: DATE_TIME)
			-- Reset this timestamp to the time represented by the given argument
		require
			arg_exists: v /= Void
		do
			date := v
			generate
		ensure
			has_date_by_reference: date = v
		end

	--|--------------------------------------------------------------

	set_from_time_t (v: like Kta_time_t)
		local
			dt: DATE_TIME
		do
--RFO 			cached_time_t := v
--			create dt.make_from_epoch (v.to_integer_32)
			create dt.make_from_epoch (v)
			set_from_date_time (dt)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	generate
			-- Generate the string representation of the time held by the
			-- associated date-time object
		local
			pf: AEL_PRINTF
			ts: STRING
		do
			create pf
			if is_fine_grained then
				ts := pf.aprintf (timestamp_format_string,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.fine_second >>)
			else
				ts := pf.aprintf (timestamp_format_string,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.second >>)
			end
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

	--|--------------------------------------------------------------

	timestamp_format_string: STRING
			-- Format string to use when generating the string representation
			-- of this timestamp
		do
			if is_fine_grained then
				Result := "%%04d-%%02d-%%02d %%02d:%%02d:%%06.3f"
			else
				Result := "%%04d-%%02d-%%02d %%02d:%%02d:%%02d"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	terse_timestamp_format_string: STRING
			-- Format string to use when generating the terse
			-- string representation of this timestamp
		do
			if is_fine_grained then
				Result := "%%04d%%02d%%02d-%%02d%%02d%%06.3f"
			else
				Result := "%%04d%%02d%%02d-%%02d%%02d%%02d"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	time_only_format_string: STRING
			-- Format string to use when generating the string representation
			-- of the time-only form of this timestamp
		do
			if is_fine_grained then
				Result := "%%02d:%%02d:%%06.3f"
			else
				Result := "%%02d:%%02d:%%02d"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	terse_time_only_format_string: STRING
			-- Format string to use when generating the terse
			-- string representation of the time-only form of Current
		do
			if is_fine_grained then
				Result := "%%02d%%02d%%06.3f"
			else
				Result := "%%02d%%02d%%02d"
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	date_only_format_string: STRING
			-- Format string to use when generating the
			-- string representation of the date-only form of Current
		do
			Result := "%%04d-%%02d-%%02d"
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	terse_date_only_format_string: STRING
			-- Format string to use when generating the terse
			-- string representation of the date-only form of Current
		do
			Result := "%%04d%%02d%%02d"
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	atr: AEL_DATE_TIME_ROUTINES
		once
			create Result
		end

--|========================================================================
feature -- Special terse timestamp
--|========================================================================

	generate_terse
			-- Generate the terse string representation of the time held by the
			-- associated date-time object
		local
			pf: AEL_PRINTF
			ts: STRING
		do
			create pf
			if is_fine_grained then
				ts := pf.aprintf (terse_timestamp_format_string,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.fine_second >>)
			else
				ts := pf.aprintf (terse_timestamp_format_string,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.second >>)
			end
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

	generate_time_only (tf: BOOLEAN)
			-- Generate the string representation of the time-only part
			-- If 'tf' then generate the terse form
		local
			pf: AEL_PRINTF
			ts: STRING
		do
			create pf
			if is_fine_grained then
				ts := pf.aprintf (
					terse_time_only_format_string,
					<< date.hour, date.minute, date.fine_second >>)
			else
				ts := pf.aprintf (
					terse_time_only_format_string,
					<< date.hour, date.minute, date.second >>)
			end
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

	generate_date_only (tf: BOOLEAN)
			-- Generate the string representation of the date only part
			-- If 'tf' then generate the terse form
		local
			pf: AEL_PRINTF
			ts: STRING
		do
			create pf
			ts := pf.aprintf (
				terse_date_only_format_string,
				<< date.year, date.month, date.day>>)
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

	--|--------------------------------------------------------------

	generate_iso
			-- Generate the ISO 8601 string representation of the time
			-- held by the associated date-time object
		local
			pf: AEL_PRINTF
			fmt, ts: STRING
			sb, hb, mb: INTEGER
		do
			create pf
			if is_fine_grained then
				fmt := "%%04d-%%02d-%%02dT%%02d:%%02d:%%06.3f"
			else
				fmt := "%%04d-%%02d-%%02dT%%02d:%%02d:%%02d"
			end
			sb := seconds_behind_utc
			if is_utc or sb = 0 then
				fmt.extend ('Z')
			else
				if sb <= 0 then
					-- Actually _ahead_ of UTC
					sb := -sb
					fmt.extend ('+')
				else
					fmt.extend ('-')
				end
				hb := sb // 3600
				mb := (sb - (hb * 3600)) // 60
				-- "-hh:mm
				fmt.append (pf.aprintf ("%%02d:%%02d", << hb, mb >>))
			end
			if is_fine_grained then
				ts := pf.aprintf (fmt,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.fine_second >>)
			else
				ts := pf.aprintf (fmt,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.second >>)
			end
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

	--|--------------------------------------------------------------

	generate_slashed
			-- Generate the string representation of the time
			-- held by the associated date-time object, using
			-- forward slashes as date separators
			-- Format:  yyyy/mm/dd hh:mm:ss
			-- Unless requested leave off fine-grain fields
		local
			pf: AEL_PRINTF
			fmt, ts: STRING
		do
			create pf
			if is_fine_grained then
				fmt := "%%04d/%%02d/%%02d  %%02d:%%02d:%%06.3f"
			else
				fmt := "%%04d/%%02d/%%02d  %%02d:%%02d:%%02d"
			end
			if is_fine_grained then
				ts := pf.aprintf (fmt,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.fine_second >>)
			else
				ts := pf.aprintf (fmt,
				                  << date.year, date.month, date.day,
										date.hour, date.minute, date.second >>)
			end
			wipe_out
			append (ts)
		ensure
			generated: not is_empty
		end

--|--------------------------------------------------------------
invariant
	has_date: date /= Void
	has_form: not is_empty

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 005 16-Oct-2013
--|     Added inherit of AEL_DATE_TIME_TYPES
--|     Changed, throughout the AEL time classes, the type of time_t
--|     from natural 32 to integer 32 to comply with UNIX definition.
--|     Swapped times in time comparisons in to_time_t and
--|     to_seconds_and_parts to fix problem that was producing incorrect
--|     timestamp values
--| 004 30-Apr-2013
--|     Added support for ISO 8601 format, with 'generate_iso'
--|     Supports only full date/time format. Date-only is no different.
--| 003 08-Aug-2012
--|     Updated for void-safety. Compiled and tested using Eiffel 7.0
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--|
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|
--| If you want to mark local time, use either make or
--| make_fine_grained.
--|
--| If you want to mark UTC time, use either make_utc or
--| make_fine_utc.
--|
--| Although this is a descendent of STRING, the string representation
--| is NOT simply Current and so references to objects of this class
--| can NOT be treated as if they were strings.  Instead use
--| <this>.string when needing a valid STRING object from Current.
--|
--| If you set the associated date bia set_from_date_time, be sure to
--| update the string rendition every time the external data object is
--| updated (if at all).  The string representation is kept up-to-date
--| automatically in all other cases.
--|
--| This class depends on the Eiffel Base libraries, the TIME library
--| from Eiffel Software, and the AEL_PRINTF library from Amalasoft.
--|----------------------------------------------------------------------

end -- class AEL_TIMESTAMP
