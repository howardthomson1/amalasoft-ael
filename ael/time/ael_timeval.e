--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| A timeval-like pair of seconds and microseconds since epoch
--|----------------------------------------------------------------------

class AEL_TIMEVAL

inherit
	COMPARABLE
		redefine
			out, default_create
		end

create
	make_with_values, make_with_value_tuple,
	make_now, make_with_total_milliseconds

 --|========================================================================
feature {NONE} -- Creation
 --|========================================================================

	make_with_values (s, ms: INTEGER)
			-- Create Current with seconds 's' and additional 
			-- milliseconds 'ms'
		do
			default_create
			set_seconds (s)
			set_mseconds (ms)
		end

	make_with_value_tuple (vt: TUPLE [INTEGER_32, INTEGER_32])
			-- Create Current with seconds and milliseconds to values in 'vt'
			-- respectively
		require
			exists: vt /= Void
			msecs_in_range: is_valid_milliseconds (vt.integer_item (2))
		do
			make_with_values (vt.integer_item (1), vt.integer_item (2))
		end

	--|--------------------------------------------------------------

	make_with_total_milliseconds (ms: INTEGER_64)
			-- Create Current with total milliseconds
		do
			default_create
			set_seconds ((ms // 1000).as_integer_32)
			set_mseconds ((ms - (seconds * 1000)).as_integer_32)
		end

	--|--------------------------------------------------------------

	make_now
		do
			default_create
			set_to_present
		end

	--|--------------------------------------------------------------

	default_create
			-- Create current in its default state
		do
			create area.make (c_struct_size)
		end

--|========================================================================
feature -- Status
--|========================================================================

	area: MANAGED_POINTER
			-- Memory allocated for associated timeb structure
 
	address: POINTER
			-- Base address of Current's allocated structure memory
		do
			Result := area.item
		end

	--|--------------------------------------------------------------

	seconds: INTEGER
			-- Whole seconds since epoch

	milliseconds: INTEGER
			-- Additional milliseconds since epoch, in addition to seconds

	total_milliseconds: INTEGER_64
			-- Total milliseconds (INCLUDING all seconds) since epoch
		do
			Result := (seconds * 1000) + milliseconds
		end

	useconds: INTEGER
			-- Additional microseconds since epoch, in addition to seconds
			-- N.B. usecs are approximated from milliseconds

	to_double: DOUBLE
		do
			Result := seconds + (milliseconds / 1_000)
		end

	--|--------------------------------------------------------------

	out: STRING
			-- String reprentation of Current
		do
			create Result.make (24)
			Result.append_integer (seconds)
			Result.extend (':')
			Result.append_integer (milliseconds)
		end

--|========================================================================
feature -- Field access
--|========================================================================

	time_field_value: INTEGER
			-- Value of the time field in the associated struct
			-- (Seconds sinces epoch)
		do
			Result := c_tb_time (address)
		end

	millitm_field_value: INTEGER
			-- Value of the millitm field in the associated struct
			-- (Additional milliseconds sinces epoch)
		do
			Result := c_tb_millitm (address)
		ensure
			non_negative: Result >= 0
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_to_present
			-- Get current time from OS, record in struct and cached values
		do
			c_ftime (address)
			update_cached_values
		ensure
			values_match: seconds = time_field_value
			values_match: milliseconds = millitm_field_value
			usecs_match: useconds = (milliseconds * 1000)
		end

	--|--------------------------------------------------------------

	set_seconds (v: INTEGER)
			-- Set value of cached 'seconds' and in-struct 'time' to 'v'
		do
			seconds := v
			c_set_tb_time (address, v)
		ensure
			values_match: seconds = time_field_value
		end

	--|--------------------------------------------------------------

	set_mseconds (v: INTEGER)
			-- Set to 'v' the cached 'milliseconds' value and in-struct
			-- 'millitm' to 'v'
		require
			msecs_in_range: v.abs <= 1000
		do
			milliseconds := v
			useconds := v * 1000
			c_set_tb_millitm (address, v)
		ensure
			values_match: milliseconds = millitm_field_value
			usecs_match: useconds = (milliseconds * 1000)
		end

	--|--------------------------------------------------------------

	set_value_tuple (vt: TUPLE [INTEGER_32, INTEGER_32])
			-- Set seconds and milliseconds to values in 'vt', 
			-- respectively
		do
			set_values (vt.integer_item (1), vt.integer_item (2))
		end

	set_values (sec, msec: INTEGER_32)
			-- Set seconds to 'sec' and milliseconds to 'msec'
		do
			set_seconds (sec)
			set_mseconds (msec)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			if seconds = other.seconds then
				Result := milliseconds < other.milliseconds
			else
				Result := seconds < other.seconds
			end
		end

	--|--------------------------------------------------------------

	difference (other: like Current): like Current
			-- Number of seconds and milliseconds between Current and
			-- other as in Current - other
		local
			tms, otms: INTEGER_64
		do
			tms := total_milliseconds
			otms := other.total_milliseconds
			create Result.make_with_total_milliseconds (tms - otms)
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	difference_in_milliseconds (other: like Current): INTEGER_64
			-- Number of total milliseconds between Current and
			-- other as in Current - other
		do
			Result := difference_in_values (
				[other.seconds, other.milliseconds],
				[seconds, milliseconds])
		end

	--|--------------------------------------------------------------

	difference_in_values (v1, v2: TUPLE [INTEGER_32, INTEGER_32]): INTEGER_64
			-- Number of total milliseconds between time value pairs 
			-- 'v1' and 'v2
			-- As in v2 - v1
		require
			values_exist: v1 /= Void and v2 /= Void
			msecs1_in_range: is_valid_milliseconds (v1.integer_item (2))
			msecs2_in_range: is_valid_milliseconds (v2.integer_item (2))
		local
			sec1, sec2, msec1, msec2: INTEGER_32
		do
			sec1 := v1.integer_item (1)
			msec1 := v1.integer_item (2)
			sec2 := v2.integer_item (1)
			msec2 := v2.integer_item (2)
			if sec2 < sec1 then
				Result := -difference_in_values (v2, v1)
			else
				Result := ((sec2 - sec1) * 1000) + (msec2 - msec1)
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_milliseconds (v: INTEGER): BOOLEAN
			-- Is 'v  a valid number of additional milliseconds?
		do
			if v < 0 then
				Result := K_range_milliseconds.has (-v)
			else
				Result := K_range_milliseconds.has (v)
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	update_cached_values
			-- Update cached seconds and milliseconds from values in 
			-- struct
		do
			seconds := c_tb_time (address)
			milliseconds := c_tb_millitm (address)
			useconds := milliseconds * 1000
		ensure
			values_match: seconds = time_field_value
			values_match: milliseconds = millitm_field_value
			usecs_match: useconds = (milliseconds * 1000)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	K_range_milliseconds: INTEGER_INTERVAL
			-- Range of legit values for milliseconds
		once
			create Result.make (0, 999)
		end

--|========================================================================
feature {NONE} -- Externals
--|========================================================================

	c_struct_size: INTEGER
			-- We use ael_types.h because struct timeb is defined in 
			-- sys/time.h on UNIX systems, but in winsock.h on Windows, GRRRR
		external
		"C [macro %"ael_types.h%"]: EIF_INTEGER"
		alias
		"sizeof(struct timeb)"
		end

	--|--------------------------------------------------------------

	c_tb_time (p: POINTER): INTEGER
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb): EIF_INTEGER"
		alias
		"time"
		end

	--|------------------------------------------------------------------------

	c_tb_millitm (p: POINTER): INTEGER
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb): EIF_INTEGER"
		alias
		"millitm"
		end

	--|--------------------------------------------------------------

	c_set_tb_time (p: POINTER; v: INTEGER)
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb, int)"
		alias
		"time"
		end

	--|------------------------------------------------------------------------

	c_set_tb_millitm (p: POINTER; v: INTEGER)
		require
			valid_address: p /= Default_pointer
		external
		"C [struct %"ael_types.h%"] (struct timeb, int)"
		alias
		"millitm"
		end

	--|--------------------------------------------------------------

	c_ftime (p: POINTER)
		require
			valid_address: p /= Default_pointer
		external
		"C [macro <time.h>] (struct timeb *)"
		alias
		"ftime"
		end

 --|------------------------------------------------------------------------
invariant
	valid_milliseconds: is_valid_milliseconds (milliseconds)

end -- class AEL_TIMEVAL

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 22-Jul-2018
--|     Simplified implementation of 'difference '
--|     Compiled and tested using Eiffel 18.1
--| 001 18-Apr-2009
--|     Original module in this context; compiled under ec 6.5
--|     Adapted from much older existing module
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of this class using either make_now (for the
--| current date/time) or make_with_values, providing the seconds and
--| microseconds values.
--| Once created, it is also possible to reset to the present 
--| date/time by calling set_to_present, or to any specific time by
--| calling the set_seconds and/or set_mseconds routines.
--| Instances of this class are comparable and computable via the
--| is_less ("<") and difference routines.
--| The out routine provides a two-field string, with a colon 
--| separator.
--|
--| This class depends on the Eiffel Base libraries, the TIME library
--| from Eiffel Software, and the AEL_PRINTF library from Amalasoft.
--|----------------------------------------------------------------------
