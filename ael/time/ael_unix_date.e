--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Extension of C_DATE from Eiffel Software
--|
--|----------------------------------------------------------------------

class AEL_UNIX_DATE

inherit
	C_DATE
		redefine
			out
		end
	AEL_DATE_TIME_TYPES
		undefine
			default_create, is_equal, copy, out
		end

create
	default_create, make_utc, make_from_timet, make_from_timet_utc

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make_from_timet (v: like Kta_time_t)
		do
			default_create
			update_from_timet (v)
		end

	make_from_timet_utc (v: like Kta_time_t)
		do
			make_utc
			update_from_timet (v)
		end

	--|--------------------------------------------------------------

	update_from_timet (t: like Kta_time_t)
			-- Set time/date to given time_t value
		local
			l_tm, l_time: POINTER
		do
			l_time := l_time.memory_alloc (time_t_structure_size)
			set_time_t (l_time, t)
			if is_utc then
				l_tm := gmtime ($t)
			else
				l_tm := localtime ($t)
			end
			internal_item.item.memory_copy (l_tm, tm_structure_size)
			l_time.memory_free
		end

--|========================================================================
feature -- External representation
--|========================================================================

	out: STRING
		local
			d: DATE
			t: TIME
			dt: DATE_TIME
		do
			create d.make (year_now, month_now, day_now)
			create t.make_fine (
				hour_now, minute_now, second_now + millisecond_now / 1000)
			create dt.make_by_date_time (d, t)
			Result := dt.formatted_out (Ks_udate_fmt)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_udate_fmt: STRING = "[0]mm/[0]dd/yyyy hh12:[0]mi:[0]ss AM";

--|========================================================================
feature -- UTC Externals; callable anonymously
--|========================================================================

	set_time_t (a_time: POINTER; t: like Kta_time_t)
		external
			"C inline use <time.h>"
		alias
			"*((time_t *) $a_time) = (time_t) $t;"
		end

	--|--------------------------------------------------------------

	current_utc: like Kta_time_t
			-- Current GMT time in UTC form (seconds since epoch)
		external
			"C inline use <time.h>"
		alias
			"[
				return time (NULL);
			]"
		end

end -- class AEL_UNIX_DATE

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 16-Oct-2013
--|     Added inherit of AEL_DATE_TIME_TYPES
--|     Changed, throughout the AEL time classes, the type of time_t 
--|     from natural 32 to integer 32 to comply with UNIX definition.
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|----------------------------------------------------------------------
