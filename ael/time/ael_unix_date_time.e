--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Extension of DATE_TIME from Eiffel Software
--|
--|----------------------------------------------------------------------

class AEL_UNIX_DATE_TIME

inherit
	DATE_TIME
		redefine
			default_format_string, relative_duration,
			make_now_utc
		end
	AEL_DATE_TIME_TYPES
		undefine
			default_create, is_equal, copy, out
		end

create
	make_from_timet, make_from_timet_utc,
	make, make_fine, make_by_date_time, make_by_date,
	make_now, make_now_utc, make_from_epoch,
	make_from_string, make_from_string_with_base,
	make_from_string_default, make_from_string_default_with_base

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_timet (v: like Kta_time_t)
		do
			update_from_timet (v, False)
		end

	make_from_timet_utc (v: like Kta_time_t)
		do
			is_utc := True
			update_from_timet (v, True)
		end

	make_now_utc
			-- Get the date and the time from the system.
		do
			is_utc := True
			Precursor
		end

	--|--------------------------------------------------------------

	update_from_timet (v: like Kta_time_t; uf: BOOLEAN)
			-- Pointer to `struct tm' area.
			-- 'uf' denotes UTC if true
		local
			ud: AEL_UNIX_DATE
			d: DATE
			t: TIME
		do
			if (uf) then
				create ud.make_from_timet_utc (v)
			else
				create ud.make_from_timet (v)
			end

			create d.make (ud.year_now, ud.month_now, ud.day_now)
			create t.make_fine (ud.hour_now, ud.minute_now,
			ud.second_now +ud. millisecond_now / 1000)
			make_by_date_time (d, t)
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_utc: BOOLEAN
			-- Is Current based on UTC?

--|========================================================================
feature -- External representation
--|========================================================================

	default_format_string: STRING
		do
			Result := Ks_update_fmt
		end

--|========================================================================
feature -- Duration
--|========================================================================

	relative_duration (other: like Current): AEL_UNIX_DATE_TIME_DURATION
			-- Duration from `other' to current date
		do
			create Result.make_fine (0, 0, days - other.days,
				hour - other.hour, minute - other.minute,
				fine_second - other.fine_second)
			Result := Result.to_canonical (other)
			Result.set_origin_date_time (other.twin)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_update_fmt: STRING = "[0]mm/[0]dd/yyyy hh12:[0]mi:[0]ss AM"

end -- class AEL_UNIX_DATE_TIME

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 003 16-Oct-2013
--|     Added inherit of AEL_DATE_TIME_TYPES
--|     Changed type of utc from natural to Kta_time_t (int 32)
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|----------------------------------------------------------------------
