--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| Extension of DATE_TIME_DURATION from Eiffel Software
--|
--|----------------------------------------------------------------------

class AEL_UNIX_DATE_TIME_DURATION

inherit
	DATE_TIME_DURATION
		redefine
			origin_date_time, set_origin_date_time
		end

create
	make,
	make_definite,
	make_fine,
	make_by_date_time,
	make_by_date

 --|========================================================================
feature
 --|========================================================================

	origin_date_time: detachable AEL_UNIX_DATE_TIME

	--|--------------------------------------------------------------

	set_origin_date_time (dt: detachable AEL_UNIX_DATE_TIME)
		do
			origin_date_time := dt
			if dt /= Void then
				date.set_origin_date (dt.date)
			else
				date.set_origin_date (Void)
			end
		end

end -- class AEL_UNIX_DATE_TIME_DURATION

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 28-Jul-2009
--|     Compiled and tested using Eiffel 6.2 and 6.4
--|----------------------------------------------------------------------
--| How-to
--| Create an instance of the this class using any of the listed
--| creation procedures.
--|----------------------------------------------------------------------
