note
	description: "{
Constants and related features for USML tags and document elements
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_CONSTANTS

inherit
	AEL_PRINTF

--|========================================================================
feature -- Constants
--|========================================================================

--TODO clean up dupes
	Ks_dflt_color_text: STRING = "000000"
	Ks_dflt_bgcolor_text: STRING = "FFFFFF"

	Ks_dflt_font_color: STRING = "000000"

	K_dflt_point_size_doc_title: INTEGER = 18
	K_dflt_font_family_doc_title: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_doc_title: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_h1: INTEGER = 16
	K_dflt_font_family_h1: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_h1: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_h2: INTEGER = 14
	K_dflt_font_family_h2: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_h2: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_h3: INTEGER = 12
	K_dflt_font_family_h3: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_h3: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_h4: INTEGER = 12
	K_dflt_font_family_h4: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_h4: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_h5: INTEGER = 12
	K_dflt_font_family_h5: STRING
		once
			Result := Ks_ff_sans.twin
		end
	Ks_dflt_font_color_h5: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	K_dflt_point_size_p: INTEGER = 11
	K_dflt_font_family_p: STRING
		once
			Result := Ks_ff_times.twin
		end
	Ks_dflt_font_color_p: STRING
		once
			Result := Ks_dflt_font_color.twin
		end

	--|--------------------------------------------------------------

	Ks_ff_sans: STRING = "sans"
	Ks_ff_times: STRING = "times"
	Ks_ff_fixed: STRING = "fixed"
	Ks_ff_dflt: STRING = "sans"

	reserved_labels: HASH_TABLE [STRING, STRING]
			--	Table of upper-case reserved labels (tag types and variants)
		local
			ni, nlim: INTEGER
			ts: STRING
		once
			create Result.make (37)
			Result.extend ("P", "P")
			Result.extend ("USML", "USML")
			Result.extend ("INLINE", "INLINE")
			Result.extend ("INCLUDE", "INCLUDE")
			Result.extend ("F", "F")
			Result.extend ("FP", "FP")
			Result.extend ("MP", "MP")
			Result.extend ("M", "M")
			Result.extend ("X", "X")
			Result.extend ("V", "V")
			Result.extend ("H+", "H+")
			Result.extend ("H-", "H-")
			Result.extend ("HT", "HT")
			Result.extend ("FHT", "FHT")
			nlim := K_usml_hdr_lvl_max
			from ni := 1
			until ni > nlim
			loop
				ts := "H" + ni.out
				Result.extend (ts, ts)
				ts := "MH" + ni.out
				Result.extend (ts, ts)
				ts := "FH" + ni.out
				Result.extend (ts, ts)
				ts := "L" + ni.out
				Result.extend (ts, ts)
				ts := "ML" + ni.out
				Result.extend (ts, ts)
				ts := "FL" + ni.out
				Result.extend (ts, ts)
				ni := ni + 1
			end
		end

	Ks_usml_tag_prefix: STRING = "<usml "
	Ks_usml_tag_prefix_nb: STRING = "<usml"
	Ks_usml_tag_close_prefix: STRING = "</usml"
	Ks_usml_tag_prefix_format: STRING = "<usml f "
	Ks_usml_tag_prefix_inline_u: STRING = "<usml inline "
	Ks_usml_tag_prefix_inline_u_nb: STRING = "<usml inline"
	Ks_usml_tag_prefix_inline: STRING = "<inline "
	Ks_usml_tag_prefix_inline_nb: STRING = "<inline"
	Ks_usml_tag_prefix_include_u: STRING = "<usml include "
	Ks_usml_tag_prefix_include: STRING = "<include "

	Ks_usml_tag_v_open_u: STRING = "<usml v>"
	Ks_usml_tag_v_open_prefix_u: STRING = "<usml v"
	Ks_usml_tag_v_close_u: STRING = "</usml v>"
	Ks_usml_tag_v_open: STRING = "<v>"
	Ks_usml_tag_v_open_prefix: STRING = "<v"
	Ks_usml_tag_v_close: STRING = "</v>"

	Ks_usml_tag_x_open_prefix_u: STRING = "<usml x "
			-- multi-line prefix; n.b. MUST have space after 'x'
	Ks_usml_tag_x_open_prefix: STRING = "<x "
			-- terse multi-line prefix; n.b space after 'x'

	Ks_usml_tag_image_prefix_u: STRING = "<usml image "
	Ks_usml_tag_image_prefix: STRING = "<image "

	Ks_usml_tag_title_heading: STRING = "<usml ht>"
--	Ks_usml_tag_inline_close_prefix: STRING = "</usml"
--	Ks_usml_tag_inline_close_prefix_i: STRING = "</inline"
--	Ks_usml_tag_inline_close_long: STRING = "</usml inline>"
	Ks_usml_tag_inline_close_u: STRING = "</usml>"
	Ks_usml_tag_inline_close: STRING = "</inline>"
	Ks_usml_tag_comment_u: STRING = "<usml c>"
	Ks_usml_tag_comment: STRING = "<c>"
	Ks_usml_tag_xcomment_u: STRING = "<usml x c>"
--	Ks_usml_tag_bad_xcomment: STRING = "<usml xc>"
	Ks_usml_tag_xcomment: STRING = "<x c>"
	Ks_usml_tag_x_close_u: STRING = "</usml x>"
	Ks_usml_tag_x_close: STRING = "</x>"

	--|--------------------------------------------------------------

	K_usml_min_tag_len: INTEGER = 8
			-- <usml p>

	Ks_fmt_tag_char: STRING = "char="
	Ks_fmt_tag_par: STRING = "par="
	Ks_fmt_tag_ol: STRING = "out="

	Ks_fmt_tag_macro: STRING = "macro="
	Ks_fmt_tag_stype: STRING = "type="

	Ks_interline_space: STRING = " "

	Ks_char_formats_pf_fmt: STRING = "%"%%s,%%s,%%s,%%s,%%s,%%s,%%s,%%s,%%s%""
	Ks_par_formats_pf_fmt: STRING = "%"%%s,%%s,%%s,%%s,%%s%""
	Ks_ol_formats_pf_fmt: STRING = "%"%%s,%%s,%%s%""
	Ks_xref_formats_pf_fmt: STRING = "%"%%s,%%s%""

	--RFO Ks_inline_w_chars_pf_fmt: STRING
	--RFO 	once
	--RFO 		create Result.make (32)
	--RFO 		-- "<usml inline "
	--RFO 		Result.append (Ks_usml_tag_prefix_inline)
	--RFO 		-- "char="
	--RFO 		Result.append (Ks_fmt_tag_char)
	--RFO 		Result.append ("%"%%s,%%s,%%s,%%s,%%s,%%s,%%s,%%s,%%s%"")
	--RFO 		Result.append (">")
	--RFO 	end

	Ks_fmt_tag_align: STRING = "align="
	Ks_fmt_tag_bold: STRING = "bold="
	Ks_fmt_tag_case: STRING = "case="
	Ks_fmt_tag_bg_color: STRING = "bgcolor="
	Ks_fmt_tag_color: STRING = "color="
	Ks_fmt_tag_font: STRING = "font="
	Ks_fmt_tag_italic: STRING = "italic="
	Ks_fmt_tag_margin_bottom: STRING = "bottom="
	Ks_fmt_tag_margin_left: STRING = "left="
	Ks_fmt_tag_margin_right: STRING = "right="
	Ks_fmt_tag_margin_top: STRING = "top="
	Ks_fmt_tag_offset: STRING = "offset="
	Ks_fmt_tag_name: STRING = "name="
	Ks_fmt_tag_desc: STRING = "desc="
	Ks_fmt_tag_num_sep: STRING = "num_sep="
	Ks_fmt_tag_num_fmt: STRING = "num_fmt="
	Ks_fmt_tag_num_start: STRING = "start="
	Ks_fmt_tag_intext: STRING = "intext="
	Ks_fmt_tag_point_size: STRING = "point_size="
	Ks_fmt_tag_pre_text: STRING = "pre_text="
	Ks_fmt_tag_strike_through: STRING = "strike="
	Ks_fmt_tag_underlined: STRING = "underlined="
	Ks_fmt_tag_no_inlines: STRING = "no_inlines"

	Ks_fmt_tag_ref: STRING = "ref="
			-- For in-line reference definition tags
			-- <usml inline ref="my_ref_id"...
	Ks_fmt_tag_xref: STRING = "xref="
			-- For in-line cross-reference definition tags
			-- <usml inline xref="my_ref_id"...
			-- <usml inline xref="uri,my_ref_id"...
	Ks_fmt_tag_img: STRING = "img="
			-- For in-line cross-reference to image tags
			-- <usml inline img="my_folder/my_image.png"...
	Ks_fmt_tag_pbuf: STRING = "pbuf="
			-- For in-line cross-reference to pixel buffer tags
			-- <usml inline img="pic_pbuf"...

	Ks_xref_type_anchor: STRING = "#"
	Ks_xref_type_ext_img: STRING = "exti"
	Ks_xref_type_emb_img: STRING = "embi"
	Ks_xref_type_pbuf: STRING = "pbuf"
	Ks_xref_type_url: STRING = "url"
	Ks_xref_type_file: STRING = "file"
	Ks_xref_type_action: STRING = "act"

	K_xref_type_dflt: INTEGER = 0
	K_xref_type_anchor: INTEGER = 0
	K_xref_type_ext_img: INTEGER = 1
	K_xref_type_emb_img: INTEGER = 2
	K_xref_type_pbuf: INTEGER = 3
	K_xref_type_url: INTEGER = 4
	K_xref_type_file: INTEGER = 5
	K_xref_type_action: INTEGER = 6

	K_usml_char_fields: INTEGER = 9
			-- char="<f>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<c>"
	K_usml_char_fld_ff: INTEGER = 1
	K_usml_char_fld_ps: INTEGER = 2
	K_usml_char_fld_cv: INTEGER = 3
	K_usml_char_fld_vs: INTEGER = 4
	K_usml_char_fld_b: INTEGER = 5
	K_usml_char_fld_i: INTEGER = 6
	K_usml_char_fld_u: INTEGER = 7
	K_usml_char_fld_st: INTEGER = 8
	K_usml_char_fld_tc: INTEGER = 9

	K_usml_par_fields: INTEGER = 5
			-- par="<av>,<ml>,<mr>,<mt>,<mb>"
	K_usml_par_fld_av: INTEGER = 1
	K_usml_par_fld_ml: INTEGER = 2
	K_usml_par_fld_mr: INTEGER = 3
	K_usml_par_fld_mt: INTEGER = 4
	K_usml_par_fld_mb: INTEGER = 5
--RFO 	K_usml_par_fld_nt: INTEGER = 6
--RFO 	K_usml_par_fld_lw: INTEGER = 7 -- enable/disable linewrap
--RFO 	K_usml_par_fld_pt: INTEGER = 8

	Ks_dflt_num_sep: STRING = " "

	K_usml_ol_fields: INTEGER = 3
			-- outline="<lt>,<ns>,<pt>"
	K_usml_ol_fld_lt: INTEGER = 1
	K_usml_ol_fld_ns: INTEGER = 2
	K_usml_ol_fld_pt: INTEGER = 3

	-- hdr tag type values align with hdr depth

	K_usml_tag_h: INTEGER = 1000
	K_usml_tag_h1: INTEGER = 1
	K_usml_tag_h2: INTEGER = 2
	K_usml_tag_h3: INTEGER = 3
	K_usml_tag_h4: INTEGER = 4
	K_usml_tag_h5: INTEGER = 5
--	K_usml_tag_h6: INTEGER = 6
--	K_usml_tag_h7: INTEGER = 7
--	K_usml_tag_h8: INTEGER = 8
--	K_usml_tag_h9: INTEGER = 9

	heading_types: ARRAY [INTEGER]
			-- Supported heading levels
			--(and corresponding section types)
		once
			Result := {ARRAY [INTEGER]} <<
				K_usml_tag_h1,
				K_usml_tag_h2,
				K_usml_tag_h3,
				K_usml_tag_h4,
				K_usml_tag_h5
				>>
		end

	Ks_tag_label_h1: STRING = "H1"
	Ks_tag_label_h2: STRING = "H2"
	Ks_tag_label_h3: STRING = "H3"
	Ks_tag_label_h4: STRING = "H4"
	Ks_tag_label_h5: STRING = "H5"

	Ks_tag_long_label_h1: STRING = "Level 1 Heading"
	Ks_tag_long_label_h2: STRING = "Level 2 Heading"
	Ks_tag_long_label_h3: STRING = "Level 3 Heading"
	Ks_tag_long_label_h4: STRING = "Level 4 Heading"
	Ks_tag_long_label_h5: STRING = "Level 5 Heading"

	heading_type_labels: ARRAY [STRING]
			-- Supported heading levels in terse label form
			--(and corresponding section types)
		once
			Result := {ARRAY [STRING]} <<
				Ks_tag_label_h1,
				Ks_tag_label_h2,
				Ks_tag_label_h3,
				Ks_tag_label_h4,
				Ks_tag_label_h5
				>>
		end

	heading_type_long_labels: ARRAY [STRING]
			-- Supported heading levels in long label form
			--(and corresponding section types)
		once
			Result := {ARRAY [STRING]} <<
				Ks_tag_long_label_h1,
				Ks_tag_long_label_h2,
				Ks_tag_long_label_h3,
				Ks_tag_long_label_h4,
				Ks_tag_long_label_h5
				>>
		end

	K_usml_hdr_lvl_min: INTEGER = 1
	K_usml_hdr_lvl_max: INTEGER = 5

	-- Non-hdr tag types do not correspond to levels (they have no levels)

	K_usml_tag_doc: INTEGER = 100
	K_usml_tag_title: INTEGER = 99
	Ks_tag_label_title: STRING = "HT"
	Ks_tag_long_label_title: STRING = "Title"

	K_usml_tag_p: INTEGER = 10
			-- Paragraph/Text tag
	Ks_tag_label_p: STRING = "P"
	Ks_tag_long_label_p: STRING = "Paragraph"

	K_usml_tag_sp: INTEGER = 11
			-- subordinate pair (K_usml_tag_h_plus & K_usml_tag_h_minus)
			-- for parsing, use individual tag types

	--|--------------------------------------------------------------

	K_usml_tag_m: INTEGER = 101
			-- Macro tag

	K_usml_tag_c: INTEGER = 102
			-- Comment tag
	Ks_tag_label_c: STRING = "C"
	Ks_tag_long_label_c: STRING = "Comment"

	K_usml_tag_f: INTEGER = 103
			-- format tag

	K_usml_tag_h_plus: INTEGER = 300
	K_usml_tag_h_minus: INTEGER = 301
	K_usml_tag_inline: INTEGER = 302

	K_usml_tag_v_open: INTEGER = 500
	K_usml_tag_v_close: INTEGER = 501
	Ks_tag_label_v: STRING = "V"
	Ks_tag_long_label_v_open: STRING = "Verbatim Open"
	Ks_tag_long_label_v_close: STRING = "Verbatim Close"

	K_usml_tag_char_fmt_only: INTEGER = -1
	K_usml_tag_untyped: INTEGER = 0

	K_inline_token_text: INTEGER = 0
	K_inline_token_open: INTEGER = 1
	K_inline_token_close: INTEGER = 2
	K_inline_token_captive: INTEGER = 3

	-- Format parameter depths (when not implied by context)
	K_fmt_depth_all: INTEGER = 0
	K_fmt_depth_char_only: INTEGER = 1
	K_fmt_depth_char_plus: INTEGER = 2
	K_fmt_depth_char_par: INTEGER = 3

	--|--------------------------------------------------------------

	-- numlist tag type values align with numlist depth (plus bias)

	K_usml_tag_list_bias: INTEGER = 200
	K_usml_tag_l: INTEGER = 2000
	K_usml_tag_l1: INTEGER = 201
	K_usml_tag_l2: INTEGER = 202
	K_usml_tag_l3: INTEGER = 203
	K_usml_tag_l4: INTEGER = 204
	K_usml_tag_l5: INTEGER = 205

	K_usml_list_lvl_min: INTEGER = 201
	K_usml_list_lvl_max: INTEGER = 205

	list_item_types: ARRAY [INTEGER]
			-- Supported list item levels
			--(and corresponding biased section types)
		once
			Result := {ARRAY [INTEGER]} <<
				K_usml_tag_l1,
				K_usml_tag_l2,
				K_usml_tag_l3,
				K_usml_tag_l4,
				K_usml_tag_l5
				>>
		end

	Ks_tag_label_l1: STRING = "L1"
	Ks_tag_label_l2: STRING = "L2"
	Ks_tag_label_l3: STRING = "L3"
	Ks_tag_label_l4: STRING = "L4"
	Ks_tag_label_l5: STRING = "L5"

	Ks_tag_long_label_l1: STRING = "Level 1 List Item"
	Ks_tag_long_label_l2: STRING = "Level 2 List Item"
	Ks_tag_long_label_l3: STRING = "Level 3 List Item"
	Ks_tag_long_label_l4: STRING = "Level 4 List Item"
	Ks_tag_long_label_l5: STRING = "Level 5 List Item"

	list_type_labels: ARRAY [STRING]
			-- Supported list item levels in terse label form
			--(and corresponding biased section types)
		once
			Result := {ARRAY [STRING]} <<
				Ks_tag_label_l1,
				Ks_tag_label_l2,
				Ks_tag_label_l3,
				Ks_tag_label_l4,
				Ks_tag_label_l5
				>>
		end

	list_type_long_labels: ARRAY [STRING]
			-- Supported list item levels in long label form
			--(and corresponding biased section types)
		once
			Result := {ARRAY [STRING]} <<
				Ks_tag_long_label_l1,
				Ks_tag_long_label_l2,
				Ks_tag_long_label_l3,
				Ks_tag_long_label_l4,
				Ks_tag_long_label_l5
				>>
		end

	--|--------------------------------------------------------------

--RFO 	K_align_left: INTEGER = 0
--RFO 	K_align_center: INTEGER = 2
--RFO 	K_align_right: INTEGER = -1
--RFO 	K_align_justified: INTEGER = 1
	-- Changed to 'align' with those in EV_PARAGRAPH_CONSTANTS, in case
	K_align_left: INTEGER = 1
	K_align_center: INTEGER = 2
	K_align_right: INTEGER = 3
	K_align_justified: INTEGER = 4

--|========================================================================
feature {NONE} -- Format types
--|========================================================================

	-- Format types; used as indices into change map
	K_ft_font_family: INTEGER = 1
	K_ft_point_size: INTEGER = 2
	K_ft_case_value: INTEGER = 3
	K_ft_vertical_offset: INTEGER = 4
	K_ft_is_bold: INTEGER = 5
	K_ft_is_italic: INTEGER = 6
	K_ft_is_underlined: INTEGER = 7
	K_ft_is_strike_through: INTEGER = 8
	K_ft_text_color: INTEGER = 9
	K_ft_bg_color: INTEGER = 10

	K_ft_alignment_value: INTEGER = 11
	K_ft_margin_left: INTEGER = 12
	K_ft_margin_right: INTEGER = 13
	K_ft_margin_top: INTEGER = 14
	K_ft_margin_bottom: INTEGER = 15
	K_ft_intext: INTEGER = 16

	K_ft_pre_text: INTEGER = 17
	K_ft_num_sep: INTEGER = 18
	K_ft_num_fmt: INTEGER = 19
	K_ft_num_start: INTEGER = 20
	K_ft_ol_nums: INTEGER = 21

	K_ft_ref_label: INTEGER = 22
	K_ft_xref_type: INTEGER = 23
--	K_ft_target_label: INTEGER = 24
	K_ft_xref_target: INTEGER = 24

	K_ft_max: INTEGER
		once
			Result := K_ft_xref_target
		end
--	K_ft_lw_disabled: INTEGER = 20

	character_parameter_ids: ARRAY [INTEGER]
		once
			Result := {ARRAY [INTEGER]}
				<<
				K_ft_font_family,
				K_ft_point_size,
				K_ft_case_value,
				K_ft_vertical_offset,
				K_ft_is_bold,
				K_ft_is_italic,
				K_ft_is_underlined,
				K_ft_is_strike_through,
				K_ft_text_color,
				K_ft_bg_color
				>>
		end

	paragraph_parameter_ids: ARRAY [INTEGER]
		once
			Result := {ARRAY [INTEGER]}
				<<
				K_ft_alignment_value,
				K_ft_margin_left,
				K_ft_margin_right,
				K_ft_margin_top,
				K_ft_margin_bottom,
				K_ft_intext
				>>
		end

	outline_parameter_ids: ARRAY [INTEGER]
		once
			Result := {ARRAY [INTEGER]}
				<<
				K_ft_pre_text,
				K_ft_num_sep,
				K_ft_num_fmt,
				K_ft_num_start
				>>
		end

	--|--------------------------------------------------------------

	K_max_levels: INTEGER = 7
			-- Maximum nesting levels, for headings, numbered lists

	K_max_inline_levels: INTEGER = 7
			-- Maximum nesting level for inline tags

	K_max_preprocess_depth: INTEGER = 7

	K_max_outline_instance: INTEGER = 5000

--	num_format_tags: STRING = "#AaIit"
	num_format_tags: ARRAY [STRING]
		once
			Result := {ARRAY [STRING]}<< "#", "A", "a", "I", "i", "t" >>
			Result.rebase (0)
			Result.compare_objects
		end

--RFO 	num_formats_number: ARRAY [STRING]
--RFO 		once
--RFO 			Result := {ARRAY [STRING]} << "1", "2", "3", "4", "5", "6", "7" >>
--RFO 		end

	Ks_up_alphabet: STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	up_alphabet_1_5200: ARRAY [STRING]
			-- 1-26      A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
			-- 27-52     AA AB AC AD AE AF AG AH AI AJ AK AL AM 
			--              AN AO AP AQ AR AS AT AU AV AW AX AY AZ
			-- 53-78     BA BB BC BD BE BF BG BH BI BJ BK BL BM
			--              BN BO BP BQ BR BS BT BU BV BW BX BY BZ
			-- ..
			-- 677-702=ZA-ZZ  703-728=AAA-AAZ  729-754=ABA-ABZ
			-- 755-780 ACA-ACZ  .. 1404-1429 AZZ
			-- 1430-1455 BAA
			-- ..
			--          ZAA ZAB ZAC ZAD ZAE ZAF ZAG
			--               ZAH ZAI ZAJ ZAK ZAL ZAM ZAN ZAO ZAP
			--               ZAQ ZAR ZAS ZAT ZAU ZAV ZAW ZAX ZAY ZAZ
		local
			i, j, lim, idx: INTEGER
			bs, ts: STRING
			c: CHARACTER
		once
			create Result.make_filled ("", 1, 5200)
			bs := ""
			lim := 5200
			from idx := 1
			until idx > lim
			loop
				from i := 1
				until i > 26 or idx > lim
				loop
					from j := 1
					until j > 26 or idx > lim
					loop
						c := Ks_up_alphabet.item (j)
						ts := bs + c.out
						Result.put (ts, idx)
						j := j + 1
						idx := idx + 1
					end
					bs := Ks_up_alphabet.item (i).out
					i := i + 1
				end
			end
		end

--RFO 	num_formats_lo_alpha: ARRAY [STRING]
--RFO 		once
--RFO 			Result := {ARRAY [STRING]} << "a", "b", "c", "d", "e", "f", "g" >>
--RFO 		end

	roman_numerals_1_100_up: ARRAY [STRING]
		once
			Result := {ARRAY [STRING]}
				<<
				"I", "II", "III", "IV", "V",
				"VI", "VII", "VIII", "IX", "X",
				"XI", "XII", "XIII", "XIV", "XV",
				"XVI", "XVII", "XVIII", "XIX", "XX",
				"XXI", "XXII", "XXIII", "XXIV", "XXV",
				"XXVI", "XXVII", "XXVIII", "XXIX", "XXX",
				"XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV",
				"XXXVI", "XXXVII", "XXXVIII", "XXXIX", "XL",
				"XLI", "XLII", "XLIII", "XLIV", "XLV",
				"XLVI", "XLVII", "XLVIII", "XLIX", "L",
				"LI", "LII", "LIII", "LIV", "LV",
				"LVI", "LVII", "LVIII", "LIX", "LX",
				"LXI", "LXII", "LXIII", "LXIV", "LXV",
				"LXVI", "LXVII", "LXVIII", "LXIX", "LXX",
				"LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV",
				"LXXVI", "LXXVII", "LXXVIII", "LXXIX", "LXXX",
				"LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV",
				"LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC",
				"XCI", "XCII", "XCIII", "XCIV", "XCV",
				"XCVI", "XCVII", "XCVIII", "XCIX", "C"
				>>
		end

	roman_numeral_magnitude_labels: ARRAY [STRING]
			-- Labels major Roman Numeral magnitudes
			--    1	I
			--    4	IV
			--    5	V
			--    9	IX
			--   10	X
			--   40	XL
			--   50	L
			--   90	XC
			--  100	C
			--  400	CD
			--  500	D
			--  900	CM
			-- 1000	M
		once
			Result := {ARRAY [STRING]}
				<< "I", "IV", "V", "IX", "X", "XL", "L",
				"XC", "C", "CD", "D", "CM", "M"
				>>
		end

	roman_numeral_magnitudes: ARRAY [INTEGER]
			-- Values corresponding, by index, to magnitude labels
		once
			Result := {ARRAY [INTEGER]}
				<< 1, 4, 5, 9, 10, 40, 50,
				90, 100, 400, 500, 900, 1000 >>
		end

--RFO 	num_formats_up_roman: ARRAY [STRING]
--RFO 		once
--RFO 			Result := {ARRAY [STRING]}
--RFO 				<< "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" >>
--RFO 		end

--RFO 	num_formats_lo_roman: ARRAY [STRING]
--RFO 		once
--RFO 			Result := {ARRAY [STRING]}
--RFO 				<< "i", "ii", "iii", "iv", "v", "vi", "vii" >>
--RFO 		ensure
--RFO 			valid_count: Result.count = K_max_levels
--RFO 		end

	K_num_format_number: INTEGER = 0
			-- Dflt outline number format, all digits
			-- Denoted by '#' in format spec
	K_num_format_up_alpha: INTEGER = 1
			-- Outline number format, as upper-case letter
			-- Denoted by 'A' in format spec
	K_num_format_lo_alpha: INTEGER = 2
			-- Outline number format, as lower-case letter
			-- Denoted by 'a' in format spec
	K_num_format_up_roman: INTEGER = 3
			-- Outline number format as upper-case Roman numeral
			-- Denoted by 'I' in format spec
	K_num_format_lo_roman: INTEGER = 4
			-- Outline number format as upper-case Roman numeral
			-- Denoted by 'i' in format spec
	K_num_format_text: INTEGER = 5
			-- Outline number format as user-supplied text (pre-text)
			-- Denoted by 't' in format spec

	K_case_value_as_is: INTEGER = 0
	K_case_value_upper: INTEGER = 1
	K_case_value_lower: INTEGER = -1
	K_case_value_title: INTEGER = 10

--|========================================================================
feature -- Line numbering
--|========================================================================

	K_lineno_width: INTEGER = 5
			-- Width of leading line numbers "0001 "
			-- Based on Ks_lineno_pf_fmt

	Ks_lineno_pf_fmt: STRING = "%%04d "
	Ks_linenum_pf_fmt: STRING = "%%04d%N"

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_CONSTANTS
