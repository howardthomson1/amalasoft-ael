note
	description: "{
Constants and related features for USML tags and document elements
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_CORE

inherit
	AEL_PRINTF
	USML_CONSTANTS

--|========================================================================
feature -- Comparison
--|========================================================================

	strings_same_ins (v1, v2: detachable STRING): BOOLEAN
			-- Are strings 'v1' and 'v2' the same, case-insensitive?
			-- True if BOTH are Void
			-- OR, BOTH are non-Void AND have same values when
			-- coerced to same case
		do
			Result := asr.strings_are_same_case_insensitive (v1, v2)

--RFO 			if v1 = Void and v2 = Void then
--RFO 				Result := True
--RFO 			elseif attached v1 as ts1 and attached v2 as ts2 then
--RFO 				Result := ts1.as_lower ~ ts2.as_lower
--RFO 			end
		end

--|========================================================================
feature -- Serialization support
--|========================================================================

	--RFO format_to_tag (
	--RFO 	fmt: USML_FORMAT; tt, pd: INTEGER; xf, cf, df: BOOLEAN): STRING
	--RFO 		-- Tag text, for format 'fmt' and tag type 'tt'
	--RFO 		-- Where non-zero tag type denotes macro or format tag
	--RFO 		-- If 'xf', then as mult-line
	--RFO 		-- If 'cf', then compact-as-possible
	--RFO 		-- 'pd' is parameter depth
	--RFO 	do
	--RFO 		if xf then
	--RFO 			Result := format_to_multi_tag (fmt, tt, pd, cf, df)
	--RFO 		else
	--RFO 			Result := format_to_single_tag (fmt, tt, pd, cf, df)
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	format_to_multi_tag (
		fmt: USML_FORMAT; tt, pd: INTEGER cf, df: BOOLEAN): STRING
			-- Tag text, for format 'fmt' and tag type 'tt'
			-- 'tt' is tag type; non-zero denotes macro or format tag
			-- If 'cf', then compact-as-possible
			-- If 'df' then include only params that deviate from 
			-- default values
			-- 'pd' is parameter depth
		local
			dfmt: USML_FORMAT
			--mfmt: USML_MACRO_FORMAT
			fdl: LINKED_LIST [INTEGER]
			st, ft, flen: INTEGER
			istr, fs, sts, nm, des: STRING
			params: LINKED_LIST [STRING]
			cof: BOOLEAN
		do
			create Result.make (256)
			istr := "  "
			inspect tt
			else
			end
			st := fmt.section_type
			if st = 0 then
				sts := ""
			else
				sts := section_type_to_label (st).as_upper
			end
			if pd = K_fmt_depth_char_only then
				cof := True
			end
			if tt = K_usml_tag_m then
				nm := fmt.name
				if cf then
					Result.append ("<X M" + sts + ">%N")
				else
					Result.append (
						Ks_usml_tag_x_open_prefix.as_upper + "M" + sts + ">%N")
				end
				Result.append (aprintf (
					"%%s%%s%%s;%N", << istr, Ks_fmt_tag_name, quoted (nm) >>))
				des := fmt.description
				if not des.is_empty then
					Result.append (aprintf (
						"%%s%%s%%s;%N", << istr, Ks_fmt_tag_desc, quoted (des) >>))
				end
			elseif tt = K_usml_tag_f then
				if cf then
					Result.append ("<X F" + sts + ">%N")
				else
					Result.append (
						Ks_usml_tag_x_open_prefix.as_upper + "F" + sts + ">%N")
				end
			else
				if cf then
					Result.append ("<X " + sts + ">%N")
				else
					Result.append (
						Ks_usml_tag_x_open_prefix.as_upper + sts + ">%N")
				end
			end

			if cf then
				Result.append (
					fmt.compact_out (st, df, cof, True, istr))
			else
				flen := Result.count
				create params.make
				if df then
					-- Tag should include only values that deviate from the 
					-- default for the selected section type
					dfmt := default_format_for_type (st)
					fdl := dfmt.differences (fmt)
				else
					fdl := fmt.all_params
				end
				from fdl.start
				until fdl.after
				loop
					ft := fdl.item
					fs := named_format_out (fmt, st, ft, istr)
					fdl.forth
					if not fs.is_empty then
						params.extend (fs)
					end
				end
				from params.start
				until params.after
				loop
					fs := params.item
					Result.append (fs)
					params.forth
					if not params.after then
						Result.append (";%N")
					end
				end
			end
			--				if Result.count > flen then
			if Result.item (Result.count) /= '%N' then
				Result.append ("%N")
			end
--RFO 			if cf then
				Result.append (Ks_usml_tag_x_close.as_upper)
--RFO 			else
--RFO 				Result.append (Ks_usml_tag_x_close_u.as_upper)
--RFO 			end
		end

	--|--------------------------------------------------------------

	format_to_single_tag (
		fmt: USML_FORMAT; tt, pd: INTEGER; cf, df: BOOLEAN): STRING
			-- Tag text, for format 'fmt' and tag type 'tt'
			-- 'tt' is tag type; non-zero denotes macro or format tag
			-- If 'cf', then compact-as-possible
			-- If 'df' then include only params that deviate from 
			-- default values
			-- 'pd' is parameter depth
		local
			dfmt: USML_FORMAT
			--mfmt: USML_MACRO_FORMAT
			fdl: LINKED_LIST [INTEGER]
			st, ft, flen: INTEGER
			istr, fs, sts, nm: STRING
			params: LINKED_LIST [STRING]
			cof: BOOLEAN
		do
			create Result.make (256)
			istr := ""
			inspect tt
			else
			end
			st := fmt.section_type
			if st = 0 then
				sts := ""
			else
				sts := section_type_to_label (st).as_upper
			end
			if pd = K_fmt_depth_char_only then
				cof := True
			end
			if tt = K_usml_tag_m then
				nm := fmt.name
--RFO 				if cf then
					Result.append ("<M" + sts + " ")
--RFO 				else
--RFO 					Result.append (
--RFO 						Ks_usml_tag_prefix.as_upper + "M" + sts + " ")
--RFO 				end
				Result.append (aprintf (
					"%%s%%s;", << Ks_fmt_tag_name, quoted (nm) >>))
			elseif tt = K_usml_tag_f then
--RFO 				if cf then
					Result.append ("<F" + sts + " ")
--RFO 				else
--RFO 					Result.append (
--RFO 						Ks_usml_tag_prefix.as_upper + "F" + sts + " ")
--RFO 				end
			else
--RFO 				if cf then
					Result.append ("<" + sts + " ")
--RFO 				else
--RFO 					Result.append (
--RFO 						Ks_usml_tag_prefix.as_upper + sts + " ")
--RFO 				end
			end

			if cf then
				Result.append (
					fmt.compact_out (st, df, cof, False, istr))
			else
				flen := Result.count
				create params.make
				if df then
					-- Tag should include only values that deviate from the 
					-- default for the selected section type
					dfmt := default_format_for_type (st)
					fdl := dfmt.differences (fmt)
				else
					fdl := fmt.all_params
				end
				from fdl.start
				until fdl.after
				loop
					ft := fdl.item
					fs := named_format_out (fmt, st, ft, istr)
					fdl.forth
					if not fs.is_empty then
						params.extend (fs)
					end
				end
				from params.start
				until params.after
				loop
					fs := params.item
					Result.append (fs)
					params.forth
					if not params.after then
						Result.append (";")
					end
				end
			end
			if not Result.is_empty then
				Result.prune_all_trailing (';')
			end
			Result.append (">")
		end

	--|--------------------------------------------------------------

	named_format_out (fmt: USML_FORMAT; st, ft: INTEGER; ist: STRING): STRING
			-- Format value, in 'fmt', by format index 'ft', as 
			-- tag-value pair suitable for use in a multi-line tag for 
			-- section type 'st'
			-- If inappropriate for section type, then empty
		do
			if not is_applicable_parameter (ft, st) then
				Result := ""
			else
				inspect ft
				when K_ft_font_family then
					if attached fmt.font_family as ff then
						Result := ist + Ks_fmt_tag_font + quoted (ff)
					else
						Result := ist + Ks_fmt_tag_font + "%"%""
					end
				when K_ft_point_size then
					Result := ist + Ks_fmt_tag_point_size + fmt.point_size.out
				when K_ft_case_value then
					Result := ist + Ks_fmt_tag_case + fmt.case_value.out
				when K_ft_vertical_offset then
					Result := ist + Ks_fmt_tag_offset + fmt.vertical_offset.out
				when K_ft_is_bold then
					Result := ist + Ks_fmt_tag_bold + fmt.is_bold.out
				when K_ft_is_italic then
					Result := ist + Ks_fmt_tag_italic + fmt.is_italic.out
				when K_ft_is_underlined then
					Result := ist + Ks_fmt_tag_underlined + fmt.is_underlined.out
				when K_ft_is_strike_through then
					Result := ist + Ks_fmt_tag_strike_through +
						fmt.is_strike_through.out
				when K_ft_text_color then
					if attached fmt.text_color as tc and then not tc.is_empty then
						Result := ist + Ks_fmt_tag_color + quoted (tc)
					else
						--Result := ist + Ks_fmt_tag_color + "%"%""
						Result := ""
					end
				when K_ft_bg_color then
					if attached fmt.bg_color as bg and then not bg.is_empty then
						Result := ist + Ks_fmt_tag_bg_color + quoted (bg)
					else
						--Result := ist + Ks_fmt_tag_bg_color + "%"%""
						Result := ""
					end
				when K_ft_alignment_value then
					Result := ist + Ks_fmt_tag_align + fmt.alignment_value.out
				when K_ft_margin_left then
					Result := ist + Ks_fmt_tag_margin_left + fmt.left_margin.out
				when K_ft_margin_right then
					Result := ist + Ks_fmt_tag_margin_right + fmt.right_margin.out
				when K_ft_margin_top then
					Result := ist + Ks_fmt_tag_margin_top + fmt.top_margin.out
				when K_ft_margin_bottom then
					Result := ist + Ks_fmt_tag_margin_bottom + fmt.bottom_margin.out
				when K_ft_intext then
					if attached fmt.intext as itx and then not itx.is_empty then
						Result := ist + Ks_fmt_tag_intext + quoted (itx)
					else
						--Result := ist + Ks_fmt_tag_intext + "%"%""
						Result := ""
					end
				when K_ft_pre_text then
					if attached fmt.pre_text as pt and then not pt.is_empty then
						Result := ist + Ks_fmt_tag_pre_text + quoted (pt)
					else
						--Result := ist + Ks_fmt_tag_pre_text + "%"%""
						Result := ""
					end
				when K_ft_num_sep then
					if attached fmt.number_separator as ns
						and then ns /= Ks_dflt_num_sep
					 then
						 Result := ist + Ks_fmt_tag_num_sep + quoted (ns)
					else
						Result := ""
					end
				when K_ft_num_fmt then
					Result := ist + Ks_fmt_tag_num_fmt +
						quoted (num_format_tag (fmt.number_format))
				when K_ft_num_start then
					Result := ist + Ks_fmt_tag_num_start + fmt.number_start.out
				when K_ft_ref_label then
					Result := ist + fmt.reference_as_named_param
				when K_ft_xref_target then
					-- do not also handle K_ft_xref_type case, as the 
					-- output for that is combined with target label
					-- TODO maybe make this is 2-value shorthand?
					-- Or maybe require type and label, then extract the 
					-- label as function?
					-- See notes in format and tag classes
					if fmt.xref_target.is_empty then
						Result := ist + Ks_fmt_tag_xref + quoted ("")
					else
						Result := ist + fmt.xref_target_as_named_param
					end
				else
					Result := ""
				end
			end
		end

--|========================================================================
feature -- Analysis and Conversion
--|========================================================================

	tag_to_parts (v, emsg: STRING): ARRAY [STRING]
			--  Tag string 'v', decomposed into parts
			-- <pfx>,<tag_type>,<stype>,<lvl>,<formats>,<after_text>
			-- If any field has an error, replace field value with "E", 
			-- denoting an error condition
			--
			-- N.B. at present, this routine is NOT used for inline or 
			-- include tags, as they are handled during assembly and
			-- in the preprocessor , respectively
		require
			--is_tag: is_tag_bounded (v)
			is_tag: v.count >= 3 and then
				v.item (1) = '<' and v.index_of ('>', 2) > 2
		local
			ts, pfx, low_pfx, tmark: STRING
			len, plen, sp, ep, spp, etp, epx, fsi: INTEGER
			done, has_spc, long_f, macro_f: BOOLEAN
			tc, c1, c2: CHARACTER
			mpl: LIST [STRING]
		do
			create Result.make_filled ("", 1, 6)
			len := v.count

			spp := v.index_of (' ', 2)
			etp := v.index_of ('>', 2)
			-- etp /= 0 Assured by precodition
			if etp /= len then
				-- Has end text, whether it should or not
				Result.put (v.substring (etp + 1, len), 6)
			end
			if spp /= 0 then
				epx := etp.min (spp)
			else
				epx := etp
				spp := 0
			end
			pfx := tag_prefix (v, emsg)
			low_pfx := pfx.as_lower
			plen := pfx.count
			if pfx ~ "E" then
				Result.put ("E", 1)
				done := True
			else
				-- Prefix is at least somewhat valid
				if low_pfx.item (2) = 'x' and low_pfx.item (3) = ' ' then
					-- Multi-line tag?  Need to capture type markers that 
					-- follow the REQUIRED space separator
					-- If multi hass been unfolded, it no longer appears 
					-- as a multi, so this case must be happening earlier 
					-- in the parsing sequence (e.g. the preprocessor)
					--
					-- Shift to find tag type
					-- "<x p>", "<x hx>", "<x lx">
					-- "<x m>", "<x mx"
					-- "<x fx>"
					-- Multiline open tags 'x' MUST be followed by a space
					-- Multiline open tags MUST NOT have trailing text!
					-- Multiline open tags MUST NOT have format params
					-- Params are on lines between open and close tags
					tmark := v.substring (4, etp - 1)
				elseif low_pfx ~ Ks_usml_tag_prefix then
					Result.put (Ks_usml_tag_prefix, 1)
					long_f := True
					-- Capture tag type marker
					-- <usml tmark
					-- 1     ^sp  ^ep
					sp := Ks_usml_tag_prefix.count + 1
					ep := v.index_of (' ', sp)
					if ep /= 0 and ep < etp then
						has_spc := True
					else
						--ep := v.index_of ('>', sp)
						ep := etp
					end
					if ep = 0 then
						-- ERROR
						Result.put ("E", 2)
						done := True
					else
						tmark := v.substring (sp, ep - 1)
						if has_spc then
							fsi := ep + 1
						end
					end
				else
					-- Tag type marker is terse-form or macro
					--  <tmark
					-- sp^ p  ^ep
					sp := 2
					tmark := v.substring (sp, epx - 1)
					if spp /= 0 and spp < etp then
						fsi := spp + 1
					end
				end
				-- Check type marker for known macro
				if attached tmark then
					if macros.has (tmark) then
						-- "<mymacro","m","mymacro" ...
						macro_f := True
						-- Add standard prefix, as-if
						Result.put (Ks_usml_tag_prefix, 1)
						Result.put ("m", 2)
						Result.put (tmark, 3)
					elseif tmark.as_lower.starts_with (Ks_fmt_tag_macro) then
						-- No type marker seen to this point, so this 
						-- must be a macro invocation (vs a format param)
						-- macro="foo"
						mpl := tmark.split ('=')
						Result.put (Ks_usml_tag_prefix, 1)
						Result.put ("m", 2)
						ts := quotes_stripped (mpl.last)
						Result.put (ts, 3)
					else
						-- Not a macro name; needs to be an actual type marker
						-- At this point, if tag were multi-line, it 
						-- would have been unwrapped during initial read 
						-- from stream, and comments have been stripped by the 
						-- preprocessor, so look for only 'normal' tag types
						tmark := tmark.as_lower
						c1 := tmark.item (1)
						inspect c1
						when 'c' then
							-- comment should have been ripped by 
							-- preprocessor, but this routine can be called 
							-- _by_ the preprocessor, to capture it
							Result.put (pfx, 1)
							Result.put (tmark, 2)
						when 'x' then
							-- Is a multi mark, should have been ripped, above
						when 'p' then
							-- Paragraph; tag type must be unqualified
							if tmark.count /= 1 then
								if long_f then
									Result.put ("E", 2)
								else
									Result.put ("E", 1)
								end
							else
								-- type tag is 'p' only, so OK
								if not long_f then
									Result.put (low_pfx, 1)
								end
								Result.put (c1.out, 2)
								Result.put (c1.out, 3)
							end
						when 'h', 'l' then
							c2 := tmark.item (2)
							if not long_f then
								Result.put (low_pfx, 1)
							end
							if c2.is_digit or c2 = '*' then
								Result.put (c1.out, 2)
								Result.put (c1.out, 3)
								Result.put (c2.out, 4)
							elseif c1 = 'h' then
								inspect c2
								when 't', '+', '-' then
									Result.put (c1.out, 2)
									Result.put (c1.out, 3)
									Result.put (c2.out, 4)
								else
									Result.put ("E", 2)
									done := True
								end
							else
								Result.put ("E", 2)
								done := True
							end
						when 'm', 'f' then
							if not long_f then
								-- TODO, are terse-form macros and formats legal?
								Result.put (low_pfx, 1)
							end
							-- Untyped
							-- ..m name=..
							-- ..macro name=..
							-- Typed
							-- ..mp..
							-- ..mh2..
							-- 
							if tmark.count = 1 or tmark ~ "macro" then
								-- An untyped macro definition,
								-- nothing but formats after this point
								-- ..m name=..
								Result.put (c1.out, 2)
								-- Untyped macro def does not have an stype fld
							else
								-- Only p, h and l structure types are valid
								-- Can be long form macro instance tag
								--  .. macro="foo"
								-- OR might be a typed macro definition
								Result.put (c1.out, 2)
								c2 := tmark.item (2)
								if c2 = 'p' then
									if tmark.count /= 2 then
										Result.put ("E", 3)
										done := True
									else
										Result.put (c2.out, 3)
									end
								elseif c2 = 'h' or c2 = 'l' then
									Result.put (c2.out, 3)
									if tmark.count < 3 then
										Result.put ("E", 3)
										done := True
									else
										tc := tmark.item (3)
										if tc.is_digit then
											Result.put (tc.out, 4)
										else
											inspect tc
											when  '*' then
												-- Formats ok, macros invalid
												if c1 = 'm' then
													Result.put ("E", 4)
													done := True
												else
													Result.put (tc.out, 4)
												end
											when 't' then
												-- Headings ok, macros and formats
												if c2 = 'h' then
													Result.put (c2.out, 3)
													Result.put (tc.out, 4)
												else
													Result.put ("E", 3)
													done := True
												end
											when '+', '-' then
												Result.put ("E", 3)
												done := True
											else
												Result.put ("E", 3)
												done := True
											end
										end
									end
								end -- p, h or l
							end -- 'm', 'f'
						else
							-- Unrecognized or invalid tag type char
							if long_f then
								Result.put ("E", 2)
							else
								Result.put ("E", 1)
							end
						end
					end
				end
			end
			if not done then
				-- Prefix and type fields extracted; levels extracted for 
				-- outline types; trailing text extracted;
				-- All what remains are format params, if any
				if fsi /= 0 then
					-- Move past space to find formats
					Result.put (v.substring (fsi, etp - 1), 5)
				end
			end
		end

	--|--------------------------------------------------------------

	tag_prefix (v, emsg: STRING): STRING
			-- Prefix string from tag string 'v'
			-- "E" if invalid
			-- If a special error msg is needed, copy into 'emsg', else 
			-- Result suffices
		require
			is_tag: v.count >= 3 and then v.item (1) = '<' and v.has ('>')
		local
			len, tlen, spp, etp, epx: INTEGER
			low_v, ts, low_ts, ots: STRING
			tc, tc2: CHARACTER
			err_f, multi_f: BOOLEAN
		do
			Result := "E"
			len := v.count
			-- Precondition asserts that 'v' starts with '<' and has 
			-- closing '>' somewhere, and is at least 3 chars long
			-- so don't check again
			--
			-- Next character defines the tag type
			-- Comment, list, heading and paragraph types are 
			-- allowed as terse forms:  <c, <lx, <hx, <p
			-- as are defined typed macros and include tags ("<include ")
			--
			-- A terse-form multi-line tag begins with "<x" but, unlike 
			-- its long-form counterpart, does not have its type tag 
			-- immeditately followint the 'x'.  Instead, there is an 
			-- intervening space.  This is to make a clear distinction 
			-- between terse multi-line tags and macros that might 
			-- resemble them. valid: "<x p>"  invalid or macro: "<xp>"
			--
			-- Terse forms can be parameter-less:  <p>
			-- or have parameters:  <p color="ff0000">
			-- If tag has parameters, prefix includes space after type
			-- Type markers are case-insensitive, macros are 
			-- case-sensitive

			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_prefix) then
				Result := Ks_usml_tag_prefix
			elseif low_v.starts_with (Ks_usml_tag_image_prefix) then
				Result := Ks_usml_tag_image_prefix
			else
				tc := low_v.item (2)
				spp := v.index_of (' ', 2)
				etp := v.index_of ('>', 2)
				if etp = 0 then
					-- ERROR - checked in precondition
					err_f := True
				else
					if spp = 0 or spp > etp then
						-- Terse tags and macros might not have any blank chars
						-- Label starts immediately after '<'
						spp := 2
						epx := etp
					else
						epx := etp.min (spp)
					end
					ts := v.substring (2, epx - 1)
					if macros.has (ts) then
						Result := v.substring (1, epx)
					elseif low_v.starts_with (Ks_usml_tag_prefix_include) then
						Result := Ks_usml_tag_prefix_include
					elseif epx = 2 then
						-- ERROR "< " or "<>"
						err_f := True
						emsg.append ("Incomplete tag")
					elseif tc = 'x' then
						-- Is a multi-line tag; shift to find tag type
						-- "<x p>", "<x hx>", "<x lx">
						-- "<x m>", "<x mx"
						-- "<x fx>"
						-- Multiline open tags 'x' MUST be followed by a space
						-- Multiline open tags MUST NOT have trailing text!
						-- Multiline open tags MUST NOT have format params
						-- Params are on lines between open and close tags
						if spp /= 3 then
							-- Not a known construct
						else
							-- if etp /= len then
							-- ERROR, multis can't have trailing text,
							-- BUT, the question is "does this start with an 
							-- open tag?" and the answer is actually "yes"
							multi_f := True
							ts := v.substring (4, etp - 1)
							spp := 0
							epx := etp
						end
					end
				end
			end
			if Result ~ "E" and (not err_f)
				and attached ts and then not ts.is_empty
			 then
				 -- Not a macro, nor a long-form tag, nor a terse include
				 low_ts := ts.as_lower
				 tlen := low_ts.count
				 tc := low_ts.item (1)
				 inspect tc
				 when 'h', 'l' then
					 -- "<hx>", "<lx>", "<hx ", "<lx "
					 -- 'low_ts' includes only 'hx' or 'lx'
					 if is_valid_outlined_type_tag (low_ts) then
						 if spp /= 0 and spp < etp then
							 Result := low_v.substring (1, spp)
						 else
							 Result := low_v.substring (1, epx - 1)
						 end
					 else
						 -- ERROR
					 end
				 when 'c', 'p' then
					 -- "<c ", "<p ", "<c>", "<p>" only
					 -- 'low_ts' includes only 'c' or 'p', even in case 
					 -- where tag has space separator
					 if low_ts.count /= 1 then
						 -- ERROR
					 elseif spp /= 0 and spp < etp then
						 -- "<c ", "<p "
						 Result := low_v.substring (1, spp)
					 else
						 -- "<c>", "<p>" only; else error
							 Result := low_v.substring (1, epx - 1)
					 end
				when 'm' then
					-- "<m ", "<mp ", "<mhx ", "<mlx "
					-- "<x m>", "<x mp>", "<x mhx>", "<x mlx>"
					 -- 'low_ts' includes only chars, no space of brace
					if multi_f or (spp /= 0 and spp < etp) then
						-- Terse macros must have separator,
						-- but a multi-line macro open tag will not
						if tlen = 1 then
							-- Untyped
							if multi_f then
								-- "<x m>"
								Result := low_v.substring (1, etp - 1)
							else
								-- "<m ..."
								Result := low_v.substring (1, spp)
							end
						else
							-- Typed (and tlen > 1)
							tc2 := low_ts.item (2)
							inspect tc2
							when 'p' then
								if low_ts.count = 2 then
									if multi_f then
										-- capture whole prefix
										Result := low_v.substring (1, etp - 1)
									else
										Result := low_v.substring (1, spp)
									end
								else
									-- ERROR
								end
							when 'h', 'l' then
								ots := low_ts.substring (2, low_ts.count)
								if is_valid_outlined_type_tag (ots) then
									if multi_f then
										-- capture whole prefix
										Result := low_v.substring (1, etp - 1)
									else
										Result := low_v.substring (1, spp)
									end
								end
							else
								-- Could be a tag-like construct, but not 
								-- legal USML (e.g. "<meta")
								-- ERROR
							end
						end
					end
				when 'f' then
					-- "<fp ", "<fhx ", "<flx "
					-- "<x fp>", "<x fhx>", "<x flx>"
					-- 'low_ts' includes only chars, no space of brace
					-- For multi, includes only type marker part, not the 
					-- leading 'x' part
					if multi_f or (spp /= 0 and spp < etp) then
						-- Terse format must have separator,
						-- but a multi-line format open tag will not
						if low_ts.count >= 2 then
							tc2 := low_ts.item (2)
							inspect tc2
							when 'p' then
								if low_ts.count = 2 then
									if multi_f then
										-- capture whole prefix
										Result := low_v.substring (1, etp - 1)
									else
										Result := low_v.substring (1, spp)
									end
								else
									-- ERROR
								end
							when 'h', 'l' then
								ots := low_ts.substring (2, low_ts.count)
								if is_valid_outlined_type_tag (ots) then
									if multi_f then
										-- capture whole prefix
										Result := low_v.substring (1, etp - 1)
									else
										Result := low_v.substring (1, spp)
									end
								end
							else
								-- Could be a tag-like construct, but not 
								-- legal USML (e.g. "<forward")
								-- ERROR
							end
						end
					end
				else
					-- ERROR
				end
			end
		end

	--|--------------------------------------------------------------

	is_tag_bounded (v: STRING): BOOLEAN
			-- Is 'v' bounded by tag open and close chars?
			-- Must also have at least one character to be bound between 
			-- the end points
		do
			if v.count > 2 and then v.item (1) = '<' then
				if v.item (v.count) = '%R' then
					Result := v.item (v.count - 1) = '>'
				else
					Result := v.item (v.count) = '>'
				end
			end
		end

	--|--------------------------------------------------------------

	is_tag_like (v: STRING): BOOLEAN
			-- Does 'v' appear to be a tag?
			-- Must start with a '<' and have an unquoted '>' and have 
			-- at least on character in between them
			-- Must also have at least one character to be bound between 
			-- the end points
		do
			if v.count > 2 and then v.item (1) = '<' then
				Result := next_rb_pos (v, 2) > 2
			end
		end

	--|--------------------------------------------------------------

	--RFO tag_format_list (v: STRING; spos: INTEGER): LINKED_LIST [STRING]
	--RFO 		-- Formats from tag string 'v', beginning a 'spos'
	--RFO 	require
	--RFO 		is_tag: is_tag_bounded (v)
	--RFO 		valid_start: spos > 0 and spos <= v.count
	--RFO 	local
	--RFO 		ep: INTEGER
	--RFO 		ts: STRING
	--RFO 	do
	--RFO 		create Result.make
	--RFO 		ep := v.count - 1
	--RFO 		-- <... <fmt>=" ...
	--RFO 		--      ^ spos
	--RFO 		ts := v.substring (spos, ep)
	--RFO 		Result.fill (ts.split (';'))
	--RFO 	end

	--|--------------------------------------------------------------

	--RFO tag_formats (v: STRING; spos: INTEGER): STRING
	--RFO 		-- Formats from tag string 'v', beginning at 'spos'
	--RFO 	require
	--RFO 		is_tag: is_tag_bounded (v)
	--RFO 		valid_start: spos > 0 and spos <= v.count
	--RFO 	local
	--RFO 		ep: INTEGER
	--RFO 	do
	--RFO 		ep := v.count - 1
	--RFO 		-- <... <fmt>=" ...
	--RFO 		--      ^ spos
	--RFO 		Result := v.substring (spos, ep)
	--RFO 	end

	--|--------------------------------------------------------------

	starts_with_tag (v: STRING): BOOLEAN
			-- Does string 'v' begins with a VALID USML tag construct?
			-- Opening tag
			--   "<usml "
			--   "<p>", "<c>", "<p ", "<c "
			--   "<hx>", "<lx>", "<hx ", "<lx "
			-- OR closing tag (not inline)
			--   "</usml x>"
			--   "</x>"
		local
			low_v: STRING
		do
			if v.starts_with ("</") then
				low_v := v.as_lower
				-- NOT SAME AS BEING LEGAL CLOSE
				if low_v.starts_with (Ks_usml_tag_x_close_u) then
					-- "</usml x>"
				elseif low_v.starts_with (Ks_usml_tag_x_close) then
					-- "</x>"
				end
			else
				Result := starts_with_open_tag (v, True)
			end
		end

	--|--------------------------------------------------------------

	ends_with_tag_close (v: READABLE_STRING_GENERAL; sf: BOOLEAN): BOOLEAN
			-- Does 'v' end with a closing '>'?
			-- If 'sf' then use strict check, else allow for trailing 
			-- '%R'
		local
			len: INTEGER
		do
			len := v.count
			if len >= 2 then
				if v.item (len) = '>' then
					Result := True
				elseif not sf then
					if v.item (len) = '%R' and v.item (len - 1) = '>' then
						Result := True
					end
				end
			end
		end

	--|--------------------------------------------------------------

	starts_with_open_tag (v: STRING; sf: BOOLEAN): BOOLEAN
			-- Does string 'v' begins with a valid USML opening tag 
			-- construct?  Needs to begin with a tag and have a valid prefix.
			-- N.B. Does NOT imply that the whole line is valid!!!
			-- "<usml "
			-- "<p>", "<c>", "<p ", "<c "
			-- "<hx>", "<lx>", "<hx ", "<lx "
			-- "<image "
			-- "<x "
			-- "<mymacro "
			--
			-- 'sf' is 'strict' flag
			-- Rules:
			-- IF 'sf', then True if 'v' starts with '<' AND is followed
			--    immediately by a known tag label (incl. known macro)
			-- IF NOT 'sf' then True if starts with '<' AND is followed
			--    immediately by any ALPHA char (assumed to be a macro)
		local
			len: INTEGER
			--tc, tc2: CHARACTER
			--low_v, ts, 
			pfx, emsg: STRING
		do
			len := v.count
			if len >= 3 and then v.item (1) = '<' then
				if next_rb_pos (v, 2) > 2 then
					-- prefix includes leading '<'
					create emsg.make (0)
					pfx := tag_prefix (v, emsg)
					if pfx ~ "E" then
						if sf then
							-- done
						else
							-- non-strict, check for identifier after '<'
							Result := v.item (2).is_alpha
						end
					else
						-- TODO prefix being valid should be sufficient to 
						-- answer the query as True
						Result := True
					end
				end
			end
		end

	--|--------------------------------------------------------------

	starts_with_xopen_tag (v: READABLE_STRING_GENERAL; emsg: STRING): BOOLEAN
			-- Does line 'v' begin with a multiline opening tag?
			-- Does NOT imply a fully valid tag
			-- BUT, if it appears to be an xopen tag (vs a totally 
			-- different pattern), but has an error, the 
			-- write error msg into emsg, but still True
		local
			low_v: STRING
			spp: INTEGER
		do
			low_v := v.as_lower.as_string_8
			if low_v.starts_with (Ks_usml_tag_x_open_prefix_u) or
				low_v.starts_with (Ks_usml_tag_x_open_prefix)
			 then
				 Result := True
				if low_v.starts_with (Ks_usml_tag_x_open_prefix) then
					spp := Ks_usml_tag_x_open_prefix.count
				else
					spp := Ks_usml_tag_x_open_prefix_u.count
				end
				if not ends_with_tag_close (v, False) then
					emsg.append (
						"Multi-line tag do not permit trailing text")
				end
			end
		end

	--|--------------------------------------------------------------

	is_image_open_tag (v: STRING): BOOLEAN
			-- Does line 'v' begin with an image opening tag?
			-- <usml x image..>
			-- <usml image..>
			-- <image..>
			-- All images are multi-line and are terminated in the same 
			-- manner as other multi-line tags, but images do not 
			-- require an explicitly multiline opening tag
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_image_prefix) or
				low_v.starts_with (Ks_usml_tag_image_prefix_u)
			 then
				 Result := True
			end
		end

	--|--------------------------------------------------------------

	is_xcomment_open_tag (v, emsg: STRING): BOOLEAN
			-- Does line 'v' begin with a multiline comment opening tag 
			-- and contain nothing but the tag?
			-- If an error is found (but is otherwise true, 
			-- versus not being anything like it),
			-- write error into 'emsg'
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_xcomment) or
				low_v.starts_with (Ks_usml_tag_xcomment_u)
			 then
				 -- So far so good, but does not permit trailing text,
				 -- so, while it's an xcomment tag, it's a bad one
				 Result := True
				 --TODO shouldn't this be handled elsewhere?
--RFO 				if not ends_with_tag_close (v, False) then
--RFO 					emsg.append (
--RFO 						"Trailing text not allowed in multi-line tags")
--RFO 				end
			end
		end

	--|--------------------------------------------------------------

	is_comment_tag (v: STRING): BOOLEAN
			-- Does line 'v' begin with a sinle-line comment tag?
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_comment) or
				low_v.starts_with (Ks_usml_tag_comment_u) then
					-- OK to have trailing text
					Result := True
			end
		end

	--|--------------------------------------------------------------

	is_verbatim_open_tag (v: STRING): BOOLEAN
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_v_open_prefix_u) or
				low_v.starts_with (Ks_usml_tag_v_open_prefix)
			 then
--TODO look harder
				 Result := True
			end
		end

	is_verbatim_close_tag (v: STRING): BOOLEAN
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_v_close_u) or
				low_v.starts_with (Ks_usml_tag_v_close)
			 then
--TODO look harder
				 Result := True
			end
		end

	--|--------------------------------------------------------------

	starts_with_include_tag (v, parg: STRING): BOOLEAN
			-- Is string 'v' a valid USML include tag?
			-- Must also NOT have trailing text between tag and end or 
			-- tag and newline
			-- "<usml include path>"
			-- "<include path>"
			-- path needs to be a quoted, non-empty string
			-- If path exists and is valid, add to 'parg'
		local
			len, ep, sp, np: INTEGER
			ts, low_ts: STRING
		do
			len := v.count
			if len >= 3 and then v.item (1) = '<' then
				ep := next_rb_pos (v, 2)
				np := v.index_of ('%N', 1)
				if ep > 2 and (np = 0 or ep < np) then
					ts := v.substring (1, ep)
					len := ts.count
					low_ts := ts.as_lower
					if low_ts.starts_with (Ks_usml_tag_prefix_include_u) then
						sp := Ks_usml_tag_prefix_include_u.count + 1
					elseif low_ts.starts_with (Ks_usml_tag_prefix_include) then
						sp := Ks_usml_tag_prefix_include.count + 1
					end
					if sp < len then
						-- Check path param
						-- Needs to be quoted, non-empty string
						ts := ts.substring (sp, len - 1)
						if ts.count >= 3 then
							if ts.item (1) = '%"' and ts.item (ts.count) = '%"' then
								parg.append (quotes_stripped (ts))
								Result := True
							end
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	starts_with_x_close (v: READABLE_STRING_GENERAL): BOOLEAN
			-- Does string 'v' at least begin as a multiline closing tag?
			-- Does NOT imply a fully valid tag, but does ensure that at 
			-- least the line begins with an x close tag, leaving deeper 
			-- analysis to others.
			-- For a more definitive is or is-not, use is_x_close
		local
			low_v: STRING
		do
			low_v := v.as_lower.as_string_8
			Result := low_v.starts_with (Ks_usml_tag_x_close)
			if not result then
				Result := low_v.starts_with (Ks_usml_tag_x_close_u)
			end
		end

	--|--------------------------------------------------------------

	starts_with_x_close_at_pos (v: READABLE_STRING_GENERAL; spos: INTEGER): BOOLEAN
			-- Does string 'v' at least begin as a multiline closing 
			-- tag, starting at 'spos'?
		local
			low_v, ts: STRING
			len, tlen: INTEGER
		do
			len := 1 + v.count - spos
			if len >= Ks_usml_tag_x_close.count then
				tlen := Ks_usml_tag_x_close.count
			elseif len >= Ks_usml_tag_x_close_u.count then
				tlen := Ks_usml_tag_x_close_u.count
			end
			if tlen /= 0 then
				ts := v.substring (spos, spos + tlen - 1).as_string_8
				low_v := ts.as_lower
				Result := low_v.starts_with (Ks_usml_tag_x_close)
				if not Result then
					Result := low_v.starts_with (Ks_usml_tag_x_close_u)
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO next_x_close_end (v: STRING; spos: INTEGER): INTEGER
	--RFO 		-- Position, in 'v', of LAST CHAR in next multiline closing
	--RFO 		-- tag, if any, AFTER 'spos' (must be AFTER spos because
	--RFO 		-- closing must begin at start of line, so context must 
	--RFO 		-- include the preceding newline char.)
	--RFO 		-- ...
	--RFO 		-- </usml x>
	--RFO 		--         ^Result
	--RFO 		-- If none, found then Result is zero
	--RFO 	local
	--RFO 		sp, ep, lim: INTEGER
	--RFO 		ts, low_ts: STRING
	--RFO 	do
	--RFO 		lim := v.count
	--RFO 		from sp := next_lb_pos (v, spos)
	--RFO 		until sp = 0 or sp >= lim or Result /= 0
	--RFO 		loop
	--RFO 			if sp > spos then
	--RFO 				if v.item (sp - 1) = '%N' then
	--RFO 					ep := next_rb_pos (v, sp)
	--RFO 					if ep /= 0 then
	--RFO 						ts := v.substring (sp, ep)
	--RFO 						low_ts := ts.as_lower
	--RFO 						if low_ts ~ Ks_usml_tag_x_close_u then
	--RFO 							--   </usml x>
	--RFO 							-- sp^      ^Result
	--RFO 							Result := sp + Ks_usml_tag_x_close_u.count - 1
	--RFO 						elseif low_ts ~ Ks_usml_tag_x_close then
	--RFO 							--   </x>
	--RFO 							-- sp^  ^Result
	--RFO 							Result := sp + Ks_usml_tag_x_close.count - 1
	--RFO 						else
	--RFO 							-- Keep looking
	--RFO 							sp := ep
	--RFO 						end
	--RFO 					else
	--RFO 						-- No closing '>' means there ain't no closing 
	--RFO 						-- tag anywhere in stream; done looking
	--RFO 						sp := lim
	--RFO 					end
	--RFO 				else
	--RFO 					-- Was not preceded by a newline; keep looking
	--RFO 					sp := sp + 1
	--RFO 				end
	--RFO 			else
	--RFO 				-- No opening '<'; nothing in stream
	--RFO 				sp := lim
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	is_x_close (v: STRING): BOOLEAN
			-- Is string 'v' a multiline closing tag?
			-- Must be _exactly_ and _only_ a valid closing tag
		local
			low_v: STRING
		do
			if is_tag_bounded (v) then
				low_v := v.as_lower
				-- Need to allow for trailing %R (if from raw files)
				low_v.prune_all_trailing ('%R')
				Result := low_v ~ Ks_usml_tag_x_close
					or low_v ~ Ks_usml_tag_x_close_u
			end
		end

	--|--------------------------------------------------------------

	--RFO num_tag_pairs (v: STRING; spos: INTEGER): INTEGER
	--RFO 		-- Number of '<' '>' pairs in 'v', starting at 'spos'
	--RFO 	require
	--RFO 		valid_start: spos > 0 and spos <= v.count
	--RFO 	local
	--RFO 		i, lim: INTEGER
	--RFO 		lbf: BOOLEAN
	--RFO 		c: CHARACTER
	--RFO 	do
	--RFO 		lim := v.count
	--RFO 		from i := spos
	--RFO 		until i > lim
	--RFO 		loop
	--RFO 			c := v.item (i)
	--RFO 			if c = '>' then
	--RFO 				if lbf then
	--RFO 					Result := Result + 1
	--RFO 					lbf := False
	--RFO 				end
	--RFO 			elseif c = '<' then
	--RFO 				lbf := True
	--RFO 			end
	--RFO 			i := i + 1
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	--RFO next_tag_bounds (v: STRING; spos: INTEGER): ARRAY [INTEGER]
	--RFO 		-- Start and end indices, in 'v' of range bounded by '<' and 
	--RFO 		-- '>' (unquoted), at or after 'spos'
	--RFO 		-- If not found, both values of Result are zer0
	--RFO 	local
	--RFO 		sp, ep: INTEGER
	--RFO 	do
	--RFO 		Result := {ARRAY [INTEGER]}<< 0, 0 >>
	--RFO 		sp := next_lb_pos (v, spos)
	--RFO 		if sp /= 0 then
	--RFO 			ep := next_rb_pos (v, sp)
	--RFO 			if ep /= 0 then
	--RFO 				Result.put (sp, 1)
	--RFO 				Result.put (ep, 2)
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	xref_target_to_parts (v: STRING): LIST [STRING]
			-- Xref target tag/value pair, decomposed into value parts
			-- Parts include (optional) type, and label
		local
			tl: LIST [STRING]
			ll: LINKED_LIST [STRING]
			ts: STRING
		do
			if v.is_empty then
				create ll.make
				Result := ll
			else
				tl := v.split (',')
				if tl.count = 2 then
					Result := tl
				else
					create ll.make
					ll.extend ("#")
					ts:= tl.first
					if ts.item (1) = '#' then
						ts := ts.substring (2, ts.count)
					end
					ll.extend (ts)
					Result := ll
				end
			end
		end

	--|--------------------------------------------------------------

	xref_label_to_type (v: STRING): INTEGER
			-- Type constant corresponding to xref label 'v'
			-- If not value, then -1
		do
			Result := -1
			if not v.is_empty then
				if v ~ Ks_xref_type_anchor then
					Result := K_xref_type_anchor
				elseif v ~ Ks_xref_type_emb_img then
					Result := K_xref_type_emb_img
				elseif v ~ Ks_xref_type_ext_img then
					Result := K_xref_type_ext_img
				elseif v ~ Ks_xref_type_pbuf then
					Result := K_xref_type_pbuf
				elseif v ~ Ks_xref_type_url then
					Result := K_xref_type_url
				elseif v ~ Ks_xref_type_file then
					Result := K_xref_type_file
				elseif v ~ Ks_xref_type_action then
					Result := K_xref_type_action
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO xref_type_to_label (v: INTEGER): STRING
	--RFO 	do
	--RFO 		inspect v
	--RFO 		when K_xref_type_anchor then
	--RFO 			Result := Ks_xref_type_anchor
	--RFO 		when K_xref_type_ext_img then
	--RFO 			Result := Ks_xref_type_ext_img
	--RFO 		when K_xref_type_emb_img then
	--RFO 			Result := Ks_xref_type_emb_img
	--RFO 		when K_xref_type_pbuf then
	--RFO 			Result := Ks_xref_type_pbuf
	--RFO 		when K_xref_type_url then
	--RFO 			Result := Ks_xref_type_url
	--RFO 		when K_xref_type_file then
	--RFO 			Result := Ks_xref_type_file
	--RFO 		when K_xref_type_action then
	--RFO 			Result := Ks_xref_type_action
	--RFO 		else
	--RFO 			Result := Ks_xref_type_anchor
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	section_type_to_level (v: INTEGER): INTEGER
		do
			if is_valid_outlined_type (v) then
				if is_valid_usml_list_type (v) then
					Result := v - K_usml_tag_list_bias
				elseif is_valid_usml_heading_type (v) then
					if v /= K_usml_tag_title then
						Result := v
					end
				end
			end
		end

	--|--------------------------------------------------------------

	section_type_to_label (v: INTEGER): STRING
		do
			inspect v
			when K_usml_tag_p then
				Result := Ks_tag_label_p
			when K_usml_tag_c then
				-- not really a section type, but 
				Result := Ks_tag_label_c
			when K_usml_tag_title then
				Result := Ks_tag_label_title
			when K_usml_tag_v_open, K_usml_tag_v_close then
				Result := Ks_tag_label_v
			else
				if is_valid_usml_heading_type (v) then
					Result := heading_type_labels.item (v)
				elseif is_valid_usml_list_type (v) then
					Result := list_type_labels.item (v - K_usml_tag_list_bias)
				else
					Result := "UNRECOGNIZED SECTION TYPE"
				end
			end
		end

	--|--------------------------------------------------------------

	section_type_to_long_label (v: INTEGER): STRING
		do
			inspect v
			when K_usml_tag_p then
				Result := Ks_tag_long_label_p
			when K_usml_tag_c then
				-- not really a section type, but 
				Result := Ks_tag_long_label_c
			when K_usml_tag_title then
				Result := Ks_tag_long_label_title
			when K_usml_tag_v_open then
				Result := Ks_tag_long_label_v_open
			when K_usml_tag_v_close then
				Result := Ks_tag_long_label_v_close
			else
				if is_valid_usml_heading_type (v) then
					Result := heading_type_long_labels.item (v)
				elseif is_valid_usml_list_type (v) then
					Result := list_type_long_labels.item (v - K_usml_tag_list_bias)
				else
					Result := "UNRECOGNIZED SECTION TYPE"
				end
			end
		end

	--|--------------------------------------------------------------

	hex_8_bit_to_integer (v: STRING): INTEGER
			-- Integer value represented by 2-digit hex string 'v'
		require
			small_enough: v.count <= 2
		local
			c: CHARACTER
		do
			c := v.item (1)
			if c.is_digit then
				Result := (c |-| '0')
			else
				Result := (c.upper |-| 'A') + 10
			end
			if v.count = 2 then
				Result := (Result * 16)
				c := v.item (2)
				if c.is_digit then
					Result := Result + (c |-| '0')
				else
					Result := (Result + c.upper |-| 'A') + 10
				end
			end
		end

	--|--------------------------------------------------------------

	next_line_iq (str: READABLE_STRING_GENERAL; lp: CELL [INTEGER]): STRING
			-- Substring from the string 'str' that includes
			-- the characters up to but NOT including the next
			-- newline character (quoted or NOT).
			-- The line position is stored in 'lp' such that it denotes
			-- the read start position on entry and one character past
			-- the end position on exit (aka start of next read).
			-- If there are no more characters between the starting
			-- line position and the end of the string, then Result
			-- is empty and the line position is set to zero.
			-- If the end of the string is found before a newline, then
			-- the characters from the starting point to the end of
			-- the string are returned and the line position set to
			-- one greater than the length of the string.
		require
			stream_exists: str /= Void
			position_exists: lp /= Void
			valid_start: lp.item > 0 and lp.item <= str.count
		local
			spos, epos: INTEGER
		do
--TODO looks like we're keeping newlines, contrary to hdr comment
			spos := lp.item
			epos := str.index_of ('%N', spos)
			if epos = 0 then
				-- Not found
				epos := str.count + 1
			end
			if epos = spos then
				-- An empty line (but a line just the same)
				Result := ""
				lp.put (epos + 1)
			else
				Result := str.substring (spos, epos - 1).to_string_8
				if Result.is_empty then
					lp.put (0)
				else
					lp.put (epos + 1)
				end
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	image_from_stream (v: READABLE_STRING_GENERAL; emsg: STRING; pc: CELL [INTEGER]): STRING
			-- From stream 'v', all text between 'pc.item' and the next 
			-- multiline closing tag.
		local
			spos, sp, ep, lim, l1, l2: INTEGER
			done: BOOLEAN
		do
			Result := ""
			spos := pc.item
			lim := v.count
			from sp := spos
			until done or sp > lim or sp = 0 or not emsg.is_empty
			loop
				ep := v.index_of ('<', sp)
				if ep = 0 then
					-- Nothing there
					emsg.append ("Embedded image missing closing tag")
				else
--TODO check that '<' was first char in line
					if starts_with_x_close_at_pos (v, ep) then
						ep := ep - 1
						done := True
						Result := v.substring (spos, ep).as_string_8
						sp := ep
						ep := v.index_of ('>', sp)
						if ep /= 0 then
							-- Set ep past closing tag and %R%N
							ep := ep + 2
						else
							-- ERROR
						end
						pc.put (ep)
					else
						-- keep looking
						sp := ep + 1
					end
				end
			end
			if not Result.is_empty then
				-- On a Windows system (at least), and extra %R (Ctl-M) 
				-- appears at the end of each line;  Purge them all, 
				-- replacing the %R%N sequence with %N
				l1 := Result.count
				Result.replace_substring_all ("%R%N", "%N")
				l2 := Result.count
			end
		end

	--|--------------------------------------------------------------

	stream_to_plain_text (v: READABLE_STRING_GENERAL; nf: BOOLEAN): STRING
			-- Text stream 'v', possibly including CRLF pairs
			-- and embedded images, converted to plain text suitable
			-- for showing in a vanilla text widget
			-- CRLFs converted to simple newlines,
			-- Emedded images removed
			-- DOES NOT handle includes, macros, etc.
			--
			-- If 'nf', then each line of output is numbered WITH ITS 
			-- ORIGINAL LINE NUMBER
		local
			sp, lim, psp, ilc, cc, lno: INTEGER
			pcell: CELL [INTEGER]
			line, ts, emsg: STRING
			itag: USML_IMAGE_TAG
		do
			lim := v.count
			create Result.make (lim)
			create pcell.put (1)
			create emsg.make (0)
			from sp := 1
			until sp > lim or sp = 0 or not emsg.is_empty
			loop
				pcell.put (sp)

				lno := lno + 1
				line := next_line_iq (v, pcell)
				emsg.wipe_out
				if is_image_open_tag (line) then
					-- Get image content
					create itag.make_with_text (line)
					if emsg.is_empty then
						psp := pcell.item
						ts := image_from_stream (v, emsg, pcell)
						ilc := ts.occurrences ('%N')
						cc := pcell.item - psp
						-- ts includes trailing newline
						-- Replace image text with marker (comment)
						if nf then
							Result.append (aprintf (Ks_lineno_pf_fmt, << lno >>))
						end
						Result.append (aprintf (
							"<c>Embedded Image [%%s] Removed; %%d lines, %%d chars%N",
							<< itag.name, ilc, cc >>))
						-- need to account for closing tag too
						lno := lno + ilc + 1
					else
						Result.append (
							"<c>ERROR parsing embedded image%N")
					end
				else
					-- non-image line
					ts := asr.dos_to_unix (line)
					ts.prune_all_trailing ('%R')
					if nf then
						Result.append (aprintf (Ks_lineno_pf_fmt, << lno >>))
					end
					Result.append (ts)
					if pcell.item <= lim then
						Result.append ("%N")
					end
				end
				sp := pcell.item
			end
		end

	--|--------------------------------------------------------------

	stream_eol_converted_text (v: STRING): STRING
			-- Text stream 'v', possibly including CRLF pairs
			-- converted to have only %N for newlines (no %Rs)
		local
			sp, lim: INTEGER
			pcell: CELL [INTEGER]
			line, ts: STRING
		do
			lim := v.count
			create Result.make (lim)
			create pcell.put (1)
			from sp := 1
			until sp > lim or sp = 0
			loop
				pcell.put (sp)

				line := next_line_iq (v, pcell)
				ts := asr.dos_to_unix (line)
				ts.prune_all_trailing ('%R')
				Result.append (ts)
				if pcell.item <= lim then
					Result.append ("%N")
				end
				sp := pcell.item
			end
		end

	--|--------------------------------------------------------------

	stream_numbered (v: STRING): STRING
			-- Text stream 'v' (already cleaned), with leading line 
			-- numbers added
		local
			sp, lim, lno: INTEGER
			pcell: CELL [INTEGER]
			line, ts: STRING
		do
			lim := v.count
			create Result.make (lim)
			create pcell.put (1)
			from sp := 1
			until sp > lim or sp = 0
			loop
				pcell.put (sp)
				lno := lno + 1
				line := next_line_iq (v, pcell)
				ts := asr.dos_to_unix (line)
				ts.prune_all_trailing ('%R')
				Result.append (aprintf (Ks_lineno_pf_fmt, << lno >>))
				Result.append (ts)
				if pcell.item <= lim then
					Result.append ("%N")
				end
				sp := pcell.item
			end
		end

	--|--------------------------------------------------------------

	stream_numbers_stripped (v: STRING): STRING
			-- Text stream 'v', stripped of leading line numbers
		local
			sp, lim: INTEGER
			pcell: CELL [INTEGER]
			line: STRING
		do
			lim := v.count
			create Result.make (lim)
			create pcell.put (1)
			from sp := 1
			until sp > lim or sp = 0
			loop
				pcell.put (sp)
				line := next_line_iq (v, pcell)
				Result.append (line.substring (K_lineno_width + 1, line.count))
				if pcell.item <= lim then
					Result.append ("%N")
				end
				sp := pcell.item
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_outline_instance (v: INTEGER): BOOLEAN
		do
			Result := v > 0 and v <= K_max_outline_instance
		end

	is_valid_outlined_type_tag (v: STRING): BOOLEAN
			-- Is 'v' a valid heading or list item type tag?
		local
			c1, c2: CHARACTER
		do
			if v.count = 2 then
				c1 := v.item (1).as_lower
				c2 := v.item (2).as_lower
				if c2.is_digit and c2 /= '0' then
					Result := True
				elseif c2 = '*' then
					Result := True
				elseif c1 = 'h' then
					inspect c2
					when 't', '+', '-' then
						Result := True
					else
					end
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_xref (v: STRING): BOOLEAN
			-- Is 'v' a syntactically valid cross-reference param
			-- NOTE WELL 'v' does NOT include xref= tag, nor is it quoted
			-- It CAN include a comma-separated type and label
			-- OR, just a label
		local
			xt: INTEGER
			ts, rs: STRING
			xpl: LIST [STRING]
		do
			if not v.is_empty then
				xpl := xref_target_to_parts (v)
				if xpl.count = 2 then
					ts := xpl.first
					rs := xpl.last
					xt := xref_label_to_type (ts)
					if is_valid_xref_type (xt) then
						Result := is_valid_xref_target (rs, xt)
					end
				elseif xpl.count = 1 then
					-- Label only form
					rs := xpl.first
					Result := is_valid_xref_target (rs, K_xref_type_anchor)
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_xref_target (v: STRING; tp: INTEGER): BOOLEAN
			-- Is 'v' a syntactically valid xref target label?
		do
			if not v.is_empty then
--TODO
				-- Needs to be proper identifier, path, etc. depending on 
				-- anchor type
				Result := not v.is_empty
			end
		end

--RFO 	is_valid_xref_type_label (v: STRING): BOOLEAN
--RFO 		do
--RFO 			if not v.is_empty then
--RFO 				Result := is_valid_xref_type (xref_label_to_type (v))
--RFO 			end
--RFO 		end

	is_valid_xref_type (v: INTEGER): BOOLEAN
		do
			inspect v
			when
				K_xref_type_anchor, K_xref_type_emb_img,
				K_xref_type_ext_img, K_xref_type_pbuf,
				K_xref_type_url, K_xref_type_file, K_xref_type_action
			 then
				 Result := True
			else
			end
		end

	--|--------------------------------------------------------------

	next_inline_close_start (v: STRING; spos: INTEGER; emsg: STRING): INTEGER
			-- Index, within 'v', at which the next STANDARD-FORM
			-- inline closing tag occurs (if any)
			--
			-- N.B. inline tags CAN span multiple lines, even though 
			-- other tags must begin at the start of a line
		do
			if attached next_iclose_candidate (v, spos) as icc then
				Result := icc.tag_start
			end
		end

	--|--------------------------------------------------------------

	next_inline_tag_start (v: STRING; spos: INTEGER): INTEGER
			-- Index, within 'v', at which the next inline tag,
			-- whether open or close, standard or mirrored
			--
			-- N.B. inline tags CAN span multiple lines, even though 
			-- other tags must begin at the start of a line
		require
			valid_start: spos > 0 and spos <= v.count
		local
			sp, lim: INTEGER
		do
			lim := v.count
			from sp := spos
			until Result /= 0 or sp = 0 or sp > lim
			loop
				sp := next_lb_pos (v, sp)
				if sp /= 0 then
					if is_inline_open (v, sp) or is_inline_close (v, sp) then
						Result := sp
					else
						-- might be extraneous '<' char in stream; keep looking
						sp := sp + 1
					end
				end
			end
		end

	--|--------------------------------------------------------------

	next_iclose_candidate (v: STRING; spos: INTEGER): detachable USML_INLINE_CLOSE
			-- Next POTENTIAL inline closing tag, in 'v',
			-- at or after 'spos', if any
			-- Inline close tag can be standard ("</usml>" or 
			-- "</inline>") or mirrored where mirrored is a mirror of
			-- a terse macro opening tag
			-- N.B. Result is merely a candidate, and, if mirrored, must 
			-- be qualified by client (e.g. by matching with open)
			--
			-- N.B. inline tags CAN span multiple lines, even though 
			-- other tags must begin at the start of a line
		local
			sp, ep, tp, lim: INTEGER
		do
			lim := v.count
			from sp := next_lbc_pos (v, spos)
			until sp = 0 or sp >= lim or attached Result
			loop
				ep := next_rb_pos (v, sp)
				if ep = 0 then
					-- No closing '>' means not a candidate
					sp := lim
				elseif ep - sp <= 2 then
					-- "</>", keep looking
					sp := next_lbc_pos (v, sp + 1)
				else
					tp := next_lbc_pos (v, sp + 1)
					if tp = 0 then
						create Result.make_with_bounds (v, sp, ep)
						-- Done
						sp := 0
					elseif tp < ep then
						-- another open candidate exists in substring
						-- keep looking
						sp := tp
					else
						create Result.make_with_bounds (v, sp, ep)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO matching_iclose (v, emsg: STRING; ot: USML_INLINE_OPEN): detachable USML_INLINE_CLOSE
	--RFO 		-- Inline closing tag corresponding to inline open 'ot' whose 
	--RFO 		-- starting position, in 'v', is 'spos'
	--RFO 		-- Close MUST be mirror or open. e.g. if open is "<inline.." 
	--RFO 		-- then close must be "</inline>".  If open is a macro inline
	--RFO 		-- and has a non-empty macro label (i.e. legal), then
	--RFO 		-- close must be mirrored.  e.g. "<foo.." must close with "</foo>"
	--RFO 		-- If no match is found, then Result is Void
	--RFO 		--
	--RFO 		-- N.B. inline tags CAN span multiple lines, even though 
	--RFO 		-- other tags must begin at the start of a line
	--RFO 	require
	--RFO 		is_tag: is_inline_tag (v, ot.tag_start)
	--RFO 		no_error: emsg.is_empty
	--RFO 	local
	--RFO 		spos, sp, ep, lim, oc: INTEGER
	--RFO 	do
	--RFO 		--TODO
	--RFO 		-- NEEDS to check match after successful is_inline_close
	--RFO 		lim := v.count
	--RFO 		spos := ot.tag_start
	--RFO 		sp := ot.tag_end + 1
	--RFO 		from sp := next_lb_pos (v, sp)
	--RFO 		until sp = 0 or sp >= lim or attached Result or not emsg.is_empty
	--RFO 		loop
	--RFO 			if is_inline_tag (v, sp) then
	--RFO 				-- push the open count; keep looking
	--RFO 				oc := oc + 1
	--RFO 				sp := next_lb_pos (v, sp + 1)
	--RFO 			elseif is_inline_close (v, sp) then
	--RFO 				if oc = 0 then
	--RFO 					ep := next_rb_pos (v, sp)
	--RFO 					if ep /= 0 then
	--RFO 						if attached macros.item (ts) then
	--RFO 							Result := True
	--RFO 						end
	--RFO 						create Result.make_with_bounds (v, sp, ep)
	--RFO 					else
	--RFO 						-- ERROR, unbalanced inline tags
	--RFO 						emsg.append ("Unbalanced inline open and close tags")
	--RFO 					end
	--RFO 				else
	--RFO 					-- Not a match; pop the open count; keep looking
	--RFO 					oc := oc - 1
	--RFO 					sp := next_lb_pos (v, sp + 1)
	--RFO 				end
	--RFO 			else
	--RFO 				-- a '<' but not a tag; keep looking
	--RFO 				sp := next_lb_pos (v, sp + 1)
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	--RFO inline_open_length (v: STRING; spos: INTEGER): INTEGER
	--RFO 		-- Number of characters in inline opening tag that starts at 
	--RFO 		-- index 'spos' in string 'v'
	--RFO 		-- Zero if invalid
	--RFO 	require
	--RFO 		valid_start: spos > 0 and spos <= v.count
	--RFO 		is_tag_start: v.item (spos) = '<'
	--RFO 		large_enough: (1 + v.count - spos) >= 3
	--RFO 	local
	--RFO 		sp, ep, spp, etp: INTEGER
	--RFO 		tag, ts, low_v: STRING
	--RFO 	do
	--RFO 		etp := next_rb_pos (v, spos)
	--RFO 		if etp /= 0 then
	--RFO 			tag := v.substring (spos, etp)
	--RFO 			etp := tag.count
	--RFO 			low_v := tag.as_lower
	--RFO 			if low_v.starts_with (Ks_usml_tag_prefix_inline_u) then
	--RFO 				-- <usml inline ...>
	--RFO 				-- <usml inline macro="I";ref="pos_1">
	--RFO 				-- <usml inline I ref="pos_1">
	--RFO 				sp := asr.index_of_next_non_whitespace (
	--RFO 					low_v, Ks_usml_tag_prefix_inline_u.count + 1, True)
	--RFO 			elseif low_v.starts_with (Ks_usml_tag_prefix_inline) then
	--RFO 				-- <inline ...>
	--RFO 				-- <inline macro="I";ref="pos_1">
	--RFO 				-- <inline I ref="pos_1">
	--RFO 				sp := asr.index_of_next_non_whitespace (
	--RFO 					low_v, Ks_usml_tag_prefix_inline.count + 1, True)
	--RFO 			elseif low_v.starts_with (Ks_usml_tag_prefix) then
	--RFO 				-- <usml ...>
	--RFO 				-- <usml macro="I";ref="pos_1">
	--RFO 				-- <usml I ref="pos_1">
	--RFO 				-- Must be a named macro
	--RFO 				--sp := Ks_usml_tag_prefix.count + 1
	--RFO 				sp := asr.index_of_next_non_whitespace (
	--RFO 					low_v, Ks_usml_tag_prefix.count + 1, True)
	--RFO 			else
	--RFO 				-- <??...>
	--RFO 				-- <I ref="pos_1">
	--RFO 				-- Must be a named macro
	--RFO 				sp := 2
	--RFO 			end
	--RFO 			spp := tag.index_of (' ', sp)
	--RFO 			if spp = 0 then
	--RFO 				ep := etp - 1
	--RFO 			else
	--RFO 				ep := spp - 1
	--RFO 			end
	--RFO 			-- Must have either params or macro, or both
	--RFO 			ts := tag.substring (sp, ep)
	--RFO 			if ts.has ('=') then
	--RFO 				Result := tag.count
	--RFO 			elseif ts.is_empty then
	--RFO 				-- Nope
	--RFO 			elseif attached macros.item (ts) as tm then
	--RFO 				Result := tag.count
	--RFO 			else
	--RFO 				-- Nope
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	is_inline_tag (v: STRING; spos: INTEGER): BOOLEAN
			-- Is the substring in 'v', beginning at position 'spos' an 
			-- inline opening tag?
		do
			Result := is_inline_open (v, spos)
		end

	is_inline_open (v: STRING; spos: INTEGER): BOOLEAN
			-- Is the substring in 'v', beginning at position 'spos' an 
			-- inline opening tag?
			-- N.B. Special case: if spos=1, then is NOT an 
			-- inline tag if it begins with "<usml macro=", as that form 
			-- denotes a single-line tag semi-terse form (type label 
			-- being omitted)
		require
			valid_start: spos > 0 and spos <= v.count
		local
			sp, ep, spp, etp: INTEGER
			tag, ts, low_v, nm, emsg: STRING
			tv: LIST [STRING]
		do
			create emsg.make (0)
			etp := next_rb_pos (v, spos)
			if v.item (spos) = '<' and v.count >= 3 and etp /= 0 then
				tag := v.substring (spos, etp)
				etp := tag.count
				low_v := tag.as_lower
				if low_v.starts_with (Ks_usml_tag_prefix_inline_u) then
					-- <usml inline ...>
					-- <usml inline macro="I";ref="pos_1">
					-- <usml inline I ref="pos_1">
					sp := asr.index_of_next_non_whitespace (
						low_v, Ks_usml_tag_prefix_inline_u.count + 1, True)
				elseif low_v.starts_with (Ks_usml_tag_prefix_inline) then
					-- <inline ...>
					-- <inline macro="I";ref="pos_1">
					-- <inline I ref="pos_1">
					sp := asr.index_of_next_non_whitespace (
						low_v, Ks_usml_tag_prefix_inline.count + 1, True)
				elseif low_v.starts_with (Ks_usml_tag_prefix) then
					-- <usml ...>
					-- <usml macro="I";ref="pos_1">
					-- <usml I ref="pos_1">
					-- Must be a named macro
					sp := asr.index_of_next_non_whitespace (
						low_v, Ks_usml_tag_prefix.count + 1, True)
					if spos = 1 then
						-- Might be inline or might be single
						-- If has a matching close, it's an inline
						if next_inline_close_start (
							v, Ks_usml_tag_prefix.count, emsg) = 0 then
								-- Not an inline
								-- Is more likely a single-line open, omitting the 
								-- type label, so NO, it's not an inline
								sp := tag.count
						end
					end
				else
					-- <??...>
					-- <I ref="pos_1">
					-- Must be a named macro
					sp := 2
				end
				spp := tag.index_of (' ', sp)
				if spp = 0 then
					ep := etp - 1
				else
					ep := spp - 1
				end
				-- Must have either params or macro, or both
				if ep /= 0 and sp <= ep then
					ts := tag.substring (sp, ep)
					if ts.starts_with ("macro=") then
--						if spos = 1 then
						if True then
							-- could be an inline or a single-line
							tv := ts.split ('=')
							nm := quotes_stripped (tv.i_th (2))
							if attached macros.item (nm) as tm then
								-- not a typed macro, should be an inline
								Result := tm.macro_type = 0
							else
								-- Invalid macro, or something else
								-- but not an inline tag
								sp := tag.count
							end
						end
					elseif ts.has ('=') then
						Result := True
					elseif ts.is_empty then
						-- Nope
					elseif attached macros.item (ts) as tm then
						Result := tm.macro_type = 0
					else
						-- Nope
					end
				else
					-- Nope
				end
			end
		end

	--|--------------------------------------------------------------

	is_inline_close (v: STRING; spos: INTEGER): BOOLEAN
			-- Is 'spos', within 'v', the start of an inline open tag?
			-- True if tag is standard-form, or at least appears to be a 
			-- valid mirrored macro form
		require
			valid_start: spos > 0 and spos <= v.count
		local
			ep, lim: INTEGER
			ts: STRING
		do
			lim := v.count
			-- inline close, must be at least 4 chars long "</C>"
			if lim >= (spos + 3) then
				ep := next_rb_pos (v, spos)
				if ep /= 0 then
					ts := v.substring (spos, ep)
					if ts.as_lower ~ Ks_usml_tag_inline_close then
						Result := True
					elseif ts.as_lower ~ Ks_usml_tag_inline_close_u then
						Result := True
					else
						-- might be mirrored macro close
						ep := ep + 1
						ts := ts.substring (3, ts.count - 1)
						if attached macros.item (ts) then
							Result := True
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO inline_tags_stripped (v: STRING; spos, epos: INTEGER): STRING
	--RFO 		-- String in 'v', from spos through epos, with all inline
	--RFO 		-- tags (if any) removed
	--RFO 	require
	--RFO 		valid_start: spos > 0 and spos <= v.count
	--RFO 		valid_end: epos >= spos and epos <= v.count
	--RFO 	local
	--RFO 		sp, ep, lim: INTEGER
	--RFO 		buf, emsg: STRING
	--RFO 	do
	--RFO 		create emsg.make (0)
	--RFO 		lim := epos
	--RFO 		create buf.make (1 + epos - spos)
	--RFO 		Result := buf
	--RFO 		from sp := spos
	--RFO 		until sp = 0 or sp > lim or not emsg.is_empty
	--RFO 		loop
	--RFO 			ep := next_inline_tag_start (v, sp)
	--RFO 			if ep /= 0 then
	--RFO 				if ep = sp then
	--RFO 					-- "><" back-to-back tags
	--RFO 					--   |
	--RFO 				else
	--RFO 					buf.append (v.substring (sp, ep - 1))
	--RFO 				end
	--RFO 				sp := next_rb_pos (v, ep + 1)
	--RFO 				if sp = 0 then
	--RFO 					-- ERROR, invalid tag
	--RFO 					buf := v
	--RFO 					sp := 0
	--RFO 				else
	--RFO 					sp := sp + 1
	--RFO 				end
	--RFO 			elseif sp > 0 and sp <= lim then
	--RFO 				buf.append (v.substring (sp, lim))
	--RFO 				sp := 0
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	inline_tokens (v: STRING): TWO_WAY_LIST [USML_INLINE_TOKEN]
			-- Collection of strings, each either render-able text or an 
			-- inline tag
		local
			sp, ep, lim, tp, oc: INTEGER
			buf, emsg: STRING
			tok: USML_INLINE_TOKEN
		do
			create Result.make
			create emsg.make (0)
			lim := v.count
			create buf.make (v.count)
			from sp := 1
			until sp = 0 or sp > lim or not emsg.is_empty
			loop
				ep := next_inline_tag_start (v, sp)
				if ep /= 0 then
					--Result.extend (v.substring (sp, ep-1))
					if ep = sp then
						-- "><" back-to-back tags
						--   |
					else
						if oc = 0 then
							tp := K_inline_token_text
						else
							tp := K_inline_token_captive
						end
						create tok.make_with_bounds (v, sp, ep-1, tp)
						Result.extend (tok)
					end
					sp := next_rb_pos (v, ep + 1)
					if sp = 0 then
						-- ERROR, invalid tag
						buf := v
						sp := 0
					else
						if is_inline_open (v, ep) then
							tp := K_inline_token_open
							oc := oc + 1
						else
							tp := K_inline_token_close
							oc := oc - 1
						end
						create tok.make_with_bounds (v, ep, sp, tp)
						Result.extend (tok)
						sp := sp + 1
					end
				elseif sp > 0 and sp <= lim then
					create tok.make_with_bounds (v, sp, lim, 0)
					Result.extend (tok)
					sp := 0
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_usml_tag_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML tag type?
		do
			if is_valid_usml_heading_level (v) then
				Result := True
			elseif is_valid_usml_list_type (v) then
				Result := True
			else
				inspect v
				when K_usml_tag_p, K_usml_tag_c, K_usml_tag_f, K_usml_tag_m then
					Result := True
				when K_usml_tag_title, K_usml_tag_doc, K_usml_tag_sp then
					Result := True
				else
				end
			end
		end

	is_valid_usml_heading_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML heading tag type?
		do
			Result := is_valid_usml_heading_level (v)
		end

	is_valid_usml_heading_level (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML heading tag level?
		do
			Result := v >= K_usml_hdr_lvl_min and v <= K_usml_hdr_lvl_max
			if not Result then
				Result := v = K_usml_tag_title or v = K_usml_tag_doc
			end
		end

	is_valid_usml_list_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML list tag type?
			-- List types and list levels are synched
		do
			Result := is_valid_usml_list_level (v)
		end

	is_valid_usml_list_level (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML numbered list level?
			-- N.B. list levels are biased to distinquish from headings
		do
			Result := v >= K_usml_list_lvl_min and v <= K_usml_list_lvl_max
		end

	is_valid_outlined_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid USML outlined section type?
		do
			Result := is_valid_usml_heading_type (v) or
				is_valid_usml_list_type (v)
		end

	--|--------------------------------------------------------------

	is_character_param (v: INTEGER): BOOLEAN
			-- Is 'v' a valid character parameter index?
		do
			Result := character_parameter_ids.has (v)
		end

	is_paragraph_param (v: INTEGER): BOOLEAN
			-- Is 'v' a valid paragraph parameter index?
		do
			Result := paragraph_parameter_ids.has (v)
		end

	is_outline_param (v: INTEGER): BOOLEAN
			-- Is 'v' a valid outline parameter index?
		do
			Result := outline_parameter_ids.has (v)
		end

	is_applicable_parameter (v, st: INTEGER): BOOLEAN
			-- Is parameter type 'v' applicable to section type 'st'?
		do
			inspect v
			when K_ft_ref_label, K_ft_xref_target then
				Result := True
			else
				if st = K_usml_tag_title then
					Result := is_character_param (v) or
						is_paragraph_param (v)
				elseif st = K_usml_tag_p then
					Result := is_character_param (v) or
						is_paragraph_param (v)
				elseif is_valid_outlined_type (st) then
					Result := is_character_param (v) or
						is_paragraph_param (v) or
						is_outline_param (v)
				else
					Result := is_character_param (v)
				end
			end
		end

	--|--------------------------------------------------------------

	is_valid_macro_label (v: STRING): BOOLEAN
			-- Is 'v' a syntactically valid macro label (name)?
			-- Need not be an known macro label, but must not be a 
			-- reserved label
		local
			i, lim: INTEGER
			c: CHARACTER
		do
			if not v.is_empty and then v.item (1). is_alpha then
				Result := True
				lim := v.count
				from i := 2
				until i > lim or not Result
				loop
					c := v.item (i)
					if c.is_alpha or c.is_digit or c = '_' then
						-- OK
					else
						Result := False
					end
					i := i + 1
				end
				if Result then
					Result := not reserved_labels.has (v.as_upper)
				end
			end
		end

	is_valid_macro_section_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid macro section type?
			-- Macro section types correspond to to tag/section types, 
			-- if typed, or special values if untyped
		do
			Result := is_valid_usml_tag_type (v)
			if not Result then
				Result := v = K_usml_tag_untyped or v = K_usml_tag_char_fmt_only
			end
		end

	--|--------------------------------------------------------------

	is_valid_font_family (v: STRING): BOOLEAN
			-- Is 'v' a valid font family?
		local
			low_v: STRING
		do
			low_v := v.as_lower
			if low_v ~ Ks_ff_sans then
				Result := True
			elseif low_v ~ Ks_ff_times then
				Result := True
			elseif low_v ~ Ks_ff_fixed then
				Result := True
			else
			end
		end

	is_valid_point_size (v: INTEGER): BOOLEAN
		do
			Result := v > 0
		end

	--|--------------------------------------------------------------

	is_valid_rgb_color (v: STRING): BOOLEAN
			-- Is 'v' a valid hexadecimal denoting a 24-bit RGB value?
		local
			ts: STRING
			vc, spos: INTEGER
			c: CHARACTER
			i, lim: INTEGER
		do
			if attached v as tv and then not tv.is_empty then
				ts := tv.as_lower
				if ts.starts_with ("0x") then
					vc := 8
					spos := 3
				else
					vc := 6
					spos := 1
				end
				if ts.count = vc then
					Result := True
					lim := vc
					from i := spos
					until i > lim or not Result
					loop
						c := v.item (i)
						if not c.is_hexa_digit then
							Result := False
						else
							i := i + 1
						end
					end
				end
			end
		end

	is_valid_color_id (v: STRING): BOOLEAN
			-- Is 'v' a valid color ID?
			-- Can be a valid RGB hex value, or a known color macro
		do
			Result := is_valid_rgb_color (v)
			if not Result then
				Result := is_valid_color_macro (v)
			end
		end

	is_valid_color_macro (v: STRING): BOOLEAN
			-- Is 'v' a known and valid color-only macro?
		do
			Result := attached color_macro_by_name (v)
		end

	--|--------------------------------------------------------------

	is_valid_format_type (v: INTEGER): BOOLEAN
			-- Is 'v' a valid format type?
		do
			Result := v > 0 and v <= K_ft_max
		end

	is_valid_outline_label_type (v: STRING): BOOLEAN
		local
			tv: INTEGER
		do
			if v.is_integer then
				tv := v.to_integer
				Result := tv >= num_format_tags.lower
					and tv <= num_format_tags.upper
			else
				Result := num_format_tags.has (v)
			end
		end

	--|--------------------------------------------------------------

	is_valid_alignment_value (v: STRING): BOOLEAN
			-- Is 'v' a valid alignment value?
			-- Can be integer value, character, or string
			-- 0, 'L', "left" denote left
			-- -1, 'R', "right" denote right
			-- 1, 'J', "justified" denote justified
			-- 2, 'C', "center" denote center
		local
			tv: INTEGER
			hi_v: STRING
		do
			if v.is_integer then
				tv := v.to_integer
				inspect tv
				when
					K_align_left, K_align_right,
					K_align_justified, K_align_center
				 then
					 Result := True
				else
				end
			elseif v.is_empty then
			else
				hi_v := v.as_upper
				if v.count = 1 then
					inspect hi_v.item (1)
					when 'L', 'R', 'C', 'J' then
						Result := True
					else
					end
				elseif hi_v ~ "LEFT" then
					Result := True
				elseif hi_v ~ "RIGHT" then
					Result := True
				elseif hi_v ~ "CENTER" then
					Result := True
				elseif hi_v ~ "JUSTIFIED" then
					Result := True
				end
			end
		end

	is_valid_boolean_tag (v: STRING): BOOLEAN
			-- Is 'v' a valid Boolean tag
			-- Booleans are expressed as "1", "0", "true", or "false"
			-- Long-forms are case-insensitive
		do
			inspect boolean_tag_to_value (v)
			when 0, 1 then
				Result := True
			else
			end
		end

--|========================================================================
feature {NONE} -- Support
--|========================================================================

	quotes_stripped (v: STRING): STRING
			-- The given string stripped of matching enclosing quotes, if 
			-- any
			-- N,B  If 'v' has no enclosing quotes, then Result = v
		do
			Result := asr.quotes_stripped (v)
		end

	--RFO cp_of_quotes_stripped (v: STRING): STRING
	--RFO 		-- The given string stripped of matching enclosing quotes, if 
	--RFO 		-- any
	--RFO 		-- N,B  If 'v' has no enclosing quotes, then Result = v
	--RFO 	local
	--RFO 		qc: CHARACTER
	--RFO 	do
	--RFO 		Result := v
	--RFO 		if v.count >= 2 then
	--RFO 			if v.item (1) = '%'' then
	--RFO 				qc := '%''
	--RFO 			elseif v.item (1) = '"' then
	--RFO 				qc := '"'
	--RFO 			else
	--RFO 				Result := v
	--RFO 			end
	--RFO 		end
	--RFO 		if qc /= '%U' then
	--RFO 			if v.item (v.count) = qc then
	--RFO 				if v.count = 2 then
	--RFO 					Result := ""
	--RFO 				else
	--RFO 					Result := v.substring (2,v.count - 1)
	--RFO 				end
	--RFO 			else
	--RFO 				Result := v
	--RFO 			end
	--RFO 		end
	--RFO 	end

	quoted (v: STRING): STRING
			-- The given string enclosed in matching dbl quotes
		do
			Result := "%"" + v + "%""
		end

--RFO 	carriage_returns_converted (v: STRING): STRING
--RFO 		do
--RFO 			Result := v.twin
--RFO 			Result.prune_all ('%R')
--RFO 		end

	--|--------------------------------------------------------------

	newlines_before_index (txt: STRING; v: INTEGER): INTEGER
			-- Number of newlines (lines) in string 'txt' before 
			-- index 'v'
		local
			i, lim: INTEGER
		do
			if not txt.is_empty then
				lim := v.min (txt.count)
				from i := 1
				until i > lim
				loop
					if txt.item (i) = '%N' then
						Result := Result + 1
					end
					i := i + 1
				end
			end
		end

	--|--------------------------------------------------------------

	boolean_tag_to_value (v: STRING): INTEGER
			-- Value of Boolean tag
			-- True=1
			-- False=0
			-- Invalid=-1
		do
			Result := -1
			if v.count = 1 then
				if v.item (1) = '1' then
					Result := 1
				elseif v.item (1) = '0' then
					Result := 0
				end
			elseif v.as_lower ~ "true" then
				Result := 1
			elseif v.as_lower ~ "false" then
				Result := 0
			end
		end

	bool_to_int (v: BOOLEAN): INTEGER
		do
			if v then
				Result := 1
			end
		end

	bool_to_istr (v: BOOLEAN): STRING
		do
			Result := bool_to_int (v).out
		end

	--|--------------------------------------------------------------

	alignment_value_from_string (v: STRING): INTEGER
		require
			valid: is_valid_alignment_value (v)
		local
			tv: INTEGER
		do
			if v.is_integer then
				tv := v.to_integer
			else
				if v.count = 1 then
					inspect v.item (1).as_upper
					when 'L' then
						tv := K_align_left
					when 'R' then
						tv := K_align_right
					when 'C' then
						tv := K_align_center
					when 'J' then
						tv := K_align_justified
					else
					end
				else
					if v ~ "left" then
						tv := K_align_left
					elseif v ~ "right" then
						tv := K_align_right
					elseif v ~ "center" then
						tv := K_align_center
					elseif v ~ "justified" then
						tv := K_align_justified
					else
					end
				end
			end
			inspect tv
			when
				K_align_left, K_align_right,
				K_align_justified, K_align_center
			 then
				 Result := tv
			else
				-- ERROR, but should have been caught with precondition
			end
		end

--|========================================================================
feature -- Common elements
--|========================================================================

	current_document: USML_DOCUMENT
		do
			Result := current_doc_cell.item
		end

	set_current_document (v: like current_document)
		do
			current_doc_cell.put (v)
		ensure
			is_set: current_document = v
		end

	current_doc_cell: CELL [USML_DOCUMENT]
		once
			create Result.put (dummy_doc)
		end

	dummy_doc: USML_DOCUMENT
		once
			create Result.make_empty
		end

	macros: HASH_TABLE [USML_TAG, STRING]
			-- Table of tag macros (macro tags), keyed by name
			-- In Current Document (vs in lib, built-in, etc.)
		do
			Result := current_document.macros_table
		end

	color_macro_by_name (v: STRING): detachable USML_TAG
			-- Color-only macro by name 'v', if any
		do
			if not v.is_empty and then attached macros.item (v) as tm then
				Result := tm
			end
		end

	ref_target_by_label (v: STRING): detachable USML_REF
			-- Reference target with label 'v', if any
		do
			Result := current_document.reference_target_by_label (v)
		end

	image_by_label (v: STRING): detachable USML_IMAGE_TAG
			-- Embedded image with label 'v', if any
		do
			Result := current_document.image_tag_by_label (v)
		end

	links: HASH_TABLE [USML_REF, STRING]
			-- Table of defined reference links (xref and img tags),
			-- keyed by name
		do
			Result := current_document.reference_links
		end

--|========================================================================
feature -- Constants
--|========================================================================

	default_format_for_type (v: INTEGER): USML_FORMAT
			-- Default format, for section type 'v', from dummy document
		do
			inspect v
			when K_usml_tag_p then
				Result := dummy_doc.default_format_p
			when K_usml_tag_title then
				Result := dummy_doc.default_format_doc_title
			when K_usml_tag_h1 then
				Result := dummy_doc.default_format_h1
			when K_usml_tag_h2 then
				Result := dummy_doc.default_format_h2
			when K_usml_tag_h3 then
				Result := dummy_doc.default_format_h3
			when K_usml_tag_h4 then
				Result := dummy_doc.default_format_h4
			when K_usml_tag_h5 then
				Result := dummy_doc.default_format_h5
			when K_usml_tag_l1 then
				Result := dummy_doc.default_format_l1
			when K_usml_tag_l2 then
				Result := dummy_doc.default_format_l2
			when K_usml_tag_l3 then
				Result := dummy_doc.default_format_l3
			when K_usml_tag_l4 then
				Result := dummy_doc.default_format_l4
			when K_usml_tag_l5 then
				Result := dummy_doc.default_format_l5
			else
				-- must be inline; defaults aren't relevant
				-- use paragraph defaults
				Result := dummy_doc.default_format_p
			end
		end

	usml_tag_prefix_heading (lvl: INTEGER): STRING
			-- Tag, through type, W/O trailing space or closing '>' for 
			-- heading level 'lvl'
			-- e.g. "<usml h2"
		require
			valid_level: is_valid_usml_heading_level (lvl)
		do
			Result := Ks_usml_tag_prefix + "h" + lvl.out
		end

	--|--------------------------------------------------------------

	multi_open_tag (st: INTEGER; tf: BOOLEAN): STRING
			-- Opening tag for multi-line tag for section type 'st'
			-- If 'tf', then terse form, else long form
		local
			pfx, sts: STRING
		do
			Result := ""
			pfx := ""
			sts := ""
			if st = K_usml_tag_inline then
				Result := "INVALID"
			else
				if tf then
					pfx := Ks_usml_tag_x_open_prefix
				else
					Result := Ks_usml_tag_x_open_prefix_u
				end
				inspect st
				when K_usml_tag_p then
					sts := "P"
				when K_usml_tag_title then
					sts := "HT"
				when K_usml_tag_h1 then
					sts := "H1"
				when K_usml_tag_h2 then
					sts := "H2"
				when K_usml_tag_h3 then
					sts := "H3"
				when K_usml_tag_h4 then
					sts := "H4"
				when K_usml_tag_h5 then
					sts := "H5"
				when K_usml_tag_l1 then
					sts := "L1"
				when K_usml_tag_l2 then
					sts := "L2"
				when K_usml_tag_l3 then
					sts := "L3"
				when K_usml_tag_l4 then
					sts := "L4"
				when K_usml_tag_l5 then
					sts := "L5"
				else
					sts := ""
				end
				if not sts.is_empty then
					Result := pfx + sts + ">"
				end
			end
		end

	num_format_tag_to_index (v: STRING): INTEGER
			-- Numerical equiv of num fmt tag 'v'
			-- NOTE WELL, num fmt types begin with 0 (#)
			-- and so zero is a valid index / fmt type
			-- If tag is not valid, defaults to number
		local
			i, lim: INTEGER
			found: BOOLEAN
		do
			if is_valid_outline_label_type (v) then
				if v.is_integer then
					Result := v.to_integer
				else
					lim := num_format_tags.upper
					from i := num_format_tags.lower
					until i > lim or found
					loop
						if num_format_tags.item (i) ~ v then
							found := True
							Result := i
						else
							i := i + 1
						end
					end
				end
			end
		end

	num_format_tag (v: INTEGER): STRING
			-- Single-char tag corresponding to number format type 'v'
			-- NOTE WELL, num fmt types begin with 0
		do
			if v >= num_format_tags.lower and v <= num_format_tags.upper then
				Result := num_format_tags.item (v)
			else
				Result := ""
			end
		end

	num_format_label (v: INTEGER): STRING
		do
			inspect v
			when K_num_format_up_alpha then
				Result := "Alpha upper case"
			when K_num_format_lo_alpha then
				Result := "Alpha lower case"
			when K_num_format_up_roman then
				Result := "Roman upper case"
			when K_num_format_lo_roman then
				Result := "Roman lower case"
			when K_num_format_text then
				Result := "Text"
			else
				-- Default to K_num_format_number
				Result := "Number"
			end
		end

	num_format_out (v, fmt: INTEGER): STRING
			-- Output form of outline number 'v', for format 'fmt'
		do
			if v > 0 and v <= K_max_outline_instance then
				-- and v <= K_max_levels then
				inspect fmt
				when K_num_format_up_alpha then
					Result := number_to_up_alpha (v)
				when K_num_format_lo_alpha then
					Result := number_to_lo_alpha (v)
				when K_num_format_up_roman then
					Result := number_to_up_roman (v)
				when K_num_format_lo_roman then
					Result := number_to_lo_roman (v)
				else
					Result := v.out
				end
			else
				Result := v.out
			end
		end

	number_to_up_alpha (v: INTEGER): STRING
		local
			tv, vm: INTEGER
			c: CHARACTER
		do
			create Result.make (6)
			from tv := v
			until tv <= 0
			loop
				vm := ((tv - 1) \\ 26) + 1
				c := Ks_up_alphabet.item (vm)
				tv := (tv - 1) // 26
				Result.prepend (c.out)
			end
		end

	old_number_to_up_alpha (v: INTEGER): STRING
		require
			valid: is_valid_outline_instance (v)
		do
			Result := up_alphabet_1_5200.item (v)
		end

	number_to_lo_alpha (v: INTEGER): STRING
		require
			valid: is_valid_outline_instance (v)
		do
			--Result := up_alphabet_1_5200.item (v).as_lower
			Result := number_to_up_alpha (v).as_lower
		end

	number_to_up_roman (v: INTEGER): STRING
		require
			valid: is_valid_outline_instance (v)
		do
--			Result := roman_numerals_1_100_up.item (v)
			Result := integer_to_roman (v)
		end

	number_to_lo_roman (v: INTEGER): STRING
		require
			valid: is_valid_outline_instance (v)
		do
			Result := number_to_up_roman (v).as_lower
		end

	integer_to_roman (v: INTEGER): STRING
		local
			tv, rv, idx: INTEGER
		do
			create Result.make (6)
			tv := v
			from
				idx := closest_roman_magnitude_index (v)
				rv := roman_numeral_magnitudes.item (idx)
			until idx = 0 or tv <= 0
			loop
				tv := tv - rv
				Result.append (roman_numeral_magnitude_labels.item (idx))
				if tv /= 0 then
					idx := closest_roman_magnitude_index (tv)
					rv := roman_numeral_magnitudes.item (idx)
				end
			end
--			Result.append (tv.max (0).out)
		end

	closest_roman_magnitude_index (v: INTEGER): INTEGER
			-- Index, in magnitudes tables, of largest value less than 
			-- or equal to 'v'
		local
			i, lim, mv: INTEGER
			ma: like roman_numeral_magnitudes
		do
			ma := roman_numeral_magnitudes
			lim := ma.lower
			from i := ma.upper
			until i < lim or Result /= 0
			loop
				mv := ma.item (i)
				if mv <= v then
					Result := i
				else
					i := i - 1
				end
			end
		end

--|========================================================================
feature -- Debugging support
--|========================================================================

	debug_enabled: BOOLEAN
		do
			Result := debug_enabled_cell.item
		end

	enable_debug
		do
			debug_enabled_cell.put (True)
		end

	disable_debug
		do
			debug_enabled_cell.put (False)
		end

	debug_line_start_matches: HASH_TABLE [STRING, STRING]
			-- Strings to match, during document initialization from 
			-- stream, at start of line (for setting breakpoints without 
			-- the need to recompile for each different match)
		once
			create Result.make (3)
		end

	add_debug_line_start_match (v: STRING)
		do
			if not debug_line_start_matches.has (v) then
				debug_line_start_matches.extend (v, v)
			end
		end

	remove_debug_line_start_match (v: STRING)
		do
			if debug_line_start_matches.has (v) then
				debug_line_start_matches.remove (v)
			end
		end

	purge_debug_line_start_matches
		do
			debug_line_start_matches.wipe_out
		end

	--|--------------------------------------------------------------

	debug_enabled_cell: CELL [BOOLEAN]
		once
			create Result.put (False)
		end

--|========================================================================
feature {NONE} -- Shared services
--|========================================================================

	asr: AEL_SPRT_STRING_ROUTINES
		once
			create Result
		end

	next_space_pos (str: STRING; spos: INTEGER): INTEGER
			-- Index, in 'str', at or after 'spos', of space or tab
		require
			valid_start: spos > 0 and spos <= str.count
		do
			Result := asr.index_of_next_whitespace (str, spos, False)
		end

	next_nonspace_pos (str: STRING; spos: INTEGER): INTEGER
			-- Index, in 'str', at or after 'spos', of next NON whitespace
		require
			valid_start: spos > 0 and spos <= str.count
		do
			Result := asr.index_of_next_non_whitespace (str, spos, False)
		end

	next_rb_pos (str: STRING; spos: INTEGER): INTEGER
			-- Index, in 'str', at or after 'spos', of next UNQUOTED '>'
		require
			valid_start: spos > 0 and spos <= str.count
		do
			Result := asr.index_of_next_unquoted2 ('>', str, spos)
		end

	next_lb_pos (str: STRING; spos: INTEGER): INTEGER
			-- Index, in 'str', at or after 'spos', of next UNQUOTED '<'
		require
			valid_start: spos > 0 and spos <= str.count
		do
			Result := asr.index_of_next_unquoted2 ('<', str, spos)
		end

	next_lbc_pos (str: STRING; spos: INTEGER): INTEGER
			-- Index, in 'str', at or after 'spos', of next UNQUOTED "</"
		require
			valid_start: spos > 0 and spos <= str.count
		local
			sp, lim: INTEGER
		do
			lim := str.count
			from sp := next_lb_pos (str, spos)
			until sp = 0 or sp > lim or Result /= 0
			loop
				-- No opening '<' means no chance of "</"
				if sp < lim and then str.item (sp + 1) = '/' then
					Result := sp
				else
					-- keep looking
					sp := next_lb_pos (str, sp + 1)
				end
			end
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 12-Dec-2019
--|     Created original module from UMSL_CONSTANTS
--|     Compiled with Eiffel 18.07, void-safe
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_CORE
