note
	description: "{
A major section, within a structured USML document,
that contains subsections
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_CPLX_SECTION

inherit
	USML_SECTION
		redefine
			default_create, text, type_label, has_items, is_empty
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	default_create
			-- Create Current in its default initial state
		do
			create headings.make
			create items.make
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	items: TWO_WAY_LIST [like item]

	count: INTEGER
			-- Number of items in items
		do
			Result := items.count
		end

	type_label: STRING
		do
			Result := "Complex Section"
		end

	--|--------------------------------------------------------------

	headings: TWO_WAY_LIST [USML_SECTION]

	num_headings: INTEGER
			-- Number of heading items
		do
			Result := headings.count
		end

	text: STRING
			-- Text belonging (directly) to Current
		do
			Result := Precursor
		end

	--|--------------------------------------------------------------

	is_empty: BOOLEAN
			-- Is items empty?
		do
			Result := count = 0
		end

	--|--------------------------------------------------------------

	has (v: like item): BOOLEAN
			-- Does items have the given item?
			-- Object or reference; reference by default
		do
			Result := items.has (v)
		end

	--|--------------------------------------------------------------

	has_items: BOOLEAN
			-- Does Current have any subordinate elements?
		do
			Result := not items.is_empty
		end

--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

	item: USML_SECTION
			-- Item at cursor
		do
			Result := items.item
		end

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

	start do items.start end
			-- Set the items cursor to the start of items

	finish do items.finish end
			-- Set the items cursor to the end of items

	forth do items.forth end
			-- Advance the items cursor one position

	back do items.back end
			-- Retreat the items cursor one position

	after: BOOLEAN do Result := items.after end
			-- Is the items cursor past the end?

 	before: BOOLEAN do Result := items.before end
			-- Is the items cursor off the beginning?

	go_to (oc: CURSOR) do items.go_to (oc) end
			-- Reset the items cursor to 'oc'

--|========================================================================
feature -- Element change
--|========================================================================

	last_instance_number: INTEGER
			-- Instance number of most recently added item
			-- Might not be same as count, or prev+1

	extend (v: like item)
			-- Add item 'v' to end of items
		do
			-- add to items to maintain hierarchy
			items.extend (v)
			if v.is_heading then
				headings.extend (v)
				last_instance_number := last_instance_number + 1
				v.set_instance_number (last_instance_number)
			end
			v.set_parent (Current)
		ensure then
			parented: v.parent = Current
		end

	--|--------------------------------------------------------------

	extend_restarted (v: like item; sn: INTEGER)
			-- Add item 'v' to end of items
			-- Restart numbering at 'sn'
		require
			valid_start: sn >= 0
		do
			-- add to items to maintain hierarchy
			items.extend (v)
			if v.is_heading then
				v.set_instance_number (sn)
				headings.extend (v)
				last_instance_number := sn
			end
			v.set_parent (Current)
		ensure then
			parented: v.parent = Current
		end

--|========================================================================
feature -- Element removal
--|========================================================================

	wipe_out
			-- Remove all items
		do
			items.wipe_out
			headings.wipe_out
		end

	--|--------------------------------------------------------------

	remove
			-- Remove item at cursor
		do
			items.remove
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	items_out: STRING
		local
			ti: USML_SECTION
		do
			create Result.make (2048)
			from start
			until after
			loop
				ti := item
				if ti.is_title then
					Result.append (ti.type_label + "%N")
				elseif ti.is_heading then
					if attached {USML_HEADING} ti as sec then
						Result.append (ti.type_label + " ")
						Result.append (sec.outline_number + "%N")
--RFO 						if not sec.is_empty then
--RFO 							Result.append (sec.sections_out)
--RFO 						end
					end
				elseif ti.is_list then
					if attached {USML_LIST} ti as lst then
						Result.append (ti.type_label + " ")
						Result.append (lst.outline_number + "%N")
						-- Do Not descend lists, each is a section
					end
				else
					Result.append ("Paragraph%N")
				end
				forth
			end
		end

	--|--------------------------------------------------------------

	items_unrolled: TWO_WAY_LIST [USML_SECTION]
			-- All sections within Current, with outlined items and 
			-- their subs unrolled into a flat sequence
		local
			ti: USML_SECTION
		do
			create Result.make
			from start
			until after
			loop
				ti := item
				if ti.is_title then
					Result.extend (ti)
				elseif attached {USML_HEADING} ti as sec then
					Result.extend (ti)
					Result.append (sec.items_unrolled)
				elseif attached {USML_LIST} ti as lst then
					Result.extend (ti)
					Result.append (lst.items_unrolled)
				else
					Result.extend (ti)
				end
				forth
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_CPLX_SECTION
