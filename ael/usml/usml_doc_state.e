note
	description: "{
Encapsulation of document processing state
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_DOC_STATE

inherit
	ANY
		undefine
			default_create
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
		do
			default_create
		end

	default_create
		do
 			create heading_stack.make
		end

--|========================================================================
feature -- Status
--|========================================================================

	heading_stack: TWO_WAY_LIST [USML_HEADING]

	heading_instance_number: INTEGER
			-- Instance number of most recently added heading, if any

	list_instance_number: INTEGER
			-- Instance number of most recently added list, if any

--|========================================================================
feature -- Status setting
--|========================================================================

	set_heading_instance_number (v: INTEGER)
		do
			heading_instance_number := v
		end

	set_list_instance_number (v: INTEGER)
		do
			list_instance_number := v
		end

	increment_heading_instance
		do
			set_heading_instance_number (heading_instance_number + 1)
		end

	increment_list_instance
		do
			set_list_instance_number (list_instance_number + 1)
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_DOC_STATE
