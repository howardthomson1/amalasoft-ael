note
	description: "{
A structured USML document
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_DOCUMENT

inherit
	USML_CPLX_SECTION
		rename
			headings as section_headings
		redefine
			is_document, default_create, item, format, items_unrolled,
			extend, extend_restarted, text_out,
			set_values_from_tag, outline_numbering_enabled, type_label,
			default_format_p, default_format_v, default_format_doc_title,
			default_format_h1, default_format_h2, default_format_h3,
			default_format_h4, default_format_h5,
			default_format_l1, default_format_l2, default_format_l3,
			default_format_l4, default_format_l5,
			out
		end

create
	make_dummy, make_empty, make_from_stream

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_stream (ts, v: STRING)
			-- Create Current from stream 'v', with title 'ts'
			-- Stream must be in USML (Ultra-simple markup language) 
			-- format
		require
			valid_title: not ts.is_empty
		do
			stream_length := v.count
			default_create
			set_title (ts)
			set_current_document (Current)
			preprocess_depth := 0
			line_number := 0
			preprocess (v, Void)
			line_number := 0
			if not has_error then
				init_from_stream
				is_initialized := True
			end
		end

	--|--------------------------------------------------------------

	make_empty
		do
			default_create
			set_title ("empty")
		end

	make_dummy
		do
			default_create
			set_title ("dummy")
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			initialize_defaults
 			create states.make
 			states.extend (create {USML_DOC_STATE}.make)
			doc_depth := 1
			create lines.make
			create images.make (7)
 			create title.make (0)
			create tags.make
			create includes.make
 			create headings.make
			create list_stack.make
--			create heading_stack.make
			create lists.make
			create macros_table.make (11)
			create reference_targets.make (11)
			create live_reference_targets.make (11)
			create reference_links.make (11)
			Precursor
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_stream
			-- Initialize Current from stream captured in 'lines'
			-- Stream must be in USML (Ultra-simple markup language) 
			-- format
			-- Format includes tags that define the style of the 
			-- text that follows (until the next tag or end of stream).
			-- Tags act like "sponges" for following text.
			-- Tags with defined "intext" are self-contained and do
			-- not follow the sponge model, Any non-tag tag after an 
			-- intext tag is orphaned and a warning issued (the text is 
			-- added to the document as a paragraph)
			-- Each non-comment tag is a section.
			-- Sections are kept in simple sequence.
			-- Document can have sub-documents.  A sub-document has a 
			-- separate state, that state including the headings stack
			-- Subdocuments exist to support a picture-in-picture model, 
			-- where a document can decribe (by example, and therefore 
			-- include) another document.  Headings in the 
			-- subdocument(s) are numbered independently of the main 
			-- document and will not appear in the TOC of the rendered 
			-- document.
			-- Headings are kept in order of appearance, and can have 
			-- subordinates, the subordinates managed by the headings 
			-- themselves, but a stack of headings is maintained also by 
			-- the document state.
			-- Lists are sections and, like headings, are outlined.
			-- A separate list stack is maintained to track the 
			-- outlining.
			-- 
			-- Example
			--   <usml h1>
			--   Introduction (this is text of the heading)
			--   <usml t>
			--   This is the text of the introduction, a text section
		local
			in_multi, is_tag, intoff: BOOLEAN
			line, emsg: STRING
			tline: USML_LINE
			tag, xtag: USML_TAG
			tsx, tsec: USML_SECTION
			vsec: USML_VERBATIM_SECTION
			spc: CELL [detachable USML_SECTION]
			st: USML_DOC_STATE
			tdata, xtdata: USML_TAG_DATA
		do
--print (lines_out)
			create spc.put (Void)
			create emsg.make (0)
			purge_list_stack
			purge_heading_stack
			from lines.start
			until lines.after or has_error
			loop
				tdata := Void
				is_tag := False
				emsg.wipe_out
				tline := lines.item
				line := tline.text
				line_number := tline.src_lineno
				if line.count >= 3 and then
					line.item (1) = '<' and line.index_of ('>', 2) > 2 then
					-- Looks like it might be a tag
					-- Could an open tag, close tag or false alarm
					create tdata.make_from_line (tline)
					-- TODO differentiate non-tag look-alikes from genuinely 
					-- bad usml tags
 					if attached tdata.warning_message as wmsg then
						record_warning (
							"%%s at line %%s",
							<< wmsg, tline.line_number_out >>)
					end
 					if attached tdata.error_message as msg then
						emsg.append (msg + " at line " + tline.line_number_out)
--RFO 						set_error_message (
--RFO 							msg + " at line " + tline.line_number_out)
						emsg.wipe_out
 					end
--					elseif tdata.is_usml_tag then
					if tdata.is_usml_tag and not tdata.is_inline then
						if tdata.is_verbatim_open then
							-- Disable interpretation
							if intoff then
								-- ERROR??, off without intervening on
							end
							intoff := True
							-- create a literal section; make it the sponge
							create tag.make_from_tag_data (tdata)
							create vsec.make_with_tag_and_data (tag, tdata)
							vsec.set_starting_line (line_number)
							extend (vsec)
							spc.put (vsec)
							is_tag := True
						elseif tdata.is_verbatim_close then
							if not intoff then
								-- ERROR, on without preceding off
							end
							-- TODO add end position, tdata
							intoff := False
							if vsec /= Void then
								vsec.set_tdata_close (tdata)
							else
								-- ERROR
							end
							create tag.make_from_tag_data (tdata)
							spc.put (Void)
							is_tag := True
						elseif intoff then
							if vsec = Void then
								-- ERROR
							else
							end
						else
							if tdata.is_open_tag then
								if attached spc.item as sponge then
									-- occurrence of a new tag closes out previous 
									-- sponge's text, even if it had no text lines 
									-- following it
									if sponge.has_trailing_text then
										-- Don't let it be a sponge anymore
										--spc.put (Void)
										if attached sponge.trailing_text as tx then
											--TODO Why does it not?
											if tx.item (tx.count) /= '%N' then
												sponge.append_text ("%N")
											end
										end
										sponge.clear_trailing_text
									end
									-- TODO, if extracting inlines on section closure, 
									-- do it here
									sponge.assemble_text
									spc.put (Void)
								else
									--printf ("sponge was void for line: %%s%N",<<line>>)
								end
								if attached xtag then
									-- Close out existing multi-line tag
									tag := xtag
									xtag := Void
								end
								if tdata.is_multiline then
									xtdata := tdata
									tag := Void
									create xtag.make_multiline (line)
									xtag.set_src_position (tdata.src_position)
									xtag.set_src_start_line (tdata.src_lineno)
									if attached xtag.error_message as msg then
										set_error_message (
											msg + " at line " + tline.line_number_out)
									end
									in_multi := True
								else
									create tag.make_from_tag_data (tdata)
									if attached tag.error_message as msg then
										set_error_message (
											msg + " at line " + tline.line_number_out)
									end
									in_multi := False
								end
								is_tag := True
							else
								-- Must be a close tag, open already created as 
								-- 'xtag'
								-- End multiline tag
								in_multi := False
								if not attached xtag then
									-- ERROR
								else
									xtag.parse_multiline_tag_text
									-- N.B. trailing text is considered benign, so 
									-- will not generate an error
									if attached xtag.error_message as msg then
										set_error_message (msg + " at line " +
											tline.line_number_out)
									end
									tag := xtag
									if attached xtdata as xtd then
										-- Capture multi-open line
									else
										-- ERROR
									end
									xtag := Void
									is_tag := True
								end
							end
						end
					else -- if tdata.is_usml_tag then
						-- Not a tag (though it did start with '<')
					end
				end
				if not is_tag then
					-- Not a tag, open, close or otherwise
					if attached xtag then
						-- In multiline tag, continue capturig text
						xtag.append_tag_text (line)
					elseif attached spc.item as sponge then
						-- Has an active component, add to its text
						if sponge.has_intext then
-- This should not happen as sponge is voided with intext
-- If it does happen, needs to be an error
							-- TODO
							-- don't take the line????
						else
-- TODO this fouls up position tracking
							if sponge.has_trailing_text then
								if attached sponge.trailing_text as txt then
									sponge.append_text ("%N")
								end
								sponge.append_line (line)
								sponge.clear_trailing_text
							else
								sponge.append_line (line)
							end
						end
					else
						--printf ("sponge was void for non-tag line: %%s%N",<<line>>)
						-- No place to put non-tag text
						-- Is this an error or not?
						-- Consider this a paragraph-without-portfolio
						-- at least for now UNLESS prev tag had intext, in 
						-- which case it's illegal to have any following 
						-- text
						if attached tag as tt and then tag.has_intext then
							set_error_message (aprintf("{
Sections with intext must not have following text.
Line %%d might be missing a paragraph tag.
}", 
							<< line_number >>))
						else
							create tag.make_with_text (Ks_usml_tag_prefix + "p>")
							purge_list_stack
							create tsec.make_as_text (Current, tag)
							tsec.set_starting_line (line_number)
							tsec.append_line (line)
							tsec.assemble_text
							-- Don't be a sponge
							spc.put (Void)
							extend (tsec)
						end
					end
				elseif attached tag and not has_error then
					-- Is some kind of tag
					tags.extend (tag)
					if tag.is_verbatim_open or tag.is_verbatim_close then
					end
					if tag.is_macro then
						-- Is a macro _definition_ tag
						-- N.B. Macro check MUST precede other checks
						-- Tag defines a macro for subsequent use in tags
						-- Add macro
						if attached tag.macro_name as mnm then
							if macros.has (mnm) then
								-- Replace vs error
								-- OK to redefine macros, also, macro would 
								-- be identified, but not codified, in preprocess
								macros.replace (tag, mnm)
							else
								macros.extend (tag, mnm)
							end
						else
							-- ERROR Should have been caught at tdata
							set_error_message (
								"Macro tag is missing name at line "
								+ tline.line_number_out)
						end
					elseif tag.is_format then
						-- format tag is not a document component, so do 
						-- not flush the list or section stacks
						--
						-- Tag defines format for a class of section, 
						-- from this point forward, unless redefined in
						-- that section's tag
						--ptag := tag
						set_default_format_from_tag (tag)
					elseif tag.is_doc then
						-- TODO is this still relevant?
						-- Set document values from tag
						set_values_from_tag (tag)
						purge_list_stack
					elseif tag.is_title then
						-- Should not happen
						create tsx.make_leveled (Current, tag)
						extend (tsx)
 						spc.put (tsx)
						tsx.set_starting_line (line_number)
						purge_list_stack
					elseif tag.is_heading then
						-- can be single or multi
						purge_list_stack
						process_heading_tag (tag, spc, tline)
					elseif tag.is_list then
						process_list_tag (tag, spc, tline)
					elseif tag.is_sub_start then
						-- Push a fresh state
 						create st.make
 						states.extend (st)
						doc_depth := doc_depth + 1
						in_subdoc := True
					elseif tag.is_sub_end then
						-- Pop state to return to former
						if doc_depth <= 1 then
							-- ERROR
						else
 							states.finish
 							states.remove
							doc_depth := doc_depth - 1
 							in_subdoc := doc_depth > 1
						end
					elseif tag.is_paragraph then
						-- Not heading or list, or macro or format
						-- Lists can have nested lists, but 
						-- no other tag types
						-- TODO check all other such cases
						purge_list_stack
						create tsec.make_as_text (Current, tag)
						tsec.set_starting_line (line_number)
						if tsec.has_intext then
							-- Don't be a sponge
							spc.put (Void)
						else
							spc.put (tsec)
						end
						extend (tsec)
					elseif tag.is_verbatim_open or tag.is_verbatim_close then
						-- handled above
					else
						-- A comment (should have been stripped by preprocessor)
						--
						-- Add comment as section
						-- TODO Maintain comment state separately to permit
						-- mid-paragraph/inline comments ???  Nope
						create tsec.make_as_comment (Current, tag)
						tsec.set_starting_line (line_number)
						spc.put (tsec)
					end
				end
				lines.forth
			end
--print (text_out)
--print (sections_out)
--print (headings_out)
--print (tags_out)
--print (formats_out)
		end

	--|--------------------------------------------------------------

	process_heading_tag (
		tag: USML_TAG;
		spc: CELL [detachable USML_SECTION];
		tline: USML_LINE)
			-- Process heading tag 'tag' from input stream
			-- Each heading is a kind of section, and has an 
			-- associated format.
			-- Heading outline is managed by the heading stack
			-- A heading tag can specify a start number
			-- A specified start number clears the heading stack because 
			-- specifying a start number indicates the start of a new 
			-- sequence
		require
			heading: tag.is_heading
		local
			lvl: INTEGER
			top_h, tsh: USML_HEADING
		do
			-- Heading text follows heading (next line) UNLESS it has 'intext'
			-- and should be assigned to sec.text

			create tsh.make_leveled (Current, tag)
			if tag.is_multiline then
				tsh.set_starting_line (tag.src_start_line)
				tsh.set_closing_line (line_number)
				tag.set_src_end_position (tline.src_position + tline.text.count)
			else
				tsh.set_starting_line (line_number)
			end
			lvl := tag.level
			if lvl = 1 then
				if tag.has_number_start and tag.number_start /= 0 then
					extend_restarted (tsh, tag.number_start)
				else
					extend_outlined (tsh, lvl)
				end
				purge_heading_stack
				heading_stack.extend (tsh)
			else
				if heading_stack.is_empty then
					-- Issue; a non-top-level heading, but no 
					-- headings to which to add it
					-- Not necessarily an error, but likely
					-- Cannot assign a reasonable instance number without 
					-- a parent
					-- TODO, maybe leverage 'start' param, but if not 
					-- present, count as error?
					record_warning (
						"Level %%d heading at line %%s has no parent",
						<< lvl, tline.line_number_out >>)
					extend_outlined (tsh, lvl)
					heading_stack.extend (tsh)
				else
					top_h := current_heading
					if top_h.level /= lvl - 1 then
						-- Purge stack until a suitable parent heading
						-- is found, or empty
						unwind_heading_stack (lvl)
						if heading_stack.is_empty then
							-- Issue
							record_warning (
								"Heading [%%d] at line %%s has no parent",
								<< lvl, tline.line_number_out >>)
						end
					end
					if heading_stack.is_empty then
						extend_outlined (tsh, lvl)
					else
						top_h := current_heading
						if tag.has_number_start and tag.number_start /= 0 then
							top_h.extend_restarted (tsh, tag.number_start)
						else
							top_h.extend (tsh)
						end
						--top_h.extend (tsh)
						-- Add to items, but bypass 'extend', lest parent be 
						-- clobbered
						items.extend (tsh)
					end
					heading_stack.extend (tsh)
				end
			end
			spc.put (tsh)
		end

	--|--------------------------------------------------------------

	process_list_tag (
		tag: USML_TAG;
		spc: CELL [detachable USML_SECTION];
		tline: USML_LINE)
			-- Process list tag 'tag' from input stream
			-- Each list item is a kind of section, and has an 
			-- associated format (like headings and paragraphs)
			-- List membership is managed by the list stack, and the 
			-- list stack is purged whenever a non-list tag is 
			-- encountered, and outlining is restarted
			-- A list tag can specify a start number (like headings)
			-- A specified start number clears the list stack because 
			-- specifying a start number indicates the start of a new list
		local
			lvl: INTEGER
			top_li, tli, pli: USML_LIST
		do
			-- Void the active component, if any
			-- Defer assignment to list stack
--			print_list_stack
			create tli.make_as_text (Current, tag)
			tli.set_starting_line (line_number)
			lvl := tag.list_level
			if list_stack.is_empty then
				-- Allow for a continuation-after-break situation
				-- if the tag has a start number and the start 
				-- number is greater than 1.  For such a continuation,
				-- go back to the most recent list (from 'lists', NOT
				-- from the stack), to find the appropriate parent
				--
				-- If the start number is <= 1 then is a new list
				-- If list level is 1, w/o start number, then is new list
				--
				-- In all cases, add the new item to the top of the stack
				if tag.has_number_start and tag.number_start > 1 then
					-- Continuation after break; find the parent
					pli := list_guardian (tli)
					-- If pli is Void, then start a new list, but apply 
					-- the specified start number anyway
					tli.set_top_item (pli)
					tli.set_instance_number (tag.number_start)
				else
					-- New list, tli is the top item
					tli.set_top_item (tli)
					tli.set_instance_number (1)
				end
				list_stack.extend (tli)
				spc.put (tli)
			else
				-- Stack is NOT empty
				top_li := current_list
				if top_li.list_level = lvl then
					-- Another item of same level without 
					-- intervening non-list section
					-- If not level 1, then capture parent from 
					-- current top, and extend parent
					-- Else (lvl=1), up instance number and add 
					-- to current _section_, making new item the 
					-- new sponge
					if lvl = 1 then
						-- no parent, but there is a top
						-- and bump instance number of new
						-- replace old top with new
						--top_li.add_peer (tli)
						if tag.has_number_start and tag.number_start > 1 then
							top_li.add_peer (tli, tag.number_start)
						else
							top_li.add_peer (tli, 0)
						end
						--tli.set_instance_number (top_li.instance_number+1)
						purge_list_stack
						list_stack.extend (tli)
						-- Add to parent section ONLY if is top 
						-- and first, so NOT here
						-- Instead, add as peer of top
						spc.put (tli)
					else
						-- Peer in same parent
						-- Grab parent, replace old top with new
						-- and bump instance number of new
						--top_li.add_peer (tli)
						if tag.has_number_start and tag.number_start > 1 then
							top_li.add_peer (tli, tag.number_start)
						else
							top_li.add_peer (tli, 0)
						end
						if attached top_li.parent as lp then
							--lp.extend (tli)
							replace_top_list (tli)
							spc.put (tli)
						else
							-- ERROR; void parent???
							record_error (
								"List item at line %%s missing parent",
								<< tline.line_number_out >>)
							spc.put (Void)
						end
					end
				elseif top_li.list_level = lvl - 1 then
					-- Item is immediate child of top
					--top_li.extend (tli)
					--top_li.add_sub (tli)
					if tag.has_number_start and tag.number_start > 1 then
						top_li.add_sub (tli, tag.number_start)
					else
						top_li.add_sub (tli, 0)
					end
					list_stack.extend (tli)
					spc.put (tli)
				else
					-- Unwind stack until a peer is found (or 
					-- empty)
					-- Bump instance number from peer, replace 
					-- peer in stack
					-- If no peer found (and stack not empty) 
					-- then extend top as first instance at level,
					-- add to stack
					unwind_list_stack (lvl)
					if list_stack.is_empty then
						-- Issue, but not necessarily an error
						-- Set new item as stack top, and first 
						-- instance of its level
						list_stack.extend (tli)
						tli.set_instance_number (1)
						tli.set_top_item (top_li)
						if attached {USML_CPLX_SECTION}(spc.item) as sponge then
							sponge.extend (tli)
						else
--							extend (tli)
						end
						spc.put (tli)
					else
						top_li := current_list
						if top_li.list_level = lvl then
							-- Top is a peer
							--top_li.add_peer (tli)
							if tag.has_number_start and tag.number_start > 1 then
								top_li.add_peer (tli, tag.number_start)
							else
								top_li.add_peer (tli, 0)
							end
							if attached top_li.parent as lp then
								--lp.extend (tli)
								replace_top_list (tli)
								spc.put (tli)
							else
								-- ERROR; void parent???
								record_error (
									"List item at line %%s missing parent",
									<< tline.line_number_out >>)
							end
						else
							-- Top is a parent
							--top_li.extend (tli)
							--top_li.add_sub (tli)
							if tag.has_number_start and tag.number_start > 1 then
								top_li.add_sub (tli, tag.number_start)
							else
								top_li.add_sub (tli, 0)
							end
							list_stack.extend (tli)
							spc.put (tli)
						end
					end
				end
			end
			extend (tli)
			lists.extend (tli)
		end

	--|--------------------------------------------------------------

	current_heading: USML_HEADING
			-- Heading at top of stack, if any
		require
			not_empty: not heading_stack.is_empty
		do
			Result := heading_stack.last
		end

	current_list: USML_LIST
			-- List at top of stack, if any
		require
			not_empty: not list_stack.is_empty
		do
			Result := list_stack.last
		end

	--|--------------------------------------------------------------

	replace_top_list (v: USML_LIST)
			-- Replace the current top item in the stack list with 'v'
		require
			not_empty: not list_stack.is_empty
		do
			list_stack.finish
			list_stack.remove
			list_stack.extend (v)
		end

	--|--------------------------------------------------------------

	purge_heading_stack
			-- Wipe out heading stack
		do
			if not heading_stack.is_empty then
				heading_stack.wipe_out
			end
		end

	purge_list_stack
			-- Wipe out list stack
		do
			if not list_stack.is_empty then
				list_stack.wipe_out
			end
		end

	--|--------------------------------------------------------------

	unwind_heading_stack (lvl: INTEGER)
			-- Purge heading stack until a suitable parent heading
			-- is found, or empty
		local
			done: BOOLEAN
			sl: like heading_stack
		do
			sl := heading_stack
			from sl.finish
			until sl.is_empty or done
			loop
				if sl.item.level = lvl - 1 then
					done := True
				else
					sl.remove
					sl.finish
				end
			end
		end

	--|--------------------------------------------------------------

	unwind_list_stack (lvl: INTEGER)
			-- Unwind stack until a peer or parent item
			-- is found, or empty
		local
			done: BOOLEAN
			sl: like list_stack
		do
			sl := list_stack
			from sl.finish
			until sl.is_empty or done
			loop
				if sl.item.list_level <= lvl then
					done := True
				else
					sl.remove
					sl.finish
				end
			end
		end

	--|--------------------------------------------------------------

	list_guardian (cli: USML_LIST): detachable USML_LIST
			-- Best-guess "parent" of child list item 'cli'
			-- If cli.list_level = 1, then Result is top_item,
			-- else is closest item with level 1 less than cli
		local
			ti: USML_LIST
			lvl: INTEGER
		do
			if not lists.is_empty then
				lvl := cli.list_level
				lists.finish
				if lvl = 1 then
					-- Get top_item
					Result := lists.item.top_item
				else
					from
					until attached Result or lists.before
					loop
						ti := lists.item
						if ti.list_level = lvl - 1 then
							Result := ti
						else
							lists.back
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	print_heading_stack
			-- Print contents of heading stack to console
		local
			sl: like heading_stack
		do
			print ("Heading stack:%N")
			sl := heading_stack
			from sl.start
			until sl.after
			loop
				print (sl.item.private_text)
				sl.forth
			end
		end

	print_list_stack
			-- Print contents of list stack to console
		local
			sl: like list_stack
		do
			print ("List stack:%N")
			sl := list_stack
			from sl.start
			until sl.after
			loop
				print (sl.item.private_text)
				sl.forth
			end
		end

	--|--------------------------------------------------------------

	outline_numbering_enabled: BOOLEAN
		do
			if private_format.outline_numbering_has_changed then
				Result := private_format.outline_numbering_enabled
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_document: BOOLEAN = True
			-- Is Current a document? Of course it is

	stream_length: INTEGER
			-- Length of original stream whence Current sprang

	type_label: STRING
		do
			Result := "Document"
		end

	has_title: BOOLEAN

 	states: TWO_WAY_LIST [USML_DOC_STATE]
			-- Stack-like structure to track document states

	doc_depth: INTEGER

 	top_state: USML_DOC_STATE
 		do
 			Result := states.last
 		end

	heading_stack: TWO_WAY_LIST [USML_HEADING]
  		do
  			Result := top_state.heading_stack
  		end

	headings: TWO_WAY_LIST [USML_HEADING]

	lines: TWO_WAY_LIST [USML_LINE]
			-- Input stream as sequence of lines

	is_initialized: BOOLEAN
			-- Has Current been initialized?

	total_chars: INTEGER
			-- Number of chars in tag objects in Current
			-- SHOULD be same as chars in rendered text
		do
			if is_initialized then
				from items.start
				until items.after
				loop
					Result := Result + items.item.text.count
					items.forth
				end
			end
		end

	images: HASH_TABLE [USML_IMAGE_TAG, STRING]
			-- Image tags

	list_stack: TWO_WAY_LIST [USML_LIST]
			-- Stack-like structure to track list item affiliation

	line_number: INTEGER
			-- Current line number, in input stream, being processed

	tags: TWO_WAY_LIST [USML_TAG]

	includes: TWO_WAY_LIST [USML_TAG]

	title: STRING

	macros_table: HASH_TABLE [USML_TAG, STRING]
			-- Table of tag macros (macro tags), keyed by name

	reference_links: HASH_TABLE [USML_REF, STRING]
			-- Table of defined cross-reference links, within Current,
			-- keyed by reference ID

	lists: TWO_WAY_LIST [USML_LIST]

	heading_instance_number: INTEGER
			-- Instance number of most recently added heading, if any

	last_heading_with_level (v: INTEGER): detachable USML_HEADING
			-- Most recent heading with level 'v', if any
			-- Takes into account the document state
		local
			th: USML_HEADING
		do
			if not heading_stack.is_empty then
				from heading_stack.finish
				until attached Result or heading_stack.before
				loop
					th := heading_stack.item
					-- TODO this should be handled by state stack
					if in_subdoc = th.in_subdoc and then th.level = v then
						Result := th
					else
						heading_stack.back
					end
				end
			end
		end

	last_heading_instance_number: INTEGER
			-- Instance number of most recently added heading
			-- Takes into account the document state
		local
			th: USML_HEADING
		do
			if not headings.is_empty then
				from headings.finish
				until Result /= 0 or headings.before
				loop
					th := headings.item
					if in_subdoc = th.in_subdoc then
						Result := th.instance_number
					else
						headings.back
					end
				end
			end
		end

	list_instance_number: INTEGER
			-- Instance number of most recently added list, if any

	--|--------------------------------------------------------------

	format: USML_FORMAT
			-- Format of Current in its present state
		do
			Result := Precursor
			if outline_numbering_enabled /= Result.outline_numbering_enabled then
				if outline_numbering_enabled then
					Result.enable_outline_numbering
				end
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_title (v: STRING)
		require
			no_title: not has_title
		do
 			title.wipe_out
 			title.append (v)
			has_title := True
		end

	enable_outline_numbering
		do
			private_format.enable_outline_numbering
		end

	--|--------------------------------------------------------------

	set_values_from_tag (v: USML_TAG)
		do
			Precursor (v)
			if v.has_number_format then
				enable_outline_numbering
			end
		end

	--|--------------------------------------------------------------

	set_default_format_from_tag (tag: USML_TAG)
			-- Set the default format for element type where 
			-- format includes values from 'tag'
		require
			is_format_tag: tag.is_format
		local
			dfmt: USML_FORMAT
			lvl, i, lim: INTEGER
			ta: ARRAY [INTEGER]
		do
			-- Check first for 'wildcard' types
--			inspect tag.section_type
			lvl := tag.format_level
			inspect lvl
			when K_usml_tag_h, K_usml_tag_l then
--				if tag.section_type = K_usml_tag_h then
				if lvl = K_usml_tag_h then
					-- Is a heading wildcard
					-- Set default values for all heading levels
					-- other than title
					ta := heading_types
				else
					-- Is a list item wildcard
					-- Set default values for all list item level
					ta := list_item_types
				end
				lim := ta.upper
				from i := ta.lower
				until i > lim
				loop
					if attached tag.format as fmt then
						lvl := ta.item (i)
						dfmt := default_format_by_level (lvl)
						set_default_format (dfmt, fmt)
					else
						-- ERROR?
					end
					i := i + 1
				end
			when K_usml_tag_v_open then
				-- format verbatim sections
				if attached tag.format as fmt then
					dfmt := default_format_by_level (lvl)
					set_default_format (dfmt, fmt)
				else
					-- ERROR?
				end
			else
				if attached tag.format as fmt then
					dfmt := default_format_by_level (tag.level)
					set_default_format (dfmt, fmt)
				else
					-- ERROR?
				end
			end
		end

	old_set_default_format_from_tag (tag: USML_TAG)
			-- Set the default format for element type where 
			-- format includes values from 'tag'
		require
			is_format_tag: tag.is_format
		local
			bf, itf, uf, stf, lwf: BOOLEAN
			ps, cv, vs, av, lm, rm, ts, bs: INTEGER
			ff, rc, pt: STRING
			fmt: USML_FORMAT
			lvl: INTEGER
		do
			lvl := tag.level
			fmt := default_format_by_level (lvl)
			-- Char formats
			-- TODO switch to using fmt.change_map
			if attached tag.text_font_family as tff and then not tff.is_empty then
				ff := tff
			elseif attached fmt.font_family as dff and then not dff.is_empty then
				ff := dff
			else
				ff := dflt_font_family (lvl)
			end
			if tag.has_point_size then
				ps := tag.text_point_size
			else
				ps := fmt.point_size
			end
			if tag.has_case_value then
				cv := tag.text_case_value
			else
				cv := fmt.case_value
			end
			if tag.has_vertical_offset then
				vs := tag.text_vertical_offset
			else
				vs := fmt.vertical_offset
			end
			if tag.has_bold then
				bf := tag.text_is_bold
			else
				bf := fmt.is_bold
			end
			if tag.has_italic then
				itf := tag.text_is_italic
			else
				itf := fmt.is_italic
			end
			if tag.has_underline then
				uf := tag.text_is_underlined
			else
				uf := fmt.is_underlined
			end
			if tag.has_strike_through then
				stf := tag.text_is_strike_through
			else
				stf := fmt.is_strike_through
			end
			if attached tag.text_rgb_color as c and then not c.is_empty then
				rc := c
			elseif attached fmt.text_color as dtc and then not dtc.is_empty then
				rc := dtc
			else
				rc := Ks_dflt_font_color
			end
			-- Par formats
			if tag.has_alignment then
				av := tag.alignment_value
			else
				av := fmt.alignment_value
			end
			if tag.has_left_margin then
				lm := tag.left_margin
			else
				lm := fmt.left_margin
			end
			if tag.has_right_margin then
				rm := tag.right_margin
			else
				rm := fmt.right_margin
			end
			if tag.has_top_margin then
				ts := tag.top_margin
			else
				ts := fmt.top_margin
			end
			if tag.has_bottom_margin then
				bs := tag.bottom_margin
			else
				bs := fmt.bottom_margin
			end
			if attached tag.pre_text as t and then not t.is_empty then
				pt := t
			else
				pt := ""
			end
			fmt.set_char_values (ff, rc, ps, cv, vs, bf, itf, uf, stf)
			fmt.set_par_values (av, lm, rm, ts, bs, lwf, pt)
			fmt.set_number_separator (tag.number_separator)
			fmt.set_number_format (tag.number_format)
		end

	--|--------------------------------------------------------------

	set_default_format (dfmt, fmt: USML_FORMAT)
			-- Update values in default format 'dfmt',
			-- with new values from 'fmt'
			-- Set only values marked as changed in 'fmt'
		local
			map: ARRAY [BOOLEAN]
		do
			map := fmt.change_map
			-- Char formats
			if map.item (K_ft_font_family) then
				if attached fmt.font_family as tff and then not tff.is_empty then
					dfmt.set_font_family (tff)
				end
			end
			if map.item (K_ft_point_size) then
				dfmt.set_point_size (fmt.point_size)
			end
			if map.item (K_ft_case_value) then
				dfmt.set_case_value (fmt.case_value)
			end
			if map.item (K_ft_vertical_offset) then
				dfmt.set_vertical_offset (fmt.vertical_offset)
			end
			if map.item (K_ft_is_bold) then
				dfmt.set_is_bold (fmt.is_bold)
			end
			if map.item (K_ft_is_italic) then
				dfmt.set_is_italic (fmt.is_italic)
			end
			if map.item (K_ft_is_underlined) then
				dfmt.set_is_underlined (fmt.is_underlined)
			end
			if map.item (K_ft_is_strike_through) then
				dfmt.set_is_strike_through (fmt.is_strike_through)
			end
			if map.item (K_ft_text_color) then
				if attached fmt.text_color as txc and then not txc.is_empty then
					dfmt.set_dflt_text_color (txc)
				end
			end
			if map.item (K_ft_bg_color) then
				if attached fmt.bg_color as bgc and then not bgc.is_empty then
					dfmt.set_dflt_bg_color (bgc)
				end
			end
			-- Par formats
			if map.item (K_ft_alignment_value) then
				dfmt.set_alignment_value (fmt.alignment_value)
			end
			if map.item (K_ft_margin_left) then
				dfmt.set_left_margin (fmt.left_margin)
			end
			if map.item (K_ft_margin_right) then
				dfmt.set_right_margin (fmt.right_margin)
			end
			if map.item (K_ft_margin_top) then
				dfmt.set_top_margin (fmt.top_margin)
			end
			if map.item (K_ft_margin_bottom) then
				dfmt.set_bottom_margin (fmt.bottom_margin)
			end
			if map.item (K_ft_pre_text) then
				if attached fmt.pre_text as pt and then not pt.is_empty then
					dfmt.set_pre_text (pt)
				end
			end
			if map.item (K_ft_num_sep) then
				if attached fmt.number_separator as ns and then not ns.is_empty then
					dfmt.set_number_separator (ns)
				end
			end
			if map.item (K_ft_num_fmt) then
				dfmt.set_number_format (fmt.number_format)
			end
			-- Some format value are not valid for defaults
			-- These include start number, ref and xref, intext,
			-- and no_inlines
		end

	--|--------------------------------------------------------------

	set_default_font_family (v: STRING; lvl: INTEGER)
			-- Set the default font family for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_font_family (v)
		end

	set_default_point_size (v, lvl: INTEGER)
			-- Set the default point size for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_point_size (v)
		end

	set_default_bold (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default bold for element level 'lvl' to 'tf'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_bold (tf)
		end

	set_default_italic (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default italic for element level 'lvl' to 'tf'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_italic (tf)
		end

	set_default_underlined (tf: BOOLEAN; lvl: INTEGER)
			-- Set the default underlined for element level 'lvl' to 'tf'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_is_underlined (tf)
		end

	set_default_case_value (v, lvl: INTEGER)
			-- Set the default case value for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_case_value (v)
		end

	set_default_color (v: STRING; lvl: INTEGER)
			-- Set the default color for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_text_color (v)
		end

	--|--------------------------------------------------------------

	set_default_left_margin (v, lvl: INTEGER)
			-- Set the default left margin for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_left_margin (v)
		end

	set_default_right_margin (v, lvl: INTEGER)
			-- Set the default right margin for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_right_margin (v)
		end

	set_default_top_margin (v, lvl: INTEGER)
			-- Set the default top margin for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_top_margin (v)
		end

	set_default_bottom_margin (v, lvl: INTEGER)
			-- Set the default bottom margin for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_bottom_margin (v)
		end

	set_default_alignment_value (v, lvl: INTEGER)
			-- Set the default alignment value for element level 'lvl'
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			fmt.set_alignment_value (v)
		end

	--|--------------------------------------------------------------

	set_heading_instance_number (v: INTEGER)
		do
			--top_state.set_heading_instance_number (v)
			heading_instance_number := v
		end

	set_list_instance_number (v: INTEGER)
		do
			--top_state.set_list_instance_number (v)
			list_instance_number := v
		end

	increment_heading_instance
		do
--			top_state.increment_heading_instance
			set_heading_instance_number (heading_instance_number + 1)
		end

	increment_list_instance
		do
--			top_state.increment_list_instance
			set_list_instance_number (list_instance_number + 1)
		end

	--|--------------------------------------------------------------

	unroll_items
			-- All sections within Current, with outlined items and 
			-- their subs unrolled into a flat sequence
		local
			ti: USML_SECTION
			til: TWO_WAY_LIST [like item]
			uil: like items_unrolled
			sid: INTEGER
		do
			create til.make
			uil := items_unrolled
			sid := 1
			from uil.start
			until uil.after
			loop
				ti := uil.item
				til.extend (ti)
				ti.set_section_index (sid)
				uil.forth
				sid := sid + 1
			end
			all_items := til
		end

	--|--------------------------------------------------------------

	items_unrolled: TWO_WAY_LIST [USML_SECTION]
			-- All sections within Current, with outlined items and 
			-- their subs unrolled into a flat sequence
		local
			ti: USML_SECTION
		do
			create Result.make
			from start
			until after
			loop
				ti := item
				if ti.is_title then
					Result.extend (ti)
				elseif attached {USML_HEADING} ti as sec then
					-- Document maintains headings as top-level items, so 
					-- no recursion here
					Result.extend (ti)
					--Result.append (sec.items_unrolled)
				elseif attached {USML_LIST} ti as lst then
					Result.extend (ti)
					Result.append (lst.items_unrolled)
				else
					Result.extend (ti)
				end
				forth
			end
		end

--|========================================================================
feature -- Access
--|========================================================================

	all_items: detachable TWO_WAY_LIST [like item]
			-- All items in Current and its subordinate elements, 
			-- unrolled and numbered

	item: USML_SECTION
			-- Item at cursor
		do
			Result := items.item
		end

	first: like item
			-- First item in items
		do
			Result := items.first
		end

	last: like item
			-- Last item in items
		do
			Result := items.last
		end

	i_th alias "[]", at alias "@" (i: INTEGER): like item
			-- Item at `i'-th position
		do
			Result := items.i_th (i)
		end

	i_th_section (i: INTEGER): detachable like item
			-- Item, in 'all_items' at `i'-th position
		do
			if attached all_items as ail then
				Result := ail.i_th (i)
			end
		end

	--|--------------------------------------------------------------

	section_by_src_position (v: INTEGER): detachable USML_SECTION
			-- Section, if any, at position 'v' in document text (where 
			-- text includes tag)
		local
			sp, ep: INTEGER
			ti: like item
		do
--TODO CANNOT WORK THIS WAY
-- loses macro and format tags (not doc section)
-- Need to track original stream
-- Likely best to do in application that creates doc, not here
			from start
			until attached Result or after
			loop
				ti := item
				sp := ti.plain_text_tag_start
				ep := ti.plain_text_txt_start + ti.raw_text.count - 1
				if v >= sp then
					if v <= ep then
						Result := ti
					else
						-- keep looking
					end
				else
					-- past it
				end
				forth
			end
		end

	section_by_rendered_position (v: INTEGER): detachable USML_SECTION
			-- Section, if any, at position 'v' in RENDERED document text
		local
			sp, ep: INTEGER
			ti: like item
		do
			from start
			until attached Result or after
			loop
				ti := item
				sp := ti.rendered_start
				ep := ti.rendered_end
				if v >= sp then
					if v <= ep then
						Result := ti
					else
						-- keep looking
					end
				else
					-- past it
				end
				forth
			end
		end

	reference_target_by_label (v: STRING): detachable USML_REF
			-- Reference target with label 'v', if any
		do
			Result := reference_targets.item (v)
			if not attached Result then
				Result := live_reference_targets.item (v)
			end
		end

	image_tag_by_label (v: STRING): detachable USML_IMAGE_TAG
			-- Embedded image with label 'v', if any
		do
			Result := images.item (v)
		end

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

	cursor: CURSOR do Result := items.cursor end
			-- Items cursor

	valid_cursor (v: like cursor): BOOLEAN
			-- Is the given cursor valid?
		do
			Result := items.valid_cursor (v)
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	extend (v: like item)
			-- Add item 'v' to end of items
		do
			items.extend (v)
			v.set_parent (Current)
		end

	--|--------------------------------------------------------------

	extend_restarted (v: USML_SECTION; sn: INTEGER)
			-- Add item 'v' to end of items
			-- Restart numbering by flushing headings stack
		do
			if attached {USML_HEADING}v as th then
--				headings.wipe_out
				th.set_instance_number (sn)
				headings.extend (th)
			elseif v.is_list then
--TODO is this OK?? seems to be, but ..
				lists.wipe_out
				v.set_instance_number (sn)
			end
			extend (v)
		end

	--|--------------------------------------------------------------

	extend_outlined (v: USML_SECTION; lvl: INTEGER)
			-- Add item 'v' to end of items
			-- Set outline number according to level and type
		require
			outlined: v.is_heading or v.is_list
			valid_level: lvl > 0
		local
			num: INTEGER
		do
			if attached {USML_HEADING} v as vh then
				if attached last_heading_with_level (lvl) as th then
					num := th.instance_number + 1
				else
					-- Hmmm; Makes no sense, and was checked before?
					num := 1
				end
				headings.extend (vh)
				vh.set_instance_number (num)
				-- Add to items, but bypass 'extend', lest parent be 
				-- clobbered
--				items.extend (vh)
				extend (vh)
			elseif v.is_list then
--maybe
				increment_list_instance
				extend (v)
			end
		end

	--|--------------------------------------------------------------

	add_reference_target (ref: USML_REF)
			-- Add 'ifmt' to reference_targets
		do
			reference_targets.extend (ref, ref.label)
		end

	add_live_reference_target (ref: USML_REF)
			-- Add 'ifmt' to 'live' reference_targets (targets in 
			-- rendered text)
		do
			live_reference_targets.extend (ref, ref.label)
		end

	clear_live_reference_targets
			-- Remove all 'live' reference_targets (targets in 
			-- rendered text)
		do
			live_reference_targets.wipe_out
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	out: STRING
			-- Contents of Current as document stream
		local
			ti: USML_SECTION
		do
			create Result.make (stream_length)
			from start
			until after
			loop
				ti := item
				if ti.is_title then
					-- if format is default, just the tag, else include 
					-- the format parameters
--					Result.append ("<usml ht>%N" + ti.private_text + "%N")
				elseif ti.is_heading then
				elseif ti.is_list then
				else
				end
				forth
			end
		end

	sections_out: STRING
		do
			Result := items_out
		end

	--|--------------------------------------------------------------

	headings_out: STRING
		local
			ti: USML_SECTION
			lvl: INTEGER
		do
			create Result.make (2048)
			from start
			until after
			loop
				ti := item
				if ti.is_heading or ti.is_title then
					lvl := ti.level
					if ti.is_title then
						Result.append (ti.text + "%N")
					elseif attached {USML_OUTLINED} ti as sec then
						Result.append (sec.outline_number + " " + sec.text + "%N")
						if not sec.is_empty then
							Result.append (sec.headings_out)
						end
					end
				end
				forth
			end
		end

	--|--------------------------------------------------------------

	text_out: STRING
			-- Text-only from Current and its items
		local
			ti: USML_SECTION
			--lvl: INTEGER
		do
			create Result.make (4096)
			Result.append (text)
			from start
			until after
			loop
				ti := item
				if ti.is_title then
					Result.append (ti.text)
				elseif attached {USML_SECTION} ti as sec then
					Result.append (sec.text_out)
				end
				forth
			end
		end

	--|--------------------------------------------------------------

	formats_out: STRING
		local
			ti: USML_SECTION
			fmt: USML_FORMAT
		do
			create Result.make (2048)
			from start
			until after
			loop
				ti := item
				Result.append ("(" + ti.level.out + ")")
				fmt := ti.format
				Result.append (fmt.out)
				Result.append ("%N")
				forth
			end
		end

	--|--------------------------------------------------------------

	tags_out: STRING
		local
			tag: USML_TAG
		do
			create Result.make (2048)
			from tags.start
			until tags.after
			loop
				tag := tags.item
				Result.append (tag.tag_text + "%N")
				tags.forth
			end
		end

	lines_out: STRING
		do
			create Result.make (2048)
			from lines.start
			until lines.after
			loop
				Result.append (lines.item.out)
				lines.forth
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	reference_targets: HASH_TABLE [USML_REF, STRING]
			-- Table of reference (link) targets, within Current,
			-- keyed by reference ID (FROM DOCUMENT SOURCE)

	live_reference_targets: HASH_TABLE [USML_REF, STRING]
			-- Table of reference (link) targets, within Current,
			-- keyed by reference ID (from RENDERED TEXT)

	preprocess_depth: INTEGER

	preprocess (v: STRING; src: detachable STRING)
			-- From stream 'v', apply preprocessing steps:
			--   Convert all CRLF instances into simple %N
			--   Identify and expand included files
			--   Add to 'lines' all expanded text as line objects
			--
			-- Do not interpret any tags other than comment and include.
			-- 'src' is file name (if known)
		local
			sp, lim, lno: INTEGER
			pcell: CELL [INTEGER]
			strm, line, low_line, ts, cof, emsg, parg, buf, tln: STRING
			ts32: STRING
			lnlst: LIST [STRING]
			tline: USML_LINE
			tf: RAW_FILE
			itag: USML_IMAGE_TAG
			mtag: USML_TAG
			tdata: USML_TAG_DATA
			tref: USML_REF
			intoff: BOOLEAN
		do
			preprocess_depth := preprocess_depth + 1
			lno := 0
			strm := stream_eol_converted_text (v)
			lim := strm.count

			create pcell.put (1)
			create emsg.make (0)
			from sp := 1
			until sp > lim or sp = 0 or has_error
			loop
				pcell.put (sp)

				line := next_line_iq (strm, pcell)
				low_line := line.as_lower
				lno := lno + 1
				emsg.wipe_out
--				if preprocess_depth = 1 then
--					line_number := lno
--				end
				if is_verbatim_open_tag (line) then
					-- Suspend any interpretation until after the next 
					-- verbatim close tag
					intoff := True
					create tline.make_with_text (line, lno, sp, False)
					tline.set_context (src, lno)
					lines.extend (tline)
				elseif is_verbatim_close_tag (line) then
					-- Resume interpretation at next line
					intoff := False
					create tline.make_with_text (line, lno, sp, False)
					tline.set_context (src, lno)
					lines.extend (tline)
				elseif intoff then
					create tline.make_with_text (line, lno, sp, False)
					tline.set_context (src, lno)
					lines.extend (tline)
				elseif is_xcomment_open_tag (line, emsg) then
					if emsg.is_empty then
						-- Capture comment position for later, then
						-- collect comment text, and adjust line number to 
						-- allow for lines in comment
						create buf.make (line.count * 4)
						--create tline.make_with_text (line, lno, pcell.item, 
						--False)
						create tline.make_with_text (line, lno, sp, False)
						tline.set_context (src, lno)
						lines.extend (tline)
						buf.append (line)
						buf.append ("%N")
						-- original text can have a %R at end, stripped 
						-- from 'line'
						sp := sp + line.count + 1
						pcell.put (sp)
						ts := text_to_next_xclose (strm, emsg, pcell, False)
						if emsg.is_empty then
							buf.append (ts)
							-- lno has not been updated since reading
							-- the open tag and needs to be updated now
							-- e.g.  <open tag>  lno=10
							--        a line
							--        a line
							--        a line
							--       <close tag>  lno=14
							--       No newline at end of buf
							-- lno or close = old lno + number of %N in buf
							lno := lno + buf.occurrences ('%N')
						else
							-- ERROR
							set_error_message (emsg + "; at line " + lno.out)
						end
					else
						set_error_message (emsg + "; at line " + lno.out)
					end
				elseif is_image_open_tag (line) then
					-- Capture, as image, lines up to next xclose tag
					create itag.make_with_text (line)
					itag.set_src_position (sp)
					itag.set_src_line_number (lno)
--TODO have itag validate image name vs existing ref targets
					if attached itag.error_message as msg then
						set_error_message (msg + "; at line " + lno.out)
					else
						-- Get image content
						pcell.put (sp)
						ts32 := image_from_stream (strm, emsg, pcell)
						if emsg.is_empty then
							-- ts includes trailing newline
							-- Prune it
							ts32.remove (ts32.count)
							itag.set_image_text (ts32)
							images.extend (itag, itag.name)
							-- Add a reference target for image
							create tref.make_for_int_image (itag.name)
							add_reference_target (tref)
--							ts := carriage_returns_converted (ts32)
--							create tline.make_with_text (ts, lno, pcell.item, True)
--							tline.set_context (src, lno)
--							lines.extend (tline)
						end
						-- line with closing tag was omitted, and newline 
						-- before it was pruned, so add 2?????
 						lno := lno + ts32.occurrences ('%N') + 2
					end
				elseif starts_with_xopen_tag (line, emsg) then
					-- multiline open, but not a multiline comment
					-- Look for common syntax error for xcomments
 					if not emsg.is_empty then
						record_warning ("%%s at line %%s",<< emsg, lno.out >>)
 					else
						-- Multi-line tags get compacted within the 
						-- document, losing their tag length (newlines and 
						-- leading spaces are lost, for example)
						-- Need to capture start line, yes, but also 
						-- capture the closing tag's line number somehow
						--create tline.make_with_text (line, lno, pcell.item, 
						--True)
						create tline.make_with_text (line, lno, sp, True)
						tline.set_context (src, lno)
						lines.extend (tline)

						-- sp is position of start of line.
						-- CRLFs need to be stripped before this point, but 
						-- CRs (%N) are still there
						sp := sp + line.count + 1

						-- tight-spin here, rather than in outer loop
						--pcell.put (sp)
						ts := text_to_next_xclose (strm, emsg, pcell, False)
						-- pcell has position of character following
						-- next xclose tag (i.e. next line after multi line 
						-- tag
						if emsg.is_empty then
							-- Break buf into lines and add to 'lines'
							-- lno is off because xopen tag cannot have
							-- any trailing text
							lno := lno + 1
							lnlst := ts.split ('%N')
							from lnlst.start
							until lnlst.after
							loop
								tln := lnlst.item
								create tline.make_with_text (
--									lnlst.item, lno, pcell.item, True)
									tln, lno, sp, True)
								tline.set_context (src, lno)
								lines.extend (tline)
								lnlst.forth
								if not lnlst.after then
									lno := lno + 1
									sp := sp + tln.count + 1
								end
							end
							--pcell.put (sp)
						else
							-- ERROR
							set_error_message (emsg + "; at line " + lno.out)
						end
					end
				elseif is_tag_like (line) then
					-- Only really interesting open tag is an include tag
					-- BUT, all lines need counting for subsequent 
					-- operations!!
					-- Macro tags need to be captured for subsequent 
					-- identfiication as legit tags, but only label is 
					-- needed during this process
					create emsg.make (0)
					create parg.make (0)
					if starts_with_include_tag (line, parg) then
						-- Include tag
						-- include_contents_of_file (parg)
						create tf.make_with_name (parg)
						if not tf.exists then
							set_error_message (
								aprintf ("Include file [%%s] does not exist.",
								<< ts >>))
						else
							tf.open_read
							tf.read_stream (tf.count)
							cof := tf.last_string
							tf.close
							-- Contents of file does not have its newlines 
							-- stripped (not having been processed using 
							-- next_line_iq, so no repair is needed
							if preprocess_depth >= K_max_preprocess_depth then
								-- ERROR
								set_error_message ("Max include depth exceeded")
							else
								preprocess (cof, ts)
							end
						end
					elseif is_comment_tag (line) then
						-- discard text but capture comment instance and 
						-- line count
						--create tline.make_with_text (line, lno, pcell.item, 
						--False)
						create tline.make_with_text (line, lno, sp, False)
						tline.set_context (src, lno)
						lines.extend (tline)
						--sp := sp + line.count + 1
						--pcell.put (sp)
						-- Do a tight spin on the comment text, rather than 
						-- have the outer loop do it
						ts := text_to_next_tag (strm, emsg, pcell)
						-- char count includes newlines and (on Windows) %Rs
						-- so add 1 cpos for each preceding line when 
						-- debugging on Windows
						if emsg.is_empty then
							lno := lno + ts.occurrences ('%N')
						else
							-- ERROR
							set_error_message (emsg + "; at line " + lno.out)
						end
					else
						-- some other tag; snag it
						--create tline.make_with_text (line, lno, pcell.item, 
						--True)
						create tline.make_with_text (line, lno, sp, True)
						tline.set_context (src, lno)
						lines.extend (tline)
						sp := sp + line.count + 1
						-- Check if a macro
						if line.count >= 3 and then
							line.item (1) = '<' and line.index_of ('>', 2) > 2
						 then
							 -- Looks like it might be a tag
							 -- Could an open tag, close tag or false alarm
							 create tdata.make_from_text (line)
							if tdata.is_macro_definition then
								create mtag.make_from_tag_data (tdata)
								if attached mtag.macro_name as mnm then
									macros.extend (mtag, mnm)
								end
							end
						end
					end
				else
					-- non-tag line; snag it
					--create tline.make_with_text (line, lno, pcell.item, 
					--True)
					create tline.make_with_text (line, lno, sp, True)
					tline.set_context (src, lno)
					lines.extend (tline)
					sp := sp + line.count + 1
					pcell.put (sp)
				end
				sp := pcell.item
			end
			preprocess_depth := preprocess_depth - 1
		end

	--|--------------------------------------------------------------

	text_to_next_xclose (v, emsg: STRING; pc: CELL [INTEGER]; pf: BOOLEAN): STRING
			-- From stream 'v', beginning with a multi-line open tag at 
			-- 'pc.item', all text UP TO AND INCLUDING the closing 
			-- tag corresponding to the open.
			-- Key rules:
			--   Multiline tags have an open tag that defines the type 
			--   of tag and section and nothing else
			--   Multiline tags cannot have trailing text
			--   All lines between open and closing tags are considered 
			--   parameters
			--   Multi-line tags cannot be nested
			--   Multi-line tags cannot have embedded include directives
			--   Multi-line tags cannot have embedded comments
			-- If 'pf', prune the trailing close tag and its preceding '%N'
		local
			sp, lim: INTEGER
			done: BOOLEAN
			line: STRING
		do
			create Result.make (128)
			lim := v.count
			from sp := pc.item
			until done or sp > lim or sp = 0 or has_error
			loop
				pc.put (sp)
				line := next_line_iq (v, pc)
				-- Put back the newline stripped during extraction
				if is_x_close (line) then
					if not pf then
						Result.append (line)
					end
					done := True
				else
					Result.append (line)
					Result.append ("%N")
				end
				sp := pc.item
			end
		end

	--|--------------------------------------------------------------

	text_to_next_tag (v, emsg: STRING; pc: CELL [INTEGER]): STRING
			-- From stream 'v', beginning at position 'pc.item', text
			-- UP TO AND NOT INCLUDING the next line beginning with a
			-- usml tag OR WHAT LOOKS LIKE ONE
			-- N.B. During preprocess, macros have yet to be identified 
			-- and defined UGH!  Because this function is called ONLY 
			-- during preprocess, it needs to be very forgiving about 
			-- what is and is not the "next tag"
		local
			sp, lim: INTEGER
			done: BOOLEAN
			line: STRING
		do
			create Result.make (128)
			lim := v.count
			from sp := pc.item
			until done or sp > lim or sp = 0 or has_error
			loop
				pc.put (sp)
				line := next_line_iq (v, pc)
				if starts_with_open_tag (line, False) then
					-- Put back the line, incl. newline
					--Result.append ("%N")
					pc.put (sp)
					done := True
				else
					Result.append (line)
					-- Put back the newline stripped during extraction
					Result.append ("%N")
					sp := pc.item
				end
			end
		end

	--|--------------------------------------------------------------

	next_line_test_string: STRING
		once
			Result := "{
this is a test of line scraping
This is line 2
This is line 3, the next line is empty

and this is line 5
}"
		end

--|========================================================================
feature -- Defaults
--|========================================================================

	--|--------------------------------------------------------------

	initialize_defaults
			-- Initialize per-document default formats
		do
			create default_format_p.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p, K_usml_tag_p,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 0, 0, 0, 6, False, "")
			default_format_p.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_v.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p, K_usml_tag_p,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 0, 0, 0, 6, False, "")
			default_format_v.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_doc_title.make_with_values (
				K_dflt_font_family_doc_title, Ks_dflt_font_color_doc_title,
				K_usml_tag_title,
				K_dflt_point_size_doc_title, 0, 0, True, False, False, False,
				K_align_center, 0, 0, 0, 15, False, "")
			default_format_doc_title.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_h1.make_with_values (
				K_dflt_font_family_h1, Ks_dflt_font_color_h1,
				K_usml_tag_h1,
				K_dflt_point_size_h1, 0, 0, True, False, False, False,
				K_align_left, 0, 0, 10, 10, False, "")
			default_format_h1.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_h2.make_with_values (
				K_dflt_font_family_h2, Ks_dflt_font_color_h2,
				K_usml_tag_h2,
				K_dflt_point_size_h2, 0, 0, True, False, False, False,
				K_align_left, 0, 0, 10, 10, False, "")
			default_format_h2.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_h3.make_with_values (
				K_dflt_font_family_h3, Ks_dflt_font_color_h3,
				K_usml_tag_h3,
				K_dflt_point_size_h3, 0, 0, True, False, False, False,
				K_align_left, 0, 0, 6, 6, False, "")
			default_format_h3.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_h4.make_with_values (
				K_dflt_font_family_h4, Ks_dflt_font_color_h4,
				K_usml_tag_h4,
				K_dflt_point_size_h4, 0, 0, True, True, False, False,
				K_align_left, 0, 0, 6, 6, False, "")
			default_format_h4.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_h5.make_with_values (
				K_dflt_font_family_h5, Ks_dflt_font_color_h5,
				K_usml_tag_h5,
				K_dflt_point_size_h5, 0, 0, False, True, False, False,
				K_align_left, 0, 0, 6, 6, False, "")
			default_format_h5.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_l1.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p,
				K_usml_tag_l1,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 5, 0, 6, 6, False, "")
			default_format_l1.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_l2.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p,
				K_usml_tag_l2,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 10, 0, 0, 0, False, "")
			default_format_l2.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_l3.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p,
				K_usml_tag_l3,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 15, 0, 0, 0, False, "")
			default_format_l3.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_l4.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p,
				K_usml_tag_l4,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 20, 0, 0, 0, False, "")
			default_format_l4.set_bg_color (Ks_dflt_bgcolor_text)
			create default_format_l5.make_with_values (
				K_dflt_font_family_p, Ks_dflt_font_color_p,
				K_usml_tag_l5,
				K_dflt_point_size_p, 0, 0, False, False, False, False,
				K_align_left, 25, 0, 0, 0, False, "")
			default_format_l5.set_bg_color (Ks_dflt_bgcolor_text)
		end

	--|--------------------------------------------------------------

	default_format_p: USML_FORMAT

	default_format_v: USML_FORMAT

	default_format_doc_title: USML_FORMAT

	default_format_h1: USML_FORMAT

	default_format_h2: USML_FORMAT

	default_format_h3: USML_FORMAT

	default_format_h4: USML_FORMAT

	default_format_h5: USML_FORMAT

	--|--------------------------------------------------------------

	default_format_l1: USML_FORMAT

	default_format_l2: USML_FORMAT

	default_format_l3: USML_FORMAT

	default_format_l4: USML_FORMAT

	default_format_l5: USML_FORMAT

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_DOCUMENT
