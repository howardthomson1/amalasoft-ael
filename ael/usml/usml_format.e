note
	description: "{
Format specification for a USML document section
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_FORMAT

inherit
	USML_CORE
		redefine
			default_create, out
		end

create
	default_create, make_with_values, make_with_section_type,
	make_from_other, make_blended

--make_with_char_values, 
--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_values (
		f, rc: STRING; st, p, cv, vs: INTEGER; b, i, u, stf: BOOLEAN;
		av, lm, rm, ts, bs: INTEGER; lw: BOOLEAN; pt: STRING)
			-- Create Current with font and paragraph parameters
			-- 'f' is font family
			-- 'rc' is RGB color
			-- 'st' is section type
			-- 'p' is point size
			-- 'cv' is case value (0 no change, <0 lower, >0 upper)
			-- 'vs' is character vertical offset (super/sub)
			-- 'b' is bold (strong) flag
			-- 'i' is italics flag
			-- 'u' is underline flag
			-- 'stf' is strike-through flag
			-- 'av' is paragraph alignment value
			-- 'lm' is paragraph left margin
			-- 'rm' is paragraph right margin
			-- 'ts' is paragraph top spacing (in pixels)
			-- 'bs' is paragraph bottom spacing (in pixels)
			-- 'lw' is line wrap/concatenation disabled flag
 			-- 'pt' is pre-text (text before paragraph text)
		require
			valid_font_family: is_valid_font_family (f)
			valid_color: is_valid_rgb_color (rc)
			valid_point_size: p > 0
			valid_alignment: is_valid_alignment_value (av.out)
		do
			is_initializing := True
			default_create
			section_type := st
			set_char_values (f, rc, p, cv, vs, b, i, u, stf)
			set_par_values (av, lm, rm, ts, bs, lw, pt)
			is_initializing := False
		end

	make_with_section_type (v: INTEGER)
			-- Create Current with section type only (other values to be 
			-- set subsequently)
		do
			default_create
			section_type := v
		end

	--RFO make_with_char_values (f,c: STRING; p,cv,vs: INTEGER; b,i,u,st: BOOLEAN)
	--RFO 		-- Create Current with font parameters
	--RFO 		-- 'f' is font family
	--RFO 		-- 'c' is RGB color
	--RFO 		-- 'p' is point size
	--RFO 		-- 'cv' is case value (0=no change, <0=all lower, >0=all upper)
	--RFO 		-- 'vs' is vertical space (super/sub)
	--RFO 		-- 'b' is bold (strong) flag
	--RFO 		-- 'i' is italics flag
	--RFO 		-- 'u' is underline flag
	--RFO 		-- 'st' is strike-through flag
	--RFO 	require
	--RFO 		valid_font_family: is_valid_font_family (f)
	--RFO 		valid_color: is_valid_rgb_color (c)
	--RFO 		valid_point_size: p > 0
	--RFO 	do
	--RFO 		is_initializing := True
	--RFO 		default_create
	--RFO 		set_char_values (f, c, p, cv, vs, b, i, u, st)
	--RFO 		is_initializing := False
	--RFO 	end

	--|--------------------------------------------------------------

	make_from_other (v: like Current)
		do
			default_create
			init_from_other (v, True)
		end

	make_blended (newfmt, oldfmt: like Current; cf, pf: BOOLEAN)
			-- Create Current as a blend of old and new formats
			-- Give precedence to the new values, if defined,
			-- and deferring to the old values if not
			-- If 'cf' then blend character and effect values
			-- If 'pf; then blend paragraph values
		do
			make_from_other (newfmt)
			blend (oldfmt, cf, pf)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create change_map.make_filled (False, 1, K_ft_max)
			number_separator := Ks_dflt_num_sep.twin
			reference_label := ""
--			target_label := ""
			xref_target := ""
			create name.make (0)
			create description.make (0)
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_initializing: BOOLEAN
	is_for_validation: BOOLEAN

	name: STRING
	description: STRING
	section_type: INTEGER

	font_family: detachable STRING
	point_size: INTEGER
	point_size_is_relative: BOOLEAN
	case_value: INTEGER
	vertical_offset: INTEGER
	has_case_value: BOOLEAN
		do
			Result := case_value /= 0
		end
	is_upper: BOOLEAN
		do
			Result := case_value = K_case_value_upper
		end

	is_lower: BOOLEAN
		do
			Result := case_value = K_case_value_lower
		end

	is_title_case: BOOLEAN
		do
			Result := case_value = K_case_value_title
		end

	is_bold: BOOLEAN
	is_italic: BOOLEAN
	is_underlined: BOOLEAN
	is_strike_through: BOOLEAN
	text_color: detachable STRING
	bg_color: detachable STRING

	left_margin: INTEGER
	right_margin: INTEGER
	top_margin: INTEGER
	bottom_margin: INTEGER

	alignment_value: INTEGER

	is_left_aligned: BOOLEAN
		do
			Result := alignment_value = K_align_left
		end

	is_right_aligned: BOOLEAN
		do
			Result := alignment_value = K_align_right
		end

	is_center_aligned: BOOLEAN
		do
			Result := alignment_value = K_align_center
		end

	is_justified: BOOLEAN
		do
			Result := alignment_value = K_align_justified
		end

	pre_text: detachable STRING
			-- Text to precede element text, if any

	intext: detachable STRING
			-- Text included with tag (subst for following text)

	number_separator: STRING
			-- Text to separate outline number from item text

	number_format: INTEGER
			-- Format of outline numbers (if any)

	number_start: INTEGER
			-- Ordinal number for 1st instance in outline set
			-- Zero denotes continue existing numbering

	outline_numbering_enabled: BOOLEAN
			-- Is outline numbering, for headings, enabled?

	--|--------------------------------------------------------------

	reference_label: STRING

	--|--------------------------------------------------------------

	xref_target: STRING

	target_label: STRING
			-- Target label part of xref (type,label)
		local
			p: INTEGER
		do
			p := xref_target.index_of (',', 1)
			if p /= 0 then
				Result := xref_target.substring (p + 1, xref_target.count)
			else
				Result := xref_target
			end
		end

	xref_type: INTEGER
			-- Type part of xref (type,label)
		--RFO local
		--RFO 	p: INTEGER
		--RFO do
		--RFO 	p := xref_target.index_of (',', 1)
		--RFO 	if p > 1 then
		--RFO 		Result := xref_label_to_type (xref_target.substring (1, p - 1))
		--RFO 	end
		--RFO end

	--|--------------------------------------------------------------

	is_valid: BOOLEAN
		do
			if is_for_validation then
				Result := (not has_error)
					and
						(attached font_family as ff and then
						is_valid_font_family (ff))
						and
							is_valid_point_size (point_size)
							and
								(attached text_color as rc and then
								is_valid_rgb_color (rc))
			else
				Result := True
			end
		end

	has_error: BOOLEAN
		do
			Result := attached validation_error as ve and then not ve.is_empty
		end

	validation_error: detachable STRING

	--|--------------------------------------------------------------

	change_map: ARRAY [BOOLEAN]

	changes: LINKED_LIST [INTEGER]
		local
			i, lim: INTEGER
		do
			create Result.make
			lim := change_map.count
			from i := 1
			until i > lim
			loop
				if change_map.item (i) then
					Result.extend (i)
				end
				i := i + 1
			end
		end

	--|--------------------------------------------------------------

	differences (other: USML_FORMAT): LINKED_LIST [INTEGER]
			-- List of format types that differ between Current and other
		do
			create Result.make
			if font_family /~ other.font_family then
				-- stored in lower case, so simply is_equal suffices?
				Result.extend (K_ft_font_family)
			end
			if point_size /= other.point_size then
				Result.extend (K_ft_point_size)
			end
			if is_bold /= other.is_bold then
				Result.extend (K_ft_is_bold)
			end
			if is_italic /= other.is_italic then
				Result.extend (K_ft_is_italic)
			end
			if is_underlined /= other.is_underlined then
				Result.extend (K_ft_is_underlined)
			end
			if is_strike_through /= other.is_strike_through then
				Result.extend (K_ft_is_strike_through)
			end
			if case_value /= other.case_value then
				Result.extend (K_ft_case_value)
			end
			if not strings_same_ins (text_color, other.text_color) then
				Result.extend (K_ft_text_color)
			end
			if not strings_same_ins (bg_color, other.bg_color) then
				Result.extend (K_ft_bg_color)
			end
			if vertical_offset /= other.vertical_offset then
				Result.extend (K_ft_vertical_offset)
			end
			if alignment_value /= other.alignment_value then
				Result.extend (K_ft_alignment_value)
			end
			if left_margin /= other.left_margin then
				Result.extend (K_ft_margin_left)
			end
			if right_margin /= other.right_margin then
				Result.extend (K_ft_margin_right)
			end
			if top_margin /= other.top_margin then
				Result.extend (K_ft_margin_top)
			end
			if bottom_margin /= other.bottom_margin then
				Result.extend (K_ft_margin_bottom)
			end
			if not strings_same_ins (pre_text, other.pre_text) then
				Result.extend (K_ft_pre_text)
			end
			if not strings_same_ins (intext, other.intext) then
				Result.extend (K_ft_intext)
			end
			if not strings_same_ins (
				number_separator, other.number_separator) then
				Result.extend (K_ft_num_sep)
			end
			if number_format /= other.number_format then
				Result.extend (K_ft_num_fmt)
			end
			if number_start /= other.number_start then
				Result.extend (K_ft_num_start)
			end
			-- labels are case-sensitive
			if reference_label /~ other.reference_label then
				Result.extend (K_ft_ref_label)
			end
			if xref_type /~ other.xref_type then
				Result.extend (K_ft_xref_type)
			end
			-- labels are case-sensitive
			if xref_target /~ other.xref_target then
				Result.extend (K_ft_xref_target)
			end
		end

	all_params: LINKED_LIST [INTEGER]
			-- List of all format types
		do
			create Result.make
			Result.extend (K_ft_font_family)
			Result.extend (K_ft_point_size)
			Result.extend (K_ft_is_bold)
			Result.extend (K_ft_is_italic)
			Result.extend (K_ft_is_underlined)
			Result.extend (K_ft_is_strike_through)
			Result.extend (K_ft_case_value)
			Result.extend (K_ft_text_color)
			Result.extend (K_ft_bg_color)
			Result.extend (K_ft_vertical_offset)
			Result.extend (K_ft_alignment_value)
			Result.extend (K_ft_margin_left)
			Result.extend (K_ft_margin_right)
			Result.extend (K_ft_margin_top)
			Result.extend (K_ft_margin_bottom)
			Result.extend (K_ft_pre_text)
			Result.extend (K_ft_intext)
			Result.extend (K_ft_num_sep)
			Result.extend (K_ft_num_fmt)
			Result.extend (K_ft_num_start)
			Result.extend (K_ft_ref_label)
			Result.extend (K_ft_xref_target)
		end

	--|--------------------------------------------------------------

	format_has_changed (ft: INTEGER): BOOLEAN
			-- Has format type 'ft' changed since initialization?
		require
			valid: is_valid_format_type (ft)
		do
			Result := change_map.item (ft)
		end

	--|--------------------------------------------------------------

	char_formats_have_changed: BOOLEAN
			-- Have any character formats changed since initialization?
		do
			Result := font_family_has_changed or
				point_size_has_changed or
				is_bold_has_changed or
				is_italic_has_changed or
				is_underlined_has_changed or
				is_strike_through_has_changed or
				case_value_has_changed or
				vertical_offset_has_changed or
				text_color_has_changed or
				bg_color_has_changed
		end

	font_family_has_changed: BOOLEAN
			-- Has font family changed since initialization?
		do
			Result := format_has_changed (K_ft_font_family)
		end

	point_size_has_changed: BOOLEAN
			-- Has point size changed since initialization?
		do
			Result := format_has_changed (K_ft_point_size)
		end

	is_bold_has_changed: BOOLEAN
			-- Has is_bold changed since initialization?
		do
			Result := format_has_changed (K_ft_is_bold)
		end

	is_italic_has_changed: BOOLEAN
			-- Has is_italic changed since initialization?
		do
			Result := format_has_changed (K_ft_is_italic)
		end

	is_underlined_has_changed: BOOLEAN
			-- Has is_underlined changed since initialization?
		do
			Result := format_has_changed (K_ft_is_underlined)
		end

	is_strike_through_has_changed: BOOLEAN
			-- Has is_strike_through changed since initialization?
		do
			Result := format_has_changed (K_ft_is_strike_through)
		end

	case_value_has_changed: BOOLEAN
			-- Has case_value changed since initialization?
		do
			Result := format_has_changed (K_ft_case_value)
		end

	vertical_offset_has_changed: BOOLEAN
			-- Has vertical_offset changed since initialization?
		do
			Result := format_has_changed (K_ft_vertical_offset)
		end

	text_color_has_changed: BOOLEAN
			-- Has text_color changed since initialization?
		do
			Result := format_has_changed (K_ft_text_color)
		end

	bg_color_has_changed: BOOLEAN
			-- Has bg_color changed since initialization?
		do
			Result := format_has_changed (K_ft_bg_color)
		end

	--|--------------------------------------------------------------

	par_formats_have_changed: BOOLEAN
			-- Have any paragraph formats changed since initialization?
		do
			Result := left_margin_has_changed or
				right_margin_has_changed or
				top_margin_has_changed or
				bottom_margin_has_changed or
				alignment_has_changed or
				pre_text_has_changed or
				outline_numbering_has_changed
		end

	left_margin_has_changed: BOOLEAN
			-- Has left margin value changed since initialization?
		do
			Result := format_has_changed (K_ft_margin_left)
		end

	right_margin_has_changed: BOOLEAN
			-- Has right margin value changed since initialization?
		do
			Result := format_has_changed (K_ft_margin_right)
		end

	top_margin_has_changed: BOOLEAN
			-- Has top margin value changed since initialization?
		do
			Result := format_has_changed (K_ft_margin_top)
		end

	bottom_margin_has_changed: BOOLEAN
			-- Has bottom margin value changed since initialization?
		do
			Result := format_has_changed (K_ft_margin_bottom)
		end

	alignment_has_changed: BOOLEAN
			-- Has bottom magin value changed since initialization?
		do
			Result := change_map.item (K_ft_alignment_value)
		end

	pre_text_has_changed: BOOLEAN
			-- Has pre-text value changed since initialization?
		do
			Result := change_map.item (K_ft_pre_text)
		end

	intext_has_changed: BOOLEAN
			-- Has intext value changed since initialization?
		do
			Result := change_map.item (K_ft_intext)
		end

	number_separator_has_changed: BOOLEAN
			-- Has number_separator changed since initialization?
		do
			Result := change_map.item (K_ft_num_sep)
		end

	number_format_has_changed: BOOLEAN
			-- Has number_format changed since initialization?
		do
			Result := change_map.item (K_ft_num_fmt)
		end

	number_start_has_changed: BOOLEAN
			-- Has number_start changed since initialization?
		do
			Result := change_map.item (K_ft_num_start)
		end

	outline_numbering_has_changed: BOOLEAN
			-- Has outline numbering setting changed since initialization?
		do
			Result := change_map.item (K_ft_ol_nums)
		end

	reference_label_has_changed: BOOLEAN
			-- Has reference_label changed since initialization?
		do
			Result := change_map.item (K_ft_ref_label)
		end

	xref_type_has_changed: BOOLEAN
			-- Has xref_type changed since initialization?
		do
			Result := change_map.item (K_ft_xref_type)
		end

	xref_target_has_changed: BOOLEAN
			-- Has xref_target changed since initialization?
		do
			Result := change_map.item (K_ft_xref_target)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	init_from_other (v: USML_FORMAT; initf: BOOLEAN)
		do
			is_initializing := initf
			section_type := v.section_type
			if attached v.font_family as ff and then not ff.is_empty then
				set_font_family (ff)
			end
			if v.point_size_is_relative then
				set_relative_point_size (v.point_size)
			else
				set_point_size (v.point_size)
			end
			set_case_value (v.case_value)
			set_vertical_offset (v.vertical_offset)
			set_is_bold (v.is_bold)
			set_is_italic (v.is_italic)
			set_is_underlined (v.is_underlined)
			set_is_strike_through (v.is_strike_through)
			if attached v.text_color as rgb and then not rgb.is_empty then
				set_text_color (rgb)
			end
			if attached v.bg_color as bg and then not bg.is_empty then
				set_bg_color (bg)
			end
			set_left_margin (v.left_margin)
			set_right_margin (v.right_margin)
			set_top_margin (v.top_margin)
			set_bottom_margin (v.bottom_margin)
			if is_valid_alignment_value (v.alignment_value.out) then
				set_alignment_value (v.alignment_value)
			end
			if attached v.pre_text as pt and then not pt.is_empty then
				set_pre_text (pt)
			end
			if attached v.intext as itx and then not itx.is_empty then
				set_intext (itx)
			end
			set_number_separator (v.number_separator)
			set_number_format (v.number_format)
			if v.outline_numbering_enabled then
				enable_outline_numbering
			end
			if not v.reference_label.is_empty then
				set_ref_label (v.reference_label)
			end
			if not v.xref_target.is_empty then
				set_xref_target (v.xref_target)
				set_xref_type (v.xref_type)
			end
			is_initializing := False
		end

	blend (pf: USML_FORMAT; charf, parf: BOOLEAN)
			-- Blend values of Current with those of its 
			-- predecessor/parent 'pf', giving precedence to
			-- the values in Current, if defined, and deferring 
			-- to the values in 'pf' if not
			-- If 'charf' then blend character and effect values
			-- If 'parf; then blend paragraph values
		do
			if charf then
				blend_char_formats (pf)
			end
			if parf then
				blend_par_formats (pf)
			end
--TODO shouldn't we be looking at outline fmts, and outliers too?
		end

	--|--------------------------------------------------------------

	blend_from_tag (v: USML_TAG)
			-- Blend values of Current with those from tag 'v'
			-- where only values found in tag are applied to Current
		do
			if attached v.format as fmt then
				if attached fmt.font_family as ff and then not ff.is_empty then
					set_font_family (ff)
				end
				if v.has_point_size then
					if fmt.point_size_is_relative then
						set_relative_point_size (fmt.point_size)
					else
						set_point_size (fmt.point_size)
					end
				end
				if v.has_bold then
					set_is_bold (fmt.is_bold)
				end
				if v.has_italic then
					set_is_italic (fmt.is_italic)
				end
				if v.has_case_value then
					set_case_value (fmt.case_value)
				end
				if v.has_vertical_offset then
					set_vertical_offset (fmt.vertical_offset)
				end
				if v.has_underline then
					set_is_underlined (fmt.is_underlined)
				end
				if v.has_strike_through then
					set_is_strike_through (fmt.is_strike_through)
				end
				if attached v.text_rgb_color as rgb and then not rgb.is_empty then
					set_text_color (rgb)
				end
				if attached v.bg_color as bg and then not bg.is_empty then
					set_bg_color (bg)
				end
				if v.has_alignment then
					set_alignment_value (fmt.alignment_value)
				end
				if v.has_left_margin then
					set_left_margin (fmt.left_margin)
				end
				if v.has_right_margin then
					set_right_margin (fmt.right_margin)
				end
				if v.has_top_margin then
					set_top_margin (fmt.top_margin)
				end
				if v.has_bottom_margin then
					set_bottom_margin (fmt.bottom_margin)
				end
				if v.has_pre_text then
					if attached fmt.pre_text as pt then
						set_pre_text (pt)
					else
						-- TODO
						-- Do we need to nuke current's pre_text?
					end
				end
				if v.has_intext then
					if attached fmt.intext as it then
						set_intext (it)
					else
						-- TODO
						-- Do we need to nuke current's intext?
					end
				end
				if v.has_number_separator then
					set_number_separator (fmt.number_separator)
				end
				if v.has_number_format then
					set_number_format (fmt.number_format)
				end
				if v.has_number_start then
					set_number_start (fmt.number_start)
				end
				-- outline level is handled by section type
				if v.has_outline_level then
				end
				if v.has_outline_restart then
				end
				--if v.inlines_disabled then
				--end
			end
		end

	--|--------------------------------------------------------------

	blend_char_formats (pf: USML_FORMAT)
			-- Blend character format values of Current with those of 'pf'
			-- giving precedence to the values in Current, if defined,
			-- and deferring to the values in 'pf' if not
		do
			if not attached font_family as ff or else ff.is_empty then
				if attached pf.font_family as pff and then not pff.is_empty then
					set_font_family (pff)
				end
			end
			if not (point_size_is_relative or point_size_has_changed) then
				if pf.point_size_has_changed then
					set_point_size (pf.point_size)
				end
			end
			if not is_bold_has_changed then
				if pf.is_bold_has_changed then
					set_is_bold (pf.is_bold)
				end
			end
			if not is_italic_has_changed then
				if pf.is_italic_has_changed then
					set_is_italic (pf.is_italic)
				end
			end
			if not is_underlined_has_changed then
				if pf.is_underlined_has_changed then
					set_is_underlined (pf.is_underlined)
				end
			end
			if not is_strike_through_has_changed then
				if pf.is_strike_through_has_changed then
					set_is_strike_through (pf.is_strike_through)
				end
			end
			if not attached text_color as tc or else tc.is_empty then
				if attached pf.text_color as ptc then
					set_text_color (ptc)
				end
			end
			if not attached bg_color as bg or else bg.is_empty then
				if attached pf.bg_color as pbg then
					set_bg_color (pbg)
				end
			end
			if not vertical_offset_has_changed then
				if pf.vertical_offset_has_changed then
					set_vertical_offset (pf.vertical_offset)
				end
			end
		end

	blend_par_formats (pf: USML_FORMAT)
			-- Blend paragraph format values of Current with those of 'pf'
			-- giving precedence to the values in Current, if defined,
			-- and deferring to the values in 'pf' if not
		do
			if not alignment_has_changed then
				if pf.alignment_has_changed then
					set_alignment_value (pf.alignment_value)
				end
			end
			if not left_margin_has_changed then
				if pf.left_margin_has_changed then
					set_left_margin (pf.left_margin)
				end
			end
			if not right_margin_has_changed then
				if pf.right_margin_has_changed then
					set_right_margin (pf.right_margin)
				end
			end
			if not top_margin_has_changed then
				if pf.top_margin_has_changed then
					set_top_margin (pf.top_margin)
				end
			end
			if not bottom_margin_has_changed then
				if pf.bottom_margin_has_changed then
					set_bottom_margin (pf.bottom_margin)
				end
			end
			if not outline_numbering_has_changed then
				if pf.outline_numbering_enabled then
					enable_outline_numbering
				end
			end
		end

	--|--------------------------------------------------------------

	set_char_values (f, rc: STRING; p,cv,vs: INTEGER; b, i, u, st: BOOLEAN)
			-- (re)Initialize Current with font parameters
			-- 'f' is font family
			-- 'rc' is text color
			-- 'p' is point size
			-- 'cv' is case value (0=no change, <0=all lower, >0=all upper)
			-- 'vs' is vertical space (super/sub)
			-- 'b' is bold (strong) flag
			-- 'i' is italics flag
			-- 'u' is underline flag
			-- 'st' is strike-through flag
		require
			valid_font_family: is_valid_font_family (f)
			valid_color: rc.is_empty or is_valid_rgb_color (rc)
--			valid_point_size: p > 0
		do
			set_font_family (f)
			set_text_color (rc)
			set_point_size (p)
			set_case_value (cv)
			set_vertical_offset (vs)
			set_is_bold (b)
			set_is_italic (i)
			set_is_underlined (u)
			set_is_strike_through (st)
		end

	--|--------------------------------------------------------------

	set_par_values (av, lm, rm, ts, bs: INTEGER; lw: BOOLEAN; pt: STRING)
			-- (re)Initialize Current with paragraph parameters
			-- 'av' is alignment value
			-- 'lm' is left margin
			-- 'rm' is right margin
			-- 'ts' is top spacing (in pixels)
			-- 'bs' is bottom spacing (in pixels)
			-- 'lw' is line wrap/concatenation flag (disabled=True)
			-- 'pt' is pre-text (text before paragraph text)
		require
			valid_alignment: is_valid_alignment_value (av.out)
		do
			set_alignment_value (av)
			set_left_margin (lm)
			set_right_margin (rm)
			set_top_margin (ts)
			set_bottom_margin (ts)
			set_pre_text (pt)
		end

	--|--------------------------------------------------------------

	set_section_type (v: INTEGER)
		do
			section_type := v
		end

	--|--------------------------------------------------------------

	set_validation_error (v: STRING)
		do
			validation_error := v
		end

	--|--------------------------------------------------------------

	set_format_changed (ft: INTEGER; tf: BOOLEAN)
			-- Record that format type 'ft' has changed (tf=True) or 
			-- been reset to original (tf=False)
		require
			valid_type: is_valid_format_type (ft)
		do
			change_map.put (tf, ft)
		end

	--|--------------------------------------------------------------

	set_font_family (v: STRING)
		require
			valid: is_valid_font_family (v)
		local
			ffs, low_v: STRING
		do
			low_v := v.as_lower
-- Don't want to risk corruption from shared string
--			if attached font_family as ff then
--				ffs := ff
--			else
				create ffs.make (8)
				font_family := ffs
--			end
--			ffs.wipe_out
			ffs.append (low_v)
			if not is_initializing then
				set_format_changed (K_ft_font_family, True)
			end
		end

	set_point_size (v: INTEGER)
		require
			valid: v >= 0
		do
			point_size := v
			if not is_initializing then
				set_format_changed (K_ft_point_size, True)
			end
		end

	set_relative_point_size (v: INTEGER)
		do
			point_size_is_relative := True
			point_size := v
			if not is_initializing then
				set_format_changed (K_ft_point_size, True)
			end
		end

	set_is_bold (tf: BOOLEAN)
		do
			is_bold := tf
			if not is_initializing then
				set_format_changed (K_ft_is_bold, True)
			end
		end

	set_is_italic (tf: BOOLEAN)
		do
			is_italic := tf
			if not is_initializing then
				set_format_changed (K_ft_is_italic, True)
			end
		end

	set_is_underlined (tf: BOOLEAN)
		do
			is_underlined := tf
			if not is_initializing then
				set_format_changed (K_ft_is_underlined, True)
			end
		end

	set_is_strike_through (tf: BOOLEAN)
		do
			is_strike_through := tf
			if not is_initializing then
				set_format_changed (K_ft_is_strike_through, True)
			end
		end

	set_case_value (v: INTEGER)
		do
			case_value := v
			if not is_initializing then
				set_format_changed (K_ft_case_value, True)
			end
		end

	set_vertical_offset (v: INTEGER)
		do
			vertical_offset := v
			if not is_initializing then
				set_format_changed (K_ft_vertical_offset, True)
			end
		end

	set_text_color (v: STRING)
		require
			valid: v.is_empty or else is_valid_color_id (v)
		local
			cv: STRING
		do
			if v.is_empty then
				text_color := Void
			else
-- Don't want to risk corruption from shared string
--				if attached text_color as rc then
--					cv := rc
--				else
					create cv.make (8)
					text_color := cv
--				end
--				cv.wipe_out
				if is_valid_rgb_color (v) then
					cv.append (v.as_lower)
				elseif attached color_macro_by_name (v) as tm then
					if attached tm.text_rgb_color as c then
						cv.append (c)
					else
						-- Should not be possible
					end
				else
				end
			end
			if not is_initializing then
				set_format_changed (K_ft_text_color, True)
			end
		end

	--|--------------------------------------------------------------

	set_dflt_text_color (v: STRING)
		require
			valid: is_valid_rgb_color (v) or v.is_empty
		local
			cv: STRING
		do
			if v.is_empty then
				text_color := Void
			else
-- Don't want to risk corruption from shared string
--				if attached text_color as rc then
--					cv := rc
--				else
					create cv.make (8)
					text_color := cv
--				end
--				cv.wipe_out
				cv.append (v.as_lower)
			end
			if not is_initializing then
				set_format_changed (K_ft_text_color, True)
			end
		end

	--|--------------------------------------------------------------

	set_bg_color (v: STRING)
		require
			valid: is_valid_color_id (v) or v.is_empty
		local
			cv: STRING
		do
			if v.is_empty then
				bg_color := Void
			else
-- Don't want to risk corruption from shared string
--				if attached bg_color as rc then
--					cv := rc
--				else
					create cv.make (8)
					bg_color := cv
--				end
--				cv.wipe_out
				if is_valid_rgb_color (v) then
					cv.append (v.as_lower)
				elseif attached color_macro_by_name (v) as tm then
					if attached tm.text_rgb_color as c then
						cv.append (c)
					else
						-- Should not be possible
					end
				else
				end
			end
			if not is_initializing then
				set_format_changed (K_ft_bg_color, True)
			end
		end

	--|--------------------------------------------------------------

	set_dflt_bg_color (v: STRING)
		require
			valid: is_valid_rgb_color (v) or v.is_empty
		local
			cv: STRING
		do
			if v.is_empty then
				bg_color := Void
			else
-- Don't want to risk corruption from shared string
--				if attached bg_color as rc then
--					cv := rc
--				else
					create cv.make (8)
					bg_color := cv
--				end
--				cv.wipe_out
				cv.append (v.as_lower)
			end
			if not is_initializing then
				set_format_changed (K_ft_bg_color, True)
			end
		end

	--|--------------------------------------------------------------

	set_left_margin (v: INTEGER)
		do
			left_margin := v
			if not is_initializing then
				set_format_changed (K_ft_margin_left, True)
			end
		end

	set_right_margin (v: INTEGER)
		do
			right_margin := v
			if not is_initializing then
				set_format_changed (K_ft_margin_right, True)
			end
		end

	set_top_margin (v: INTEGER)
		do
			top_margin := v
			if not is_initializing then
				set_format_changed (K_ft_margin_top, True)
			end
		end

	set_bottom_margin (v: INTEGER)
		do
			bottom_margin := v
			if not is_initializing then
				set_format_changed (K_ft_margin_bottom, True)
			end
		end

	set_alignment_value (v: INTEGER)
		require
			valid: is_valid_alignment_value (v.out)
		do
			alignment_value := v
			if not is_initializing then
				set_format_changed (K_ft_alignment_value, True)
			end
		end

	set_pre_text (v: STRING)
		local
			txt: STRING
		do
			create txt.make (8)
			txt.append (v)
			pre_text := txt
			if not is_initializing then
				set_format_changed (K_ft_pre_text, True)
			end
		end

	set_intext (v: STRING)
		local
			txt: STRING
		do
-- Don't want to risk corruption from shared string
--			if attached intext as ts then
--				txt := ts
--			else
				create txt.make (8)
				intext := txt
--			end
--			txt.wipe_out
			txt.append (v)
			if not is_initializing then
				set_format_changed (K_ft_intext, True)
			end
		end

	--|--------------------------------------------------------------

	set_number_separator (v: STRING)
			-- Set text to separate outline number from item text
		do
			create number_separator.make (8)
			number_separator.append (v)
			if not is_initializing then
				set_format_changed (K_ft_num_sep, True)
			end
		end

	--|--------------------------------------------------------------

	set_number_format (v: INTEGER)
			-- Set format for outline numbers
		do
			number_format := v
			if not is_initializing then
				set_format_changed (K_ft_num_fmt, True)
			end
		end

	--|--------------------------------------------------------------

	set_number_start (v: INTEGER)
			-- Set ordinal number for 1st instance in outline set
		do
			number_start := v
			if not is_initializing then
				set_format_changed (K_ft_num_start, True)
			end
		end

	--|--------------------------------------------------------------

	enable_outline_numbering
		do
			outline_numbering_enabled := True
			if not is_initializing then
				set_format_changed (K_ft_ol_nums, True)
			end
		end

	--|--------------------------------------------------------------

	set_ref_label (v: STRING)
			-- Set label for reference by others (xrefs)
		do
			reference_label.wipe_out
			reference_label.append (v)
			if not is_initializing then
				set_format_changed (K_ft_ref_label, True)
			end
		end

	set_xref_type (v: INTEGER)
			-- Set xref type
		do
			xref_type := v
			if not is_initializing then
				set_format_changed (K_ft_xref_type, True)
			end
		end

	--|--------------------------------------------------------------

--	set_target_label (v: STRING)
	set_xref_target (v: STRING)
			-- Set xref target type+label to which to cross reference
		do
			xref_target.wipe_out
			xref_target.append (v)
			if not is_initializing then
				set_format_changed (K_ft_xref_target, True)
			end
		end

	--|--------------------------------------------------------------

	set_name (v: STRING)
		require
--TODO change to 'valid name'
			valid: is_valid_macro_label (v)
		do
			name.wipe_out
			name.append (v)
		end

	set_description (v: STRING)
		do
			description.wipe_out
			description.append (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_same (other: like Current): BOOLEAN
			-- Is 'other' the same as Current (effectively)
		do
			Result := is_equal (other)
		end

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	out: STRING
		do
			create Result.make (256)
			if attached font_family as ff then
				Result.append (ff)
			end
			Result.extend (',')
			Result.append (point_size.out)
			Result.extend (',')
			Result.append (case_value.out)
			Result.extend (',')
			Result.append (vertical_offset.out)
			Result.extend (',')
			Result.append (is_bold.out)
			Result.extend (',')
			Result.append (is_italic.out)
			Result.extend (',')
			Result.append (is_underlined.out)
			Result.extend (',')
			Result.append (is_strike_through.out)
			Result.extend (',')
			if attached text_color as tc then
				Result.append (tc)
			end
			Result.extend (',')
			if attached bg_color as bg then
				Result.append (bg)
			end
			Result.extend (',')
			Result.append (alignment_value.out)
			Result.extend (',')
			Result.append (left_margin.out)
			Result.extend (',')
			Result.append (right_margin.out)
			Result.extend (',')
			Result.append (top_margin.out)
			Result.extend (',')
			Result.append (bottom_margin.out)
			Result.extend (',')
			if attached pre_text as pt then
				Result.append (pt)
			end
		end

	--|--------------------------------------------------------------

	compact_out (st: INTEGER; df, cof, mxf: BOOLEAN; istr: STRING): STRING
			-- Values of Current, in compact form (using shorthand 
			-- notation wherever possible)
			-- If 'df', include only values that differ from default for 
			-- section type 'st', else include all values
			-- If 'cof' then format is restricted to character-only
			-- If 'mxf', break groups into lines for multi-line form
			-- Include 'istr' before each line in multiline form
		local
			ffs, pss, cvs, vos, bfs, ifs, ufs, sfs, tcs: STRING
			bgcs, nss: STRING
			avs, mls, mrs, mts, mbs: STRING
			nfs, sns, pts: STRING
			stp, plen: INTEGER
			dfmt: USML_FORMAT
		do
			create Result.make (256)
			ffs := ""; pss := ""; cvs := ""; vos := ""; bfs := ""
			ifs := ""; ufs := ""; sfs := ""; tcs := ""; bgcs := ""
			avs := ""; mls := ""; mrs := ""; mts := ""; mbs := ""
			nfs := ""; sns := ""; pts := ""; nss := ""
			stp := st
			if not is_valid_usml_tag_type (stp) then
				-- default to paragraph
				stp := K_usml_tag_p
			end

			dfmt := default_format_for_type (stp)

			--   <char_fmt> ::= char="<ff>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<rc>"
			if attached font_family as ff then
				if (not df) or font_family /~ dfmt.font_family then
					ffs := ff
				end
			end
			if (not df) or point_size /= dfmt.point_size then
				pss := point_size.out
			end
			if (not df) or case_value /= dfmt.case_value then
				cvs := case_value.out
			end
			if (not df) or vertical_offset /= dfmt.vertical_offset then
				vos := vertical_offset.out
			end
			if (not df) or is_bold /= dfmt.is_bold then
				bfs := bool_to_istr (is_bold)
			end
			if (not df) or is_italic /= dfmt.is_italic then
				ifs := bool_to_istr (is_italic)
			end
			if (not df) or is_underlined /= dfmt.is_underlined then
				ufs := bool_to_istr (is_underlined)
			end
			if (not df) or is_strike_through /= dfmt.is_strike_through then
				sfs := bool_to_istr (is_strike_through)
			end
			if attached text_color as tc then
				if  (not df) or not strings_same_ins (tc, dfmt.text_color) then
					tcs := tc.twin
				end
			end
			if attached bg_color as bg then
				if  (not df) or not strings_same_ins (bg, dfmt.bg_color) then
					bgcs := Ks_fmt_tag_bg_color + quoted (bg)
				end
			end

			plen := ffs.count + pss.count + cvs.count + vos.count
			plen := plen + bfs.count + ifs.count + ufs.count + sfs.count
			plen := plen + tcs.count + bgcs.count

			if (not df) or plen /= 0 then
				if mxf then
					Result.append (istr)
				end
				Result.append (Ks_fmt_tag_char)
				Result.append (
					aprintf (Ks_char_formats_pf_fmt,
					<< ffs, pss, cvs, vos, bfs, ifs, ufs, sfs, tcs >>))
				if not bgcs.is_empty then
					if not Result.is_empty then
						if mxf then
							Result.append (";%N" + istr)
						else
							Result.append (";")
						end
					end
					Result.append (bgcs)
				end
			end
			-- Now for the paragraph params
			-- <par_fmt> ::= par="<av>,<l>,<r>,<t>,<b>"

			if not cof then
				if (not df) or alignment_value /= dfmt.alignment_value then
					avs := alignment_value.out
				end
				if (not df) or left_margin /= dfmt.left_margin then
					mls := left_margin.out
				end
				if (not df) or right_margin /= dfmt.right_margin then
					mrs := right_margin.out
				end
				if (not df) or top_margin /= dfmt.top_margin then
					mts := top_margin.out
				end
				if (not df) or bottom_margin /= dfmt.bottom_margin then
					mbs := bottom_margin.out
				end
				plen := avs.count + mls.count + mrs.count + mts.count + mbs.count

				if (not df) or plen /= 0 then
					if Result.is_empty then
						if mxf then
							Result.append (istr)
						end
					else
						if mxf then
							Result.append (";%N" + istr)
						else
							Result.append (";")
						end
					end
					Result.append (Ks_fmt_tag_par)
					Result.append (
						aprintf (Ks_par_formats_pf_fmt,
						<< avs, mls, mrs, mts, mbs >>))
				end
			end

			-- Now for the outline params
			-- <ol_fmt> ::= out="<nfs>,<sns>,<pts>"
			if is_valid_outlined_type (st) then
				if (not df) or number_format /= dfmt.number_format then
					nfs := number_format.out
				end
				if (not df) or number_start /= dfmt.number_start then
					sns := number_start.out
				end
				if attached pre_text as pt then
					if (not df) or pre_text /~ dfmt.pre_text then
						pts := pt
					end
				end
				if (not df) or number_separator /~ dfmt.number_separator then
					nss := number_separator
				end
				plen := nfs.count + sns.count + pts.count + nss.count
				if (not df) or plen /= 0 then				
					if Result.is_empty then
						if mxf then
							Result.append (istr)
						end
					else
						if mxf then
							Result.append (";%N" + istr)
						else
							Result.append (";")
						end
					end
					Result.append (Ks_fmt_tag_ol)
					Result.append (
						aprintf (Ks_ol_formats_pf_fmt, << nfs, sns, pts >>))

					if not nss.is_empty then
						if not Result.is_empty then
							if mxf then
								Result.append (";%N" + istr)
							else
								Result.append (";")
							end
						end
						Result.append (Ks_fmt_tag_num_sep + quoted (nss))
					end
				end
			end

			-- Now for the outliers
--TODO check section type for relevance
			if attached intext as it then
				if (not df) or intext /~ dfmt.intext then
					if not Result.is_empty then
						if mxf then
							Result.append (";%N" + istr)
						else
							Result.append (";")
						end
					end
					Result.append (Ks_fmt_tag_intext + quoted (it))
				end
			end

			if (not df) or reference_label /~ dfmt.reference_label then
				if not Result.is_empty then
					if mxf then
						Result.append (";%N" + istr)
					else
						Result.append (";")
					end
				end
				Result.append (reference_as_named_param)
			end
			if (not df) or (xref_target /~ dfmt.xref_target) then
				if not Result.is_empty then
					if mxf then
						Result.append (";%N" + istr)
					else
						Result.append (";")
					end
				end
				Result.append (xref_target_as_named_param)
			end
		end

	--|--------------------------------------------------------------

	inline_out: STRING
			-- Values of Current, suitable for inclusion in
			-- an inline open tag
			-- Exclude values not suitable for inlines, even if defined
		local
			ffs, pss, cvs, vos, bfs, ifs, ufs, sfs, tcs: STRING
		do
			create Result.make (256)
			Result.append (Ks_fmt_tag_char)
			if attached font_family as ff then
				ffs := ff
			else
				ffs := ""
			end
			pss := point_size.out
			cvs := case_value.out
			if vertical_offset /= 0 then
				vos := vertical_offset.out
			else
				vos := ""
			end
			if is_bold then
				bfs := "1"
			else
				bfs := "0"
			end
			if is_italic then
				ifs := "1"
			else
				ifs := "0"
			end
			if is_underlined then
				ufs := "1"
			else
				ufs := "0"
			end
			if is_strike_through then
				sfs := "1"
			else
				sfs := "0"
			end
			if attached text_color as tc then
				tcs := tc
			else
				tcs := ""
			end

			Result.append (
				aprintf (Ks_char_formats_pf_fmt,
				<< ffs, pss, cvs, vos, bfs, ifs, ufs, sfs, tcs >>))

			if attached bg_color as bg then
				Result.append (";bgcolor=" + quoted (bg))
			end
		end

	--|--------------------------------------------------------------

	long_out: STRING
		do
			create Result.make (1024)
			Result.append (char_values_long_out)
			Result.append ("----------------------------------------%N")
			Result.append (par_values_long_out)
			Result.append ("----------------------------------------%N")
			Result.append (outline_values_long_out)
		end

	--|--------------------------------------------------------------

	char_values_long_out: STRING
		local
			ff, cvs, tcs, bgs: STRING
		do
			create Result.make (1024)
			if attached font_family as f then
				ff := f
			else
				ff := ""
			end
			if is_upper then
				cvs := "Upper"
			elseif is_lower then
				cvs := "Lower"
			elseif is_title_case then
				cvs := "Title"
			else
				cvs := "Normal"
			end
			if attached text_color as tc then
				tcs := tc
			else
				tcs := ""
			end
			if attached bg_color as bg then
				bgs := bg
			else
				bgs := ""
			end

			Result.append ("Font family:      " + ff + "%N")
			Result.append ("Point size:       " + point_size.out + "%N")
			Result.append ("Case value:       " + cvs + "%N")
			Result.append ("Offset:           " + vertical_offset.out + "%N")
			Result.append ("Bold:             " + is_bold.out + "%N")
			Result.append ("Italic:           " + is_italic.out + "%N")
			Result.append ("Underlined:       " + is_underlined.out + "%N")
			Result.append ("Strike-thru:      " + is_strike_through.out + "%N")
			Result.append ("Text color:       " + tcs + "%N")
			Result.append ("BG color:         " + bgs + "%N")
		end

	--|--------------------------------------------------------------

	par_values_long_out: STRING
		local
			avs, its: STRING
		do
			create Result.make (1024)
			inspect alignment_value
			when K_align_left then
				avs := "Left"
			when K_align_right then
				avs := "Right"
			when K_align_center then
				avs := "Center"
			when K_align_justified then
				avs := "Justified"
			else
				avs := "Left"
			end
			if attached intext as it then
				its := it
			else
				its := ""
			end

			Result.append ("Alignment:        " + avs + "%N")
			Result.append ("Left margin:      " + left_margin.out + "%N")
			Result.append ("Right margin:     " + right_margin.out + "%N")
			Result.append ("Top margin:       " + top_margin.out + "%N")
			Result.append ("Bottom margin:    " + bottom_margin.out + "%N")
			Result.append ("In-tag text:      [" + its + "]%N")
		end

	--|--------------------------------------------------------------

	outline_values_long_out: STRING
		local
			pts, nfs: STRING
		do
			create Result.make (1024)
			nfs := num_format_label (number_format)
			if attached pre_text as pt then
				pts := pt
			else
				pts := ""
			end

			Result.append ("Number format:    " + nfs + "%N")
			Result.append ("Pre text:         [" + pts + "]%N")
			Result.append ("Starting no.:     " + number_start.out + "%N")
			Result.append ("Number separator: [" + number_separator + "]%N")
		end

--|========================================================================
feature -- Parameter conversion
--|========================================================================

	xref_target_as_named_param: STRING
		do
			Result := Ks_fmt_tag_xref + quoted (xref_target)
		end

	reference_as_named_param: STRING
		do
			Result := Ks_fmt_tag_ref + quoted (reference_label)
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_dflt_font_family: STRING
		once
			Result := Ks_ff_sans.twin
		end

	K_dflt_point_size: INTEGER = 12

	Ks_dflt_color_rgb: STRING = "000000"

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_FORMAT
