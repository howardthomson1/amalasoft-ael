note
	description: "{
A major (aka 'heading') section, within a structured USML document
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_HEADING

inherit
	USML_OUTLINED
		redefine
			is_heading, is_indexable, type_label
		end

create
	make_leveled, make_as_text

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================

	is_heading: BOOLEAN = True
			-- Is Current a heading section?

	is_indexable: BOOLEAN
			-- Is Current indexable?
			-- i.e. is it suitable for inclusing in a TOC
			-- True unless within a subdoc
		do
			Result := not in_subdoc
		end

	type_label: STRING
		do
			Result := "Heading"
		end

--|========================================================================
feature -- Status setting
--|========================================================================

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_HEADING
