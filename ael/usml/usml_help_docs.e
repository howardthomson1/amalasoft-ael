note
	description: "{
A collection of document components or sections
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_HELP_DOCS

inherit
	ANY
		redefine
			default_create
		end

create
	make

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make
		do
			default_create
			init_documents
		end

	default_create
		do
			create doc_list.make
			create doc_table.make (7)
 			create headings.make
			Precursor
		end

	init_documents
			-- Fill 'documents' with documents
		do
			add_document (Ks_docname_intro, Ks_usml_doc_intro)
			add_document (Ks_docname_model, Ks_usml_doc_model)
			add_document (Ks_docname_language, Ks_usml_doc_language)
			add_document (Ks_docname_beyond, Ks_usml_doc_beyond)
			add_document (Ks_docname_app, Ks_usml_doc_app)
			add_document (Ks_docname_app_a, Ks_usml_doc_appendix_a)
			add_document (Ks_docname_app_b, Ks_usml_doc_appendix_b)
			add_document (Ks_docname_app_c, Ks_usml_doc_appendix_c)
			add_document (Ks_docname_app_d, Ks_usml_doc_appendix_d)
			add_document (Ks_docname_app_e, Ks_usml_doc_appendix_e)
			add_document (Ks_docname_app_f, Ks_usml_doc_appendix_f)
		end

	add_document (ts, ds: STRING)
			-- Create document with title 'ts' from content 'ds' and add 
			-- to doc_list and doc_table
		local
			doc: USML_DOCUMENT
		do
			create doc.make_from_stream (ts, ds)
			doc_list.extend (doc)
			doc_table.extend (doc, ts)
			headings.extend (ts)
		end

--|========================================================================
feature -- Access
--|========================================================================

	doc_by_name (v: STRING): detachable USML_DOCUMENT
		do
			Result := doc_table.item (v)
		end

--|========================================================================
feature -- Documents
--|========================================================================

	doc_list: TWO_WAY_LIST [USML_DOCUMENT]
	doc_table: HASH_TABLE [USML_DOCUMENT, STRING]
	headings: TWO_WAY_LIST [STRING]

	--|--------------------------------------------------------------

	intro_doc: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_intro)
			if not attached Result then
				create Result.make_empty
			end
		end

	model_doc: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_model)
			if not attached Result then
				create Result.make_empty
			end
		end

	language_doc: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_language)
			if not attached Result then
				create Result.make_empty
			end
		end

 	beyond_basics_doc: USML_DOCUMENT
 		do
 			Result := doc_table.item (Ks_docname_beyond)
 			if not attached Result then
 				create Result.make_empty
 			end
 		end

	app_doc: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_a: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_a)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_b: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_b)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_c: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_c)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_d: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_d)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_e: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_e)
			if not attached Result then
				create Result.make_empty
			end
		end

	appendix_f: USML_DOCUMENT
		do
			Result := doc_table.item (Ks_docname_app_f)
			if not attached Result then
				create Result.make_empty
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_docname_intro: STRING = "1 Introduction"
	Ks_docname_model: STRING = "2 The Model"
	Ks_docname_language: STRING = "3 USML Language Basics"
	Ks_docname_beyond: STRING = "4 USML Beyond the Basics"
	Ks_docname_app: STRING = "5 The Application"
	Ks_docname_app_a: STRING = "Appendix A Single-Line Tags"
	Ks_docname_app_b: STRING = "Appendix B Multi-Line Tags"
	Ks_docname_app_c: STRING = "Appendix C Format Parameters"
	Ks_docname_app_d: STRING = "Appendix D Default Formats"
	Ks_docname_app_e: STRING = "Appendix E A Collection of Macros"
	Ks_docname_app_f: STRING = "Appendix F A Basic Template"

	--|--------------------------------------------------------------

	Ks_usml_doc_intro: STRING
		once
			create Result.make (5600)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<H1>
Introduction
<P>
USML, the acronym, stands for Ultra-Simple Markup Language.  Aura is the Amalasoft USML Reader Application.  Aura reads, interprets, and displays USML-formatted text.  Aura also supports document creation and editing.  While Aura functions as a standalone application, it serves also (and quite intentionally) as an example of how to use the Amalasoft USML classes in applications.
Perhaps the obvious question then is "Why?".  The motivation for creating Aura, and especially for creating the USML markup language and supporting classes, was to make it much easier to include formatted text in applications.
While there are alternatives, each has its benefits and limitations, and in the end, none was sufficiently appealing for the intended purpose.  Something that seems simple enough, like an outline-formatted help document or even a numbered list, turn out to be far more challenging than they should be.
An RTF file can be generated from MS Word and its work-alikes, certainly, but the files so generated are large (and they are files, so they don't lend themselves well to embedding in application code, and require path management, adding unnecessary complexity to the applications).
HTML documents can be created, and these can be displayed in any web browser (a separate application).  Some application frameworks include a web browser widget class, but these require a facility like webkit to be installed on the host platform.  This limitation makes that option less appealing.
Common desktop application frameworks include a rich text widget, capable of displaying basic RTF content.  Such widgets that can be filled, programmatically, piece-by-piece, with decent formatting control.  Again though, this option loses its shine for anything but relatively small-scale documents, and the document can become lost in the code needed to format it.
It seems more reasonable to create documents using plain text and a few simple formatting tags, leaving the heavy lifting of working with the RTF widgets and frameworks to the software that understands the formatting tags, and the widget.  Created in this way, documents can still be files (and viewed as such) if so desired, but they also can be embedded into applications very easily, as they are just strings, and not excessively large (the size dependent on content, not on massive header and boilerplate data).
A secondary motivation (realized only after becoming immersed in the project), was to show that decent text formatting is possible without the often overwhelming complexity of current HTML/CSS and its associates.  Sometimes, the power and expressiveness of those tools are unnecessary and get in the way of the goal - to create well-formatted text.  That goal is at the core of USML.  .  USML is very simple, but expressive and extensible (including the ability to define your own custom tags and formats).

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_model: STRING
		once
			create Result.make (56000)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<H1 start=2>The Model
<P>
The conceptual model behind the language and application has, at its core, the notion of a structured document.  A structured document is a collection of document sections (and is itself a kind of document section).  (Click <usml inline color="0000ff";xref="exti,$AURA_HOME/images/datamodel.png">here</usml> to show image)
In addition to the document itself, there are 3 major subtypes of document section:
<b1>Heading
<b1>List Item
<b1>Paragraph Text
<p>
There are also 2 unformatted section types.
<b1>Comment
<b1>Verbatim
<note>
The unformatted sections are, as might be assumed, unformatted.  Comment sections do not appear in the rendered document.  Verbatim sections, though unformatted, do appear in the rendered text.
<p>
Paragraph text sections contain paragraph text; possibly multiple paragraphs per section.
List items and headings are <usml I>outlined</usml>, and have outline levels.  Outlined sections, like the document itself, are complex sections, meaning that they are sections and are containers of sections.
<taglit left=10>
--------------------------------------------------------
Document                 is-a           Complex Section
Complex Section          is-a           Document Section
Complex Section   is-a-collection-of    Document Section
Text                     is-a           Document Section
Outlined                 is-a           Complex Section
Heading                  is-a           Outlined
List                     is-a           Outlined
---------------------------------------------------------
Inline Tag               is-a           Tag
Comment                  is-a           Tag
Inline Format            is-a           Format
---------------------------------------------------------
Document Section         has-a          Format
Tag                      has-a          Format
Inline Tag               has-a          Inline Format
---------------------------------------------------------
<p>
Apart from comments and the document heading tag, there are only 4 structure types (and only 3 of those being formatted), and their associated tag types.  This might seem to be just a little too simple, lacking any sort of expressiveness.  That would be the case if it were not for a few other important features of the language.
These features include, as might be apparent from the model above, the ability to associate formats with tags.  Note, in the model, that there are subtypes of tags.  Format tags enable definition of a format for a structure type (headings, list items, and text sections) for the whole document.  Macro tags enable definition of sets of format parameters.  Each of these tag types is discussed in detail in later sections of this document.
<csep>
<p>
Every (formatted) document section has an associated format and, if not user-supplied, is defined by default values in the application (e.g. Aura).
A document is created from an input stream, that stream being USML-compliant text.  The stream can begin as a text file, as a string constant within an application, or as a text string acquired in some other manner (e.g. via a pipe or network connection).  The point is, it's just a string of text.
The stream is scanned for tags.  From the tags are extracted formatting parameters and type information (special tag type, document section type, or a combination thereof).  The text that is not part of a tag is the document text and is formatted according to the associated tags.
Each section can have its specific characteristics.  Each section's format definition is a blend of the default definition (for that type and level) and any instance-specific format parameters from the tag.
Rendering is performed, separately, after assembly is complete, section-by-section.  (Click <usml inline color="0000ff";xref="exti,$AURA_HOME/images/process.png">here</usml> to show image)
The rendering engine walks through the document sections, appending the sections' text and applying the sections' formats.

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_language: STRING
		once
			create Result.make (24000)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<H1 start=3>USML Language Basics
<p>
The USML language consists of tags and tag parameters.  Tags identify or define the type of element and its format.  There are very few built-in tag types.  The major tags, corresponding to the major document section types, are:
<b1>Heading (qualified by level)
<b1>List Item (qualified by level)
<b1>Paragraph (text section)
<b1>Unformatted Sections (Verbatim and Comment)
<p>
The language also includes specialty tags (format, macro and inline; more on those later) and comment tags.
Tags can be single-line, multi-line, or inline.  Text associated with (formatted according to) a tag is the text that follows the tag, up to but not including the next tag.
<b1>Single-line tags occupy exactly one line of text.
<b1>Multi-line tags are tags spread across multiple lines.
<b1>Inline tags can appear within a line of non-tag text.
<note>
Verbatim section tags are single-line tags but, unlike other section tags, are paired.  There is a verbatim section opening tag (<usml v>) and a verbatim section closing tag (</usml v>).
<p>
Each kind of tag has a specific syntax, though much of the syntax is common to all.
With the exception of inline tags, USML tags <U>must begin at the start of a line</usml>.  This seemingly overzealous constraint is intended to simplify the language and its use.
The most basic USML document would have no tags at all, just plain text.  An example of a slightly more interesting document will help to illustrate the language basics.
<taglit>
 <usml ht>
 A Very Simple Document
 <usml p>
 Not much more to say.
 No, really, we're done here.
<p>
The first line of the document includes a single-line tag, defining the document title heading.  The tag begins with the standard USML tag prefix.
<taglit>
 <usml ht>
 ^----^ Tag Prefix
<p>
Following the tag prefix is the tag label.  In this case, the tag label specifies the tag type (heading) and, being a outlined type, the outline level (in this case, the level is document title).
<taglit>
 <usml ht>
       ^ Tag type (h=heading)
 <usml ht>
        ^ Outline level (t=title)
<p>
Other outline levels are numeric, from 1 through 5.
The second line of the document does not have a tag.  Because the previous line included the heading tag, the text on the second line is taken as the text of the section defined by the preceding tag (the document title heading).
<note>
All lines of text between tagged lines belong to the preceding tag.  As such, multiple lines of text can be associated with one tag.
<p>
The third line of the document is also a single-line tag, but this time, the tag label has only the tag type marker 'p'.
<taglit>
 <usml p>
       ^ Tag type (p=paragraph/text section)
<p>
The 'p' marker identifies a section containing one or more paragraphs (a.k.a. a text section).  The text on the following lines (lines 4 and 5) is included that section.  Paragraphs are not outlined types, and so do not have levels.
When rendered using default formats, the result looks something like this.
<sep>
<h+>
<htx>A Very Simple Document
<p>
Not much more to say.
No, really, we're done here.
<h->
<sep>
<p>
The fourth major section type (beyond document, heading and text) is list item (or just 'list' for short).
The type marker for list items is 'L' (or 'l', the marker is case-insensitive but an upper-case L is easier to distinguish from a numeral '1').
As with heading tags, the list type marker is followed by a qualifier, indicating the outline level of the section it represents.  Unlike heading tags, list tags do not permit 't' (title) as a level qualifier.  The qualifier must be a decimal digit from 1 through 5.
<taglit>
 <usml ht>
 A Still Simple Document
 <usml p>
 Here comes a list section (numbered by default)
 <usml L1>
 This is a list item, outline level 1, instance 1
 <usml L2>
 This is a list item, outline level 2, instance 1
 <usml L1>
 This is a list item, outline level 1, instance 2
<p>
Applying the default formats that include outline numbering, the rendered text would look like this:
<sep>
<h+>
<htx>A Still Simple Document
<p>
Here comes a list section (numbered by default)
<L1>This is a list item, outline level 1, instance 1
<L2>This is a list item, outline level 2, instance 1
<L1>This is a list item, outline level 1, instance 2
<h->
<sep>
<p>
Outline numbering is managed by the document interpreter.  The input need only identify the list items and their respective outline levels.
To illustrate the use of heading tags, beyond the document title case, merge the preceding examples into a slightly more complex document.
<taglit>
 <usml ht>
 A Slightly More Complex Document
 <usml h1>
 From the First Example
 <usml p>
 Not much more to say.
 No, really, we're done here.
 <usml h1>
 From the Second Example
 <usml p>
 Here comes a list section (numbered by default)
 <usml L1>
 This is a list item, outline level 1, instance 1
 <usml L2>
 This is a list item, outline level 2, instance 1
 <usml L1>
 This is a list item, outline level 1, instance 2
<p>
Once again, applying the default formats, the rendered result looks like this.
<sep>
<h+>
<htx>A Still Simple Document
<H1>From the First Example
<p>
Not much more to say.
No, really, we're done here.
<h1>From the Second Example
<p>
Here comes a list section (numbered by default)
<L1>This is a list item, outline level 1, instance 1
<L2>This is a list item, outline level 2, instance 1
<L1>This is a list item, outline level 1, instance 2
<h->
<sep>
<p>
To this point, we have seen 3 major tag types (heading, text, and list), reflecting the 3 major (formatted) document section types.  One might be satisfied with this level of control, and indeed, many documents might require nothing more.
While there are only the 3 such sections, and their corresponding tag types (H, L, and P), USML includes a few other tag types that should come in handy.  The simplest of these is the aforementioned comment tag.
The comment tag allows comments to be present in a document, but have the comment text omitted from the rendered text (line numbers have been added here for illustration purposes only).
<taglit>
 001  <usml ht>
 002  A Nearly Content-Free Document
 003  <usml c>Here is a comment line
 004  Here is more comment text
 005  and here is even more
 006  <usml p>
 007  Did you catch the comments before or after this point?
 008  <usml c>Just checking
 009  <usml p>
 010  Let's hope not. 
 011  <usml x c>
 012  Here is another comment line
 013  Here is yet more comment text
 014  <usml p>
 015  and here is a line with a p tag
 016  It should be ignored because it's in a multi-line comment
 017  </usml x>
 018  Did you catch the other comments?
 019  <usml c>Just checking
 020  <usml p>
 021  Again, let's hope not
<p>
The example introduces, not just the comment tag, but a new construct - the multi-line tag.  In this case, it is a multi-line comment tag.  A multi-line comment tag is more than just a comment with multiple lines of text.  It defines the bounds of the text to be associated with the comment.  That text can include what would otherwise be other USML tags.
<note>
All multi-line tags are paired (with an open and close tag each).
<p>
The rendered document would include none of the commented text.
<sep>
<h+>
<htx>A Nearly Content-Free Document
<p>
Did you catch the comments before or after this point?
Let's hope not. 
Did you catch the other comments?
Again, let's hope not
<h->
<sep>
<p>
Notice anything unexpected on the second line of rendered text?  Recall from the input text that there was a single-line comment tag on line 003, and it was followed, on the same line, by some comment text, and on subsequent lines by more comment text.  The comment was terminated when another tag (<usml p>) was found on line 006.  That's how ordinary comments work.  The same behavior can be observed on line 008.  The text on the same line as the comment tag is permitted, but not required.
Things get a bit more interesting at line 011.  That line contains "<usml x c>".  Any tag that begins with "<usml x " is a multi-line tag.  The tag context continues until a line beginning with the multi-line closing tag ("</usml x>") is found.  That occurs at line 017 in the example.  If a line beginning with a different tag (or any other text) is found in the interim, it is considered part of the comment tag context.
<note>
This is not the same behavior as seen in HTML, where the content between paired tags is considered document text.  The content between a multi-line open tag and its corresponding closing tag belongs to the tag itself.
In USML, verbatim sections, delimited by verbatim open and close tags, though paired like multi-line tags, are each single-line tags.  As such, the text between them does not, strictly speaking, belong to the tag(s), but does belong to the section defined by the tags.
<p>
What is significant about this example is not just that it is multi-line, but that the line following the comment closing tag (line 018) is plain text.  Because there was no intervening single-line structure tag (or multi-line opening tag) outside of the multi-line comment tag context, the text <I>section</usml> that began with the tag on line 009 is continued after the multi-line comment.  This is, quite simply, because comments are not <I>formatted</usml> document sections.  The document is rendered as if the comments were never there in the first place.
<H2 start=1>Trailing Text
<p>
In the previous example, comment tags appeared as both single-line and multi-line.  What might not have been obvious is that the single-line comments, in the example, included "trailing text"; text that follows the actual tag on the same line.
<taglit>
 008  <usml c>Just checking
 009  <usml p>
<p>
Line 008 had a single-line comment, with trailing text, followed by a line that had a single-line tag (a paragraph tag).  In this case, the text associated with the comment was limited to the trailing text.  In contrast, the single-line comment at line 003 also had trailing text, but the lines that followed did not begin with a new tag, and so the text of those lines was also associated with the preceding comment tag.
<taglit>
 003  <usml c>Here is a comment line
 004  Here is more comment text
 005  and here is even more
<p>
Single-line structure tags and comment tags can have trailing text and/or associated text from subsequent lines.  The association is terminated when a new single-line or multi-line (open) tag is found.
<nota_bene>
Multi-line tags (opening or closing) cannot have trailing text.
<H2>Verbatim Text
<p>
Verbatim tags define document sections that are taken, verbatim, into the rendered document, without any tag interpretation.  Whatever format had been in place where the verbatim open tag was found remains in place for the verbatim section, and until another formatted section tag appears.
<exh>Example
<taglit>
001  <usml ht>
002  A Document with Some Verbatim Text
003  <usml p>
004  This is an ordinary paragraph
005  <usml v>
006  This is text within a verbatim section
007  <usml ht>
008  This would have been a document title heading
009  Except that it was in a verbatim section
010  </usml v>
<sep>
<h+>
<htx>A Document with Some Verbatim Text
<p>
This is an ordinary paragraph
<usml v>
This is text within a verbatim section
<usml ht>
This would have been a document title heading
Except that it was in a verbatim section
</usml v>
<h->
<sep>
<p>
The rendered document would include the formatted title heading and paragraph text, but would also include the text in the verbatim section, though without interpretation.  That means, in this example, that the document title heading tag would not be interpreted as a tag (i.e. taken verbatim).
Verbatim sections can be especially handy if you are writing a document about USML.
Verbatim sections are formatted, not by their own parameters, but by the context in whey they are found.  In the previous example, the context was that of a simple paragraph, and so the verbatim text was rather unremarkable.  If the verbatim section were to follow immediately a heading, then the verbatim text would be rendered in the same format as that heading.  While this should lead one to assume that care should be taken in this regard, it also provides an opportunity to format otherwise unformattable verbatim text - by establishing, before the verbatim section, an enclosing context with the desired format.  Formatting is discussed in detail in later sections of this document.
<h2>Review
<p>
Now might be a good time for a quick review.
<b1>
There are 3 formatted structure types (in addition to the document itself)
<b2>Heading
<b2>List Item
<b2>Paragraph Text
<b1>There are 3 major tag configurations
<b2>Single-line
<b3>Single-line tags occupy a single line
<b3>Single-line tags begin at the start of a line
<b3>Single-line tags consume the text that follows them, either on the same line or on subsequent lines, or both
<b3>Consumption of document text by a single-line tag continues until another single line tag, or a multi-line opening tag is found
<b3>Single-line tags (in standard format) begin with "<usml " and are followed by a tag type
<b2>Multi-line
<b3>Multi-line tags begin with a multi-line open tag
<b3>Multi-line tags continue, as tags, until a multi-line closing tag is found at the start of a line
<b3>All text between a multi-line opening tag and its corresponding closing tag belongs to that tag
<b3>Multi-line tags cannot have trailing text (i.e text, outside of the open tag on the same line)
<b2>Inline
<b3>Inline tags occur within text that is not itself a single-line or multi-line USML tag (more on this in the next section)
<p>
Verbatim tags, though paired, are single-line tags and, as such, must begin at the start of a line.

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_beyond: STRING
		once
			create Result.make (38000)
			Result.append (Ks_usml_doc_common_macros)
			Result.append (Ks_usml_doc_beyond_part1)
			Result.append (Ks_usml_doc_beyond_part2)
		end

	Ks_usml_doc_beyond_part1: STRING
		once
			create Result.make (20000)
			Result.append ("{
<H1 start=4>USML - Beyond the Basics
<h2>Inline Tags
<p>
Unlike structure tags, inline tags are <I>format-only</usml> tags, and do not define document sections.  They are un-typed.
Format options for inline tags are limited to character format parameters (and macros that define them).  Inline tags do not accept paragraph or outline parameters.  Inline tags do, however, accept reference and cross-reference parameters (see Hyperlinks section).
Inline tags can occur anywhere within a line of text that is not itself a USML tag.
Inline tags have corresponding closing tags ("</usml>").
<exh>Example
<taglit>
This is text with an <usml inline italic=1;color="ff0000">inline</usml> tag.
<p>
The rendered result would apply the format parameters from the inline tag to the range of text between the inline opening tag and the closing tag (the "captive text") only.
<sep>
<p>
This is text with an <usml inline italic=1;color="ff0000">inline</usml> tag.
<sep>
<h3>Nesting Inline Tags
<p>
USML supports nested inline tags.  While this capability might not be for the timid, it can come in handy for certain situations.  Imagine that you wanted to set out a phrase or clause within an otherwise normally formatted paragraph.  For that you would wrap that text in an inline tag pair, with a suitable format, italics for example.
<exh>Example
<taglit>
This is ordinary, except for <usml inline italic=1>these few words</usml> only.
<p>
The rendered result would apply the format parameters from the inline tag to the range of text between the inline opening tag and the closing tag (the "captive text") only.
<sep>
<p>
This is ordinary, except for <inline italic=1>these few words</usml> only.
<sep>
<p>
That's fine as far as it goes, but what if you wanted additional emphasis for a subrange of the text already tagged?
<sep>
<p>
This is ordinary, except for <inline italic=1>these <inline underlined=1>few</usml> words</usml> only.
<sep>
<p>
To accomplish that, you could break the text into 5 regions: plain, italic, underlined+italic, italic, then plain.  Or, you could use the nested inline tag facility in USML.
<taglit>
This is ordinary, except for <usml inline italic=1>these <usml inline underlined=1>few</usml> words</usml> only.
<p>
The rendering engine would format the outer tag's captive text with italics.  Because the outer tag's captive text includes the inner tag's captive text (they're nested), the inner tag's captive text is formatted along with the outer tag's captive text (italic) and then is formatted according the definition supplied by the inner tag (underlined).
Because italics and underlining are complementary, the inner captive text in the example is italicized <inline italic=1>and</usml> underlined.  If the inner format conflicts with the outer format (e.g. a different text color), then the inner format overrides the outer format (for the inner captive text only).
<note>
The Aura application limits inline nesting depth to 7.
<h3>Suppressing Inline Tag Interpretation
<p>
If, for some reason (e.g. you are writing a document about USML), you need to suppress interpretation of inline tags for a document section, add the "<code>no_inlines</usml>" parameter to the section tag.  The no_inlines parameter does not take a value.
If included, the no_inlines parameter will suppress interpretation for that document section only.  Normal behavior would resume with the next section tag.
<exh>Example
<taglit>
 <usml p no_inlines;font=sans>
<p>
Like all parameters, the no_inlines parameter is separated from any others by a semicolon.
<h2>Terse Tag Forms
<p>
While the standard format for the built-in tags begins with the "<usml " prefix, the language also supports terse forms.
The longer form is helpful when starting out, and to distinguish USML texts from texts in other markup languages like HTML.  Once the language and its capabilities are understood, shifting to using the terse forms might be preferred.  Standard-form and terse-form tags can coexist in the same document.
The terse forms simply omit the "usml " from the standard-form tag.  This is true also for multi-line opening and closing tags, and for inline tags.
<taglit>
 <usml p>        becomes   <p>
 <usml p ..      becomes   <p ..
 <usml c>        becomes   <c>
 <usml h1>       becomes   <h1>
 <usml h1 ..     becomes   <h1 ..
 <usml L1>       becomes   <L1>
 <usml L1 ..     becomes   <L1 ..
 <usml x ..      becomes   <x ..
 </usml x>       becomes   </x>
 <usml v>        becomes   <v>
 </usml v>       becomes   </v>
 <usml inline .. becomes   <inline ..
<p>
Terse tags, like their standard-form counterparts, are case-insensitive.
<nota_bene>
Note well that single-line tags (terse or otherwise) must still begin at the start of a line.
<exh>Example
<p>
Revisiting an earlier example, using terse tags this time, we have:
<taglit>
 <ht>
 A Terse Document
 <p>
 Here comes a list section (numbered by default)
 <L1>
 This is a list item, outline level 1, instance 1
 <L2>
 This is a list item, outline level 2, instance 1
 <L1>
 This is a list item, outline level 1, instance 2
<p>
Applying the default formats that include outline numbering, the rendered text would look like this:
<sep>
<h+>
<htx>A Terse Document
<p>
Here comes a list section (numbered by default)
<L1>This is a list item, outline level 1, instance 1
<L2>This is a list item, outline level 2, instance 1
<L1>This is a list item, outline level 1, instance 2
<h->
<sep>
<p>
There are two variants of the terse form of inline tags.  The first omits the "usml" prefix, where the second omits the "inline" qualifier.
<taglit>
 <usml inline .. becomes   <inline ..
OR
 <usml inline .. becomes   <usml ..
<exh>Example A
<taglit>
This is text with a <usml italic=1>terse</usml> inline tag.
<sep>
<p>
This is text with a <usml italic=1>terse</usml> inline tag.
<sep>
<exh>Example B
<taglit>
This is text with a <inline italic=1>terse</usml> inline tag.
<sep>
<p>
This is text with a <inline italic=1>terse</usml> inline tag.
<sep>
<p>
If you have previously defined a (character-only) formatting macro, then the name of the macro can be used in place of its parameters in the inline tag.  See the Macro Tags section.
<H2>A Touch of Style
<p>
At this point, you might be asking yourself "Is That All There Is?"
The answer is, in a word, no.  But the important thing to remember is that the few tags and constructs described so far will satisfy most basic formatting needs.  The primary goal is simplicity, after all.
But, you're asking, what if I want to change the size of the title, or headings, or the paragraph text?  That is accomplished easily, using format parameters.
Format parameters are divided into groups:  Character Formats, Paragraph Formats, and Outline Formats.  There are also a few stragglers, outliers, that do not belong to any major group.
<taglit>
             Character   Paragraph   Outline
    Heading      Y           Y          Y
  List Item      Y           Y          Y
  Paragraph      Y           Y          N
     Inline      Y           N          N
<h3>Character Formats
<p>
The tags introduced so far, less the comment tags, accept a rich set of format parameters.  Let's use point size of paragraph text to illustrate.
To set the point size of text in a document section, you can add the point_size parameter to the tag.  For example, to set the point size of text in a paragraph section to 14, the tag would look like this:
<taglit>
 <usml p point_size=14>
<p>
If you wanted the text also to be bold, italicized, underlined, blue, and with a with fixed-pitch font face, you would add those parameters.
<taglit>
 <usml p point_size=14;font="fixed";italic=1;bold=1;underlined=1;color="0000ff">
<note>
Note that the named parameters are separated by semicolons (';'), with no other intervening spaces or separators.
<p>
Alternately, you could use the character format <I>shorthand</usml> form:
<taglit>
 <usml p char="fixed,14,,,1,1,1,,0000ff">
<p>
The character format shorthand is a quoted, comma-separated list of parameters.
<taglit>
 <char_fmt> ::= char="<ff>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<tc>"
      <ff> ::= <font family>     (font=)
      <ps> ::= <point size>      (point_size=)
      <cv> ::= <case_value>      (case=)
      <vs> ::= <offset value>    (offset=)
      <b>  ::= <bool value>      (bold=)
      <i>  ::= <bool value>      (italic=)
      <u>  ::= <bool value>      (underlined=)
      <st> ::= <bool value>      (strike=)
      <tc> ::= <color value>     (color=)
 Where:
  <font family>    ::= "sans" | "fixed" | "times" | ""
  <point size>     ::= <positive integer> | ""
  <case value>     ::= <all_upper> | <all lower> | <title_case> | ""
    <all upper>    ::= 1 | "U"
    <all lower>    ::= -1 | "L"
    <title_case>   ::= 10 | "T"
  <offset value>   ::= <integer> | ""  (vertical offset, in pixels)
  <bool value>     ::= <true value> | <false value> | ""
    <true value>   ::= 1 | "true"
    <false value>  ::= 0 | "false"
  <color_value>    ::= <rgb hex> | ""
    <rgb hex>      ::= 6-char hex RGB color
<p>
In the shorthand form, each parameter position must exist, but can be empty (i.e. there must 8 commas, even if there is nothing between them).
<minor>
Not Included in the Shorthand Form
<p>
Defined separately, using the "bgcolor=" parameter, is the text background color.  This is typically left to the defaults (i.e. none), but can be handy for highlighting special text.  Like the text color parameter, the background color parameter accepts a 6-character hexadecimal RGB value.
<minor>
Multi-Line Form
<p>
To accommodate multiple parameters (including but not limited to those in the shorthand notation), but without resorting to the compact but somewhat cryptic shorthand form, tags can use the multi-line configuration.  The previous example would then look like this:
<taglit>
 <usml x p>
   point_size=14;
   font="fixed";
   italic=1;
   bold=1;
   underlined=1;
   color="0000ff"
 </usml x>
<p>
Parameters in a multi-line tag can be a mix of shorthand and named formats.  In case of conflicting values, the later entries supersede the earlier ones.  Take the following example.
<taglit>
 <usml x p>
   char="fixed,14,,,1,1,,0000ff">
   point_size=12;
   color="00ff00"
 </usml x>
<p>
The character format shorthand is the same as in the previous example, but following that entry, there are named parameters for point size and color.  Because the named parameters appear after the shorthand format, they supersede the corresponding values in the shorthand format.
The result is the same as before except that the point size would now be 12 and the color would be green.  A little better planning might omit the point size and color from the shorthand form altogether.  The effect is the same.
<taglit>
 <usml x p>
   char="fixed,1,,,1,1,,">
   point_size=12;
   color="00ff00"
 </usml x>
<p>
Multi-line forms do not require all parameters to be present, but any shorthand parameters in a multi-line tag must still comply with the all-fields-accounted-for rule.
<H3>Paragraph Formats
<p>
In addition to the character-oriented format parameters just described, USML supports paragraph-oriented parameters.  The shorthand form is:
<taglit>
 par="<av>,<l>,<r>,<t>,<b>"
    <av> ::= <alignment>
    <l>  ::= <left margin>
    <r>  ::= <right margin>
    <t>  ::= <top spacing>
    <b>  ::= <bottom spacing>
  <alignment>   ::= <align_l> | <align_r> | <align_c> | <align_j> | ""
    <align_l>  ::= "L" | "l" | "left" | "0"
    <align_r>  ::= "R" | "r" | "right" | "-1"
    <align_c>  ::= "C" | "c" | "center" | "2"
    <align_j>  ::= "J" | "j" | "justified" | "1"
  <left_margin>       ::= <positive integer> | ""
  <right_margin>      ::= <positive integer> | ""
  <top_spacing>       ::= <positive integer> | ""
  <bottom_spacing>    ::= <positive integer> | ""
<p>
The alignment parameter determines the alignment of associated paragraphs in the rendered text.
Left and right margins do as one should expect, though values are expressed in pixels.
Top and bottom spacing sets the number of pixels above and below associated paragraphs.
<minor>
Multi-Line Form
<p>
As with character formats, there is the option to use the multi-line form.  In this example, the multi-line form includes the shorthand character form for the character parameters, and specifies separately the left margin parameter.
<taglit>
 <usml x p>
   char="fixed,14,,,1,1,,0000ff"l
   left=10;
   bottom=6
 </usml x>
<p>
Multi-line forms do not require all parameters to be present.
<minor>
Not Included in Shorthand Form
<p>
Paragraph formats can include also the "<code>intext=</usml>" parameter.  If specified, the text value is taken as belonging to the associated tag, as if it were added between the tag and any subsequent tag.
<exh>Example
<taglit>
 <usml p intext="some arbitrary text">
<p>
Why, one might ask, would someone choose the intext parameter, rather than simply adding the text as usual?  While it makes little or no sense in the given example, it comes in especially handy when defining macros (see the Macro Tags section).
<nota_bene>
Note well that, if the intext parameter is specified, the tag will not accept/associate any additional text (from subsequent lines or trailing).
<h3>Outline Formats
<p>
Lists and headings have outline levels, and corresponding outline level markers (a.k.a. outline numbers).  The format for the outline level markers is defined using a shorthand outline format parameter, or separate outline format parameters.  The shorthand form has the following syntax.
<taglit>
 out="<lt>,<si>,<pt>"
   <lt>  ::= <label type>
   <si>  ::= <start instance>
   <pt>  ::= <pre text>

  <label_type>      ::=  "#" | "A" | "a" | "I" | "i" | "T" | "t" | ""
  <start_instance>  ::= <integer> | ""   (ordinal starting instance)
  <pre_text>        ::= <string>  | ""   (text to use as, or precede label)
<p>
The label_type parameter defines the type of outline marker to be used for the associated outlined sections (headings and lists).
<taglit>
 '#' uses decimal numbers
 'A' uses upper-case letters
 'a' uses lower-case letters
 'I' uses upper-case Roman numerals
 'i' uses lower-case Roman numerals
 'T' or 't' uses provided pre_text (if any, else none)
<p>
The start_instance parameter is a positive decimal integer and indicates that this instance of this type should have an ordinal instance number of that provided (versus the value one greater than the previous instance of that type and level).
The value of the instance number relates to the label type.  For decimal types, the instance number is simply the number.  For other types, the instance number is the value in that type's character set.  For example, if the label type is 'A' and the instance number is 3, then the instance value would be 'C' (the 3rd item in the upper-case letter sequence).
Setting the instance number can be handy when it is necessary to have non-list items (paragraphs) between list items, as this would otherwise reset the next instance number to 1.  It is also handy for numbering major document sections (e.g. appendices) differently.
The instance number is not relevant if the label type is text.  The text label type enables definition of bullet lists or annotated lists.
The parameters encoded in the shorthand form are, as separate parameters:
<taglit>
 "num_fmt="
 "start="
 "pre_text="
<minor>
Not Included in the Shorthand Form
<p>
Defined separately, using the "num_sep=" format, is the number separator.  The number separator is a string inserted between the outline label and the item's text.  The default separator is a single space.
Outline formats can, like other formats, use the multi-line form.
<H3>Formatting the Unformattable
<p>
Verbatim sections do not have explicit formats.  They acquire their format from the rendered context.  This does not mean that verbatim text needs always to be lost in that context.  If, for example, you wanted to set off a verbatim section from surrounding default paragraph text, you could insert a customized paragraph tag just before the verbatim opening tag and an unqualified paragraph tag after the verbatim closing tag.
<exh>Example
<taglit>
 <usml p>
 This is ordinary text, using default paragraph format
 <usml p char="fixed,,,,1,,,,000080">
 <usml v>
 This is verbatim text, repeating the previous source lines
 <usml p>
 This is ordinary text, using default paragraph format
 <usml p char="fixed,,,,1,,,,000080">
 </usml v>
 <p>
 Back to default format
<p>
When rendered, the verbatim text assumes the formatting of the context, this having been defined by the immediately preceding paragraph tag.
<sep>
<p>
This is ordinary text, using default paragraph format
<usml p char="fixed,,,,1,,,,000080">
<usml v>
This is verbatim text, repeating the previous source lines
<usml p>
This is ordinary text, using default paragraph format
<usml p char="fixed,,,,1,,,,000080">
</usml v>
<p>
Back to default format
<sep>
<p>
Note well that the tags within the verbatim section are transferred, without interpretation, to the rendered text.
<h2>Format Tags
<p>
Format tags make it possible to set the default format for document section types and have the format applied to all such sections in the document (unless explicitly overridden on a per-instance basis).
<nota_bene>
Note well that setting the default format for a component type does so for the remainder of document, from the point of definition forward.
<p>
Format tags begin with the "<usml " tag prefix.  The standard prefix is followed by the 'f' marker, indicating that the tag is a format definition tag.
<taglit>
 <usml f<stype> ..
       ^
<p>
The 'f' marker is followed immediately by the (formatted) section type to which the format definition will apply.  Section types are paragraph ('P'), heading ('H'), and list ('L'), with heading and list types also requiring outline levels.
<exh>
Examples
<taglit>
 <usml fp     Sets default formats for paragraph text sections
 <usml fh1    Sets default formats for level 1 headings
 <usml fL3    Sets default formats for level 3 list items
<p>
The remainder of the format tag resembles other tags, with the format parameters following the section type marker(s).
<exh>
Example
<taglit>
 <usml fp char="sans,12,,,,,,,">
<ipar_1>
Changes the default font family and point size for paragraph text, but leaves all other parameters in their former state.
<p>
Format tags also support terse forms (dropping the <code>"usml= "</usml> prefix).  The multi-line constructs (terse and standard) can also be used with format definition tags.
<exh>
Example (standard form, multi-part)
<taglit>
 <usml x fp>
  char="sans,12,,,,,,,";
  par=",,,0,0"
 </usml x>
<exh>
Example (terse form, multi-part)
<taglit>
 <x fp>
  char="sans,12,,,,,,,";
  par=",,,0,0"
 </usml x>
<p>
Format definition tags can specify all relevant format parameters (character and paragraph for all types; outline for list and heading types).  Any parameters not defined in the format definition tag defer to the default values for that type.
<H3>Wildcard Formats
<p>
Headings and list items have types that correspond, directly or with bias, to their respective outline levels.  USML supports, for these section types, the ability to set some format values for all levels with a single "wildcard" format tag.
<nota_bene>
Wildcard formats apply only to outlined types.
Heading wildcard formats do not apply to the 't' level (title).
<p>
For example, if you wished to set the point size for all list items to the same, non-default value, you would use a format tag whose type marker denotes list item ('L'), but instead of including the outline level as a decimal digit, specify that all levels are to be formatted this way by using an asterisk ('*') in place of the outline level number.
<exh>Example
<taglit>
 <usml fL* point_size=12>
<p>
Wildcards should be used with caution, of course.  As with all format tags, the format(s) specified apply from the point of definition to the end of the document.  Also, because defaults are defined using the wildcard method, any subsequent per-instance format parameters will supersede the corresponding default parameters.

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_beyond_part2: STRING
		once
			create Result.make (20000)
			Result.append ("{
<H2>Macro Tags
<p>
Many document authoring systems provide a means by which to define styles, and to apply those styles to document components.  USML enables users to define formats globally (for the whole document) as just described (using format tags), or to create format <I>macros</usml> that can be applied as desired within a document.
There are two major variants of macro tags:  typed and untyped.
<h3>Untyped Macros
<p>
Untyped macros (and the tags that define them) define a format, with a given name, that format being applicable to different types of document components.
An example of an untyped format macro affords an opportunity to illustrate inline tags as well, though to be clear, macros are not restricted to use in inline tags.
Imagine that the challenge is to show some text, in the middle of an otherwise ordinary paragraph, as being superscript, perhaps an exponent.  While it is certainly possible to define such a construct without a macro, having a macro for such an occasion comes in handy - especially if you plan to use it a lot.
<exh>
Example
<taglit>
 <usml p>
 This doesn't have N<usml inline char=",-3,,4,,,,,ff00ff">2</usml> complexity.
                    ^                                    ^ ^-----^
                    |------------ inline tag ------------| | closing tag
                                             captive text| |
<p>
In the example an inline tag surrounds (and therefore formats) the exponent "2".  The shorthand character format calls for a point size that is 3 points smaller than the default paragraph font, and a vertical offset of 4 pixels upward.  The superscripted text will be magenta, for good measure.  Note well that we did not define the type face (left it blank in the shorthand format), so it will be the same as the rest of the text in that section.
The rendered text would look like this.
<sep>
<p>
This doesn't have N<inline super>2</usml> complexity.
<sep>
<p>
The superscript format could be applied to an entire paragraph if so desired, but that would be very strange looking.  It is more likely that, once a superscript format is defined, it would be used in a few different places.  It makes sense then to define a macro to capture that format.  The macro could look like this.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
<p>
Having the macro in hand, the previous example text would look like this.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
 <usml p>
 This doesn't have N<usml inline macro="super">2</usml> complexity.
<p>
Admittedly, that's not a massive reduction, but there's more.  Because we can substitute the macro name for the {macro="super"} construct, we can further reduce the noise level.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
 <usml p>
 This doesn't have N<usml inline super>2</usml> complexity.
<p>
The macro name stands in for the macro definition (pretty much in the spirit of macros).  In the case of inline tags, using the macro enables you to omit the "inline" keyword from the tag, further simplifying the construct.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
 <usml p>
 This doesn't have N<usml super>2</usml> complexity.
<p>
That still might not be enough to convince you.  Maybe the next example will help.
Imagine that you want to show a formula of some kind.  You might choose a fixed-pitch typeface, making the paragraph format different than the one in the previous example.  Turning to Pythagoras, we'd very much like to see the formula resemble this.
	A<usml super>2</usml> + B<usml super>2</usml> = C<usml super>2</usml>
To accomplish this using the "super" macro just defined, the formula paragraph would look like this.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
 <usml p>
 This doesn't have N<usml super>2</usml> complexity.
 <usml p font="fixed">
 A<usml super>2</usml> + B<usml super>2</usml> = C<usml super>2</usml>
<p>
Because we have identified the macro by name, in the tag, the macro is applied to the exponents as we had hoped.
<sep>
<p>
This doesn't have N<usml super>2</usml> complexity.
<codep>
A<usml super>2</usml> + B<usml super>2</usml> = C<usml super>2</usml>
<sep>
<p>
Note that the first paragraph has a different typeface than the second paragraph, but that the same macro can be applied to each.  The macro defines character format only.
Macro definition tags can also take the form of multi-line tags.  The previous macro definition, in multi-line form, would look like this.
<taglit>
 <usml x m>
  name="super";
  char=",-3,,4,,,,,";
  color="ff00ff"
 </usml x>
<p>
Pulling out all stops, the terse multi-line form would be:
<taglit>
 <x m>
  name="super";
  char=",-3,,4,,,,,";
  color="ff00ff"
 </usml x>
<p>
Note well that multi-line macro tags must have an intervening space between the "<x" prefix and the macro type tag.  The reason for this is to reduce the chances that a multi-line tag would conflict with a macro name, or that macro names would be unnecessarily restrictive.  This reasoning will become more apparent in later sections.
<h3>Typed Macros
<p>
Typed macros, unlike their untyped counterparts, apply to specific section types (paragraphs, lists and headings).  The associated type is included in the macro definition tag, as a single character immediately following the 'm' tag type marker.  The section type markers are the same as they appear in other contexts.
<exh>
Example
<taglit>
 <usml mp name="ipar_1";left=10>
<p>
This simple macro defines a paragraph having a left margin of 10 pixels.
Typed macro invocations, because they are associated with section types, cannot be inline.  They can be whole-line or multi-line only.
<exh>
Example
<taglit>
 <usml mp name="ipar_1";left=10>
 <usml ht>
 Typed Macro Example
 <usml macro="ipar_1">
 This text is indented by 10 pixels.
 <usml p>
 This text is not.
<sep>
<h+>
<htx>Typed Macro Example
<usml macro="ipar_1">
This text is indented by 10 pixels.
<usml p>
This text is not.
<h->
<sep>
<p>
As described previously, a named macro need not have the "macro=" prefix.
<taglit>
 <usml ipar_1>
 This text is indented by 10 pixels.
<p>
Typed macros also qualify for use in terse-form tags.  See the "Custom Tags Using Macros" section for details and examples.
<minor>
Color Macros
<p>
USML's macro facility supports one rather specialized "type" of macro - color.  Colors for text and background are parameters to character formats, and are specified using a 6 hexadecimal digit RGB string (e.g. "ff0000" for red).  USML lets you define color-only macros, using the same syntax as other macro definitions, but specifying a single parameter.  This, by itself is not all that special.
definitions, but specifying a single parameter.  This, by itself is not all that special.
What is special is that these color macros can be used in place of color parameters in other tags (and in other macros).
For example, if you wanted to use "red" instead of "ff0000" each time you wished to specify red as a color, you would define a 'red' macro.
<taglit>
 <usml m name="red";color="ff0000">
<p>
To use this macro an inline tag, for example, you could have something like this, adapted from an example in the Inline Tags section:
<taglit>
This is text with an <usml inline italic=1;color="red">inline</usml> tag.
<p>
The rendered result would look the same as if you had specified the text color as an RGB value.
<sep>
<p>
This is text with an <inline color="ff0000";italic=1>inline</usml> tag.
<sep>
<p>
Once defined, as seen with the codep/code example, the macro can be used within other macros.
<exh>
Example
<taglit>
 <usml m name="purple";color="8000f0">
 <usml mP name="pbip";char=",,,,1,1,,,purple">
 <pbip>
 Some bold, purple, italicized text
<sep>
<p char=",,,,1,1,,,purple">
Some bold, purple, italicized text
<sep>
<p>
The name of the color macro need not be a standard color name.
<h3>Custom Tags Using Macros
<p>
As was illustrated in the previous section, the USML macro facility offers considerable flexibility.  Using the macro facility, it is also possible to create custom tags.
For example, USML includes a built-in list item type, but does not define any subtypes for lists or list items.  The default list outline parameters include a hierarchical decimal numbering scheme.
What if you needed bulleted lists too?  Certainly (as previously described), you could define per-instance formats, or set the list type default format for the whole document, but what if you needed the default numbered list <usml B>and</usml> bullet lists?  You'd have to toggle the formats back and forth, each time you wanted a different list subtype.  The macro facility delivers what you need.
In this example, we define 3 macros: "b1", "b2", and "b3".  These are defined as applying to List levels 1, 2, and 3, respectively, using typed format macros.  The macros define, using shorthand notation, list item formats with different left margins for each level.  Each also defines top and bottom margins, and a pre text string (the bullet).
<taglit>
 <usml mL1 name="b1";par=",5,,0,0";out=",,- ">
 <usml mL2 name="b2";par=",15,,0,0";out=",,o ">
 <usml mL3 name="b3";par=",25,,0,0";out=",,* ">
<p>
The example above uses the shorthand notations, but could have used named parameters, or a mix of shorthand and named parameters, in whole-line or multi-line formats.
<taglit>
 <usml x mL1>
  name="b1";
  left=15;
  top=0;
  bottom=0;
  out=",,- "
 </usml x>
<p>
If the intent is to distinguish bulleted items even more, changing the character format to bold, italic, or both might be the answer.  To do that, simply add the appropriate character format definitions.
<taglit>
 <usml mL1 name="b1";char=",,,,1,,,,";par=",5,,0,0";out=",,- ">
 <usml mL2 name="b2";char=",,,,1,,,,";par=",15,,0,0";out=",,o ">
 <usml mL3 name="b3";char=",,,,1,1,,,";par=",25,,0,0";out=",,* ">
<p>
In the example, level 1 and 2 bullet items have bold fonts, and level 3 items have bold and italic fonts.
As was illustrated earlier, to use a macro in the document, all that is necessary is to insert a tag identifying the macro by name.  Because the macro is typed, it stands in for the tag representing that type.
<exh>
Example
<taglit>
 <usml b1>
 This is a level 1 bullet list item
 <usml b2>
 This is a level 2 bullet list item
 <usml b3>
 This is a level 3 bullet list item
 <usml b2>
 This is another level 2 bullet list item
<p>
The rendered result looks like this.
<sep>
<b1>This is a level 1 bullet list item
<b2>This is a level 2 bullet list item
<b3>This is a level 3 bullet list item
<b2>This is another level 2 bullet list item
<sep>
<p>
Having a custom subtype defined in this way has no effect on the formatting of existing types.  This lets you mix default and custom types in the same document with ease.
But, there's more.  The macro facility, as just seen, enables you to define typed macros (and untyped ones).  Taking the bullet list macros just defined as examples, you have the option to use the macro name in place for the standard USML opening prefix, effectively creating your own markup language (or at least what appears to be one).
<exh>
Example
<taglit>
 <b1>
 This is a level 1 bullet list item
 <b2>
 This is a level 2 bullet list item
 <b3>
 This is a level 3 bullet list item
 <b2>
 This is another level 2 bullet list item
<p>
The rendered result looks exactly the same as when the longer forms are used.
<sep>
<b1>This is a level 1 bullet list item
<b2>This is a level 2 bullet list item
<b3>This is a level 3 bullet list item
<b2>This is another level 2 bullet list item
<sep>
<p>
It is very important to choose macro names well, especially if they are to be used in this manner.
<h2>Including External Text
<p>
Now that you have a way to define custom tags and subtypes, it would be very handy to be able to collect those somewhere and reuse them without having to type them into each document.  For this, USML provides the include tag.
<exh>
Example
<taglit>
 <usml include "./files/my_macros.usdf">
<note>
The ".usdf" suffix is for illustration purposes.  The file suffix could just as well be ".txt", or the file might have no suffix at all.  Using an easily differentiated file suffix, like ".usdf", can make it easier to identify such files among files of other types.
<note>
The <path> parameter can include environment variables.
<taglit>
 <usml include "$MY_MACROS">
<p>
The content of "my_macros.usdf" is a collection of macros and format definitions, but no actual text.  The include mechanism does not impose any such restriction, and it might be handy in some cases to have text snippets managed separately, to be included in multiple documents.  Copyright notices, legal disclaimers come to mind.
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
 <usml m name="code";char="fixed,,,,1,,,,000080">
 <usml mp name="codep";macro="code";par=",,,0,0">
 <usml mL1 name="b1";par=",10,,0,";out="t,,- ">
 <usml mL2 name="b2";par=",20,,0,";out="t,,o ">
 <usml mL3 name="b3";par=",30,,0,";out="t,,* ">
 <usml mL4 name="b4";par=",40,,0,";out="t,,- ">
<p>
Note well that the macro named "code" is untyped, where the macro named "codep" is typed, AND uses the "code" macro to define a portion of the format (the character parameters).
The document that includes these definitions would look something like this.
<taglit>
 <usml include "my_macros.usdf">
 <usml p>
 This is an ordinary paragraph section.
 It has two lines of text.
 <usml p macro="codep">
 This is a code-style paragraph.
 It also has two lines of text.
 <usml b1>
 This is a level one bullet list item
 <usml b1>
 This is another level 1 bullet list item
 <usml b2>
 This is a level 2 bullet list item
 <usml b3>
 This is a level 3 bullet list item
 <usml b4>
 This is a level 4 bullet list item
 <usml b2>
 This is another level 2 bullet list item
 <usml p>
 This is not a bullet list item, but this is a <usml code>code</usml> word
<p>
The rendered text then would look like this.
<sep>
<usml p>
This is an ordinary paragraph section.
It has two lines of text.
<usml p macro="codep">
This is a code-style paragraph.
It also has two lines of text.
<usml b1>
This is a level one bullet list item
<usml b1>
This is another level 1 bullet list item
<usml b2>
This is a level 2 bullet list item
<usml b3>
This is a level 3 bullet list item
<usml b4>
This is a level 4 bullet list item
<usml b2>
This is another level 2 bullet list item
<usml p>
This is not a bullet list item, but this is a <usml code>code</usml> word
<sep>
<h2>Hyperlinks
<p>
USML includes support for in-document and external hyperlinks.  Hyperlinks rely on two kinds of format parameters, reference labels, and cross-reference labels.
Reference labels resemble other single-value parameters and have the form:
<taglit>
 ref="<label>"
<p>
Cross-reference labels are similar, but as they identify existing reference labels, they have the form:
<taglit>
 xref="<label>"
<p>
Absent an explicit xref_type, the type of cross-reference is "anchor" (a labeled location within the document).  Xref types include:
<taglit>
 Marker  Type             Label Interpretation
 --------------------------------------------------
  #      Anchor           target reference label
  exti   External image   file path
  file   External file    file path
  act    Internal action  internal procedure call,
                          with optional argument(s)
  url    URL              URL
  embi   Embedded image   name of embedded image
  pbuf   Pixel buffer     name of pixel buffer class
<exh>
Examples
<taglit>
 ..xref="position_1"..
 ..xref="#position_1"..
 ..xref="#,position_1"..
 ..xref="exti,some_folder/some_file.png"..
 ..xref="file,some_folder/some_file.txt"..
 ..xref="act,on_select(fxbn)"..
 ..xref="url,www.eiffel.org"..
 ..xref="embi,datamodel"..
 ..xref="pbuf,process_pbuf"..
<p>
A tag can have a reference label, a cross-reference label, both, or neither.
<nota_bene>
Reference and cross-reference label parameters are associated with inline tags only, but inline tags can be used with all section tags.
<p>
In the Aura application, when a user changes the cursor (caret) position by means of a mouse click, that position is compared with known cross-reference label positions.  If the cross_reference is found, its target label is extracted and then compared with the collected reference labels.
If the target label matches a known reference label (an anchor), then the position of the reference label is determined and the application scrolls to the line at which the target begins, and selects (highlights) the defined character sequence.
The reference label need not be the same as the character sequence it represents (and typically is not).
Note well that the tags that have label parameters are nothing special.  The ref and xref parameters are just that, parameters in otherwise ordinary (inline) tags.  
<exh>Example
<taglit>
 <usml p>
 This is paragraph <usml inline ref="par_1">one</usml>
 This is paragraph <usml inline ref="par_2">two</usml>
 This is a paragraph with no references or cross references
 This has links to <usml inline char=",,,,,,1,,";xref="par_1">paragraph 1</usml> and <usml inline char=",,,,,,1,,";xref="par_2">paragraph 2</usml>
 This is a paragraph with no references or cross references
<p>
The fact that locations in the text are labeled in this way is not reflected in the rendered text, apart from any formatting parameters you might have added.  The text in the example above would look like this.
<sep>
<p bottom=2;top=2>
This is paragraph <usml inline ref="par_1_ex">one</usml>
This is paragraph <usml inline ref="par_2_ex">two</usml>
This is a paragraph with no references or cross references
This has links to <usml inline underlined=1;xref="par_1_ex">paragraph 1</usml> and <usml inline underlined=1;xref="par_2_ex">paragraph 2</usml>
This is a paragraph with no references or cross references
<sep>
<p>
Notice the underlining that was added to the cross-reference instances, and that there is no apparent change to the rendering of the targets.  When a user selects or clicks on a character or range of characters for which a cross-reference is defined ("paragraph 1" and "paragraph 2" in line 4 of the example), the text widget is scrolled to show the defined target.  As the example is only 5 lines long, it doesn't offer much in the way of demonstration.  Adding a few pages of text between these points will make for a respectable demo.
Note also that the character format in the example xref cases have the underlined flag set to True.  This is for illustration and is not a requirement, certainly, but can help.  If links will be used often, then investing in a macro could be worthwhile.  The xref construct might look something like this.
<taglit>
 <usml m name="link";char=",,,,,,1,,00c000">
<h3>External Images
<p>
As listed previously, USML supports typed cross-reference variants, in addition to the default internal anchors.  Typed cross-references include:  "exti" (external image), "file" (external non-image file), "url" (a network URL), "embi" (embedded image), and "pbuf" (compiled pixel buffer).  Each of these requires interpretation by the rendering application, of course.
The external image link includes the "exti" xref type label, and an image file pathname.
<taglit>
 ..xref="exti,<path>"..
<exh>Example
<taglit>
 ..<usml inline xref="exti,C:\Projects\p1\model.png">diagram</usml>..
<p>
The <path> parameter can include environment variables.
<taglit>
 ..<usml inline xref="exti,$PROJ_1\model.png">diagram</usml>..
 ..<usml inline xref="exti,$PROJ_1\$MODEL_PIC">diagram</usml>..
<nota_bene>
Only whole segments (i.e. path segments beginning with a '$' character) are substituted
<p>
If the rendering application is using the EV_RICH_TEXT widget, it would not be able to embed images directly into the rendered text, but (as is done in Aura), the application could, for example, respond to the mouse click on the link by opening a dialog in which the image could be displayed.
<h3>Files
<p>
The external file link includes the "file" xref type label, and a (non-image) file pathname.
<taglit>
 ..xref="file,<path>"..
<exh>Example
<taglit>
 ..<usml inline xref="file,C:\Project\example.e">example.e</usml>..
<p>
Try this one: <usml inline color="0000f0";xref="file,$AURA_HOME/examples/example.e">example.e</usml> as a demonstration
<p>
The <path> parameter can include environment variables.
<nota_bene>
Only whole segments (i.e. path segments beginning with a '$' character) are substituted
<taglit>
 ..<usml inline xref="file,$PROJ_1\example.e">example.e</usml>..
<p>
The rendering application could, for example, respond to the mouse click on the link by opening a text dialog in which the file's contents could be displayed, or perhaps open the file with the user's preferred editor.  An application might go so far as to open different external programs, based on the given file's suffix.
<h3>Actions
<p>
The action link includes the "act" xref type label, and a procedure name, optionally with an argument list.
<taglit>
..<inline xref="act,on_select(some_value)">..
..<inline xref="act,do_something">..
<p>
If there are no arguments, then only the procedure name is included; no parentheses are used.
If there are arguments, they are in the form of a comma-separated list of strings, enclosed in a pair of parentheses.
If the argument values are strings (vs numbers), they are unquoted within the argument list (the xref parameter value is already quoted).  Their interpretation as having types other than string is deferred to the procedure being called.
<h3>URLs
<p>
The external URL link includes the "url" xref type label, and a browser-suitable URL.
<taglit>
 ..xref="url,<URL>"..
<exh>Example
<taglit>
 ..<usml inline xref="url,www.eiffel.org/welcome">Eiffel</usml>..
<p>
The <URL> parameter can include environment variables.
<nota_bene>
Because URLs can contain escaped '$' characters, extra care should be taken when using this option.  As with environment variables in paths, described above, only whole segments (i.e. segments beginning with a '$' character) are substituted.
<p>
The rendering application could, for example, respond to the mouse click on the link by opening a browser with the given URL, or perhaps using an embedded browser widget to do the same.
<h3>Embedded Images
<p>
The embedded image link is similar to the others.  Th xref type label is "embi", followed by the name of the embedded image.
<exh>Example
<taglit>
..<usml inline xref="embi,datamodel">diagram</usml>..
<p>
Embedded images can be PNG binaries, bitmaps, or other binary image types.
Again, it is the responsibility of the rendering application to interpret these elements.  To embed an image in a USML document, insert the image code (as a stream of bytes), between an image opening tag and a multiline closing tag.  The image opening tag must include a "name=" parameter.
<exh>Example
<taglit>
 <image name="datamodel">
 ..Image Binary Data..
 ..Image Binary Data..
 ..
 ..Image Binary Data..
 ..Image Binary Data..
 </usml x>
<p>
Embedding images might not be for the timid, but has the advantage of avoiding all the messiness of external files.
Note well that embedded images are captured during the preprocessor phase and so do not have to appear inline with any references to them in the rest of the document text.  For example, embedded images can appear at the end of document, not compromising the readability of the main text of the document.
<h3>Pixel Buffers
<p>
The compiled pixel buffer link has an xref type label of "pbuf" and the name the name given to a instance of EV_PIXEL_BUFFER within the rendering application.
<exh>Example
<taglit>
 ..<usml inline xref="pbuf,process_pbuf">diagram</usml>
<p>
While this might have little value in a general-purpose USML reader, it can be a handy facility for purpose-built applications that use USML to drive the application content.

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_app: STRING
		once
			create Result.make (3200)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<h1 start=5>The Aura Application
<p>
Aura is a native desktop application and has been compiled for 64-Bit Intel systems running Windows 10 and CentOS 7 Linux.
The application comes as a stand-alone executable, along with a few example files, as well as image files to which links are supplied in this text.
<h2>The User Interface
<h3>The Main Window
<p>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_main_labeled.png">[Main Window Screenshot]</usml>
The main tool bar presents the following components.
<taglit>
 Open
     Open a new document from file
 Reload
     Re-load the current document from file.
     The Reload capability is present to enable
     quick edit/render turnaround.
 Clear
     Clear source and rendered text fields
<p>
Below the menu bar and the main tool bar, the main window presents a split area, with two major components.
<h4>Source Area
<p>
The left-side component is the source area and displays, in an unformatted text widget, the tagged (or to-be-tagged) source text, along with associated controls in small panels above and below the text widget.
Above the text widget in the source area is a toolbar presenting a save-as-text item, and a text search control (a search text field and buttons for searching case-sensitive and case-insensitive).
<taglit>
 Save-As Text
     Save document as plain text
 Search Input Field
     String for which to search in source test
 Search
     Search source for next instance
     of text string from input field;
     Case-sensitive
 Search Insensitive
     Search source for next instance
     of text string from input field;
     Case-INsensitive
<p>
Below the text widget in the source area is a toolbar presenting several items.
<taglit>
 Enable Source Editing
     Enables editing within source text widget
 Show Line Numbers
     Generates line numbers for each line
     of source text
 Insert Text at Position
     Insert text, from external file, at start,
     current position or end of document
 Open Tag Maker
     Open Tag Maker dialog to create and
     insert a USML tag into source
 Open Macro Wizard
     Open Macro Wizard dialog to create, 
     step-by-step, a typed or untyped macro
 Add Tag
     Open Add Tag Menu to add a tag to
     source text from options in menu
<p>
The source text field has editing disabled initially.  Checking the Enable Edits button enables the text in that field to be edited.
The Tag Maker and Macro Wizard toolbar items invoke the associated dialogs.
<minor>
Line Numbering in Source
<p>
The Show Line Numbers check button, if selected, will cause generation of line numbers within the source text field.  These line numbers are not part of the document.  They exist solely within the text field, to aid in navigation, or relate positions in the text field to those in the original document text (in an external editor, for example).
<nota_bene>
While line numbers are present, edit and search operations (in the source text) are disabled.  Line numbering can be toggled on and off, as desired, and when off, no such restrictions apply.
<minor>
Inserting Text and Tags
<p>
The Insert Text tool bar items let you insert text (assuming edits are enabled) at the start or end of the document text, or at the current caret position in the text field.  Text to be inserted comes from external files, chosen by the typical file selection dialog.
The Add Tag button opens a series of menus presenting a full complement of tag options.
Below the lower tool bar is a panel containing the Render button.
<taglit>
   Render     Render USML text from source area
              to rendering area
<h4>Rendering Area
<p>
The right-side (rendering area) component includes an instance of USML_DOC_BOX ('doc box') and associated controls.
Above the doc box is a toolbar with the following components.
<taglit>
 Save-As RTF
     Save document in RTF (Rich Text Format)
 Sarch Input Field
     Accepts string for which to search
     in rendered document
 Search
     Search rendered text for next instance
     of text string from input field;
     Case-sensitive
 Search Insensitive
     Search rendered text for next instance
     of text string from input field;
     Case-INsensitive
 Select Corresponding Source
     Selects, in the source text field, the text
     corresponding to the selection in the rendered
     text field
<h4>The Doc Box
<p>
The 'doc box' includes a vertically split area, on the left side of which is the heading navigator and on the right side of which is the text widget presenting the rendered text (an instance of EV_RICH_TEXT).
The contents of the heading navigator are generated automatically from any heading tags defined in the document source at the time of rendering.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_doc_box.png">[Doc Box Screenshot]</usml>
Below the doc box is a status field that shows loading status, and current position and selection information.
<minor>
Connecting Rendered to Source
<p>
From the rendered content in the doc_box, it is possible to select a range of rendered text and then, by pressing the Select Corresponding Source button, have the corresponding text in the source area highlighted.
<h3>The Macro Picker Dialog
<p>
The Macro Picker dialog presents a collection of pre-defined macros that can be selected and added to the current document.
<note>
The Macro Picker Dialog is invoked by selecting, from the main menu bar, the Tools/Macro Picker item.
<p>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_mpicker_no_cursor.png">[Macro Picker Screenshot]</usml>
Each pre-defined macro is listed in the navigator pane at top-left.  Selecting a macro from the navigator will expose, in the rendered text area at the top-right, the macro, a brief description, and an example of its effect.
Selecting a macro loads its name and tag text into the fields in the lower area.  The different tag form options are presented, and the macro is shown in the adjacent text area in accordance with the selected options.  The selected macro's tag (in the chosen form) can be added to the current document by selecting one of the insertion buttons (each corresponding to a position in the document.
The dialog remains open, enabling multiple selection opportunities, until being closed explicitly.
<h3>The Tag Maker Dialog
<p>
The Tag Maker dialog presents a collection of controls that let you select the type of tag and any associated format parameters.  Parameters and other options are arranged as a collection of topical frames: Section Type, Character Format, Paragraph Format, Outline Format, Tag Generation, Section Text and generated text fields.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_tag_maker.png">[Tag Maker Screenshot]</usml>
The Section Type frame presents radio buttons corresponding to section types (including outline levels) and special variants.  The Section Type frame also includes the "Init with Defaults" button, selection of which will fill the remaining fields in the Tag Maker dialog with values corresponding to the defaults for the selected type.
<note>
Changing the selected section type does not automatically update the fields to default values.
<p>
The Character Format, Paragraph Format, and Outline Format frames present options for setting character, paragraph and outline format parameters, respectively.
The Tag Generation frame presents options for tag generation, based on the values in the format fields.  The Generate button must be selected each time there is a change in values (the generated tag is not updated automatically with each change).
The Section Text field accepts arbitrary text to be included in the generated text (as an option, else built-in values are used.
In the lower area of the Tag Maker dialog has two text fields, side-by-side, the left one presenting the generated source (the tag along with a little context for illustration), and the right one presenting the output of rendering the source.
At the bottom the Tag Maker dialog are buttons that present options for copying or saving the generated tag, and for adding some or all of the generated tag, and/or some or all of the surrounding text to the document.
Tags created with the Tag Maker are single instances (i.e. neither macros nor document-wide format tags).
<h3>
The Macro Wizard Dialog
<p>
The Macro Wizard dialog walks through a succession of steps by which to create a custom macro.  The opening panel presents a (very) short introduction.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz/ss_mwiz_intro.png">[Macro Wizard Opening Panel Screenshot]</usml>
From the introduction step, press the Start button the reach Step 1.
In Step 1, define a (required) name for the macro and, optionally, a brief description.  Then select the macro type.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz/ss_mwiz_1.png">[Macro Wizard Step 1 Screenshot]</usml>
In this example, the macro name is "code" and the description is "fixed-pitch, code-like characters".  The type is untyped, character-only.
The title string of the wizard reflects the progress through the steps.  In this case, we are at step 1 of 2.  Other macro types would involve more steps.
As we want the macro to define only specific character format parameters (those for which we have deviated from the default settings), we elect to include only the non-default parameters we set (you can change this option at the final step).
Pressing the Next button takes us to Step 2.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz/ss_mwiz_2.png">[Macro Wizard Step 2 Screenshot]</usml>
In Step 2, we change the font family to be Fixed, set the weight to Bold, but leave the point size at 11 points.  We also leave letter casing as-is, but change the color of the text from black to a dark blue (0x000080).
<note>
To set the color, we can edit the hexadecimal value directly, or open the color picker dialog by selecting the ".." button.  If editing directly, the value is checked for validity at each change, and the if not valid, the field "swatch" field at the left turns orange and contains a large 'E' to indicate an error.
<p>
In the text area near the bottom of the window we can see the effect of the macro parameters on example text.  Assuming we are satisfied with that result, we press the Finish button.
The finish pane presents our macro as we defined it, along with an example of it being applied and rendered.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz/ss_mwiz_fin.png">[Macro Wizard Finish Pane Screenshot]</usml>
We now have the opportunity to regenerate the macro in different forms.  We can have the macro include all parameters, whether we set them or not.  We can have the macro generated in single-line form (vs the default multi-line form).  We can also choose (as in the example) to have the macro use as compact a form as possible, including using shorthand notations where appropriate (the default being long-form, with individual named parameters).
At this point, we can save the new macro to the document (the source text widget), or to a file, or we can go back to earlier steps and change some of the parameters.
<h4>Creating an Appendix Heading Macro
<p>
Another, more ambitious example is in order.  This time, we'll create a typed macro: an appendix heading.
In Step 1, select "H1" as the kind of macro.
<note>
Note that the dialog title string changes to show that there are 4 steps involved (vs the 2 steps for a character-only macro).
<p>
We'll call this macro "AH1", using all upper case letters in the name.
<note>
Macro names, unlike tags, are case-sensitive.
<p>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_1.png">[Appendix Heading Macro Step 1]</usml>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_2.png">[Appendix Heading Macro Step 2]</usml>
Pressing the Next button brings us to the by-now-familiar Step 2.  We want the appendix heading to look like a regular heading (with exceptions yet to come), so we have nothing to change in Step 2.  We can advance to Step 3 by selecting either the Skip button or the Next button.  They have the same effect in this context.
In Step 3, we have the opportunity to change paragraph format parameters, but, again, we want to keep the defaults for a level-1 heading, so we move on the Step 4.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_3.png">[Appendix Heading Macro Step 3]</usml>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_4.png">[Appendix Heading Macro Step 4]</usml>
In Step 4, we change the outline format.  Instead of numbers, we want our outline labels to be upper case letters ("ALPHA") and we want those labels to include "Appendix", so we  add that string to the Pre-Text field.  The starting value remains at 1, but we change the separator string from a single space to a space-hyphen-space sequence (" - ").
The effect of these changes is reflected in the text field at the bottom of the pane.  Assuming total satisfaction with the result, select the Finish button to review the generated macro.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_fin.png">[Appendix Heading Macro Finish]</usml>
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_mwiz_ah1/ss_mwiz_ah1_fin_comp.png">[Appendix Heading Macro Finish - Compact]</usml>
The rendered text uses the macro name and description as its example text.
We can leave the form as-is, using the default configuration (multi-line, long-form), or we can, as we did in the first example, opt for the compact form (though staying with the multi-line option).  Note well that the rendered form is the same, regardless of the macro form options we select at this point.
We can, as before, save the new macro to the document (the source text widget), or to a file, or we can go back to earlier steps and change some of the parameters.
<h2>Loading a Document from a File
<p>
Begin by selecting the Open item from the tool bar (or the corresponding option from the menu bar) and select, from the File Open dialog, the file ex_headings.usdf.
The document will be read from the selected file, loaded into the source text widget, then interpreted, and rendered in the rich text widget.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_ex_headings.png">[Screenshot ex_headings.usdf]</usml>
Each heading defined in the document appears in the navigator.  Items in the navigator are live links, so selecting a heading from the navigator will cause the rendered text widget to scroll to expose the selected heading.  In the example, there is no scrolling needed, so this behavior is not apparent.
<h2>Creating/Editing a Document
<p>
To create a document, begin with an existing one or start from scratch, but first, enable editing in the source text field by selected in the Enable Edits check button in the lower toolbar.
The source text field is an unformatted text field.  Text can be entered or deleted (and searched if so desired).
To make document creation and editing a more productive activity, Aura offers a few helpful tools, available from the lower toolbar in the source area.  Each of these was described in earlier sections.
When the document source is to your liking, or you're just curious, press the Render button at the base of the source area to generate a new document from the text in the source text field, and render that document in the rendered text field.
In this example, we used the Add Tag menu system to add the heading and paragraph tags, then (emboldened by our success to this point) hand-entered the comment tag and text.
Then, using the Macro Wizard, we created a macro called "goofy", inserted it into our document, and then used it to define the format of the last paragraph.
  <usml inline color="0000ff";xref="exti,$AURA_HOME/images/ss_app/ss_goofy.png">[Screenshot goofy]</usml>
<note>
Note that, in the source, the headings (document and level-1) have their associated text immediately following, on the same line as the tag, but the paragraph and comment tags (and goofy), have their associated text on the line(s) after the tag.  This is merely convention.  Single-line tags can have their associated text on the same line or on following lines, or both.  Multi-line tags do not accept text on the same line as the tag.
<p>
The goofy macro is so appealing, why not us that background color to highlight other text in the document?  Rather than create a new macro for this, there is the option to define a color for use by the highlighting facility ("Highlighter" is trademarked, as is the item after which this facility is modeled: "Hi-Liter").
From the Options menu in the main menu bar, select the "Set Highlighting Color" item.  This will open the Color Picker dialog (introduced earlier, in the Tag Maker Dialog section).  Select a suitable color from the dialog.
Now, in the rendered text, select the range of text you wish to highlight, then click on the Highlight Selected Text item in the rendering area toolbar.
This will insert an inline tag, in the source text, that sets the background color for the selected region to the color chosen in the Color Picker dialog.  If no color has been chosen, the default highlighting color (yellow) is applied.  The changed text is then rendered.
<h2>Saving the Document
<p>
Aura lets you save the current document as a plain text file, with or without markup, or as an RTF (Rich Text Format) file.  Use the preferred items from the tool bars.
<nota_bene>
Note well that the save-as-RTF feature uses the underlying implementation of the rich text widget (EV_RICH_TEXT) and there are issues, it seems with the accuracy of that implementation.
<h2>Search
<p>
Aura lets you search the source text field or the rendered text field for a given text string.  To search for a string, enter it in the text input field in the appropriate tool bar (in the source area to search the source text, or the rendered area to search the rendered text) and select either the Search or Search Case-Insensitive items from that tool bar.
The search begins at the present caret (cursor) position.  Successive invocations continue the search from the last match, if any.  If the end of the document is reached before a match is found, the search will wrap around the beginning of the document.

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_a: STRING
		once
			create Result.make (3600)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=1>Single-Line Tags
<codeb>
________________________________________________________________________
<codeb>
                               Structure Tags
________________________________________________________________________
Prefix (Case-Insensitive)    Parameters          Description
------------------------------------------------------------------------
 <usml C>                    None                Comment
      <C>
 <usml P[ <formats>]>        Char, Par           Paragraph text
      <P[ <formats>]>
 <usml HT[ <formats>]>       Char, Par           Title Heading
      <HT[ <formats>]>
 <usml H1[ <formats>]>       Char, Par, Out      Heading Level 1
      <H1[ <formats>]>
 <usml H2[ <formats>]>       Char, Par, Out      Heading Level 2
      <H2[ <formats>]>
 <usml H3[ <formats>]>       Char, Par, Out      Heading Level 3
      <H3[ <formats>]>
 <usml H4[ <formats>]>       Char, Par, Out      Heading Level 4
      <H4[ <formats>]>
 <usml H5[ <formats>]>       Char, Par, Out      Heading Level 5
      <H5[ <formats>]>
 <usml L1[ <formats>]>       Char, Par, Out      List Item Level 1
      <L1[ <formats>]>
 <usml L2[ <formats>]>       Char, Par, Out      List Item Level 2
      <L2[ <formats>]>
 <usml L3[ <formats>]>       Char, Par, Out      List Item Level 3
      <L3[ <formats>]>
 <usml L4[ <formats>]>       Char, Par, Out      List Item Level 4
      <L4[ <formats>]>
 <usml L5[ <formats>]>       Char, Par, Out      List Item Level 5
 <L5[ <formats>]>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codeb>
                             Format Definitions
________________________________________________________________________
Prefix (Case-Insensitive)  Params    Description
------------------------------------------------------------------------
 <usml FP[ <formats>]>     Char, Par  Paragraph Format Definition
      <FP[ <formats>]>
 <usml FHT [ <formats>]>   Char, Par  Title Heading Format Definition
      <FHT [ <formats>]>
 <usml FH1 [ <formats>]>   Char, Par  Heading Level 1 Format Definition
      <FH1[ <formats>]>
 <usml FH2 [ <formats>]>   Char, Par  Heading Level 2 Format Definition
      <FH2[ <formats>]>
 <usml FH3 [ <formats>]>   Char, Par  Heading Level 3 Format Definition
      <FH3[ <formats>]>
 <usml FH4 [ <formats>]>   Char, Par  Heading Level 4 Format Definition
      <FH4[ <formats>]>
 <usml FH5 [ <formats>]>   Char, Par  Heading Level 5 Format Definition
      <FH5[ <formats>]>
 <usml FL1 [ <formats>]>   Char, Par  List Item Level 1 Format Definition
      <FL1 [ <formats>]>
 <usml FL2 [ <formats>]>   Char, Par  List Item Level 2 Format Definition
      <FL2 [ <formats>]>
 <usml FL3 [ <formats>]>   Char, Par  List Item Level 3 Format Definition
      <FL3 [ <formats>]>
 <usml FL4 [ <formats>]>   Char, Par  List Item Level 4 Format Definition
      <FL4 [ <formats>]>
 <usml FL5 [ <formats>]>   Char, Par  List Item Level 5 Format Definition
      <FL5 [ <formats>]>
------------------------------------------------------------------------

<codeb>
________________________________________________________________________
<codeb>
                        Untyped (Format-Only) Macros
________________________________________________________________________
Prefix (Case-Insensitive)  Parameters      Description
------------------------------------------------------------------------
 <usml M[ <formats>]>      Char, Par, Out  Macro, Un-typed
      <M[ <formats>]>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codeb>
                                Typed Macros
________________________________________________________________________
Prefix (Case-Insensitive)  Parameters      Description
------------------------------------------------------------------------
 <usml MP[ <formats>]>     Char, Par       Paragraph Macro
      <MP[ <formats>]>
 <usml MHT[ <formats>]>    Char, Par, Out  Title Heading Macro
      <MHT[ <formats>]>
 <usml MH1[ <formats>]>    Char, Par, Out  Heading Level 1 Macro
      <MH1[ <formats>]>
 <usml MH2[ <formats>]>    Char, Par, Out  Heading Level 2 Macro
      <MH2[ <formats>]>
 <usml MH3[ <formats>]>    Char, Par, Out  Heading Level 3 Macro
      <MH3[ <formats>]>
 <usml MH4[ <formats>]>    Char, Par, Out  Heading Level 4 Macro
      <MH4[ <formats>]>
 <usml MH5[ <formats>]>    Char, Par, Out  Heading Level 5 Macro
      <MH5[ <formats>]>
 <usml ML1[ <formats>]>    Char, Par, Out  List Item Level 1 Macro
      <ML1[ <formats>]>
 <usml ML2[ <formats>]>    Char, Par, Out  List Item Level 2 Macro
      <ML2[ <formats>]>
 <usml ML3[ <formats>]>    Char, Par, Out  List Item Level 3 Macro
      <ML3[ <formats>]>
 <usml ML4[ <formats>]>    Char, Par, Out  List Item Level 4 Macro
      <ML4[ <formats>]>
 <usml ML5[ <formats>]>    Char, Par, Out  List Item Level 5 Macro
      <ML5[ <formats>]>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codeb>
                               Specialty Tags
________________________________________________________________________
Prefix (Case-Insensitive)  Parameters      Description
------------------------------------------------------------------------
 <usml include <path>>     Char, Par       Include directive
      <include <path>>
 <usml V>                  NONE            Verbatim section open
      <V>
 </usml V>                 NONE            Verbatim section close
      </V>
 <usml H+>                 NONE            Pushed document context
      <H+>
 <usml H->                 NONE            Pops document context
      <H->
------------------------------------------------------------------------

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_b: STRING
		once
			create Result.make (5200)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=2>Multi-Line Tags
<p>
Multi-line tags begin with an opening tag and terminate with "</usml x>" or "</x>" at the start of a line.
<codeb>
________________________________________________________________________
<codebc>
Structure Tags
<codeb>
________________________________________________________________________
Opening Tag           Formats                  Description
------------------------------------------------------------------------
 <usml X C>           None                     Comment
      <X C>			           
 <usml X P>           Char, Par                Paragraph text
      <X P>			           
 <usml X HT>          Char, Par                Title Heading
      <X HT>			           
 <usml X H1>          Char, Par, Out           Heading Level 1
      <X H1>			           
 <usml X H2>          Char, Par, Out           Heading Level 2
      <X H2>			           
 <usml X H3>          Char, Par, Out           Heading Level 3
      <X H3>			           
 <usml X H4>          Char, Par, Out           Heading Level 4
      <X H4>			           
 <usml X H5>          Char, Par, Out           Heading Level 5
      <X H5>			           
 <usml X L1>          Char, Par, Out           List Item Level 1
      <X L1>			           
 <usml X L2>          Char, Par, Out           List Item Level 2
      <X L2>			           
 <usml X L3>          Char, Par, Out           List Item Level 3
      <X L3>			           
 <usml X L4>          Char, Par, Out           List Item Level 4
      <X L4>			           
 <usml X L5>          Char, Par, Out           List Item Level 5
      <X L5>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codebc>
Format Definitions
<codeb>
________________________________________________________________________
Opening Tag        Formats         Description
------------------------------------------------------------------------
 <usml X FP>       Char, Par       Paragraph Format Definition
      <X FP>
 <usml X FHT>      Char, Par       Title Heading Format Definition
      <X FHT>
 <usml X FH1>      Char, Par, Out  Heading Level 1 Format Definition
      <X FH1>
 <usml X FH2>      Char, Par, Out  Heading Level 2 Format Definition
      <X FH2>
 <usml X FH3>      Char, Par, Out  Heading Level 3 Format Definition
      <X FH3>
 <usml X FH4>      Char, Par, Out  Heading Level 4 Format Definition
      <X FH4>
 <usml X FH5>      Char, Par, Out  Heading Level 5 Format Definition
      <X FH5>
 <usml X FL1>      Char, Par, Out  List Item Level 1 Format Definition
      <X FL1>
 <usml X FL2>      Char, Par, Out  List Item Level 2 Format Definition
      <X FL2>
 <usml X FL3>      Char, Par, Out  List Item Level 3 Format Definition
      <X FL3>
 <usml X FL4>      Char, Par, Out  List Item Level 4 Format Definition
      <X FL4>
 <usml X FL5>      Char, Par, Out  List Item Level 5 Format Definition
      <X FL5>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codebc>
Untyped (Format-Only) Macros
<codeb>
________________________________________________________________________
Opening Tag            Formats           Description
------------------------------------------------------------------------
 <usml X M>            Char, Par, Out    Macro, Un-typed
      <X M>
------------------------------------------------------------------------
<codeb>
________________________________________________________________________
<codebc>
Typed Macros
<codeb>
________________________________________________________________________
Opening Tag            Formats           Description
------------------------------------------------------------------------
 <usml X MP>           Char, Par         Paragraph Macro
      <X MP>
 <usml X MHT>          Char, Par         Title Heading Macro
      <X MHT>
 <usml X MH1>          Char, Par, Out    Heading Level 1 Macro
      <X MH1>
 <usml X MH2>          Char, Par, Out    Heading Level 2 Macro
      <X MH2>
 <usml X MH3>          Char, Par, Out    Heading Level 3 Macro
      <X MH3>
 <usml X MH4>          Char, Par, Out    Heading Level 4 Macro
      <X MH4>
 <usml X MH5>          Char, Par, Out    Heading Level 5 Macro
      <X MH5>
 <usml X ML1>          Char, Par, Out    List Item Level 1 Macro
      <X ML1>
 <usml X ML2>          Char, Par, Out    List Item Level 2 Macro
      <X ML2>
 <usml X ML3>          Char, Par, Out    List Item Level 3 Macro
      <X ML3>
 <usml X ML4>          Char, Par, Out    List Item Level 4 Macro
      <X ML4>
 <usml X ML5>          Char, Par, Out    List Item Level 5 Macro
      <X ML5>
------------------------------------------------------------------------

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_c: STRING
		once
			create Result.make (6600)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=3>Format Parameters
<AH2>Character Parameters
<p>
Character format parameters can be specified individually or using the shorthand form (or by a combination of the two).
<minor>Shorthand Form
<p>
Shorthand forms are quoted, comma-separated lists of parameters.  Each parameter position must exist, but can be empty (i.e. there must 8 commas, even if there is nothing between them).
<codelit>
 <char_fmt> ::= char="<ff>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<rc>"
       <ff> ::= <font family>     (font=)
       <ps> ::= <point size>      (point_size=)
       <cv> ::= <case_value>      (case=)
       <vs> ::= <offset value>    (offset=)
       <b>  ::= <bool value>      (bold=)
       <i>  ::= <bool value>      (italic=)
       <u>  ::= <bool value>      (underlined=)
       <st> ::= <bool value>      (strike=)
       <rc> ::= <color value>     (color=)
<exh>Example
<taglit>
 <usml p char="fixed,14,,,1,1,,0000ff">
<minor>Not Included in the Shorthand Form
<p>
Defined separately, using the "bgcolor=" parameter, is the text background color.
<codeb>
_____________________________________________________________
Label       Characteristics            Acceptable Values
-------------------------------------------------------------
font        Font Family                sans, fixed, times
point_size  Size of characters  Positive decimal integer
            in points
case        Letter case                1, -1, 10, U, L, T
            applies to all associated text
offset      Vertical offset in pixels  Signed decimal integer
            (positive is upward)
bold        Bold flag                  1, 0, true, false
italic      Italics flag               1, 0, true, false
underlined  Underlined flag            1, 0, true, false
strike      Strike-through flag        1, 0, true, false
color       Text color                 6-char hex RGB color
-------------------------------------------------------------
bgcolor     Text background color      6-char hexa RGB color
-------------------------------------------------------------
<AH2>Paragraph Parameters
<p>
Paragraph format parameters can be specified individually or using the shorthand form (or by a combination of the two).
<minor>Shorthand Form
<p>
Shorthand forms are quoted, comma-separated lists of parameters.  Each parameter position must exist, but can be empty (i.e. there must 4 commas, even if there is nothing between them).
<codelit>
 <par="<av>,<l>,<r>,<t>,<b>"
    <av> ::= <alignment>        (align=)
    <l>  ::= <left margin>      (left=)
    <r>  ::= <right margin>     (right=)
    <t>  ::= <top spacing>      (top=)
    <b>  ::= <bottom spacing>   (bottom=)
<exh>Example
<taglit>
 <usml p par=",10,,6,12">
<minor>
Not Included in the Shorthand Form
<p>
Defined separately, using the <code>"intext="</usml> parameter, is the text to be included with the tag, in lieu of text from stream (i.e. outside the tag).
<codeb>
____________________________________________________________
Label      Characteristics          Acceptable Values
------------------------------------------------------------
align      Alignment                L, R, C, J, 0, 1, -1, 2
left       Left margin              Positive decimal integer
right      Right margin             Positive decimal integer
top        Top spacing              Positive decimal integer
bottom     Bottom spacing           Positive decimal integer
------------------------------------------------------------
intext     Text included with tag   String
           (in lieu of text from stream)
------------------------------------------------------------
<AH2>Outline Parameters
<p>
Outline format parameters can be specified individually or using the shorthand form (or by a combination of the two).
<minor>Shorthand Form
<p>
Shorthand forms are quoted, comma-separated lists of parameters.  Each parameter position must exist, but can be empty (i.e. there must 2 commas, even if there is nothing between them).
<codelit>
 out="<lt>,<si>,<pt>"
   <lt>  ::= <label type>         (num_fmt=)
   <si>  ::= <start instance>     (start=)
   <pt>  ::= <pre text>           (pre_text=)
<exh>Example
<taglit>
 <usml L1 out="t,,*">
<minor>
Not Included in the Shorthand Form
<p>
Defined separately, using the <code>"num_sep="</usml> parameter, is the outline label separator (the string separating the outline label from the text).
<codeb>
___________________________________________________________
Label     Characteristics          Acceptable Values
-----------------------------------------------------------
num_fmt   Outline label format     #, A, a, I, i, t
start     Starting instance value  Positive decimal integer
pre_text  Outline label text       String
          (in lieu of std labels)  for num_fmt 't' only
-----------------------------------------------------------
num_sep   Outline label separator  String
          (between label and text)
-----------------------------------------------------------
<AH2>Independent Parameters (not in shorthand forms)
<codeb>
_______________________________________________________________
Label     Characteristics                  Acceptable Values
---------------------------------------------------------------
ref       Reference label                  Contiguous string
xref      Cross-reference (target) label   Contiguous string
no_lines  Disables inline tag              Standalone
          interpretation in section        no associated values
---------------------------------------------------------------
<p>
The <code>no_inlines</usml> parameter is standalone, without a <code>"<param>="</usml> prefix.  Inclusion in a structure tag suppresses interpretation of inline tags for that section.
The <code>include</usml> tag accepts a pathname as a single string parameter, without a prefix.
<exh>Example
<taglit>
 <usml include "some_folder/my_macros.usdf">
<p>
or
<taglit>
 <include "some_folder/my_macros.usdf">
<p>
just some text as a test

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_d: STRING
		once
			create Result.make (16000)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=4>Default Formats
<AH2>Default Paragraph Formats
<codeb>
_________________________________________________________
Format                  Parameter name   Default Value
---------------------------------------------------------
Font family             font             "times"
Text size               point_size       11 pt
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              0
Bottom margin/spacing   bottom           6 pixels
Pre-paragraph text      pre_text         Undefined
---------------------------------------------------------
<AH2>Default Title Heading Formats
<codeb>
_________________________________________________________
Format                  Parameter name   Default Value
---------------------------------------------------------
Font family             font             "sans"
Text size               point_size       18
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             True
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Center
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              0
Bottom margin/spacing   bottom           15 pixels
Pre-paragraph text      pre_text         Undefined
---------------------------------------------------------
<AH2>Default Level-1 Heading Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "sans"
Text size               point_size       16
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             True
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              10 pixels
Bottom margin/spacing   bottom           10 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-2 Heading Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "sans"
Text size               point_size       14
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             True
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              10 pixels
Bottom margin/spacing   bottom           10 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-3 Heading Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "sans"
Text size               point_size       12
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             True
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              6 pixels
Bottom margin/spacing   bottom           6 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-4 Heading Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "sans"
Text size               point_size       12
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             True
Italics                 italic           True
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              6 pixels
Bottom margin/spacing   bottom           6 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-5 Heading Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "sans"
Text size               point_size       12
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           True
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             0
Right margin            right            0
Top margin/spacing      top              6 pixels
Bottom margin/spacing   bottom           6 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-1 List Item Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "times"
Text size               point_size       11
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             5 pixels
Right margin            right            0
Top margin/spacing      top              6 pixels
Bottom margin/spacing   bottom           6 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-2 List Item Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "times"
Text size               point_size       11
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             10 pixels
Right margin            right            0
Top margin/spacing      top              0 pixels
Bottom margin/spacing   bottom           0 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-3 List Item Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "times"
Text size               point_size       11
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             15 pixels
Right margin            right            0
Top margin/spacing      top              0 pixels
Bottom margin/spacing   bottom           0 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-4 List Item Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "times"
Text size               point_size       11
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             20 pixels
Right margin            right            0
Top margin/spacing      top              0 pixels
Bottom margin/spacing   bottom           0 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------
<AH2>Default Level-5 List Item Formats
<codeb>
______________________________________________________________
Format                  Parameter name   Default Value
--------------------------------------------------------------
Font family             font             "times"
Text size               point_size       11
Text color              color            Black
Text background color   bgcolor          Undefined
Text case               case             Undefined
Text vertical offset    offset           0
Bold                    bold             False
Italics                 italic           False
Underlining             underline        False
Strike-through          strike           False
Paragraph alignment     align            Left
Left margin             left             25 pixels
Right margin            right            0
Top margin/spacing      top              0 pixels
Bottom margin/spacing   bottom           0 pixels
Outline number format   num_fmt          Decimal integer ('#')
Outline label separator num_sep          Single space
Pre-paragraph text      pre_text         Undefined
--------------------------------------------------------------

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_e: STRING
		once
			create Result.make (16000)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=5>A Collection of Macros
<minor>Bold text
<note>
for inlines, other macros
<taglit>
 <usml m name="I";italic=1>
<minor>Italicized text
<note>
For inlines, other macros
<taglit>
 <usml m name="I";italic=1>
<minor>Underlined text
<note>
For inlines, other macros
<taglit>
 <usml m name="U";char=",,,,,,1,,">
<minor>Text color
<note>
For inlines, other macros
<taglit>
 <usml m name="Red";char=",,,,,,,,c00000">
 <usml m name="Green";char=",,,,,,,,008000">
 <usml m name="Blue";char=",,,,,,,,0000c0">
<minor>Fixed-pitch, bold, navy text
<note>
Character format only, for inlines, other macros
<taglit>
 <usml m name="code";char="fixed,-2,,,1,,,,000080">
<minor>Fixed-pitch, normal, navy text
<note>
Character format only, for inlines, other macros
<taglit>
 <usml m name="codelt";char="fixed,-2,,,,,,,000080">
<minor>Fixed-pitch, navy text, (paragraph, inlines enabled)
<note>
Paragraph macro; uses "code" character macro
<taglit>
 <usml mp name="codep";macro="code";par=",,,0,0">
<minor>Fixed-pitch, bold, navy text
<note>
Paragraph, inlines suppressed (literal)
<taglit>
 <usml mp name="codelit";macro="code";par=",,,0,0";no_inlines>
<minor>Fixed-pitch, normal, navy text
<note>
Paragraph, inlines suppressed (literal)
<taglit>
 <usml mp name="taglit";macro="codelt";par=",,,0,0";no_inlines>
<minor>Fixed-pitch, bold, black text
<note>
Paragraph
<taglit>
 <usml mp name="codeb";macro="code";color="000000";par=",,,0,0">
<minor>Fixed-pitch, black text, centered
<note>
Paragraph
<taglit>
 <usml mp name="codebc";macro="code";color="000000";par="c,,,0,0">
<minor>Superscript macro
<note>
Character format, for inlines, other macros
<taglit>
 <usml m name="super";char=",-3,,4,,,,,ff00ff">
<minor>Indented and italicized
<note>
Paragraph
<taglit>
 <usml mp name="note";char=",,,,,1,,,";par=",10,,,">
<minor>Indented paragraph
<note>
Paragraph
<taglit>
 <usml mp name="ipar";par=",10,,,">
<minor>Indented paragraph as example in text
<note>
Paragraph
<taglit>
 <usml mp name="ipar_1";left=10>
<minor>Indented and bold+italicized
<note>
Paragraph
<taglit>
 <usml mp name="nota_bene";char=",,,,1,1,,,";par=",10,,,">
<minor>Heading-like format (not outlined); bold
<note>
Paragraph; for examples
<taglit>
 <usml mp name="exh";bold=1>
<minor>Minor heading-like (not outlined) bold
<note>
Paragraph
<taglit>
 <usml mp name="minor";char="sans,+1,,,1,,,,">
<minor>Centered separator
<note>
Paragraph, with intext
<taglit>
 <usml x mp>
  name="csep";
  char=",,,,1,,1,,";
  par="c,,,,";
  intext="--------------------------------------------------"
  </usml x>
<minor>Left-aligned separator
<note>
Paragraph, with intext
<taglit>
 <usml x mp>
  name="sep";
  char=",,,,1,,1,,";
  par=",,,,";
  intext="--------------------------------------------------"
 </usml x>
<minor>Level-1 Appendix heading
<note>
Heading (outlined), with pre-text
<taglit>
 <usml x mH1>
  name="AH1";
  par=",,,10,6";
  out="A,,Appendix "
 </usml x>
<minor>Level-2 Appendix heading
<note>
Heading (outlined), with pre-text
<taglit>
 <usml x mH2>
  name="AH2";
  par=",,,10,6";
  out="#,,"
 </usml x>
<minor>Level-1 bullet list item
<note>
List item (outlined), with pre-text
<taglit>
 <usml x mL1>
  name="b1";
  par=",10,,0,";
  out="t,,- "
 </usml x>
<minor>Level-2 bullet list item
<note>
List item (outlined), with pre-text
<taglit>
 <usml x mL2>
  name="b2";
  par=",20,,0,";
  out="t,,o "
 </usml x>
<minor>Level-3 bullet list item
<note>
List item (outlined), with pre-text
<taglit>
 <usml x mL3>
  name="b3";
  par=",30,,0,";
  out="t,,* "
 </usml x>
<minor>Level-4 bullet list item
<note>
List item (outlined), with pre-text
<taglit>
 <usml x mL4>
  name="b4";
  par=",40,,0,";
  out="t,,- "
 </usml x>

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_appendix_f: STRING
		once
			create Result.make (3600)
			Result.append (Ks_usml_doc_common_macros)
			Result.append ("{
<AH1 start=6>A Basic Template
<p>
What follows is a basic document template, with a few macros and a simple document structure.
<taglit>
<usml v>
<HT>Document Title
<c>-------------------------
By convention, front matter (incl. macro defs, includes, ..) follows document title, so one need search for the title
<c>-------------------------
<c>Superscript character macro
<usml m name="super";char=",-3,,4,,,,,">
<c>-------------------------
<c>Subscript character macro
<usml m name="sub";char=",-3,,-4,,,,,">
<c>-------------------------
<c>An indented and italicized paragraph
<x mp>
  name="note";
  char=",,,,,1,,,";
  par=",10,,,"
</x>
<c>-------------------------
<c>An indented and bold + italicized paragraph
<x mp>
  name="nota_bene";
  char=",,,,1,1,,,";
  par=",10,,,"
</x>
<c>-------------------------
<c>Minor heading-like (not outlined) bold
<x mp>
  name="minor_heading";
  char="sans,+1,,,1,,,,"
</x>
<c>-------------------------
<c>Centered separator using asterisks
<x mp>
  name="starsep_c";
  char=",,,,1,,1,,";
  align="c";
  intext="*************************"
</x>
<c>-------------------------
<c>Level-1 Appendix heading
<x mH1>
  name="AH1";
  par=",,,10,6";
  out="A,,Appendix ";
  num_sep=" - "
 </x>
<c>-------------------------
<c>Level-2 Appendix heading
<x mH2>
  name="AH2";
  par=",,,10,6";
  out="#,,";
  num_sep=" - "
</x>
<c>-------------------------
<c>Level-1 bullet list item
<x mL1>
  name="B1";
  par=",10,,0,";
  out="t,,- "
 </x>
<c>-------------------------
Level-2 bullet list item
<x mL2>
  name="B2";
  par=",20,,0,";
  out="t,,o "
 </usml x>
<c>-------------------------
Level-3 bullet list item
<x mL3>
  name="B3";
  par=",30,,0,";
  out="t,,* "
 </x>
<c>-------------------------
Level-4 bullet list item
<x mL4>
  name="B4";
  par=",40,,0,";
  out="t,,- "
 </x>
<c>-------------------------
<H1>Introduction
<H2>Overview
<p>
Add an overview
<H1>Another Chapter
<p>
Some text
<H2>A subsection
<p>
Some text
<AH1 start=1>My First Appendix
<p>
Some text
<AH2>A Subsection in my First Appendix
<p>
Some text
<AH1> My Second Appendix
<p>
Some text
</usml v>

}") -- "} format issues
		end

	--|--------------------------------------------------------------

	Ks_usml_doc_common_macros: STRING
		once
			create Result.make (3600)
			Result.append ("{
<usml fp char="times,12,,,,,,,";par=",,,3,3">
<usml mht name="htx";char=",-2,,,,,,,">
<usml fL1 point_size=12>
<usml fL2 point_size=12>
<usml fL3 point_size=12>
<usml fL4 point_size=12>
<usml fL5 point_size=12>
<usml c>
This is bold text (e.g. for inlines)
<usml m name="B";char=",,,,1,,,,">
<usml c>
This is italicized text (e.g. for inlines)
<usml m name="I";char=",,,,,1,,,">
<usml c>
This is underlined text (e.g. for inlines)
<usml m name="U";char=",,,,,,1,,">
<usml c>
These are text color macros
<usml m name="Red";char=",,,,,,,,c00000">
<usml m name="Green";char=",,,,,,,,008000">
<usml m name="Blue";char=",,,,,,,,0000c0">
<usml c>
This is a fixed-pitch, navy text, char format only
<usml m name="codelt";char="fixed,-2,,,,,,,000080">
<usml c>
This is a fixed-pitch, bold, navy text, char format only
<usml m name="code";char="fixed,-2,,,1,,,,000080">
<usml c>
This is a fixed-pitch, bold, navy text, paragraph
<usml mp name="codep";macro="code";par=",,,0,0">
<usml c>
This is a fixed-pitch, navy text, paragraph
<usml mp name="codeltp";macro="codelt";par=",,,0,0">
<usml c>
This is a fixed-pitch, bold, navy text, paragraph with inlines suppressed
<usml mp name="codelit";macro="code";par=",,,0,0";no_inlines>
<usml c>
This is a fixed-pitch, navy text, paragraph with inlines suppressed
<usml mp name="taglit";macro="codelt";par=",,,0,0";no_inlines>
<usml c>
This is a fixed-pitch, black text, paragraph
<usml mp name="codeb";macro="code";color="000000";par=",,,0,0">
<usml c>
This is a fixed-pitch, black text, centered paragraph
<usml mp name="codebc";macro="code";color="000000";par="c,,,0,0">
<usml c>
This is an indented and italicized paragraph
<usml mp name="note";char=",,,,,1,,,";par=",10,,,">
<usml c>
This is an indented paragraph
<usml mp name="ipar";par=",10,,,">
<usml c>This is an indented paragraph as example in text
<usml mp name="ipar_1";left=10>
<usml c>
This is an indented and bold+italicized paragraph
<usml mp name="nota_bene";char=",,,,1,1,,,";par=",10,,,">
<usml c>
This is an example heading; bold
<usml mp name="exh";bold=1>
<usml c>
This is a minor heading (not outlined) bold paragraph
<usml mp name="minor";char="sans,+1,,,1,,,,">
<usml c>
This is a centered separator
<usml x mp>
 name="csep";
 char=",,,,1,,1,,";
 par="c,,,,";
 intext="-----------------------------------------------------------------"
</usml x>
<usml c>
This is a left-aligned separator
<usml x mp>
 name="sep";
 char=",,,,1,,1,,";
 par=",,,,";
 intext="-----------------------------------------------------------------"
</usml x>
<usml c>
This a level-1 Appendix heading
<usml x mH1>
 name="AH1";
 par=",,,10,6";
 out="A,,Appendix "
</usml x>
<usml c>
This a level-2 Appendix heading
<usml x mH2>
 name="AH2";
 par=",,,10,6";
 out="#,,"
</usml x>
<usml c>
This a level-1 bullet list item
<usml x mL1>
 name="b1";
 par=",10,,0,";
 out="t,,- "
</usml x>
<usml c>
This a level-2 bullet list item
<usml x mL2>
 name="b2";
 par=",20,,0,";
 out="t,,o "
</usml x>
<usml c>
This a level-3 bullet list item
<usml x mL3>
 name="b3";
 par=",30,,0,";
 out="t,,* "
</usml x>
<usml c>
This a level-4 bullet list item
<usml x mL4>
 name="b4";
 par=",40,,0,";
 out="t,,- "
</usml x>
<usml c>
This is a superscript macro, for inline use
<usml m name="super";char=",-3,,4,,,,,ff00ff">
<usml c>
--------------------------------------------------

}") -- "} format issues
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-May-2019
--|     Created original module (for Eiffel 18.1)
--|     Adapted from AEL_V2_VERTICAL_BOX
--|----------------------------------------------------------------------

end -- class USML_HELP_DOCS
