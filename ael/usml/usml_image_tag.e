note
	description: "{
A USML tag identifying an in-stream image (picture)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_IMAGE_TAG

inherit
	USML_TAG
		redefine
			default_create, is_image, parse_tag_text
		end

create
	make_with_text

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	default_create
			-- Create Current in its default initial state
		do
			image_text := ""
			name := ""
			Precursor
		end

	--|--------------------------------------------------------------

	parse_tag_text
			-- Image tags can be long form or terse
			-- Long form prefix is "<usml image "
			-- Terse prefix is "<image "
			-- Name parameter is REQUIRED
			-- Associated text is binary image (equivalent of a PNG 
			-- file's contents)
			-- Images are NOT rendered in Rich Text, but can be shown 
			-- separately in appropriate widgets
			-- Images are stored in the document, keyed by name
			-- References can be added to the document to refer to the 
			-- image by name (as an xref).  It is the responsibility of 
			-- the rendering agent to deal with the hyperlinking and 
			-- image rendering
		local
			ts, low_v, low_ts, params: STRING
			sp, etp: INTEGER
			fl: LIST [STRING]
		do
			params := ""
			low_v := tag_text.as_lower
			if low_v.starts_with (Ks_usml_tag_image_prefix_u) then
				-- <usml image ...>
				sp := Ks_usml_tag_image_prefix_u.count
			elseif low_v.starts_with (Ks_usml_tag_image_prefix) then
				-- <image ...>
				sp := Ks_usml_tag_image_prefix.count
			else
				-- ERROR
				set_parse_error ("Invalid image prefix: " + tag_text)
			end
			if sp /= 0 then
				sp := sp + 1
				etp := next_rb_pos (tag_text, sp)
				if etp /= 0 then
					params := tag_text.substring (sp, etp - 1)
				else
					-- ERROR
					set_parse_error ("Image tag missing parameters: " + tag_text)
				end
			end
			if not has_error then
				fl := params.split (';')
				from fl.start
				until fl.after or has_error
				loop
					ts := fl.item
					low_ts := ts.as_lower
					if low_ts.starts_with (Ks_fmt_tag_name) then
						name := string_format_value (
							ts, Ks_fmt_tag_name.count + 1)
					else
						-- may be more params later, like image type
						set_parse_error (
							"Unexpected image tag parameter: " + tag_text)
					end
					fl.forth
				end
				if name.is_empty then
					set_parse_error (
						"Image tag missing name parameter: " + tag_text)
				end
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_image: BOOLEAN = True
	name: STRING

	src_line_number: INTEGER

	image_text: STRING

--|========================================================================
feature -- Section format (for text until next tag)
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_image_text (v: STRING)
		do
			image_text := v
		end

	set_src_line_number (v: INTEGER)
		do
			src_line_number := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_IMAGE_TAG
