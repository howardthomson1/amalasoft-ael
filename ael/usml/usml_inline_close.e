note
	description: "{
Position and tag text of an inline closing tag candidate
NOT an actual, valid inline closing tag unless qualified as such,
either as is_standard, or is_macro
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INLINE_CLOSE

inherit
	COMPARABLE
		undefine
			default_create
		end
	USML_CORE
		undefine
			is_equal
		redefine
			default_create
		end

create
	make_with_bounds

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_bounds (v: STRING; sp, ep: INTEGER)
		require
			large_enough: v.count >= 3
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep >= sp and ep <= v.count
			valid_lb: v.item (sp) = '<' and v.item (sp+1) = '/'
		do
			default_create
			tag_start := sp
			tag_end := ep
			set_tag_text (v.substring (sp, ep))
			if tag_text.as_lower ~ Ks_usml_tag_inline_close then
				is_standard := True
			elseif tag_text.as_lower ~ Ks_usml_tag_inline_close_u then
				is_standard := True
			else
				extract_macro_label
			end
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create tag_text.make (32)
			Precursor
		end

	--|--------------------------------------------------------------

	extract_macro_label
			-- Extract, from tag_text, the macro label
			-- If valid macro label, then set 'is_macro" to True
		require
			not_standard: not is_standard
		local
			ep: INTEGER
			ts: STRING
		do
			ep := tag_text.count - 1
			ts := tag_text.substring (3, ep)
			if attached macros.item (ts) then
				macro_label := ts
				is_macro := True
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	tag_start: INTEGER
			-- Index, in raw source, of 1st char of opening tag ('<')
	tag_end: INTEGER
			-- Index, in raw source, of last char of opening tag ('>')

	tag_text: STRING

	tag_length: INTEGER
			-- Length to opening tag (Current)
		do
			Result := tag_text.count
		end

	macro_label: detachable STRING
   is_standard: BOOLEAN
   is_macro: BOOLEAN

--|========================================================================
feature -- Status setting
--|========================================================================

	set_tag_text (v: STRING)
		do
			tag_text.wipe_out
			tag_text.append (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := tag_start < other.tag_start
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_INLINE_CLOSE
