note
	description: "{
A inline USML format (format with a start/end position of captive text)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INLINE_FORMAT

inherit
	USML_FORMAT
		redefine
			default_create
		end

create
	make_from_tag

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_tag (v: USML_INLINE_TAG)
			-- Create Current from inline tag 'v'
		require
			has_format: attached v.format
		do
			default_create
			section_start := v.section_start
			tag_text_start := v.tag_text_start
			tag_text_end := v.tag_text_end
			closing_tag_start := v.closing_tag_start
			closing_tag_end := v.closing_tag_end
			captive_text_start := v.captive_text_start
			captive_text_end := v.captive_text_end
			has_image_reference := v.has_image_reference
			has_pbuf_reference := v.has_pbuf_reference

			--captive_text_length := v.captive_text_length
			if attached v.format as fmt then
				init_from_other (fmt, True)
			end
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	tag_text_start: INTEGER
	tag_text_end: INTEGER
	tag_text_length: INTEGER
		do
			if tag_text_end >= tag_text_start then
				Result := 1 + tag_text_end - tag_text_start
			end
		end

	closing_tag_start: INTEGER
	closing_tag_end: INTEGER
	closing_tag_length: INTEGER
		do
			if closing_tag_end >= closing_tag_start then
				Result := 1 + closing_tag_end - closing_tag_start
			end
		end

	section_start: INTEGER
			-- Line number, in original text, at which section, in wich 
			-- Current resides, began

	captive_text_start: INTEGER
			-- Index, in paragraph text, of start of captive text
			-- IN ORIGINAL DOCUMENT

	captive_text_rendered_start: INTEGER
			-- Index, in paragraph text, of start of captive text
			-- IN RENDERED DOCUMENT

	captive_text_end: INTEGER
			-- Index, in paragraph text, of end of captive text
			-- IN ORIGINAL DOCUMENT

	captive_text_rendered_end: INTEGER
			-- Index, in paragraph text, of end of captive text
			-- IN RENDERED DOCUMENT

	has_image_reference: BOOLEAN
			-- Does current have a reference to an image?

	has_pbuf_reference: BOOLEAN
			-- Does current have a reference to a pixel buffer?

--RFO 	captive_text_length: INTEGER
			-- Length of captive text

	--|--------------------------------------------------------------

	positions_and_sizes_out: STRING
		local
			osp, oep, olen, csp, cep, clen: INTEGER
		do
			create Result.make (2048)
			osp := tag_text_start
			oep := tag_text_end
			olen := 1 + oep - osp
			csp := closing_tag_start
			cep := closing_tag_end
			clen := closing_tag_length
			Result.append (aprintf ("{
sp=%d, ep=%d, len=%d, cs=%d, ce=%d, cl=%d
}",
			<< osp, oep, olen, csp, cep, clen >>))
		end

--|========================================================================
feature -- Section format (for text until next tag)
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_captive_text_start (v: INTEGER)
			-- Set the index, in paragraph text, of start of captive text
		do
			captive_text_start := v
		end

	set_captive_text_end (v: INTEGER)
			-- Set the index, in paragraph text, of end of captive text
		do
			captive_text_end := v
		end

	--|--------------------------------------------------------------

	set_captive_text_rendered_start (v: INTEGER)
			-- Set the index, in paragraph text, of start of captive text
			-- in RENDERED text
		do
			captive_text_rendered_start := v
		end

	set_captive_text_rendered_end (v: INTEGER)
			-- Set the index, in paragraph text, of end of captive text
			-- in RENDERED text
		do
			captive_text_rendered_end := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_INLINE_FORMAT
