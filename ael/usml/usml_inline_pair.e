note
	description: "{
A pair of inline tags data, with start and end positions of opening
and closing tags, tag text and captive text, for use in sorting out
nested inline tags.
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INLINE_PAIR

inherit
	COMPARABLE
		undefine
			default_create
		end
	USML_CORE
		undefine
			is_equal
		redefine
			default_create
		end

create
	make_with_open_start
--, make_with_opens_and_closes

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_open_start (v: INTEGER)
		do
			default_create
			open_tag_start := v
		end

--RFO 	make_with_opens_and_closes (v: STRING; ol, cl: LIST [INTEGER_INTERVAL])
--RFO 		do
--RFO 			default_create
--RFO 			init_with_opens_and_closes (v, ol, cl)
--RFO 		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create open_tag_text.make (32)
			create close_tag_text.make (8)
			create captive_text_ranges.make
			Precursor
		end

	--|--------------------------------------------------------------

	init_with_opens_and_closes (v: STRING; ol, cl: LIST [INTEGER_INTERVAL])
			-- Initialize Current from source stream 'v' containing 
			-- inline opening tag positions (open+close) 'ol' and 
			-- closing positions 'cl', capturing tag attributes as well 
			-- as captive text values (may be non-contiguous, if nested)
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_outer: BOOLEAN
			-- Is Current an OUTER pair (having pairs nested within it)?

	is_inner: BOOLEAN
			-- Is Current an INNER pair (being nested within another pair)?

	open_tag_start: INTEGER
			-- Index, in raw source, of 1st char of opening tag ('<')
	open_tag_end: INTEGER
			-- Index, in raw source, of last char of opening tag ('>')

	open_tag_text: STRING

	open_tag_length: INTEGER
			-- Length to opening tag (Current)
		do
			Result := open_tag_text.count
		end

	close_tag_start: INTEGER
			-- Index, in raw source, of 1st char of closing tag
	close_tag_end: INTEGER
			-- Index, in raw source, of last char of closing tag

	close_tag_text: STRING

	close_tag_length: INTEGER
		do
			Result := close_tag_text.count
		end

	outer_open_start: INTEGER
			-- Index, in raw source, of first char of opening tag
			-- of immediately enclosing tag (IF Current is inner)
	outer_open_end: INTEGER
			-- Index, in raw source, of last char of opening tag
			-- of immediately enclosing tag (IF Current is inner)

	tags_length: INTEGER
			-- Total number of characters in Current's open and close 
			-- tags
			-- N.B. does NOT inlcude any tags nested in Current
		do
			Result := (1 + open_tag_end - open_tag_start) +
				(1 + close_tag_end - close_tag_start)
		end

	captive_text_ranges: TWO_WAY_LIST [INTEGER_INTERVAL]
			-- Ordered list of captive text start and stop indices
			-- in source text, AS-IF tags were NOT present
			-- (pre rendering, post inline conversion)

--|========================================================================
feature -- Section format (for text until next tag)
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_is_outer
		do
			is_outer := True
		end

	set_is_inner
		do
			is_inner := True
		end

	set_open_tag_end (v: INTEGER)
		require
			has_start: open_tag_start > 0
			valid: v > open_tag_start
		do
			open_tag_end := v
			open_tag_text.wipe_out
		ensure
			is_set: open_tag_end = v
		end

	set_open_tag_text (v: STRING)
		do
			open_tag_text.wipe_out
			open_tag_text.append (v)
		end

	--|--------------------------------------------------------------

	set_close_tag_start (v: INTEGER)
		require
			has_open_start: open_tag_start > 0
			valid: v > open_tag_start
		do
			close_tag_start := v
		end

	set_close_tag_end (v: INTEGER)
		require
			has_close_start_end: close_tag_start > 0
			valid: v > close_tag_start
		do
			close_tag_end := v
		ensure
			is_set: close_tag_end = v
		end

	set_close_tag_text (v: STRING)
		do
			close_tag_text.wipe_out
			close_tag_text.append (v)
		end

	--|--------------------------------------------------------------

	set_outer_open_start (v: INTEGER)
		require
			inner: is_inner
		do
			outer_open_start := v
		end

	set_outer_open_end (v: INTEGER)
		require
			inner: is_inner
		do
			outer_open_end := v
		end

	--|--------------------------------------------------------------

	add_captive_text_range (v: INTEGER_INTERVAL)
			-- Add captive text range 'v' to ranges
			-- Captive text indices pertain to source AFTER inlines have 
			-- been converted
		do
			captive_text_ranges.extend (v)
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := open_tag_start < other.open_tag_start
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_INLINE_TAG
