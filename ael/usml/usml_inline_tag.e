note
	description: "{
A inline USML (ultra-simple markup language) tag
Most USML tags are whole-line, but USML does permit special tags in-line
Inline tags have a different syntax than whole-line tags
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INLINE_TAG

inherit
	USML_TAG
		rename
			parse_tag_text as whole_line_parse_tag_text
		redefine
			default_create, is_inline
		end

create
	make_from_stream, make_from_substream

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_stream (v: STRING; spos: INTEGER)
			-- Create Current from stream 'v' beginning at index 'spos'
		local
			ep, csp, cep, clen: INTEGER
			emsg: STRING
		do
			default_create
			tag_text_start := spos
			ep := next_rb_pos (v, spos)
			--  ...<usml inline char="..";ref="blah">...
			--     ^                                ^
			--     spos                             ep
			--
			--  ...<usml mymacro>...
			--     ^            ^
			--     spos         ep
			--
			--  ...<inline mymacro>...
			--     ^              ^
			--     spos           ep
			--
			--  ...<mymacro>...
			--     ^       ^
			--     spos    ep
			if ep = 0 then
				-- ERROR, missing closing char
				set_parse_error ("Missing closing char for inline tag")
			else
				create emsg.make (0)
				set_tag_text (v.substring (spos, ep))
				csp := next_inline_close_start (v, ep, emsg)
				if not emsg.is_empty then
					set_parse_error (emsg)
				elseif csp = 0 then
					-- ERROR, missing closing char
					set_parse_error ("Missing inline closing tag")
				else
					cep := next_rb_pos (v, csp)
					clen := 1 + cep - csp
					--clen := Ks_usml_tag_inline_close.count
					closing_tag_start := csp
					closing_tag_end := csp + clen - 1
					closing_tag_length := clen
					parse_tag_text (tag_text)
				end
			end
		end

	--|--------------------------------------------------------------

	make_from_substream (v: STRING; osp, oep, csp, cep, sln: INTEGER)
			-- Create Current from substreeam of 'v' beginning with 
			-- opening tag start 'osp' and end position 'oep', 
			-- and closing tag start 'csp' and end 'cep', from source 
			-- line number 'sln'
		require
			valid_open: osp > 0 and is_inline_tag (v, osp) and oep > osp
			valid_close: csp > 0 and cep <= v.count and cep > csp
		do
			default_create
			init_from_substream (v, osp, oep, csp, cep, sln)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create items.make
			create captive_text_ranges.make
			Precursor
		end

	--|--------------------------------------------------------------

	init_from_substream (v: STRING; osp, oep, csp, cep, sln: INTEGER)
			-- Initialize Current from substreeam of 'v' beginning
			-- opening tag start 'osp' and end position 'oep', 
			-- and closing tag start 'csp' and end 'cep', from source 
			-- line number 'sln'
			--
			-- Captive text indices are "as-if" tags were removed, and 
			-- as such cannot be determined solely from indices given as 
			-- args, as those are relative to original stream.  While it 
			-- seems that this would be OK, it works only for the
			-- first pair of inline tags within a section because 
			-- all subsequent inlines are biased by the length of
			-- the tags that preceded them.
		require
			valid_open: osp > 0 and is_inline_tag (v, osp) and oep > osp
			valid_close: csp > 0 and cep <= v.count and cep > csp
			valid_line: sln > 0
		local
			otag: STRING
		do
			set_section_start (sln)
			otag := v.substring (osp, oep)
			tag_text_start := osp
			set_tag_text (otag)
			closing_tag_start := csp
			closing_tag_end := cep
			closing_tag_length := 1 + cep - csp
			parse_tag_text (otag)
		end

	--|--------------------------------------------------------------

	parse_tag_text (v: STRING)
			-- Opening tag begins with prefix, and maybe with the inline 
			-- prefix, but might use a macro instead
			-- Tag can include either a macro or a char format (or char 
			-- format macro, or separate params)
			-- Tag may also include a reference ID or cross-reference ID
			-- or combos thereof
			-- Must NOT include paragraph formatting
			--
			--   <usml inline char=",,,4,,,,,">xxx</usml>
			--   <usml inline macro="emphasis">xxx</usml>
			--   <usml inline emphasis>xxx</usml>
			--   <usml inline ref="my_ref_id">xxx</usml>
			--   <usml inline xref="my_ref_id">xxx</usml>
			-- Terse form (absent "inline")
			--   <usml emphasis>xxx</usml>
			-- Alt terse form (absent "inline")
			--   <inline emphasis>xxx</usml>
			-- Ultra-terse macro form:
			--   <emphasis>xxx</emphasis>
			--   <I>xxx</I>
			--
			-- Text between opening tag and closing tag is 'captive'
			-- Captive text is followed by closing tag
			--   </usml> (short/preferred form)
			--
			-- Char format is same as for whole-line tags
			-- Example, super-script
			--   <usml inline char=",,,4,,,,,">2</usml>AFTER
			--
			-- Macro format has the macro=name or just the macro name
			-- Example, macro
			--   <usml inline macro=super>2</usml inline>AFTER
			--   <usml super>2</usml inline>AFTER
			--
			-- If macro-name-only, then can still have format params, 
			-- those params superseding the values from the macro
			--
			-- Multiple params are semi-colon separated, as with other tags
			-- Example
			--   <usml inline bold=true;xref="somewhere_else">blah</usml>
		require
			valid: is_tag_bounded (v)
		local
			ts, low_v, mp, params: STRING
			sp, ep, spp, etp: INTEGER
		do
			etp := v.index_of ('>', 1)
			low_v := v.as_lower
			if low_v.starts_with (Ks_usml_tag_prefix_inline_u) then
				-- <usml inline ...>
				-- <usml inline macro="I";ref="pos_1">
				-- <usml inline I ref="pos_1">
				--sp := Ks_usml_tag_prefix_inline.count + 1
				sp := asr.index_of_next_non_whitespace (
					low_v, Ks_usml_tag_prefix_inline_u.count + 1, True)
			elseif low_v.starts_with (Ks_usml_tag_prefix_inline) then
				-- <inline ...>
				-- <inline macro="I";ref="pos_1">
				-- <inline I ref="pos_1">
				--sp := Ks_usml_tag_prefix_terse_inline.count + 1
				sp := asr.index_of_next_non_whitespace (
					low_v, Ks_usml_tag_prefix_inline.count + 1, True)
			elseif low_v.starts_with (Ks_usml_tag_prefix) then
				-- <usml ...>
				-- <usml macro="I";ref="pos_1">
				-- <usml I ref="pos_1">
				-- Must be a named macro
				--sp := Ks_usml_tag_prefix.count + 1
				sp := asr.index_of_next_non_whitespace (
					low_v, Ks_usml_tag_prefix.count + 1, True)
			else
				-- <??...>
				-- <I ref="pos_1">
				-- Must be a named macro
				sp := 2
			end
			spp := v.index_of (' ', sp)
			if spp = 0 then
				ep := etp - 1
			else
				ep := spp - 1
			end
			params := ""
			ts := v.substring (sp, ep)
			if ts.has ('=') then
				params := ts
			elseif ts.is_empty then
				set_parse_error ("Invalid inline tag: " + v)
			elseif attached macros.item (ts) as mtag then
				-- add macro to params, if any
				mp := "macro=%"" + ts + "%""
				if ep < etp - 1 then
					-- Has params
					-- <usml inline foobar param=foo>
					--            sp^    ^ep
					sp := ep + 2
					ts := v.substring (sp, etp - 1)
					params := mp + ";" + ts
				else
					params := mp
				end
			else
				set_parse_error (
					"Invalid inline parameter: " + v)
			end
			if not has_error then
				parse_formats (params)
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_inline: BOOLEAN = True

	items: TWO_WAY_LIST [like Current]

	tag_length: INTEGER
			-- Length to opening tag (Current)
		do
			Result := tag_text.count
		end

	section_start: INTEGER
			-- Line, in original source, at which section containing 
			-- Current began

	tag_text_start: INTEGER
			-- Index, in paragraph text, of start of tag

	tag_text_end: INTEGER
			-- Index, in paragraph text, of end of tag
		do
			if tag_text_start /= 0 and tag_text.count /= 0 then
				Result := tag_text_start + tag_text.count - 1
			end
		end

	tag_pair: detachable USML_INLINE_PAIR

	closing_tag_start: INTEGER
			-- Index, in paragraph text, of start of closing tag
	closing_tag_end: INTEGER
			-- Index, in paragraph text, of end of closing tag
	closing_tag_length: INTEGER

	captive_text_ranges: LINKED_LIST [INTEGER_INTERVAL]

	captive_text_start: INTEGER
			-- Index, in paragraph text, of start of captive text
--		do
--			Result := captive_text_ranges.first.lower
--		end

	captive_text_end: INTEGER
			-- Index, in paragraph text, of end of captive text
--		do
--			Result := captive_text_ranges.first.lower
--		end

	captive_text_length: INTEGER
		do
			if captive_text_start /= 0 then
				Result := 1 + captive_text_end - captive_text_start
			end
		end

--|========================================================================
feature -- Section format (for text until next tag)
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_section_start (v: INTEGER)
		do
			section_start := v
		end

	set_tag_pair (v: USML_INLINE_PAIR)
		do
			tag_pair := v
		end

	add_captive_text_range (sp, ep: INTEGER)
		do
			captive_text_ranges.extend (create {INTEGER_INTERVAL}.make (sp, ep))
		end

	set_captive_text_start (v: INTEGER)
			-- Set the index, in paragraph text, of start of captive text
		do
			captive_text_start := v
		end

	set_captive_text_end (v: INTEGER)
			-- Set the index, in paragraph text, of end of captive text
		do
			captive_text_end := v
		end

	new_set_captive_text_start (v: INTEGER)
			-- Set the index, in paragraph text, of start of captive text
		require
			ranges_empty: captive_text_ranges.is_empty
		do
			--captive_text_start := v
			captive_text_ranges.extend (create {INTEGER_INTERVAL}.make (v, v))
		end

	new_set_captive_text_end (v: INTEGER)
			-- Set the index, in paragraph text, of end of captive text
		require
			single_range: captive_text_ranges.count = 1
		do
			--captive_text_end := v
			captive_text_ranges.first.resize (captive_text_start, v)
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_INLINE_TAG
