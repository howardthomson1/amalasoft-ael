note
	description: "{
A token in a stream containing inline tags, captive text, and
non-captive text, with positions and associated text
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INLINE_TOKEN

inherit
	COMPARABLE
		undefine
			default_create
		end
	USML_CORE
		undefine
			is_equal
		redefine
			default_create
		end

create
	make_with_bounds

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_bounds (v: STRING; sp, ep: INTEGER; tp: INTEGER)
			-- Create Current from stream 'v', with start and end positions 
			-- 'sp' and 'ep', and token type 'tp'
		require
			valid_start: sp > 0
			valid_end: ep >= sp
		do
			default_create
			start_pos := sp
			end_pos := ep
			set_text (v.substring (sp, ep))
			token_type := tp
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create text.make (32)
			create captive_tokens.make
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	start_pos: INTEGER
			-- Index, in raw source, of start of Current
	end_pos: INTEGER
			-- Index, in raw source, of start of Current

	rendered_start: INTEGER
			-- Index, in rendered stream, of start of Current
			-- Valid for captive free-text tokens ONLY

	rendered_end: INTEGER
			-- Index, in rendered stream, of end of Current
			-- Valid for captive and free-text tokens ONLY

	text: STRING

	captive_tokens: TWO_WAY_LIST [like Current]

	captive_text: STRING
		local
			tok: like Current
		do
			create Result.make (32)
			from captive_tokens.start
			until captive_tokens.after
			loop
				tok := captive_tokens.item
				Result.append (tok.text)
				captive_tokens.forth
			end
		end

	captive_start: INTEGER
			-- Start index, within parsed substream, at which captive 
			-- text begins.  Is often the same as the length of the open 
			-- tag (+1), but would not be in the case of an inner tag 
			-- nested immediately after current's open tag
		do
			if not captive_tokens.is_empty then
				Result := captive_tokens.first.rendered_start
			end
		end

	captive_end: INTEGER
			-- Index, within parsed substream, at which captive text ends
		do
			if not captive_tokens.is_empty then
				Result := captive_tokens.last.rendered_end
			end
		end

	token_type: INTEGER

	closing_token: detachable like Current

	--|--------------------------------------------------------------

	is_open: BOOLEAN
		do
			Result := token_type = K_inline_token_open
		end

	is_close: BOOLEAN
		do
			Result := token_type = K_inline_token_close
		end

	is_captive: BOOLEAN
		do
			Result := token_type = K_inline_token_captive
		end

	is_free_text: BOOLEAN
		do
			Result := token_type = K_inline_token_text
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_text (v: STRING)
		do
			text.wipe_out
			text.append (v)
		end

	add_captive_text (v: like Current)
			-- Add captive text token 'v' to captive_tokens
		require
			is_captive: v.is_captive
		do
			captive_tokens.extend (v)
		end

	set_closing_token (v: like Current)
		require
			is_open: is_open
		do
			closing_token := v
		end

	set_rendered_start (v: INTEGER)
		require
			not_tag: is_captive or is_free_text
		do
			rendered_start := v
		end

	set_rendered_end (v: INTEGER)
		require
			not_tag: is_captive or is_free_text
		do
			rendered_end := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

	is_less alias "<" (other: like Current): BOOLEAN
			-- Is current object less than `other'?
		do
			Result := start_pos < other.start_pos
		end

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant
	captive_open_only: (not captive_text.is_empty) implies is_open

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_INLINE_TOKEN
