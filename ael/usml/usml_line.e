note
	description: "{
A marked line of input from a USML text stream
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_LINE

inherit
	USML_CORE
		undefine
			default_create
		redefine
			out
		end

create
	default_create, make_with_text

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_text (v: STRING; lno, cp: INTEGER; rf: BOOLEAN)
		do
			default_create
			src_lineno := lno
			src_position := cp
			is_relevant := rf
			text := v
			-- When reading text from a raw file, and that file was 
			-- created on Windows, lines are terminated with CRLF pair 
			-- and so the %R needs to be pruned so that the line can be 
			-- used elsewhere (especially so in rich text widgets)
			text.prune_all_trailing ('%R')
		end

	default_create
		do
			text := ""
		end

--|========================================================================
feature -- Status
--|========================================================================

	text: STRING
	src_lineno: INTEGER
	src_position: INTEGER
			-- Character position of start of Current, in context
			-- Not dependant on being an included file

	is_relevant: BOOLEAN

	source: detachable STRING
			-- Filename of source (if any)

	context_lineno: INTEGER
			-- 0 unless in an included file,
			-- then lineno in that stream

	out: STRING
		do
			Result := aprintf ("%%04d: %%s%N", << src_lineno, text >>)
		end

	line_number_out: STRING
		do
			if attached source as src then
				Result := aprintf (
					"%%d (in %%s) from %%d", << context_lineno, src, src_lineno >>)
			else
				Result := aprintf ("%%d", << src_lineno >>)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_context (src: detachable STRING; lno: INTEGER)
		do
			source := src
			context_lineno := lno
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_LINE
