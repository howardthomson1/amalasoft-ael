note
	description: "{
An outlined section, within a structured USML document, with associated elements
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_LIST

inherit
	USML_OUTLINED
		redefine
			default_create, is_list, item, set_parent, type_label,
			build_outline_number, lineage, ancestry
		end

create
	make_leveled, make_as_text

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	default_create
			-- Create Current in its default initial state
		do
			create peers.make
			create subs.make
			create nest.make
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_list: BOOLEAN = True
			-- Is Current a list-type component?

	list_level: INTEGER
			-- Levels 201-207 are lists (e.g. "L1")
			-- list_level removes the bias
		do
			Result := level - K_usml_tag_list_bias
		end

	type_label: STRING
		do
			Result := "List Item"
		end

	top_item: detachable like Current
			-- First level 1 instance associated with Current
			-- If Void, then Current is top

	is_top_item: BOOLEAN
		do
			Result := top_item = Current
		end

	peers: TWO_WAY_LIST [like item]
	subs: TWO_WAY_LIST [like item]
	nest: TWO_WAY_LIST [like item]
			-- List, used as a stack, to track items in a list set

	--|--------------------------------------------------------------

	pixels_to_spaces (v: INTEGER): STRING
			-- String of spaces equivalent to number of pixels 'v'
		local
			nsp: INTEGER
		do
--TODO
			if v = 0 then
				Result := ""
			else
--				nsp := v //pixels_per_space
-- Values are resolution and point size dependent
-- Maybe init, from app, using textw.maximum_character_width
-- EV_SCREEN gives horizontal_resolution as pixels per inch
				nsp := (v / 3.0).rounded
				create Result.make_filled (' ', nsp)
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_parent (v: like parent)
		do
			Precursor (v)
		end

	set_top_item (v: detachable like Current)
			-- Set first level 1 instance to 'v'
			-- If 'v' is Void or = Current, then Current is top
		do
			if v = Current then
				-- new list
				nest.wipe_out
				nest.extend (Current)
			end
			top_item := v
		end

	--|--------------------------------------------------------------

	build_outline_number
			-- Build hierachical instance number as string of dot-separated 
			-- decimals representing the instances number of Current's
			-- lineage
			-- Include ONLY numlist ancestors, not headings, nor document
		local
			pl: like lineage
			nv, pt: STRING
			nfi, tv: INTEGER
		do
			outline_number.wipe_out
			nfi := number_format
			pt := pre_text
			if nfi = K_num_format_text then
				outline_number.append (pt)
			else
				pl := lineage
				-- Skip first item, as it's the document itself
				from pl.start
				until pl.after
				loop
					if pl.item.is_list then
						tv := pl.item.instance_number
						nfi := pl.item.number_format
						nv := num_format_out (tv, nfi)
						if attached pt then
							outline_number.append (pt)
						end
						outline_number.append (nv)
						if not pl.islast then
							outline_number.extend ('.')
						end
					end
					pl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	ancestry: TWO_WAY_LIST [USML_LIST]
			-- List ancestry of Current, from top_item to Current's 
			-- parent, inclusive
		do
			create Result.make
			if attached list_parent as psec then
				Result.fill (psec.lineage)
			end
		end

	lineage: TWO_WAY_LIST [USML_LIST]
			-- List lineage of Current, from top_item to Current,
			-- inclusive
		do
			create Result.make
			Result.fill (ancestry)
			Result.extend (Current)
		end

--|========================================================================
feature -- Access
--|========================================================================

	item: USML_LIST
			-- Item at cursor
		do
			Result := items.item
		end

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

	last_peer_instance_number: INTEGER
			-- Instance number of most recently added peer
			-- Might not be same as peers.count, or prev+1

	add_sub (v: like item; iin: INTEGER)
			-- Add a subordinate item to Current
			-- If 'iin' /= 0 then is instance number of 'v'
		require
			is_sub: v.list_level > list_level
		do
--			v.set_instance_number (count+1)
			if iin /= 0 then
				last_instance_number := iin
			else
				last_instance_number := last_instance_number + 1
			end
			v.set_instance_number (last_instance_number)
			v.set_top_item (top_item)
			-- Do NOT add sub to 'items', lest clients believe this is a 
			-- nested section.  List item nesting is managed separately
			subs.extend (v)
			v.set_list_parent (Current)
		ensure
			has_list_parent: v.list_parent = Current
		end

	add_peer (v: like item; iin: INTEGER)
			-- Add a peer item to Current
			-- If 'iin' /= 0 then is instance number of 'v'
		require
			same_level: v.list_level = list_level
		do
			peers.extend (v)
--			v.set_instance_number (instance_number + peers.count)
 			if iin /= 0 then
 				last_peer_instance_number := iin
 			elseif last_peer_instance_number = 0 then
 				last_peer_instance_number := instance_number + 1
			else
 				last_peer_instance_number := last_peer_instance_number + 1
 			end
 			v.set_instance_number (last_peer_instance_number)
			v.set_parent (parent)
			v.set_list_parent (list_parent)
			v.set_top_item (top_item)
		ensure
			has_same_parent: v.list_parent = list_parent
		end


--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_LIST
