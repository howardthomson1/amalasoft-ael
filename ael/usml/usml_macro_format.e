note
	description: "{
A macro format specification for a USML document
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_MACRO_FORMAT

inherit
	USML_FORMAT
		rename
			out as format_out
		redefine
			default_create
		end

create
	make_with_format, make_from_stream, make_as_ah1

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_as_ah1
		do
			make_from_stream (Ks_ah1_tag_text)
		end

	make_with_format (nm: STRING; v: USML_FORMAT; mt: INTEGER)
			-- Create Current from format 'v', with name 'nm',
			-- for macro type 'mt'
		do
			default_create
			set_name (nm)
			set_macro_section_type (mt)
			init_from_other (v, True)
		end

	--|--------------------------------------------------------------

	make_from_stream (v: STRING)
		do
			default_create
			init_from_stream (v)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			name := "UN_NAMED"
			Precursor
		end

	--|--------------------------------------------------------------

	init_from_stream (v: STRING)
		local
			tag: USML_TAG
			dfmt: USML_FORMAT
			mt, st: INTEGER
		do
			create tag.make_multiline (v)
			tag.parse_multiline_tag_text

			if attached tag.macro_name as nm then
				if is_valid_macro_label (nm) then
					set_name (nm)
				else
					-- ERROR invalid name
				end
			else
				-- ERROR, missing name
			end
			if attached tag.description as des then
				set_description (des)
			end
			mt := tag.macro_type
			if is_valid_usml_tag_type (mt) then
				st := mt
			else
				-- default to paragraph
				st := K_usml_tag_p
			end
			dfmt := default_format_for_type (st)
			init_from_other (dfmt, False)
			-- If using dflt format for paragraph, and macro
			-- is NOT typed, would make macro look like it IS typed
			-- as a paragraph so, reset section type in that case
			if tag.macro_type = 0 then
				set_section_type (0)
			end
			blend_from_tag (tag)

			--TODO, need to determine which untyped this might be
			-- perhaps checking for anything that is not char-only, and 
			-- treating char-only as a higher level of qualifcation, 
			-- rather than as a restriction
			set_macro_section_type (tag.macro_type)
		end

--|========================================================================
feature -- Status
--|========================================================================

	macro_section_type: INTEGER

--RFO 	section_type: INTEGER
--RFO 			-- Document section type corresponding to macro type
--RFO 			-- Zero if untyped
--RFO 		do
--RFO 			if is_valid_usml_tag_type (macro_section_type) then
--RFO 				Result := macro_section_type
--RFO 			else
--RFO 				-- zero; untyped
--RFO 			end
--RFO 		end

	is_char_only: BOOLEAN
		do
			Result := macro_section_type = K_usml_tag_char_fmt_only
		end

	is_untyped: BOOLEAN
		do
			Result := macro_section_type <= 0
		end

	gen_as_compact: BOOLEAN

--|========================================================================
feature -- Status setting
--|========================================================================

	set_macro_section_type (v: INTEGER)
		require
			valid_type: is_valid_macro_section_type (v)
		do
			macro_section_type := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	multi_out: STRING
			-- Multi-line form of Current
		local
			dfmt: USML_FORMAT
			fdl: LINKED_LIST [INTEGER]
			st, ft, flen: INTEGER
			istr, fs, sts: STRING
			params: LINKED_LIST [STRING]
			chl: LINKED_LIST [INTEGER]
		do
			create Result.make (1024)
			istr := "    "
			st := section_type
			if st = 0 then
				sts := ""
			else
				sts := section_type_to_label (st).as_upper
			end
			chl := changes
--RFO 			if not description.is_empty then
--RFO 				Result.append (
--RFO 					Ks_usml_tag_terse_comment.as_upper + "----- " +
--RFO 					description + "-----%N")
--RFO 			end
			if gen_as_compact then
				Result.append ("<X M" + sts + ">%N")
				Result.append (
					aprintf ("%%s%%s%%s;%N",
					<< istr, Ks_fmt_tag_name, quoted (name) >>))
				Result.append (
					compact_out (st, False, is_char_only, True, istr))
			else
				Result.append (
					Ks_usml_tag_x_open_prefix.as_upper + "M" + sts + ">%N")
				flen := Result.count
				create params.make
				-- First param is name
				params.extend (aprintf (
					"%%s%%s%%s", << istr, Ks_fmt_tag_name, quoted (name) >>))
				-- Second param is description (does not show in 'differences'
				if not description.is_empty then
					params.extend (aprintf (
						"%%s%%s%%s", <<istr,Ks_fmt_tag_desc,quoted(description)>>))
				end
				-- Macro should include only values that deviate from the 
				-- default for the selected section type
				dfmt := default_format
				fdl := dfmt.differences (Current)
--				fdl := changes
				from fdl.start
				until fdl.after
				loop
					ft := fdl.item
					fs := named_format_out (Current, st, ft, istr)
					fdl.forth
					if not fs.is_empty then
						params.extend (fs)
					end
				end
				from params.start
				until params.after
				loop
					fs := params.item
					Result.append (fs)
					params.forth
					if not params.after then
						Result.append (";%N")
					end
				end
				if Result.item (Result.count) /= '%N' then
					Result.append ("%N")
				end
				Result.append (Ks_usml_tag_x_close.as_upper + "%N")
			end
		end

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	default_format: USML_FORMAT
		local
			st: INTEGER
		do
			st := section_type
			if not is_valid_usml_tag_type (st) then
				-- default to paragraph
				st := K_usml_tag_p
			end
			Result := default_format_for_type (st)
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	Ks_ah1_tag_text: STRING = "{
<USML X MH1>
    name="AH1";
    out="1,,Appendix";
    num_sep=" - "
</USML X>

}"

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_MACRO_FORMAT
