note
	description: "{
A major section, within a structured USML document,
that follows an outline format
Examples include headings and lists;  need not be numbered,
strictly speaking, but typically are marked with instance ID
and lineage (exception being bullet-style lists)
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_OUTLINED

inherit
	USML_CPLX_SECTION
		redefine
			default_create, is_outlined, set_parent, type_label,
			assemble_text
		end

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	default_create
			-- Create Current in its default initial state
		do
			create outline_number.make (0)
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	is_outlined: BOOLEAN = True
			-- Is Current outlined?

	outline_number: STRING
			-- Hierachical instance number as string of dot-separated 
			-- decimals representing the instances number of Current's
			-- lineage

	type_label: STRING
		do
			Result := "Outlined"
		end

	--|--------------------------------------------------------------

	assemble_text
			-- Discover any embedded inline formats and capture their
			-- parameters for subsequent formatting
		local
			xs, atx, onum, pfx: STRING
			ifmt: USML_INLINE_FORMAT
			plen: INTEGER
		do
			-- Before assembly, ensure that any leftovers from previous 
			-- assembly attempts are cleared
			-- TODO don't call it if not needed, or already called!!!
			if attached assembled_text as atxt then
				inline_formats.wipe_out
			else
				onum := outline_number
				pfx := onum + number_separator
				plen := pfx.count
				if private_text.is_empty then
					xs := ""
				elseif not inlines_disabled then
					xs := inlines_converted (private_text, 1, inline_formats)
					-- Adjust captive text positions based on outline 
					-- number and separator
					if has_inline_formats and not inlines_disabled then
						from inline_formats.start
						until inline_formats.after
						loop
							ifmt := inline_formats.item
							ifmt.set_captive_text_start (ifmt.captive_text_start + plen)
							ifmt.set_captive_text_end (ifmt.captive_text_end + plen)
							inline_formats.forth
						end
					end
				else
					xs := private_text
				end
				create atx.make (xs.count + onum.count + 4)
				atx.append (pfx + xs)
				assembled_text := atx
			end
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	build_outline_number
			-- Build hierachical instance number as string of dot-separated 
			-- decimals representing the instances number of Current's
			-- lineage
		local
			pl: like lineage
			nv, pt: STRING
			nfi, tv, nd: INTEGER
		do
			outline_number.wipe_out
			nfi := number_format
			pt := pre_text
			if nfi = K_num_format_text then
				outline_number.append (pt)
			else
				pl := lineage
				-- Skip first item, as it's the document itself
				from pl.start
				until pl.after
				loop
					if not pl.item.is_document then
						nd := nd + 1
						tv := pl.item.instance_number
						nfi := pl.item.number_format
						nv := num_format_out (tv, nfi)
						if attached pt then
							outline_number.append (pt)
							if nd = 1 then
								-- Need space between pre_text and number 
								-- itself UNLESS number type is 'text'
								outline_number.extend (' ')
							end
						end
						outline_number.append (nv)
						if not pl.islast then
							outline_number.extend ('.')
						end
					end
					pl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	set_parent (v: like parent)
		do
			Precursor (v)
			build_outline_number
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	headings_out: STRING
		local
			ti: USML_SECTION
			lvl: INTEGER
		do
			create Result.make (2048)
			from start
			until after
			loop
				ti := item
				if ti.is_heading or ti.is_title then
					lvl := ti.level
					if ti.is_title then
						Result.append (ti.text + "%N")
					elseif attached {USML_OUTLINED} ti as sec then
						Result.append (sec.outline_number + " " + sec.text + "%N")
						if not sec.is_empty then
							Result.append (sec.headings_out)
						end
					end
				end
				forth
			end
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature -- Implementation
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_OUTLINED
