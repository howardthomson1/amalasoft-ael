note
	description: "{
A USML reference anchor or cross-referent to a reference anchor
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_REF

inherit
	USML_CORE
		undefine
			default_create
		end

create
	make_with_values, make_for_int_image
--, make_for_ext_image

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_values (lb, tlb: STRING; spos, epos: INTEGER)
			-- Create Current with label 'lb', target label 'tlb',
			-- start position (in document stream 'spos' thru 'epos')
			-- Lb identifies Current, but is not necessarily the text 
			-- from the document (and most likely is not).
			-- If Current is an embedded image,
			--    target string is empty
			--    label is image name (from image tag)
			--    spos and epos are zero (i.e. not in stream, anymore)
			-- If Current is an anchor ONLY,
			--    target string is empty
			--    len is zero (moot)
			-- If Current is a cross-reference,
			--    target is ID of reference to which Current refers
			--    len is length of text, in document stream, that
			--    constitutes Current
			-- Current can be both an anchor and cross reference
		do
			default_create
			set_label (lb)
			set_target (tlb)
			start_position := spos
			end_position := epos
		end

	--|--------------------------------------------------------------

--RFO 	make_for_ext_image (lb, tlb: STRING)
--RFO 			-- Create Current as a reference to an external image,
--RFO 			-- rather than to a position in the rendered text stream
--RFO 		do
--RFO 			is_ext_image := True
--RFO 			default_create
--RFO 			set_label (lb)
--RFO 			set_target (tlb)
--RFO 		end

	make_for_int_image (lb: STRING)
			-- Create Current as a reference to an internal (embedded)
			-- image, rather than to a position in the stream
		do
			is_int_image := True
			xref_type := K_xref_type_emb_img
			default_create
			set_label (lb)
		end

	--|--------------------------------------------------------------

	default_create
		do
			create label.make (12)
			create target.make (12)
		end

--|========================================================================
feature -- Status
--|========================================================================

	label: STRING

	target: STRING

	start_position: INTEGER
	end_position: INTEGER

	is_ext_image: BOOLEAN
			-- Is Current a reference to an image file?

	is_int_image: BOOLEAN
			-- Is Current a reference to an embedded image?

	xref_type: INTEGER

--|========================================================================
feature -- Status setting
--|========================================================================

	set_label (v: STRING)
		do
			label.wipe_out
			label.append (v)
		ensure
			is_set: label ~ v
		end

	set_target (v: STRING)
		do
			target.wipe_out
			target.append (v)
		ensure
			is_set: target ~ v
		end

	set_xref_type (v: INTEGER)
			-- Set xref type
		do
			xref_type := v
		end

end -- USML_REF
