note
	description: "{
A section within a structured USML document
Sections are potentially "complex" components, being components
that can contain other components
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_SECTION

inherit
	USML_CORE
		redefine
			default_create, out
		end

create
	make_leveled, make_as_text, make_as_comment

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_leveled (doc: like document; tag: USML_TAG)
			-- Create Current with level defined in 'tag'
		do
			--set_document (doc)
			in_subdoc := doc.in_subdoc
			set_level (tag.level)
			make_with_tag (tag)
			-- clobber level 0 (text format)
			create private_format.make_from_other (default_format_by_level (level))
			set_values_from_tag (tag)
		end

	make_as_text (doc: like document; tag: USML_TAG)
			-- Create Current as a text section (paragraphs, no heading)
			-- Level 0
		do
--			set_document (doc)
			in_subdoc := doc.in_subdoc
			make_with_tag (tag)
			set_values_from_tag (tag)
		end

	make_as_comment (doc: like document; tag: USML_TAG)
		do
--			set_document (doc)
			in_subdoc := doc.in_subdoc
			make_with_tag (tag)
			-- comments have no values to set, they are not rendered
		end

	make_with_tag (tag: USML_TAG)
		do
			src_tag := tag
			default_create
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create private_format.make_from_other (default_format_by_level (0))
			create error_messages.make
			create warning_messages.make
			create private_text.make (0)
			create inline_formats.make
			Precursor
		end

--|========================================================================
feature -- Status
--|========================================================================

	out: STRING
			-- Tagged representation of Current, with format and 
			-- associated text, if any
		do
			create Result.make (0)
		end

	--|--------------------------------------------------------------

	type_label: STRING
		do
			Result := "Section"
		end

	lineage: TWO_WAY_LIST [USML_SECTION]
			-- Lineage of Current, from document to Current, inclusive
		do
			create Result.make
			Result.fill (ancestry)
			Result.extend (Current)
		end

	list_context: TWO_WAY_LIST [USML_SECTION]
			-- Ancestry of Current w/r/t list elements (vs doc sections)
		do
			create Result.make
			if attached list_parent as lp then
				Result.fill (lp.list_context)
			end
		end

	section_index: INTEGER
			-- Position, in document, of Current (index)

	document: USML_DOCUMENT
		do
			Result := current_document
		end

	tag_start_position: INTEGER
			-- Character position, in original text (less CRLFs), at 
			-- which Current's tag starts
		do
			if attached src_tag as tag then
				Result := tag.src_position
			end
		end

	tag_end_position: INTEGER
			-- Character position, in original text (less CRLFs), at 
			-- which Current's tag starts
			-- Valid for multi-line tags only
		do
			if attached src_tag as tag then
				Result := tag.src_end_position
			end
		end

	starting_line: INTEGER
			-- Line number, in original text, at which Current started
			-- N.B. this is the start of the tag

	closing_line: INTEGER
			-- Line number, in original text, at which Current ends
			-- N.B. this is the end of the tag
			-- for single-line tags, same as starting_line

	text_starting_line: INTEGER
			-- Line number, in original text, at which the text 
			-- associated with Current started
		do
			Result := starting_line + 1
			if has_trailing_text then
				Result := starting_line
			elseif attached src_tag as st then
				if attached st.trailing_text as tt and then not tt.is_empty then
					Result := starting_line
				elseif st.is_multiline then
					if closing_line /= 0 then
						Result := closing_line + 1
					end
				elseif not st.tag_text.is_empty then
					Result := starting_line + st.tag_text.occurrences ('%N') + 1
				end
			end
		end

	src_position_is_in_tag (cp: INTEGER): BOOLEAN
			-- Is 'cp' within Current's tag?
			-- i.e. is it NOT within the section text?
		local
			p1, p2: INTEGER
		do
			if is_multiline then
				p1 := tag_start_position
				p2 := tag_end_position
			else
				p1 := tag_start_position
				p2 := p1 + tag_length - 1
			end
			if cp >= p1 and cp <= p2 then
				Result := True
			end
		end

	--|--------------------------------------------------------------

	plain_text_tag_start: INTEGER
			-- Position, in a plain text FIELD, at which Current begins
			-- i.e. start of opening tag

	plain_text_tag_end: INTEGER
			-- Position, in a plain text FIELD, at which Current ends
			-- i.e. all of Current, including tag and text

	plain_text_txt_start: INTEGER
			-- Position, in a plain text FIELD, at which Current's text 
			-- (NOT tag) begins

	rendered_start_line: INTEGER
			-- Line, in a rendered text field, at which Current begins

	rendered_start: INTEGER
			-- Position, in a rendered text field, at which Current begins

	rendered_end: INTEGER
			-- Position, in a rendered text field, at which Current ends

	--|--------------------------------------------------------------

	in_subdoc: BOOLEAN
			-- Is Current within a sub-document?

	instance_number: INTEGER
			-- Instance, within context, of Current
			-- e.g. if second section of same level in parent
			-- then value is 2

	--|--------------------------------------------------------------

	level: INTEGER
			-- Level in document hierarchy
			-- Level 1 is top-level heading ("h1") Level 2 is 2nd level
			-- heading, etc.
			-- Level 0 is non-heading, non-list text ("p")
			-- List levels begin at 201 (biased by 200)
			-- Document itself is also level 0, but is known to be the
			-- document

	parent: detachable USML_SECTION

	ancestry: TWO_WAY_LIST [USML_SECTION]
			-- Ancestry of Current, from document to Current's parent, inclusive
		do
			create Result.make
			if attached parent as psec then
				Result.fill (psec.lineage)
			end
		end

	list_parent: detachable like Current

	src_tag: detachable USML_TAG
			-- Tag from which Current's values were sources, if any

	is_multiline: BOOLEAN
			-- Is Current defined by a multi-line tag?
		do
			if attached src_tag as tag then
				Result := tag.is_multiline
			end
		end

	tag_length: INTEGER
			-- Length of (opening) tag defining Current
		local
			pdiff: INTEGER
		do
			if attached src_tag as tag then
				if tag.is_multiline then
					pdiff := tag.src_end_position - tag.src_position
					if pdiff > 0 then
						Result := pdiff
					end
				else
					Result := tag.original_tag_text.count
				end
			end
		end

	raw_text: STRING
			-- Text belonging (directly) to Current
			-- Unadorned, un-numbered, etc.
		do
			Result := private_text
		end

	assembled_text: detachable STRING

	text: STRING
			-- Text belonging (directly) to Current
		do
			if private_text.is_empty then
				if intext.is_empty then
					Result := ""
				else
					Result := intext + "%N"
				end
			elseif attached assembled_text as atxt then
				Result := atxt
			else
				Result := "** INTERNAL ERROR: Text assembly called out of order"
				-- If assemble_text is called at closure, then should not 
				-- be needed here (assemble_text will be non-void)
				assemble_text
				if attached assembled_text as atx then
					Result := atx
				end
			end
		end

	assemble_text
			-- Discover any embedded inline formats and capture their
			-- parameters for subsequent formatting
		local
			xs, atx: STRING
		do
			-- Before assembly, ensure that any leftovers from previous 
			-- assembly attempts are cleared
			-- TODO don't call it if not needed, or already called!!!
			if attached assembled_text as atxt then
				inline_formats.wipe_out
			end
			if inlines_disabled or private_text.is_empty then
				xs := private_text
			else
				xs := inlines_converted (private_text, 1, inline_formats)
			end
			create atx.make (xs.count)
			atx.append (xs)
			assembled_text := atx
		end

	--|--------------------------------------------------------------

	trailing_text: detachable STRING

	has_trailing_text: BOOLEAN
			-- Did Current receive some text from the tag itself
			-- (aka "trailing text")?
		do
			Result := attached trailing_text as txt and then not txt.is_empty
		end

	--|--------------------------------------------------------------

	inline_formats: TWO_WAY_LIST [USML_INLINE_FORMAT]

	inlines_disabled: BOOLEAN
			-- Is the text associated with Current to be taken 
			-- literally, without inline tag interpretation?
			-- Formatting still applies

	has_inline_formats: BOOLEAN
			-- Did the text of Current (prior to processing) have any
			-- inline tags (and therefore _has_ inline formats)?
		do
			Result := not inline_formats.is_empty
		end

	has_xrefs: BOOLEAN
			-- Did the text of Current (prior to processing) have any
			-- inline tags that were cross-references?
		do
			Result := not xrefs.is_empty
		end

	--|--------------------------------------------------------------

	xrefs: TWO_WAY_LIST [USML_INLINE_FORMAT]
		local
			ti: USML_INLINE_FORMAT
		do
			create Result.make
			from inline_formats.start
			until inline_formats.after
			loop
				ti := inline_formats.item
				if not ti.target_label.is_empty then
					Result.extend (ti)
				end
				inline_formats.forth
			end
		end

	anchors: TWO_WAY_LIST [USML_INLINE_FORMAT]
		local
			ti: USML_INLINE_FORMAT
		do
			create Result.make
			from inline_formats.start
			until inline_formats.after
			loop
				ti := inline_formats.item
				if ti.target_label.is_empty and not ti.reference_label.is_empty then
					Result.extend (ti)
				end
				inline_formats.forth
			end
		end

	--|--------------------------------------------------------------

	xref_by_position (v: INTEGER): detachable USML_INLINE_FORMAT
			-- Inline format, if any, at position 'v' in text field
		local
			sp, ep: INTEGER
			ti: USML_INLINE_FORMAT
			xl: like xrefs
		do
			xl := xrefs
			from xl.start
			until attached Result or xl.after
			loop
				ti := xl.item
--				sp := ti.captive_text_start
--				ep := ti.captive_text_end
				sp := ti.captive_text_rendered_start
				ep := ti.captive_text_rendered_end
				if v >= sp and v <= ep then
					Result := ti
				else
					xl.forth
				end
			end
		end

	--|--------------------------------------------------------------

	--RFO anchor_by_position (v: INTEGER): detachable USML_INLINE_FORMAT
	--RFO 		-- Inline format, if any, at position 'v' in text field
	--RFO 	local
	--RFO 		sp, ep: INTEGER
	--RFO 		ti: USML_INLINE_FORMAT
	--RFO 		al: like anchors
	--RFO 	do
	--RFO 		al := anchors
	--RFO 		from al.start
	--RFO 		until attached Result or al.after
	--RFO 		loop
	--RFO 			ti := al.item
	--RFO 			sp := ti.captive_text_start
	--RFO 			ep := ti.captive_text_end
	--RFO 			if v >= sp and v <= ep then
	--RFO 				Result := ti
	--RFO 			else
	--RFO 				al.forth
	--RFO 			end
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	default_format: USML_FORMAT
			-- Default format for instances of Current
		do
			Result := default_format_by_level (level)
		end

	assembled_format: detachable USML_FORMAT

	format: USML_FORMAT
			-- Format of Current in its present state
		do
			if not attached assembled_format then
				assemble_format
			end
			if attached assembled_format as af then
				Result := af
			else
				create Result.make_from_other (default_format_by_level (level))
			end
		end

	font_family: STRING
		do
			if private_format.font_family_has_changed then
				if attached private_format.font_family as ff then
					Result := ff
				end
			end
			if not attached Result then
				Result := dflt_font_family (level)
			end
		end

	point_size: INTEGER
		do
			if private_format.point_size_has_changed then
				Result := private_format.point_size
			end
			if Result = 0 then
				Result := dflt_point_size (level)
			end
		end

	is_bold: BOOLEAN
		do
			if private_format.is_bold_has_changed then
				Result := private_format.is_bold
			else
				Result := dflt_is_bold (level)
			end
		end

	is_italic: BOOLEAN
		do
			if private_format.is_italic_has_changed then
				Result := private_format.is_italic
			else
				Result := dflt_is_italic (level)
			end
		end

	is_underlined: BOOLEAN
		do
			if private_format.is_underlined_has_changed then
				Result := private_format.is_underlined
			else
				Result := dflt_is_underlined (level)
			end
		end

	is_strike_through: BOOLEAN
		do
			if private_format.is_strike_through_has_changed then
				Result := private_format.is_strike_through
			else
				Result := dflt_is_strike_through (level)
			end
		end

	case_value : INTEGER
		do
			if private_format.case_value_has_changed then
				Result := private_format.case_value
			else
				Result := dflt_case_value (level)
			end
		end

	vertical_offset : INTEGER
		do
			if private_format.vertical_offset_has_changed then
				Result := private_format.vertical_offset
			end
		end

	text_color: STRING
		do
			if private_format.text_color_has_changed then
				Result := private_format.text_color
			end
			if not attached Result then
				Result := dflt_text_color (level)
			end
		end

	bg_color: STRING
		do
			if private_format.bg_color_has_changed then
				Result := private_format.bg_color
			end
			if not attached Result then
				Result := ""
			end
		end

	alignment_value : INTEGER
		do
			if private_format.alignment_has_changed then
				Result := private_format.alignment_value
			else
				Result := default_format_by_level (level).alignment_value
			end
		end

	left_margin : INTEGER
		do
			if private_format.left_margin_has_changed then
				Result := private_format.left_margin
			else
				Result := default_format_by_level (level).left_margin
			end
		end

	right_margin : INTEGER
		do
			if private_format.right_margin_has_changed then
				Result := private_format.right_margin
			else
				Result := default_format_by_level (level).right_margin
			end
		end

	top_margin : INTEGER
		do
			if private_format.top_margin_has_changed then
				Result := private_format.top_margin
			else
				Result := default_format_by_level (level).top_margin
			end
		end

	bottom_margin : INTEGER
		do
			if private_format.bottom_margin_has_changed then
				Result := private_format.bottom_margin
			else
				Result := default_format_by_level (level).bottom_margin
			end
		end

	pre_text: STRING
		do
			if private_format.pre_text_has_changed then
				Result := private_format.pre_text
			else
				Result := default_format_by_level (level).pre_text
			end
			if not attached Result then
				Result := ""
			end
		end

	has_intext: BOOLEAN
		do
			Result := not intext.is_empty
		end

	intext: STRING
		do
			if private_format.intext_has_changed then
				Result := private_format.intext
			else
				Result := default_format_by_level (level).intext
			end
			if not attached Result then
				Result := ""
			end
		end

	number_separator: STRING
		do
			if private_format.number_separator_has_changed then
				Result := private_format.number_separator
			else
				Result := default_format_by_level (level).number_separator
			end
		end

	number_format: INTEGER
		do
			if private_format.number_format_has_changed then
				Result := private_format.number_format
			else
				Result := default_format_by_level (level).number_format
			end
		end

	outline_numbering_enabled: BOOLEAN
		do
			if attached document as doc then
				Result := doc.outline_numbering_enabled
			end
		end

	--|--------------------------------------------------------------

	error_messages: LINKED_LIST [STRING]
	warning_messages: LINKED_LIST [STRING]

	last_error_message: detachable STRING
		do
			if not error_messages.is_empty then
				Result := error_messages.last
			end
		end

	last_warning_message: detachable STRING
		do
			if not warning_messages.is_empty then
				Result := warning_messages.last
			end
		end

	has_error: BOOLEAN
		do
			Result := not error_messages.is_empty
		end

	clear_errors
		do
			error_messages.wipe_out
		end

	has_warning: BOOLEAN
		do
			Result := not warning_messages.is_empty
		end

	clear_warnings
		do
			warning_messages.wipe_out
		end

--|========================================================================
feature -- Quick-check status, reflecting eventual type
--|========================================================================

	is_empty: BOOLEAN
			-- Is items empty?
		do
		end

	has_items: BOOLEAN
			-- Does Current have items?
			-- i.e. is it a complex structure
		do
		end

	is_document: BOOLEAN
			-- Is Current a document?
		do
		end

	is_subdoc: BOOLEAN
			-- Is Current a sub-doc heading?
			-- Sub-doc headings drive document analysis states
		do
		end

	is_indexable: BOOLEAN
			-- Is Current indexable?
			-- i.e. is it suitable for inclusing in a TOC
		do
		end

	is_outlined: BOOLEAN
			-- Is Current outlined?
		do
		end

	is_heading: BOOLEAN
			-- Is Current a heading?
		do
		end

	is_list: BOOLEAN
			-- Is Current a list-type paragraph?
		do
		end

	is_title: BOOLEAN
			-- Is Current a title?
		do
			Result := level = K_usml_tag_title
		end

	is_paragraph: BOOLEAN
			-- Is Current a paragraph/text section?
		do
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_instance_number (v: INTEGER)
			-- Instance, within context, of Current
		require
			valid: v >= 0
		do
			instance_number := v
		end

	set_text (v: STRING)
		do
			-- TODO is this correct behavior?
			if has_trailing_text then
				append_text (" " + v)
			else
				private_text.wipe_out
				append_text (v)
			end
		end

	append_text (v: STRING)
		do
			--if not v.is_empty then
				private_text.append (v)
			--end
--Huh?
			--set_rendered_end (private_text.count)
		end

	append_line (v: STRING)
			-- Append text 'v' to text, adding a newline at end
		do
			append_text (v)
			append_text ("%N")
		end

	clear_trailing_text
		do
			trailing_text := Void
		end

	--|--------------------------------------------------------------

	append_substring (v: STRING; sp, ep: INTEGER)
		require
			valid_start: sp > 0 and sp <= v.count
			valid_end: ep >= sp and ep <= v.count
		do
			if not v.is_empty then
				append_text (v.substring (sp, ep))
			end
		end

	set_starting_line (v: INTEGER)
			-- Record line number where Current begins, in source stream
		do
			starting_line := v
		end

	set_closing_line (v: INTEGER)
			-- Record line number where Current end, in source stream
			-- Relevant for multis only
		do
			closing_line := v
		end

	--|--------------------------------------------------------------

	set_level (v: INTEGER)
			-- Set level in doc hierarchy for Current
		do
			level := v
		end

	set_parent (v: like parent)
		do
			parent := v
		end

	set_list_parent (v: like list_parent)
		do
			list_parent := v
		end

	set_plain_text_tag_start (v: INTEGER)
		require
			valid: v >= 0
		do
			plain_text_tag_start := v
			plain_text_tag_end := v + text.count - 1
		end

	set_plain_text_txt_start (v: INTEGER)
		require
			valid: v >= 0
		do
			plain_text_txt_start := v
		end

	set_rendered_start_line (v: INTEGER)
		require
			valid: v >= 0
		do
			rendered_start_line := v
		end

	set_rendered_start (v: INTEGER)
		require
			valid: v >= 0
		do
			rendered_start := v
			set_rendered_end (v + text.count - 1)
		end

	set_rendered_end (v: INTEGER)
		require
			valid: v >= 0
		do
			rendered_end := v
		end

	set_section_index (v: INTEGER)
		require
			valid: v >= 0
		do
			section_index := v
		end

	--|--------------------------------------------------------------

	set_values_from_tag (v: USML_TAG)
		do
			src_tag := v
			set_level (v.level)
			if attached v.text_font_family as ff then
				if is_valid_font_family (ff) then
					set_font_family (ff)
				end
			end
			if v.has_point_size then
				if v.point_size_is_relative then
					set_relative_point_size (v.text_point_size)
				else
					set_point_size (v.text_point_size)
				end
			end
			if v.has_bold then
				set_is_bold (v.text_is_bold)
			end
			if v.has_italic then
				set_is_italic (v.text_is_italic)
			end
			if v.has_underline then
				set_is_underlined (v.text_is_underlined)
			end
			if v.has_strike_through then
				set_is_strike_through (v.text_is_strike_through)
			end
			if v.has_case_value then
				set_case_value (v.text_case_value)
			end
			if attached v.text_rgb_color as tc then
				if is_valid_rgb_color (tc) then
					set_text_color (tc)
				end
			end
			if attached v.bg_color as bg then
				if is_valid_rgb_color (bg) then
					set_bg_color (bg)
				end
			end
			if v.has_alignment then
				set_alignment_value (v.alignment_value)
			end
			if v.has_left_margin then
				set_left_margin (v.left_margin)
			end
			if v.has_right_margin then
				set_right_margin (v.right_margin)
			end
			if v.has_top_margin then
				set_top_margin (v.top_margin)
			end
			if v.has_bottom_margin then
				set_bottom_margin (v.bottom_margin)
			end
			if attached v.pre_text as pt then
				set_pre_text (pt)
			end
			if attached v.intext as itx then
				set_intext (itx)
			end
			if v.has_number_separator then
				set_number_separator (v.number_separator)
			end
			if v.has_number_format then
				set_number_format (v.number_format)
			end
			if attached v.trailing_text as ttx then
				trailing_text := ttx.twin
				append_text (ttx)
			end
			inlines_disabled := v.inlines_disabled
			assemble_format
 		end

	--|--------------------------------------------------------------

	set_text_font (ff: STRING; ps: INTEGER; b, i, u: BOOLEAN)
			-- Set the preferred font by which to render 'text'
			-- 'ff' is font family ("sans", "times", or "fixed")
			-- 'ps' is point size (positive integer)
			-- 'b' is bold flag (bold if True)
			-- 'i' is italics flag (italics if True)
			-- 'u' is underline flag (underlined if True)
			--
			-- N.B. sets all values to non-default, even if values are
			-- equivalent to default, so any change doc-wide will be
			-- ignored for these value.  Preference is to use separate
			-- set functions for each value
		require
			valid_family: is_valid_font_family (ff)
		do
			set_font_family (ff)
			set_point_size (ps)
			set_is_bold (b)
			set_is_italic (i)
			set_is_underlined (u)
		end

	set_font_family (v: detachable STRING)
			-- Set the font family of Current's text
		require
			valid_if_not_void: attached v as vi implies is_valid_font_family (vi)
		do
			if attached v and then not v.is_empty then
				private_format.set_font_family (v)
			else
				-- Do nothing; an element doesn't get its format reset
				-- after the fact
			end
		ensure
			is_set: attached v as tv and then not tv.is_empty
				implies font_family ~ tv
		end

	set_point_size (v: INTEGER)
			-- Set the point size of Current's text
		do
			private_format.set_point_size (v)
		ensure
			is_set: v /= 0 implies point_size = v
		end

	set_relative_point_size (v: INTEGER)
			-- Set the point size of Current's text, relative to the
			-- default for this level
		do
			private_format.set_point_size (dflt_point_size (level) + v)
		end

	set_is_bold (tf: BOOLEAN)
			-- Set the boldness of Current's text to 'tf'
		do
			private_format.set_is_bold (tf)
		ensure
			is_set: is_bold = tf
		end

	set_is_italic (tf: BOOLEAN)
			-- Set the italicized state of Current's text to 'tf'
		do
			private_format.set_is_italic (tf)
		ensure
			is_set: is_italic = tf
		end

	set_is_underlined (tf: BOOLEAN)
			-- Set the underline state of Current's text to 'tf'
		do
			private_format.set_is_underlined (tf)
		ensure
			is_set: is_underlined = tf
		end

	set_is_strike_through (tf: BOOLEAN)
			-- Set the strike-through state of Current's text to 'tf'
		do
			private_format.set_is_strike_through (tf)
		ensure
			is_set: is_strike_through = tf
		end

	set_case_value (v: INTEGER)
			-- Set the case (e.g. all upper) for Current's text
			-- 0 is as-is (from stream)
			-- <0 is all-lower
			-->0 is all-upper
		do
			private_format.set_case_value (v)
		ensure
			is_set: case_value = v
		end

	set_text_color (v: STRING)
			-- Set the preferred color of Current's text
			-- Color is specified in 24-bit RGB has hex string
		do
			private_format.set_text_color (v)
		ensure
			is_set: text_color ~ v
		end

	set_bg_color (v: STRING)
			-- Set the preferred Background color of Current's text
			-- Color is specified in 24-bit RGB has hex string
		do
			private_format.set_bg_color (v)
		ensure
			is_set: bg_color ~ v
		end

	set_vertical_offset (v: INTEGER)
		do
			private_format.set_vertical_offset (v)
		end

	set_alignment_value (v: INTEGER)
			-- Set the alignment value for Current's text
		do
			private_format.set_alignment_value (v)
		ensure
			is_set: alignment_value = v
		end

	set_left_margin (v: INTEGER)
			-- Set the left margin for Current's text
		do
			private_format.set_left_margin (v)
		ensure
			is_set: left_margin = v
		end

	set_right_margin (v: INTEGER)
			-- Set the right margin for Current's text
		do
			private_format.set_right_margin (v)
		ensure
			is_set: right_margin = v
		end

	set_top_margin (v: INTEGER)
			-- Set the top margin for Current's text
		do
			private_format.set_top_margin (v)
		ensure
			is_set: top_margin = v
		end

	set_bottom_margin (v: INTEGER)
			-- Set the bottom margin for Current's text
		do
			private_format.set_bottom_margin (v)
		ensure
			is_set: bottom_margin = v
		end

	set_pre_text (v: STRING)
			-- Set the pre_text for Current
		do
			private_format.set_pre_text (v)
		ensure
			is_set: attached v as tv implies pre_text ~ tv
		end

	set_intext (v: STRING)
			-- Set the intext value for Current
		do
			private_format.set_intext (v)
		ensure
			is_set: attached v as tv implies intext ~ tv
		end

	set_number_separator (v: STRING)
			-- Set the number_separator for Current
		do
			private_format.set_number_separator (v)
		ensure
			is_set: number_separator ~ v
		end

	set_number_format (v: INTEGER)
			-- Set the number_format for Current
		do
			private_format.set_number_format (v)
		ensure
			is_set: number_format = v
		end

	--|--------------------------------------------------------------

	set_error_message (v: STRING)
		do
			error_messages.extend (v)
		end

	set_warning_message (v: STRING)
		do
			warning_messages.extend (v)
		end

	--|--------------------------------------------------------------

	assemble_format
			-- Assemble format of Current in its present state
--TODO this doesn't work for par params, but set from tag does??????
		local
			ff, dff: STRING
			fmt: USML_FORMAT
		do
			create fmt.make_from_other (default_format_by_level (level))
			assembled_format := fmt
			ff := font_family
			dff := fmt.font_family
			if ff /~ dff then
				if attached ff and then not ff.is_empty then
					fmt.set_font_family (ff)
				end
			end
			if point_size /= fmt.point_size then
				fmt.set_point_size (point_size)
			end
			if is_bold /= fmt.is_bold then
				fmt.set_is_bold (is_bold)
			end
			if is_italic /= fmt.is_italic then
				fmt.set_is_italic (is_italic)
			end
			if is_underlined /= fmt.is_underlined then
				fmt.set_is_underlined (is_underlined)
			end
			if is_strike_through /= fmt.is_strike_through then
				fmt.set_is_strike_through (is_strike_through)
			end
			if case_value /= fmt.case_value then
				fmt.set_case_value (case_value)
			end
			if text_color /~ fmt.text_color then
				if attached text_color as c and then not c.is_empty then
					fmt.set_text_color (c)
				end
			end
			if bg_color /~ fmt.bg_color then
				if attached bg_color as bg and then not bg.is_empty then
					fmt.set_bg_color (bg)
				end
			end
			if left_margin /= fmt.left_margin then
				fmt.set_left_margin (left_margin)
			end
			if right_margin /= fmt.right_margin then
				fmt.set_right_margin (right_margin)
			end
			if top_margin /= fmt.top_margin then
				fmt.set_top_margin (top_margin)
			end
			if bottom_margin /= fmt.bottom_margin then
				fmt.set_bottom_margin (bottom_margin)
			end
			if alignment_value /= fmt.alignment_value then
				fmt.set_alignment_value (alignment_value)
			end
			if pre_text /~ fmt.pre_text then
				if attached pre_text as pt and then not pt.is_empty then
					fmt.set_pre_text (pt)
				end
			end
			if intext /~ fmt.intext then
				if attached intext as itx and then not itx.is_empty then
					fmt.set_intext (itx)
				end
			end
			if number_separator /~ fmt.number_separator then
				if attached number_separator as ns and then not ns.is_empty then
					fmt.set_number_separator (ns)
				end
			end
			if number_format /= fmt.number_format then
				fmt.set_number_format (number_format)
			end
		end

	--|--------------------------------------------------------------

	record_error (fmt: STRING; args: detachable ANY)
			-- Set the error state with the given error message format
			-- values (using printf conventions)
		require
			msg_exists: fmt /= Void and then not fmt.is_empty
		do
			set_error_message (aprintf (fmt, args))
		ensure
			has_error: has_error
		end

	record_warning (fmt: STRING; args: detachable ANY)
			-- Record formatted warning message
			-- (using printf conventions)
		require
			msg_exists: fmt /= Void and then not fmt.is_empty
		do
			set_warning_message (aprintf (fmt, args))
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Cursor movement and state
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

	text_out: STRING
			-- Text-only from Current and its items
		do
			create Result.make (4096)
			Result.append (text)
		end

	--RFO inlines_out: STRING
	--RFO 	local
	--RFO 		ifl: like inline_formats
	--RFO 		ifmt: USML_INLINE_FORMAT
	--RFO 		olen, clen, rt: INTEGER
	--RFO 	do
	--RFO 		--TODO, once nesting is supported, let tags provide the 'out'
	--RFO 		create Result.make (2048)
	--RFO 		ifl := inline_formats
	--RFO 		from ifl.start
	--RFO 		until ifl.after
	--RFO 		loop
	--RFO 			ifmt := ifl.item
	--RFO 			olen := ifmt.tag_text_length
	--RFO 			clen := ifmt.closing_tag_length
	--RFO 			rt := rt + olen + clen
	--RFO 			Result.append (ifmt.positions_and_sizes_out + ", rt=" + rt.out)
	--RFO 			Result.append ("%N")
	--RFO 			ifl.forth
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	--RFO exp_tag_chars_before_position (v: INTEGER): INTEGER
	--RFO 		-- Total number of characters within INLINE tags in Current
	--RFO 		-- before rendered text position 'v'
	--RFO 	local
	--RFO 		sp, ctrs, ctre: INTEGER
	--RFO 		itsp, itlen, ictsp, ictlen: INTEGER
	--RFO 		ifmts: TWO_WAY_LIST [USML_INLINE_FORMAT]
	--RFO 		ifmt: USML_INLINE_FORMAT
	--RFO 		resolved: BOOLEAN
	--RFO 	do
	--RFO 		ifmts := inline_formats
	--RFO 		sp := v
	--RFO 		from ifmts.start
	--RFO 		until ifmts.after or resolved
	--RFO 		loop
	--RFO 			ifmt := ifmts.item
	--RFO 			itsp := ifmt.tag_text_start
	--RFO 			ictsp := ifmt.closing_tag_start
	--RFO 			ictlen := ifmt.closing_tag_length
	--RFO 			ctrs := ifmt.captive_text_rendered_start
	--RFO 			ctre := ifmt.captive_text_rendered_end
	--RFO 			-- Because selection originates in rendered text,
	--RFO 			-- ONLY source text corresponding to the rendered 
	--RFO 			-- text should be highlighted (not the tags)

	--RFO 			-- TODO, allow for selection that spans tags
	--RFO 			-- should be easy enough; just add to selection 
	--RFO 			-- length the length of the tag(s) involved, right?
	--RFO 			if ctrs /= 0 then
	--RFO 				if sp >= ctrs then
	--RFO 					-- Position falls after start of inline tag
	--RFO 					-- Add length of inline open tag to src pos
	--RFO 					-- Shift startpos by previous inline tag length, 
	--RFO 					-- if any
	--RFO 					itlen := ifmt.tag_text_length
	--RFO 					if sp >= ctre then
	--RFO 						-- Position begins after closing tag
	--RFO 						-- so add length of closing tag to offset. 
	--RFO 						itlen := itlen + ictlen
	--RFO 					else
	--RFO 						-- Position is after opening tag, but
	--RFO 						-- before closing tag.  Done
	--RFO 						resolved := True
	--RFO 					end
	--RFO 				end
	--RFO 				Result := Result + itlen
	--RFO 			else
	--RFO 				resolved := True
	--RFO 			end
	--RFO 			ifmts.forth
	--RFO 		end
	--RFO 	end

	tag_chars_before_position (v: INTEGER): INTEGER
			-- Total number of characters within inline tags in Current
			-- before rendered text position 'v'
		local
			ctre, ctrs, itlen: INTEGER
			ifmts: TWO_WAY_LIST [USML_INLINE_FORMAT]
			ifmt: USML_INLINE_FORMAT
			resolved: BOOLEAN
		do
			ifmts := inline_formats
			--       |3|     |3|   |4 |     | 5 ||  7  |  |  7  |    |3|
			--Par w/ <A>_A_, <B>_B_<CC>,_CC_</CC></usml>, </usml>and <D>_D_...
			--Par w/ _A_, _B_,_CC_, and _D_ tags.
			--      8|                   |28
			-- Result should be 3 + 3 + 4 + 5 + 7 + 7 + 3
			from ifmts.start
			until ifmts.after or resolved
			loop
				ifmt := ifmts.item
				ctrs := ifmt.captive_text_rendered_start - rendered_start
				ctre := ifmt.captive_text_rendered_end - rendered_start
				if v > ctrs then
					-- capture opening tag length
					itlen := itlen + ifmt.tag_text_length
				else
					resolved := True
				end
				if v > ctre then
					-- capture closing tag length
					itlen := itlen + ifmt.closing_tag_length
				end
				ifmts.forth
			end
			Result := itlen
		end

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {USML_CONSTANTS} -- Implementation
--|========================================================================

	private_text: STRING
			-- Text belonging (directly) to Current

	private_format: USML_FORMAT

	--|--------------------------------------------------------------

	inlines_converted (v: STRING; spos: INTEGER; fl: like inline_formats): STRING
			-- Text resulting from sweep through 'v', starting at 'spos',
			-- looking for inline tokens (tags, captive text and free 
			-- text).
			-- Tags are removed and the associated inline formats are 
			-- saved (in order or start position) in 'fl'
			--
		local
			csp, cep: INTEGER
			buf: STRING
			itag: USML_INLINE_TAG
			ifmt: USML_INLINE_FORMAT
			toks, otoks: TWO_WAY_LIST [USML_INLINE_TOKEN]
			ttoks: LIST [USML_INLINE_TOKEN]
			tok, top_tok: USML_INLINE_TOKEN
			tstack: LINKED_STACK [USML_INLINE_TOKEN]
			comment_bp: STRING
		do
			Result := ""
			comment_bp := "Break-able comment"
			if spos = 1 and starts_with_open_tag (v, True) and
				not is_inline_tag (v, 1)
			 then
				 -- Is a single or multi-line tag, and therefore CANNOT 
				 -- have any inline tags embedded in it
				 Result := v
			else
				toks := inline_tokens (v)
				if toks.count <= 1 then
					Result := v
				else
					-- Has at least 1 tag in the stream
					create buf.make (v.count)
					Result := buf
					create tstack.make
					create otoks.make
					from toks.start
					until toks.after or has_error
					loop
						tok := toks.item
						if tstack.is_empty then
							top_tok := Void
						else
							top_tok := tstack.item
						end
						inspect tok.token_type
						when K_inline_token_open then
							-- push token
							if tstack.count = K_max_inline_levels then
								set_error_message (
									"Inline tag nesting level maximum exceeded")
							else
								tstack.extend (tok)
								otoks.extend (tok)
							end
						when K_inline_token_close then
							-- Pop stack; associate all text between end of 
							-- open and start of close with current pair, 
							-- whether or not another pair might associate 
							-- some or all of it also
							top_tok := tstack.item
							top_tok.set_closing_token (tok)
							tstack.remove
						when K_inline_token_captive then
							tok.set_rendered_start (buf.count + 1)
							-- associate with open tokens
							ttoks := tstack.linear_representation
							if ttoks.is_empty then
								-- ERROR
								set_error_message (
									"Unexpected inline closing tag: " + tok.text) 
							else
								from ttoks.start
								until ttoks.after
								loop
									ttoks.item.add_captive_text (tok)
									ttoks.forth
								end
							end
							buf.append (tok.text)
							tok.set_rendered_end (buf.count)
						else
							-- add to output as-is
							tok.set_rendered_start (buf.count + 1)
							buf.append (tok.text)
							tok.set_rendered_end (buf.count)
						end
						toks.forth
					end
					comment_bp.wipe_out
					from otoks.start
					until otoks.after or has_error
					loop
						tok := otoks.item
--RFO 						comment_bp.append (aprintf (
--RFO 							"%%d-%%d %%s%N",
--RFO 							<< tok.captive_start, tok.captive_end,
--RFO 							tok.captive_text>>))
						if attached tok.closing_token as ct then
							csp := ct.start_pos
							cep := ct.end_pos
							create itag.make_from_substream (
								v, tok.start_pos, tok.end_pos, csp, cep,
								starting_line)
							itag.set_captive_text_start (tok.captive_start)
							itag.set_captive_text_end (tok.captive_end)
							create ifmt.make_from_tag (itag)
							fl.extend (ifmt)
						else
							-- ERROR
							set_error_message (
								"Inline opening tag without closing tag")
						end

						otoks.forth
					end
				end
			end
		end

--|========================================================================
feature -- Default character formats (document-wide, changeable)
--|========================================================================

	dflt_font_family (lvl: INTEGER): STRING
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			if attached fmt.font_family as ff and then not ff.is_empty then
				Result := ff.twin
			else
				Result := K_dflt_font_family_p.twin
			end
		end

	dflt_point_size (lvl: INTEGER): INTEGER
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.point_size
		end

	dflt_is_bold (lvl: INTEGER): BOOLEAN
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.is_bold
		end

	dflt_is_italic (lvl: INTEGER): BOOLEAN
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.is_italic
		end

	dflt_is_underlined (lvl: INTEGER): BOOLEAN
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.is_underlined
		end

	dflt_is_strike_through (lvl: INTEGER): BOOLEAN
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.is_strike_through
		end

	dflt_case_value (lvl: INTEGER): INTEGER
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			Result := fmt.case_value
		end

	dflt_text_color (lvl: INTEGER): STRING
		local
			fmt: USML_FORMAT
		do
			fmt := default_format_by_level (lvl)
			if attached fmt.text_color as c then
				Result := c
			else
				Result := Ks_dflt_font_color
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	default_format_by_level (lvl: INTEGER): USML_FORMAT
			-- Default format for element level 'lvl'
		do
			inspect lvl
			when K_usml_tag_title then
				Result := default_format_doc_title
			when K_usml_tag_h1 then
				Result := default_format_h1
			when K_usml_tag_h2 then
				Result := default_format_h2
			when K_usml_tag_h3 then
				Result := default_format_h3
			when K_usml_tag_h4 then
				Result := default_format_h4
			when K_usml_tag_h5 then
				Result := default_format_h5

			when K_usml_tag_l1 then
				Result := default_format_l1
			when K_usml_tag_l2 then
				Result := default_format_l2
			when K_usml_tag_l3 then
				Result := default_format_l3
			when K_usml_tag_l4 then
				Result := default_format_l4
			when K_usml_tag_l5 then
				Result := default_format_l5
--RFO 			when K_usml_tag_l6 then
--RFO 				Result := default_format_l5
--RFO 			when K_usml_tag_l7 then
--RFO 				Result := default_format_l5
			when K_usml_tag_v_open then
				Result := default_format_v
			else
				Result := default_format_p
			end
		end

	--|--------------------------------------------------------------

	default_format_p: USML_FORMAT
		do
			Result := document.default_format_p
		end

	default_format_v: USML_FORMAT
		do
			Result := document.default_format_v
		end

	default_format_doc_title: USML_FORMAT
		do
			Result := document.default_format_doc_title
		end

	default_format_h1: USML_FORMAT
		do
			Result := document.default_format_h1
		end

	default_format_h2: USML_FORMAT
		do
			Result := document.default_format_h2
		end

	default_format_h3: USML_FORMAT
		do
			Result := document.default_format_h3
		end

	default_format_h4: USML_FORMAT
		do
			Result := document.default_format_h4
		end

	default_format_h5: USML_FORMAT
		do
			Result := document.default_format_h5
		end

	--|--------------------------------------------------------------

	default_format_l1: USML_FORMAT
		do
			Result := document.default_format_l1
		end

	default_format_l2: USML_FORMAT
		do
			Result := document.default_format_l2
		end

	default_format_l3: USML_FORMAT
		do
			Result := document.default_format_l3
		end

	default_format_l4: USML_FORMAT
		do
			Result := document.default_format_l4
		end

	default_format_l5: USML_FORMAT
		do
			Result := document.default_format_l5
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 11-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_SECTION
