note
	description: "{
A USML (ultra-simple markup language) tag
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_TAG

inherit
	USML_CORE
		undefine
			default_create
		end

create
	make_with_text, make_as_heading, make_as_doc_title, make_multiline,
	make_from_tag_data

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_as_heading (lvl: INTEGER)
			-- Create Current as a default format, level 'lvl' heading tag
		require
			valid_level: is_valid_usml_heading_level (lvl)
		do
			make_with_text (Ks_usml_tag_prefix + "h" + lvl.out + ">")
		end

	make_as_doc_title
			-- Create Current as a default format doc title tag
		do
			make_with_text (Ks_usml_tag_title_heading)
		end

	make_with_text (v: STRING)
			-- Create Current with tag text 'v'
		require
			--valid: is_valid_usml_tag (v)
			is_tag: starts_with_tag (v)
		do
			default_create
			set_tag_text (v)
			parse_tag_text
		end

	--|--------------------------------------------------------------

	make_multiline (v: STRING)
			-- Create Current as a multi-line tag
			-- with opening tag text 'v'
		require
			--valid: v.starts_with (Ks_usml_tag_x_open_prefix)
			valid: starts_with_xopen_tag (v, create {STRING}.make (0))
		local
			len, etp: INTEGER
		do
			is_multiline := True
			default_create
			set_tag_text (v)
			-- Check for trailing text (illegal for multis)
			-- TODO, decide if trailer is ignored or fatal
			len := tag_text.count
			etp := tag_text.index_of ('>', 1)
			if etp /= len then
				-- remove trailing text from tag
				--tag_text.keep_head (etp)
				-- SHOULD be captured; not removed
			end
		end

	--|--------------------------------------------------------------

	make_from_tag_data (v: USML_TAG_DATA)
			-- Create and initialize Current from tag data 'v'
		require
			valid: v.is_usml_tag and not v.has_error
		do
			is_multiline := v.is_multiline
			default_create
			init_from_tag_data (v)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create tag_text.make (0)
			original_tag_text := tag_text
			--text_rgb_color := Ks_dflt_font_color
			Precursor
		end

	--|--------------------------------------------------------------

	init_from_other (mtag: USML_TAG)
			-- Initialize Current from macro tag 'mtag'
		local
			tt: INTEGER
		do
			if attached mtag.format as mfmt then
				create format
				format.init_from_other (mfmt, False)
			end
			if mtag.macro_type /= 0 then
				-- From a type-specific macro (e.g. <usml mp ...)
				tt := mtag.macro_type
				tag_type := tt
				if level = 0 then
					-- Level not yet defined
					-- Seems that headings and lists are missing their levels
					-- Level should be reflected in macro type
					if is_valid_usml_heading_level (tt) then
						level := tt
					elseif is_valid_usml_list_level (tt) then
						level := tt
					else
						-- Assume it's a paragraph (only major type left)
					end
				end
			end
			has_point_size := mtag.has_point_size
			has_bold := mtag.has_bold
			has_italic := mtag.has_italic
			has_case_value := mtag.has_case_value
			has_vertical_offset := mtag.has_vertical_offset
			has_underline := mtag.has_underline
			has_strike_through := mtag.has_strike_through
			has_alignment := mtag.has_alignment
			has_left_margin := mtag.has_left_margin
			has_right_margin := mtag.has_right_margin
			has_top_margin := mtag.has_top_margin
			has_bottom_margin := mtag.has_bottom_margin
			has_pre_text := mtag.has_pre_text
			has_intext := mtag.has_intext
			has_number_separator := mtag.has_number_separator
			has_number_format := mtag.has_number_format
			has_number_start := mtag.has_number_start
			has_outline_level := mtag.has_outline_level
			has_outline_restart := mtag.has_outline_restart
			inlines_disabled := mtag.inlines_disabled
		end

	--|--------------------------------------------------------------

	init_from_tag_data (v: USML_TAG_DATA)
			-- Initialize Current from tag data 'v'
		require
			valid: v.is_usml_tag and not v.has_error
		do
			src_position := v.src_position
			is_macro_invocation := v.is_macro_invocation
			is_macro_definition := v.is_macro_definition
			is_multiline := v.is_multiline
			is_verbatim_open := v.is_verbatim_open
			is_verbatim_close := v.is_verbatim_close

			if attached v.trailing_text as tx then
				trailing_text := tx.twin
			end
			if attached v.tag_text as ts then
				tag_text := ts.twin
				original_tag_text := tag_text
			end
			if attached v.macro as tm then
				init_from_other (tm)
			end
			if v.is_typed_macro then
				-- From a type-specific macro (e.g. <usml mp ...)
				tag_type := K_usml_tag_m
				macro_type := v.section_type
				macro_name := v.macro_name
				description := v.description
			elseif v.is_macro then
				tag_type := v.tag_type
				macro_name := v.macro_name
				description := v.description
			elseif v.is_format then
				tag_type := v.tag_type
				format_level := v.section_type
			else
				tag_type := v.tag_type
			end
			if v.is_outlined then
				level := v.section_type
			end
			if not v.is_comment then
				if attached v.params as params and then not params.is_empty then
					parse_formats (params)
				end
			end
		end

--|========================================================================
feature -- Format parsing/analysis
--|========================================================================

	parse_formats (v: STRING)
			-- Parse format portion 'v', of valid tag
			--
			--   <formats> ::= <format>[";"<formats>]
			--   <format> ::= <char_fmt>|<par_fmt>|<out_fmt>
			--   <char_fmt> ::= char="<ff>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<rc>"
			--    <ff> ::= <font family> | ""
			--    <ps> ::= <point size> | ""
			--    <cv> ::- <case_value> | ""
			--    <b> ::= 0|1|""
			--    <i> ::= 0|1|""
			--    <u> ::= 0|1|""
			--    <rc> ::= <rgb_hex>|""
			--   <par_fmt> ::= par="<av>,<l>,<r>,<t>,<b>"
			--    <av> ::= <alignment_value>|""
			--     <l> ::= <int>|""
			--     <r> ::= <int>|""
			--     <t> ::= <int>|""
			--     <b> ::= <int>|""
			--   <out_fmt> ::= outline="<lvl>,<nt>,<nf>,<pt>"
			--    <lvl> ::= <positive integer>         (outline level)
			--    <nt>  ::= "#"|"A"|"a"|"I"|"i"|"t"|"" ('number' type)
			--    <nf>  ::= <bool_value>|""            (start-over flag)
			--    <pt>  ::= <string>                   (pre-text; may be empty)
		require
			valid: not v.is_empty
		local
			ts, low_ts: STRING
			fl: LIST [STRING]
		do
			fl := v.split (';')
			from fl.start
			until fl.after or has_error
			loop
				ts := fl.item
				low_ts := ts.as_lower
				if low_ts.starts_with (Ks_fmt_tag_char) then
					parse_char_format (ts)
				elseif low_ts.starts_with (Ks_fmt_tag_par) then
					parse_paragraph_format (ts)
				elseif low_ts.starts_with (Ks_fmt_tag_ol) then
					if is_outlined then
						parse_outline_format (ts)
					else
						-- ERROR
						set_parse_error (
							"Outline format applies to heading and list types only")
					end
				elseif low_ts.starts_with (Ks_fmt_tag_macro) then
					parse_macro_format (ts)
				elseif low_ts.starts_with (Ks_fmt_tag_ref) then
					parse_reference (ts)
				elseif low_ts.starts_with (Ks_fmt_tag_xref) then
					parse_cross_reference (ts)
 				else
 					parse_single_format (ts)
				end
				fl.forth
			end
		end

	--|--------------------------------------------------------------

	parse_reference  (v: STRING)
			-- Capture reference ID from 'v'
			-- ref="<ref label>"
		local
			ts: STRING
			len: INTEGER
		do
			len := v.count
			if len <= Ks_fmt_tag_ref.count + 1 then
				-- ERROR
				set_parse_error ("Missing reference label")
			else
				ts := v.substring (Ks_fmt_tag_ref.count + 1, len)
				ts := quotes_stripped (ts)
				if ts.is_empty then
					--set_parse_error ("Empty reference label")
					-- Ignore; ref="" should be OK as benign placeholder
				elseif attached ref_target_by_label (ts) as tref then
					-- Aready used
					-- ERROR to reuse anchor reference
					set_parse_error ("Reference target [" + ts + "] already in use")
				else
					if not attached format then
						create format
					end
--TODO add to table?
					format.set_ref_label (ts)
				end
			end
		end

	--|--------------------------------------------------------------

	parse_cross_reference (v: STRING)
			-- Capture cross-reference ID from 'v' and position and length 
			-- from document stream
			-- xref="<xref label>"
			-- xref="#<xref label>"
			-- xref="<type>,<xref label>"
		local
			ts, lbl: STRING
			len, xt: INTEGER
			xpl: LIST [STRING]
		do
			len := v.count
			if len <= Ks_fmt_tag_xref.count + 1 then
				-- ERROR
				set_parse_error ("Missing reference label")
			else
				ts := v.substring (Ks_fmt_tag_xref.count + 1, len)
				ts := quotes_stripped (ts)
				if ts.is_empty then
					--set_parse_error ("Empty reference label")
					-- Ignore; xref="" should be OK as benign placeholder
				elseif not is_valid_xref (ts) then
					set_parse_error ("Invalid anchor syntax: " + ts)
				else
					xpl := xref_target_to_parts (ts)
					if xpl.count /= 2 then
						set_parse_error ("Empty reference label")
					else
						lbl := xpl.last
						xt := xref_label_to_type (xpl.first)
						if not is_valid_xref_type (xt) then
							set_parse_error (
								"Invalid cross reference type: " + xpl.first)
						elseif xt = K_xref_type_ext_img then
							has_image_reference := True
						end
						if not has_error then
							if attached links.item (lbl) as tref then
								-- Aready used
								-- ERROR to reuse anchor reference
								set_parse_error (
									"Reference link ID [" + lbl + "] already in use")
							end
						end
						if not has_error then
							if not attached format then
								create format
							end
							format.set_xref_target (lbl)
							format.set_xref_type (xt)
						end
					end
--TODO add to table
				end
			end
		end

	--|--------------------------------------------------------------

	parse_font_family (v: STRING)
		require
			is_font_family: v.starts_with (Ks_fmt_tag_font)
		local
			ts: STRING
		do
			ts := string_format_value (v, Ks_fmt_tag_font.count + 1)
			if not ts.is_empty then
				if is_valid_font_family (ts) then
					if not attached format then
						create format
					end
					format.set_font_family (ts)
				else
					set_parse_error ("Invalid font family [" + ts + "]")
				end
			end
		end

	parse_text_color (v: STRING)
		require
			is_text_color: v.starts_with (Ks_fmt_tag_color)
		local
			ts: STRING
		do
			ts := string_format_value (v, Ks_fmt_tag_color.count + 1)
			if not ts.is_empty then
				if is_valid_color_id (ts) then
					if not attached format then
						create format
					end
					if attached color_macro_by_name (ts) as tm then
						if attached tm.text_rgb_color as c then
							format.set_text_color (c)
						else
							-- Should not be possible
						end
					else
						format.set_text_color (ts)
					end
				else
					set_parse_error ("Invalid RGB color value")
				end
			end
		end

	parse_bg_color (v: STRING)
		require
			is_bg_color: v.starts_with (Ks_fmt_tag_bg_color)
		local
			ts: STRING
		do
			ts := string_format_value (v, Ks_fmt_tag_bg_color.count + 1)
			if not ts.is_empty then
				if is_valid_rgb_color (ts) then
					if not attached format then
						create format
					end
					format.set_bg_color (ts)
				else
					set_parse_error ("Invalid RGB bg color value")
				end
			end
		end

	parse_point_size (v: STRING)
		require
			is_pt_size: v.starts_with (Ks_fmt_tag_point_size)
		local
			ic: CELL [INTEGER]
			tv: INTEGER
		do
			create ic.put (0)
			tv := integer_format_value (v, Ks_fmt_tag_point_size.count + 1, ic)
			if not has_error then
				has_point_size := True
				if not attached format then
					create format
				end
				if ic.item = 0 then
					format.set_point_size (tv)
				else
					format.set_relative_point_size (tv)
				end
			end
		end

	parse_case_value (v: STRING)
			-- Case value can be numeric or symbolic
			-- 1, -1, 10, "U", "L", "T"
			-- Empty or zero denotes "as-is"
		require
			is_case: v.starts_with (Ks_fmt_tag_case)
		local
			ts: STRING
			tv, cv: INTEGER
		do
			ts := string_format_value (v, Ks_fmt_tag_case.count + 1)
			if not ts.is_empty then
				if ts.is_integer then
					tv := ts.to_integer
					inspect tv
					when
						K_case_value_upper,
						K_case_value_lower,
						K_case_value_title
					 then
						cv := tv
					when K_case_value_as_is then
						-- leave as-is
					else
						-- ERROR
						set_parse_error ("Invalid case value param: " + v)
					end
				elseif ts.count = 1 then
					inspect ts.item (1).as_lower
					when 'u' then
						cv := K_case_value_upper
					when 'l' then
						cv := K_case_value_lower
					when 't' then
						cv := K_case_value_title
					else
						-- ERROR; invalid char
						set_parse_error ("Invalid case value label: " + v)
					end
				else
					-- ERROR; neither integer nor single char
					set_parse_error ("Invalid case value: " + v)
				end
			end
			if cv /= 0 then
				has_case_value := True
				if not attached format then
					create format
				end
				format.set_case_value (cv)
			end
		end

	parse_left_margin (v: STRING)
		require
			is_left_margin: v.starts_with (Ks_fmt_tag_margin_left)
		local
			ic: CELL [INTEGER]
			tv: INTEGER
		do
			create ic.put (0)
			tv := integer_format_value (v, Ks_fmt_tag_margin_left.count + 1, ic)
			if not has_error then
				has_left_margin := True
				if not attached format then
					create format
				end
				if ic.item = 0 then
					format.set_left_margin (tv)
				else
					-- '+' and '-' not permitted for margins
					set_parse_error ("Invalid margin format [" + v + "]")
				end
			end
		end

	parse_right_margin (v: STRING)
		require
			is_right_margin: v.starts_with (Ks_fmt_tag_margin_right)
		local
			ic: CELL [INTEGER]
			tv: INTEGER
		do
			create ic.put (0)
			tv := integer_format_value (v, Ks_fmt_tag_margin_right.count + 1, ic)
			if not has_error then
				has_right_margin := True
				if not attached format then
					create format
				end
				if ic.item = 0 then
					format.set_right_margin (tv)
				else
					-- '+' and '-' not permitted for margins
					set_parse_error ("Invalid margin format [" + v + "]")
				end
			end
		end

	parse_top_margin (v: STRING)
		require
			is_top_margin: v.starts_with (Ks_fmt_tag_margin_top)
		local
			ic: CELL [INTEGER]
			tv: INTEGER
		do
			create ic.put (0)
			tv := integer_format_value (v, Ks_fmt_tag_margin_top.count + 1, ic)
			if not has_error then
				has_top_margin := True
				if not attached format then
					create format
				end
				if ic.item = 0 then
					format.set_top_margin (tv)
				else
					-- '+' and '-' not permitted for margins
					set_parse_error ("Invalid margin format [" + v + "]")
				end
			end
		end

	parse_bottom_margin (v: STRING)
		require
			is_bottom_margin: v.starts_with (Ks_fmt_tag_margin_bottom)
		local
			ic: CELL [INTEGER]
			tv: INTEGER
		do
			create ic.put (0)
			tv := integer_format_value (v, Ks_fmt_tag_margin_bottom.count + 1, ic)
			if not has_error then
				has_bottom_margin := True
				if not attached format then
					create format
				end
				if ic.item = 0 then
					format.set_bottom_margin (tv)
				else
					-- '+' and '-' not permitted for margins
					set_parse_error ("Invalid margin format [" + v + "]")
				end
			end
		end

	--|--------------------------------------------------------------

	parse_single_format (v: STRING)
		local
			ts, low_v: STRING
			tv: INTEGER
			ic: CELL [INTEGER]
		do
			low_v := v.as_lower
			if low_v.starts_with (Ks_fmt_tag_font) then
				-- Font family
				parse_font_family (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_color) then
				-- Text color
				parse_text_color (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_bg_color) then
				-- Text background color
				parse_bg_color (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_point_size) then
				-- Point size
				parse_point_size (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_case) then
				parse_case_value (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_margin_left) then
				-- Left margin, in pixels
				parse_left_margin (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_margin_right) then
				-- Right margin, in pixels
				parse_right_margin (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_margin_top) then
				-- Top margin, in pixels
				parse_top_margin (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_margin_bottom) then
				-- Bottom margin, in pixels
				parse_bottom_margin (low_v)
			elseif low_v.starts_with (Ks_fmt_tag_bold) then
				-- Bold
				tv := boolean_format_value (v, Ks_fmt_tag_bold.count + 1)
				inspect tv
				when 1, 0 then
					has_bold := True
					if not attached format then
						create format
					end
					format.set_is_bold (tv = 1)
				else
				end
			elseif low_v.starts_with (Ks_fmt_tag_italic) then
				-- Italic
				tv := boolean_format_value (v, Ks_fmt_tag_italic.count + 1)
				inspect tv
				when 1, 0 then
					has_italic := True
					if not attached format then
						create format
					end
					format.set_is_italic (tv = 1)
				else
				end
			elseif low_v.starts_with (Ks_fmt_tag_underlined) then
				-- Underlined
				tv := boolean_format_value (v, Ks_fmt_tag_underlined.count + 1)
				inspect tv
				when 1, 0 then
					has_underline := True
					if not attached format then
						create format
					end
					format.set_is_underlined (tv = 1)
				else
				end
			elseif low_v.starts_with (Ks_fmt_tag_strike_through) then
				-- Strike-through
				tv := boolean_format_value (v, Ks_fmt_tag_strike_through.count + 1)
				inspect tv
				when 1, 0 then
					has_strike_through := True
					if not attached format then
						create format
					end
					format.set_is_strike_through (tv = 1)
				else
				end
			elseif low_v.starts_with (Ks_fmt_tag_align) then
				-- Paragraph alignment
				-- Alignment can be specified with a number, a letter
				-- or a whole word
				ts := string_format_value (v, Ks_fmt_tag_align.count + 1)
				if is_valid_alignment_value (ts) then
					has_alignment := True
					tv := alignment_value_from_string (ts)
				else
					set_parse_error ("Invalid alignment value [" + ts + "]")
				end
				if has_alignment and not has_error then
					if not attached format then
						create format
					end
					format.set_alignment_value (tv)
				end
			elseif low_v.starts_with (Ks_fmt_tag_offset) then
				-- Vertical offset, +/-
				create ic.put (0)
				tv := integer_format_value (v, Ks_fmt_tag_offset.count + 1, ic)
				if not has_error then
					has_vertical_offset := True
					if not attached format then
						create format
					end
					if ic.item < 0 then
						tv := -tv
					end
					format.set_vertical_offset (tv)
				end
			elseif low_v.starts_with (Ks_fmt_tag_pre_text) then
				-- Pre-text
				ts := string_format_value (v, Ks_fmt_tag_pre_text.count + 1)
				if not ts.is_empty then
					has_pre_text := True
					if not attached format then
						create format
					end
					format.set_pre_text (ts)
				end
			elseif low_v.starts_with (Ks_fmt_tag_num_sep) then
				-- Number separator (before item text)
				ts := string_format_value (v, Ks_fmt_tag_num_sep.count + 1)
				if not ts.is_empty then
					has_number_separator := True
					if not attached format then
						create format
					end
					format.set_number_separator (ts)
				end
			elseif low_v.starts_with (Ks_fmt_tag_num_fmt) then
				-- Number format
				ts := string_format_value (v, Ks_fmt_tag_num_fmt.count + 1)
				if not ts.is_empty then
					has_number_format := True
					if not attached format then
						create format
					end
					format.set_number_format (num_format_tag_to_index (ts))
				end
			elseif low_v.starts_with (Ks_fmt_tag_num_start) then
				-- Numbering instance value
				create ic.put (0)
				tv := integer_format_value (v, Ks_fmt_tag_num_start.count + 1, ic)
				if not has_error then
					has_number_start := True
					if not attached format then
						create format
					end
					format.set_number_start (tv)
				end
			elseif low_v.starts_with (Ks_fmt_tag_intext) then
				-- Included text
				ts := string_format_value (v, Ks_fmt_tag_intext.count + 1)
				if not ts.is_empty then
					has_intext := True
					if not attached format then
						create format
					end
					format.set_intext (ts)
				end
			elseif low_v.starts_with (Ks_fmt_tag_name) then
				-- Macro name name="mymacro"
				ts := string_format_value (v, Ks_fmt_tag_name.count + 1)
				if not is_valid_macro_label (ts) then
					set_parse_error (aprintf ("Invalid macro name [%%s]",<< ts >>))
				else
					macro_name := ts
				end
			elseif low_v.starts_with (Ks_fmt_tag_desc) then
				-- Macro description desc="some description"
				description := string_format_value (v, Ks_fmt_tag_desc.count + 1)
			elseif low_v ~ Ks_fmt_tag_no_inlines then
				-- "no_inlines"
				-- Does not have an associated value, nor a prefix
				inlines_disabled := True
				-- Does not impact other format values
			elseif attached macros.item (v) as mtag then
				-- macro name in param list, w/o "macro=" prefix
				macro_name := v
				init_from_other (mtag)
			else
				-- ERROR
				set_parse_error ("Unrecognized format type [" + v + "]")
			end
		end

	--|--------------------------------------------------------------

	boolean_format_value (v: STRING; sp: INTEGER): INTEGER
			-- Value of BOOLEAN format expression 'v'
			-- e.g. bold="1" or bold=true or bold=1 or bold=""
			-- If False, then Result = 0
			-- If True, then Result = 1
			-- If empty, then Result = 2
			-- If Error, then Result = -1 (and error is recorded)
		local
			ep: INTEGER
			ts: STRING
		do
			ep := v.count
			ts := v.substring (sp, ep)
			ts := quotes_stripped (ts)
			if ts.is_empty then
				Result := 2
			elseif ts.count = 1 then
				if ts.item (1) = '1' then
					Result := 1
				elseif ts.item (1) = '0' then
					Result := 0
				else
					-- ERROR
					set_parse_error ("Invalid Boolean value [" + ts + "]")
					Result := -1
				end
			elseif ts.as_lower ~ "true" then
				Result := 1
			elseif ts.as_lower ~ "false" then
				Result := 0
			else
				-- ERROR
				set_parse_error ("Invalid Boolean value [" + ts + "]")
				Result := -1
			end
		end

	--|--------------------------------------------------------------

	string_format_value (v: STRING; sp: INTEGER): STRING
			-- Value of STRING format expression 'v'
			-- e.g. color="ff0000" or color=""
		local
			ts: STRING
		do
			ts := v.substring (sp, v.count)
			ts := quotes_stripped (ts)
			Result := ts
		end

	integer_format_value (v: STRING; sp: INTEGER; ic: CELL[INTEGER]): INTEGER
			-- Value of INTEGER format expression 'v'
			-- e.g. points="-12" or points=12 or points=""
			-- If preceded by '+' then ic.item = 1
			-- If preceded by '-', then ic.item = -1
			-- Else ic.item remains unset
		local
			ts: STRING
		do
			ts := v.substring (sp, v.count)
			ts := quotes_stripped (ts)
			if ts.is_empty then
				-- Do nothing
			else
				if ts.item (1) = '+' then
					ic.put (1)
					ts := ts.tail (ts.count - 1)
				elseif ts.item (1) = '-' then
					ic.put (-1)
					ts := ts.tail (ts.count - 1)
				end
				if ts.is_integer then
					Result := ts.to_integer
				else
					set_parse_error ("Value [" + ts + "] is not an integer")
				end
			end
		end

	--|--------------------------------------------------------------

	parse_char_format (v: STRING)
			-- Parse char format from valid tag
			--
			--   <char_fmt> ::= char="<ff>,<ps>,<cv>,<vs>,<b>,<i>,<u>,<st>,<rc>"
			--    <ff> ::= <font family> | ""
			--    <ps> ::= <point size> | ""
			--    <cv> ::- <case_value> | ""
			--    <vs> ::- <vertical_offset> | ""
			--    <b> ::= 0|1|""
			--    <i> ::= 0|1|""
			--    <u> ::= 0|1|""
			--    <st> ::= 0|1|""
			--    <rc> ::= <rgb_hex>|""
		require
			valid_char_fmt: v.as_lower.starts_with (Ks_fmt_tag_char)
		local
			len, ps, cv, vs, relps: INTEGER
			fs, ts, ff, tc, low_v: STRING
			fsl: LIST [STRING]
			fl: ARRAYED_LIST [STRING]
			bf, itf, uf, stf, is_plus, is_minus: BOOLEAN
			has_bf, has_itf, has_uf, has_cv, has_stf, has_vs: BOOLEAN
		do
			len := v.count
			if len <= Ks_fmt_tag_char.count + 1 then
				-- ERROR
				set_parse_error ("Missing character format")
			else
				low_v := v.as_lower
				ts := low_v.substring (Ks_fmt_tag_char.count + 1, len)
				ts := quotes_stripped (ts)
				fsl := ts.split (',')
				create fl.make (fsl.count)
				fl.fill (fsl)
				if fl.count /= K_usml_char_fields then
					-- ERROR
					set_parse_error ("Invalid field count [" + fl.count.out + "]")
				else
					fs := fl.i_th (K_usml_char_fld_ff)
					if is_valid_font_family (fs) then
						ff := fs
					elseif not fs.is_empty then
						-- ERROR
						set_parse_error ("Invalid font family [" + fs + "]")
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_ps)
					if not fs.is_empty then
						if fs.item (1) = '-' then
							-- Relative less
							is_minus := True
						elseif fs.item (1) = '+' then
							-- Relative greater
							is_plus := True
						end
						if is_plus or is_minus then
							fs := fs.substring (2, fs.count)
						end
						if fs.is_integer then
							ps := fs.to_integer
							if is_plus then
								relps := ps
							elseif is_minus then
								relps := -ps
							end
							has_point_size := True
						else
							-- ERROR
							set_parse_error ("Non-integer point size")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_cv)
					if not fs.is_empty then
						has_cv := True
						has_case_value := True
						if fs.is_integer then
							cv := fs.to_integer
						elseif fs ~ "u" then
							cv := K_case_value_upper
						elseif fs ~ "l" then
							cv := K_case_value_lower
						elseif fs ~ "t" then
							cv := K_case_value_title
						else
							-- ERROR
							has_cv := False
							has_case_value := False
							set_parse_error ("Non-integer case value")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_vs)
					if not fs.is_empty then
						if fs.is_integer then
							vs := fs.to_integer
							has_vs := True
							has_vertical_offset := True
						else
							-- ERROR
							set_parse_error ("Non-integer vertical offset")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_b)
					if not fs.is_empty then
						if is_valid_boolean_tag (fs) then
							has_bold := True
							has_bf := True
							bf := fs.item (1) = '1'
						else
							--ERROR
							set_parse_error ("Invalid bold flag")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_i)
					if not fs.is_empty then
						if is_valid_boolean_tag (fs) then
							has_italic := True
							has_itf := True
							itf := fs.item (1) = '1'
						else
							--ERROR
							set_parse_error ("Invalid italic flag")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_u)
					if not fs.is_empty then
						if is_valid_boolean_tag (fs) then
							has_uf := True
							has_underline := True
							uf := fs.item (1) = '1'
						else
							--ERROR
							set_parse_error ("Invalid underline flag")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_st)
					if not fs.is_empty then
						if is_valid_boolean_tag (fs) then
							has_stf := True
							has_strike_through := True
							stf := fs.item (1) = '1'
						else
							--ERROR
							set_parse_error ("Invalid strike-through flag")
						end
					end
				end
				if not has_error then
					fs := fl.i_th (K_usml_char_fld_tc)
					if not fs.is_empty then
						if is_valid_rgb_color (fs) then
							tc := fs
						elseif is_valid_color_macro (fs) then
							tc := fs
						else
							-- ERROR
							set_parse_error ("Invalid RGB color value")
						end
					end
				end
			end
			if not has_error then
				-- Set only those format values defined in tag
				if not attached format then
					create format
				end
				if attached ff then
					format.set_font_family (ff)
				end
				if ps /= 0 then
					if is_plus or is_minus then
						format.set_relative_point_size (relps)
					else
						format.set_point_size (ps)
					end
				end
				if has_cv then
					format.set_case_value (cv)
				end
				if has_vs then
					format.set_vertical_offset (vs)
				end
				if has_bf then
					format.set_is_bold (bf)
				end
				if has_itf then
					format.set_is_italic (itf)
				end
				if has_uf then
					format.set_is_underlined (uf)
				end
				if has_stf then
					format.set_is_strike_through (stf)
				end
				if attached tc then
					format.set_text_color (tc)
				end
			end
		end

	--|--------------------------------------------------------------

	parse_paragraph_format (v: STRING)
			-- Parse par format from valid tag
			--
			-- <par_fmt> ::= par="<av>,<l>,<r>,<t>,<b>"
			--      <av> ::= <alignment_value>|""
			--       <l> ::= <int>|""
			--       <r> ::= <int>|""
			--       <t> ::= <int>|""
			--       <b> ::= <int>|""
		require
			valid_par_fmt: v.as_lower.starts_with (Ks_fmt_tag_par)
		local
			len, av, lm, rm, tsp, bsp: INTEGER
			ts, low_v: STRING
			fls: LIST [STRING]
			fl: ARRAYED_LIST [STRING]
			has_av, has_lm, has_rm: BOOLEAN
			has_tsp, has_bsp: BOOLEAN
		do
			len := v.count
			if len <= Ks_fmt_tag_par.count + 2 then
				-- ERROR
				set_parse_error ("Missing paragraph format")
			else
				low_v := v.as_lower
				ts := low_v.substring (Ks_fmt_tag_par.count + 1, len)
				ts := quotes_stripped (ts)
				fls := ts.split (',')
				if fls.count /= K_usml_par_fields then
					-- ERROR
					set_parse_error ("Invalid paragraph tag field count")
				else
					create fl.make (fls.count)
					fl.fill (fls)
					ts := fl.i_th (K_usml_par_fld_av)
					if not ts.is_empty then
						if is_valid_alignment_value (ts) then
							has_alignment := True
							has_av := True
							av := alignment_value_from_string (ts)
						else
							-- ERROR
							set_parse_error ("Invalid alignment value")
						end
					end
					if not has_error then
						ts := fl.i_th (K_usml_par_fld_ml)
						if not ts.is_empty then
							if ts.is_integer then
								lm := ts.to_integer
								if lm < 0 then
									set_parse_error ("Negative left margin value")
								else
									has_lm := True
									has_left_margin := True
								end
							else
								--ERROR
								set_parse_error ("Invalid left margin value")
							end
						end
					end
					if not has_error then
						ts := fl.i_th (K_usml_par_fld_mr)
						if not ts.is_empty then
							if ts.is_integer then
								rm := ts.to_integer
								if rm < 0 then
									set_parse_error ("Negative right margin value")
								else
									has_rm := True
									has_right_margin := True
								end
							else
								--ERROR
								set_parse_error ("Invalid right margin value")
							end
						end
					end
					if not has_error then
						ts := fl.i_th (K_usml_par_fld_mt)
						if not ts.is_empty then
							if ts.is_integer then
								tsp := ts.to_integer
								if tsp < 0 then
									set_parse_error ("Negative top spacing value")
								else
									has_tsp := True
									has_top_margin := True
								end
							else
								--ERROR
								set_parse_error ("Invalid top spacing value")
							end
						end
					end
					if not has_error then
						ts := fl.i_th (K_usml_par_fld_mb)
						if not ts.is_empty then
							if ts.is_integer then
								bsp := ts.to_integer
								if bsp < 0 then
									set_parse_error ("Negative bottom spacing value")
								else
									has_bsp := True
									has_bottom_margin := True
								end
							else
								--ERROR
								set_parse_error ("Invalid bottom spacing value")
							end
						end
					end
				end
			end
			if not has_error then
				-- Set only those format values defined in tag
				if not attached format then
					create format
				end
				if has_av then
					format.set_alignment_value (av)
				end
				if has_lm then
					format.set_left_margin (lm)
				end
				if has_rm then
					format.set_right_margin (rm)
				end
				if has_tsp then
					format.set_top_margin (tsp)
				end
				if has_bsp then
					format.set_bottom_margin (bsp)
				end
			end
		end

	--|--------------------------------------------------------------

	parse_outline_format (v: STRING)
			-- Parse outline format from valid tag
			--
			--   <out_fmt> ::= out="<lt>,<nf>,<pt>"
			--    <lt>  ::= "#"|"A"|"a"|"I"|"i"|"t"|"" (label type)
			--    <ns>  ::= <positive integer>|""      (starting instance)
			--    <pt>  ::= <string>                   (pre-text; may be empty)
		require
			valid_par_fmt: v.as_lower.starts_with (Ks_fmt_tag_ol)
		local
			tv, len, ns: INTEGER
			ts, pret, lt: STRING
			fls: LIST [STRING]
			has_lt, has_ns: BOOLEAN
		do
			len := v.count
			if len <= Ks_fmt_tag_ol.count + 2 then
				-- ERROR
				set_parse_error ("Missing outline format")
			else
				ts := v.substring (Ks_fmt_tag_ol.count + 1, len)
				ts := quotes_stripped (ts)
				fls := ts.split (',')
				if fls.count /= K_usml_ol_fields then
					-- ERROR
					set_parse_error ("Invalid outline tag field count")
				else
					ts := fls.i_th (K_usml_ol_fld_lt)
					if not ts.is_empty then
						if not is_valid_outline_label_type (ts) then
							set_parse_error ("Invalid outline label type")
						else
							has_lt := True
							lt := ts
						end
					end
					if not has_error then
						ts := fls.i_th (K_usml_ol_fld_ns)
						-- Numbering instance value
						if ts.is_integer then
							tv := ts.to_integer
							if tv >= 0 then
								has_ns := True
								ns := tv
							else
								--ERROR
								set_parse_error ("Invalid list instance start value")
							end
						elseif not ts.is_empty then
							set_parse_error ("Invalid list instance start field")
						end
					end
					if not has_error then
						ts := fls.i_th (K_usml_ol_fld_pt)
						pret := ts
					end
				end
			end
			if not has_error then
				-- Set only those format values defined in tag
				if not attached format then
					create format
				end
				if has_lt and then attached lt as tnt then
					has_number_format := True
					format.set_number_format (num_format_tag_to_index (tnt))
				end
				if has_ns then
					has_number_start := True
 					format.set_number_start (ns)
				end
				if attached pret and then not pret.is_empty then
					has_pre_text := True
					format.set_pre_text (pret)
				end
			end
		end

	--|--------------------------------------------------------------

	parse_align_format (v: STRING)
			-- Parse align-only format from valid tag
			--
			--   <align_fmt>  ::= align="<alignment>"
			--   <alignment>  ::= <align_word>|<align_char>|<align_val>
			--   <align_word> ::="left"|"right"|"center"|"justified"
			--   <align_char> ::= "l"|"r"|"c"|"j"
			--   <align_val> ::= 0|-1|1|2
		require
			valid_align_fmt: v.as_lower.starts_with (Ks_fmt_tag_align)
		local
			len, av: INTEGER
			ts: STRING
			scf, has_av: BOOLEAN
		do
			len := v.count
			if len <= 7 then
				-- ERROR
				set_parse_error ("Missing alignment format")
			else
				ts := v.substring (7, len)
				ts := quotes_stripped (ts.as_lower)
				if ts.is_empty then
					-- ERROR or just dumb? Ignore for now
				else
					if ts.is_integer then
						av := ts.to_integer
						if is_valid_alignment_value (ts) then
							has_av := True	
						else
							-- ERROR
							set_parse_error ("Invalid alignment value")
						end
					else
						scf := ts.count = 1
						inspect ts.item (1).as_lower
						when 'l' then
							if scf or ts ~ "left" then
								-- 0 denotes left
								av := 0
								has_av := True	
							else
								-- ERROR invalid alignment spec
								set_parse_error ("Invalid alignment type")
							end
						when 'r' then
							if scf or ts ~ "right" then
								-- -1 denotes right
								av := -1
								has_av := True	
							else
								-- ERROR invalid alignment spec
								set_parse_error ("Invalid alignment type")
							end
						when 'c' then
							if scf or ts ~ "center" then
								-- 2 denotes center (it's even)
								av := 2
								has_av := True	
							else
								-- ERROR invalid alignment spec
								set_parse_error ("Invalid alignment type")
							end
						when 'j' then
							if scf or ts ~ "justified" then
								-- 1 denotes justified
								av := 2
								has_av := True	
							else
								-- ERROR invalid alignment spec
								set_parse_error ("Invalid alignment type")
							end
						else
							-- ERROR invalid alignment spec
							set_parse_error ("Invalid alignment type")
						end
					end
				end
			end
			if not has_error then
				if not attached format then
					create format
				end
				if has_av then
					has_alignment := True
					format.set_alignment_value (av)
				end
			end
		end

	--|--------------------------------------------------------------

	parse_macro_format (v: STRING)
			-- Parse macro format from valid tag
			--
			--   <macro_fmt> ::= macro=<macro_name>
			--
			-- If macro exists, then use its formats
		require
			valid_macro_fmt: v.as_lower.starts_with (Ks_fmt_tag_macro)
		local
			len: INTEGER
			ts: STRING
		do
			len := v.count
			if len <= Ks_fmt_tag_macro.count + 1 then
				-- ERROR
				set_parse_error ("Missing macro name")
			else
				ts := v.substring (Ks_fmt_tag_macro.count + 1, len)
				ts := quotes_stripped (ts)
				if attached macros.item (ts) as mtag then
					init_from_other (mtag)
				else
					-- ERROR
				end
			end
		end

--|========================================================================
feature -- Status
--|========================================================================

	src_position: INTEGER
			-- Character position, in source, of start of Current

	src_start_line: INTEGER
			-- Line number, in source, of start of Current
			-- If multiline, then line number of OPEN tag
			-- Else, might not be relevant
			-- Note that closing tag line is recorded by section

	src_end_position: INTEGER
			-- Character position, in source, of end of Current
			-- Valid for multi-line tags only

	--|--------------------------------------------------------------

	is_multiline: BOOLEAN

	is_verbatim_open: BOOLEAN
			-- Is Current a verbatim section open tag?
			-- Verbatim section opens with turning OFF interpretation
	is_verbatim_close: BOOLEAN
			-- Is Current a verbatim section close tag?
			-- Verbatim section opens with turning ON interpretation

	is_inline: BOOLEAN
		do
		end

	is_image: BOOLEAN
		do
		end

	tag_type: INTEGER
			-- Major tag type
			-- K_usml_tag_h1: INTEGER = 1
			-- K_usml_tag_h2: INTEGER = 2
			-- K_usml_tag_h3: INTEGER = 3
			-- K_usml_tag_h4: INTEGER = 4
			-- K_usml_tag_h5: INTEGER = 5
			-- K_usml_tag_n1: INTEGER = 201
			-- K_usml_tag_n2: INTEGER = 202
			-- K_usml_tag_n3: INTEGER = 203
			-- K_usml_tag_n4: INTEGER = 204
			-- K_usml_tag_n5: INTEGER = 205
			-- K_usml_tag_doc: INTEGER = 100
			-- K_usml_tag_title: INTEGER = 99
			-- K_usml_tag_p: INTEGER = 10
			-- K_usml_tag_sp: INTEGER = 11
			-- K_usml_tag_m: INTEGER = 101
			-- K_usml_tag_c: INTEGER = 102
			-- K_usml_tag_f: INTEGER = 103

	macro_type: INTEGER
			-- Type, if any, for which Current, as macro tag, applies

	is_doc: BOOLEAN
		do
			--Result := level = K_usml_tag_doc
			Result := tag_type = K_usml_tag_doc
		end

	is_title: BOOLEAN
		do
			Result := tag_type = K_usml_tag_title
		end

	is_paragraph: BOOLEAN
		do
			Result := tag_type = K_usml_tag_p or
				(tag_type = K_usml_tag_m and macro_type = K_usml_tag_p)
		end

	is_heading: BOOLEAN
		do
			-- Heading level corresponds to tag type
			Result := is_valid_usml_heading_level (tag_type)
			if not Result then
				if is_macro then
					Result := is_valid_usml_heading_level (macro_type)
				elseif is_format then
 					Result := is_valid_usml_heading_level (level)
				end
			end
		end

	is_list: BOOLEAN
		do
			Result := is_valid_usml_list_type (tag_type)
			if not Result then
				if is_macro then
					Result := is_valid_usml_list_type (macro_type)
				elseif is_format then
 					Result := is_valid_usml_list_type (level)
				end
			end
		end

	is_outlined: BOOLEAN
			-- Is Current outlined (or outline-able)?
		do
			Result := is_heading or is_list
		end

	is_sub_start: BOOLEAN
		do
			Result := tag_type = K_usml_tag_h_plus
		end

	is_sub_end: BOOLEAN
		do
			Result := tag_type = K_usml_tag_h_minus
		end

	is_comment: BOOLEAN
		do
			Result := tag_type = K_usml_tag_c
		end

	is_format: BOOLEAN
		do
			Result := tag_type = K_usml_tag_f
		end

	is_macro: BOOLEAN
		do
			Result := tag_type = K_usml_tag_m
		end

	is_macro_invocation: BOOLEAN
	is_macro_definition: BOOLEAN

	inlines_disabled: BOOLEAN
			-- Is the text associated with Current to be taken 
			-- literally, without inline tag interpretation?
			-- Formatting still applies

	macro_name: detachable STRING
			-- Name of Current as macro tag, if any

	description: detachable STRING
			-- Description text for Current as macro tag, if any

	--|--------------------------------------------------------------

	level: INTEGER
			-- Level in element hierarchy
			-- Corresponds to section type for headings and
			-- list items, else zero
			-- Levels 1-7 are headings (e.g. "h1")
			-- Levels 201-207 are numbered ists (e.g. "n1")
			-- Level K_usmal_tag_title is doc title
			-- Level 0 is non-heading ("p", "f", or "c")

	list_level: INTEGER
			-- Levels 201-207 are numbered ists (e.g. "n1")
			-- list_level removes the bias
		do
			if is_list then
				Result := level - K_usml_tag_list_bias
			else
				Result := level
			end
		end

	tag_text: STRING
			-- Text of Current (tag string)

	original_tag_text: STRING
			-- Original text of Current (tag string)
			-- BEFORE unrolling for multi-line
			-- If not multi, and not yet unrolled, is just another 
			-- handle to tag_text;  Attachment changes at start of 
			-- unroll process

	trailing_text: detachable STRING
			-- Text that came on the same line as Current, but after the 
			-- closing tag

	--|--------------------------------------------------------------

	has_error: BOOLEAN
		do
			Result := attached error_message as e and then not e.is_empty
		end

	error_message: detachable STRING

	--|--------------------------------------------------------------

	format: detachable USML_FORMAT
		note
			option: stable attribute
		end

	--|--------------------------------------------------------------

	-- Flags, if true, then indicate that a value has been set within 
	-- Current.  Detachable values (ff and color) are detectable with
	-- an attachment test, scalars are not

	has_font_family: BOOLEAN

	has_point_size: BOOLEAN
	has_bold: BOOLEAN
	has_italic: BOOLEAN
	has_case_value: BOOLEAN
	has_vertical_offset: BOOLEAN
	has_underline: BOOLEAN
	has_strike_through: BOOLEAN
	has_alignment: BOOLEAN
	has_left_margin: BOOLEAN
	has_right_margin: BOOLEAN
	has_top_margin: BOOLEAN
	has_bottom_margin: BOOLEAN
	has_pre_text: BOOLEAN
	has_number_separator: BOOLEAN
	has_number_format: BOOLEAN
	has_number_start: BOOLEAN
	has_outline_restart: BOOLEAN
	has_outline_level: BOOLEAN
	has_intext: BOOLEAN
	has_image_reference: BOOLEAN
	has_pbuf_reference: BOOLEAN

--|========================================================================
feature -- Section format (for text until next tag)
--|========================================================================

	format_level: INTEGER
			-- Component level to which a format definition applies
			-- Valid only for format def tags (K_usml_tag_f)

	text_font_family: detachable STRING
		do
			if attached format then
				Result := format.font_family
			end
		end

	point_size_is_relative: BOOLEAN
		do
			if attached format then
				Result := format.point_size_is_relative
			end
		end

	text_point_size : INTEGER
		do
			if attached format then
				Result := format.point_size
			end
		end

	text_case_value : INTEGER
		do
			if attached format then
				Result := format.case_value
			end
		end

	text_vertical_offset : INTEGER
		do
			if attached format then
				Result := format.vertical_offset
			end
		end

	text_is_bold: BOOLEAN
		do
			if attached format then
				Result := format.is_bold
			end
		end

	text_is_italic: BOOLEAN
		do
			if attached format then
				Result := format.is_italic
			end
		end

	text_is_underlined: BOOLEAN
		do
			if attached format then
				Result := format.is_underlined
			end
		end

	text_is_strike_through: BOOLEAN
		do
			if attached format then
				Result := format.is_strike_through
			end
		end

	text_is_all_upper: BOOLEAN
		do
			Result := text_case_value = K_case_value_upper
		end
	text_is_all_lower: BOOLEAN
		do
			Result := text_case_value = K_case_value_lower
		end
	text_is_title_case: BOOLEAN
		do
			Result := text_case_value = K_case_value_title
		end

	text_rgb_color: detachable STRING
		do
			if attached format then
				Result := format.text_color
			end
		end

	bg_color: detachable STRING
		do
			if attached format then
				Result := format.bg_color
			end
		end

	alignment_value: INTEGER
		do
			if attached format then
				Result := format.alignment_value
			end
		end

	left_margin: INTEGER
		do
			if attached format then
				Result := format.left_margin
			end
		end

	right_margin: INTEGER
		do
			if attached format then
				Result := format.right_margin
			end
		end

	top_margin: INTEGER
		do
			if attached format then
				Result := format.top_margin
			end
		end

	bottom_margin: INTEGER
		do
			if attached format then
				Result := format.bottom_margin
			end
		end

	pre_text: detachable STRING
		do
			if attached format then
				Result := format.pre_text
			end
		end

	intext: detachable STRING
		do
			if attached format then
				Result := format.intext
			end
		end

	number_separator: STRING
		do
			if attached format then
				Result := format.number_separator
			else
				Result := " "
			end
		end

	number_format: INTEGER
		do
			if attached format then
				Result := format.number_format
			end
		end

	number_start: INTEGER
		do
			if attached format then
				Result := format.number_start
			end
		end

--|========================================================================
feature -- External representation
--|========================================================================

--|========================================================================
feature -- Status setting
--|========================================================================

	set_src_position (v: INTEGER)
			-- Set character position, in source, of START of Current
		do
			src_position := v
		end

	set_src_start_line (v: INTEGER)
			-- Set line number, in src, at which Current begins
			-- N.B. this is meaningful for multiline tags because the 
			-- closing tag of a multi records its line number too
		do
			src_start_line := v
		end

	set_src_end_position (v: INTEGER)
			-- Set character position, in source, of END of Current
			-- Valid for MULTI only
		do
			src_end_position := v
		end

	set_tag_text (v: STRING)
		do
			tag_text.wipe_out
			tag_text.append (v)
			original_tag_text := tag_text
		end

	append_tag_text (v: STRING)
		do
			tag_text.append (v)
		end

	--|--------------------------------------------------------------


	parse_multiline_tag_text
			-- Multi-line tag first-lines begin with "<x " and MUST NOT 
			-- have trailing text.  Prefix check has been done already, 
			-- but need to check trailing text now
		require
			valid: is_multiline
		do
			unroll_tag_text
--			if not starts_with_tag (tag_text) then
--				-- ERROR, but checked previously
--			else
				parse_tag_text
--			end
		end

	--|--------------------------------------------------------------

	parse_tag_text
			-- "<usml title>"
			-- "<usml p"[<formats>]">"
			-- "<usml h?"[<formats>]">"
			-- "<usml l?"[<formats>]">"
			-- "<usml f "<formats>">"
			-- "<usml m name="<name>";"<formats>">"
			-- "<usml inline>" ...</usml>
			-- "<p"[<formats>]">"
			-- "<hx"[<formats>]">"
			-- "<lx"[<formats>]">"
			-- "<usml x..
			-- Include tags should have been processed already by the 
			-- proprocessor befor we got to this point
			-- "<include "<path>">"
			-- "<usml include "<path>">"
		require
			is_tag: starts_with_tag (tag_text)
		local
			tt, len, i, lim, stype, otype, lvl: INTEGER
			ts, emsg: STRING
			mtag: USML_TAG
			tc: CHARACTER
			parts: like tag_to_parts
			done, is_include: BOOLEAN
		do
			len := tag_text.count
			create emsg.make (0)
			parts := tag_to_parts (tag_text, emsg)
			-- Parts must include the prefix, at least
			if parts.item (1).is_empty then
				set_parse_error ("Missing tag prefix: " + tag_text)
			end
			-- Walk through parts to find error flags
			lim := parts.count
			from i := 1
			until i > lim or has_error
			loop
				if parts.item (i) ~ "E" then
					-- Error during decomposition
					inspect i
					when 1 then
						-- prefix
						set_parse_error (
							"Invalid tag prefix: " + tag_text)
					when 2 then
						-- tag type
						set_parse_error (
							"Invalid tag type: " + tag_text)
					when 3 then
						-- stype
						set_parse_error (
							"Invalid structure type: " + tag_text)
					when 4 then
						-- level
						set_parse_error (
							"Invalid outline level: " + tag_text)
					else
						-- formats, here to end
						set_parse_error (
							"Invalid format parameter(s): " + tag_text)
					end
				end
				i := i + 1
			end
			if not has_error then
				-- First part is prefix (or error)
				-- Prefix was validated during decomposition, and has at 
				-- least 2 characters
				--
				-- Prefix can be 'standard' "<usml "
				-- or 'terse' "<p", "<c"
				--            "<p ","<hx", "<lx",
				--            "<hx ", "<lx "
				ts := parts.item (1).as_lower
				tc := ts.item (2)
				if ts.count = 2 then
					-- "<p" or "<c" no additional parameters
					if tc = 'p' then
						tt := K_usml_tag_p
						tag_type := tt
					elseif tc = 'c' then
						tt := K_usml_tag_c
						tag_type := tt
					else
						-- ERROR
						set_parse_error (
							"Invalid terse tag type: " + tag_text)
					end
					done := True
				elseif ts ~ Ks_usml_tag_prefix_include then
					-- Should have been caught in preprocessor??????
					is_include := True
					done := True
				end
			end
			if not (has_error or done) then
				-- Part #2 is tag general type, a single-char string
				-- 'c', 'f', 'h', 'l', 'm', 'p'
				ts := parts.item (2).as_lower
				if ts.count /= 1 then
					-- ERROR
				else
					tc := ts.item (1)
					inspect tc
					when 'c' then
						tt := K_usml_tag_c
						tag_type := tt
						done := True
					when 'f' then
						tt := K_usml_tag_f
						tag_type := tt
					when 'h' then
						tt := K_usml_tag_h
						tag_type := tt
						-- part #4 is level
					when 'l' then
						tt := K_usml_tag_l
						tag_type := tt
						-- part #4 is level
					when 'm' then
						-- macro invocation or definition
						-- if invocation, next field is macro name
						-- if definition, next field is 'm'
						-- N.B. tag type is a placeholder, pending further
						-- processing
						-- next part is macro name, if invocation, already 
						-- found during decomposition, and put in part 3
						tt := K_usml_tag_m
						tag_type := tt
					when 'p' then
						tt := K_usml_tag_p
						tag_type := tt
					else
						set_parse_error (
							"Invalid tag type marker: " + tag_text)
					end
				end
			end
			if not (has_error or done) then
				-- Part 3 is structure type
				-- Field is either a macro name (for macro INVOCATION),
				-- or a single-char string 'f', 'h', 'l', 'm','p'
				-- Comments were captured in step 2
				-- If empty, could be an untyped macro DEFINITION
				ts := parts.item (3)
				if tt = K_usml_tag_m and ts.is_empty then
					-- Assume a macro definition
				elseif attached macros.item (ts) as mt then
					tt := 0
					tag_type := 0
					mtag := mt
					init_from_other (mtag)
					-- Macros do not support instance mods?????
					-- If macro has instance mods, they appear in part 5 
				elseif ts.count /= 1 then
					-- ERROR
					set_parse_error (
						"Invalid structure type part: " + tag_text)
				else
					tc := ts.item (1).as_lower
					inspect tc
					when 'f' then
						-- ERROR
						set_parse_error (
							"Invalid structure type marker: " + tag_text)
					when 'h' then
						-- heading type marker in parts does not have 
						-- level; level is in part #4
						stype := K_usml_tag_h
					when 'l' then
						-- list type marker in parts does not have 
						-- level; level is in part #4
						stype := K_usml_tag_l
						if tag_type = K_usml_tag_m then
							-- macro type is type of macro
							macro_type := K_usml_tag_l
						elseif tag_type = K_usml_tag_f then
							-- wait for level to clarify format, leaving tag 
							-- type as-is
						else
							-- list item, not from macro or format
						end
					when 'm' then
						-- Macro definition, un-typed
						-- name will appear as first item in format params
					when 'p' then
						stype := K_usml_tag_p
--TODO
						if tag_type = K_usml_tag_m then
							-- A typed macro
							macro_type := K_usml_tag_p
						elseif tag_type = K_usml_tag_f then
							-- Paragraph format setting
							level := 0
						else
							tag_type := stype
						end
					else
						-- ERROR
						set_parse_error (
							"Unrecognized structure type marker: " + tag_text)
					end
				end
			end
			if not (has_error or done) then
				-- Parts 1-3 processed; Part 4 is outline level
				-- Field is a single-char string, either a digit or 
				-- special character
				ts := parts.item (4)
--TODO
-- tt should be a macro type if a typed macro, or format type if 
-- format
				inspect tt
				when K_usml_tag_l, K_usml_tag_h then
					-- OK so far
					if ts.is_empty then
						-- Outline type must have a level specified
						-- ERROR
						set_parse_error (
							"Outline type missing level part: " + tag_text)
					else
						otype := tt
					end
				when K_usml_tag_m, K_usml_tag_f then
					if stype = K_usml_tag_l or stype = K_usml_tag_h then
						if ts.is_empty then
							-- Outline type must have a level specified
							-- ERROR
							set_parse_error (
								"Missing level part: " + tag_text)
						else
							otype := stype
						end
					elseif not ts.is_empty then
						-- ERROR
						set_parse_error (
						 "Outline level part misapplied: " + tag_text)
					end
				else
					if not ts.is_empty then
						-- ERROR
						set_parse_error (
							"Level applied to non-outline type: " + tag_text)
					end
				end
				if otype /= 0 then
					-- Outlined type and non-empty level part
					tc := ts.item (1).as_lower
					if tc.is_digit then
						if tc = '0' then
							-- ERROR
							set_parse_error (
								"Zero is not a valid level: " + tag_text)
						else
							lvl := ts.to_integer
							if otype = K_usml_tag_l then
								lvl := lvl + K_usml_tag_list_bias
								if not is_valid_usml_list_level (lvl) then
									-- ERROR
									set_parse_error (
										"List level out of range: " + tag_text)
								else
-- TODO
--									tag_type := lvl
									if tt = K_usml_tag_l then
										tag_type := lvl
									elseif tt = K_usml_tag_m then
										macro_type := lvl
									end
									level := lvl
								end
							else
								if not is_valid_usml_heading_level (lvl) then
									-- ERROR
									set_parse_error (
										"Heading level out of range: " + tag_text)
								else
-- TODO
--									tt := K_usml_tag_h1 + lvl - 1
--									tag_type := tt
									if tt = K_usml_tag_h then
										tag_type := lvl
									elseif tt = K_usml_tag_m then
										macro_type := lvl
									end
									level := lvl
								end
							end
						end
					elseif tc = '*' then
						-- TODO wildcard
						set_parse_error (
							"Wildcard level not supported: " + tag_text)
					elseif tt = K_usml_tag_h then
						inspect tc
						when 't' then
							-- Title heading
							tt := K_usml_tag_title
							tag_type := tt
							level := tt
						when '+' then
							-- Is a sub-doc start (push state)
							-- No format is associated with tag
							tt := K_usml_tag_h_plus
							tag_type := tt
						when '-' then
							-- Is a sub-doc end (pop state)
							-- No format is associated with tag
							tt := K_usml_tag_h_minus
							tag_type := tt
						else
							-- ERROR
							set_parse_error (
								"Title applies to headings only: " + tag_text)
						end
					else
						-- ERROR
						set_parse_error (
							"Invalid outline level: " + tag_text)
					end
				end
			end
			if not has_error then
				-- Even tags that are 'done' processing earlier parts
				-- can have trailing parameters (e.g. formats)
				-- Parts 1-4 processed; Part 5 is formats as 
				-- semicolon-separated tv pairs or path (if 'include')
				ts := parts.item (5)
				if not ts.is_empty then
					if ts ~ "E" then
						-- ERROR
						set_parse_error (
							"Invalid format params: " + tag_text)
					elseif is_include then
						-- preprocessor?
					else
						parse_formats (ts)
					end
				end
			end
			if not has_error then
				ts := parts.item (6)
				if not ts.is_empty then
					-- Tag had trailing text (text on same line)
					trailing_text := ts
				end
			end
		end

	--|--------------------------------------------------------------

	unroll_tag_text
			-- Unroll multi-line text into a single line without extra 
			-- white spaces, etc.
		require
			multi: is_multiline
		local
			i, len, sp, xp, bc: INTEGER
			buf, tbuf, low_tt: STRING
			--ssl: LIST [STRING]
			inq, lcsem: BOOLEAN
			c: CHARACTER
		do
			-- Preserve original tag text before unrolling
			original_tag_text := tag_text.twin
			if tag_text.has ('%N') then
				tag_text.replace_substring_all ("%N", "")
			end
			sp := tag_text.index_of ('>', 1)
			if sp = 0 then
				--ERROR
			else
				-- tag_text has opening tag, including its '>'
				-- Lines within the multi-line context have been appended 
				-- to the tag.  The closing tag is NOT appended to the 
				-- tag_text.
				-- Replace opening tag's '>' char with blank
				-- <usml xL1> foo=.. --> <usml xL1 foo=..
				tag_text.put (' ', sp)
				-- If last tag inside line has a semicolon separator, 
				-- replace it with a '>', else, simply add a '>' to the 
				-- end of tag_text
				if tag_text.item (tag_text.count) = ';' then
					tag_text.put ('>', tag_text.count)
				else
					tag_text.extend ('>')
				end
				len := tag_text.count
				-- Build an unrolled version of the tag and its parts, 
				-- making it resemble a legal single-line tag, with the 
				-- 'x' marker
				create buf.make (len)
				-- replace 'x' in prefix
				-- "<usml x" --> "<usml "
				-- "<x" --> "<usml "
				low_tt := tag_text.as_lower
				xp := low_tt.index_of ('x', 1)
				buf.append (Ks_usml_tag_prefix)
				buf.append (tag_text.substring (xp + 1, tag_text.count))
				-- Tag text should NOT include the closing tag
				-- There should be no more '<' chars at this point, other 
				-- than the closing tag (if any)
				sp := buf.index_of ('<', 2)
				if sp /= 0 then
					-- Replace with single-line close char and truncate
					buf.put ('>', sp)
					buf.keep_head (sp)
				end
				-- Now purge any irrelevant white space
				tbuf := buf.twin
				buf.wipe_out
				len := tbuf.count
				from i := 1; inq := False
				until i > len
				loop
					c := tbuf.item (i)
					if c = ' ' then
						if inq then
							buf.extend (c)
						else
							if bc = 0 and not lcsem then
								buf.extend (c)
								bc := 1
							else
								-- Skip dupe
								bc := bc + 1
							end
						end
						lcsem := False
					elseif c = ';' then
						buf.extend (c)
						lcsem := True
					else
						if c = '%"' then
							inq := not inq
						end
						buf.extend (c)
						bc := 0
						lcsem := False
					end
					i := i + 1
				end
				tag_text := buf
			end
		end

	--|--------------------------------------------------------------

	set_level (v: INTEGER)
			-- Set level in doc hierarchy for Current
		do
			level := v
		end

	--|--------------------------------------------------------------

	set_text_font (ff: STRING; ps: INTEGER; b, i, u: BOOLEAN)
			-- Set the preferred font by which to render all text 
			-- following tag, until a new tag is found
			-- 'ff' is font family ("sans", "times", or "fixed")
			-- 'ps' is point size (positive integer)
			-- 'b' is bold flag (bold if True)
			-- 'i' is italics flag (italics if True)
			-- 'u' is underline flag (underlined if True)
		require
			valid_family: is_valid_font_family (ff)
		local
		do
			if not attached format then
				create format
			end
			format.set_font_family (ff)
			format.set_point_size (ps)
			format.set_is_bold (b)
			format.set_is_italic (i)
			format.set_is_underlined (u)
		end

	--|--------------------------------------------------------------

	set_parse_error (v: STRING)
		do
			error_message := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	macro_name_from_tag (v: STRING; spos: INTEGER; pc: CELL [INTEGER]): STRING
			-- Macro name defined in macro tag text 'v'
			-- <usml m name="myname";"<formats>">"
			--         ^     ^
			--      spos     sp
		local
			low_v: STRING
			sp, ep: INTEGER
		do
			low_v := v.as_lower
			Result := ""
			sp :=  low_v.substring_index (Ks_fmt_tag_name, spos)
			if sp /= spos then
				-- ERROR
			else
				sp := spos + Ks_fmt_tag_name.count
				if sp >= v.count then
					-- ERROR
				else
					ep := v.index_of ('%"', sp)
					if ep = 0 then
						-- ERROR
					else
						Result := v.substring (sp, ep - 1)
						pc.put (ep + 1)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	include_contents_of_file (fn: STRING)
		require
			valid: not fn.is_empty
		do
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_TAG
