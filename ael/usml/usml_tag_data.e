note
	description: "{
Data describing a USML tag, or not (i.e. looks like, but isn't)
Includes tag type, section type, macro name, etc. if actually a tag
Includes error if nearly a tag, but has errors
Includes flag for false alarm (not a tag at all)
Includes context info
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_TAG_DATA

inherit
	USML_CORE
		undefine
			default_create
		end

create
	make_from_line, make_from_text

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_from_line (v: USML_LINE)
			-- Create Current from line 'v'
			-- line.text begins with '<' but might not be a valid tag
		require
			tag_like: v.text.count >= 3 and then
				v.text.item (1) = '<' and v.text.index_of ('>', 2) > 2
		do
			default_create
			src_line := v
			init_from_stream (v.text)
		end

	make_from_text (v: STRING)
			-- Create Current from line of text 'v'
		do
			default_create
			init_from_stream (v)
		end

	--|--------------------------------------------------------------

	default_create
			-- Create Current in its default initial state
		do
			create src_line
--			create tag_text.make (0)
			Precursor
		end

	--|--------------------------------------------------------------

	init_from_stream (v: STRING)
			-- Initialize Current from stream 'v' that begins with '<' 
			-- but might not be a tag
			-- Record tag characteristics, errors, never-minds, etc
			--
			-- "<usml title>"
			-- "<usml p"[<formats>]">"
			-- "<usml h?"[<formats>]">"
			-- "<usml l?"[<formats>]">"
			-- "<usml f "<formats>">"
			-- "<usml m name="<name>";"<formats>">"
			-- "<usml inline>" ...</usml>
			-- "<usml x..
			-- "<usml v>
			-- "<p"[<formats>]">"
			-- "<hx"[<formats>]">"
			-- "<lx"[<formats>]">"
			-- "<f.."
			-- "<m.."
			-- "<inline>" ...</usml>
			-- Include tags should have been processed already by the 
			-- proprocessor before we got to this point
			-- "<include "<path>">"
			-- "<usml include "<path>">"
		require
			tag_like: v.count >= 3 and then
				v.item (1) = '<' and v.index_of ('>', 2) > 2
		local
			tlen: INTEGER
			sp, etp, spp: INTEGER
			tag, low_tag, tmark, emsg, pfx: STRING
			is_single: BOOLEAN
		do
			create emsg.make (0)
			-- <foobar>...
			--        ^etp
			etp := next_rb_pos (v, 2)
			if etp /= 0 then
				tag := v.substring (1, etp)
				low_tag := tag.as_lower
				tag_text := tag
				tlen := tag.count
				if etp < v.count then
					-- Has trailing text (or is an inline at pos 1)
					trailing_text := v.substring (etp + 1, v.count)
				end
				if is_verbatim_open_tag (tag) then
					tag_type := K_usml_tag_v_open
					is_single := True
				elseif is_verbatim_close_tag (tag) then
					tag_type := K_usml_tag_v_close
				elseif starts_with_xopen_tag (tag, emsg) then
					 -- Looks like a multi open, but ...
					if not emsg.is_empty then
						-- Now what? Is this an error?
						-- Yes, an error in what appeared to be an xopen tag
						set_parse_error (emsg)
					else
						-- Really is a multi open
						-- Check if its a comment
						if is_xcomment_open_tag (tag, emsg) then
							-- Comments are kept now to help with position 
							-- tracking between rendered and source
							tag_type := K_usml_tag_c
							--set_parse_error (
							--"Multi-line comment found after preprocessing")
						else
							is_multiline := True
							-- Parse what's possible in opening tag
							if tag.item (2).as_lower = 'x' then
								-- Terse form <x ?
								--            1  4
								-- Constant DOES include space separator!!
								is_terse := True
								sp := Ks_usml_tag_x_open_prefix.count + 1
							else
								-- Long form <usml x ?
								--           1       9
								-- Constant includes space separator
								is_long := True
								sp := Ks_usml_tag_x_open_prefix_u.count + 1
							end
							if sp = etp then
								-- ERROR <usml x >
								set_parse_error (
									"Invalid multiline open tag: " + tag)
							else
								spp := tag.index_of (' ', sp)
								if spp /= 0 then
									-- has multiple parts
									-- <usml x ??? ..
									--       sp^  ^spp
									tmark := tag.substring (sp, spp - 1).as_lower
								else
									-- Has type marker only
									tmark := tag.substring (sp, etp - 1).as_lower
								end
								analyze_type_marker (tmark)
								if tag_type = 0 then
									tag_type := section_type
								end
								-- Are params allowed in an xopen tag?
								-- Why would they be or not be?
								-- TODO
								-- Params run afoul of unrolling logic
								-- Once that is updated, then params could 
								-- appear within open tag if desired
								-- Option B, capture here and recraft tag to 
								-- look as though it has none, letting the 
								-- tag object sort things out when building 
								-- with Current
							end
							-- It's really a closing tag, BUT, does it have 
							-- trailing text (a NO-NO)?
							if attached trailing_text then
								set_parse_error (
									"Multiline tag has trailing text")
							end
						end
					end
				elseif starts_with_x_close (tag) then
					-- Multiline close
					is_close := True
					if tag.item (3).as_lower = 'u' then
						is_long := True
					end
					if attached trailing_text then
						set_parse_error (
							"Multiline close has trailing text")
					end
				elseif is_inline_open (v, 1) then
					-- inline in pos 1, treat as non-tag
					-- lose the trailing text (not valid for inlines)
					if next_inline_close_start (v, 2, emsg) = 0 then
						set_parse_error (
							"Inline tag at start of line has no closing tag")
					else
						is_inline := True
						trailing_text := Void
						-- Leave param extraction to post-processor
					end
				else
					-- Not a multiline open, nor close (would have been 
					-- captured when extracting/unrolling multi),
					-- BUT looks a lot like a single-line tag, SO FAR
					is_single := True
				end
				if is_single then
					-- <usml h2 params..>...
					-- ^                ^etp
					-- 1    ^spp
					-- <b2 params..>...
					-- ^           ^etp
					-- 1  ^spp
					-- <p>...
					-- ^ ^etp  (spp = 0)
					spp := tag.index_of (' ', 2)
					if spp = 0 then
						-- No separators, must be label-only and terse
						-- (IF it's a USML tag at all)
						-- <h2>...
						--    ^etp
						-- Can be macro or a terse-form single
						is_terse := True
						analyze_simple_tag (tag)
					else
						-- Has more than one part
						-- Could be long-form OR terse, could have params
						pfx := tag.substring (2, spp - 1)
						if attached macros.item (pfx) as mtag then
							-- terse macro invocation
							-- <b3 ..>
							--    ^spp
							is_terse := True
							tag_type := K_usml_tag_m
							is_macro_invocation := True
							extract_attrs_from_macro (pfx, mtag)
							-- macro invocation might have params
							sp := spp + 1
							if sp < etp then
								params := v.substring (sp, etp - 1)
							end
						elseif pfx.as_lower ~ "usml" then
							-- Long-form tag
							-- <usml ...>
							--      ^spp
							is_long := True
							-- Skip to next part, after separator
							sp := spp + 1
							-- <usml ???...>
							--       ^sp
							spp := tag.index_of (' ', sp)
							if spp = 0 then
								-- <usml ?????>
								--       ^sp  (spp = 0)
								if sp = tag.count then
									-- <usml >
									--       ^sp
									set_parse_error ("Invalid tag: " + tag )
								else
									-- <usml ?????>
									--     sp^
									-- Type marker only, no params
									-- Can't be macro def or fmt
									-- but CAN be macro invocation
									-- either super-terse or macro=
									-- <usml Z>     <usml macro="foo">
									tmark := tag.substring (sp, tag.count - 1)
									if attached macros.item (tmark) as mtag then
										-- long macro invocation
										-- <usml b1>
										tag_type := K_usml_tag_m
										is_macro_invocation := True
										extract_attrs_from_macro (tmark, mtag)
									elseif tmark.starts_with (Ks_fmt_tag_macro) then
										-- <usml macro=???? ...>
										--       ^sp       ^spp
										tag_type := K_usml_tag_m
										is_macro_invocation := True
										sp := tmark.index_of ('=', 1)
										tmark := tmark.substring (sp+1, tmark.count)
										tmark := quotes_stripped (tmark)
										if attached macros.item (tmark) as mtag then
											extract_attrs_from_macro (tmark, mtag)
										else
											set_parse_error (
												"Macro does not exist: " + tmark)
										end
									else
										analyze_type_marker (tmark)
										if tag_type = 0 then
											tag_type := section_type
										end
									end
								end
							else
								-- <usml ????? ...>
								--       ^sp  ^spp
								-- Has params
								tmark := tag.substring (sp, spp - 1)

								if tmark.starts_with (Ks_fmt_tag_macro) then
									-- <usml macro=???? ...>
									--       ^sp       ^spp
									tag_type := K_usml_tag_m
									is_macro_invocation := True
									sp := tmark.index_of ('=', 1)
									tmark := tmark.substring (sp+1, tmark.count)
									tmark := quotes_stripped (tmark)
									if attached macros.item (tmark) as mtag then
										extract_attrs_from_macro (tmark, mtag)
									else
										set_parse_error (
											"Macro by that name does not exist: " + tmark)
									end
								elseif attached macros.item (tmark) as mtag then
									is_macro_invocation := True
									extract_attrs_from_macro (tmark, mtag)
								else
									analyze_type_marker (tmark)
									if not has_error then
										if tag_type = 0 then
											tag_type := section_type
										end
									end
								end
								if not has_error then
									params := v.substring (spp + 1, etp - 1)
								end
							end
						else
							-- Either terse form with params or not a tag
							-- <h2 ..>
							--    ^spp
							is_terse := True
							analyze_type_marker (pfx)
							if not has_error then
								if tag_type = 0 then
									tag_type := section_type
								end
								params := v.substring (spp + 1, etp - 1)
							end
						end
					end
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_simple_tag (v: STRING)
			-- Analyze a tag candidate that has no separators
			-- Must be label-only (aka terse form)
			-- <h2>...
			--    ^etp
			-- Can be macro or a terse-form single
		require
			bounded: v.count >= 3
				and then v.item (1) = '<' and v.item (v.count) = '>'
		local
			lbl, hi_lbl: STRING
		do
			lbl := v.substring (2, v.count - 1)
			if attached macros.item (lbl) as mtag then
				is_macro_invocation := True
				extract_attrs_from_macro (lbl, mtag)
			else
				-- Not a macro, nor a multi (open or close), nor verbatim close
				-- Must be terse-form single, or not a tag at all
				hi_lbl := lbl.as_upper
				if lbl.count = 1 then
					-- Can be par, verbatime, or comment only
					inspect hi_lbl.item (1)
					when 'V' then
						tag_type := K_usml_tag_v_open
					when 'C' then
						tag_type := K_usml_tag_c
					when 'P' then
						tag_type := K_usml_tag_p
						section_type := K_usml_tag_p
					else
						-- Not other single-char types
						set_parse_error (
							"Invalid section type: " + lbl)
					end
				elseif lbl.count = 2 then
					inspect hi_lbl.item (1)
					when 'H', 'L' then
						-- <H2>
						analyze_outlined_mark (lbl)
						if section_type /= 0 then
							tag_type := section_type
						end
					else
						set_parse_error (
							"Unrecognized section type: " + lbl)
					end
				else
					set_parse_error (
						"Unrecognized terse tag: " + lbl)
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_type_marker (v: STRING)
			-- Analyze type marker
		require
			valid: not v.is_empty and not v.has (' ')
			not_macro_name: not macros.has (v)
		local
			c: CHARACTER
		do
			c := v.item (1).as_lower
			inspect v.count
			when 1 then
				inspect c
				when 'v' then
					-- Is is a verbatim open or close
					tag_type := K_usml_tag_v_open
				when 'c' then
					tag_type := K_usml_tag_c
				when 'p' then
					tag_type := K_usml_tag_p
				when 'm' then
					-- macro definition
					tag_type := K_usml_tag_m
					is_macro_definition := True
				else
					-- ERROR if long-form, else not a USML tag
					if is_long then
						set_parse_error ("Invalid 1-char type marker: " + v)
					else
						set_warning ("Invalid tag type marker: " + v)
					end
				end
			when 2 then
				inspect c
				when 'h', 'l' then
					-- H?, L?
					analyze_outlined_mark (v)
				when 'f' then
					-- Must be single-char type
					-- (p is only formattable case)
					if v.item (2).as_lower = 'p' then
						tag_type := K_usml_tag_f
						section_type := K_usml_tag_p
					elseif v.item (2).as_lower = 'v' then
						tag_type := K_usml_tag_f
						section_type := K_usml_tag_v_open
					else
						-- Too few chars
						-- ERROR if long-form, else not a USML tag
						if is_long then
							set_parse_error (
								"Invalid 1-char type in format tag: " + v)
						end
					end
				when 'm' then
					-- m?
					-- Must be single-char type
					-- (p is only formattable case)
					if v.item (2).as_lower = 'p' then
						-- "mp"
						tag_type := K_usml_tag_m
						is_macro_definition := True
						section_type := K_usml_tag_p
					else
						-- ERROR if long-form, else not a USML tag
						if is_long then
							set_parse_error (
								"Invalid 1-char type in macro definition: " + v)
						end
					end
				else
					-- ERROR if long-form, else not a USML tag
					if is_long then
						set_parse_error (
							"Invalid 2-char type marker: " + v)
					end
				end
			when 3 then
				inspect c
				when 'h', 'l' then
					-- ERROR 'h??"
					-- ERROR if long-form, else not a USML tag
					if is_long then
						set_parse_error (
							"Invalid outlined type marker: " + v)
					end
				when 'f' then
					-- f??
					inspect v.item (2).as_lower
					when 'h', 'l' then
						-- FH?, FL?
						analyze_outlined_mark (v.substring (2, v.count))
						tag_type := K_usml_tag_f
					when 'v' then
						section_type := K_usml_tag_v_open
					else
						-- ERROR if long-form, else not a USML tag
						if is_long then
							set_parse_error (
								"Invalid format type marker: " + v)
						end
					end
				when 'm' then
					-- Typed macro definition
					inspect v.item (2).as_lower
					when 'h', 'l' then
						-- MH?, ML?
						analyze_outlined_mark (v.substring (2, v.count))
						tag_type := K_usml_tag_m
						is_macro_definition := True
					else
						-- ERROR if long-form, else not a USML tag
						if is_long then
							set_parse_error (
								"Invalid macro type marker: " + v)
						end
					end
				else
					-- ERROR if long-form, else not a USML tag
					if is_long then
						set_parse_error (
							"Invalid type marker: " + v)
					end
				end
			else
				if v.as_lower ~ "macro" then
					-- Untyped macro definition; name is in params
					tag_type := K_usml_tag_m
					is_macro_definition := True
				else
					-- ERROR if long-form, else not a USML tag
					if is_long then
						set_parse_error (
							"Invalid type marker: " + v)
					end
				end
			end
		end

	--|--------------------------------------------------------------

	analyze_outlined_mark (v: STRING)
			-- Analyze outlined type marker
		require
			is_outline_type: is_valid_outlined_type_tag (v)
		local
			tc, c: CHARACTER
		do
			tc := v.item (1).as_lower
			inspect tc
			when 'h', 'l' then
				c := v.item (2).as_lower
				if c.is_digit then
					outline_level := c.out.to_integer
					if outline_level = 0 then
						set_parse_error (
							"Zero is not a valid outline level: " + v)
					else
						if tc = 'h' then
							section_type := outline_level
						else
							section_type := outline_level + K_usml_tag_list_bias
						end
					end
				elseif c = '*' then
					-- Applicable to format tags only
					-- Sets format params to all elements of type
					if tc = 'l' then
						-- Specs all list levels
						section_type := K_usml_tag_l
					else
						-- Specs all heading levels
						-- except title (by current convention)
						section_type := K_usml_tag_h
					end
				elseif tc = 'h' then
					inspect c
					when 't' then
						section_type := K_usml_tag_title
					when '+' then
						tag_type := K_usml_tag_h_plus
					when '-' then
						tag_type := K_usml_tag_h_minus
					else
						set_parse_error (
							"Invalid heading outline level: " + v)
					end
				else
					set_parse_error (
						"Invalid list outline level: " + v)
				end
			else
				-- precondition ensures this doesn't happen
			end
		end

	--|--------------------------------------------------------------

	extract_attrs_from_macro (lbl: STRING; mtag: USML_TAG)
			-- Extract attrs from known macro 'mtag'
			-- Must be a macro invocation, not a macro definition
			-- Format details should be extracted later, during tag 
			-- creation, using init_from_other with 'macro' as arg
		require
			is_invocation: is_macro_invocation
		local
			tt: INTEGER
		do
			macro := mtag
			macro_name := lbl.twin
			tag_type := K_usml_tag_m
			if mtag.macro_type /= 0 then
				-- From a type-specific macro (e.g. <usml mp ...)
				tt := mtag.macro_type
				section_type := tt
				-- Replace macro tag type with type it represents
				tag_type := tt
				if is_valid_usml_heading_level (tt) then
					outline_level := tt
				elseif is_valid_usml_list_level (tt) then
					outline_level := tt - K_usml_tag_list_bias
				else
					-- Assume it's a paragraph (only major type left)
					outline_level := 0
				end
			else
				-- Attempt to invoke a format-only macro
				-- ERROR?????
				-- Seems ok to do; e.g. an inline at line start
				set_parse_error (
					"Misapplied untyped macro: " + lbl)
			end
		end

--|========================================================================
feature -- Format parsing/analysis
--|========================================================================

--|========================================================================
feature -- Status
--|========================================================================

	src_line: USML_LINE
			-- Line from which Current was constructed

	src_lineno: INTEGER
		do
			Result := src_line.src_lineno
		end

	src_position: INTEGER
		do
			Result := src_line.src_position
		end

	--|--------------------------------------------------------------

	is_usml_tag: BOOLEAN
		do
			Result := is_open_tag or is_close or is_inline
		end

	is_open_tag: BOOLEAN
		do
			Result := tag_type /= 0
		end

	is_multiline: BOOLEAN
	is_inline: BOOLEAN
			-- Does Current represent an inline tag?
			-- If so, tag_text is open tag and trailing text begins with 
			-- captive text, followed by closing tag and after-text

	is_close: BOOLEAN
	is_terse: BOOLEAN
	is_long: BOOLEAN
			-- Is Current suspected of being a long-form tag?
			-- True if it starts with "<usml "

	tag_type: INTEGER
			-- Type of tag:
			--   structure/section (e.g. h1, l2, p)
			--   macro definition, macro invocation, format definition
			--   include, doc state push/pop

	section_type: INTEGER
			-- Type, if any, to which Current applies
			-- e.g. If a structure tag, then the specific section type
			--      If a typed macro or format, then the section type 
			--      defined by the macro or format spec
			--      If an untyped macro, the zero

	params: detachable STRING
			-- Parameter portion of tag

	--|--------------------------------------------------------------

	is_title: BOOLEAN
		do
			Result := tag_type = K_usml_tag_title or
				(tag_type = K_usml_tag_m and section_type = K_usml_tag_title)
		end

	is_paragraph: BOOLEAN
		do
			Result := tag_type = K_usml_tag_p or
				(tag_type = K_usml_tag_m and section_type = K_usml_tag_p)
		end

	is_heading: BOOLEAN
		do
			-- Heading level corresponds to tag type
			Result := is_valid_usml_heading_level (tag_type)
			if not Result then
				inspect tag_type
				when K_usml_tag_m, K_usml_tag_f then
					Result := is_valid_usml_heading_level (section_type)
				else
				end
			end
		end

	is_list: BOOLEAN
		do
			-- List level corresponds to tag type
			Result := is_valid_usml_list_type (tag_type)
			if not Result then
				inspect tag_type
				when K_usml_tag_m, K_usml_tag_f then
					Result := is_valid_usml_list_type (section_type)
				else
				end
			end
		end

	is_outlined: BOOLEAN
			-- Is Current outlined (or outline-able)?
		do
			Result := is_heading or is_list
		end

	is_sub_start: BOOLEAN
		do
			Result := tag_type = K_usml_tag_h_plus
		end

	is_sub_end: BOOLEAN
		do
			Result := tag_type = K_usml_tag_h_minus
		end

	is_verbatim_open: BOOLEAN
		do
			Result := tag_type = K_usml_tag_v_open
		end

	is_verbatim_close: BOOLEAN
		do
			Result := tag_type = K_usml_tag_v_close
		end

	is_comment: BOOLEAN
		do
			Result := tag_type = K_usml_tag_c
		end

	is_format: BOOLEAN
		do
			Result := tag_type = K_usml_tag_f
		end

	is_macro_definition: BOOLEAN
	is_macro_invocation: BOOLEAN

	is_macro: BOOLEAN
		do
			Result := tag_type = K_usml_tag_m
		end

	is_typed_macro: BOOLEAN
		do
			Result := tag_type = K_usml_tag_m and section_type /= 0
		end

	inlines_disabled: BOOLEAN
			-- Is the text associated with Current to be taken 
			-- literally, without inline tag interpretation?
			-- Formatting still applies

	macro_name: detachable STRING
			-- Name of macro invoked in Current, if any
			-- If Current is macro def, name is in 'params'

	description: detachable STRING
			-- Description text for Current as macro tag, if any

	macro: detachable USML_TAG
			-- Macro being invoked by Current, if any

	format: detachable USML_FORMAT
			-- Format from macro being invoked by Current
		do
			if attached macro as tm then
				Result := tm.format
			end
		end

	--|--------------------------------------------------------------

	outline_level: INTEGER
			-- Level in element hierarchy
			-- Unlike usage in tag object itself, value in this context 
			-- is not biased for lists (but title still has special value)

	tag_text: detachable STRING
			-- Text of tag (between < >)

	trailing_text: detachable STRING
			-- Text that came on the same line as Current, but after the 
			-- closing tag

	--|--------------------------------------------------------------

	has_error: BOOLEAN
		do
			Result := attached error_message as e and then not e.is_empty
		end

	error_message: detachable STRING

	has_warning: BOOLEAN
		do
			Result := attached warning_message as w and then not w.is_empty
		end

	warning_message: detachable STRING

--|========================================================================
feature -- Status setting
--|========================================================================

	set_parse_error (v: STRING)
		do
			error_message := v
		end

	set_warning (v: STRING)
		do
			warning_message := v
		end

--|========================================================================
feature -- Access
--|========================================================================

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	value_from_param (v: STRING): STRING
			-- Value part of a tag-value pair, or, if not a pair, then 
			-- the whole string
			-- e.g.  v = name="foo"  Result = foo
			-- e.g.  v = a_value  Result = a_value
		require
			not_empty: not v.is_empty
			valid_quotes: v.has ('%"') implies v.occurrences ('%"') = 2
		local
			sp, qp: INTEGER
		do
			Result := ""
			sp := v.index_of ('=', 1)
			if sp = 0 then
				Result := v
			else
				qp := v.index_of ('%"', 1)
				if qp /= 0 then
					if qp = sp + 1 and v.item (v.count) = '%"' then
						-- ...="
						-- Assume a quoted value in a tv pair
						Result := v.substring (sp + 1, v.count)
						Result := quotes_stripped (Result)
					elseif qp = 1 and v.item (v.count) = '%"' then
						Result := quotes_stripped (v)
					end
				else
					Result := v.substring (sp + 1, v.count)
				end
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_TAG_DATA
