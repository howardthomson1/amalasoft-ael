note
	description: "{
A major section, within a structured USML document,
that contains verbatim (uninterpreted) text
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_VERBATIM_SECTION

inherit
	USML_SECTION
		redefine
			type_label, set_level
		end

create
	make_with_tag_and_data

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_tag_and_data (tag: USML_TAG; td: USML_TAG_DATA)
		do
			level := K_usml_tag_v_open
			tag_data_open := td
			make_with_tag (tag)
			-- Capture formatting, if any
			set_values_from_tag (tag)
		end

--|========================================================================
feature -- Status
--|========================================================================

	tag_data_open: USML_TAG_DATA
	tag_data_close: detachable USML_TAG_DATA

	type_label: STRING
		do
			Result := "Verbatim Section"
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_tdata_close (td: USML_TAG_DATA)
		do
			tag_data_close := td
		end

	set_level (v: INTEGER)
			-- Set level in doc hierarchy for Current
			-- Do nothing; level is fixed
		do
		end

	--|--------------------------------------------------------------

end -- class USML_VERBATIM_SECTION
