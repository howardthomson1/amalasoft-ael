note
	description: "{
A container widget in which can be presented a structured USML document
in a rich text component, and an index of headings in a tree component
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_DOC_BOX

inherit
	EV_VERTICAL_BOX
		redefine
			create_interface_objects, show
		end
	USML_GUI_CORE
		undefine
			default_create, copy, is_equal
		end

create
	make, make_with_document, make_no_toc,
	make_with_level, make_with_document_and_level

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

--	make_no_toc (doc: like document)
	make_no_toc
			-- Create Current without a TOC
		do
			toc_disabled := True
			toc_top_level := 0
--			make_with_document (doc)
			make
		end

	make_with_level (v: INTEGER)
		require
			valid_level: v >= 0 and v <= 2
		do
			toc_top_level := v
			make
		end

	make_with_document_and_level (doc: like document; v: INTEGER)
		require
			valid_level: v >= 0 and v <= 2
		do
			toc_top_level := v
			make_with_document (doc)
		end

	make_with_document (v: like document)
		do
			document := v
			core_make
			load_document
		end

	--|--------------------------------------------------------------

 	make
		do
 			document := dummy_doc
			core_make
 		end

	core_make
			-- like default_create, but can't redefine that, so ..
		do
 			low_text := ""
 			default_create
 			complete_initialization
		end

--|========================================================================
feature {EV_ANY} -- Initialization (after default_create)
--|========================================================================

	complete_initialization
			-- To be called by the client AFTER default_create
			--
			-- Complete initialization of components
			--
			-- Redefine in child but call Precursor as first 
			-- instruction in redefined feature, BEFORE any other 
			-- instructions
		do
			initialize_interface_values
			build_interface_components
			initialize_interface_actions
			post_initialize
			update_widget_rendition
		end

--|========================================================================
feature {NONE} -- Initialization (during default_create)
--|========================================================================

	create_interface_objects
			-- Create interface objects as part of the creation sequence 
			-- for Current
		do
			Precursor

			create main_sa

			create toc_frame
			create toc_tree

			create text_box
			create text_frame
			create textw
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		local
			vb: EV_VERTICAL_BOX
		do
			if toc_disabled then
				extend (text_frame)
			else
				extend (main_sa)
				main_sa.extend (toc_frame)
				toc_frame.set_minimum_width (70)
				create vb
				toc_frame.extend (vb)
				vb.extend (toc_tree)
				main_sa.extend (text_frame)
			end
			text_frame.extend (text_box)
			text_box.extend (textw)
			textw.disable_edit
--			textw.disable_word_wrapping
		end

	--|--------------------------------------------------------------

	build_toc_tree
			-- Construct the contents of the TOC tree by traversing the 
			-- document's headings.  Unlike sections, headings ARE 
			-- outlined, and so recursion is used
		local
			tmi: EV_TREE_ITEM
			doc: like document
			th: USML_HEADING
			lvl: INTEGER
			headings: LIST [USML_HEADING]
		do
			doc := document
			headings := document.headings

			from headings.start
			until headings.after
			loop
				-- From 'headings', should see only top-level headings 
				-- and the occasional non-indexable heading (from a subdoc)
				th := headings.item
				if th.is_indexable then
--					create tmi.make_with_text (th.text)
--					tmi.set_data (th)
					lvl := th.level
					if lvl = 1 then
						-- Top-level heading
						create tmi.make_with_text (th.text)
						tmi.set_data (th)
						-- This logic seems to lose track of the
						-- first instance of a level-2 heading
						-- Maybe revisit it eventually (maybe not)
--						if toc_top_level = 2 then
						if toc_top_level = 99 then
							-- omit top-level headings
							if th.has_items then
								build_toc_branch (tmi, th, True)
							end
						else
							toc_tree.extend (tmi)
							if th.has_items then
								build_toc_branch (tmi, th, False)
							end
--						else
--							-- problem
						end
					else
						--printf ("heading has level other than 1 (%%d]%N", << lvl >>)
					end
				end
				headings.forth
			end
		end

	--|--------------------------------------------------------------

	build_toc_branch (tb: EV_TREE_ITEM; psec: USML_HEADING; tf: BOOLEAN)
		local
			tmi: EV_TREE_ITEM
		do
			from psec.start
			until psec.after
			loop
				if attached {USML_HEADING}psec.item as sec then
					if not sec.is_indexable then
						-- Should not happen
					else
						create tmi.make_with_text (sec.text)
						tmi.set_data (sec)
						if tf and tb.parent = Void then
							toc_tree.extend (tb)
						else
							tb.extend (tmi)
						end
						if sec.has_items then
							build_toc_branch (tmi, sec, False)
						end
					end
				end
				psec.forth
			end
		end

	--|--------------------------------------------------------------

--|========================================================================
feature {NONE} -- Interface initialization
--|========================================================================

	initialize_interface_values
			-- Define values needed after component creation and BEFORE 
			-- component assembly or initialization
		do
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			if not toc_disabled then
				toc_tree.select_actions.extend (agent on_toc_select)
			end
			focus_in_actions.extend (agent on_focus_in)
			textw.caret_move_actions.extend (agent on_caret_move)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
		end

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

 	document: USML_DOCUMENT
--RFO 		do
--RFO 			Result := current_document
--RFO 		end

	low_text: STRING

	last_search_end: INTEGER

	toc_disabled: BOOLEAN
			-- Is TOC disabled? (i.e. show only the doc itself)

	toc_top_level: INTEGER
			-- Highest level heading to add to TOC tree
			-- If 0, unconstrained (none handled with toc_disabled)
			-- If 1, then top-level headings only
			-- If 2, then omit top-level headings and show only
			-- levels 2-5

--|========================================================================
feature -- Status setting
--|========================================================================

	enable_line_wrap
		do
			-- Does not have selection correlation, so no need to 
			-- disable wrapping, BUT need to disable expand to force 
			-- wrapping.  Hmmmm
			textw.enable_word_wrapping
			--textw.set_minimum_width (text_frame.width)
			--text_box.disable_item_expand (textw)
		end

	disable_line_wrap
		do
			-- Selection correlation, so no needs line/word wrapping 
			-- disabled to ensure correct line number to position 
			-- calculations
			textw.disable_word_wrapping
		end

	reset_current_document
			-- Reset, to 'document' the doc shared by other elements of 
			-- the system, the parsers, etc.
		do
			set_current_document (document)
		end

	set_pbuf_display_proc (v: PROCEDURE)
		do
			pbuf_display_proc := v
		end

	set_url_display_proc (v: PROCEDURE)
		do
			url_display_proc := v
		end

	set_file_display_proc (v: PROCEDURE)
		do
			file_display_proc := v
		end

	set_link_action_proc (v: PROCEDURE)
		do
			link_action_proc := v
		end

	set_toc_selection_proc (v: PROCEDURE)
		do
			toc_selection_proc := v
		end

	set_selection_notify_proc (v: PROCEDURE)
		do
			selection_notify_proc := v
		end

	set_caret_move_notify_proc (v: PROCEDURE)
		do
			caret_move_notify_proc := v
		end

	--|--------------------------------------------------------------

	updating_doc: BOOLEAN
	doc_total_chars: INTEGER

	set_document (v: detachable USML_DOCUMENT)
		do
			if attached v as tv then
				document := tv
				load_document
			else
				document := dummy_doc
				low_text := ""
				load_document
				set_status_text ("")
			end
		end

	--|--------------------------------------------------------------

	load_document
		local
			tp, lc: INTEGER
			doc: USML_DOCUMENT
		do
			doc := document
			updating_doc := True
			doc.clear_live_reference_targets
			doc_total_chars := doc.total_chars
			textw.caret_move_actions.wipe_out
			textw.selection_change_actions.wipe_out
			textw.pointer_button_press_actions.wipe_out
			textw.pointer_button_release_actions.wipe_out

			if not toc_disabled then
				toc_tree.wipe_out
			end
			--set_current_document (v)
			reset_current_document
			app_root.process_events

			textw.disable_sensitive
			fill_rich_text
			textw.enable_sensitive
			if not toc_disabled then
				build_toc_tree
			end
			app_root.process_graphical_events

			tp := textw.text_length
			if tp /= 0 then
				lc := textw.line_number_from_position (tp)
				set_status_text (
					aprintf ("Done: %%d characters; %%d lines",<< tp, lc >>))
			end
			textw.set_caret_position (1)
			textw.scroll_to_line (1)

			textw.caret_move_actions.extend (agent on_caret_move)
			textw.selection_change_actions.extend (agent on_selection_change)
			textw.pointer_button_press_actions.extend (agent on_mouse_down)
			textw.pointer_button_release_actions.extend (agent on_mouse_up)
			updating_doc := False
		end

	--|--------------------------------------------------------------

	rebuild_toc_tree
		do
			toc_frame.hide
			toc_tree.wipe_out
			build_toc_tree
			toc_frame.show
		end

	--|--------------------------------------------------------------

	was_shown: BOOLEAN

	on_focus_in
		do
		end

	show
		do
			Precursor
			if not updating_doc then
				if not toc_disabled then
					rebuild_toc_tree
					main_sa.set_proportion ({REAL}0.25)
					expand_toc_tree
				end
			end
			was_shown := True
		end

	--|--------------------------------------------------------------

	fill_rich_text
			-- Fill textw with elements from 'document'
		local
			doc: like document
			sec: USML_SECTION
			tp, lno, pd: INTEGER
		do
			textw.set_caret_position (1)
			textw.scroll_to_line (1)
			textw.deselect_all
			textw.remove_text
			textw.set_current_format (title_char_format)

			doc := document
			doc.set_rendered_start (1)
			from doc.start
			until doc.after
			loop
				pd := tp
				sec := doc.item
				if attached {USML_VERBATIM_SECTION}sec then
--TODO set format appropriate to verbatim text
--MAYBE retain current format for duration of section
				end
				write_doc_section (sec)
				-- Don't make user wait for rendering to start showing 
				-- results
				-- Because text is continually appended to textw, 
				-- performance decreases as the text gets larger.
				tp := textw.text_length
				pd := tp - pd
				if pd > 60 then
					lno := textw.line_number_from_position (tp)
					set_status_text (
						aprintf ("Processing; %%d (of %%d); %%d lines",
						<< tp, doc_total_chars, lno >>))
					app_root.process_events
				end
				doc.forth
			end
			doc.set_rendered_end (textw.text.count)
			low_text := textw.text.as_lower
		end

	--|--------------------------------------------------------------

	previous_section_format: detachable USML_FORMAT

	write_doc_section (ds: USML_SECTION)
			-- Write contents of 'd's to 'textw'
			-- While headings are outlined, sections are flat, so do NOT 
			-- process sections recursively
		local
			sp, ep, sec_start, lno, tlno: INTEGER
			par_fmt: EV_PARAGRAPH_FORMAT
			char_fmt, ichar_fmt: EV_CHARACTER_FORMAT
			ifmts: TWO_WAY_LIST [USML_INLINE_FORMAT]
			ifmt: USML_INLINE_FORMAT
			tfmt, sfmt: USML_FORMAT
			ref, xref: USML_REF
			sec_text: STRING
			is_ref: BOOLEAN
		do
			sec_text := ds.text
			tfmt := ds.default_format
			if attached ds.format as fmt then
				sfmt := fmt
			end
			if attached sfmt then
				-- Case value is not handled by EV_CHARACTER_FORMAT
				if sfmt.is_upper then
					sec_text := sec_text.as_upper
				elseif sfmt.is_lower then
					sec_text := sec_text.as_lower
				elseif sfmt.is_title_case then
					sec_text := sec_text.twin
					asr.recase_substring (
						sec_text, 1, sec_text.count, K_case_value_title)
				end
				char_fmt := character_format_from_format (sfmt, sfmt, tfmt)
				par_fmt := paragraph_format_from_format (sfmt)
				previous_section_format := sfmt
			else
				-- Use default format
--				char_fmt := default_char_format (ds.level)
--				par_fmt := default_par_format (ds.level)
				char_fmt := character_format_from_format (tfmt, tfmt, tfmt)
				par_fmt := paragraph_format_from_format (tfmt)
				previous_section_format := Void
			end
			-- start and end of region are determined by length of text 
			-- in widget before and after adding section text
			-- Before: foo
			--         1 3 len = 3, sp = 4
			-- After:  foo2
			--         1  4 len = 4, ep = 4
			-- Precondition on setting format is the sp < ep, so cannot, 
			-- it seems, set format on a 1-char region
			sp := textw.text_length + 1
			sec_start := sp
			lno := textw.line_count
			ds.set_rendered_start (sp)
			ds.set_rendered_start_line (lno)
			if ds.has_error then
				document.set_error_message (ds.error_messages.last)
			elseif not sec_text.is_empty then
				textw.set_current_format (char_fmt)
				textw.append_text (sec_text)
--				ep := textw.text_length
				ep := textw.text_length + 1
				if sp < ep then
					textw.format_region (sp, ep, char_fmt)
					textw.format_paragraph (sp, ep, par_fmt)
				end
				if ds.has_inline_formats then
					-- Re-format any text captured by inline tag
					-- This is done after the full text is written
					-- Inlines can also be references (w/ or w/o formatting)
					ifmts := ds.inline_formats
					from ifmts.start
					until ifmts.after
					loop
						ifmt := ifmts.item
						if not ifmt.reference_label.is_empty then
							-- Update position of captive text to reflect 
							-- rendered context
							ifmt.set_captive_text_rendered_start (
								ifmt.captive_text_start + sec_start)
							ifmt.set_captive_text_rendered_end (
								ifmt.captive_text_end + sec_start)
							create ref.make_with_values (
								ifmt.reference_label, ifmt.target_label,
								ifmt.captive_text_rendered_start,
								ifmt.captive_text_rendered_end)
 							if attached ref_target_by_label (ref.label) then
 								-- ERROR
 								document.set_error_message (
 									"Duplicate reference, from section starting at line "
 									+ ifmt.section_start.out)
 							else
								document.add_live_reference_target (ref)
 							end
							is_ref := True
						end
						if not ifmt.target_label.is_empty then
							-- Update position of captive text to reflect 
							-- rendered context
							-- Format already exists in section, so no need 
							-- to add it, right???
							ifmt.set_captive_text_rendered_start (
								ifmt.captive_text_start + sec_start)
							ifmt.set_captive_text_rendered_end (
								ifmt.captive_text_end + sec_start)
							create xref.make_with_values (
								ifmt.reference_label, ifmt.target_label,
								ifmt.captive_text_rendered_start,
								ifmt.captive_text_rendered_end)
--RFO refs seem to have been added previously (preprocessor?)
--							document.reference_links.extend (
-- 								xref, ifmt.reference_label)
							is_ref := True
						end
						-- Format captive text regions
						if attached ds.format as df then
							tfmt := df
						else
							tfmt := ds.default_format
						end
						ichar_fmt := character_format_from_format (
							ifmt, tfmt, ds.default_format)
						if not is_ref then
							-- captive text start/end not reset above
							ifmt.set_captive_text_rendered_start (
								ifmt.captive_text_start + sec_start)
							ifmt.set_captive_text_rendered_end (
								ifmt.captive_text_end + sec_start)
						end
						-- Formatting seems to prefer 0-based positions???
--						sp := ifmt.captive_text_start
						sp := (ifmt.captive_text_rendered_start - 1).max (1)
--						ep := ifmt.captive_text_end + 1
						ep := ifmt.captive_text_rendered_end
						-- Case value is not handled by EV_CHARACTER_FORMAT
						-- Change case before applying character format
						if ifmt.is_upper then
							recase_region (sp, ep, K_case_value_upper)
						elseif ifmt.is_lower then
							recase_region (sp, ep, K_case_value_lower)
						elseif ifmt.is_title_case then
							recase_region (sp, ep, K_case_value_title)
						end
						if sp < ep then
							textw.format_region (sp, ep, ichar_fmt)
						end
						sp := ep + 1
						tlno := textw.line_number_from_position (sp)
						ep := textw.last_position_from_line_number (tlno)
						if sp < ep then
							textw.set_current_format (char_fmt)
							textw.format_region (sp, ep, char_fmt)
							textw.format_paragraph (sp, ep, par_fmt)
						end

						ifmts.forth
					end
				end
				-- Put it back the way it was
				textw.set_current_format (char_fmt)
				--TODO might not be enough.  Try formatting a zero-length 
				--region  OR move call to inside loop
			else
				sp := sp -- for breakpoint when debugging
			end
		end

	--|--------------------------------------------------------------

	recase_region (sp, ep: INTEGER; cv: INTEGER)
			-- Force chars in region between sp and ep to case value 'cv'
		local
			ts, ts2: STRING
		do
			textw.select_region (sp, ep)
			ts := textw.selected_text
			inspect cv
			when K_case_value_upper then
				ts := ts.as_upper
			when K_case_value_lower then
				ts := ts.as_lower
			when K_case_value_title then
				-- Need to copy ts. lest original be corrupted
				ts2 := ts.twin
				asr.recase_substring (ts2, 1, ts.count, cv)
				ts := ts2
			else
				-- ERROR, but do nothing; should have been caught during 
				-- tag parsing
			end
			-- Now put it back
			textw.enable_edit
			textw.delete_selection
			textw.set_caret_position (sp)
			textw.insert_text (ts)
			textw.disable_edit
			textw.deselect_all
		end

	--|--------------------------------------------------------------

	set_top_window (v: EV_WINDOW)
		do
			top_window := v
		end

	set_status_label (v: EV_LABEL)
		do
			status_label := v
		end

	set_status_text (v: STRING)
		do
			if attached status_label as lb then
				lb.set_text (v)
			end
		end

--|========================================================================
feature -- Formats
--|========================================================================

	title_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_center_alignment
			Result.set_bottom_spacing (9)
		end

	hdr1_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_top_spacing (2)
			Result.set_bottom_spacing (3)
		end

	hdr2_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (2)
		end

	hdr3_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	hdr4_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	hdr5_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_bottom_spacing (1)
		end

	normal_par_format: EV_PARAGRAPH_FORMAT
		once
			create Result
			Result.enable_left_alignment
			Result.set_top_spacing (0)
			Result.set_bottom_spacing (0)
		end

	--|--------------------------------------------------------------

	code_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				fixed_font (14, True, False))
		end

	title_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				sans_serif_font (18, True, False))
		end

	hdr1_char_format: EV_CHARACTER_FORMAT
		local
			cfe: EV_CHARACTER_FORMAT_EFFECTS
		once
			create Result.make_with_font (
				times_font (14, True, False))
			create cfe
			cfe.enable_underlined
			Result.set_effects (cfe)
		end

	hdr2_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, True, False))
		end

	hdr3_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, True, True))
		end

	hdr4_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, False, True))
		end

	hdr5_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (11, False, True))
		end

	normal_char_format: EV_CHARACTER_FORMAT
		once
			create Result.make_with_font (
				times_font (12, False, False))
		end

--|========================================================================
feature -- Access
--|========================================================================

	--num_matches_last_search: INTEGER

	next_instance_of (v: STRING; spos: INTEGER; cif, sf: BOOLEAN): INTEGER
			-- Position, of next instance of 'v' at or after 'spos'
			-- If 'cif', then search case-insensitive, else search
			-- case-sensitive
			-- If 'sf' then select found instance
		require
			valid_start: spos > 0
		local
			sp, ep, lno: INTEGER
		do
			-- TODO, based on search criteria, capture number of 
			-- instances of search string in text (or low_text)
			if cif then
				sp := low_text.substring_index (v, spos)
			else
				sp := textw.search (v, spos)
				-- search in widget is case-sensitive :(
				-- fp is position of caret before 1st char of found text
				-- looking for foo and found it
			end
			if sp = 0 then
				textw.deselect_all
				last_search_end := 0
			elseif sf then
				-- Select the instance
				ep := sp + v.count - 1
				lno := textw.line_number_from_position (sp)
				if lno /= 0 then
					is_linking := True
					-- Scroll only if necessary to expose line
					if is_valid_caret_position (sp) and
						is_valid_caret_position (ep) and then
						not (textw.character_displayed (sp)
						and textw.character_displayed (ep))
					 then
						 textw.scroll_to_line (lno)
					end
					-- set caret to END of instance (to setup for 
					-- subsequent searches)
					-- N.B. setting caret position deselects all, so must 
					-- be done before selecting
					-- Select the instance
					textw.set_caret_position (sp)
					textw.select_region (sp, ep)
					last_search_end := ep + 1
					is_linking := False
				end
			end
			textw.set_focus
			Result := sp
		end

--|========================================================================
feature -- Comparison
--|========================================================================

--|========================================================================
feature -- Element change
--|========================================================================

--|========================================================================
feature -- Element removal
--|========================================================================

--|========================================================================
feature -- Persistence support
--|========================================================================

--|========================================================================
feature -- Serialization support
--|========================================================================

--|========================================================================
feature -- Conversion
--|========================================================================

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_textw_position (xp, yp: INTEGER): BOOLEAN
			-- Is position (xp, yp) within the text widget?
		do
			Result := xp >= 0 and xp < textw.width
				and yp >= 0 and yp < textw.height
		end

	is_valid_caret_position (cp: INTEGER): BOOLEAN
			-- Is caret position 'cp' within the text in the text widget?
			-- N.B. End-of-text in widget is NOT a valid caret position, 
			-- it is off-the end (text_length + 1)
		do
			Result := cp >= 1 and cp <= textw.text_length
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	pbuf_display_proc: detachable PROCEDURE
	url_display_proc: detachable PROCEDURE
	file_display_proc: detachable PROCEDURE
	link_action_proc: detachable PROCEDURE
	toc_selection_proc: detachable PROCEDURE
	selection_notify_proc: detachable PROCEDURE
	caret_move_notify_proc: detachable PROCEDURE

	is_linking: BOOLEAN
	last_xrf_at_ptr: detachable USML_INLINE_FORMAT

	on_caret_move (v: INTEGER)
			-- Respond to change of caret position in textw
		do
			last_search_end := 0
			last_vline1 := textw.first_visible_line
			show_caret_position
			if attached caret_move_notify_proc as proc then
				proc.call
			end
		end

	on_selection_change
			-- Respond to selection change event in text
		do
			-- Do not try to void last_xrf_at_ptr as this agent is 
			-- called rather often, and oding so will foul up xref states
			if textw.selected_text.count > 1 then
				show_caret_position
			end
			if attached selection_notify_proc as proc then
				proc.call
			end
		end

	on_mouse_down (xp, yp, b: INTEGER; st, yt, p: DOUBLE; sx, sy :INTEGER)
			-- Respond to mouse up event
		do
			last_xrf_at_ptr := Void
			if updating_doc then
				-- Ignore
			elseif b = 1 and is_valid_textw_position (xp, yp) then
				last_xrf_at_ptr := xref_at_position (xp, yp)
			end
		end

	--|--------------------------------------------------------------

	on_mouse_up (xp, yp, b: INTEGER; st, yt, p: DOUBLE; sx, sy :INTEGER)
			-- Respond to mouse up event
		local
			lno, sp, ep: INTEGER
			lb: STRING
		do
			if updating_doc then
				-- Ignore
			elseif b = 1 and is_valid_textw_position (xp, yp) then
				if attached xref_at_position (xp, yp) as xrf then
					if last_xrf_at_ptr = xrf then
						-- release at same point as press; is a click
						lb := xrf.target_label
						--print ("Found xref to: " + lb + "%N")
						-- Catch image xref here, before looking up the 
						-- target because the target isn't in the table (it 
						-- has no position in the document)
						if xrf.xref_type = K_xref_type_url then
							-- Xref refers to a URL
							show_url_contents (xrf)
						elseif xrf.xref_type =  K_xref_type_action then
							-- Execution action associated with xref
							exec_link_action (xrf)
						elseif xrf.xref_type =  K_xref_type_file then
							-- Xref refers to a local file
							show_file_contents (xrf)
						elseif xrf.has_image_reference then
							-- Has reference to image file
							exec_image_link (xrf)
							--elseif attached targs.item (lb) as tref then
						elseif attached image_by_label (lb) as iref then
							-- Xref refers to an embedded image
							show_embedded_image (iref)
						elseif attached pbuf_by_label (lb) as pb then
							-- Xref refers to a precompiled pixel buffer
							show_pixel_buffer (pb)
						elseif attached ref_target_by_label (lb) as tref then
							-- Xref refers to an internal location
							--
							-- Regions for highlighting seem to be 0-based???
							-- Not exactly, but the character position in 
							-- the widget is _before_ the character, so a 
							-- left-shift is required to ensure that the 
							-- right characters are highlighted
							sp := (tref.start_position - 1).max (1)
							ep := (tref.end_position - 1).max (1)
							lno := textw.line_number_from_position (sp)
							if lno /= 0 then
								is_linking := True
								-- Scroll only if necessary to expose line
								if is_valid_caret_position (sp) and
									is_valid_caret_position (ep) and then
									not (textw.character_displayed (sp)
									and textw.character_displayed (ep))
								 then
									 textw.scroll_to_line (lno)
								end
								-- set caret to start of captive text
								if is_valid_caret_position (sp) then
									textw.set_caret_position (sp)
									-- Select the captive text
									textw.select_region (sp, ep)
								end
								is_linking := False
							end
						end
					end
				end
				last_xrf_at_ptr := Void
			end
		end

	--|--------------------------------------------------------------

	exec_link_action (ref: USML_INLINE_FORMAT)
			-- Execution action associated with link 'ref'
			-- target_label includes name of action and an optional
			-- parameter(s) in the style of a function call
			--     <name>[(<arg_list>)]
			-- Client or descendent manages actions, with client 
			-- procedure being called if defined.
			-- To define action processor, client sets link_action_proc
			-- to be a procedure accepting, as arguments, an attached 
			-- STRING (the action name), and a detachable LIST [STRING] 
			-- that holds the arguments, if any
			-- In this way, link actions are similar to other xrefs
			-- (e.g. ext images)
		local
			ts, anm: STRING
			sp, ep: INTEGER
			args: LIST [STRING]
		do
			ts := ref.target_label
			if attached link_action_proc as proc and not ts.is_empty then
				ep := ts.index_of ('(', 1)
				if ep = 0 then
					-- no args
					anm := ts
					anm.prune_all_trailing (' ')
				else
					anm := ts.substring (1, ep - 1)
					anm.prune_all_trailing (' ')
					sp := ep
					ep := ts.index_of (')', sp)
					if ep = 0 then
						-- ERROR
					elseif ep = (sp + 1) then
						-- No args
					else
						ts := ts.substring (sp + 1, ep - 1)
						args := ts.split (',')
					end
				end
				proc.call (anm, args)
			end
		end

	--|--------------------------------------------------------------

	exec_image_link (ref: USML_INLINE_FORMAT)
			-- Open, in the image dialog, the image referenced in 'ref' 
			-- as target
		local
			fp: PATH
		do
			-- Check ref for env var syntax, and if env var, get that 
			-- from exenv and assemble actual path from it
			-- Env vars begin with '$'
			fp := asr.path_from_env_path (ref.target_label)
			show_image_file (fp)
		end

	show_image_file (fp: PATH)
			-- Open, in the image dialog, the image in file at 'fp'
		local
			pbuf: EV_PIXEL_BUFFER
			tf: RAW_FILE
		do
			create tf.make_with_path (fp)
			if not tf.exists then
				-- ERROR
				--printf("Image file not found: %%s%N", << fp.out >>)
			else
				create pbuf
				pbuf.set_with_named_path (fp)
				show_pixel_buffer (pbuf)
			end
		end

	show_pixel_buffer (pbuf: EV_PIXEL_BUFFER)
			-- Open, in the image dialog, the pixel buffer 'pbuf'
		do
			if attached pbuf_display_proc as proc then
				proc.call (pbuf)
			end
		end

	show_url_contents (ref: USML_INLINE_FORMAT)
		local
			ts: STRING
		do
			if attached url_display_proc as proc then
				ts := ref.target_label
				--if not url_env_expansion_disabled then
				if True then
					ts := asr.url_from_env (ts)
				end
				proc.call (ts)
			end
		end

	show_file_contents (ref: USML_INLINE_FORMAT)
		local
			fp: PATH
		do
			-- Check ref for env var syntax, and if env var, get that 
			-- from exenv and assemble actual path from it
			-- Env vars begin with '$'
			fp := asr.path_from_env_path (ref.target_label)
			if attached file_display_proc as proc then
				proc.call (fp)
			end
		end

	show_embedded_image (iref: USML_IMAGE_TAG)
			-- Open, in the image dialog, the embedded image from 'iref'
		local
			pbuf: EV_PIXEL_BUFFER
			cany: ANY
			ptr: POINTER
			len: INTEGER
		do
			len := iref.image_text.count
			cany := iref.image_text.to_c
			ptr := $cany
			create pbuf
			pbuf.set_with_pointer (ptr, len)
			show_pixel_buffer (pbuf)
		end

	--|--------------------------------------------------------------

	on_toc_select
			-- Respoond to select event in TOC tree
			-- Scroll to selection
		local
			cp, lno: INTEGER
		do
			if attached toc_tree.selected_item as ti then
				if attached {USML_SECTION}ti.data as sec then
					cp := sec.rendered_start
					lno := textw.line_number_from_position (cp)
					if lno /= 0 then
						textw.scroll_to_line (lno)
						textw.set_caret_position (cp)
						show_caret_position
						-- toc_selection_proc has been defined, call it, 
						-- with the section selected from the toc AFTER 
						-- other processing
						if attached toc_selection_proc as tsp then
							tsp.call (sec)
						end
					end
				end
			end
		end

--|========================================================================
feature -- Implementation
--|========================================================================

	expand_toc_tree
			-- Expand each item in the tree
		do
			if not toc_tree.is_empty then
				toc_tree.recursive_do_all (agent expand_toc_node)
				toc_tree.ensure_item_visible (toc_tree.first)
			end
		end

	--|--------------------------------------------------------------

	expand_toc_node (tn: EV_TREE_NODE)
			-- Expand the tree to expose node 'tn'
		do
			toc_tree.ensure_item_visible (tn)
		end

--|========================================================================
feature -- Support
--|========================================================================

	xref_at_position (xp, yp: INTEGER): detachable USML_INLINE_FORMAT
			-- Xref at position, if any (xref itself, not its target)
		require
			valid: is_valid_textw_position (xp, yp)
		local
			cp: INTEGER
		do
			-- Doc uses stream-based indices, where widget indices 
			-- identify the character AFTER the caret.  To reconcile 
			-- this, add one to the widget-based index.
			cp := textw.index_from_position (xp, yp) + 1
			if attached document.section_by_rendered_position (cp) as tsec then
				Result := tsec.xref_by_position (cp)
			end
		end

	show_caret_position
			-- Show current caret position in status bar
		local
			cp, lno, eln, lc, sp, ep: INTEGER
			ts, msg: STRING
		do
			cp := textw.caret_position
			if textw.valid_caret_position (cp) then
				lno := textw.line_number_from_position (cp)
				lc := textw.line_count
				sp := textw.start_selection
				ep := textw.end_selection
				if sp /= ep then
					-- Has selection; get selection start/end line(s)
					lno := textw.line_number_from_position (sp)
					eln := textw.line_number_from_position (ep)
					ts := textw.selected_text
					if lno = eln then
						msg := aprintf (
							"Chars %%d-%%d, of %%d; Line %%d of %%d",
							<< sp, ep-1, textw.text_length, lno, lc >>)
					else
						msg := aprintf (
							"Chars %%d-%%d, of %%d; Lines %%d-%%d of %%d",
							<< sp, ep-1, textw.text_length, lno, eln, lc >>)
					end
				elseif cp > textw.text_length then
					msg := aprintf (
						"Bottom; total %%d; Line %%d of %%d",
						<< textw.text_length, lno, lc >>)
				else
					msg := aprintf (
						"Char [%%s] at %%d of %%d; Line %%d of %%d",
						<< textw.text.item (cp).out, cp,
						textw.text_length, lno, lc >>)
				end
				set_status_text (msg)
			end
			-- TODO maybe qualify this a bit more
			textw.set_focus
		end

	--|--------------------------------------------------------------

	doc_section_by_position (v: INTEGER): detachable USML_SECTION
			-- Document section corresponding to rendered textw caret
			-- position 'v' (if any)
		do
			Result := document.section_by_rendered_position (v)
		end

	--RFO doc_section_at_caret: detachable USML_SECTION
	--RFO 		-- Document section corresponding to current position (if 
	--RFO 		-- any)
	--RFO 	local
	--RFO 		cp: INTEGER
	--RFO 	do
	--RFO 		cp := textw.caret_position
	--RFO 		if textw.valid_caret_position (cp) then
	--RFO 			Result := doc_section_by_position (cp)
	--RFO 		end
	--RFO 	end

	--RFO doc_section_by_lineno (v: INTEGER): detachable USML_SECTION
	--RFO 		-- Document section corresponding to line number 'v' (if any)
	--RFO 	local
	--RFO 		cp: INTEGER
	--RFO 	do
	--RFO 		cp := textw.first_position_from_line_number (v)
	--RFO 		if textw.valid_caret_position (cp) then
	--RFO 			Result := doc_section_by_position (cp)
	--RFO 		end
	--RFO 	end

	--|--------------------------------------------------------------

	selection_to_sections: LINKED_LIST [USML_SECTION]
			-- Document sections corresponding to selections (if any)
			-- in rendered text, for current document
		local
			rtsp, rtep: INTEGER
			sid_start, sid_end, sid: INTEGER
			doc: USML_DOCUMENT
			txt: STRING
		do
			create Result.make
			doc := document
			if textw.has_selection then
				rtsp := textw.start_selection
				rtep := textw.end_selection
				txt := textw.selected_text
				-- ISSUE: when newline is selected, rtep is 1 char too far
				-- e.g. selection last char in line and newline (2 chars 
				-- total) gives a range of 3 chars, perhaps due to 
				-- embedded linefeeds?
				-- This conditions occurs only when newline is last char 
				-- in selection (it seems)
				-- Shifting the selection end pos in this will work 
				-- around that issue, but will remove ability to select 
				-- just the newline character
				if not txt.is_empty and then txt.item (txt.count) = '%N' then
					rtep := rtep - 1
				end
				if attached doc_section_by_position (rtsp) as tsec then
					sid_start := tsec.section_index
				end
				if attached doc_section_by_position (rtep) as tsec then
					sid_end := tsec.section_index
				end
				from sid := sid_start
				until sid > sid_end
				loop
					if attached doc.i_th_section (sid) as ts then
						Result.extend (ts)
					end
					sid := sid + 1
				end
			end
		end

	--|--------------------------------------------------------------

	lines_to_first_visible: INTEGER
			-- Number of visible lines before current caret position
		local
			cln, vln: INTEGER
		do
			cln := textw.current_line_number
			vln := textw.first_visible_line
			Result := cln - vln
		end

	line_is_visible (lno: INTEGER): BOOLEAN
			-- Is 'lno' within display area?
		local
			cp: INTEGER
		do
			if textw.valid_line_index (lno) then
				cp := textw.first_position_from_line_number (lno)
				if is_valid_caret_position (cp) then
					Result := textw.character_displayed (cp)
				end
			end
		end

	position_from_charpos (cp: INTEGER): EV_COORDINATE
			-- XY coordinate, in display area, of char pos 'cp'
			-- If not presently displayed, then zero
		do
			create Result
			if is_valid_caret_position (cp) then
				if textw.character_displayed (cp) then
					Result := textw.position_from_index (cp)
				end
			end
		end

	last_vline1: INTEGER
			-- Most recent 'first_visible_line'

--|========================================================================
feature -- Textw access
--|========================================================================

	text_length: INTEGER
		do
			Result := textw.text_length
		end

	caret_position: INTEGER
		do
			Result := textw.caret_position
		end

	set_caret_position (v: INTEGER)
		do
			textw.set_caret_position (v)
		end

	has_selection: BOOLEAN
		do
			Result := textw.has_selection
		end

	selected_text: STRING
		do
			Result := textw.selected_text
		end

	selection_start: INTEGER
		do
			Result := textw.start_selection
		end

	selection_end: INTEGER
		do
			Result := textw.end_selection
		end

--|========================================================================
feature -- Components
--|========================================================================

	toc_frame: EV_FRAME
	toc_tree: EV_TREE

	main_sa: EV_HORIZONTAL_SPLIT_AREA

	text_box: EV_HORIZONTAL_BOX
	text_frame: EV_FRAME
	textw: EV_RICH_TEXT

	top_window: detachable EV_WINDOW
			-- Top-level window parent of Current

	status_label: detachable EV_LABEL
			-- Label in parent status bar, if any

	app_root: APPLICATION_ROOT
			-- Root of application
		do
			Result := app_env.application
		end

--|========================================================================
feature -- Defaults
--|========================================================================

	default_char_format (lvl: INTEGER): EV_CHARACTER_FORMAT
			-- Default character format for section level 'lvl'
		do
			inspect lvl
			when 1 then
				Result := hdr1_char_format
			when 2 then
				Result := hdr2_char_format
			when 3 then
				Result := hdr3_char_format
			when 4 then
				Result := hdr4_char_format
			when 5 then
				Result := hdr5_char_format
			else
				Result := normal_char_format
			end
		end

	default_par_format (lvl: INTEGER): EV_PARAGRAPH_FORMAT
			-- Default paragraph format for section level 'lvl'
		do
			inspect lvl
			when 1 then
				Result := hdr1_par_format
			when 2 then
				Result := hdr2_par_format
			when 3 then
				Result := hdr3_par_format
			when 4 then
				Result := hdr4_par_format
			when 5 then
				Result := hdr5_par_format
			else
				Result := normal_par_format
			end
		end

--|========================================================================
feature -- Fonts
--|========================================================================

	sans_serif_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (
				ev_font_constants.Family_sans, wt, it, ps)
			Result.preferred_families.extend ("Arial")
		end

	--|--------------------------------------------------------------

	times_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (
				ev_font_constants.Family_roman, wt, it, ps)
			Result.preferred_families.extend ("Times New Roman")
		end

	--|--------------------------------------------------------------

	fixed_font (ps: INTEGER; bf, itf: BOOLEAN): EV_FONT
		local
			wt: INTEGER
			it: INTEGER
		do
			wt := font_weight (bf)
			it := font_style (itf)
			create Result.make_with_values (
				ev_font_constants.Family_typewriter, wt, it, ps)
			Result.preferred_families.extend ("Courier New")
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	app_env: APPLICATION_ENV
		once
			create Result
		end

	exenv: EXECUTION_ENVIRONMENT
			-- Access to underlying execution environment.
		once
			create Result
		end

	openv: OPERATING_ENVIRONMENT
			-- Access to underlying operating environment.
		once
			create Result
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 26-May-2019
--|     Created original module (for Eiffel 18.1)
--|     Adapted from AEL_V2_VERTICAL_BOX
--|----------------------------------------------------------------------

end -- class USML_DOC_BOX
