note
	description: "{
Constants and related features for USML GUI components
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2019 Amalasoft.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_GUI_CORE

inherit
	USML_CORE

--|========================================================================
feature -- Pixel buffers, pixmaps, etc.
--|========================================================================

	pixel_buffers: HASH_TABLE [EV_PIXEL_BUFFER, STRING]
		once
			create Result.make (7)
		end

	pbuf_by_label (lb: STRING): detachable EV_PIXEL_BUFFER
		do
			Result := pixel_buffers.item (lb)
		end

--|========================================================================
feature -- Format conversion/generation
--|========================================================================

	font_family_from_format (fmt: USML_FORMAT): INTEGER
		do
			if fmt.font_family ~ Ks_ff_sans then
				Result := ev_font_constants.family_sans
			elseif fmt.font_family ~ Ks_ff_times then
				Result := ev_font_constants.family_roman
			elseif fmt.font_family ~ Ks_ff_fixed then
				Result := ev_font_constants.family_typewriter
			else
				Result := ev_font_constants.family_sans
			end
		end

	font_from_format (fmt: USML_FORMAT): EV_FONT
			-- Attributes of 'fmt', as a font
		local
			ff, ps, wt, sh: INTEGER
		do
			ff := font_family_from_format (fmt)
			ps := fmt.point_size
			if ps = 0 then
				ps := K_dflt_point_size_p
			end
			wt := font_weight_from_format (fmt)
			sh := font_shape_from_format (fmt)
			create Result.make_with_values (ff, wt, sh, ps)
			if ff = ev_font_constants.Family_roman then
				Result.preferred_families.extend ("Times New Roman")
			elseif ff = ev_font_constants.Family_typewriter then
				Result.preferred_families.extend ("Courier New")
			else
				Result.preferred_families.extend ("Arial")
			end
		end

	font_from_formats (fmt, rfmt, dfmt: USML_FORMAT): EV_FONT
			-- Attributes of 'fmt', as a font
			-- rfmt is format of context (for inlines)
			-- dfmt is default format for context
		local
			ff, ps, wt, sh: INTEGER
		do
			ff := font_family_from_format (fmt)
			if fmt.point_size_is_relative and attached rfmt as rf then
				ps := rf.point_size + fmt.point_size
			else
				ps := fmt.point_size
			end
			if ps = 0 then
				ps := dfmt.point_size
			end
			wt := font_weight_from_format (fmt)
			sh := font_shape_from_format (fmt)
			create Result.make_with_values (ff, wt, sh, ps)
			if ff = ev_font_constants.Family_roman then
				Result.preferred_families.extend ("Times New Roman")
			elseif ff = ev_font_constants.Family_typewriter then
				Result.preferred_families.extend ("Courier New")
			else
				Result.preferred_families.extend ("Arial")
			end
		end

	--|--------------------------------------------------------------

	font_shape_from_format (fmt: USML_FORMAT): INTEGER
		do
			if fmt.is_italic then
				Result := ev_font_constants.shape_italic
			else
				Result := ev_font_constants.shape_regular
			end
		end

	font_weight_from_format (fmt: USML_FORMAT): INTEGER
		do
			if fmt.is_bold then
				Result := ev_font_constants.weight_bold
			else
				Result := ev_font_constants.weight_regular
			end
		end

	font_color_from_format (fmt: USML_FORMAT): EV_COLOR
		local
			low_rc, rs, gs, bs: STRING
			rv, gv, bv: INTEGER
		do
			if attached fmt.text_color as rc and then is_valid_rgb_color (rc) then
				if rc.starts_with ("0x") then
					low_rc := rc.substring (3, rc.count).as_lower
				else
					low_rc := rc.as_lower
				end
				rs := low_rc.substring (1, 2)
				gs := low_rc.substring (3, 4)
				bs := low_rc.substring (5, 6)
				rv := hex_8_bit_to_integer (rs)
				gv := hex_8_bit_to_integer (gs)
				bv := hex_8_bit_to_integer (bs)
				create Result.make_with_8_bit_rgb (rv, gv, bv)
			else
				Result := ev_colors.black
			end
		end

	bg_color_from_format (fmt: USML_FORMAT): detachable EV_COLOR
		local
			low_rc, rs, gs, bs: STRING
			rv, gv, bv: INTEGER
		do
			if attached fmt.bg_color as rc and then is_valid_rgb_color (rc) then
				if rc.starts_with ("0x") then
					low_rc := rc.substring (3, rc.count).as_lower
				else
					low_rc := rc.as_lower
				end
				rs := low_rc.substring (1, 2)
				gs := low_rc.substring (3, 4)
				bs := low_rc.substring (5, 6)
				rv := hex_8_bit_to_integer (rs)
				gv := hex_8_bit_to_integer (gs)
				bv := hex_8_bit_to_integer (bs)
				create Result.make_with_8_bit_rgb (rv, gv, bv)
			end
		end

	char_effects_from_format (fmt: USML_FORMAT): EV_CHARACTER_FORMAT_EFFECTS
		do
			create Result
			if fmt.is_strike_through then
				Result.enable_striked_out
			end
			if fmt.is_underlined then
				Result.enable_underlined
			end
			Result.set_vertical_offset (fmt.vertical_offset)
		end

	character_format_from_format (
		fmt, rfmt, dfmt: USML_FORMAT): EV_CHARACTER_FORMAT
		local
			bfmt: USML_FORMAT
		do
			if rfmt /= fmt then
				create bfmt.make_blended (fmt, rfmt, True, True)
			else
				bfmt := fmt
			end
			create Result.make_with_font (font_from_formats (bfmt, rfmt, dfmt))
			Result.set_color (font_color_from_format (bfmt))
			if attached bg_color_from_format (bfmt) as bg then
				Result.set_background_color (bg)
			end
			Result.set_effects (char_effects_from_format (bfmt))
		end

	paragraph_format_from_format (fmt: USML_FORMAT): EV_PARAGRAPH_FORMAT
		do
			create Result
			if fmt.is_right_aligned then
				Result.enable_right_alignment
			elseif fmt.is_center_aligned then
				Result.enable_center_alignment
			elseif fmt.is_justified then
				Result.enable_justification
			else
				Result.enable_left_alignment
			end

			Result.set_left_margin (fmt.left_margin)
			Result.set_right_margin (fmt.right_margin)
			Result.set_top_spacing (fmt.top_margin)
			Result.set_bottom_spacing (fmt.bottom_margin)
		end

	--|--------------------------------------------------------------

	align_const_usml_to_ev (v: INTEGER): INTEGER
			-- Eiffel Vision constant corresponding to
			-- internal alignment value 'v'
		do
			inspect v
			when K_align_left then
				Result := evp_constants.Alignment_left
			when K_align_center then
				Result := evp_constants.Alignment_center
			when K_align_right then
				Result := evp_constants.Alignment_right
			when K_align_justified then
				Result := evp_constants.Alignment_justified
			else
				Result := evp_constants.Alignment_left
			end
		end

	align_const_ev_to_usml (v: INTEGER): INTEGER
			-- Eiffel Vision constant corresponding to
			-- internal alignment value 'v'
		do
			if v = evp_constants.Alignment_left then
				Result := K_align_left
			elseif v = evp_constants.Alignment_center then
				Result := K_align_center
			elseif v = evp_constants.Alignment_right then
				Result := K_align_right
			elseif v = evp_constants.Alignment_justified then
				Result := K_align_justified
			else
				Result := K_align_left
			end
		end

--|========================================================================
feature {NONE} -- Constants
--|========================================================================

	ev_font_constants: EV_FONT_CONSTANTS
			-- Family_screen, Family_roman, Family_sans,
			-- Family_typewriter, Family_modern
			-- Weight_thin, Weight_regular, Weight_bold, Weight_black
			-- Shape_regular, Shape_italic
		once
			create Result
		end

	ev_colors: EV_STOCK_COLORS
		once
			create Result
		end

	evp_constants: EV_PARAGRAPH_CONSTANTS
		once
			create Result
		end

	font_weight (bf: BOOLEAN): INTEGER
		do
			if bf then
				Result := ev_font_constants.Weight_bold
			else
				Result := ev_font_constants.Weight_regular
			end
		end

	font_style (itf: BOOLEAN): INTEGER
		do
			if itf then
				Result := ev_font_constants.Shape_italic
			else
				Result := ev_font_constants.Shape_regular
			end
		end

	--|--------------------------------------------------------------
invariant

--|----------------------------------------------------------------------
--| History
--|
--| 001 27-Jun-2019
--|     Created original module (Eiffel 18.07, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--|----------------------------------------------------------------------

end -- class USML_GUI_CORE
