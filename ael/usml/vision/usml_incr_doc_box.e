note
	description: "{
A container widget in which can be presented a collection of
structured USML documents in a rich text component, and an 
two-part index of headings in a pair of tree components
| Lvl 1 Headings | Lvl 2-N Headings | Rendered Text |
}"
	system: "Amalasoft Eiffel Library"
	source: "Amalasoft"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class USML_INCR_DOC_BOX

inherit
	EV_HORIZONTAL_BOX
		redefine
			create_interface_objects
		end
	USML_GUI_CORE
		undefine
			default_create, copy, is_equal
		end

create
	make_with_documents

--|========================================================================
feature {NONE} -- Creation and initialization
--|========================================================================

	make_with_documents (v: LIST [USML_DOCUMENT])
		require
			has_docs: not v.is_empty
		do
			create documents.make
			documents.fill (v)
			core_make
		end

	--|--------------------------------------------------------------

	core_make
			-- like default_create, but can't redefine that, so ..
		do
 			default_create
 			complete_initialization
		end

	--|--------------------------------------------------------------

	create_interface_objects
			-- Create any auxilliary objects needed for Curret
			-- Initialization for these objects must be performed in 
			-- `user_initialization'.
		do
			Precursor

			create doc_box_frame
			create toc_frame
			create toc_tree
			if not documents.is_empty then
				create doc_box.make_with_document_and_level (
					documents.first, 2)
			else
				create doc_box.make_with_level (2)
			end
			-- Does not have selection correlation, so no need to 
			-- disable wrapping, BUT need to disable expand to force 
			-- wrapping.  Hmmmm
			doc_box.enable_line_wrap
		end

	--|--------------------------------------------------------------

	build_interface_components
			-- Assemble interface components previously created
		do
			extend (toc_frame)
			toc_frame.extend (toc_tree)
			toc_frame.set_minimum_width (170)
			disable_item_expand (toc_frame)
			extend (doc_box_frame)
			doc_box_frame.extend (doc_box)

			build_toc_tree
		end

	--|--------------------------------------------------------------

	build_toc_tree
			-- Construct the contents of the TOC tree by traversing the 
			-- document's headings.  Unlike sections, headings ARE 
			-- outlined, and so recursion is used, but (unlike in the 
			-- doc_box itself), this tree has only level-1 headings 
		local
			tmi: EV_TREE_ITEM
			docs: LIST [USML_DOCUMENT]
			td: USML_DOCUMENT
			ref: HELP_DOC_REF
			tt: STRING
		do
			docs := documents

			from docs.start
			until docs.after
			loop
				td := docs.item
				tt := td.title
				create tmi.make_with_text (tt)
				create ref.make_with_doc (td)
				tmi.set_data (ref)
				toc_tree.extend (tmi)
				docs.forth
			end
			toc_tree.select_actions.extend (agent on_topic_select)
		end

--|========================================================================
feature {EV_ANY} -- Initialization (after default_create)
--|========================================================================

	complete_initialization
			-- To be called by the client AFTER default_create
			--
			-- Complete initialization of components
			--
			-- Redefine in child but call Precursor as first 
			-- instruction in redefined feature, BEFORE any other 
			-- instructions
		do
			build_interface_components
			initialize_interface_actions
			post_initialize
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			doc_box.set_pbuf_display_proc (agent show_pixel_buffer)
			doc_box.set_url_display_proc (agent show_url_contents)
			doc_box.set_file_display_proc (agent show_file_contents)
		end

	--|--------------------------------------------------------------

	post_initialize
			-- Perform any initialization operations that are needed 
			-- after the other operations defined from 
			-- complete_initialization and before completing the
			-- creation/initialization sequence
		do
		end

	--|--------------------------------------------------------------

	update_widget_rendition
			-- Set rendition of component widgets to reflect current state
		do
		end

--|========================================================================
feature -- Status
--|========================================================================

	loading_document: BOOLEAN
	documents: TWO_WAY_LIST [USML_DOCUMENT]

--|========================================================================
feature -- Status setting
--|========================================================================

	reset_current_document
			-- Reset, to 'document' the doc shared by other elements of 
			-- the system, the parsers, etc.
		do
			set_current_document (documents.first)
		end

	set_pbuf_display_proc (v: PROCEDURE)
		do
			pbuf_display_proc := v
		end

	set_url_display_proc (v: PROCEDURE)
		do
			url_display_proc := v
		end

	set_file_display_proc (v: PROCEDURE)
		do
			file_display_proc := v
		end

	set_selection_notify_proc (v: PROCEDURE)
		do
			selection_notify_proc := v
		end

	set_caret_move_notify_proc (v: PROCEDURE)
		do
			caret_move_notify_proc := v
		end

--|========================================================================
feature -- Agents and actions
--|========================================================================

	pbuf_display_proc: detachable PROCEDURE
	url_display_proc: detachable PROCEDURE
	file_display_proc: detachable PROCEDURE
	selection_notify_proc: detachable PROCEDURE
	caret_move_notify_proc: detachable PROCEDURE

	on_topic_select
			-- Respond to select event from a toc tree node associated 
			-- with topic document reference 'ref'
			-- Show selected topic
		local
			evc: EV_POINTER_STYLE
		do
			if loading_document then
				-- Ignore selection event
			elseif attached toc_tree.selected_item as ti then
				app_env.application.process_events
				loading_document := True
				create evc.make_predefined (
					{EV_POINTER_STYLE_CONSTANTS}.busy_cursor)
				set_pointer_style (evc)
				doc_box_frame.set_pointer_style (evc)
				doc_box.disable_sensitive

				if attached {HELP_DOC_REF}ti.data as ref then
					if attached ref.box as tbox then
						-- already rendered
						doc_box := tbox
					else
						create doc_box.make_with_document_and_level (ref.document, 2)
						doc_box.enable_line_wrap
					end
					ref.set_box (Void)
					replace_doc_box (doc_box)
					ref.set_box (doc_box)
				end
				loading_document := False
				doc_box.enable_sensitive
				create evc.make_predefined (
					{EV_POINTER_STYLE_CONSTANTS}.standard_cursor)
				set_pointer_style (evc)
				doc_box_frame.set_pointer_style (evc)
			end
		end

	--|--------------------------------------------------------------

	replace_doc_box (v: like doc_box)
		do
			if attached doc_box_frame.item as tdb then
			end
			doc_box.reset_current_document
			doc_box_frame.replace (v)
			v.set_pbuf_display_proc (agent show_pixel_buffer)
			v.set_url_display_proc (agent show_url_contents)
			v.set_file_display_proc (agent show_file_contents)
			doc_box.show
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	--|--------------------------------------------------------------

	exec_image_link (ref: USML_INLINE_FORMAT)
			-- Open, in the image dialog, the image referenced in 'ref' 
			-- as target
		local
			fp: PATH
		do
			create fp.make_from_string (ref.target_label)
			show_image_file (fp)
		end

	show_image_file (fp: PATH)
			-- Open, in the image dialog, the image in file at 'fp'
		local
			pbuf: EV_PIXEL_BUFFER
			tf: RAW_FILE
		do
			create tf.make_with_path (fp)
			if not tf.exists then
				-- ERROR
			else
				create pbuf
				pbuf.set_with_named_path (fp)
				show_pixel_buffer (pbuf)
			end
		end

	show_pixel_buffer (pbuf: EV_PIXEL_BUFFER)
			-- Open, in the image dialog, the pixel buffer 'pbuf'
		do
			if attached pbuf_display_proc as proc then
				proc.call (pbuf)
			end
		end

	show_url_contents (v: STRING)
		do
			if attached url_display_proc as proc then
				proc.call (v)
			end
		end

	show_file_contents (fp: PATH)
		do
			if attached file_display_proc as proc then
				proc.call (fp)
			end
		end

	show_embedded_image (iref: USML_IMAGE_TAG)
			-- Open, in the image dialog, the embedded image from 'iref'
		local
			pbuf: EV_PIXEL_BUFFER
			cany: ANY
			ptr: POINTER
			len: INTEGER
		do
			len := iref.image_text.count
			cany := iref.image_text.to_c
			ptr := $cany
			create pbuf
			pbuf.set_with_pointer (ptr, len)
			show_pixel_buffer (pbuf)
		end

--|========================================================================
feature -- Components
--|========================================================================

	toc_frame: EV_FRAME
	toc_tree: EV_TREE
	doc_box_frame: EV_FRAME
	doc_box: USML_DOC_BOX

--	refs: LINKED_LIST [HELP_DOC_REF]

	app_env: APPLICATION_ENV
		once
			create Result
		end

end -- class USML_INCR_DOC_BOX
