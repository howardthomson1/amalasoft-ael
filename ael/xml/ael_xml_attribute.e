class AEL_XML_ATTRIBUTE

inherit
	AEL_SPRT_STRING_ROUTINES
		redefine
			default_create
		end

create
	make_with_values, make_from_stream

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_with_values (t, v: STRING)
		require
			tag_exists: t /= Void and then not t.is_empty
			value_exists: v /= Void
		do
			default_create
			tag.wipe_out
			tag.append (t)
			value.wipe_out
			value.append (v)
--			core_make
		end

	--|--------------------------------------------------------------

	make_from_stream (v: STRING; spos: INTEGER)
		require
			exists: v /= Void
			valid_start: spos <= v.count
		do
			default_create
--			core_make
			from_stream (v, spos)
		end

	--|--------------------------------------------------------------

	default_create
		do
			create tag.make (0)
			create value.make (0)
		end

	--|--------------------------------------------------------------

--RFO 	core_make
--RFO 		do
--RFO 		end

	--|--------------------------------------------------------------

	from_stream (v: STRING; startpos: INTEGER)
		require
			exists: v /= Void
		local
			eqpos, spos, epos: INTEGER
			qc: CHARACTER
		do
			eqpos := v.index_of ('=', startpos)
			tag := v.substring (startpos, eqpos - 1)
			if tag.is_equal ("comment") then
				qc := qc;
			end
			qc := '%"'
			spos := v.index_of (qc, eqpos)
			if spos /= 0 then
			else
				qc := '''
				-- single ' throws off hilight and indent
				spos := v.index_of (qc, eqpos)
				if spos = 0 then
					-- ERROR - no quotes
					has_error := True
				end
			end
			spos := spos + 1
			epos := v.index_of (qc, spos)
			if epos < spos then
				-- ERROR, no closing quote
				has_error := True
			elseif epos = spos then
				value := ""
				last_end_position := epos
			else
				value := v.substring (spos, epos - 1)
				last_end_position := epos
			end
		end

--|========================================================================
feature -- Values
--|========================================================================

	tag: STRING
	value: STRING

	last_end_position: INTEGER

	has_error: BOOLEAN

	--|--------------------------------------------------------------
invariant
	has_tag: tag /= Void and then not tag.is_empty
	has_value: value /= Void

end -- class AEL_XML_ATTRIBUTE
