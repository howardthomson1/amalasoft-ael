class AEL_XML_ATTRIBUTE_2

inherit
	AEL_SPRT_ATTRIBUTE

create
	make_with_tag_and_value, make_from_stream

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_from_stream (v: STRING; spos: INTEGER)
		require
			exists: v /= Void
			valid_start: spos <= v.count
		do
			default_create
			core_make
			from_stream (v, spos)
		end

	--|--------------------------------------------------------------

	core_make
		do
		end

	--|--------------------------------------------------------------

	from_stream (v: STRING; startpos: INTEGER)
		require
			exists: v /= Void
		local
			eqpos, spos, epos: INTEGER
			qc: CHARACTER
		do
			eqpos := v.index_of ('=', startpos)
			tag := v.substring (startpos, eqpos - 1)
			if tag.is_equal ("comment") then
				qc := qc;
			end
			qc := '%"'
			spos := v.index_of (qc, eqpos)
			if spos /= 0 then
			else
				qc := '''
				-- single ' throws off hilight and indent
				spos := v.index_of (qc, eqpos)
				if spos = 0 then
					-- ERROR - no quotes
					set_error_message ("Missing closing quote")
				end
			end
			spos := spos + 1
			epos := v.index_of (qc, spos)
			if epos < spos then
				-- ERROR, no closing quote
				set_error_message ("Missing closing quote for value")
			elseif epos = spos then
				value := ""
				last_end_position := epos
			else
				value := v.substring (spos, epos - 1)
				last_end_position := epos
			end
		end

--|========================================================================
feature -- Values
--|========================================================================

--RFO	tag: STRING
--RFO	value: STRING

	last_end_position: INTEGER

--RFO	has_error: BOOLEAN

	--|--------------------------------------------------------------
invariant
--RFO	has_tag: tag /= Void and then not tag.is_empty
--RFO	has_value: value /= Void

end -- class AEL_XML_ATTRIBUTE_2
