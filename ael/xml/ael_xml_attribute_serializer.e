class AEL_XML_ATTRIBUTE_SERIALIZER

inherit
	AEL_SPRT_ATTRIBUTE_SERIALIZER
		redefine
			attribute_by_name, is_valid_input_stream,
			serial_out_prolog, serial_out_epilog, serial_out_prolog_end,
			Shared_flags, Shared_patterns
		end

create
	default_create

--|========================================================================
feature -- Access
--|========================================================================

 	attribute_by_name (al: like attrs; v: STRING): detachable AEL_XML_ATTRIBUTE_2
			-- Attribute name and value associated with label 'v', if any
		do
			if attached {like attribute_by_name} Precursor (al, v) as ta then
				Result := ta
			end
		end

--|========================================================================
feature -- External representation
--|========================================================================

	serial_out_prolog (cl: AEL_SPRT_ATTRIBUTABLE): STRING
			-- String that precedes attributes in serial output
		do
			if attached {AEL_XMLABLE_2} cl as xc then
				Result := left_pad + xc.start_tag
			else
				Result := ""
			end
		end

	serial_out_prolog_end (cl: like client): STRING
 			-- String the terminates the serial_out_prolog,
 			-- after attributes have been added
 		do
			if cl.has_elements then
				Result := ">%N"
			else
				Result := ""
			end
 		end

	serial_out_epilog (cl: like client): STRING
			-- String that follows attributes and elements in serial output
		do
			if attached {AEL_XMLABLE_2} cl as xc then
				Result := xc.xml_end_tag
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	attribute_out (sp: AEL_SPRT_SERIAL_PARAMS; t, v: STRING): STRING
			-- Attribute, represented by tag 't' and value 'v' in
			-- string serialized form
		local
			tv: STRING
		do
			if v = Void then
				tv := ""
			else
				tv := v
			end
			create Result.make (t.count + tv.count + 3)
			Result.append (t)
			Result.append ("=%"")
			Result.append (tv)
			Result.append ("%"")
		end

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_input_stream (v: STRING): BOOLEAN
			-- Is the string 'v' a valid stream from which to initialize 
			-- attributes associated with Current?
		do
			if Precursor (v) then
				Result := True
			end
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	shared_flags: HASH_TABLE [BOOLEAN, STRING]
			-- Flags shared by all instances of this class or its 
			-- descendents
		once
			create Result.make (11)
			--RFO let descendents decide
			--Result.force (True, "include_element_id")
		end

	--|--------------------------------------------------------------

	shared_patterns: HASH_TABLE [STRING, STRING]
			-- Patterns (strings) shared by all instances of this
			-- class or its descendents
		once
			create Result.make (11)
			Result.force ("", "left_pad")
			Result.force ("0", "element_id")
			Result.force ("", "element_separator")
		end

end -- class AEL_XML_ATTRIBUTE_SERIALIZER
