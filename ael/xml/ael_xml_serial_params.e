class AEL_XML_SERIAL_PARAMS

inherit
	AEL_SPRT_SERIAL_PARAMS
		redefine
			initialize_patterns, initialize_flags
		end

create
	default_create

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_patterns
			-- Initialize common and special patterns
		do
			Precursor
			set_pattern ("attribute_separator", " ")
		end

	initialize_flags
			-- Initialize common and special flags
		do
			Precursor
		end

end -- class AEL_XML_SERIAL_PARAMS
