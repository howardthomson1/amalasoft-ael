--|----------------------------------------------------------------------
--| Copyright (c) 1995-2009, All rights reserved by
--| Amalasoft Corporation
--| 273 Harwood Avenue
--| Littleton, MA 01460 USA 
--|
--| See additional information at bottom of file
--|----------------------------------------------------------------------
--| Description
--|
--| An XML tag
--|----------------------------------------------------------------------

class AEL_XML_TAG

inherit
	AEL_SPRT_STRING_ROUTINES
		redefine
			default_create
		end

create
	make, make_with_values, make_from_stream, make_from_substream

--|========================================================================
feature {NONE} -- Create
--|========================================================================

	make
		do
			default_create
		end

	--|--------------------------------------------------------------

	make_with_values
		do
			make
		end

	--|--------------------------------------------------------------

	make_from_stream (v: STRING; spos: INTEGER)
		do
			make
			from_stream (v, spos)
		end

	--|--------------------------------------------------------------

	make_from_substream (v: STRING; spos, epos: INTEGER)
		do
			make
			init_from_substream (v, spos, epos)
		end

	--|--------------------------------------------------------------

	default_create
		do
			create tag_label.make (0)
			create attributes.make (17)
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	from_stream (v: STRING; startpos: INTEGER)
		require
			exists: v /= Void and not v.is_empty
			--is_tag: v.item (1) = '<'
		local
			lim: INTEGER
		do
			lim := index_of_tag_end (v, startpos) - 1
			init_from_substream (v, startpos, lim)
		end

	--|--------------------------------------------------------------

	init_from_substream (v: STRING; startpos, endpos: INTEGER)
		require
			exists: v /= Void and not v.is_empty
			--is_tag: v.item (1) = '<'
		local
			i, lim, spos: INTEGER
			rexp: like K_rexp_xml_attr
			attr: AEL_XML_ATTRIBUTE
		do
			lim := endpos
			spos := index_of_next_whitespace (v, startpos, True)
			if spos = 0 or spos > lim then
				spos := lim + 1
			end
			tag_label := v.substring (startpos + 1, spos - 1)
			if v.item (lim) = '/' then
				-- A closed tag
				is_closed := True
				end_position := lim + 1
			else
				-- An open tag
				create rexp.make_insensitive ("\</" + tag_label + "\>")
				rexp.search_forward (v, spos, v.count)
				if rexp.found then
					end_position := rexp.end_position
					end_of_contents := rexp.start_position - 1
				end
			end

			rexp := K_rexp_xml_attr_tag
			from i := startpos + tag_label.count
			until i > lim
			loop
				rexp.search_forward (v, i, lim)
				if rexp.found then
					spos := rexp.start_position
					create attr.make_from_stream (v, spos)
--RFO 					attributes.extend (attr, attr.tag.as_lower)
					attributes.force (attr, attr.tag.as_lower)
					if attr.last_end_position = 0 then
						i := i
					else
						i := attr.last_end_position
					end
				else
				end
				i := i + 1
			end
			last_parse_position := lim + 1
		end

--|========================================================================
feature -- Queries
--|========================================================================

	value_for_attribute (v: STRING) : STRING
		require
			exists: v /= Void
		local
			attr: detachable AEL_XML_ATTRIBUTE
		do
			attr := attributes.item (v.as_lower)
			if attr /= Void then
				Result := attr.value
			else
				Result := ""
			end
		end

	--|--------------------------------------------------------------

	has_error: BOOLEAN

--|========================================================================
feature -- Attributes
--|========================================================================

	attributes: HASH_TABLE [AEL_XML_ATTRIBUTE,STRING]

	last_parse_position: INTEGER
			-- last index into stream parsed

	end_position: INTEGER
			-- If closed, then index in the stream of the last character
			-- of the opening tag.
			-- If open, then index in the stream of the last character of
			-- the closing tag.

	end_of_contents: INTEGER
			-- index into stream before the corresponding end tag if any
			-- If closed, this is zero

	tag_label: STRING

	is_closed: BOOLEAN

--|========================================================================
feature {NONE} -- Search expressions
--|========================================================================

	K_rexp_xml_closed_tag_end: AEL_SPRT_REGEXP
		once
			create Result.make ("/\>")
		ensure
			exists: Result /= Void
			compiled: Result.compiled
		end

--[a-z]+[a-z0-9_]*=".*"
	K_rexp_xml_attr: AEL_SPRT_REGEXP
		once
			create Result.make_insensitive ("{
[a-z]+\w*=\".*\"
}")
		ensure
			exists: Result /= Void
			compiled: Result.compiled
		end

--[a-z]+[a-z0-9_]*=
	K_rexp_xml_attr_tag: AEL_SPRT_REGEXP
		once
			create Result.make_insensitive ("[a-z]+\w*=")
		ensure
			exists: Result /= Void
			compiled: Result.compiled
		end

end -- class AEL_XML_TAG

--|----------------------------------------------------------------------
--| License
--|
--| This software is furnished under the Eiffel Forum License, version 2,
--| and may be used and copied only in accordance with the terms of that
--| license and with the inclusion of the above copyright notice.
--|
--| Refer to the Eiffel Forum License, version 2 text for specifics.
--|
--| The information in this software is subject to change without notice
--| and should not be construed as a commitment by Amalasoft.
--|
--| Amalasoft assumes no responsibility for the use or reliability of this
--| software.
--|
--|----------------------------------------------------------------------
--| History
--|
--| 002 09-Sep-2010
--|     Changed REGEXP (old matching library) to AEL_SPRT_REGEXP (new
--|     interface to possible different libraries)
--|     Added history
--|----------------------------------------------------------------------
--| How-to
--|
--| Instantiate empty or with content (make_from_stream)
--| Access tag attributes with 'value_for_attribute'
--|----------------------------------------------------------------------
