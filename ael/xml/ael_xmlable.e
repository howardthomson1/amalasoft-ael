deferred class AEL_XMLABLE

inherit
	AEL_SPRT_FALLIBLE

--|========================================================================
feature -- Creation
--|========================================================================

	make_from_tag (t: AEL_XML_TAG)
		require
			exists: t /= Void
			is_valid_tag: is_valid_tag (t)
		do
			private_left_pad := ""
			core_make
			init_from_xml_tag (t)
		end

	--|--------------------------------------------------------------

	core_make
		deferred
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	init_from_xml_tag (t: AEL_XML_TAG)
		require
			exists: t /= Void
			is_valid_tag: is_valid_tag (t)
		do
			attributes := t.attributes
			extract_xml_attributes
		ensure
			valid_count: attributes.count = t.attributes.count
		end

--|========================================================================
feature -- Queries
--|========================================================================

	is_valid_tag (t: AEL_XML_TAG): BOOLEAN
		do
			Result := t /= Void and then t.tag_label.is_equal (tag_label)
		end

	--|--------------------------------------------------------------

	has_attribute_error: BOOLEAN
			-- Has there been an attribute processing error?
		do
			Result := has_error and then error_code = K_attribute_error
		end

	has_tag_error: BOOLEAN
			-- Has there been a tag processing error?
		do
			Result := has_error and then error_code = K_tag_error
		end

	has_filter_tag (v: STRING): BOOLEAN
			-- Does Current have a tag filter with the given string?
		require
			exists: v /= Void
		do
			if attached {CONTAINER [STRING]} tag_filter as tc then
				Result := tc.has (v)
			end
		end

--|========================================================================
feature -- Value setting
--|========================================================================

	extract_xml_attributes
		require
			attrs_exist: attributes /= Void
		deferred
		end

	--|--------------------------------------------------------------

	attribute_value (v: STRING): STRING
			-- Value for attribute of given label, Void if nonexistent
		local
			ta: AEL_XML_ATTRIBUTE
		do
			ta := attributes.item (v)
			if ta /= Void then
				Result := ta.value
			end
		end

	--|--------------------------------------------------------------

	required_attribute_value (v: STRING): STRING
			-- Value for attribute of given label
			-- If nonexistent, then set has_attribute_error
		do
			Result := attribute_value (v)
			if Result = Void or else Result.is_empty then
				set_attribute_error ("Required attribute missing")
			end
		ensure
			no_error_exists: (not has_attribute_error) implies Result /= Void
			error_no_exists: Result = Void implies has_attribute_error
		end

--|========================================================================
feature -- External representation
--|========================================================================

	tag_label: STRING
		deferred
		end

	elements_tag_label: STRING
		do
			Result := "elements"
		end

	--|--------------------------------------------------------------

	to_xml: STRING
			-- XML element representing Current
		require
			supports_xml_out: xml_out_supported
		do
			if attributes = Void then
				set_attributes_from_current_values
			end
			create Result.make (128)
			Result.append (to_xml_tag)
			if has_elements then
				Result.append (elements_to_xml)
			end
			Result.append (xml_end_tag)
		end

	--|--------------------------------------------------------------

	to_xml_tag_filtered (ll: like tag_filter): STRING
			-- XML element representing Current, but with only
			-- those attributes matching the labels in 'll'
		require
			supports_xml_out: xml_out_supported
			labels_exist: ll /= Void
		do
			tag_filter := ll
			Result := to_xml_tag
			tag_filter := Void
		end

	--|--------------------------------------------------------------

	to_xml_tag: STRING
			-- XML tag representing Current
		do
			if attributes = Void then
				set_attributes_from_current_values
			end
			create Result.make (128)
			Result.append (xml_start_tag)
			if tag_filter /= Void then
				Result.append (attributes_by_tag_to_xml (tag_filter))
			else
				Result.append (attributes_to_xml)
			end
			if has_elements then
				Result.append (">%N")
			else
				Result.append ("/>%N")
			end
		end

	xml_start_tag: STRING
		-- Start tag with opening bracket and label, but without
		-- closing bracket
		do
			create Result.make (32)
			Result.append (left_pad)
			Result.extend ('<')
			Result.append (tag_label)
			Result.extend (' ')
		end

	xml_end_tag: STRING
		do
			create Result.make (tag_label.count + left_pad.count + 3)
			if has_elements then
				Result.append (left_pad)
				Result.append ("</")
				Result.append (tag_label)
				Result.append (">%N")
			end
		end

	--|--------------------------------------------------------------

	elements_start_tag: STRING
			-- Start tag with open+close brackets and label,
			-- for sequence of elements subordinate to Current
		require
			has_elements: has_elements
		local
			lp: STRING
		do
			lp := left_pad.twin
			lp.extend (' ')
			create Result.make (elements_tag_label.count + lp.count + 3)
			Result.append (lp)
			Result.extend ('<')
			Result.append (elements_tag_label)
			Result.append (">%N")
		end

	elements_end_tag: STRING
			-- End tag with open+close brackets and label,
			-- for sequence of elements subordinate to Current
		require
			has_elements: has_elements
		local
			lp: STRING
		do
			lp := left_pad.twin
			lp.extend (' ')
			create Result.make (elements_tag_label.count + lp.count + 3)
			Result.append (lp)
			Result.append ("</")
			Result.append (elements_tag_label)
			Result.append (">%N")
		end

	--|--------------------------------------------------------------

	attributes: HASH_TABLE [AEL_XML_ATTRIBUTE,STRING]
			-- Attributes from corresponding XML tag, if any

	--|--------------------------------------------------------------

	attributes_to_xml : STRING
		require
			supports_xml_out: xml_out_supported
			attrs_exists: attributes /= Void
		local
			ta: AEL_XML_ATTRIBUTE
		do
			create Result.make (128)
			from attributes.start
			until attributes.after
			loop
				ta := attributes.item_for_iteration
				Result.append (ta.tag + "=%"" + ta.value + "%" ")
				attributes.forth
			end
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	attributes_by_tag_to_xml (ll: TO_SPECIAL [STRING]): STRING
			-- Attributes corresponding to the given labels
		require
			labels_exist: ll /= Void
			supports_xml_out: xml_out_supported
			attrs_exists: attributes /= Void
		local
			i, lim: INTEGER
			tl, tv: STRING
		do
			if not attached {FINITE [STRING]} ll as fa then
				-- Internal error
			else
				create Result.make (128)
				lim := fa.count
				from i := 1
				until i > lim
				loop
					tl := ll.item (i)
					tv := attribute_string_for_value (tl)
					if tv = Void then
						tv := ""
					end
					Result.append (tl + "=%"" + tv + "%" ")
					i := i + 1
				end
			end
		ensure
			exists: Result /= Void
		end

	attribute_string_for_value (v: STRING): STRING
			--
		require
			label_exist: v /= Void
			supports_xml_out: xml_out_supported
			attrs_exists: attributes /= Void
		do
			Result := attribute_value (v)
		end


	--|--------------------------------------------------------------

	elements_to_xml: STRING
		require
			supports_xml_out: xml_out_supported
		do
		ensure
			exists: Result /= Void
		end

	--|--------------------------------------------------------------

	has_elements: BOOLEAN
			-- Does this tag have any elements (currently) ?
		require
			supports_xml_out: xml_out_supported
		do
		end

	--|--------------------------------------------------------------

	xml_out_supported: BOOLEAN
		do
		end

	--|--------------------------------------------------------------

	left_pad: STRING
			-- Padding string to use when generating XML output
		do
			Result := private_left_pad
			if Result = Void then
				Result := ""
			end
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Attribute setting
--|========================================================================

	set_attributes_from_current_values
		do
			if attributes = Void then
				create attributes.make (11)
			end
			attributes.wipe_out
		ensure
			attrs_set: attributes /= Void
		end

	--|--------------------------------------------------------------

	add_xml_attribute (tag, value: STRING)
		require
			tag_exists: tag /= Void and then not tag.is_empty
			attributes_exist: attributes /= Void
		local
			ts: STRING
		do
			if value = Void then
				ts := ""
			else
				ts := value
			end
			attributes.extend (
				create {AEL_XML_ATTRIBUTE}.make_with_values (tag,ts), tag)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_attribute_error (msg: STRING)
		require
			message_exists: msg /= Void
		do
			set_error (K_attribute_error, msg)
		end

	set_tag_error (msg: STRING)
		require
			message_exists: msg /= Void
		do
			set_error (K_tag_error, msg)
		end

	--|--------------------------------------------------------------

	set_left_pad (v: STRING)
		do
			private_left_pad := v
		end

	--|--------------------------------------------------------------

	set_tag_filter (v: like tag_filter)
		do
			tag_filter := v
		end

	--|--------------------------------------------------------------

	set_element_tag_filter (v: like tag_filter)
		do
			element_tag_filter := v
		end

	tag_filter: TO_SPECIAL [STRING]
	element_tag_filter: TO_SPECIAL [STRING]

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_left_pad: STRING

	tag_lookup_function: FUNCTION [STRING, STRING]
			-- Function to call to find attribute value that is not
			-- otherwise directly accessible from 'attributes'
			-- Useful for derived values and custom representations
		do
		end

	K_tag_error: INTEGER = 1501
	K_attribute_error: INTEGER = 1502

end -- class AEL_XMLABLE
