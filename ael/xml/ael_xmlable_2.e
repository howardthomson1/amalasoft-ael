deferred class AEL_XMLABLE_2

inherit
	AEL_SPRT_ATTRIBUTABLE
		rename
			core_make as attr_core_make,
			serial_out_supported as xml_out_supported
		redefine
			serial_elements, make_elements, initialize_serial_params
		end

--|========================================================================
feature -- Creation
--|========================================================================

	make_from_tag (t: AEL_XML_TAG)
		require
			exists: t /= Void
			is_valid_tag: is_valid_tag (t)
		do
			attr_core_make
			core_make
			init_from_xml_tag (t)
		end

	--|--------------------------------------------------------------

	core_make
		deferred
		end

--|========================================================================
feature {NONE} -- Private initialization
--|========================================================================

	make_serial_params
		do
			create private_xml_params
		end

	make_serializer
		do
			create private_xml_serializer
		end

	make_elements
		do
			create private_xml_elements.make
			Precursor
		end

--|========================================================================
feature -- Initialization
--|========================================================================

	initialize_serial_params
		do
			serial_params.set_attribute_separator (" ")
		end

	--|--------------------------------------------------------------

	init_from_xml_tag (t: AEL_XML_TAG)
		require
			made: default_created
			exists: t /= Void
			is_valid_tag: is_valid_tag (t)
		local
			ta: AEL_XML_ATTRIBUTE
			oc: CURSOR
		do
			if attached attribute_values as la then
				la.wipe_out
			else
				create attribute_values.make (7)
			end
			if attached attribute_values as la and attached t.attributes as al then
				oc := al.cursor
				from al.start
				until al.after
				loop
					ta := al.item_for_iteration
					la.force (ta.value, ta.tag)
					al.forth
				end
				al.go_to (oc)
			end
			extract_attributes
		ensure
			has_attributes: attribute_values /= Void
			same_count: attribute_values.count = t.attributes.count
		end

	--|--------------------------------------------------------------

	init_from_substream (v: STRING; spos, epos: INTEGER)
			-- An XML stream is nested collected of XML tags, each
			-- of which may have some number of attributes
		local
			t: AEL_XML_TAG
		do
			create t.make_from_substream (v, spos, epos)
			init_from_xml_tag (t)
		end

--|========================================================================
feature -- Status
--|========================================================================

	has_attribute_error: BOOLEAN
			-- Has there been an attribute processing error?
		do
			Result := has_error and then last_error_code = K_attribute_error
		end

	has_tag_error: BOOLEAN
			-- Has there been a tag processing error?
		do
			Result := has_error and then last_error_code = K_tag_error
		end

	--|--------------------------------------------------------------

	serializer: AEL_SPRT_ATTRIBUTE_SERIALIZER
			-- Serializing service for Current
		do
			check attached private_xml_serializer as pxs then
				Result := pxs
			end
		end

	serial_params: AEL_SPRT_SERIAL_PARAMS
			-- Parameters directing serialization
		do
			check attached private_xml_params as pxp then
				Result := pxp
			end
		end

	--|--------------------------------------------------------------

	serial_elements: CONTAINER [AEL_SPRT_ATTRIBUTABLE]
			-- Serializable subordinate elements, if any
		do
			check attached private_xml_elements as pxe then
				Result := pxe
			end
		end

--|========================================================================
feature -- External representation
--|========================================================================

	to_xml: STRING
			-- XML element representing Current
		require
			supports_xml_out: xml_out_supported
		do
			Result := serializer.serial_out (Current, serial_params)
		ensure
			exists: xml_out_supported implies Result /= Void
		end

	--|--------------------------------------------------------------

	to_xml_tag_filtered (ll: LIST [STRING]): STRING
			-- XML element representing Current, but with only
			-- those attributes matching the labels in 'll'
		require
			supports_xml_out: xml_out_supported
			labels_exist: ll /= Void
		do
			Result := serial_out_filtered (ll)
		end

	--|--------------------------------------------------------------

	to_xml_tag: STRING
			-- XML tag (element) representing Current
		local
			ts: STRING
		do
			if attribute_values = Void then
				set_attributes_from_current_values
			end
			create Result.make (128)
			Result.append (start_tag)
			if attached tag_filter as ltf then
				Result.append (attributes_out_by_tag (ltf))
			elseif attached attribute_values as al then
				from al.start
				until al.after
				loop
					ts := al.item_for_iteration
					Result.append (
						serializer.attribute_out (serial_params, al.key_for_iteration, ts))
					al.forth
				end
			end
			if has_elements then
				Result.append (">%N")
			else
				Result.append ("/>%N")
			end
		end

	--|--------------------------------------------------------------

	start_tag: STRING
		-- Start tag with opening bracket and label, but without
		-- closing bracket
		do
			create Result.make (32)
			Result.extend ('<')
			Result.append (tag_label)
			Result.extend (' ')
		end

	xml_end_tag: STRING
		do
			create Result.make (tag_label.count + 16)
			if has_elements then
				Result.append ("</")
				Result.append (tag_label)
				Result.append (">%N")
			else
				Result.append ("/>%N")
			end
		end

	--|--------------------------------------------------------------

	attribute_string_for_value (v: STRING): STRING
			--
		require
			label_exist: v /= Void
			supports_xml_out: xml_out_supported
			attrs_exists: attribute_values /= Void
		do
			Result := value_for_attribute (v)
		end


	--|--------------------------------------------------------------

	elements_to_xml: STRING
		require
			supports_xml_out: xml_out_supported
		do
			Result := ""
		ensure
			exists: Result /= Void
		end

--|========================================================================
feature -- Attribute setting
--|========================================================================

	add_xml_attribute (tag, value: STRING)
		require
			tag_exists: tag /= Void and then not tag.is_empty
			attributes_exist: attribute_values /= Void
--RFO 		local
--RFO 			ts: STRING
		do
--RFO 			if value = Void then
--RFO 				ts := ""
--RFO 			else
--RFO 				ts := value
--RFO 			end
--RFO 			attributes.extend (value, tag)
--RFO 		ensure
--RFO 			added: attributes.has (tag)
			add_attribute (tag, value)
		end

--|========================================================================
feature -- Status setting
--|========================================================================

	set_attribute_error (msg: STRING)
		require
			message_exists: msg /= Void
		do
			set_error (K_attribute_error, msg)
		end

	set_tag_error (msg: STRING)
		require
			message_exists: msg /= Void
		do
			set_error (K_tag_error, msg)
		end

	--|--------------------------------------------------------------

	set_element_tag_filter (v: like tag_filter)
		do
			element_tag_filter := v
		end

	--|--------------------------------------------------------------

	element_tag_filter: detachable LIST [STRING]

--|========================================================================
feature -- Validation
--|========================================================================

	is_valid_tag (t: AEL_XML_TAG): BOOLEAN
		do
			Result := t /= Void and then t.tag_label.is_equal (tag_label)
		end

--|========================================================================
feature {NONE} -- Implementation
--|========================================================================

	private_xml_serializer: detachable AEL_XML_ATTRIBUTE_SERIALIZER
			-- Serializing service for Current
		note
			option: stable
			attribute
		end

	private_xml_params: detachable AEL_XML_SERIAL_PARAMS
			-- Serialization parameters for Current
		note
			option: stable
			attribute
		end

	private_xml_elements: detachable LINKED_LIST [AEL_XMLABLE_2]
			-- Subordinate elements of Current (if any)
		note
			option: stable
			attribute
		end

	K_tag_error: INTEGER = 1501
	K_attribute_error: INTEGER = 1502

end -- class AEL_XMLABLE_2
