note
	description: "{
Application name, shared by all classes in this system
}"
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class APPLICATION_NAME

--|========================================================================
feature -- Status
--|========================================================================

	application_name: STRING = "ael_v2_test"
		--| Name of actual application

	--|--------------------------------------------------------------
invariant
	invariant_clause: True -- Your invariant here

--|----------------------------------------------------------------------
--| History
--|
--| 001 01-Dec-2013
--|     Created original module (for Eiffel 7.2, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--| Edit the 'application_name' feature to replace 'aelvapp' with 
--| the name of your application.
--|
--|----------------------------------------------------------------------

end -- class APPLICATION_NAME
