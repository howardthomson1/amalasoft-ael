class CAL_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			create_interface_objects,
			window_initial_height, window_initial_width
		end

create
	make, make_with_title

--|========================================================================
feature {NONE}
--|========================================================================

	create_interface_objects
		do
			Precursor
			create calbox.make
		end

	fill_working_area
		do
			working_area.extend (calbox)
		end

	calbox: AEL_V2_CALENDAR_BOX

	window_initial_width: INTEGER
		do 
			Result := 450
		end

	window_initial_height: INTEGER
		do
			Result := 250
		end

end -- class CAL_DIALOG
