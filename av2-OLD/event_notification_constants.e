
deferred class EVENT_NOTIFICATION_CONSTANTS

inherit
	ANY
		undefine
			default_create, copy
		end

 --|========================================================================
feature {NONE} -- Constants
 --|========================================================================

	K_evid_none: INTEGER = 0
	K_evid_all: INTEGER = 99

	K_evid_change: INTEGER = 1

end -- class EVENT_NOTIFICATION_CONSTANTS
