class GRAPH_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			window_initial_height, window_initial_width,
			create_interface_objects
		end

create
	make_with_title

--|========================================================================
feature {NONE}
--|========================================================================

	create_interface_objects
		do
			Precursor
			create hbar_graph.make
			create vbar_graph.make
			create line_graph.make
			create combo_graph.make
		end

	fill_working_area
		local
			--tc: EV_CELL
			--lb: EV_LABEL
			sep: EV_HORIZONTAL_SEPARATOR
		do
			working_area.extend (create {EV_CELL})

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (hbar_graph)
			hbar_graph.set_minimum_height (150)
			working_area.disable_item_expand (hbar_graph)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (vbar_graph)
			vbar_graph.set_minimum_height (150)
			working_area.disable_item_expand (vbar_graph)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (line_graph)
			line_graph.set_minimum_height (150)
			working_area.disable_item_expand (line_graph)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (combo_graph)
			combo_graph.set_minimum_height (150)
			working_area.disable_item_expand (combo_graph)

			--------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (create {EV_CELL})
		end

	--|--------------------------------------------------------------

	hbar_graph: AEL_V2_HORIZONTAL_BAR_GRAPH
	vbar_graph: AEL_V2_VERTICAL_BAR_GRAPH
	line_graph: AEL_V2_LINE_GRAPH
	combo_graph: AEL_V2_BAR_AND_LINE_GRAPH

	window_initial_height: INTEGER = 500
	window_initial_width: INTEGER = 500

end -- class GRAPH_DIALOG
