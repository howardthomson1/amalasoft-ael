class PW_DIALOG

inherit
	AEL_V_DIALOG
		redefine
			K_height, K_width
		end

create
	make, make_with_title

--|========================================================================
feature {NONE}
--|========================================================================

	fill_working_area
		local
			hb: EV_HORIZONTAL_BOX
			tc: EV_CELL
		do
			working_area.extend (create {EV_CELL})

			create hb
			working_area.extend (hb)
			hb.set_background_color (colors.teal)
--			hb.set_padding_width (5)
			hb.set_border_width (5)
			hb.set_minimum_height (30)
			working_area.disable_item_expand (hb)

			create tc
			hb.extend (tc)
			tc.set_minimum_width (20)

			create pw_textw.make_with_label ("Password:", True)
			hb.extend (pw_textw)
			pw_textw.set_widget_width (150)

			create tc
			hb.extend (tc)
			tc.set_minimum_width (20)

			working_area.extend (create {EV_CELL})
		end

	pw_textw: AEL_V_LABELED_PW_TEXT

	K_height: INTEGER = 250
	K_width: INTEGER = 450

--|========================================================================
feature -- Add Comment
--|========================================================================

	pw_text: STRING
		do
			Result := pw_textw.text
		end

	set_notification_procedure (v: PROCEDURE [ANY, TUPLE [like pw_textw]])
		do
			pw_textw.set_notification_procedure (v)
		end

end -- class PW_DIALOG
