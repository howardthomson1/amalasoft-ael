
class APPLICATION_ROOT

inherit
	AEL_V2_APPLICATION
--RFO 		redefine
--RFO 			attach_application_root, create_main_window
--RFO 		end

create
	make_and_launch
--default_create, 
create {AEL_APPLICATION_ENV}
	make_invalid

--|========================================================================
feature {NONE} -- Core initialization
--|========================================================================

--RFO 	attach_application_root
--RFO 			-- Force attachement at earliest opportunity (from initialize)
--RFO 		do
--RFO 			Precursor
--RFO 			application_root_cell.put (Current)
--RFO 		end

--RFO 	create_main_window
--RFO 			-- Create the main_window object
--RFO 		do
--RFO 			create main_window.make
--RFO 		end

end -- class APPLICATION_ROOT
