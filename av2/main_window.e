
class
	MAIN_WINDOW

inherit
	AEL_V2_MAIN_WINDOW
		redefine
			make, create_interface_objects,
			build_interface_components, initialize_interface_actions,
			update_widget_rendition, initialize_interface_strings,
			window_initial_width, window_initial_height,
			request_close_window
		end
	AEL_V2_NOTIFY_CLIENT
		undefine
			default_create, copy
		redefine
			respond_to_notification
		end
	EVENT_NOTIFICATION_CONSTANTS
	INTERFACE_NAMES
		undefine
			default_create, copy
		end

create
	make

create {AEL_V2_APPLICATION}
	default_create

--|========================================================================
feature {NONE} -- Creation
--|========================================================================

	make
		do
			Precursor
			-- Set up (non-windowing) event notification
			make_as_notifier_client
		end

 --|========================================================================
feature {NONE} -- Initialization
 --|========================================================================

	create_interface_objects
		do
			current_color := colors.gold

			-- Create and add the menu bar.
			create standard_menu_bar
			create file_menu.make_with_text (Menu_file_item)

			create options_menu.make_with_text (Menu_option_item)
			create help_menu.make_with_text (Menu_help_item)
			create standard_toolbar
			create standard_status_bar
			create standard_status_label.make_with_text (
				"Add your status text here...")

			create main_container
			Precursor
		end

	--|--------------------------------------------------------------

	build_interface_components
		do
			build_standard_menu_bar
			set_menu_bar (standard_menu_bar)

			build_standard_toolbar
			upper_bar.extend (create {EV_HORIZONTAL_SEPARATOR})
			upper_bar.extend (standard_toolbar)

			build_standard_status_bar
			lower_bar.extend (standard_status_bar)

			build_main_container
			extend (main_container)
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			Precursor

				-- Execute `request_close_window' when the user clicks
				-- on the cross in the title bar.
			close_request_actions.extend (agent request_close_window)
		end

	--|--------------------------------------------------------------

	initialize_interface_strings
			-- Initialize strings for interface components
		do
			-- Set the title of the window
			set_title (Window_title)
		end

	--|--------------------------------------------------------------

	--RFO is_in_default_state: BOOLEAN
	--RFO 		-- Is the window in its default state
	--RFO 		-- (as stated in `initialize')
	--RFO 	do
	--RFO 		Result := (width = K_window_initial_width) and then
	--RFO 			(height = K_window_initial_height) and then
	--RFO 			(title.is_equal (Window_title))
	--RFO 	end

 --|========================================================================
feature {NONE} -- Menu Implementation
 --|========================================================================

	build_standard_menu_bar
			-- Create and populate `standard_menu_bar'.
		require
			menu_bar_not_yet_created: standard_menu_bar /= Void
		do
				-- Add the "File" menu
			build_file_menu
			standard_menu_bar.extend (file_menu)

			build_extra_menus

				-- Add the "Help" menu
			build_help_menu
			standard_menu_bar.extend (help_menu)
		ensure
			menu_bar_created:
				standard_menu_bar /= Void and then
				not standard_menu_bar.is_empty
		end

	--|--------------------------------------------------------------

	build_file_menu
			-- Create and populate `file_menu'.
		require
			file_menu_not_yet_created: file_menu /= Void
		local
			menu_item: EV_MENU_ITEM
		do
				--| TODO: Add the action associated with "New" here.
			create menu_item.make_with_text (Menu_file_new_item)
			file_menu.extend (menu_item)

				--| TODO: Add the action associated with "Open" here.
			create menu_item.make_with_text (Menu_file_open_item)
			file_menu.extend (menu_item)

				--| TODO: Add the action associated with "Save" here.
			create menu_item.make_with_text (Menu_file_save_item)
			file_menu.extend (menu_item)

				--| TODO: Add the action associated with "Save As..." here.
			create menu_item.make_with_text (Menu_file_saveas_item)
			file_menu.extend (menu_item)

				--| TODO: Add the action associated with "Close" here.
			create menu_item.make_with_text (Menu_file_close_item)
			file_menu.extend (menu_item)

			create menu_item.make_with_text (Menu_file_exit_item)
			file_menu.extend (create {EV_MENU_SEPARATOR})

				-- Create the File/Exit menu item and make it call
				-- `request_close_window' when it is selected.
			menu_item.select_actions.extend (agent request_close_window)
			file_menu.extend (menu_item)
		ensure
			file_menu_created: file_menu /= Void and then not file_menu.is_empty
		end

	--|--------------------------------------------------------------

	build_help_menu
			-- Create and populate `help_menu'.
		require
			help_menu_exists: help_menu /= Void
		local
			menu_item: EV_MENU_ITEM
		do
				--| TODO: Add the action associated with "Contents and Index" here.
			create menu_item.make_with_text (Menu_help_contents_item)
			help_menu.extend (menu_item)

			create menu_item.make_with_text (Menu_help_about_item)
			menu_item.select_actions.extend (agent on_about)
			help_menu.extend (menu_item)

		ensure
			help_menu_created: help_menu /= Void and then not help_menu.is_empty
		end

	--|--------------------------------------------------------------

	build_extra_menus
		local
			menu_item: EV_MENU_ITEM
		do
			standard_menu_bar.extend (options_menu)
			create menu_item.make_with_text (Menu_option_calendar_item)
			menu_item.select_actions.extend (agent on_calendar_select)
			options_menu.extend (menu_item)

--RFO 			create menu_item.make_with_text (Menu_option_pw_item)
--RFO 			menu_item.select_actions.extend (agent on_pw_select)
--RFO 			options_menu.extend (menu_item)

			create menu_item.make_with_text (Menu_option_misc_item)
			menu_item.select_actions.extend (agent on_misc_select)
			options_menu.extend (menu_item)

			create menu_item.make_with_text (Menu_option_text_item)
			menu_item.select_actions.extend (agent on_text_select)
			options_menu.extend (menu_item)

			create menu_item.make_with_text (Menu_option_graph_item)
			menu_item.select_actions.extend (agent on_graph_select)
			options_menu.extend (menu_item)
		end

 --|========================================================================
feature {NONE} -- ToolBar Implementation
 --|========================================================================

	build_standard_toolbar
			-- Create and populate the standard toolbar.
		require
			toolbar_exists: standard_toolbar /= Void
		local
			toolbar_item: EV_TOOL_BAR_BUTTON
		do

			create toolbar_item
			toolbar_item.set_pixmap (create {AEL_V2_NEW_PIXMAP} .make)
			standard_toolbar.extend (toolbar_item)

			create toolbar_item
			toolbar_item.set_pixmap (create {AEL_V2_OPEN_PIXMAP} .make)
			standard_toolbar.extend (toolbar_item)

			create toolbar_item
			toolbar_item.set_pixmap (create {AEL_V2_SAVE_PIXMAP} .make)
			standard_toolbar.extend (toolbar_item)
		ensure
			toolbar_created: standard_toolbar /= Void and then  not standard_toolbar.is_empty
		end

 --|========================================================================
feature {NONE} -- StatusBar Implementation
 --|========================================================================

	build_standard_status_bar
			-- Create and populate the standard toolbar.
		require
			status_bar_exists: standard_status_bar /= Void
		do
			standard_status_bar.set_border_width (2)

				-- Populate the status bar.
			standard_status_label.align_text_left
			standard_status_bar.extend (standard_status_label)
		ensure
			status_bar_created:
				standard_status_bar /= Void and then
				standard_status_label /= Void
		end

 --|========================================================================
feature {NONE} -- Agents and actions
 --|========================================================================

	on_about
			-- Display the About dialog.
		local
			about_dialog: ABOUT_DIALOG
		do
			create about_dialog.make
			about_dialog.show_modal_to_window (Current)
		end

	on_calendar_select
		local
			cal: CAL_DIALOG
		do
			create cal.make_with_title ("Calendar")
			cal.show_modal_to_window (Current)
		end

--RFO 	on_pw_select
--RFO 		local
--RFO 			td: PW_DIALOG
--RFO 		do
--RFO 			create td.make_with_title ("Password")
--RFO 			td.set_notification_procedure (agent on_pw_commit)
--RFO 			td.show_modal_to_window (Current)
--RFO 		end
--RFO 
--RFO 	on_pw_commit (pw: AEL_V2_LABELED_PW_TEXT)
--RFO 		local
--RFO 			td: EV_INFORMATION_DIALOG
--RFO 		do
--RFO 			create td.make_with_text (pw.text)
--RFO 			td.show_modal_to_window (Current)
--RFO 		end

	on_misc_select
		local
			td: MISC_DIALOG
			lb: EV_LABEL
		do
			create lb
			create td.make_with_values (current_color, lb.font)
			td.show_modal_to_window (Current)
		end

	on_text_select
		local
			td: AEL_V2_TEXT_DIALOG
		do
			create td.make_with_title ("Text Dialog")
			td.enable_save
			td.show_modal_to_window (Current)
		end

	on_graph_select
		local
			td: GRAPH_DIALOG
		do
			create td.make_with_title ("Bar Graph")
			td.show_modal_to_window (Current)
		end

 --|========================================================================
feature {NONE} -- Implementation, Close event
 --|========================================================================

	request_close_window
			-- The user wants to close the window
		local
			question_dialog: EV_CONFIRMATION_DIALOG
		do
			create question_dialog.make_with_text (Label_confirm_close_window)
			question_dialog.show_modal_to_window (Current)

			if question_dialog.selected_button ~ "OK" then
					-- Destroy the window
				destroy;

					-- End the application
					--| TODO: Remove this line if you don't want the application
					--|       to end when the first window is closed..
				--(create {EV_ENVIRONMENT}).application.destroy
				application_root.destroy
			end
		end

 --|========================================================================
feature {NONE} -- Implementation
 --|========================================================================

	build_main_container
			-- Create and populate `main_container'.
		require
			main_container_exists: main_container /= Void
		do
			main_container.extend (create {EV_TEXT})
		ensure
			main_container_created: main_container /= Void
		end

--|========================================================================
feature {NONE} -- Components
--|========================================================================

	main_container: EV_VERTICAL_BOX
			-- Main container (contains all widgets displayed in this window)

	standard_status_bar: EV_STATUS_BAR
			-- Standard status bar for this window

	standard_status_label: EV_LABEL
			-- Label situated in the standard status bar.
			--
			-- Note: Call `standard_status_label.set_text (...)' to change the text
			--       displayed in the status bar.

	standard_toolbar: EV_TOOL_BAR
			-- Standard toolbar for this window

	standard_menu_bar: EV_MENU_BAR
			-- Standard menu bar for this window.

	file_menu: EV_MENU
			-- "File" menu for this window (contains New, Open, Close, Exit...)

	help_menu: EV_MENU
			-- "Help" menu for this window (contains About...)

	options_menu: EV_MENU
			-- "Options" menu for this window (contains About...)

	current_color: EV_COLOR

--|========================================================================
feature -- All vision widgets, to force compilation
--|========================================================================

	cbutton: detachable AEL_V2_COLOR_BUTTON
	fbutton: detachable AEL_V2_FONT_BUTTON
	lb_combo: detachable AEL_V2_LABELED_COMBO_BOX
	lb_list: detachable AEL_V2_LABELED_LIST
	lb_mline_text: detachable AEL_V2_LABELED_MULTILINE_TEXT
	lb_positioner: detachable AEL_V2_LABELED_POSITIONER
	lb_spinb: detachable AEL_V2_LABELED_SPIN_BUTTON
	lb_text: detachable AEL_V2_LABELED_TEXT
	plist: detachable AEL_V2_PRIORITY_LIST
	rlist: detachable AEL_V2_RANKING_LIST
	tdialog: detachable AEL_V2_TEXT_DIALOG
	vtext: detachable AEL_V2_VALIDATING_TEXT
	vbar: detachable AEL_V2_VERTICAL_TOOL_BAR
	wizd: detachable AEL_V2_WIZARD_DIALOG

	calbox: detachable AEL_V2_CALENDAR_BOX

	x001: detachable AEL_V2_AEL_APP_PIXMAP
	x002: detachable AEL_V2_APP_PIXMAP
	x003: detachable AEL_V2_ARROW_DOWN_PIXMAP
	x004: detachable AEL_V2_ARROW_PIXMAP
	x005: detachable AEL_V2_ARROW_UP_PIXMAP
	x006: detachable AEL_V2_BOXES_APP_PIXMAP
	x007: detachable AEL_V2_CALENDAR_BUTTON
	x008: detachable AEL_V2_CALENDAR_DIALOG
	x009: detachable AEL_V2_CALENDAR_PIXMAP
	x010: detachable AEL_V2_CASING_TEXT
	x011: detachable AEL_V2_CENTER_PIXMAP
	x012: detachable AEL_V2_CLONE_GRAY_PIXMAP
	x013: detachable AEL_V2_CLONE_PIXMAP
	x014: detachable AEL_V2_DELETE_PIXMAP
	--x015: detachable AEL_V2_EB_MAIN_WINDOW
	x016: detachable AEL_V2_FIT_PIXMAP
	x017: detachable AEL_V2_LABELED_PW_TEXT
	x019: detachable AEL_V2_LOGIN_BOX
	x020: detachable AEL_V2_MULTI_COLUMN_LIST
	x021: detachable AEL_V2_REDO_PIXMAP
	--x022: detachable AEL_V2_SEGMENT_CTL
	x023: detachable AEL_V2_TB_INFO_PIXMAP
	x024: detachable AEL_V2_UNDO_PIXMAP
	x025: detachable AEL_V2_GRID_ROW_DATA
	x026: detachable AEL_V2_FRAMED_VERTICAL_BOX

--|========================================================================
feature -- Rendition support
--|========================================================================

	update_widget_rendition
			-- Update the states of widgets, like sensitivity, based on
			-- the current state of the application
			-- Redefine in child as needed, but be sure to include Precursor
			-- call.
			-- Note well that this cannot be called directly or indirectly
			-- from initialization routines other than finish_preparation
			-- due to the precondition.
		do
		end

--|========================================================================
feature {NONE} -- Event notification support
--|========================================================================

	respond_to_notification (v: INTEGER)
			-- For the given event ID, react as needed
			-- Events come from the various components changing their states
		do
		end

	--|------------------------------------------------------------------------

	post_changes
			-- Notify clients that changes have occurred
		do
			event_notifier.post_events_for_notification(
				<< K_evid_change >>)
		end

 --|========================================================================
feature {NONE} -- Implementation / Constants
 --|========================================================================

	Window_title: STRING = "vision_test"
			-- Title of the window.

	window_initial_width: INTEGER
			-- Initial window width; redefine in child as desired
		do 
			Result := 400
		end

	window_initial_height: INTEGER
			-- Initial window height; redefine in child as desired
		do
			Result := 400
		end
end
