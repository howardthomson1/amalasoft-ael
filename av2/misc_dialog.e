class MISC_DIALOG

inherit
	AEL_V2_DIALOG
		redefine
			window_initial_height, window_initial_width,
			initialize_interface_actions, create_interface_objects
		end

create
	make_with_values

--|========================================================================
feature {NONE} -- Add Comment
--|========================================================================

	make_with_values (c: like color; f: like orig_font)
		do
			color := c
			orig_font := f
			make_with_title ("Miscellaneous")
		end

	--|--------------------------------------------------------------

	initialize_interface_actions
		do
			--control_frame.set_ok_action_proc (agent execute_ok_action)
			Precursor
		end

--|========================================================================
feature {NONE}
--|========================================================================

	create_interface_objects
		do
			Precursor
			create color_box
			create color_btn.make_with_attributes ("Color:", color, True)
			create color_label.make_with_text ("Current")
			create font_box
			create font_btn.make_with_attributes ("Font:", orig_font, True)
			create font_label.make_with_text ("Current")
			create positioner_box
			create positioner_label.make_with_text ("Current")
			create positioner.make_with_label ("Position", True)
			create vtext_box
			create vtextw.make_for_upper ("Validating Text (Upper)", True)
			create vtext_toggle.make_with_text ("Integers Only?")
			create pr_list.make
			create r_list.make
		end

	fill_working_area
		local
			tc: EV_CELL
			lb: EV_LABEL
			sep: EV_HORIZONTAL_SEPARATOR
			dflt_font, bold_font: EV_FONT
		do
			working_area.extend (create {EV_CELL})

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (color_box)
			color_box.set_minimum_height (30)
			working_area.disable_item_expand (color_box)

			create tc
			color_box.extend (tc)
			tc.set_minimum_width (10)

			create lb.make_with_text ("Original")
			color_box.extend (lb)
			lb.set_background_color (color)
dflt_font := lb.font
bold_font := lb.font.twin
bold_font.set_weight ({EV_FONT_CONSTANTS}.Weight_bold)
lb.set_font (bold_font)
			create tc
			color_box.extend (tc)
			tc.set_minimum_width (10)

			color_box.extend (color_btn)
			color_btn.set_notification_procedure (agent update_color)

			create tc
			color_box.extend (tc)
			tc.set_minimum_width (10)

			color_box.extend (color_label)
			color_label.set_background_color (color)

			create tc
			color_box.extend (tc)
			tc.set_minimum_width (10)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (font_box)
--			font_box.set_minimum_height (50)
--			working_area.disable_item_expand (font_box)

			create tc
			font_box.extend (tc)
			tc.set_minimum_width (10)

			create lb.make_with_text ("Original")
			font_box.extend (lb)
			lb.set_font (orig_font)

			create tc
			font_box.extend (tc)
			tc.set_minimum_width (10)

			font_box.extend (font_btn)
			font_btn.set_notification_procedure (agent update_font)

			create tc
			font_box.extend (tc)
			tc.set_minimum_width (10)

			font_box.extend (font_label)
			font_label.set_font (orig_font)

			create tc
			font_box.extend (tc)
			tc.set_minimum_width (10)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (positioner_box)
--			positioner_box.set_minimum_height (50)
--			working_area.disable_item_expand (positioner_box)

			create tc
			positioner_box.extend (tc)
			tc.set_minimum_width (10)

			positioner_box.extend (positioner_label)
			positioner_label.set_font (orig_font)

			create tc
			positioner_box.extend (tc)
			tc.set_minimum_width (10)

			positioner_box.extend (positioner)
			positioner.set_notification_procedure (agent update_position)

			update_position (positioner)

			create tc
			positioner_box.extend (tc)
			tc.set_minimum_width (10)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (vtext_box)
			vtext_box.set_minimum_height (25)
			working_area.disable_item_expand (vtext_box)

			create tc
			vtext_box.extend (tc)
			tc.set_minimum_width (10)
			vtext_box.disable_item_expand (tc)

			vtext_box.extend (vtextw)
			vtextw.set_text ("ORIGINAL")
			vtextw.set_label_width (150)
			vtextw.set_text_width (100)
			vtextw.set_validation_function (agent is_text_valid)
			vtextw.set_live_validation_function (agent is_text_valid)
			vtextw.set_change_notification_procedure (agent on_text_change)

			create tc
			vtext_box.extend (tc)
			tc.set_minimum_width (10)
			vtext_box.disable_item_expand (tc)

			vtext_box.extend (vtext_toggle)
			vtext_toggle.select_actions.extend (agent on_vtext_toggle)

			create tc
			vtext_box.extend (tc)
			tc.set_minimum_width (10)
			vtext_box.disable_item_expand (tc)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (pr_list)
			pr_list.set_column_titles (<< "Col1", "Col2">>)
			pr_list.enable_multiple_selection
			pr_list.add_item (<<"Babe", "Ruth">>)
			pr_list.add_item (<<"Althea", "Gibson">>)
			pr_list.add_item (<<"Jim", "Thorpe">>)
			pr_list.add_item (<<"Babe", "Diedrickson">>)
			pr_list.add_item (<<"Jesse", "Owens">>)
			pr_list.add_item (<<"Heathcliff", "Slocumb">>)

			-------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (r_list)
			r_list.set_minimum_height (50)
			r_list.add_item ("Babe Ruth", "Baseball")
			r_list.add_item ("Althea Gibson", "Track & Field")
			r_list.add_item ("Jim Thorpe", "Track & Field, Football")
			r_list.add_item ("Babe Diedrickson", "Golf")
			r_list.add_item ("Jesse Owens", "Track & Field")
			r_list.add_item ("Heathcliff Slocumb", "Baseball")

			--------------------------

			create sep
			working_area.extend (sep)
			sep.set_minimum_height (5)
			working_area.disable_item_expand (sep)

			working_area.extend (create {EV_CELL})
		end

	--|--------------------------------------------------------------

	color_box: EV_HORIZONTAL_BOX
	color_label: EV_LABEL
	color_btn: AEL_V2_COLOR_BUTTON
	color: EV_COLOR

	font_box: EV_HORIZONTAL_BOX
	font_label: EV_LABEL
	font_btn: AEL_V2_FONT_BUTTON
	orig_font: EV_FONT

	positioner_box: EV_HORIZONTAL_BOX
	positioner_label: EV_LABEL
	positioner: AEL_V2_LABELED_POSITIONER

	vtext_box: EV_HORIZONTAL_BOX
	vtextw: AEL_V2_VALIDATING_TEXT
	vtext_toggle: EV_CHECK_BUTTON

	pr_list: AEL_V2_PRIORITY_LIST
	r_list: AEL_V2_RANKING_LIST

	window_initial_width: INTEGER
		do 
			Result := 500
		end

	window_initial_height: INTEGER
		do
			Result := 450
		end

--|========================================================================
feature -- Add Comment
--|========================================================================

	update_color (b: like color_btn)
		do
			color_label.set_background_color (b.button_color)
		end

	update_font (b: like font_btn)
		do
			font_label.set_font (b.button_font)
		end

	update_position (p: like positioner)
		do
			positioner_label.set_text (p.position_out (p.selected_position))
		end

	is_text_valid (v: STRING): BOOLEAN
		do
			if not is_destroyed then
				if default_push_button /= Void then
					remove_default_push_button
				end
				if not v.is_empty then

					Result := (not integers_only) or v.is_integer
				end
			end
		end

	--|--------------------------------------------------------------

	on_text_change (w: like vtextw)
		do
		end

	--|--------------------------------------------------------------

	on_vtext_toggle
		do
			integers_only := vtext_toggle.is_selected
		end

	integers_only: BOOLEAN

end -- class MISC_DIALOG
